﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Web.Script.Services;
namespace BusinessManager2015
{
    /// <summary>
    /// Summary description for DescriptionServices
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class DescriptionServices : System.Web.Services.WebService
    {
        
        public DescriptionServices()
        {

            //Uncomment the following line if using designed components 
            //InitializeComponent(); 
        }



             [WebMethod(EnableSession=true)]
        public List<string> GetBrand(string sDescription)
        {
            
            string compID = Session["CompanyID"].ToString();
         
            string constring = ConfigurationManager.ConnectionStrings["DBASE"].ConnectionString;
            List<string> descriptionNamesList = new List<string>();
            using (SqlConnection con = new SqlConnection(constring))
            {
                SqlCommand cmd = new SqlCommand("SELECT Name FROM BrandName where Name like @BrandName + '%' and CompID='" + compID + "' ", con);
                cmd.Parameters.AddWithValue("@BrandName", "%" + sDescription + "%");
                if (con.State==ConnectionState.Closed) { con.Open(); }
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    descriptionNamesList.Add(rdr["Name"].ToString());
                }
            }
            return descriptionNamesList;
        }


              [WebMethod(EnableSession=true)]
        public List<string> GetSalesManName(string sDescription)
        {
            
            string compID = Session["CompanyID"].ToString();
         
            string constring = ConfigurationManager.ConnectionStrings["DBASE"].ConnectionString;
            List<string> descriptionNamesList = new List<string>();
            using (SqlConnection con = new SqlConnection(constring))
            {
                SqlCommand cmd = new SqlCommand("select Name from  SaleManList where Name like @OperatorName + '%' and CompID='" + compID + "' ", con);
                cmd.Parameters.AddWithValue("@OperatorName", "%" + sDescription + "%");
                if (con.State==ConnectionState.Closed) { con.Open(); }
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    descriptionNamesList.Add(rdr["Name"].ToString());
                }
            }
            return descriptionNamesList;
        }

             [WebMethod(EnableSession=true)]
              public List<string> GetOperatorName(string sDescription)
        {
            
            string compID = Session["CompanyID"].ToString();
         
            string constring = ConfigurationManager.ConnectionStrings["DBASE"].ConnectionString;
            List<string> descriptionNamesList = new List<string>();
            using (SqlConnection con = new SqlConnection(constring))
            {
                SqlCommand cmd = new SqlCommand("select Name from Userss where Name like @OperatorName + '%' and CompID='" + compID + "' ", con);
                cmd.Parameters.AddWithValue("@OperatorName", "%" + sDescription + "%");
                if (con.State==ConnectionState.Closed) { con.Open(); }
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    descriptionNamesList.Add(rdr["Name"].ToString());
                }
            }
            return descriptionNamesList;
        }

        [WebMethod(EnableSession=true)]
        public List<string> GetDescriptions(string sDescription)
        {
            
            string compID = Session["CompanyID"].ToString();
         
            string constring = ConfigurationManager.ConnectionStrings["DBASE"].ConnectionString;
            List<string> descriptionNamesList = new List<string>();
            using (SqlConnection con = new SqlConnection(constring))
            {
                SqlCommand cmd = new SqlCommand("select Description from InvCode where Description like @Description + '%' and CompID='"+compID+"' ", con);
                cmd.Parameters.AddWithValue("@Description", "%" + sDescription + "%");
                if (con.State==ConnectionState.Closed) { con.Open(); }
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    descriptionNamesList.Add(rdr["Description"].ToString());
                }
            }
            return descriptionNamesList;
        }

        //[WebMethod]
        //public List<string> GetDescriptions(string sDescription)
        //{
        //    string constring = ConfigurationManager.ConnectionStrings["DBASE"].ConnectionString;
        //    List<string> descriptionNamesList = new List<string>();
        //    using (SqlConnection con = new SqlConnection(constring))
        //    {
        //        SqlCommand cmd = new SqlCommand("spGetmatchingDescription", con);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@Description", "%" + sDescription + "%");
        //        if (con.State==ConnectionState.Closed) { con.Open(); }
        //        SqlDataReader rdr = cmd.ExecuteReader();
        //        while (rdr.Read())
        //        {
        //            descriptionNamesList.Add(rdr["Description"].ToString());
        //        }
        //    }
        //    return descriptionNamesList;
        //}




            [WebMethod(EnableSession = true)]
        public List<string> GetExpenseHeadNames(string EName)
        {
            string compID = Session["CompanyID"].ToString();
            string constring = ConfigurationManager.ConnectionStrings["DBASE"].ConnectionString;
            List<string> partyNamesList = new List<string>();
            using (SqlConnection con = new SqlConnection(constring))
            {
                SqlCommand cmd = new SqlCommand("select Title from GLCode where  Title like @ExpenseHeadNames + '%' and Lvl=5 and Code like '0104%' and CompID='" + compID + "' ", con);
         
                cmd.Parameters.AddWithValue("@ExpenseHeadNames", "%" + EName + "%");
                if (con.State==ConnectionState.Closed) { con.Open(); }
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    partyNamesList.Add(rdr["Title"].ToString());

                }
            }
            return partyNamesList;
        }

        [WebMethod(EnableSession = true)]
        public List<string> GetPartyNames(string PName)
            {
                string compID = Session["CompanyID"].ToString();
            string constring = ConfigurationManager.ConnectionStrings["DBASE"].ConnectionString;
            List<string> partyNamesList = new List<string>();
            using (SqlConnection con = new SqlConnection(constring))
            {
                SqlCommand cmd = new SqlCommand("select Name from PartyCode where Name like @PartyNames + '%' and CompID='" + compID + "' ", con);
               
                cmd.Parameters.AddWithValue("@PartyNames", "%" + PName + "%");
                if (con.State==ConnectionState.Closed) { con.Open(); }
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    partyNamesList.Add(rdr["Name"].ToString());

                }
            }
            return partyNamesList;
        }
        [WebMethod(EnableSession = true)]
        public List<string> getPartyGroupName(string PartyGroupName)
        {
            string compID = Session["CompanyID"].ToString();
            string constring = ConfigurationManager.ConnectionStrings["DBASE"].ConnectionString;
            List<string> partyNamesList = new List<string>();
            using (SqlConnection con = new SqlConnection(constring))
            {
                SqlCommand cmd = new SqlCommand("select GroupName from PartyGroup where GroupName like @PartyGroupName + '%' and CompID='" + compID + "' ", con);
         
                cmd.Parameters.AddWithValue("@PartyGroupName", "%" + PartyGroupName + "%");
                if (con.State==ConnectionState.Closed) { con.Open(); }
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    partyNamesList.Add(rdr["GroupName"].ToString());

                }
            }
            return partyNamesList;
        }
    }
}
