﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using WebPOS.Model;

namespace WebPOS.Services
{
    /// <summary>
    /// Summary description for VoucherService
    /// </summary>
    [WebService(Namespace = "http://webpos.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class VoucherService : System.Web.Services.WebService
    {

        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetBankPaymentVoucherData(string VoucherNo)
        {
            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                Module8 objModule8 = new Module8();
                ModGLCode objModGLCode = new ModGLCode();
                //cmd.CommandText = "Select dateDr,AmountDr,VenderCode,Narration from GeneralLedger where  CompID='" + CompID + "' and V_Type='CRV' and V_No=" + VoucherNo + " and Code='" + "0101010100001" + "' and SecondDescription='FromParties'";
                cmd.CommandText = @"SELECT dateDr, AmountDr, AmountCr, V_Type, V_No, Description, VenderCode, Narration, 
                            BankCode.BankCode, 
                            BankCode.BankName, 
                            BankCode.AccountNo, 
                            GLCode.Title,
                            GLCode.Code,
                            V_No,
                            GeneralLedger.BankCode,GeneralLedger.ChequeNo
                            FROM GeneralLedger
                            Join BankCode On GeneralLedger.BankCode = BankCode.BankCode
                            Join GLCode On GeneralLedger.Code = GLCode.Code
                            WHERE AmountDr>0 and V_Type ='BPV' and V_No =" + VoucherNo;
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();

                VoucherViewModel ModelPaymentVoucher=new VoucherViewModel();
                var objModule1 = new Module1();
                var lastVoucherNo = (objModule1.MaxBPV() - 1);
                var IsLastVoucherNo = lastVoucherNo == Convert.ToDecimal(VoucherNo) || lastVoucherNo < Convert.ToDecimal(VoucherNo);
                    var data = objDs.Tables[0];

               

                if (objDs.Tables[0].Rows.Count > 0)
                {

                    string CreditGLTitle = data.Rows[0]["BankName"].ToString();
                    string VoucherNumber = data.Rows[0]["V_No"].ToString();
                    string CreditGLCode = data.Rows[0]["BankCode"].ToString();
                    string VoucherDate = Convert.ToDateTime(data.Rows[0]["dateDr"].ToString()).ToString("dd/MM/yyyy");
                    ModelPaymentVoucher = new VoucherViewModel()
                    {
                        VoucherNumber = VoucherNumber,
                        VoucherDate = VoucherDate,
                        CreditGLCode = CreditGLCode,
                        CreditGLTitle = CreditGLTitle,
                        IsLastVoucherNo= IsLastVoucherNo,
                        Success=true
                    };
                    var rows = new List<Voucher>();
                    for (int i = 0; i < data.Rows.Count; i++)
                    {
                        string AmountDr = data.Rows[i]["AmountDr"].ToString();
                        string Narration = data.Rows[i]["Narration"].ToString();
                        string GLCode = data.Rows[i]["Code"].ToString();
                        string GLTitle = data.Rows[i]["Title"].ToString();
                        string ChequeNo = data.Rows[i]["ChequeNo"].ToString();
                        var row = new Voucher()
                        {
                            ChequeNumber = ChequeNo,
                            Debit = AmountDr,
                            Narration = Narration,
                            Title = GLTitle,
                            Code=GLCode
                        };
                        rows.Add(row);
                    }
                    ModelPaymentVoucher.Rows = rows;


                }
                else
                {
                    ModelPaymentVoucher.Message="No Record Found.";
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));
            }
            catch (Exception ex)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var ModelPaymentVoucher = new ModelPaymentVoucher()
                {
                    Success = false,
                    Message = ex.Message
                };
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));

            }
            finally
            {
                con.Close();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetCashPaymentVoucherData(string VoucherNo)
        {
            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                Module8 objModule8 = new Module8();
                ModGLCode objModGLCode = new ModGLCode();
                //cmd.CommandText = "Select dateDr,AmountDr,VenderCode,Narration from GeneralLedger where  CompID='" + CompID + "' and V_Type='CRV' and V_No=" + VoucherNo + " and Code='" + "0101010100001" + "' and SecondDescription='FromParties'";
                cmd.CommandText = @"SELECT datedr,Description,Narration,AmountDr,V_NO,V_type,System_Date_Time,
							Code1 as CreditGLCode,V_No,
							GeneralLedger.Code as DebitGLCode,
							creditGL.Title as CreditGLTitle,
							debitGL.Title as DebitGLTitle,
							GeneralLedger.Code as DebitGLCode
                            FROM GeneralLedger
                            Join GLCode as debitGL On GeneralLedger.Code = debitGL.Code
                            Join GLCode as creditGL On GeneralLedger.Code1 = creditGL.Code
                            WHERE AmountDr>0 and V_Type ='CPV' and V_No =" + VoucherNo;
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();

                VoucherViewModel ModelPaymentVoucher = new VoucherViewModel();
                var objModule1 = new Module1();
                var lastVoucherNo = (objModule1.MaxCPV() - 1);
                var IsLastVoucherNo = lastVoucherNo == Convert.ToDecimal(VoucherNo) || lastVoucherNo < Convert.ToDecimal(VoucherNo);
                var data = objDs.Tables[0];



                if (objDs.Tables[0].Rows.Count > 0)
                {

                    string CreditGLTitle = data.Rows[0]["CreditGLTitle"].ToString();
                    string VoucherNumber = data.Rows[0]["V_No"].ToString();
                    string CreditGLCode = data.Rows[0]["CreditGLCode"].ToString();
                    string VoucherDate = Convert.ToDateTime(data.Rows[0]["dateDr"].ToString()).ToString("dd/MM/yyyy");
                    ModelPaymentVoucher = new VoucherViewModel()
                    {
                        VoucherNumber = VoucherNumber,
                        VoucherDate = VoucherDate,
                        CreditGLCode = CreditGLCode,
                        CreditGLTitle = CreditGLTitle,
                        IsLastVoucherNo = IsLastVoucherNo,
                        Success = true
                    };
                    var rows = new List<Voucher>();
                    for (int i = 0; i < data.Rows.Count; i++)
                    {
                        string AmountDr = data.Rows[i]["AmountDr"].ToString();
                        string Narration = data.Rows[i]["Narration"].ToString();
                        string GLCode = data.Rows[i]["DebitGLCode"].ToString();
                        string GLTitle = data.Rows[i]["DebitGLTitle"].ToString();
                        var row = new Voucher()
                        {
                            Debit = AmountDr,
                            Narration = Narration,
                            Title = GLTitle,
                            Code = GLCode
                        };
                        rows.Add(row);
                    }
                    ModelPaymentVoucher.Rows = rows;


                }
                else
                {
                    ModelPaymentVoucher.Message = "No Record Found.";
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));
            }
            catch (Exception ex)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var ModelPaymentVoucher = new ModelPaymentVoucher()
                {
                    Success = false,
                    Message = ex.Message
                };
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));

            }
            finally
            {
                con.Close();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetCashReceiveVoucherData(string VoucherNo)
        {
            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                Module8 objModule8 = new Module8();
                ModGLCode objModGLCode = new ModGLCode();
                //cmd.CommandText = "Select dateDr,AmountDr,VenderCode,Narration from GeneralLedger where  CompID='" + CompID + "' and V_Type='CRV' and V_No=" + VoucherNo + " and Code='" + "0101010100001" + "' and SecondDescription='FromParties'";
                cmd.CommandText = @"SELECT datedr,Description
,Narration,AmountCr,V_NO,V_type,System_Date_Time,
							Code1 as CreditGLCode,V_No,
							GeneralLedger.Code as DebitGLCode,
							creditGL.Title as CreditGLTitle,
							debitGL.Title as DebitGLTitle,
							GeneralLedger.Code as DebitGLCode
                            FROM GeneralLedger
                            Join GLCode as debitGL On GeneralLedger.Code = debitGL.Code
                            Join GLCode as creditGL On GeneralLedger.Code1 = creditGL.Code
                            WHERE AmountCr>0 and V_Type ='CRV' and V_No =" + VoucherNo;
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();

                VoucherViewModel ModelPaymentVoucher = new VoucherViewModel();
                var objModule1 = new Module1();
                var lastVoucherNo = (objModule1.MaxCRV() - 1);
                var IsLastVoucherNo = lastVoucherNo == Convert.ToDecimal(VoucherNo) || lastVoucherNo < Convert.ToDecimal(VoucherNo);
                var data = objDs.Tables[0];



                if (objDs.Tables[0].Rows.Count > 0)
                {

                    string CreditGLTitle = data.Rows[0]["CreditGLTitle"].ToString();
                    string VoucherNumber = data.Rows[0]["V_No"].ToString();
                    string CreditGLCode = data.Rows[0]["CreditGLCode"].ToString();
                    string VoucherDate = Convert.ToDateTime(data.Rows[0]["dateDr"].ToString()).ToString("dd/MM/yyyy");
                    ModelPaymentVoucher = new VoucherViewModel()
                    {
                        VoucherNumber = VoucherNumber,
                        VoucherDate = VoucherDate,
                        CreditGLCode = CreditGLCode,
                        CreditGLTitle = CreditGLTitle,
                        IsLastVoucherNo = IsLastVoucherNo,
                        Success = true
                    };
                    var rows = new List<Voucher>();
                    for (int i = 0; i < data.Rows.Count; i++)
                    {
                        string AmountDr = data.Rows[i]["AmountCr"].ToString();
                        string Narration = data.Rows[i]["Narration"].ToString();
                        string GLCode = data.Rows[i]["DebitGLCode"].ToString();
                        string GLTitle = data.Rows[i]["DebitGLTitle"].ToString();
                        var row = new Voucher()
                        {
                            Debit = AmountDr,
                            Narration = Narration,
                            Title = GLTitle,
                            Code = GLCode
                        };
                        rows.Add(row);
                    }
                    ModelPaymentVoucher.Rows = rows;


                }
                else
                {
                    ModelPaymentVoucher.Message = "No Record Found.";
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));
            }
            catch (Exception ex)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var ModelPaymentVoucher = new ModelPaymentVoucher()
                {
                    Success = false,
                    Message = ex.Message
                };
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));

            }
            finally
            {
                con.Close();
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetBankReceiveVoucherData(string VoucherNo)
        {
            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                Module8 objModule8 = new Module8();
                ModGLCode objModGLCode = new ModGLCode();
                //cmd.CommandText = "Select dateDr,AmountDr,VenderCode,Narration from GeneralLedger where  CompID='" + CompID + "' and V_Type='CRV' and V_No=" + VoucherNo + " and Code='" + "0101010100001" + "' and SecondDescription='FromParties'";
                cmd.CommandText = @"SELECT datedr,Description,
                            Narration,AmountCr,V_NO,V_type,System_Date_Time,
							Code1 as CreditGLCode,V_No,
							GeneralLedger.Code as DebitGLCode,
							creditGL.Title as CreditGLTitle,
							debitGL.Title as DebitGLTitle,
							GeneralLedger.Code as DebitGLCode,GeneralLedger.ChequeNo
                            FROM GeneralLedger
                            Join GLCode as debitGL On GeneralLedger.Code = debitGL.Code
                            Join GLCode as creditGL On GeneralLedger.Code1 = creditGL.Code
                            WHERE AmountCr>0 and V_Type ='BRV' and V_No =" + VoucherNo;
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();

                VoucherViewModel ModelPaymentVoucher = new VoucherViewModel();
                var objModule1 = new Module1();
                var lastVoucherNo = (objModule1.MaxBRV() - 1);
                var IsLastVoucherNo = lastVoucherNo == Convert.ToDecimal(VoucherNo) || lastVoucherNo < Convert.ToDecimal(VoucherNo);
                var data = objDs.Tables[0];



                if (objDs.Tables[0].Rows.Count > 0)
                {

                    string CreditGLTitle = data.Rows[0]["CreditGLTitle"].ToString();
                    string VoucherNumber = data.Rows[0]["V_No"].ToString();
                    string CreditGLCode = data.Rows[0]["CreditGLCode"].ToString();
                    string VoucherDate = Convert.ToDateTime(data.Rows[0]["dateDr"].ToString()).ToString("dd/MM/yyyy");
                    ModelPaymentVoucher = new VoucherViewModel()
                    {
                        VoucherNumber = VoucherNumber,
                        VoucherDate = VoucherDate,
                        CreditGLCode = CreditGLCode,
                        CreditGLTitle = CreditGLTitle,
                        IsLastVoucherNo = IsLastVoucherNo,
                        Success = true
                    };
                    var rows = new List<Voucher>();
                    for (int i = 0; i < data.Rows.Count; i++)
                    {
                        string AmountDr = data.Rows[i]["AmountCr"].ToString();
                        string Narration = data.Rows[i]["Narration"].ToString();
                        string GLCode = data.Rows[i]["DebitGLCode"].ToString();
                        string GLTitle = data.Rows[i]["DebitGLTitle"].ToString();
                        string ChequeNumber = data.Rows[i]["ChequeNo"].ToString();
                        var row = new Voucher()
                        {
                            Debit = AmountDr,
                            Narration = Narration,
                            Title = GLTitle,
                            Code = GLCode,
                            ChequeNumber= ChequeNumber
                        };
                        rows.Add(row);
                    }
                    ModelPaymentVoucher.Rows = rows;


                }
                else
                {
                    ModelPaymentVoucher.Message = "No Record Found.";
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));
            }
            catch (Exception ex)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var ModelPaymentVoucher = new ModelPaymentVoucher()
                {
                    Success = false,
                    Message = ex.Message
                };
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));

            }
            finally
            {
                con.Close();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetJournalVoucherData(string VoucherNo)
        {
            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                Module8 objModule8 = new Module8();
                ModGLCode objModGLCode = new ModGLCode();
                //cmd.CommandText = "Select dateDr,AmountDr,VenderCode,Narration from GeneralLedger where  CompID='" + CompID + "' and V_Type='CRV' and V_No=" + VoucherNo + " and Code='" + "0101010100001" + "' and SecondDescription='FromParties'";
                cmd.CommandText = @"SELECT datedr,Description,
                            Narration,AmountCr,AmountDr,V_NO,V_type,System_Date_Time,
							GeneralLedger.Code as GLCode,
							debitGL.Title as GLTitle
                            FROM GeneralLedger
                            Join GLCode as debitGL On GeneralLedger.Code = debitGL.Code
                            WHERE V_Type ='JV' and V_No =" + VoucherNo;
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();

                VoucherViewModel ModelPaymentVoucher = new VoucherViewModel();
                var objModule1 = new Module1();
                var lastVoucherNo = (objModule1.MaxJVNo() - 1);
                var IsLastVoucherNo = lastVoucherNo == Convert.ToDecimal(VoucherNo) || lastVoucherNo < Convert.ToDecimal(VoucherNo);
                var data = objDs.Tables[0];



                if (objDs.Tables[0].Rows.Count > 0)
                {
                    
                    string VoucherNumber = data.Rows[0]["V_No"].ToString();
                    string VoucherDate = Convert.ToDateTime(data.Rows[0]["dateDr"].ToString()).ToString("dd/MM/yyyy");
                    ModelPaymentVoucher = new VoucherViewModel()
                    {
                        VoucherNumber = VoucherNumber,
                        VoucherDate = VoucherDate,
                        IsLastVoucherNo = IsLastVoucherNo,
                        Success = true
                    };
                    var rows = new List<Voucher>();
                    for (int i = 0; i < data.Rows.Count; i++)
                    {
                        string AmountDr = data.Rows[i]["AmountDr"].ToString();
                        string AmountCr = data.Rows[i]["AmountCr"].ToString();
                        string Narration = data.Rows[i]["Narration"].ToString();
                        string GLCode = data.Rows[i]["GLCode"].ToString();
                        string GLTitle = data.Rows[i]["GLTitle"].ToString();
                        var row = new Voucher()
                        {
                            Debit = AmountDr,
                            Credit = AmountCr,
                            Narration = Narration,
                            Title = GLTitle,
                            Code = GLCode
                        };
                        rows.Add(row);
                    }
                    ModelPaymentVoucher.Rows = rows;


                }
                else
                {
                    ModelPaymentVoucher.Message = "No Record Found.";
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));
            }
            catch (Exception ex)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var ModelPaymentVoucher = new ModelPaymentVoucher()
                {
                    Success = false,
                    Message = ex.Message
                };
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));

            }
            finally
            {
                con.Close();
            }
        }
    }
}
