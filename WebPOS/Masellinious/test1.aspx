﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="test1.aspx.cs" Inherits="WebPOS.Masellinious.test1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <asp:ListView ID="ListView1" runat="server" DataKeyNames="Code,CompID" DataSourceID="dbform" InsertItemPosition="FirstItem">
            <AlternatingItemTemplate>
                <tr style="background-color: #FFFFFF;color: #284775;">
                   
                    <td>
                        <asp:Label ID="CodeLabel" runat="server" Text='<%# Eval("Code") %>' />
                    </td>
                    <td>
                        <asp:Label ID="DescriptionLabel" runat="server" Text='<%# Eval("Description") %>' />
                    </td>
                    <td>
                        <asp:Label ID="SellingCostLabel" runat="server" Text='<%# Eval("SellingCost") %>' />
                    </td>
                    <td>
                        <asp:Label ID="CompIDLabel" runat="server" Text='<%# Eval("CompID") %>' />
                    </td>
                     <td>
                        <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" />
                        <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" />
                    </td>
                </tr>
            </AlternatingItemTemplate>
            <EditItemTemplate>
                <tr style="background-color: #999999;">

                    <td>
                        <asp:Label ID="CodeLabel1" runat="server" Text='<%# Eval("Code") %>' />
                    </td>
                    <td>
                        <asp:TextBox ID="DescriptionTextBox" runat="server" Text='<%# Bind("Description") %>' />
                    </td>
                    <td>
                        <asp:TextBox ID="SellingCostTextBox" runat="server" Text='<%# Bind("SellingCost") %>' />
                    </td>
                    <td>
                        <asp:Label ID="CompIDLabel1" runat="server" Text='<%# Eval("CompID") %>' />
                    </td>
                                        <td>
                        <asp:Button ID="UpdateButton" runat="server" CommandName="Update" Text="Update" />
                        <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Cancel" />
                    </td>
                </tr>
            </EditItemTemplate>
            <EmptyDataTemplate>
                <table runat="server" style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;">
                    <tr>
                        <td>No data was returned.</td>
                    </tr>
                </table>
            </EmptyDataTemplate>
            <InsertItemTemplate>
                <tr style="">
                   
                    <td>
                        <asp:TextBox ID="CodeTextBox" runat="server" Text='<%# Bind("Code") %>' />
                    </td>
                    <td>
                        <asp:TextBox ID="DescriptionTextBox" runat="server" Text='<%# Bind("Description") %>' />
                    </td>
                    <td>
                        <asp:TextBox ID="SellingCostTextBox" runat="server" Text='<%# Bind("SellingCost") %>' />
                    </td>
                    <td>
                        <asp:TextBox ID="CompIDTextBox" runat="server" Text='<%# Bind("CompID") %>' />
                    </td>
                     <td>
                        <asp:Button ID="InsertButton" runat="server" CommandName="Insert" Text="Save" />
                        <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Clear" />
                    </td>
                </tr>
            </InsertItemTemplate>
            <ItemTemplate>
                <tr style="background-color: #E0FFFF;color: #333333;">
                   
                    <td>
                        <asp:Label ID="CodeLabel" runat="server" Text='<%# Eval("Code") %>' />
                    </td>
                    <td>
                        <asp:Label ID="DescriptionLabel" runat="server" Text='<%# Eval("Description") %>' />
                    </td>
                    <td>
                        <asp:Label ID="SellingCostLabel" runat="server" Text='<%# Eval("SellingCost") %>' />
                    </td>
                    <td>
                        <asp:Label ID="CompIDLabel" runat="server" Text='<%# Eval("CompID") %>' />
                    </td>
                     <td>
                        <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" />
                        <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" />
                    </td>
                </tr>
            </ItemTemplate>
            <LayoutTemplate>
                <table runat="server">
                    <tr runat="server">
                        <td runat="server">
                            <table id="itemPlaceholderContainer" runat="server" border="1" style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;font-family: Verdana, Arial, Helvetica, sans-serif;">
                                <tr runat="server" style="background-color: #E0FFFF;color: #333333;">
                                   
                                    <th runat="server">Code</th>
                                    <th runat="server">Description</th>
                                    <th runat="server">SellingCost</th>
                                    <th runat="server">CompID</th>
                                 <th runat="server"></th>
                                </tr>
                                <tr id="itemPlaceholder" runat="server">
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr runat="server">
                        <td runat="server" style="text-align: center;background-color: #5D7B9D;font-family: Verdana, Arial, Helvetica, sans-serif;color: #FFFFFF">
                            <asp:DataPager ID="DataPager1" runat="server">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" ShowLastPageButton="True" />
                                </Fields>
                            </asp:DataPager>
                        </td>
                    </tr>
                </table>
            </LayoutTemplate>
            <SelectedItemTemplate>
                <tr style="background-color: #E2DED6;font-weight: bold;color: #333333;">
                   
                    <td>
                        <asp:Label ID="CodeLabel" runat="server" Text='<%# Eval("Code") %>' />
                    </td>
                    <td>
                        <asp:Label ID="DescriptionLabel" runat="server" Text='<%# Eval("Description") %>' />
                    </td>
                    <td>
                        <asp:Label ID="SellingCostLabel" runat="server" Text='<%# Eval("SellingCost") %>' />
                    </td>
                    <td>
                        <asp:Label ID="CompIDLabel" runat="server" Text='<%# Eval("CompID") %>' />
                    </td>
                     <td>
                        <asp:Button ID="DeleteButton" runat="server" CommandName="Delete" Text="Delete" />
                        <asp:Button ID="EditButton" runat="server" CommandName="Edit" Text="Edit" />
                    </td>
                </tr>
            </SelectedItemTemplate>
        </asp:ListView>



        <asp:SqlDataSource ID="dbform" runat="server" ConnectionString="<%$ ConnectionStrings:database %>" DeleteCommand="DELETE FROM [InvCode] WHERE [Code] = @Code AND [CompID] = @CompID" InsertCommand="INSERT INTO [InvCode] ([Code], [Description], [SellingCost], [CompID]) VALUES (@Code, @Description, @SellingCost, @CompID)" SelectCommand="SELECT [Code], [Description], [SellingCost], [CompID] FROM [InvCode]" UpdateCommand="UPDATE [InvCode] SET [Description] = @Description, [SellingCost] = @SellingCost WHERE [Code] = @Code AND [CompID] = @CompID">
            <DeleteParameters>
                <asp:Parameter Name="Code" Type="Double" />
                <asp:Parameter Name="CompID" Type="String" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="Code" Type="Double" />
                <asp:Parameter Name="Description" Type="String" />
                <asp:Parameter Name="SellingCost" Type="Double" />
                <asp:Parameter Name="CompID" Type="String" />
            </InsertParameters>
            <UpdateParameters>
                <asp:Parameter Name="Description" Type="String" />
                <asp:Parameter Name="SellingCost" Type="Double" />
                <asp:Parameter Name="Code" Type="Double" />
                <asp:Parameter Name="CompID" Type="String" />
            </UpdateParameters>
        </asp:SqlDataSource>



    </div>
    </form>
</body>
</html>
