﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="test2.aspx.cs" Inherits="WebPOS.Masellinious.test2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table>
            <tr>
                <td><center> SrNo.</center></td><td>Item Code</td><td>Item Name</td><td>Quantity</td><td>Rate</td><td>Amount</td>

            </tr>

            <tr>
                <td>
                    <asp:TextBox runat="server" ID="txtSNo" /></td><td><asp:TextBox runat="server" ID="txtItemCode" /></td><td><asp:TextBox runat="server" ID="txtItemName" /></td><td><asp:TextBox runat="server" ID="txtQty" /></td><td><asp:TextBox runat="server" ID="txtRate" /></td><td><asp:TextBox runat="server" ID="txtAmount" /></td><td>
                        <asp:Button Text="Save" ID="btnSave" runat="server" /></td>

            </tr>

        </table>
        <br />
        <asp:GridView ID="GridView1" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White"
    runat="server" AutoGenerateColumns="false">
    <Columns>
        <asp:BoundField DataField="Id" HeaderText="Id" ItemStyle-Width="30" />
        <asp:BoundField DataField="Name" HeaderText="Name" ItemStyle-Width="150" />
        <asp:BoundField DataField="Country" HeaderText="Country" ItemStyle-Width="150" />
        <asp:TemplateField HeaderText="Action">
            <ItemTemplate>
                <asp:HyperLink runat="server" NavigateUrl='<%# Eval("Name", "~/Masellinious/test2.aspx?Id={0}") %>'
                    Text='<%# Eval("Id") %>' />

            </ItemTemplate>

        </asp:TemplateField>
    </Columns>
</asp:GridView>


    </div>
    </form>
</body>
</html>
