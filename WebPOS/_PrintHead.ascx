﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="_PrintHead.ascx.cs" Inherits="WebPOS._PrintHead" %>
<div class="row justify-content-center">
    <div class="col-12 text-center">

        <h1 class="CompanyName"><%= ConfigurationManager.AppSettings["CompanyName"] %></h1>
    </div>
    <div class="col-6 text-center">

        <address class="m-0">

            <p class="StationAddress font-size-17"><strong><%= Session["StationAddress"] %></strong></p>

            <p class="StationNumber font-size-17">Phone #: <%= Session["StationPhoneNumber"] %></p>
        </address>
    </div>
</div>
