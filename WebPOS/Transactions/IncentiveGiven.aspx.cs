﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using WebPOS.Model;

namespace WebPOS
{
    public partial class IncentiveGiven : System.Web.UI.Page
    {
        static string CompID = "01";
        static string CashAccountGLCode = "0101010100001";
        static string IncentiveLossGLCode = "0104020200005";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
        static Module4 objModule4 = new Module4();
        static Module1 objModule1 = new Module1();
        static ModGLCode objModGLCode = new ModGLCode();
        //private object txtCurrentDate;
        //private object txtPartyCode;
        //private object txtBalance;
        //private object lstPartyName;
        //private object txtCashPaid;

        protected void Page_Load(object sender, EventArgs e)
        {
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(ModelPaymentVoucher ModelPaymentVoucher)
        {
            var stationId = string.Empty;
            if (HttpContext.Current.Session["StationId"] != null)
            {
                stationId = HttpContext.Current.Session["StationId"].ToString();
            }

            SqlTransaction tran;
            if (con.State==ConnectionState.Closed) { con.Open(); }
            tran = con.BeginTransaction();
            string PartyCode = "";
            PartyCode = ModelPaymentVoucher.PartyCode;
            string PartyCodeOld = "";
            PartyCodeOld = ModelPaymentVoucher.OldPartyCode;

            try
            {

                ModelPaymentVoucher.VoucherNo = Convert.ToString(objModule1.MaxJVNo(con, tran));
                string SysTime = Convert.ToString(DateTime.Now);
                var VoucherDate = DateTime.ParseExact(ModelPaymentVoucher.Date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture).ToShortDateString();

                decimal CashPaid = 0;
                if (!string.IsNullOrEmpty(ModelPaymentVoucher.CashPaid)) { CashPaid = Convert.ToDecimal(ModelPaymentVoucher.CashPaid); }

                SqlCommand cmd1 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountDr,V_NO,V_type,System_Date_Time,DescriptionOfBillNo,VenderCode,Narration,CompId, StationId) values('" + IncentiveLossGLCode + "','" + VoucherDate + "','" + "Receivables-(" + ModelPaymentVoucher.PartyName + ")" + ModelPaymentVoucher.Narration + "'," + ModelPaymentVoucher.CashPaid + "," + ModelPaymentVoucher.VoucherNo + ",'" + "JV" + "','" + SysTime + "','" + "IncentiveLoss" + "','" + ModelPaymentVoucher.PartyCode + "','" + ModelPaymentVoucher.Narration + "','" + CompID + "', " + stationId + " )", con, tran);
                cmd1.ExecuteNonQuery();
                SqlCommand cmd2 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountCr,V_NO,V_type,System_Date_Time,DescriptionOfBillNo,VenderCode,Narration,CompId, StationId) values('" + ModelPaymentVoucher.PartyCode + "','" + VoucherDate + "','" + "Incentive-(" + ModelPaymentVoucher.PartyName + ")" + ModelPaymentVoucher.Narration + "'," + ModelPaymentVoucher.CashPaid + "," + ModelPaymentVoucher.VoucherNo + ",'" + "JV" + "','" + SysTime + "','" + "IncentiveLoss" + "','" + ModelPaymentVoucher.PartyCode + "','" + ModelPaymentVoucher.Narration + "','" + CompID + "', " + stationId + " )", con, tran);
                cmd2.ExecuteNonQuery();
                SqlCommand cmd3 = new SqlCommand("insert into PartiesLedger (code,datedr,DescriptionDr,AmountCr,V_NO,V_type,System_Date_Time,DescriptionOfBillNo,VenderCode,BillNo,CompId, StationId) values('" + ModelPaymentVoucher.PartyCode + "','" + VoucherDate + "','" + "Incentive(Loss) -JV # " + ModelPaymentVoucher.VoucherNo + "-" + ModelPaymentVoucher.Narration + "'," + ModelPaymentVoucher.CashPaid + "," + ModelPaymentVoucher.VoucherNo + ",'" + "JV" + "','" + SysTime + "','" + "IncentiveLoss" + "','" + ModelPaymentVoucher.PartyCode + "','" + ModelPaymentVoucher.VoucherNo + "','" + CompID + "', " + stationId + " )", con, tran);
                cmd3.ExecuteNonQuery();

                objModule4.ClosingBalancePartiesNew(PartyCode, con, tran);
                
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Saved Successfully." };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }



        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel GetLastVouchers()
        {

            try
            {



                var stationId = string.Empty;
                if (HttpContext.Current.Session["StationId"] != null)
                {
                    stationId = HttpContext.Current.Session["StationId"].ToString();
                }
                if (con.State == ConnectionState.Closed) { con.Open(); }

                var cmd = new SqlCommand(
                    "Select " +
                    "AmountCr," +
                    "V_No," +
                    "datedr," +
                    "Narration, " +
                    "PartyCode.Name as PartyName " +
                    "from GeneralLedger " +
                    " join PartyCode on PartyCode.Code=GeneralLedger.VenderCode" +
                    " where  PartyCode.CompID='" + CompID + "' and " +
                    "StationId='" + stationId + "' and " +
                    "V_Type='JV' and " +
                    "DescriptionOfBillNo='IncentiveLoss' and " +
                    "AmountCr > 0 and " +
                    " datedr>='" + DateTime.Now.AddDays(-15).ToString("MM/dd/yyyy") + "' and datedr<='" + DateTime.Now.ToString("MM/dd/yyyy") + "' " +
                    " Order by datedr asc", con);
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];



                var reports = new List<ReportViewModel>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string CashPaid = data.Rows[i]["AmountCr"].ToString();
                    string VoucherNumber = data.Rows[i]["V_No"].ToString();
                    string Date = Convert.ToDateTime(data.Rows[i]["datedr"]).ToString("dd/MM/yyyy");
                    string Narration = data.Rows[i]["Narration"].ToString();
                    string PartyName = data.Rows[i]["PartyName"].ToString();
                    var party = new ReportViewModel()
                    {
                        Date = Date,
                        PartyName = PartyName,
                        CashPaid = CashPaid,
                        Narration = Narration,
                        VoucherNumber = VoucherNumber

                    };
                    reports.Add(party);
                }


                con.Close();
                return new BaseModel() { Success = true, Reports = reports };
            }
            catch (Exception ex)
            {

                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel GetPartyLastVouchers(string partyCode)
        {

            try
            {
                var stationId = string.Empty;
                if (HttpContext.Current.Session["StationId"] != null)
                {
                    stationId = HttpContext.Current.Session["StationId"].ToString();
                }
                if (con.State == ConnectionState.Closed) { con.Open(); }

                var cmd = new SqlCommand(
                    "Select " +
                    "AmountCr," +
                    "V_No," +
                    "datedr," +
                    "Narration, " +
                    "PartyCode.Name as PartyName " +
                    "from GeneralLedger " +
                    " join PartyCode on PartyCode.Code=GeneralLedger.VenderCode" +
                    " where  PartyCode.CompID='" + CompID + "' and " +
                    "StationId='" + stationId + "' and " +
                    "PartyCode.Code='" + partyCode + "' and " +
                    "V_Type='JV' and " +
                    "DescriptionOfBillNo='IncentiveLoss' and " +
                    "AmountCr > 0 and " +
                    " datedr>='" + DateTime.Now.AddDays(-15).ToString("MM/dd/yyyy") + "' and datedr<='" + DateTime.Now.ToString("MM/dd/yyyy") + "' " +
                    " Order by datedr asc", con);
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];



                var reports = new List<ReportViewModel>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string CashPaid = data.Rows[i]["AmountCr"].ToString();
                    string VoucherNumber = data.Rows[i]["V_No"].ToString();
                    string Date = Convert.ToDateTime(data.Rows[i]["datedr"]).ToString("dd/MM/yyyy");
                    string Narration = data.Rows[i]["Narration"].ToString();
                    string PartyName = data.Rows[i]["PartyName"].ToString();
                    var party = new ReportViewModel()
                    {
                        Date = Date,
                        PartyName = PartyName,
                        CashPaid = CashPaid,
                        Narration = Narration,
                        VoucherNumber = VoucherNumber

                    };
                    reports.Add(party);
                }


                con.Close();
                return new BaseModel() { Success = true, Reports = reports };
            }
            catch (Exception ex)
            {

                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }
    }
}
