﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="LPReceiving.aspx.cs" Inherits="WebPOS.Transactions.LPReceiving" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid mb-3">
        <div class="mt-0">
            <section class="form" id="form-section">
                <h2 class="form-header fa-money-bill-wave mb-0 p-3">LP Receiving</h2>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-9">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-3 d-flex">
                                <span class="input input--hoshi">
                                    <input class="VoucherNumber VoucherNo empty1 input__field input__field--hoshi" type="text" autocomplete="off" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi"><i class="fas fa-file"></i>Voucher #</span>
                                    </label>
                                </span>
                                <span class="input input--hoshi flex-2 ml-1">
                                    <a class="btn btn-3 btn-bm btn-3e fa-edit w-100" onclick="EditVoucher()"></a>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-2">
                                <span class="input input--hoshi flex-1">
                                    <input class="empty1 Txt1 input__field input__field--hoshi" type="text" autocomplete="off" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi"></span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-2">
                                <span class="input input--hoshi">
                                    <input class="VoucherDate input__field input__field--hoshi empty1 datetimepicker Date" type="text" autocomplete="off" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi"><i class="fas fa-calendar-day"></i>Date</span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-2">
                                <span class="input input--hoshi input--filled">
                                    <select class="round empty1 input__field input__field--hoshi">
                                        <option>option 1</option>
                                        <option>option 1</option>
                                        <option>option 1</option>
                                        <option>option 1</option>
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi"><i class="fas fa-list-ul"></i>List</span>
                                    </label>
                                </span>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                                <span class="input input--hoshi">
                                    <input class="PartyBalance empty1 input__field input__field--hoshi" type="text" autocomplete="off" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi"><i class="fas fa-money-bill-alt"></i>Prev. Balance</span>
                                    </label>
                                </span>
                            </div>


                        </div>
                        <div class="row">

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                                <span class="input input--hoshi">
                                    <input id="txtPartyName" class="PartyBox PartyName autocomplete empty1  input__field input__field--hoshi" data-nextfocus="#txbxIMELPRec" data-type="Party" data-id="txtPartyName" data-function="GetParty" type="text" autocomplete="off" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi"><i class="fas fa-user"></i>Enter Party Name</span>
                                    </label>
                                </span>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                                <span class="input input--hoshi">

                                    <input class="PartyCode input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi"><i class="fas fa-address-card"></i>Party Code</span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                                <span class="input input--hoshi">
                                    <input class="Remarks input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi"><i class="fas fa-list-ol"></i>Remarks</span>
                                    </label>
                                </span>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                                <span class="input input--hoshi">

                                    <input class="LPAmount input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi"><i class="fas fa-money-bill-wave-alt"></i>LP Amount</span>
                                    </label>
                                </span>
                            </div>

                        </div>
                        <div class="row">

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 d-flex">
                                <span class="input input--hoshi checkbox--hoshi flex-3">
                                    <label>
                                        <input id="IsIMEVerify" name="IsIMEVerify" class="checkbox" type="checkbox" checked />
                                        <span></span>
                                        Verify
                                    </label>
                                </span>
                                <span class="input input--hoshi flex-1">
                                    <input id="txbxIMELPRec" data-type="PReceiving" data-function="GetLPReceivingIMEData" 
                                         data-callback="insertLPReceivingRow" data-preventautocomplete="true" class="IME empty1 input__field input__field--hoshi" type="text" autocomplete="off" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi"><i class="fas fa-barcode"></i>IME</span>
                                    </label>
                                </span>
                            </div>

                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                                <span class="input input--hoshi">
                                    <input class="Txt2 input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi"><i class="fas fa-address-card"></i>UnkownText</span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                                <span class="input input--hoshi">
                                    <input class="NewBalance input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi"><i class="fas fa-money-bill"></i>New Balance</span>
                                    </label>
                                </span>
                            </div>


                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3">
                        <div class="row">
                            <div class="col-8">
                                <span class="input input--hoshi checkbox--hoshi">
                                    <label>
                                        <input id="chxbx1" name="chxbx1" class="checkbox" type="checkbox" />
                                        <span></span>
                                        Unkown Text
                                    </label>
                                </span>
                            </div>
                            <div class="col-4">
                                <span class="input input--hoshi">
                                    <a class="btn btn-3 btn-bm btn-3e fa-print w-100" onclick="EditVoucher()"></a>
                                </span>
                            </div>

                            <div class="col-12 d-flex">
                                <span class="input input--hoshi radio--hoshi">
                                    <label>
                                        <input id="Left" name="SearchOption" class="radio Left" value="Left" type="radio" />
                                        <span></span>
                                        Left
                                    </label>
                                </span>
                                <span class="input input--hoshi radio--hoshi">
                                    <label>
                                        <input id="Right" name="SearchOption" class="radio Right" value="Right" type="radio" />
                                        <span></span>
                                        Right
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 d-flex">
                                <span class="input input--hoshi radio--hoshi">
                                    <label>
                                        <input id="AnyWhere" name="SearchOption" class="radio AnyWhere" value="AnyWhere" type="radio" />
                                        <span></span>
                                        Any Where
                                    </label>
                                </span>
                                <span class="input input--hoshi radio--hoshi">
                                    <label>
                                        <input id="ExactMatch" name="SearchOption" class="radio Right" value="ExactMatch" type="radio" checked />
                                        <span></span>
                                        Exact Match
                                    </label>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100 fa-sync" onclick="reArrangeItems()">Arrange </a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <span class="input input--hoshi">
                            <a class="btn btn-3 btn-green btn-3e w-100 fa-search">Find </a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e  w-100 fa-sync" id="btnReset" onclick="location.href=location.href">Reset </a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="save()" class=" btn btn-3 btn-bm btn-3e fa-save success w-100">Save</a>
                        </span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <div class="table-1">

                            <ul class="item-list striped narrow">
                                <li class="item item-list-header">
                                    <div class="item-row  pl-1 pr-4">

                                        <div class="item-col item-col-header flex-1">
                                            <div>
                                                <span>S#</span>
                                            </div>
                                        </div>
                                        <div class="item-col item-col-header flex-2">
                                            <div>
                                                <span>Code</span>
                                            </div>
                                        </div>
                                        <div class="item-col item-col-header flex-4">
                                            <div>
                                                <span>Desc</span>
                                            </div>
                                        </div>
                                        <div class="item-col item-col-header flex-2">
                                            <div>
                                                <span>Brand</span>
                                            </div>
                                        </div>
                                        <div class="item-col item-col-header flex-5">
                                            <div>
                                                <span>IME</span>
                                            </div>
                                        </div>
                                        <div class="item-col item-col-header flex-3">
                                            <div>
                                                <span>LP</span>
                                            </div>
                                        </div>
                                        <div class="item-col item-col-header flex-2">
                                            <div>
                                                <span>B.Code</span>
                                            </div>
                                        </div>
                                        <div class="item-col item-col-header flex-1">
                                            <div>                                                
                                            </div>
                                        </div>
                                    </div>
                                </li>

                            </ul>

                            <ul class="item-list data-list striped narrow  h-8  overflow-scroll-y">
                            </ul>
                            
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <div class="table-2">

                            <ul class="item-list striped narrow">
                                <li class="item item-list-header">
                                    <div class="item-row  pl-1 pr-4">

                                        <div class="item-col item-col-header flex-1">
                                            <div>
                                                <span>S#</span>
                                            </div>
                                        </div>
                                        <div class="item-col item-col-header flex-3">
                                            <div>
                                                <span>Code</span>
                                            </div>
                                        </div>
                                        <div class="item-col item-col-header flex-5">
                                            <div>
                                                <span>Brand</span>
                                            </div>
                                        </div>
                                        <div class="item-col item-col-header flex-3">
                                            <div>
                                                <span>Qty</span>
                                            </div>
                                        </div>
                                        <div class="item-col item-col-header flex-4">
                                            <div>
                                                <span>LP Rate</span>
                                            </div>
                                        </div>
                                        <div class="item-col item-col-header flex-4">
                                            <div>
                                                <span>Amount</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                            </ul>

                            <ul class="item-list data-list striped narrow h-8  overflow-scroll-y">
                            </ul>
                            <ul class="item-list footer-data narrow striped">
                                <li class="item item-list-header">
                                    <div class="item-row pl-3 pr-4">
                                        <div class="item-col item-col-header flex-1">
                                        </div>
                                        <div class="item-col item-col-header flex-3">
                                        </div>
                                        <div class="item-col item-col-header flex-5">
                                        </div>
                                        <div class="item-col item-col-header flex-3">
                                        </div>
                                        <div class="item-col item-col-header flex-4">
                                           
                                        </div>
                                        <div class="item-col item-col-header flex-4">
                                            <div>
                                                <input class="text-right px-2 py-0 form-control" disabled="disabled" value="0" name='totalAmount' />

                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12">
                        <div class="table-3">

                            <ul class="item-list striped narrow">
                                <li class="item item-list-header">
                                    <div class="item-row  pl-1 pr-4">

                                        <div class="item-col item-col-header flex-3">
                                            <div>
                                                <span>Code</span>
                                            </div>
                                        </div>
                                        <div class="item-col item-col-header flex-5">
                                            <div>
                                                <span>Item Name</span>
                                            </div>
                                        </div>
                                        <div class="item-col item-col-header flex-3">
                                            <div>
                                                <span>Brand</span>
                                            </div>
                                        </div>
                                        <div class="item-col item-col-header flex-4">
                                            <div>
                                                <span>Qty In Hand</span>
                                            </div>
                                        </div>
                                        <div class="item-col item-col-header flex-4">
                                            <div>
                                                <span>Date</span>
                                            </div>
                                        </div>
                                        <div class="item-col item-col-header flex-4">
                                            <div>
                                                <span>Transaction</span>
                                            </div>
                                        </div>
                                        <div class="item-col item-col-header flex-4">
                                            <div>
                                                <span>Party Name</span>
                                            </div>
                                        </div>
                                        <div class="item-col item-col-header flex-4">
                                            <div>
                                                <span>Voucher No.</span>
                                            </div>
                                        </div>
                                        <div class="item-col item-col-header flex-4">
                                            <div>
                                                <span>Price</span>
                                            </div>
                                        </div>
                                        <div class="item-col item-col-header flex-4">
                                            <div>
                                                <span>IME</span>
                                            </div>
                                        </div>
                                    </div>
                                </li>

                            </ul>

                            <ul class="item-list data-list striped narrow h-6 overflow-scroll-y">
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </div>

    </div>

</asp:Content>
<asp:Content ContentPlaceHolderID="Scripts" runat="server">
    <script src="/Script/Autocomplete.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="LPReceiving.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
   
</asp:Content>
