﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;


namespace WebPOS.Transactions
{
    public partial class Assembling : System.Web.UI.Page
    {
        static string CompId = ConfigurationManager.AppSettings["CompID"].ToString();
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(AssemblingViewModel model)
        {
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                var deleteQuery = @"Delete From Ingridients_Cost Where ItemCode='" + model.MainItemCode + "' And CompId='" + CompId + "'";
                var deleteCmd = new SqlCommand(deleteQuery, con);
                deleteCmd.ExecuteNonQuery();
                tran = con.BeginTransaction();

               

                foreach (var item in model.Ingredients)
                {
                    var query = @"Insert Into Ingridients_Cost (
                                ItemCode
                                ,Ingridient_Code
                                ,Ingridient_Qty
                                ,Ingridient_Rate
                                ,Amount
                                ,CompID)
                              Values
                                (
                                '" + 
                                model.MainItemCode +"','"+ item.Code +"','"+item.Qty+ "','" + item.Rate + "','" + item.Amount+ "','" + CompId
                                +"')";
                    var cmd = new SqlCommand(query,con,tran);
                    cmd.ExecuteNonQuery();
                }
             

                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Saved Successfully!" };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }
    }
}