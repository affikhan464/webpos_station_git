﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="ProcessDepartment.aspx.cs" Inherits="WebPOS.Transactions.ProcessDepartment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="reports searchAppSection">
        <div class="contentContainer">
            <h2 class="text-align-center"><i class=" fas fa-file-alt" aria-hidden="true"></i> Process Department</h2>
            <div class="BMSearchWrapper clearfix row">
                <!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->

                <div class="col col-12 col-sm-6 col-md-2">
                    <span class="userlLabel">Voucher#</span>
                    <input id="VoucherNumber" />
                </div>
                 <div class="col col-12 col-sm-3 col-md-1">
                    <a href="#" id="getBtn" class="btn btn-bm"><i class="fas fa-spinner"></i> Get</a>
                </div>
                <div class="col col-12 col-sm-6 col-md-2">
                    <span class="userlLabel">Date</span>
                    <input id="VoucherDate" class="datetimepicker Date" />

                </div>
                 <div class="col col-12 col-sm-3 col-md-1">
                    <a href="#" id="auto" class="btn btn-bm"><i class="fas fa-cog"></i> Auto</a>
                </div>
                <div class="col col-12 col-sm-6 col-md-2">
                    <input id="SearchBox" data-id="Stocks" data-type="Item" data-function="GetItems" class="autocomplete AllVouchersTitle descritionTxbx" placeholder="Enter Description" />
                </div>
                <div class="col col-12 col-sm-6 col-md-3">
                    <span class="userlLabel">Narration</span>
                    <input class="Narration" placeholder="Enter Narration" />
                </div>

                <div class="col col-12 col-sm-3 col-md-1">
                    <a href="#" id="saveBtn" class="btn btn-bm"><i class="fas fa-save"></i> Save</a>
                </div>

            </div>
            <!-- searchAppSection -->
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center pt-1">
                <label class="processLabel active"> <input type="radio" name="process" checked="checked" value="Issued" /> Issued</label>
            </div>
            <div class="col col-6 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center pt-1">
               
                <label class="processLabel"> <input type="radio" name="process" value="Received" /> Received</label>
            </div>
        </div>
    <div class="row">
        <div class="col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <div class="BMtable Issued">
                
				<div class="contentContainer">					

					<div class="reports recommendedSection allItemsView">


								<div class="itemsHeader">
                                  
									<div class="five  phCol">
										<a href="javascript:void(0)"> 
											S#
										</a>
									</div>
									<div class="sixteen   phCol">                                                                    
										<a href="javascript:void(0)"> 
											Code
										</a>
									</div>
                                    <div class="sixteen   phCol">                                                                    
										<a href="javascript:void(0)"> 
											Description
										</a>
									</div>
                                    <div class="sixteen   phCol">                                                                    
										<a href="javascript:void(0)"> 
											Qty
										</a>
									</div>
									<div class="sixteen  phCol">
										<a href="javascript:void(0)"> 
											Rate
										</a>
									</div>
								
									<div class="sixteen  phCol">
										<a href="javascript:void(0)"> 
										    Amount
										</a>
									</div>
                                   
								</div><!-- itemsHeader -->	
                         <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
								<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow'>
								<div class='five '><span></span></div>
								<div class='sixteen  '><span></span></div>
								<div class='sixteen  '><span></span></div>
								<div class='sixteen  '><input type="number"  disabled="disabled" value="0"  name='totalIssuedQty'/></div>
								<div class='sixteen '><input type="number"  disabled="disabled" value="0"      name='totalIssuedRate'/></div>
								<div class='sixteen  '><input type="number"  disabled="disabled" value="0"     name='totalIssuedAmount'/></div>
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
        </div>
        <div class="col col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <div class="BMtable Received">
                
				<div class="contentContainer">					

					<div class="reports recommendedSection allItemsView">


								<div class="itemsHeader">
                                  
									<div class="five  phCol">
										<a href="javascript:void(0)"> 
											S#
										</a>
									</div>
									<div class="sixteen   phCol">                                                                    
										<a href="javascript:void(0)"> 
											Code
										</a>
									</div>
                                    <div class="sixteen   phCol">                                                                    
										<a href="javascript:void(0)"> 
											Description
										</a>
									</div>
                                    <div class="sixteen   phCol">                                                                    
										<a href="javascript:void(0)"> 
											Qty
										</a>
									</div>
									<div class="sixteen  phCol">
										<a href="javascript:void(0)"> 
											Rate
										</a>
									</div>
								
									<div class="sixteen  phCol">
										<a href="javascript:void(0)"> 
										    Amount
										</a>
									</div>
                                   
								</div><!-- itemsHeader -->	
                         <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
								<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow'>
								<div class='five '><span></span></div>
								<div class='sixteen  '><span></span></div>
								<div class='sixteen  '><span></span></div>
								<div class='sixteen  '><input type="number"  disabled="disabled" value="0"  name='totalReceivedQty'/></div>
								<div class='sixteen '><input type="number"  disabled="disabled" value="0"      name='totalReceivedRate'/></div>
								<div class='sixteen  '><input type="number"  disabled="disabled" value="0"     name='totalReceivedAmount'/></div>
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
        </div>
    </div>
    </div>
    

    <script src="/Script/Autocomplete.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="/Script/ProcessDepartment.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script type="text/javascript">

        var counter = 0;

        $(document).ready(function () {

            initializeDatePicker();
            resizeTable();

        });

        $(window).resize(function () {
            resizeTable();

        });

      
        $("#toTxbx").on("keypress", function (e) {
            if (e.key == 13) {
                $("#currentPage").remove();
                $("<input hidden id='currentPage' value='0' />").appendTo("body");
                clearInvoice();
                counter = 0;
                getReport();

            }
        });
    </script>
</asp:Content>
