﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="ExpenseEntry.aspx.cs" Inherits="WebPOS.ExpenseEntry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="container">
        <div class="mt-4">

            <section class="form">
                <h2 class="form-header fa-money-bill">Expence Entry</h2>
                <div class="slider-wrapper ">
                    <a class="fas fa-chevron-up fa-chevron-down slider l-0 r-0 b-0 mx-auto position-absolute text-center p-1"></a>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                            <span class="input input--hoshi">
                                <input id="txtVoucherNo" class="VoucherNo input__field input__field--hoshi" disabled autocomplete="off" type="text" />

                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Voucher</span>
                                </label>
                            </span>
                            <select id="StationDD"></select>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-2 col-xl-2 d-flex">
                            <span class="input input--hoshi">
                                <a class="btn btn-3 btn-bm btn-3e w-100" onclick="EditVoucher()" data-toggle="tooltip" title="Edit"><i class="fas fa-edit text-white"></i></a>
                            </span>
                            <span class="input input--hoshi"> 
                                <a class="btn btn-3 btn-bm btn-3e w-100 PrintBtn" id="PrintBtn" data-toggle="tooltip" title="Print"><i class="fas fa-print text-white"></i></a>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">
                                <input id="txtDate" class="datetimepicker Date input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Date</span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">

                                <input data-nextfocus=".CashPaid" data-id="expenceTextbox" data-type="ExpenceHead" data-function="GLList" data-glcode="0104" class="autocomplete ExpenceHeadTitle empt input__field input__field--hoshi" type="text" autocomplete="off" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Expence Head</span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                            <span class="input input--hoshi">

                                <input id="txtCode" class="input__field input__field--hoshi ExpenceHeadCode empt BankCode" type="text" autocomplete="off" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Expence Head Code</span>
                                </label>
                            </span>
                        </div>


                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">

                                <input id="txtCashPaid" class="input__field input__field--hoshi CashPaid empt" type="text" autocomplete="off" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Cash</span>
                                </label>
                            </span>
                        </div>


                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <span class="input input--hoshi">

                                <input id="txtNarration" class="input__field input__field--hoshi Narration empt" type="text" autocomplete="off" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Narration</span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">
                                <a id="btnSave" onclick="SaveBefore()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                            </span>
                        </div>
                    </div>

                </div>

            </section>
        </div>
    </div>


    <div class="container mt-3 vouchers">
        <ul class="nav nav-tabs w-100 d-flex p-0">
            <li class="flex-1 mb-0">
                <a data-toggle="tab" href="#CurrentSession" class="active shadow text-center w-100 d-block">
                    <span class="light"></span>
                    Current Session Vouchers
                </a>
            </li>
            <li class="flex-1 mb-0">
                <a data-toggle="tab" href="#Last5Days" class="shadow text-center w-100 d-block">
                    <span class="light"></span>
                    Last 5 Days Vouchers
                </a>
            </li>
            <li class="flex-1 mb-0">
                <a data-toggle="tab" href="#PartyLast5Days" onclick="getPartiesLastVouchers()" class="shadow text-center w-100 d-block">
                    <span class="light"></span>
                    Selected Expence Head Last 15 Days Vouchers
                </a>
            </li>
        </ul>

        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade p-0 show active" id="CurrentSession" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="card sameheight-item items">
                    <ul class="item-list striped">
                        <li class="item item-list-header">
                            <div class="item-row">
                                <div class="item-col item-col-header flex-1">
                                    <div>
                                        <span>Sr #</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Voucher#</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Date</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-2">
                                    <div>
                                        <span>Expence Head</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Cash Paid</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-3">
                                    <div>
                                        <span>Narration</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Edit</span>
                                    </div>
                                </div>
                            </div>
                        </li>

                    </ul>
                    <ul class="item-list contentList striped" style="max-height: 400px; overflow: auto">
                    </ul>
                </div>
            </div>
            <div class="tab-pane fade p-0" id="Last5Days" role="tabpanel" aria-labelledby="nav-profile-tab">
                <div class="card sameheight-item items">
                    <ul class="item-list striped">
                        <li class="item item-list-header">
                            <div class="item-row">
                                <div class="item-col item-col-header flex-1">
                                    <div>
                                        <span>Sr #</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Voucher#</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Date</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-2">
                                    <div>
                                        <span>Expence Head</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Cash Paid</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-3">
                                    <div>
                                        <span>Narration</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Edit</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <ul class="item-list contentList striped" style="max-height: 400px; overflow: auto">
                    </ul>
                </div>
            </div>
            <div class="tab-pane fade p-0" id="PartyLast5Days" role="tabpanel" aria-labelledby="nav-profile-tab">
                <div class="card sameheight-item items">
                    <ul class="item-list striped">
                        <li class="item item-list-header">
                            <div class="item-row">
                                <div class="item-col item-col-header flex-1">
                                    <div>
                                        <span>Sr #</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Voucher#</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Date</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-2">
                                    <div>
                                        <span>Expence Head</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Cash Paid</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-3">
                                    <div>
                                        <span>Narration</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Edit</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <ul class="item-list contentList striped" style="max-height: 400px; overflow: auto">
                    </ul>
                </div>
            </div>
        </div>


    </div>

</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    <script src="../Script/Autocomplete.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="../Script/Dropdown.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>

    <script src="../Script/Voucher/ExpenceCash_Save.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>


    <script type="text/javascript">

        $(document).ready(function () {
            var loggedInStationId = $("#AppStationId").val();
            appendAttribute.init("StationDD", "Station", loggedInStationId);

        });
        
        $(document).on('click', '.PrintBtn', function () {
            var VoucherNo = $(".VoucherNo").val();
           
            window.open(
                '/Transactions/TransactionPrints/ExpenseEntryPrint.aspx?InvNo=' +VoucherNo ,
                '_blank'
            );
        })



        function EditVoucher() {
            var voucherId = $("#txtVoucherNo").val();
            window.location.href = "/Correction/ExpenseEntryEdit.aspx?v=" + voucherId;
        }




    </script>


</asp:Content>
