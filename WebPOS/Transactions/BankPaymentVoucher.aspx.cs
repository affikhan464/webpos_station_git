﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Transactions
{
    public partial class BankPaymentVoucher : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(VoucherViewModel model)
        {
            try
            {
                if (con.State == ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();
                string CompID = "01";
                Module1 objModule1 = new Module1();
                ModGLCode objModGLCode = new ModGLCode();
                Module4 objModule4 = new Module4();
                var VoucherNo = objModule1.MaxBPV();
                var selectedDate = model.VoucherDate;
                var convertedDate = DateTime.ParseExact(selectedDate + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);

                var VoucherDate = convertedDate.ToString("yyyy-MM-dd");

                foreach (var item in model.Rows)
                {
                    var GLCode = item.Code;

                    var Description = item.Title;
                    var ChqNo = item.ChequeNumber;
                    var Narration = item.Narration;
                    var AmountDr = item.Debit;
                    var Narration1 = model.CreditGLTitle + " - " + Narration + " - " + "BPV No.=" + VoucherNo;
                    var CreditGLCode = model.CreditGLCode;
                    var BankName = model.CreditGLTitle;

                    SqlCommand cmdDr = new SqlCommand("insert into GeneralLedger(code,datedr,Description,ChequeNo,Narration,AmountDr,V_NO,V_type,System_Date_Time,BankCode,CompID,Code1) values('" + GLCode + "','" + VoucherDate + "','" + Narration1 + "','" + ChqNo + "','" + Narration + "'," + AmountDr + "," + VoucherNo + ",'" + "BPV" + "','" + convertedDate + "','" + CreditGLCode + "','" + CompID + "','" + CreditGLCode + "')", con, tran);
                    cmdDr.ExecuteNonQuery();
                    SqlCommand cmdCr = new SqlCommand("insert into GeneralLedger(code, datedr, Description, ChequeNo, Narration, AmountCr, V_NO, V_type, System_Date_Time, BankCode, CompID) values('" + CreditGLCode + "', '" + VoucherDate + "', '" + Description + "', '" + ChqNo + "', '" + Description + " - " + Narration + "', " + AmountDr + ", " + VoucherNo + ", '" + "BPV" + "', '" + convertedDate + "', '" + CreditGLCode + "', '" + CompID + "')", con, tran);
                    cmdCr.ExecuteNonQuery();
                    if (GLCode.Substring(0, 8) == "01020101" || GLCode.Substring(0, 8) == "010103")
                    {
                        SqlCommand cmdPart = new SqlCommand("insert into PartiesLedger (Code,DescriptionDr,AmountDr,V_No,V_Type,datedr,System_Date_Time,DescriptionOfBillNo,VenderCode,CompId) values('" + GLCode + "','" + BankName + "-" + Narration + ",BPV.No.=" + VoucherNo + "'," + AmountDr + "," + VoucherNo + ",'" + "BPV" + "','" + VoucherDate + "','" + convertedDate + "','" + "CashPaidToParty" + "','" + CreditGLCode + "','" + CompID + "')", con, tran);
                        cmdPart.ExecuteNonQuery();
                        objModule4.ClosingBalancePartiesNew(GLCode, con, tran);
                    }

                    //SqlCommand cmd3 = new SqlCommand("insert into invoice2 (InvNo,SN0,Code,Description ,Rate,Qty,Amount,Item_Discount,PerDiscount,Pcs,DealRs,CompID,Purchase_Amount)values(" + invoiceNumber + ",'" + SNO + "','" + Code + "','" + Description + "'," + Rate + "," + Quantity + "," + Amount + "," + ItemDiscount + "," + PerDiscount + "," + Quantity + "," + DealRs + ",'" + CompID + "'," + PurchaseAmount + ")", con, tran);
                    //cmd3.ExecuteNonQuery();

                }




                //Save code for JournalVoucher

                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Saved Successfully!", LastVoucherNumber = Convert.ToString(VoucherNo) };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }
    }
}