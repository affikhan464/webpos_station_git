﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using WebPOS.Model;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS.Transactions
{
    public partial class StockMovement : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        protected void Page_Load(object sender, EventArgs e)
        {
            var objModule1 = new Module1();
            var voucherNo = objModule1.MaxStockMovementVoucher() - 1;
            txtInvoiceNo.Text = voucherNo.ToString();
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(List<SaveVoucherViewModel> models, string key)
        {
            var CompID = "01";
            try
            {
                var security = new Security.Security();
                var decryptDateTime = security.DecryptKey(key, true);
                var expireDate = Convert.ToDateTime(decryptDateTime);
                if (expireDate > DateTime.Now)
                {
                    var objModule1 = new Module1();
                    var objModule4 = new Module4();
                    var voucherNo = objModule1.MaxStockMovementVoucher();

                    if (con.State == ConnectionState.Closed) { con.Open(); }
                    tran = con.BeginTransaction();
                    foreach (var model in models)
                    {
                        var ItemCode = Convert.ToInt32(model.Code);
                        var Qty = Convert.ToInt32(model.Qty);
                        var ItemName = model.Name;
                        var FromStationCode = Convert.ToInt32(model.StationIdFrom);
                        var ToStationCode = Convert.ToInt32(model.StationIdTo);

                        var Amount = model.Amount;
                        var Rate = model.Rate;

                        var convertedDate = DateTime.ParseExact(model.Date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                           System.Globalization.CultureInfo.InvariantCulture);

                        var cmdText = "Insert into " +
                            "InventoryMovement (SMONo,Dat,ItemCode,ItemName,QTY,FromStation,ToStation,Narration,FromStationCode,ToStationCode,SNO,Amount,Rate,CompId) " +
                            "Values(" + voucherNo + ",'" + convertedDate + "'," + ItemCode + ",'" + ItemName + "'," + Qty + ",'" + model.StationNameFrom + "','" + model.StationNameTo + "','" + model.Narration + "'," + FromStationCode + "," + ToStationCode + "," + model.SNo + "," + Amount + "," + Rate + ",'" + CompID + "');";

                         cmdText += "insert into inventory (Dat,Icode,Description,Issued,BillNo,DescriptionOfBillNo,System_Date_Time,StationID,CompID,Mutual)  " +
                            " values('" + convertedDate + "'," + ItemCode + ",'" + "Stock Move to " + model.StationNameTo + ",SMO #" + voucherNo + ",Narat:" + model.Narration + " '," + Qty + "," + voucherNo + ",'" + "StockMovement" + "','" + DateTime.Now + "'," + FromStationCode + ",'" + CompID + "'," + 1 + ");";
                      

                         cmdText += "insert into inventory (Dat,Icode,Description,Received,BillNo,DescriptionOfBillNo,System_Date_Time,StationID,CompID,Mutual) " +
                            "values('" + convertedDate + "'," + ItemCode + ",'" + "Stock Move From " + model.StationNameFrom + ",SMO #" + voucherNo + ",Narat:" + model.Narration + " '," + Qty + "," + voucherNo + ",'" + "StockMovement" + "','" + DateTime.Now + "'," + ToStationCode + ",'" + CompID + "'," + 1 + ");";

                        cmdText += "insert into inventory (Dat,Icode,Description,Received,BillNo,DescriptionOfBillNo,System_Date_Time,StationID,CompID,Mutual) " +
                            "values('" + convertedDate + "'," + ItemCode + ",'" + "Stock Move From " + model.StationNameFrom + ",SMO #" + voucherNo + ",Narat:" + model.Narration + " '," + Qty + "," + voucherNo + ",'" + "StockMovement" + "','" + DateTime.Now + "'," + ToStationCode + ",'" + CompID + "'," + 1 + ");";

                        cmdText += "insert into PrintingTable(Text_1,Text_2,Text_3) values('" + voucherNo + "','" + model.StationNameFrom + "','" + model.StationNameTo + "');";

                        var cmd = new SqlCommand(cmdText, con, tran);
                        cmd.ExecuteNonQuery();

                        objModule4.ClosingBalanceNewTechniqueStationIssue(ItemCode, Qty, FromStationCode, tran, con);
                        objModule4.ClosingBalanceNewTechniqueStationReceived(ItemCode, Qty, ToStationCode, tran, con);

                    }

                    tran.Commit();
                    con.Close();
                    return new BaseModel() { Success = true, Message = "Saved Successfully!!", LastInvoiceNumber = voucherNo.ToString() };
                }
                else
                {
                    return new BaseModel() { Success = false };

                }
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
            finally
            {
                con.Close();
            }
        }



    }

}