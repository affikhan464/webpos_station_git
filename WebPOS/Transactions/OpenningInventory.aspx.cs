﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using WebPOS.Model;

namespace WebPOS
{
    public partial class OpenningInventory : System.Web.UI.Page
    {
        Int32 OperatorID = 0;
        static string CompID = "01";


        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static Module7 objModule7 = new Module7();
        static ModItem objModItem = new ModItem();
        static Module1 objModule1 = new Module1();
        static ModViewInventory objModViewInventory = new ModViewInventory();
        static ModQtyOfInventory objModQtyOfInventory = new ModQtyOfInventory();
        static Module4 objModule4 = new Module4();
        static ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
        
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(OpeningInventoryViewModel model)
        {
            decimal OpUnits = 0;
            decimal Cost = 0;
            decimal TotValue = 0;
            decimal ItemCode = 0;
            decimal SellingPrice = 0;

            SqlTransaction tran;
            if (con.State==ConnectionState.Closed) { con.Open(); }
            tran = con.BeginTransaction();
            try
            {
                var stationId = HttpContext.Current.Session["StationId"].ToString();
                OpUnits = model.OpeningUnits;
                Cost = model.PurchasingCost;
                TotValue = model.TotalValue;
                ItemCode = model.ItemCode;
                SellingPrice = model.SellingCost;
                var startDate = objModule7.StartDate();

                var cmdText = "update InventoryOpeningBalance set Opening='" + OpUnits + "',Cost_Per_Unit=" + Cost + ",Total_Value='" + TotValue + "' where CompID='" + CompID + "' and StationId=" + stationId + " and ICode=" + ItemCode + " and  Dat='" + startDate + "';";
                cmdText += "update InvCode set Ave_Cost=" + Cost + ",qty=" + OpUnits + ", SellingCost=" + SellingPrice + ", PurchasingCost=" + Cost + ",IsOpBalSet=1  where CompID='" + CompID + "' and  Code=" + ItemCode+";";
               
                SqlCommand cmd = new SqlCommand(cmdText, con, tran);
                cmd.ExecuteNonQuery();

                var objModule4 = new Module4();
                var modqty = new ModQtyOfInventory();

                modqty.QtyAvailableOne(ItemCode.ToString(), tran, con);
                modqty.QtyAvailableOneStation(ItemCode.ToString(), stationId, tran, con);

                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Saved Successfully." };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }


        }



    }
}