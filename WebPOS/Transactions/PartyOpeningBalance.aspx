﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="PartyOpeningBalance.aspx.cs" Inherits="WebPOS.PartyOpeningBalance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



    <div class="container">
        <div class="mt-4">

            <section class="form">
                <h2 class="form-header fa-user-alt">Party Opening Balance</h2>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xl-offset-6 col-lg-offset-6">
                        <span class="input input--hoshi">
                            <input id="txtStartYear" class="StartYear input__field input__field--hoshi"  autocomplete="off" type="text" maxlength="4" />

                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Start Year</span>
                            </label>
                        </span>
                    </div>

                     <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                       
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtPartyName" data-type="Party" data-id="txtPartyName" data-nextfocus=".OpeningBalance" data-function="GetParty" name="PartyName" class=" PartyBox PartyName autocomplete empt input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Party Name</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtPartyCode" disabled="disabled" name="PartyCode" class="input__field input__field--hoshi PartyCode empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Party Code</span>
                            </label>
                        </span>
                    </div>

                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtOpeningBalance" class="input__field input__field--hoshi OpeningBalance empt" type="number" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Openning Balance</span>
                            </label>
                        </span>
                    </div>


                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <span class="input input--hoshi">

                            <input id="txtNarration" class="input__field input__field--hoshi Narration empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Narration</span>
                            </label>
                        </span>
                    </div>
                </div>


                <hr />

                <div class="row  justify-content-end">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="SaveBefore()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                        </span>
                    </div>
                </div>

            </section>
        </div>
    </div>
</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    <script src="/Script/Autocomplete.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script> 
    <script src="/Script/Voucher/PartyOpeningBalance_Save.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>

    <script>
        function GetPartyOpeningBalance() {

            $.ajax({
                url: '/WebPOSService.asmx/GetPartyOpeningBalance',
                type: "Post",

                data: { "PartyCode": $(".PartyCode").val()},
                success: function (PartyOpBalance) {
                    $(".OpeningBalance").val(PartyOpBalance)
                },
                fail: function (jqXhr, exception) {

                }
            });
        }
    </script>
</asp:Content>
