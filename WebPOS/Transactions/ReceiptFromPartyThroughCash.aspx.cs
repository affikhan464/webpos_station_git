﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.ModelBinding;
using System.Web.Services;
using System.Web.Script.Services;
using WebPOS.Model;


namespace WebPOS
{
    public partial class ReceiptFromPartyThroughCash : System.Web.UI.Page
    {
        Int32 OperatorID = 0;
        static string CompID = "01";
        string UserSession = "001";

        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        static string CashAccountGLCode = "0101010100001";






        protected void Page_Load(object sender, EventArgs e)
        {
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(ModelPaymentVoucher ModelPaymentVoucher)
        {

            Module1 objModule1 = new Module1();
            Module4 objModule4 = new Module4();

            try
            {



                var stationId = string.Empty;
                if (HttpContext.Current.Session["StationId"] != null)
                {
                    stationId = HttpContext.Current.Session["StationId"].ToString();
                }
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                string SecondDescription = "FromParties";
                var VoucherNo = Convert.ToString(objModule1.MaxCRV(con, tran));
                var VoucherDate = DateTime.ParseExact(ModelPaymentVoucher.Date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture).ToShortDateString();
                string PartyCode = ModelPaymentVoucher.PartyCode;
                string PartyName = ModelPaymentVoucher.PartyName;
                var SysTime = VoucherDate + " " + DateTime.Now.ToString("HH:mm:ss");

                decimal CashPaid = Convert.ToDecimal(ModelPaymentVoucher.CashPaid);
                string Narration = ModelPaymentVoucher.Narration;


                //SqlCommand cmd1 = new SqlCommand("insert into GeneralLedger (Code,Description,AmountDr,V_No,V_Type,datedr,Code1,System_Date_Time,SecondDescription,VenderCode,Narration,CompId, StationId) values('" + PartyCode + "','" + "Cash Account" + "'," + CashPaid + "," + Convert.ToDecimal(VoucherNo) + ",'" + "CPV" + "','" + Convert.ToDateTime(VoucherDate) + "','" + CashAccountGLCode + "','" + SysTime + "','" + SecondDescription + "','" + PartyCode + "','" + Narration + "','" + CompID + "')", con, tran);
                SqlCommand cmd1 = new SqlCommand("insert into GeneralLedger (Code,Description,AmountDr,V_No,V_Type,datedr,Code1,System_Date_Time,SecondDescription,VenderCode,Narration,CompId, StationId) values('" + CashAccountGLCode + "','" + PartyName + "-" + Narration + "'," + CashPaid + "," + Convert.ToDecimal(VoucherNo) + ",'" + "CRV" + "','" + Convert.ToDateTime(VoucherDate) + "','" + PartyCode + "','" + SysTime + "','" + SecondDescription + "','" + PartyCode + "','" + Narration + "','" + CompID + "'," + stationId + ")", con, tran);

                cmd1.ExecuteNonQuery();
                //SqlCommand cmd2 = new SqlCommand("insert into GeneralLedger (Code,Description,AmountCr,V_No,V_Type,datedr,Narration,Code1,System_Date_Time,SecondDescription,VenderCode,CompId, StationId) values('" + CashAccountGLCode + "','" + PartyName + "-" + Narration + "'," + CashPaid + "," + Convert.ToDecimal(VoucherNo) + ",'" + "CPV" + "','" + VoucherDate + "','" + Narration + "','" + PartyCode + "','" + SysTime + "','" + SecondDescription + "','" + PartyCode + "','" + CompID + "')", con, tran);
                SqlCommand cmd2 = new SqlCommand("insert into GeneralLedger (Code,Description,AmountCr,V_No,V_Type,datedr,Narration,Code1,System_Date_Time,SecondDescription,VenderCode,CompId, StationId) values('" + PartyCode + "','" + "Cash Book (" + PartyName + ")" + "'," + CashPaid + "," + Convert.ToDecimal(VoucherNo) + ",'" + "CRV" + "','" + VoucherDate + "','" + Narration + "','" + CashAccountGLCode + "','" + SysTime + "','" + SecondDescription + "','" + PartyCode + "','" + CompID + "'," + stationId + ")", con, tran);
                cmd2.ExecuteNonQuery();
                //SqlCommand cmd3 = new SqlCommand("insert into PartiesLedger (Code,DescriptionDr,AmountDr,V_No,V_Type,datedr,System_Date_Time,DescriptionOfBillNo,BillNo,CompId, StationId) values('" + PartyCode + "','" + "Cash Payment-" + VoucherNo + "-" + Narration + "'," + CashPaid + "," + Convert.ToDecimal(VoucherNo) + ",'" + "CPV" + "','" + VoucherDate + "','" + SysTime + "','" + SecondDescription + "','" + VoucherNo  + "','" + CompID + "')", con, tran);
                SqlCommand cmd3 = new SqlCommand("insert into PartiesLedger (Code,DescriptionDr,AmountCr,V_No,V_Type,datedr,System_Date_Time,DescriptionOfBillNo,BillNo,CompId, StationId) values('" + PartyCode + "','" + "Cash Receipt-" + VoucherNo + "-" + Narration + "'," + CashPaid + "," + Convert.ToDecimal(VoucherNo) + ",'" + "CRV" + "','" + VoucherDate + "','" + SysTime + "','" + SecondDescription + "','" + VoucherNo + "','" + CompID + "'," + stationId + ")", con, tran);
                cmd3.ExecuteNonQuery();
                SqlCommand cmd4 = new SqlCommand("Insert into CashReceived (VoucherNo,CompID,Date1,PartyCode,PartyName,Amount,Narration,System_Date_Time) values(" + Convert.ToDecimal(VoucherNo) + ",'" + CompID + "','" + Convert.ToDateTime(VoucherDate) + "','" + PartyCode + "','" + PartyName + "'," + CashPaid + ",'" + Narration + "','" + SysTime + "')", con, tran);
                cmd4.ExecuteNonQuery();




                objModule4.ClosingBalancePartiesNew(PartyCode, con, tran);
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Cash Receipt Voucher Saved Successfully.", LastInvoiceNumber = VoucherNo.ToString() };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel GetLastVouchers()
        {
            
            try
            {



                var stationId = string.Empty;
                if (HttpContext.Current.Session["StationId"] != null)
                {
                    stationId = HttpContext.Current.Session["StationId"].ToString();
                }
                if (con.State == ConnectionState.Closed) { con.Open(); }
               
                var cmd = new SqlCommand(
                    "Select " +
                    "AmountCr," +
                    "V_No," +
                    "datedr," +
                    "Narration, " +
                    "PartyCode.Name as PartyName " +
                    "from GeneralLedger " +
                    " join PartyCode on PartyCode.Code=GeneralLedger.VenderCode" +
                    " where  PartyCode.CompID='" + CompID + "' and " +
                    "StationId='" + stationId + "' and " +
                    "V_Type='CRV' and " +
                    "SecondDescription='FromParties' and " +
                    "AmountCr > 0 and " +
                    " datedr>='"+DateTime.Now.AddDays(-15).ToString("MM/dd/yyyy")+ "' and datedr<='" + DateTime.Now.ToString("MM/dd/yyyy") + "' " +
                    " Order by datedr asc", con);
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];



                var reports = new List<ReportViewModel>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string CashPaid = data.Rows[i]["AmountCr"].ToString();
                    string VoucherNumber = data.Rows[i]["V_No"].ToString();
                    string Date = Convert.ToDateTime( data.Rows[i]["datedr"]).ToString("dd/MM/yyyy");
                    string Narration = data.Rows[i]["Narration"].ToString();
                    string PartyName = data.Rows[i]["PartyName"].ToString();    
                    var party = new ReportViewModel()
                    {
                       Date = Date,
                       PartyName=PartyName,
                       CashPaid= CashPaid,
                       Narration= Narration,
                       VoucherNumber= VoucherNumber

                    };
                    reports.Add(party);
                }

              
                con.Close();
                return new BaseModel() { Success = true, Reports= reports };
            }
            catch (Exception ex)
            {
               
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel GetPartyLastVouchers(string partyCode)
        {
            
            try
            {
                var stationId = string.Empty;
                if (HttpContext.Current.Session["StationId"] != null)
                {
                    stationId = HttpContext.Current.Session["StationId"].ToString();
                }
                if (con.State == ConnectionState.Closed) { con.Open(); }
               
                var cmd = new SqlCommand(
                    "Select " +
                    "AmountCr," +
                    "V_No," +
                    "datedr," +
                    "Narration, " +
                    "PartyCode.Name as PartyName " +
                    "from GeneralLedger " +
                    " join PartyCode on PartyCode.Code=GeneralLedger.VenderCode" +
                    " where  PartyCode.CompID='" + CompID + "' and " +
                    "StationId='" + stationId + "' and " +
                    "PartyCode.Code='" + partyCode + "' and " +
                    "V_Type='CRV' and " +
                    "SecondDescription='FromParties' and " +
                    "AmountCr > 0 and " +
                    " datedr>='"+DateTime.Now.AddDays(-15).ToString("MM/dd/yyyy")+ "' and datedr<='" + DateTime.Now.ToString("MM/dd/yyyy") + "' " +
                    " Order by datedr asc", con);
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];



                var reports = new List<ReportViewModel>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string CashPaid = data.Rows[i]["AmountCr"].ToString();
                    string VoucherNumber = data.Rows[i]["V_No"].ToString();
                    string Date = Convert.ToDateTime( data.Rows[i]["datedr"]).ToString("dd/MM/yyyy");
                    string Narration = data.Rows[i]["Narration"].ToString();
                    string PartyName = data.Rows[i]["PartyName"].ToString();    
                    var party = new ReportViewModel()
                    {
                       Date = Date,
                       PartyName=PartyName,
                       CashPaid= CashPaid,
                       Narration= Narration,
                       VoucherNumber= VoucherNumber

                    };
                    reports.Add(party);
                }

              
                con.Close();
                return new BaseModel() { Success = true, Reports= reports };
            }
            catch (Exception ex)
            {
               
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

    }
}
