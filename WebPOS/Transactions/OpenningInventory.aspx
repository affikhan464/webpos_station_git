﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="OpenningInventory.aspx.cs" Inherits="WebPOS.OpenningInventory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
  
    <div class="container">
        <div class="mt-4">

            <section class="form">
                <h2 class="form-header fa-user-alt">Openning Inventory</h2>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6 col-xl-offset-6 col-lg-offset-6">
                        <span class="input input--hoshi">
                            <input id="txtStartYear" class="StartYear input__field input__field--hoshi" autocomplete="off" type="text" maxlength="4" />

                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Start Year</span>
                            </label>
                        </span>
                    </div>

                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <input id="SearchBox" data-id="itemTextbox" data-nextfocus=".OpeningUnits" data-type="Item" data-function="GetRecords" class="clientInput SearchBox ItemDescription autocomplete emt input__field input__field--hoshi" type="text" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Item Name</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtItemCode" disabled="disabled" name="ItemCode" class="input__field input__field--hoshi ItemCode empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Item Code</span>
                            </label>
                        </span>
                    </div>

                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtOpeningUnits" class="input__field input__field--hoshi OpeningUnits empt" type="number" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Opening Units</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtRequiredClosingUnits" class="input__field input__field--hoshi RequiredClosingUnits empt" type="number" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Closing Units if Required</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtUnitsReceived" class="input__field input__field--hoshi ReceivedUnits empt" type="number"  autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Received Units</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtUnitsIssued" class="input__field input__field--hoshi IssuedUnits empt" type="number" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Issued Units</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtClosingUnits" class="input__field input__field--hoshi ClosingUnits empt" type="number"  autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Closing Units</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtCost" class="input__field input__field--hoshi PurchasingCost empt" type="number"  autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Purchasing Cost</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtSellingPrice" class="input__field input__field--hoshi SellingCost empt" type="number"  autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Selling Cost</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtTotalValue" class="input__field input__field--hoshi TotalValue empt" type="number"  autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Total Value</span>
                            </label>
                        </span>
                    </div>
                </div>


                <hr />

                <div class="row  justify-content-end">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="SaveBefore()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                        </span>
                    </div>
                </div>

            </section>
        </div>
    </div>
</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    <script src="/Script/Autocomplete.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="/Script/Voucher/OpeningInventory_Save.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>

    <script>

        function GetInventoryOpening() {

            $.ajax({
                url: '/WebPOSService.asmx/GetInventoryOpening',
                type: "Post",

                data: { "ItemCode": $(".ItemCode").val() },
                success: function (response) {
                    for (var i = 0; i < Object.values(response).length; i++) {
                        if (!Object.keys(response)[i]=="ItemCode") {
                            $("." + Object.keys(response)[i]).val(Object.values(response)[i])
                        }
                    }
                    updateInputStyle();

                },
                fail: function (jqXhr, exception) {

                }
            });
        }

    </script>
</asp:Content>
