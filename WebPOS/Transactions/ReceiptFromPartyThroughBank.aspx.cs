﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using WebPOS.Model;

namespace WebPOS
{
    public partial class ReceiptFromPartyThroughBank : System.Web.UI.Page
    {
        static string CompID = "01";
        static string CashAccountGLCode = "0101010100001";

        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
        Module4 objModule4 = new Module4();
        Module1 objModule1 = new Module1();
        ModGLCode objModGLCode = new ModGLCode();
        ModGL objModGL = new ModGL();
        static SqlTransaction tran;

        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(ModelPaymentVoucher ModelPaymentVoucher)
        {

            Module1 objModule1 = new Module1();
            Module4 objModule4 = new Module4();

            try
            {



                var stationId = string.Empty;
                if (HttpContext.Current.Session["StationId"] != null)
                {
                    stationId = HttpContext.Current.Session["StationId"].ToString();
                }
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                string SecondDescription = "FromPartiesThroughBank";
                var VoucherNo = Convert.ToString(objModule1.MaxBRV());

                var VoucherDate = DateTime.ParseExact(ModelPaymentVoucher.Date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture).ToShortDateString();

                string PartyCode = ModelPaymentVoucher.PartyCode;
                string PartyName = ModelPaymentVoucher.PartyName;
                string BankCode = ModelPaymentVoucher.BankCode;
                string BankName = ModelPaymentVoucher.BankName;
                string ChqNo = ModelPaymentVoucher.ChqNo;

                var SysTime = VoucherDate + " " + DateTime.Now.ToString("HH:mm:ss");

                decimal CashPaid = Convert.ToDecimal(ModelPaymentVoucher.CashPaid);
                string Narration = ModelPaymentVoucher.Narration;



                SqlCommand cmd1 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,ChequeNo,Narration,AmountDr,V_NO,V_type,System_Date_Time,BankCode,SecondDescription,VenderCode,CompId, StationId) values('" + BankCode + "','" + VoucherDate + "','" + Narration + "-" + PartyName + "','" + ChqNo + "','" + Narration + "'," + Convert.ToDecimal(CashPaid) + "," + VoucherNo + ",'" + "BRV" + "','" + SysTime + "','" + BankCode + "','" + SecondDescription + "','" + PartyCode + "','" + CompID + "'," + stationId + ")", con, tran);
                cmd1.ExecuteNonQuery();

                SqlCommand cmd2 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,ChequeNo,Narration,AmountCr,V_NO,V_type,System_Date_Time,BankCode,SecondDescription,VenderCode,CompId, StationId) values('" + PartyCode + "','" + VoucherDate + "','" + Narration + "','" + ChqNo + "','" + Narration + "'," + Convert.ToDecimal(CashPaid) + "," + VoucherNo + ",'" + "BRV" + "','" + SysTime + "','" + BankCode + "','" + SecondDescription + "','" + PartyCode + "','" + CompID + "'," + stationId + ")", con, tran);
                cmd2.ExecuteNonQuery();

                SqlCommand cmd3 = new SqlCommand("insert into PartiesLedger (Code,DescriptionDr,AmountCr,V_No,V_Type,datedr,System_Date_Time,DescriptionOfBillNo,VenderCode,CompId, StationId) values('" + PartyCode + "','" + Narration + ",BRV.No.=" + VoucherNo + "(" + BankName + ")" + "'," + Convert.ToDecimal(CashPaid) + "," + VoucherNo + ",'" + "BRV" + "','" + VoucherDate + "','" + SysTime + "','" + SecondDescription + "','" + BankCode + "','" + CompID + "'," + stationId + ")", con, tran);
                cmd3.ExecuteNonQuery();

                objModule4.ClosingBalancePartiesNew(PartyCode, con, tran);
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Receipt From Party Through Bank Saved Successfully.", LastInvoiceNumber = VoucherNo.ToString() };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }



        private void SaveVoucher()
        {

            //SqlTransaction tran;
            //if (con.State==ConnectionState.Closed) { con.Open(); }
            //tran = con.BeginTransaction();
            //string PartyCode = "";
            //PartyCode = objModPartyCodeAgainstName.PartyCode( txtPartyName.Text);
            //try
            //{
            //    txtVoucherNo.Text = Convert.ToString(objModule1.MaxBPV());

            //    string SysTime = Convert.ToString(DateTime.Now);
            //    decimal CashPaid = 0;
            //    if (txtCashPaid.Text != "") { CashPaid = Convert.ToDecimal(txtCashPaid.Text); }
            //    SqlCommand cmd1 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,ChequeNo,Narration,AmountDr,V_NO,V_type,System_Date_Time,BankCode,SecondDescription,VenderCode,CompId, StationId) values('" + PartyCode + "','" + Convert.ToDateTime(txtCurrentDate.Text) + "','" + txtNarration.Text + "','" + txtChequeNo.Text + "','" + txtNarration.Text + "'," + Convert.ToDecimal(txtCashPaid.Text) + "," + Convert.ToDecimal(txtVoucherNo.Text) + ",'" + "BPV" + "','" + SysTime + "','" + txtBankCode.Text + "','" + "CashPaidToPartyThroughBank" + "','" + PartyCode + "','" + CompID + "')", con, tran);

            //    cmd1.ExecuteNonQuery();
            //    SqlCommand cmd2 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,ChequeNo,Narration,AmountCr,V_NO,V_type,System_Date_Time,BankCode,SecondDescription,VenderCode,CompId, StationId) values('" + txtBankCode.Text + "','" + Convert.ToDateTime(txtCurrentDate.Text) + "','" + txtNarration.Text + "','" + txtChequeNo.Text + "','" + txtNarration.Text + "'," + Convert.ToDecimal(txtCashPaid.Text ) + "," + Convert.ToDecimal( txtVoucherNo.Text) + ",'" + "BPV" + "','" + SysTime + "','" + txtBankCode.Text + "','" + "CashPaidToPartyThroughBank" + "','" + PartyCode  + "','" + CompID + "')", con, tran);
            //    cmd2.ExecuteNonQuery();
            //    SqlCommand cmd3 = new SqlCommand("insert into PartiesLedger (Code,DescriptionDr,AmountDr,V_No,V_Type,datedr,System_Date_Time,DescriptionOfBillNo,VenderCode,CompId, StationId) values('" + PartyCode + "','" + txtNarration.Text + ",BPV.No.=" + txtVoucherNo.Text + "(" + lstBank.Text + ")" + "'," + Convert.ToDecimal(txtCashPaid.Text) + "," + txtVoucherNo.Text + ",'" + "BPV" + "','"  + Convert.ToDateTime(txtCurrentDate.Text ) + "','" + SysTime + "','" + "CashPaidToPartyThroughBank" + "','" + txtBankCode.Text + "','" + CompID + "')", con, tran);
            //    cmd3.ExecuteNonQuery();

            //    txtCashPaid.Text = "";
            //    txtNarration.Text = "";
            //    txtPartyCode.Text = "";
            //    txtBalance.Text = "";
            //    txtAddress.Text = "";
            //    txtAccountNo.Text = "";
            //    txtBankCode.Text = "";
            //    txtChequeNo.Text = "";

            //    tran.Commit();
            //    con.Close();
            //}
            //catch (Exception ex)
            //{
            //    tran.Rollback();
            //    Response.Write(ex.Message);
            //}
            //objModule4.ClosingBalanceParties(PartyCode);

            //con.Close();

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel GetLastVouchers()
        {

            try
            {



                var stationId = string.Empty;
                if (HttpContext.Current.Session["StationId"] != null)
                {
                    stationId = HttpContext.Current.Session["StationId"].ToString();
                }
                if (con.State == ConnectionState.Closed) { con.Open(); }

                var cmd = new SqlCommand(
                    "Select " +
                    "AmountCr," +
                    "V_No," +
                    "datedr," +
                    "Narration, " +
                    "PartyCode.Name as PartyName, " +
                    "GLCode.Title as BankName " +
                    "from GeneralLedger " +
                    " join PartyCode on PartyCode.Code=GeneralLedger.VenderCode" +
                    " join GLCode on GLCode.Code=GeneralLedger.bankcode" +
                    " where  PartyCode.CompID='" + CompID + "' and " +
                    "StationId='" + stationId + "' and " +
                    "V_Type='BRV' and " +
                    "SecondDescription='FromPartiesThroughBank' and " +
                    "AmountCr > 0 and " +
                    " datedr>='" + DateTime.Now.AddDays(-15).ToString("MM/dd/yyyy") + "' and datedr<='" + DateTime.Now.ToString("MM/dd/yyyy") + "' " +
                    " Order by datedr asc", con);
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];



                var reports = new List<ReportViewModel>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string CashPaid = data.Rows[i]["AmountCr"].ToString();
                    string VoucherNumber = data.Rows[i]["V_No"].ToString();
                    string Date = Convert.ToDateTime(data.Rows[i]["datedr"]).ToString("dd/MM/yyyy");
                    string Narration = data.Rows[i]["Narration"].ToString();
                    string PartyName = data.Rows[i]["PartyName"].ToString();
                    string BankName = data.Rows[i]["BankName"].ToString();
                    var party = new ReportViewModel()
                    {
                        Date = Date,
                        PartyName = PartyName,
                        BankName = BankName,
                        CashPaid = CashPaid,
                        Narration = Narration,
                        VoucherNumber = VoucherNumber

                    };

                    reports.Add(party);
                }


                con.Close();
                return new BaseModel() { Success = true, Reports = reports };
            }
            catch (Exception ex)
            {

                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel GetPartyLastVouchers(string partyCode)
        {

            try
            {
                var stationId = string.Empty;
                if (HttpContext.Current.Session["StationId"] != null)
                {
                    stationId = HttpContext.Current.Session["StationId"].ToString();
                }
                if (con.State == ConnectionState.Closed) { con.Open(); }

                var cmd = new SqlCommand(
                    "Select " +
                    "AmountCr," +
                    "V_No," +
                    "datedr," +
                    "Narration, " +
                    "PartyCode.Name as PartyName, " +
                    "GLCode.Title as BankName " +
                    "from GeneralLedger " +
                    " join PartyCode on PartyCode.Code=GeneralLedger.VenderCode" +
                    " join GLCode on GLCode.Code=GeneralLedger.bankcode" +
                    " where  PartyCode.CompID='" + CompID + "' and " +
                    "StationId='" + stationId + "' and " +
                    "PartyCode.Code='" + partyCode + "' and " +
                    "V_Type='BRV' and " +
                    "SecondDescription='FromPartiesThroughBank' and " +
                    "AmountCr > 0 and " +
                    " datedr>='" + DateTime.Now.AddDays(-15).ToString("MM/dd/yyyy") + "' and datedr<='" + DateTime.Now.ToString("MM/dd/yyyy") + "' " +
                    " Order by datedr asc", con);
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];



                var reports = new List<ReportViewModel>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string CashPaid = data.Rows[i]["AmountCr"].ToString();
                    string VoucherNumber = data.Rows[i]["V_No"].ToString();
                    string Date = Convert.ToDateTime(data.Rows[i]["datedr"]).ToString("dd/MM/yyyy");
                    string Narration = data.Rows[i]["Narration"].ToString();
                    string PartyName = data.Rows[i]["PartyName"].ToString();
                    string BankName = data.Rows[i]["BankName"].ToString();
                    var party = new ReportViewModel()
                    {
                        Date = Date,
                        PartyName = PartyName,
                        BankName = BankName,
                        CashPaid = CashPaid,
                        Narration = Narration,
                        VoucherNumber = VoucherNumber

                    };
                    reports.Add(party);
                }


                con.Close();
                return new BaseModel() { Success = true, Reports = reports };
            }
            catch (Exception ex)
            {

                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

    }
}
