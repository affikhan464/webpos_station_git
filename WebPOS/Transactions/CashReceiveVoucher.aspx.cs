﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Transactions
{
    public partial class CashReceiveVoucher : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(VoucherViewModel model)
        {
            try
            {
                if (con.State == ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();
                Module1 objModule1 = new Module1();
                ModGLCode objModGLCode = new ModGLCode();
                Module4 objModule4 = new Module4();
                var VoucherNo = objModule1.MaxCRV(con,tran);
                var CompID = "01";

                var selectedDate = model.VoucherDate;
                var SysTime = DateTime.ParseExact(selectedDate + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);

                var VoucherDate = SysTime.ToString("yyyy-MM-dd");
                var DebitGLCode = model.CreditGLCode;
                var CashBookTitle = model.CreditGLTitle;
                var CashBookNarration = model.CreditNarration;
                var CashBookTotalRs = model.CreditAmount;

                foreach (var item in model.Rows)
                {

                    var GLCode = item.Code;
                    var Description = item.Title;
                    var Narration = item.Narration;
                    var AmountCr = item.Debit;
                    var Desc = model.CreditGLTitle + " - " + Narration;


                    SqlCommand cmdDr = new SqlCommand("insert into GeneralLedger (code,datedr,Description,Narration,AmountDr,V_NO,V_type,System_Date_Time,Code1,CompID) values('" + DebitGLCode + "','" + VoucherDate + "','" + Description + "','" + CashBookNarration + "'," + AmountCr + "," + VoucherNo + ",'" + "CRV" + " ','" + SysTime + "','" + DebitGLCode + "','" + CompID + "')", con, tran);
                    cmdDr.ExecuteNonQuery();


                    SqlCommand cmdCr = new SqlCommand("insert into GeneralLedger (code,datedr,Description,Narration,AmountCr,V_NO,V_type,System_Date_Time,Code1,CompID) " +
                        "values('" + GLCode + "','" + VoucherDate + "','" + Desc + "','" + Narration + "'," + AmountCr + "," + VoucherNo + ",'" + "CRV" + "','" + SysTime + "','" + DebitGLCode + "','" + CompID + "')", con, tran);
                    cmdCr.ExecuteNonQuery();

                    if (GLCode.Substring(0, 8) == "01020101" || GLCode.Substring(0, 8) == "01010103")
                    {
                        SqlCommand cmdPart = new SqlCommand("insert into PartiesLedger (Code,DescriptionDr,AmountCr,V_No,V_Type,datedr,System_Date_Time,DescriptionOfBillNo,CompID) values('" + GLCode + "','" + Narration + "'," + AmountCr + "," + VoucherNo + ",'" + "CRV" + "','" + VoucherDate + "','" + SysTime + "','" + "FromParties" + "','" + CompID + "')", con, tran);
                        cmdPart.ExecuteNonQuery();
                        objModule4.ClosingBalancePartiesNew(GLCode, con, tran);
                    }





                }

                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Saved Successfully!", LastVoucherNumber = Convert.ToString(VoucherNo) };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }
    }
}