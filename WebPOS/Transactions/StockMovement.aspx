﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="StockMovement.aspx.cs" Inherits="WebPOS.Transactions.StockMovement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container-fluid mb-3 header-inputs">
        <div class="mt-4">

            <section class="form">
                <h2 class="form-header ">Stock Movement</h2>
                <div class="row ">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 d-flex">
                        <span class="input input--hoshi">
                            <asp:TextBox ID="txtInvoiceNo" Enabled="false" CssClass="InvoiceNumber empty1 input__field input__field--hoshi" name="Invoice" runat="server"></asp:TextBox>
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Bill #</span>
                            </label>
                        </span>
                        <span data-page="222" class="input input--hoshi w-25">
                            <a class="btn btn-3 btn-bm btn-3e w-100" id="EditBtn" data-toggle="tooltip" title="Edit"><i class="fas fa-edit text-white"></i></a>
                        </span>
                        <span data-page="461" class="input input--hoshi w-25">
                            <a class="btn btn-3 btn-bm btn-3e w-100"  href="/Correction/StockMovementPrevious.aspx" data-toggle="tooltip" title="Previous Stock Movements"><i class="fas fa-cubes text-white"></i></a>
                        </span>
                        <span class="input input--hoshi w-25">
                            <a class="btn btn-3 btn-bm btn-3e w-100" id="PrintBtn" data-toggle="tooltip" title="Print"><i class="fas fa-print text-white"></i></a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                        <div class="">                           
                            <label class="">
                                <span class="">From </span>
                            </label>
                             <select id="MoveFromDD" class="round input__field--hoshi">
                            </select>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                        <div class="">
                            <label class="">
                                <span class="">To </span>
                            </label>
                            <select id="MoveToDD" class="round input__field--hoshi">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <span class="input input--hoshi">
                            <input class="datetimepicker Date input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Date</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-10 col-sm-10 col-md-4 col-lg-4 col-xl-4">
                        <span class="input input--hoshi">
                            <input class="BC empty1 input__field input__field--hoshi" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">BC</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-2 col-sm-2 col-md-3 col-lg-2 col-xl-2">
                        <span class="input input--hoshi checkbox--hoshi">
                            <label>
                                <input class="checkbox" id="chkBox" type="checkbox" />
                                <span></span>

                            </label>
                        </span>
                    </div>
                </div>
                <div class="row">

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <span class="input input--hoshi">
                            <input class="DR empty1 input__field input__field--hoshi" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">DR</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <span class="input input--hoshi">
                            <input id="SearchBox" data-id="itemTextbox" data-type="Item" autocomplete="off" data-function="GetItemsStockMovement" class=" input__field input__field--hoshi autocomplete" type="text" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi"><i class="fas fa-search"></i>Search Item </span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <span class="input input--hoshi">
                            <input class="Narration empty1 input__field input__field--hoshi" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Narration</span>
                            </label>
                        </span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a href="/Transactions/StockMovement.aspx" class="btn btn-3 btn-bm btn-3e w-100 fa-sync">Reset </a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="save()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                        </span>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="container-fluid">
        <div class="card sameheight-item items">

            <ul class="item-list striped">
                <li class="item item-list-header">
                    <div class="item-row">
                        <div class="item-col item-col-header flex-1">
                            <div>
                                <span>Sr #</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header  flex-2">
                            <div>
                                <span>Code</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header  flex-4">
                            <div>
                                <span>Description</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header  flex-1">
                            <div>
                                <span>Qty</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header  flex-2">
                            <div>
                                <span>Rate</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header  flex-2">
                            <div>
                                <span>Amount</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header  flex-3">
                        </div>
                    </div>
                </li>

            </ul>
            <ul class="item-list contentList striped" style="min-height: 200px; overflow: auto">
            </ul>
        </div>

    </div>

</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    <script src="/Script/Autocomplete.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>

    <script src="../Script/Dropdown.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>

    <script src="../Script/StockMovement/StockMovement.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script>
        $(document).ready(function () {
            resizeContentList();
        });
        function resizeContentList() {
            var headerHeight = $(".header-inputs").height() + $(".header").height() + 120;
            $(".contentList").css("height", "calc(100vh - " + headerHeight + "px)")
        }

        $(document).on('keydown', function (e) {
            if ((e.ctrlKey || e.metaKey) && (e.key == "p" || e.charCode == 16 || e.charCode == 112 || e.keyCode == 80)) { //Ctrl + P
                e.preventDefault();
                printStockMovement();
            }
        });
    </script>
       

</asp:Content>
