﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Transactions.TransactionPrints
{
    public partial class SaleNewItems_ThermalPrint : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        { }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel GetReport(string invno)
        {
            if (HttpContext.Current.Session["UserId"] == null)
            {
                return new BaseModel() { Success = false, LoginAgain = true };
            }
            try
            {
                SqlCommand cmd = new SqlCommand(@" 
                Select 
                    Invoice1.System_Date_Time,
                    Invoice1.PartyCode,
                    Invoice1.Particular,
                    Invoice1.salesman,
                    Invoice1.Name as Name,
                    Invoice1.Address as Address,
                    Invoice1.Phone as Phone,
                    Invoice2.Code as Code,
                    Invoice2.Description as Description,
                    Invoice2.Rate as Rate,
                    Invoice2.Qty as Qty,
                    Invoice2.Item_Discount as Discount,
                    Invoice2.Amount as Amount,
                    Invoice2.PerDiscount,
                    Invoice2.DealRs,
                    Invoice2.Purchase_Amount,
                    Invoice3.Total,
                    Invoice3.Discount1,
                    Invoice3.PrevBalance,
                    Invoice3.Balance,
                    Invoice3.CashPaid,
                     PartyCode.Phone as PartyPhone,
                     PartyCode.Mobile as PartyMobile,
                     PartyCode.Address as PartyAddress

                from Invoice1
                inner join Invoice2 on invoice1.InvNo = Invoice2.InvNo
                inner join Invoice3 on invoice1.InvNo = Invoice3.InvNo
                inner join PartyCode on invoice1.PartyCode = PartyCode.Code
                where Invoice1.Compid = '" + CompID + "' and Invoice1.InvNo = " + invno, con);

                DataTable dt = new DataTable();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed)
                {
                    if (con.State == ConnectionState.Closed) { con.Open(); }
                }

                dAdapter.Fill(dt);
                con.Close();




                var model = new PrintViewModel<object>();
                var rows = new List<object>();

                var PartyName = dt.Rows[0]["Name"].ToString();
                var PartyCode = dt.Rows[0]["PartyCode"].ToString();
                var Date = Convert.ToDateTime(dt.Rows[0]["System_Date_Time"]).ToString("dd/MM/yyyy hh:mm tt");
                var Phone = dt.Rows[0]["Phone"].ToString();
                var PartyPhone = dt.Rows[0]["PartyPhone"].ToString();
                var PartyMobile = dt.Rows[0]["PartyMobile"].ToString();
                var salesman = dt.Rows[0]["salesman"].ToString();
                
                model.InvoiceName = "Sale Invoice";
                model.SaleManName = salesman;
                model.InvoiceNumber = invno;
                model.PartyName = PartyName;
                model.Date = Date;
                model.PhoneNumber = string.IsNullOrEmpty(Phone)? PartyPhone+" - "+PartyMobile: Phone;
                model.Particular=dt.Rows[0]["Particular"].ToString();
                model.PartyAddress =string.IsNullOrEmpty(dt.Rows[0]["Address"].ToString()) ? dt.Rows[0]["PartyAddress"].ToString() : dt.Rows[0]["Address"].ToString();
                model.TotalAmount = dt.Rows[0]["Total"].ToString();


                model.TotalFooterReceived = Convert.ToDecimal(dt.Rows[0]["CashPaid"]);
                model.TotalFooterDiscount = Convert.ToDecimal(dt.Rows[0]["Discount1"]);
                model.TotalFooterBillTotal = (Convert.ToDecimal(model.TotalAmount) - model.TotalFooterDiscount);
                model.TotalFooterNetBalance = Convert.ToDecimal(dt.Rows[0]["Balance"]);
                model.PreviousBalance = Convert.ToDecimal(dt.Rows[0]["PrevBalance"]);


                decimal TotalQty = 0;
                decimal TotalAmount = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string Code = dt.Rows[i]["Code"].ToString();
                    string Description = dt.Rows[i]["Description"].ToString();
                    string Rate = dt.Rows[i]["Rate"].ToString();
                    string Qty = dt.Rows[i]["Qty"].ToString();
                    var DealRs = Convert.ToDecimal(dt.Rows[i]["DealRs"].ToString());
                    TotalQty += Convert.ToDecimal(Qty);
                    var gAmount = Convert.ToDecimal(dt.Rows[i]["Amount"].ToString());
                    var Discount = Convert.ToDecimal(dt.Rows[i]["Discount"].ToString());

                    var Amount = gAmount - DealRs - Discount;
                    TotalAmount += Convert.ToDecimal(Amount);


                    var row = new 
                    {
                        SNo = i + 1,
                        Description = Description,
                        Qty = Qty,
                        Amount = Amount,
                    };
                    rows.Add(row);
                }
                model.Rows = rows;
                //if (model.Rows.Any())
                //{
                model.TotalQty = TotalQty;
                    model.TotalAmount = TotalAmount.ToString();
                //}
                return new BaseModel() { Success = true, Data = model };

            }
            catch (Exception ex)
            {

                return new BaseModel() { Success = true, Message = ex.Message };

            }
        }

        public DateTime getDate(string date)
        {
            var newdate = DateTime.ParseExact(date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);
            return newdate;
        }
    }
}