﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Transactions.TransactionPrints
{
    public partial class ExpenseEntryPrint : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        { }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel GetReport(string invno)
        {
            if (HttpContext.Current.Session["UserId"] == null)
            {
                return new BaseModel() { Success = false, LoginAgain = true };
            }
            try
            {
                string SecondDescription = "OperationalExpencesThroughCash";

                var cmd = new SqlCommand(
               "Select  " +
               "GeneralLedger.Code, " +
               "datedr, " +
               "Description, " +
               "V_No, " +
               "Narration, " +
               "AmountDr, " +
               "Station.Name as StationName, " +
               "GLCode.Title as Title " +
               "from " +                                        
               "GeneralLedger " +
               "inner join GLCode on GeneralLedger.Code = GLCode.Code "+
               "inner join Station on Station.Id = GeneralLedger.StationId " +
               "where " +       
               "GeneralLedger.CompID = '" + CompID + @"' and "+
               "GeneralLedger.V_No = '" + invno + @"' and "+
               "GeneralLedger.AmountDr > 0 and "+
               "GeneralLedger.SecondDescription = '" + SecondDescription + @"'", con);

                DataTable dt = new DataTable();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed)
                {
                    if (con.State == ConnectionState.Closed) { con.Open(); }
                }

                dAdapter.Fill(dt);
                con.Close();
                var model = new PrintViewModel<object>();
                var rows = new List<object>();

                var GLTitle = dt.Rows[0]["Title"].ToString();
                var StationName = dt.Rows[0]["StationName"].ToString();
                var Narration = dt.Rows[0]["Narration"].ToString();
                var Date = Convert.ToDateTime(dt.Rows[0]["datedr"]).ToString("dd/MM/yyyy");
                
                model.InvoiceNumber = invno;
                model.Date = Date;
                model.ReportStationName = StationName;
                model.Narration = Narration;
                

                decimal TotalAmount = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string Description = dt.Rows[i]["Description"].ToString();
                    string Amount = dt.Rows[i]["AmountDr"].ToString();
                    TotalAmount += Convert.ToDecimal(Amount);
                    var row = new 
                    {
                        SNo = i + 1,
                        GLTitle = GLTitle,
                        Description = Description,
                        Amount = Amount,
                    };
                    rows.Add(row);
                }
                model.Rows = rows;
                model.TotalAmount = TotalAmount.ToString();
                return new BaseModel() { Success = true, Data = model };

            }
            catch (Exception ex)
            {

                return new BaseModel() { Success = true, Message = ex.Message };

            }
        }

        public DateTime getDate(string date)
        {
            var newdate = DateTime.ParseExact(date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);
            return newdate;
        }
    }
}