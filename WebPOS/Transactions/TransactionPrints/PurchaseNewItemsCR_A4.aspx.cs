﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Transactions.TransactionPrints
{
    public partial class PurchaseNewItemsCR_A4 : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        { }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel GetReport(string invno)
        {
            if (HttpContext.Current.Session["UserId"] == null)
            {
                return new BaseModel() { Success = false, LoginAgain = true };
            }
            try
            {
                SqlCommand cmd = new SqlCommand(@" 
                Select 
                    Purchase1.Vender,
                    Purchase1.VenderCode,
                    Purchase1.dat,
                    Purchase1.InvNo,
                    Purchase1.Particular,
                    Purchase1.Phone , 
                    Purchase1.Address,

                    Purchase2.SNO as SNO,
                    Purchase2.Code as Code,
                    Purchase2.Description as Description,
                    Purchase2.Cost as Cost,
                    Purchase2.Qty as Qty,
                    Purchase2.Item_Discount as Discount,
                    Purchase2.Amount as Amount,
                    Purchase2.DiscountPercentageRate,
                    Purchase2.DealRs,
                    Purchase2.CostAfterDiscount,
                    Purchase2.StationId,

                    Purchase3.Total,
                    Purchase3.Discount as TotalDiscount,
                    Purchase3.Balance,
                    Purchase3.Paid,

                    PartyCode.Phone as PartyPhone,
                    Station.Name as StationName,
                    PartyCode.Mobile as PartyMobile,
                    PartyCode.Address as PartyAddress
                from Purchase1
                inner join Purchase2 on Purchase1.InvNo = Purchase2.InvNo
                inner join Purchase3 on Purchase1.InvNo = Purchase3.InvNo
                inner join PartyCode on Purchase1.VenderCode = PartyCode.Code
                left join Station on Purchase1.StationID=Station.ID
                where Purchase1.Compid = '" + CompID + "' and Purchase1.InvNo = '" + invno+"'", con);

                DataTable dt = new DataTable();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed)
                {
                    if (con.State == ConnectionState.Closed) { con.Open(); }
                }

                dAdapter.Fill(dt);
                con.Close();




                var model = new PrintViewModel<object>();
                var rows = new List<object>();

                var PartyName = dt.Rows[0]["Vender"].ToString();
                var PartyCode = dt.Rows[0]["VenderCode"].ToString();
                var Date = Convert.ToDateTime(dt.Rows[0]["dat"]).ToString("dd/MM/yyyy hh:mm:ss tt");
              
                var PartyPhone = dt.Rows[0]["PartyPhone"].ToString();
                var PartyMobile = dt.Rows[0]["PartyMobile"].ToString();
                
                model.InvoiceName = "Purchase Invoice";
                model.InvoiceNumber = invno;
                model.PartyName = PartyName;
                model.Date = Date;
                model.PhoneNumber = PartyPhone+" - "+PartyMobile;
                model.Particular=dt.Rows[0]["Particular"].ToString();
                model.PartyAddress =string.IsNullOrEmpty(dt.Rows[0]["Address"].ToString()) ? dt.Rows[0]["PartyAddress"].ToString() : dt.Rows[0]["Address"].ToString();
                model.TotalAmount = dt.Rows[0]["Total"].ToString();


                model.TotalFooterReceived = Convert.ToDecimal(dt.Rows[0]["Paid"]);
                model.TotalFooterDiscount = Convert.ToDecimal(dt.Rows[0]["TotalDiscount"]);
                model.TotalFooterBillTotal = (Convert.ToDecimal(model.TotalAmount) - model.TotalFooterDiscount);
                model.TotalFooterNetBalance = Convert.ToDecimal(dt.Rows[0]["Balance"]);


                decimal TotalQty = 0;
                decimal TotalAmount = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string SNo = dt.Rows[i]["SNO"].ToString();
                    string Code = dt.Rows[i]["Code"].ToString();
                    string Description = dt.Rows[i]["Description"].ToString();
                    string Rate = dt.Rows[i]["Cost"].ToString();
                    string ActualCost = dt.Rows[i]["CostAfterDiscount"].ToString();
                    string Qty = dt.Rows[i]["Qty"].ToString();
                    var DealRs = Convert.ToDecimal(dt.Rows[i]["DealRs"].ToString());
                    TotalQty += Convert.ToDecimal(Qty);
                    var gAmount = Convert.ToDecimal(dt.Rows[i]["Amount"].ToString());
                    var Discount = Convert.ToDecimal(dt.Rows[i]["Discount"].ToString());

                    var Amount = gAmount - DealRs - Discount;
                    TotalAmount += Convert.ToDecimal(Amount);


                    var row = new 
                    {
                        SNo = i + 1,
                        Description = Description,
                        Rate = Rate,
                        Qty = Qty,
                        Discount = Discount,
                        DealRs = DealRs,
                        Amount = Amount,
                    };
                    rows.Add(row);
                }
                model.Rows = rows;
                //if (model.Rows.Any())
                //{
                model.TotalQty = TotalQty;
                    model.TotalAmount = TotalAmount.ToString();
                //}
                return new BaseModel() { Success = true, Data = model };

            }
            catch (Exception ex)
            {

                return new BaseModel() { Success = true, Message = ex.Message };

            }
        }

        public DateTime getDate(string date)
        {
            var newdate = DateTime.ParseExact(date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);
            return newdate;
        }
    }
}