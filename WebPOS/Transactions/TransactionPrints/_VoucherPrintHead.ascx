﻿<%@  Control Language="C#" AutoEventWireup="true" CodeBehind="_VoucherPrintHead.ascx.cs" Inherits="WebPOS.Transactions._VoucherPrintHead" %>
<div class="row mb-3 justify-content-left align-self-end text-capitalize">
    <div class="col-12 text-center">
        <strong class="font-size-25 p-2"> Expenses </strong>
    </div>
</div>
<div class="row mb-3 justify-content-left align-self-end text-capitalize">
    <div class="col-6 ">
        <strong>Station Name:</strong> <span class="ReportStationName"></span>
    </div>
    <div class="col-6 text-right">
        <strong>Date:</strong> <span class="Date"> </span>
    </div>
    <div class="col-6 ">
        <strong>Voucher No.:</strong> <span class="InvoiceNumber"></span>
    </div>
</div>  