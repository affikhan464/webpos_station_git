﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SaleNewItemsCR_A4.aspx.cs" Inherits="WebPOS.Transactions.TransactionPrints.SaleNewItemsCR_A4" %>

<%--<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.4000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>--%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Print</title>
    <link rel="stylesheet" href="/css/vendor.min.css" />
    <link rel="stylesheet" href="/css/print.css" />

</head>
<body class="p-3">
    <form id="form1" runat="server">
        <%--<div>
            <CR:CrystalReportViewer ID="CRViewer" runat="server" AutoDataBind="true" HasPrintButton="True" PrintMode="ActiveX" GroupTreeStyle-ShowLines="True" />
        </div>--%>
        <%--  <div class="mx-auto w-25 text-center">
            <a class="printPageBtn"><i class="fas fa-print fa-4x p-3"></i></a>
        </div>--%>
        <div id="Page" class=" p-3 py-4 ">
            <%@ Register Src="~/_PrintHead.ascx" TagName="_PrintHead" TagPrefix="uc" %>
            <uc:_printhead id="_PrintHead1" runat="server" />
            <div class="sale">
                <%@ Register Src="~/Transactions/TransactionPrints/_TransactionPrintHead.ascx" TagName="_TransactionPrintHead" TagPrefix="uc" %>
                <uc:_transactionprinthead id="_TransactionPrintHead1" runat="server" />

                <div class="row mb-3 justify-content-left align-self-end text-capitalize">
                    <div class="col-12 ">
                        <%if(Session["NTN"]!=null && !string.IsNullOrEmpty(Session["NTN"].ToString())) {%>
                            <strong>NTN: </strong> <span> <%= Session["NTN"] %></span>
                        <%}%>

                    </div>
                    <div class="col-12 ">
                        <%if(Session["NTN"]!=null && !string.IsNullOrEmpty(Session["STRN"].ToString())) {%>
                            <strong>STRN: </strong> <span> <%= Session["STRN"] %></span>
                        <%}%>

                    </div>
                </div>
                <table>
                    <thead>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <div class="row mt-4">

                    <div class="col-12 text-capitalize font-weight-bold ">
                        <ol>
                            <li>Check item damage malfunction before picking up.</li>
                            <li>Item once sold are not Refundable or exchangeable.</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <script src="/js/vendor.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="/Script/Vendor/sweetalert.min.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="/Script/GetQueryString.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script>
        $(document).on("click", ".printPageBtn", function () {
            var printContent = document.getElementById("Page");
            var WinPrint = window.open('', '', 'width=827,height=1169');
            WinPrint.document.write('<link rel="stylesheet" href="/css/vendor.min.css" />');
            WinPrint.document.write('<link rel="stylesheet" href="/css/print.css" />');
            WinPrint.document.write(printContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        });
        $(document).ready(function () {

            $.ajax({
                url: "/Transactions/TransactionPrints/SaleNewItemsCR_A4.aspx/GetReport",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ invno: getQueryString("InvNo") }),
                success: function (BaseModel) {
                    debugger
                    if (BaseModel.d.Success) {
                        if (BaseModel.d.Data.Rows.length) {

                            var rows = BaseModel.d.Data.Rows;

                            var headrow = "";
                            for (var j = 0; j < Object.values(rows[0]).length; j++) {
                                var key = Object.keys(rows[0])[j];
                                if (key == "Description") {
                                    key = "Item Description";
                                }
                                if (key == "Rate") {
                                    key = "Cost";
                                }
                                if (key == "DealRs") {
                                    key = "Deal Rs.";
                                }
                                headrow += `<th class="text-uppercase text-center" >${key}</th>`;
                            }

                            $("table thead").append(headrow);
                            var invrows = '';
                            for (var i = 0; i < rows.length; i++) {
                                var row = '<tr>';
                                for (var j = 0; j < Object.values(rows[i]).length; j++) {
                                    var tdclass = '';
                                    var value = Object.values(rows[i])[j];
                                    var key = Object.keys(rows[i])[j];
                                    if (key == 'Qty' || key == 'Amount' || key == 'Discount'|| key == 'Cost'|| key == 'DealRs') {
                                        tdclass = 'class="text-right"';
                                        value = (Number(value)).toFixed(2);
                                    }
                                    else {
                                        tdclass = 'class="text-left"';
                                    }
                                    row += `<td ${tdclass}>${value}</td>`;

                                }
                                row += '</tr>';
                                invrows += row;
                            }
                            // Total Rows
                            var totalCol = Object.values(rows[0]).length;
                            var footerTotals = [
                                { name: 'Total Rs.', class: "" },
                                { name: 'Discount', class: "TotalFooterDiscount" },
                                { name: 'Bill Total', class: "TotalFooterBillTotal" },
                                { name: 'Received', class: "TotalFooterReceived" },
                                { name: 'Net Balance', class: "TotalFooterNetBalance" }];

                            for (var trows = 0; trows < 5; trows++) {

                                var footerrow = '<tr>';

                                for (var j = 0; j < totalCol; j++) {
                                    var currentCol = j + 1;
                                    var key = Object.keys(rows[0])[j];
                                    if (trows == 0) {
                                        if (currentCol == (totalCol - 1)) {
                                            footerrow += `<td class="font-weight-bold text-left">${footerTotals[trows].name}</td>`;
                                        } else {
                                            footerrow += `<td class="Total${Object.keys(rows[0])[j]} font-weight-bold text-right"></td>`;
                                        }
                                    }
                                    else {
                                        if (currentCol == (totalCol - 1)) {
                                            footerrow += `<td class="font-weight-bold text-left">${footerTotals[trows].name}</td>`;
                                        }
                                        else if (currentCol == (totalCol)) {
                                            footerrow += `<td class="font-weight-bold text-right ${footerTotals[trows].class}"></td>`;
                                        }
                                        else {
                                            footerrow += `<td class="font-weight-bold text-right"></td>`;
                                        }
                                    }


                                }
                                footerrow += '</tr>';
                                invrows += footerrow;
                            }


                            $("table tbody").append(invrows);

                            var data = BaseModel.d.Data;
                            for (var j = 0; j < Object.keys(data).length; j++) {
                                var key = Object.keys(data)[j];
                                var value = Object.values(data)[j];
                                if (key != "Rows") {
                                    if (isNaN(value) || value == "" || key == "InvoiceNumber" || key == "PhoneNumber" || value == null) {
                                        $("." + key).text(value);
                                    } else {
                                        $("." + key).text((Number(value)).toFixed(2));
                                    }
                                }
                            }
                            $("td").each(function () {
                                if (!$(this).text().trim().length) {
                                    $(this).addClass("border-0");
                                }
                            });
                            window.print()
                        }
                    }
                    else if (BaseModel.d.Success == false && BaseModel.d.LoginAgain == true) {
                        swal({
                            title: "Login Again",
                            html:
                                `<a target="_blank" href="/?c=1" style="display:block;color:black;font-weight:700">Login Here and print it again.</a> `,

                            type: "warning"

                        });
                        $("table tbody").append("<tr><td><h2 class='text-center'><a onclick='return location.href=location.href;'>Refresh this Page after login</a></h1></td></tr>");
                    }
                    else {
                        swal("Error", BaseModel.d.Message, "error");
                    }


                }



            })
        })
    </script>
</body>
</html>
