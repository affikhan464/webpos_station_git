﻿<%@  Control Language="C#" AutoEventWireup="true" CodeBehind="_GatePassPrintHead.ascx.cs" Inherits="WebPOS.Transactions._GatePassPrintHead" %>
<div class="row mb-3 justify-content-left align-self-end text-capitalize">
    <div class="col-12 text-center">
        <strong class="font-size-25 p-2"> Gate Pass </strong>
    </div>
</div>
<div class="row mb-3 justify-content-left align-self-end text-capitalize">
    <div class="col-6 ">
        <strong>From:</strong> <span class="StationNameFrom"></span>
    </div>
    <div class="col-6 text-right">
        <strong>DATE:</strong> <span class="Date"> </span>
    </div>
    <div class="col-6 ">
        <strong>To:</strong> <span class="StationNameTo"></span>
    </div>
    <div class="col-6 text-right">
        <strong>Voucher No.:</strong> <span class="InvoiceNumber"></span>
    </div>
    <div class="col-12 text-left p-2 mt-1 border">
        <strong>Narration:</strong> <span class="Narration"></span>
    </div>
</div>  