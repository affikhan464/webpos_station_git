﻿<%@  Control Language="C#" AutoEventWireup="true" CodeBehind="_TransactionPrintHead.ascx.cs" Inherits="WebPOS.Registration._TransactionPrintHead" %>
<div class="row mb-3 justify-content-left align-self-end text-capitalize">
    
    <div class="col-12 text-center">
        <span class="InvoiceName p-2"> Sale Invoice </span>
    </div>
</div>
<div class="row mb-3 justify-content-left align-self-end text-capitalize">
    <div class="col-12 ">
        <span>Sale Man:</span> <span class="SaleManName"></span>
    </div>
    <div class="col-6 ">
        <span>INVOICE NO:</span> <span class="InvoiceNumber"></span>
    </div>
    <div class="col-6 text-right">
        <span>DATE:</span> <span class="Date"> </span>
    </div>
    <div class="col-6 ">
        <span>CUSTOMER NAME:</span> <span class="PartyName"></span>
    </div>
    <div class="col-6 text-right">
        <span>PARTICULAR:</span> <span class="Particular"></span>
    </div>
    <div class="col-6 "> 
        <span>ADDRESS:</span> <span class="PartyAddress"></span>
    </div>
    <div class="col-6 text-right">
        <span>PRE. BAL:</span> <span class="PreviousBalance"></span>
    </div>
    <div class="col-6 "> 
        <span>PHONE/MOBILE:</span> <span class="PhoneNumber"></span> 
    </div>
</div>  