﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Transactions.TransactionPrints
{
    public partial class StockMovementCR_A4 : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        { }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel GetReport(string invno, string key)
        {
            if (HttpContext.Current.Session["UserId"] == null)
            {
                return new BaseModel() { Success = false, LoginAgain = true };
            }
            try
            {
                var security = new Security.Security();
                var decryptDateTime = security.DecryptKey(key, true);
                var expireDate = Convert.ToDateTime(decryptDateTime);
                if (expireDate > DateTime.Now)
                {

                    var cmd = new SqlCommand(
                   "Select " +
                       "SMONo," +
                       "Dat," +
                       "ItemCode," +
                       "ItemName," +
                       "QTY," +
                       "FromStation," +
                       "ToStation," +
                       "Narration," +
                       "FromStationCode," +
                       "ToStationCode," +
                       "SNO," +
                       "Amount," +
                       "Rate," +
                       "CompId  " +
                   "from InventoryMovement " +
                   "where  SMONo=" + invno + " and CompId='" + CompID + "'", con);

                    DataTable dt = new DataTable();
                    SqlDataAdapter dAdapter = new SqlDataAdapter();
                    dAdapter.SelectCommand = cmd;
                    if (con.State == ConnectionState.Closed)
                    {
                        if (con.State == ConnectionState.Closed) { con.Open(); }
                    }

                    dAdapter.Fill(dt);
                    con.Close();
                    var model = new PrintViewModel<object>();
                    var rows = new List<object>();

                    var FromStation = dt.Rows[0]["FromStation"].ToString();
                    var ToStation = dt.Rows[0]["ToStation"].ToString();
                    var Narration = dt.Rows[0]["Narration"].ToString();
                    var Date = Convert.ToDateTime(dt.Rows[0]["Dat"]).ToString("dd/MM/yyyy");

                    model.InvoiceNumber = invno;
                    model.Date = Date;
                    model.StationNameFrom = FromStation;
                    model.StationNameTo = ToStation;
                    model.Narration = Narration;


                    var TotalQty = 0;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string Code = dt.Rows[i]["ItemCode"].ToString();
                        string Description = dt.Rows[i]["ItemName"].ToString();
                        string Qty = dt.Rows[i]["Qty"].ToString();
                        TotalQty += Convert.ToInt32(Qty);

                        var row = new
                        {
                            SNo = i + 1,
                            Code = Code,
                            Name = Description,
                            Qty = Qty,
                        };
                        rows.Add(row);
                    }
                    model.Rows = rows;
                    model.TotalQty = TotalQty;
                    return new BaseModel() { Success = true, Data = model };
                }
                else
                {
                    return new BaseModel() { Success = false, IsTimeExpired = true };

                }

            }
            catch (Exception ex)
            {

                return new BaseModel() { Success = true, Message = ex.Message };

            }
        }

        public DateTime getDate(string date)
        {
            var newdate = DateTime.ParseExact(date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);
            return newdate;
        }
    }
}