﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="PaymentToPartyThroughCash.aspx.cs" Inherits="WebPOS.PaymentToPartyThroughCash" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div class="container">
        <div class="mt-5">

            <section class="form">
                <h2 class="form-header fa-money-bill-alt">Cash Payment</h2>
                <div class="slider-wrapper ">
                    <a class="fas fa-chevron-up fa-chevron-down slider l-0 r-0 b-0 mx-auto position-absolute text-center p-1"></a>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-3">
                            <span class="input input--hoshi">
                                <input id="txtVoucherNo" class="VoucherNo input__field input__field--hoshi" type="text" autocomplete="off" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Voucher</span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-3">
                            <span class="input input--hoshi">
                                <a class="btn btn-3 btn-bm btn-3e fa-edit w-100" onclick="EditVoucher()">Edit </a>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">
                                <input id="txtCurrentDate" class="input__field input__field--hoshi datetimepicker Date" type="text" autocomplete="off" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Date</span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">
                                <input id="txtPartyName" class="input__field input__field--hoshi PartyBox PartyName autocomplete empt" autocomplete="off" data-type="Party" data-id="txtPartyName" data-function="GetParty" data-nextfocus=".CashPaid" type="text" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Party Name </span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">
                                <input id="txtPartyCode" disabled="disabled" class=" PartyCode empt input__field input__field--hoshi" autocomplete="off" type="text" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Party Code </span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <span class="input input--hoshi">
                                <input id="txtAddress" class=" PartyAddress empt input__field input__field--hoshi" autocomplete="off" type="text" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Address </span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">
                                <input id="txtCashPaid" class=" CashPaid empt input__field input__field--hoshi" type="number" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Cash Paid</span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">
                                <input id="OldBalance" class=" PartyBalance empt input__field input__field--hoshi" type="number" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Old Balance </span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">
                                <input id="txtBalance" class="PartyBalance empt input__field input__field--hoshi" type="number" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Balance </span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">
                                <input id="txtNarration" class="Narration empt input__field input__field--hoshi" type="text" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Narration </span>
                                </label>
                            </span>
                        </div>
                        <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                            <span class="input input--hoshi radio--hoshi">
                                <label>
                                    <input id="Client_RadioButton" class="radio" type="radio" name="PartiesRB" />
                                    <span></span>
                                    Client
                                </label>
                            </span>
                        </div>
                        <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                            <span class="input input--hoshi radio--hoshi">
                                <label>
                                    <input id="Supplier_RadioButton" class="radio" type="radio" name="PartiesRB" />
                                    <span></span>
                                    Vendor
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">
                                <a id="btnSave" onclick="SaveBefore()" class="btnSave btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                            </span>
                        </div>
                    </div>
                </div>

            </section>
        </div>
    </div>


    <div class="container mt-3 vouchers">
        <ul class="nav nav-tabs w-100 d-flex p-0">
            <li class="flex-1 mb-0">
                <a data-toggle="tab" href="#CurrentSession" class="active shadow text-center w-100 d-block">
                    <span class="light"></span>
                    Current Session Vouchers
                </a>
            </li>
            <li class="flex-1 mb-0">
                <a data-toggle="tab" href="#Last5Days" class="shadow text-center w-100 d-block">
                    <span class="light"></span>
                    Last 5 Days Vouchers
                </a>
            </li>
            <li class="flex-1 mb-0">
                <a data-toggle="tab" href="#PartyLast5Days" onclick="getPartiesLastVouchers()" class="shadow text-center w-100 d-block">
                    <span class="light"></span>
                    Selected Party Last 15 Days Vouchers
                </a>
            </li>
        </ul>

        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade p-0 show active" id="CurrentSession" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="card sameheight-item items">
                    <ul class="item-list striped">
                        <li class="item item-list-header">
                            <div class="item-row">
                                <div class="item-col item-col-header flex-1">
                                    <div>
                                        <span>Sr #</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Voucher#</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Date</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-2">
                                    <div>
                                        <span>Party Name</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Cash Paid</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-3">
                                    <div>
                                        <span>Narration</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Edit</span>
                                    </div>
                                </div>
                            </div>
                        </li>

                    </ul>
                    <ul class="item-list contentList striped" style="max-height: 400px; overflow: auto">
                    </ul>
                </div>
            </div>
            <div class="tab-pane fade p-0" id="Last5Days" role="tabpanel" aria-labelledby="nav-profile-tab">
                <div class="card sameheight-item items">
                    <ul class="item-list striped">
                        <li class="item item-list-header">
                            <div class="item-row">
                                <div class="item-col item-col-header flex-1">
                                    <div>
                                        <span>Sr #</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Voucher#</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Date</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-2">
                                    <div>
                                        <span>Party Name</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Cash Paid</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-3">
                                    <div>
                                        <span>Narration</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Edit</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <ul class="item-list contentList striped" style="max-height: 400px; overflow: auto">
                    </ul>
                </div>
            </div>
            <div class="tab-pane fade p-0" id="PartyLast5Days" role="tabpanel" aria-labelledby="nav-profile-tab">
                <div class="card sameheight-item items">
                    <ul class="item-list striped">
                        <li class="item item-list-header">
                            <div class="item-row">
                                <div class="item-col item-col-header flex-1">
                                    <div>
                                        <span>Sr #</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Voucher#</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Date</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-2">
                                    <div>
                                        <span>Party Name</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Cash Paid</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-3">
                                    <div>
                                        <span>Narration</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Edit</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <ul class="item-list contentList striped" style="max-height: 400px; overflow: auto">
                    </ul>
                </div>
            </div>
        </div>


    </div>


</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Scripts" runat="Server">

    <script src="../Script/Autocomplete.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="../Script/Voucher/PaymentToPartyCash_Save.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script>
        function calculation() {


            var Balance = $(".PartyBalance").val();
            var Paid = $(".CashPaid").val();
            Balance = Number(Balance);
            Paid = Number(Paid);

            var NewBalance = 0;
            if (Paid == "") { Paid = 0; }
            NewBalance = (Balance - Paid);
            $("#txtBalance").val(NewBalance);


        }
        $(document).on("input", "#txtCashPaid", function (event) {

            calculation();

        });
        function EditVoucher() {
            var voucherId = $("#txtVoucherNo").val();
            window.location.href = "/Correction/PaymentToPartyThroughCashEdit.aspx?v=" + voucherId;
        }

    </script>
</asp:Content>
