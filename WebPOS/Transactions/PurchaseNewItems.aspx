﻿<%@ Page Title="Purchase Item" Language="C#" MasterPageFile="~/Transactions/TransactionsMasterPage.master" AutoEventWireup="true" CodeBehind="PurchaseNewItems.aspx.cs" Inherits="WebPOS.Transactions.PurchaseNewItems" %>


<asp:Content ContentPlaceHolderID="StyleContentPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeaderAndGridContentPlaceHolder" runat="server">


    <asp:TextBox ID="TextBox1" Style="display: none" CssClass="defaultPartyCode" runat="server"></asp:TextBox>
    <div class="searchAppSection">
        <div class="container-fluid transactionPage">
            <div class="sale row partySection clearfix">
                <div class="invDiv col  col-12 col-sm-4 col-md-4  col-lg-3 display-flex">
                    <%-- <span class="userlLabel">Invoice #</span>
                    <asp:TextBox ID="txtInvoiceNo" CssClass="InvoiceNumber form-control" name="Invoice" runat="server"></asp:TextBox>
                    --%>
                    <span class="input input--hoshi">
                        <asp:TextBox ID="txtInvoiceNo" CssClass="InvoiceNumber input__field input__field--hoshi" ClientIDMode="Static" name="Invoice" runat="server" autocomplete="off"></asp:TextBox>
                        <label class="input__label input__label--hoshi">
                            <span class="input__label-content input__label-content--hoshi">Invoice Number </span>
                        </label>
                    </span>
                    <div class="leftRightButtons display-flex">
                        <%    
                            var isAdmin = HttpContext.Current.Session["UserId"].ToString() == "0";
                            var listPages = HttpContext.Current.Session["UserPermittedPages"] as List<WebPOS.Model.MenuPage>;

                            if (isAdmin || listPages.Any(a => a.Url.Contains("/PurchaseEditNew.aspx")))
                            {
                        %>
                        <a class=" invoiceEditBtn"><i class="mt-1 fas fa-edit" aria-hidden="true"></i></a>    
                        <%
                            }
                        %>
                        <a class="printInvoiceBtn print"><i class="mt-1 fas fa-print" aria-hidden="true"></i></a>

                    </div>
                </div>
                <div class="manInv  col-12 col-sm-4 col-md-4  col-lg-3">
                    <div class="w-100 display-flex">
                        <span class="input input--hoshi mr-1 w-30">
                            <input name="ManInv" type="text" class="input__field input__field--hoshi ManInv p-1 " />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Manual Inv. </span>
                            </label>
                        </span>
                        <select id="salesManDD" class="w-100">
                        </select>
                    </div>

                    <%--<asp:DropDownList ID="lstSaleMan" CssClass="form-control" AutoPostBack="true" runat="server"></asp:DropDownList>
                    --%>
                    <span id="txtNorBal" data-toggle="tooltip" title="Nor Balance" class="px-2 pt-2 badge badge-success PartyNorBalance badge-bm inv1"></span>


                </div>
                <div class="searchDiv  col  col-12 col-sm-4 col-md-4  col-lg-3 display-flex">
                    <span class="input input--hoshi mr-1">
                        <input id="PartySearchBox" data-id="PartyTextbox" data-nextfocus="#SearchBox" data-type="Party" data-function="GetParty" class="input__field input__field--hoshi p-0 clientInput PartyName clearable PartySearchBox autocomplete" name="PartyName" type="text" />
                        <label class="input__label input__label--hoshi">
                            <span class="input__label-content input__label-content--hoshi"><i class="mr-1 fas fa-search"></i>Search a Party </span>
                        </label>
                    </span>
                    <span id="txtDealApplyNo" data-toggle="tooltip" title="Party Deal Apply Number!" class="px-2 pt-2 badge badge-success badge-bm PartyDealApplyNo inv1"></span>
                    <%    
                        if (isAdmin || listPages.Any(a => a.Url.Contains("PartyRegistration")))
                        {
                    %>
                    <div class=" ml-1 leftRightButtons display-flex w-10">
                        <a class="w-100" onclick="$('#partyRegistrationModal').modal()" data-toggle="tooltip" title="Add Party"><i class="pt-1 fas fa-plus" aria-hidden="true"></i></a>
                    </div>

                    <%
                        }
                    %>
                </div>
                <div class=" col  col-12 col-sm-4 col-md-4  col-lg-3">
                    <span class="input input--hoshi mr-1">
                        <input class="input__field input__field--hoshi clientInput PartyCode inv1" type="text" id="txtPartyCode" disabled="disabled" name="PartyCode" />
                        <label class="input__label input__label--hoshi">
                            <span class="input__label-content input__label-content--hoshi"><i class="mr-1 fas fa-user"></i>Party Code </span>
                        </label>
                    </span>
                </div>
                <div class=" col  col-12 col-sm-4 col-md-4  col-lg-3">
                    <%--<span class="userlLabel">Date</span>--%>
                    <%--<input class="form-control datetimepicker" name="Date" type="text" placeholder="01/01/2016" />--%>
                    <span class="input input--hoshi mr-1">
                        <asp:TextBox ID="txtDate" AutoPostBack="false" runat="server" name="Date" Enabled="true" CssClass="input__field input__field--hoshi datetimepicker Date"></asp:TextBox>
                        <label class="input__label input__label--hoshi">
                            <span class="input__label-content input__label-content--hoshi"><i class="mr-1 fas fa-calendar-day"></i>Date </span>
                        </label>
                    </span>
                </div>
                <div class=" col  col-12 col-sm-4 col-md-4  col-lg-3">
                    <span class="input input--hoshi mr-1">
                        <input id="txtBarCodeP" class="input__field input__field--hoshi clientInput" type="text" name="BC" />
                        <label class="input__label input__label--hoshi">
                            <span class="input__label-content input__label-content--hoshi"><i class="mr-1 fas fa-barcode"></i>Bar Code </span>
                        </label>
                    </span>
                    <%--<span class="userlLabel">Bar Code</span>--%>
                </div>
                <div class=" col  col-12 col-sm-4 col-md-4  col-lg-3">
                    <span class="input input--hoshi mr-1">
                        <input type="text" class="input__field input__field--hoshi clientInput PartyAddress" id="txtAddresss" name="PartyAddress" />
                        <label class="input__label input__label--hoshi">
                            <span class="input__label-content input__label-content--hoshi"><i class="mr-1 fas fa-map-marker-alt"></i>Address </span>
                        </label>
                    </span>
                </div>

                <div class="col  col-12 col-sm-4 col-md-4 col-lg-3">
                    <%--<span class="userlLabel">Phone No.</span>--%>
                    <span class="input input--hoshi mr-1">
                        <input type="text" id="txtPhone" class="input__field input__field--hoshi PartyPhoneNumber inv1 clientInput" name="PartyPhoneNumber" />
                        <label class="input__label input__label--hoshi">
                            <span class="input__label-content input__label-content--hoshi"><i class="mr-1 fas fa-mobile-alt"></i>Phone No. </span>
                        </label>
                    </span>
                </div>

                <div class=" col  col-12 col-sm-4 col-md-4  col-lg-3">
                    <%--<span class="userlLabel">Balance</span>--%>
                    <span class="input input--hoshi mr-1">
                        <input type="text" disabled="disabled" class="PartyBalance inv1 clientInput input__field input__field--hoshi" id="txtPreviousBalance" name="PartyBalance" />
                        <label class="input__label input__label--hoshi">
                            <span class="input__label-content input__label-content--hoshi"><i class="fas fa-hand-holding-usd mr-1 "></i>Balance </span>
                        </label>
                    </span>
                </div>

                <div class="searchDiv col   col-12 col-sm-4 col-md-4 col-lg-3 d-flex">
                    <span class="input input--hoshi mr-1">
                        <input id="SearchBox" data-id="itemTextbox" data-type="Item" data-function="GetPurchaseRecords" class="input__field input__field--hoshi clientInput SearchBox autocomplete" type="text" />
                        <label class="input__label input__label--hoshi">
                            <span class="input__label-content input__label-content--hoshi"><i class="mr-1 fas fa-search"></i>Search an item name </span>
                        </label>
                    </span>
                    <%    

                        if (isAdmin || listPages.Any(a => a.Url.Contains("/StockMovement.aspx")))
                        {
                    %>
                    <div class=" ml-1 leftRightButtons w-10">
                        <a class="w-100" target="_blank" href="/Transactions/StockMovement.aspx" data-toggle="tooltip" title="Stock Movement"><i class="pt-1 fas fa-people-carry" aria-hidden="true"></i></a>
                    </div>

                    <%
                        }
                    %>
                    <%    

                        if (isAdmin || listPages.Any(a => a.Url.Contains("/ItemRegistrationNew.aspx")))
                        {
                    %>
                    <div class=" ml-1 leftRightButtons w-10">
                        <a class="w-100" target="_blank" href="/Registration/ItemRegistrationNew.aspx" data-toggle="tooltip" title="New Item Registration"><i class="pt-1 fas fa-plus" aria-hidden="true"></i></a>
                    </div>

                    <%
                        }
                    %>
                </div>

                <div class=" col   col-12 col-sm-4 col-md-4  col-lg-3">
                    <%--<span class="userlLabel">Particular</span>--%>
                    <span class="input input--hoshi mr-1">
                        <input type="text" class="input__field input__field--hoshi clientInput PartyParticular" name="PartyParticular" />
                        <label class="input__label input__label--hoshi">
                            <span class="input__label-content input__label-content--hoshi"><i class="mr-1 fas fa-file-alt"></i>Particular </span>
                        </label>
                    </span>
                </div>
                <div class=" col   col-12 col-sm-4 col-md-4  col-lg-3">
                    <%--<span class="userlLabel">Credit Limit</span>--%>
                    <%--<input class="form-control PartyCreditLimit clientInput" type="text" name="PartyCreditLimit" />--%>
                    <span class="input input--hoshi mr-1">
                        <input type="text" class="input__field input__field--hoshi clientInput PartyParticular" name="PartyParticular" />
                        <label class="input__label input__label--hoshi">
                            <span class="input__label-content input__label-content--hoshi"><i class="mr-1 fas fa-credit-card"></i>Credit Limit </span>
                        </label>
                    </span>
                </div>

            </div>
        </div>
    </div>



    <div class="businessManagerSellSection businessManager BMtable">

        <div class="container-fluid">

            <div class="recommendedSection allItemsView table-responsive-md">
                <div id="mainTable" class="table-wrapper">


                    <div class="itemsHeader d-flex pr-4">
                        <div class="three flex-1 pl-0 phCol">
                            <a href="javascript:void(0)">S#
                            </a>
                        </div>
                        <div class="three flex-2 phCol">
                            <a href="javascript:void(0)">Code
                            </a>
                        </div>
                        <div class="fourteen flex-6 phCol">
                            <a href="javascript:void(0)">Description
                            </a>
                        </div>
                        <div class="three flex-1 phCol">
                            <a href="javascript:void(0)">Qty
                            </a>
                        </div>
                        <div class="three flex-3 phCol">
                            <a href="javascript:void(0)">Rate
											
                            </a>
                        </div>
                        <div class="three flex-3 phCol">
                            <a href="javascript:void(0)">G. Amount
											
                            </a>
                        </div>
                        <div class="three flex-1 phCol">
                            <a href="javascript:void(0)">Item D
											
                            </a>
                        </div>
                        <div class="three flex-1  phCol">
                            <a href="javascript:void(0)">Dis.%
											
                            </a>
                        </div>
                        <div class="three flex-1 phCol">
                            <a href="javascript:void(0)">Deal <span>Rs.</span>

                            </a>
                        </div>
                        <div class="three flex-3 phCol">
                            <a href="javascript:void(0)">N<span>et</span> Amount
											
                            </a>
                        </div>
                        <div class="three flex-3 phCol">
                            <a href="javascript:void(0)">Sell<span>ing</span> Price
											
                            </a>
                        </div>
                        <div class="three flex-3 phCol">
                            <a href="javascript:void(0)">Ac<span>tual </span>Cost
                            </a>
                        </div>
                        <div class="eight flex-3 phCol">
                            <a href="javascript:void(0)">Station
											
                            </a>
                        </div>
                        <div class="deleteRow three flex-3 phCol">
                            <a href="javascript:void(0)"></a>
                        </div>
                    </div>
                    <!-- itemsHeader -->
                    <div class="itemsSection">
                        <div>



                            <table class="TFtable w-100" id="myTable">

                                <tbody>
                                </tbody>

                            </table>



                        </div>

                    </div>
                    <div class="itemsFooter">
                        <div class='itemRow newRow d-flex pr-4'>
                            <div class='three flex-1'><span></span></div>
                            <div class='three flex-2'><span></span></div>
                            <div class='fourteen flex-6'><span></span></div>
                            <div class='three flex-1'>
                                <input disabled="disabled" class="form-control" name='QtyTotal' />
                            </div>
                            <div class='three flex-3'><span></span></div>
                            <div class='three flex-3'>
                                <input disabled="disabled" class="form-control" name='AmountTotal' />
                            </div>
                            <div class='three flex-1'>
                                <input disabled="disabled" class="form-control" name='ItemDisTotal' />
                            </div>
                            <div class='three flex-1'><span></span></div>
                            <div class='three flex-1'>
                                <input disabled="disabled" class="form-control" name='DealDisTotal' />
                            </div>
                            <div class='three flex-3'>
                                <input disabled="disabled" class="form-control" name='NetAmountTotal' />
                            </div>
                            <div class='three flex-3'><span></span></div>
                            <div class='three flex-3'>
                                <input disabled="disabled" class="form-control" name='PurAmountTotal' />
                            </div>
                            <div class='eight flex-3'>
                            </div>
                            <div class='three flex-3'>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- recommendedSection -->



    </div>

    <footer class="footerTotalSection">
        <div class="container-fluid ">
            <div class="footerTotalTextSection row">

                <div class="col-6 col-md-2  mt-2">


                    <label>T. pcs 0:</label>
                    <label>Brand:</label>
                    <label>Present Qty:</label>
                    <label>SP:</label>
                    <label>Selling Price:</label>

                </div>
                <div class="col-6 col-md-2  mt-2">

                    <label>
                        <input onchange="DealDisApplyOnAllitems()" id="chkAutoDeal" type="checkbox" class="checkbox rounded" /><span></span>Auto Deal Dis</label>
                    <label>
                        <input onchange="PerDisApplyOnAllitems()" id="chkAutoPerDis" type="checkbox" class="checkbox rounded" /><span></span>Auto % Dis</label>
                    <label>
                        <input id="BarCodeScanning" type="checkbox" class="checkbox rounded" /><span></span>BarCode Scanning</label>
                    <label>
                        <input id="chkAutoPrint" name="chkAutoPrint" type="checkbox" class="checkbox rounded" /><span></span>Auto Print</label>

                    <label>
                        <input id="ShowSellingprice" type="checkbox" class="checkbox rounded" /><span></span>Show Selling price</label>

                </div>
                <div class="col-12 col-md-2  mt-2">
                    <a class="btn btn-3 btn-bm btn-3c fa-save w-100 btnSave" onclick="SaveBefore()"><i class="mr-1 fas fa-save"></i>Save</a>
                </div>

                <div class="itemMeta clearfix col-12 col-md-6  mt-2">
                    <div class="row">

                        <div class="col-12 col-md-6 display-flex">
                            <%--<input id="txtTotPercentageDis" class="inv3 form-control " name="txtTotPercentageDis" disabled="disabled" type="text" placeholder="Net Discount" />--%>
                            <span class="input input--hoshi">
                                <input disabled id="txtTotPercentageDis" name="txtTotPercentageDis" class="inv3 input__field input__field--hoshi" type="text" autocomplete="off">
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">% Discount </span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-md-6 display-flex">
                            <%-- <span class="userlLabel">Total</span>
                            <input id="txtTotal" class="GrossTotal inv3 form-control " name="GrossTotal" disabled="disabled" type="text" placeholder="Total" />
                            --%>
                            <span class="input input--hoshi">
                                <input disabled id="txtTotal" name="GrossTotal" class="GrossTotal inv3 input__field input__field--hoshi" type="text" autocomplete="off">
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Total </span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-md-6 display-flex">
                            <%-- <span class="userlLabel">Deal Rs.</span>
                            <input class="DealRs inv3 form-control " id="txtTotDeal" disabled="disabled" name="DealRs" type="text" placeholder="Deal Rs." />
                            --%>
                            <span class="input input--hoshi">
                                <input disabled id="txtTotDeal" name="DealRs" class="DealRs inv3 input__field input__field--hoshi" type="text" autocomplete="off">
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Deal Rs. </span>
                                </label>
                            </span>
                        </div>

                        <div class="splitDiv  col-12 col-md-6 display-flex">
                            <%--<span class="userlLabel">Flat Disc %</span>--%>
                            <%--<input name="flatPer" type="text" class=" form-control FlatPer inv3 calcu firstSplitInput" id="txtFlatDiscountPer" placeholder=" %" />--%>
                            <%--<input name="flatDiscount" type="text" class="form-control FlatDiscount inv3 calcu" id="txtFlatDiscount" placeholder="Flat Discount" />--%>


                            <span class="input input--hoshi mr-3">
                                <input id="txtFlatDiscountPer" name="flatPer" class="FlatPer inv3 calcu input__field input__field--hoshi" type="text" autocomplete="off">
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Flat Disc % </span>
                                </label>
                            </span>
                            <span class="input input--hoshi">
                                <input id="txtFlatDiscount" name="flatDiscount" class="FlatDiscount inv3 calcu input__field input__field--hoshi" type="text" autocomplete="off">
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Flat Disc Rs. </span>
                                </label>
                            </span>
                        </div>

                        <div class="col-12 col-md-6 display-flex">
                            <%--<span class="userlLabel">Net Discount</span>--%>
                            <%--<input name="netDiscount" class="form-control NetDiscount inv3" id="txtTotalDiscount" disabled="disabled" type="text" placeholder="Net Discount" />--%>

                            <span class="input input--hoshi">
                                <input disabled id="txtTotalDiscount" name="netDiscount" class="NetDiscount inv3 input__field input__field--hoshi" type="text" autocomplete="off">
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Net Discount </span>
                                </label>
                            </span>
                        </div>
                        <div class="totalReceived col-12 col-md-6 display-flex">
                            <span class="input input--hoshi">
                                <input id="txtPaid" name="Recieved" class="Recieved inv3 calcu input__field input__field--hoshi" type="text" autocomplete="off">
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi"><i class="fas fa-money-bill mr-1"></i> Cash Received </span>
                                </label>
                            </span>
                        </div>


                        <div class="col-12 col-md-6 display-flex">
                            <%--<span class="userlLabel">Bill Total</span>--%>
                            <%--<input type="text" class="form-control BillTotal inv3" id="txtBillTotal" name="billTotal" disabled="disabled" placeholder="Bill Total" />--%>

                            <span class="input input--hoshi">
                                <input disabled id="txtBillTotal" name="billTotal" class="BillTotal inv3 input__field input__field--hoshi" type="text" autocomplete="off">
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Bill Total </span>
                                </label>
                            </span>

                        </div>

                        <div class="totalBalance col-12 col-md-6 display-flex">
                            <%--<span class="userlLabel">Balance</span>--%>
                            <%--<input name="balance" class="form-control Balance inv3" id="txtBalance" disabled="disabled" type="text" placeholder="Balance" />--%>

                            <span class="input input--hoshi">
                                <input disabled id="txtBalance" name="balance" class="Balance inv3 input__field input__field--hoshi" type="text" autocomplete="off">
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi"><i class="fas fa-hand-holding-usd mr-1 "></i>Balance </span>
                                </label>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </footer>

    <div class="IMEPortion  mt-2">
        <div class="container-fluid ">
            <div class="Ime-header  rounded text-center mb-3 p-2">
                <h4><i class="mr-1 fas fa-barcode"></i>IMEI's</h4>

            </div>
            <label class="btn btn-bm active checkbox-btn" style="width: 180px;">
                <input type="checkbox" name="isIMEVerifiedChkbx" />
                <i class="mr-1 fas fa-check mr-2"></i>
                Is IME Verified?
            </label>
            <div>

                <table id="IMEIRow">
                    <tr data-rownumber="1">
                        <td>
                            <input name="ItemRowNumber" type="hidden" />
                            <input name="Code" type="text" class="form-control" value="" /></td>
                        <td class="name">
                            <input name="ItemName" class="form-control" type="text" value="" /></td>
                        <td>
                            <input class="form-control" name="Qty" type="text" value="" /></td>
                        <td>
                            <input name="Rate" class="form-control calcu" type="text" value="" /></td>
                        <td>
                            <input class="form-control" name="Amount" type="text" value="" /></td>
                        <td>
                            <input class="form-control" type="text" value="" name="ItemDis" /></td>
                        <td>
                            <input name="PerDis" class="form-control calcu" type="text" value="" /></td>
                        <td>
                            <input name="DealDis" class="form-control" type="text" value="" /></td>
                        <td>
                            <input class="form-control" type="text" value="" name="NetAmount" /></td>
                        <td>
                            <input class="form-control" type="text" value="" name="PurAmount" /></td>
                        <td>
                            <input class="form-control" type="text" style="width: 200px; text-align: left" name="ImeiTxbx" placeholder="Enter IME" />
                        </td>
                    </tr>
                </table>
            </div>


            <div class="imei-table">
                <a class="btn btn-bm w-auto" onclick="reArrangeIMETabeRows()"><i class="mr-1 fas fa-list-ol"></i>Re-Arrange Table</a>
                <div class="businessManagerSellSection businessManager BMtable">


                    <div class="recommendedSection allItemsView">
                        <div class="itemsHeader">
                            <div class="three pl-0 phCol">
                                <a href="javascript:void(0)">S#
                                </a>
                            </div>
                            <div class="four  phCol">
                                <a href="javascript:void(0)">Code
                                </a>
                            </div>
                            <div class="fifteen  phCol">
                                <a href="javascript:void(0)">Description
                                </a>
                            </div>
                            <div class="thirteen phCol">
                                <a href="javascript:void(0)">IMEI
                                </a>
                            </div>
                            <div class="five phCol">
                                <a href="javascript:void(0)">Qty
                                </a>
                            </div>
                            <div class="eight phCol">
                                <a href="javascript:void(0)">Rate
											
                                </a>
                            </div>
                            <div class="eight phCol">
                                <a href="javascript:void(0)">G. Amount
											
                                </a>
                            </div>
                            <div class="eight phCol">
                                <a href="javascript:void(0)">Item D
											
                                </a>
                            </div>
                            <div class="six  phCol">
                                <a href="javascript:void(0)">Dis.%
											
                                </a>
                            </div>
                            <div class="eight phCol">
                                <a href="javascript:void(0)">Deal <span>Rs.</span>

                                </a>
                            </div>
                            <div class="eight phCol">
                                <a href="javascript:void(0)">N<span>et</span> Amount
											
                                </a>
                            </div>
                            <div class="eight phCol">
                                <a href="javascript:void(0)">Ac<span>tual </span>Cost
											
                                </a>
                            </div>
                            <div class="deleteRow three phCol">
                                <a href="javascript:void(0)"></a>
                            </div>
                            <div class="deleteRow three phCol">
                                <a href="javascript:void(0)"></a>
                            </div>
                        </div>

                        <div class="itemsSection">
                            <div class="imei-body">
                                <table class="TFtable" id="IMEITable">

                                    <tbody>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                        <div class="itemsFooter">
                            <div class='itemRow newRow'>
                                <div class='three'><span></span></div>
                                <div class='four'><span></span></div>
                                <div class='fifteen  '><span></span></div>
                                <div class='thirteen '><span></span></div>

                                <div class='five '>
                                    <input disabled="disabled" class="form-control" name='QtyTotal' />
                                </div>
                                <div class='eight '><span></span></div>
                                <div class='eight '>
                                    <input disabled="disabled" class="form-control" name='AmountTotal' />
                                </div>
                                <div class='eight '>
                                    <input disabled="disabled" class="form-control" name='ItemDisTotal' />
                                </div>
                                <div class='six'><span></span></div>
                                <div class='eight '>
                                    <input disabled="disabled" class="form-control" name='DealDisTotal' />
                                </div>
                                <div class='eight '>
                                    <input disabled="disabled" class="form-control" name='NetAmountTotal' />
                                </div>
                                <div class='eight '>
                                    <input disabled="disabled" class="form-control" name='PurAmountTotal' />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </div>

    </div>



</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ScriptsContentPlaceHolder" runat="server">
    <script src="/Script/PartyRegistration.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>

    <script type="text/javascript">

        $(document).ready(function () {



             appendAttribute.init("", "IncomeAccount");
             appendAttribute.init("", "COGSAccount");
            $(".invoiceEditBtn").on("click", function () {
                var invoiceNumber = $(".InvoiceNumber").val();
                window.location.href = "/Correction/PurchaseEditNew.aspx?InvNo=" + invoiceNumber;
            })


        });
    </script>
    <script src="../Script/Purchase/InsertDataInPurchase.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="../Script/CommonScript/calculationPurchase.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="../Script/Purchase/Save.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>

    <script type="text/javascript">
        $(document).on('input', '.calcu', function () {
            calculationSale(this);
        })

        $(document).on('keydown', function (e) {
            if ((e.ctrlKey || e.metaKey) && (e.key == "p" || e.charCode == 16 || e.charCode == 112 || e.keyCode == 80)) { //Ctrl + P
                e.preventDefault();
                printThisTransaction();
            }
        });
        function printThisTransaction() {
            window.open(
                '/Transactions/TransactionPrints/PurchaseNewItemsCR_A4.aspx?type=individual&InvNo=' + $(".InvoiceNumber").val(),
                '_blank'
            );
        }
        $(document).on('click', '.printInvoiceBtn', function () {
            printThisTransaction();
        })
        $(document).ready(function () {
            $("#app").addClass("hide");
            $(".page-name").text("New Purchase");
            resizeTable();
             appendAttribute.init("salesManDD", "SaleManList");

        });

    </script>
</asp:Content>
