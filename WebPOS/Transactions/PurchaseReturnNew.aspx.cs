﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Transactions
{
    public partial class PurchaseReturnNew : System.Web.UI.Page
    {
        Int32 OperatorID = 0;
        static string UserSession = "001";

        static string DiscountOnPurchaseReturnGLCode = "0101010600001";
        static string CashAccountGLCode = "0101010100001";
        static string PurchaseGLCode = "0101010600001";
        static string PurchaseReturnGLCode = "0101010600001";

        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompId"].ToString();
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;

        ModItem objModItem = new ModItem();
        Module1 objModule1 = new Module1();
        ModViewInventory objModViewInventory = new ModViewInventory();

        Module4 objModule4 = new Module4();
        ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
       

        protected void Page_Load(object sender, EventArgs e)
        {

            txtInvoiceNo.Text = (objModule1.MaxInvNoPurchaseReturn() - 1).ToString();
            txtDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
            ViewState["PageName"] = "Purchase Return";

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(SaveProductViewModel saveProductViewModel)
        {

            
            try
            {
               

                var stationId = string.Empty;
                if (HttpContext.Current.Session["StationId"] != null)
                {
                    stationId = HttpContext.Current.Session["StationId"].ToString();
                }

                if (stationId != string.Empty)
                {
                    Module1 objModule1 = new Module1();
                    Module4 objModule4 = new Module4();
                    ModItem objModItem = new ModItem();

                    ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();

                    if (con.State==ConnectionState.Closed) { con.Open(); }
                    tran = con.BeginTransaction();

                    var invoiceNumber = objModule1.MaxInvNoPurchaseReturn();

                    //if (lstSaleMan.Text != "") {SaleManID = 1; //objModPartyCodeAgainstName.SaleManCodeAgainstSaleManName(lstSaleMan.Text); }
                    var selectedDate = saveProductViewModel.ClientData.Date;
                    var convertedDate = DateTime.ParseExact(selectedDate + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                         System.Globalization.CultureInfo.InvariantCulture);
                    var InvoiceDate = convertedDate.ToString("yyyy-MM-dd");
                    var PartyName = saveProductViewModel.ClientData.Name.ToString();
                    var SaleManName = saveProductViewModel.ClientData.SalesManName;
                    var SaleManID = saveProductViewModel.ClientData.SalesManId;
                    var PartyCode = saveProductViewModel.ClientData.Code.ToString();
                    var Particular = saveProductViewModel.ClientData.Particular.ToString();
                    var Addresss = saveProductViewModel.ClientData.Address.ToString();

                    var ManualInvNo = saveProductViewModel.ClientData.ManInv.ToString();
                    var PartyPhone = saveProductViewModel.ClientData.PhoneNumber.ToString();
                    var SysTime = InvoiceDate + " " + DateTime.Now.ToString("HH:mm:ss");
                    var CompID = "01";
                    var NorBalance = Convert.ToInt32(saveProductViewModel.ClientData.NorBalance);
                    decimal Total = 0;
                    decimal Paid = 0;
                    decimal FlatDisRs = 0;
                    decimal FlatDisRate = 0;
                    decimal Balance = 0;
                    decimal NetDiscount = 0;
                    decimal PreviousBalance = 0;




                    Total = Convert.ToDecimal(saveProductViewModel.TotalData.GrossTotal);
                    Paid = Convert.ToDecimal(saveProductViewModel.TotalData.Recieved);
                    FlatDisRs = Convert.ToDecimal(saveProductViewModel.TotalData.FlatDiscount);
                    Balance = Convert.ToDecimal(saveProductViewModel.TotalData.Balance);
                    NetDiscount = Convert.ToDecimal(saveProductViewModel.TotalData.NetDiscount);
                    PreviousBalance = Convert.ToDecimal(saveProductViewModel.TotalData.PreviousBalance);
                    FlatDisRate = Convert.ToDecimal(saveProductViewModel.TotalData.FlatPer);



                    SqlCommand cmd = new SqlCommand("insert into PurchaseReturn1 (Date1,InvNo,PartyCode,Name,Total,FlatDiscount,Paid,Address,Particular,Discount,CompID,StationId) values('" + InvoiceDate + "','" + invoiceNumber + "','" + PartyCode + "','" + PartyName + "'," + Total + "," + FlatDisRs + "," + Paid + ",'" + Addresss + "','" + Particular + "'," + NetDiscount + ",'" + CompID + "'," + stationId + ")", con, tran);

                    cmd.ExecuteNonQuery();



                    //----------------------------------------------------------------------Purchase 2
                    foreach (var item in saveProductViewModel.Products)
                    {
                        var code = item.Code;
                        var SNO = Convert.ToInt16(item.SNo);
                        var Code = Convert.ToString(item.Code);
                        var Description = Convert.ToString(item.ItemName);
                        var Quantity = Convert.ToDecimal(item.Qty);
                        var Rate = Convert.ToDecimal(item.Rate);
                        var Amount = Convert.ToDecimal(item.Amount);
                        var ItemDiscount = Convert.ToDecimal(item.ItemDis);
                        var PerDiscount = Convert.ToDecimal(item.PerDis);
                        var DealRs = Convert.ToDecimal(item.DealDis);
                        var NetAmount = Convert.ToDecimal(item.NetAmount);
                        var PurchaseAmount = Convert.ToDecimal(item.PurAmount);
                        var Descrip = "Sold Inv # " + invoiceNumber + ",Party-" + PartyName;
                        var StationIDLocal = Convert.ToInt32(item.ItemStationId);

                        /////StationIDLocal = Convert.ToInt32(objModViewInventory.StationIDAgainstStationName(Convert.ToString(dset.Tables[0].Rows[i]["Station"])));
                        //SqlCommand cmd3 = new SqlCommand("insert into Purchase2 (InvNo,SN0,Code,Description ,Rate,Qty,Amount,Item_Discount,PerDiscount,Pcs,DealRs,CompID,Purchase_Amount)values(" + invoiceNumber + ",'" + SNO + "','" + Code + "','" + Description + "'," + Rate + "," + Quantity + "," + Amount + "," + ItemDiscount + "," + PerDiscount + "," + Quantity + "," + DealRs + ",'" + CompID + "'," + PurchaseAmount + ")", con, tran);
                        SqlCommand cmd3 = new SqlCommand("insert into PurchaseReturn2 (Date1,InvNo,SN0,Code,Description ,Rate,Qty,Amount,PartyCode1,PerDiscount,Item_Discount,CompID,DealRs,StationId)values('" + InvoiceDate + "'," + invoiceNumber + ",'" + SNO + "','" + Code + "','" + Description + "'," + Rate + "," + Quantity + "," + Amount + ",'" + PartyCode + "'," + PerDiscount + "," + ItemDiscount + ",'" + CompID + "'," + DealRs + "," + StationIDLocal + ")", con, tran);
                        cmd3.ExecuteNonQuery();
                        //SqlCommand cmd5 = new SqlCommand("insert into inventory (Dat,Icode,Description,Issued,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,System_Date_Time,CompID) values('" + InvoiceDate + "'," + Code + ",'" + Descrip + "'," + Quantity + "," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + SysTime + "','" + CompID + "')", con, tran);
                        SqlCommand cmd5 = new SqlCommand("insert into inventory (Dat,Icode,Description,Issued,BillNo,DescriptionOfBillNo,VenderCode,System_Date_Time,CompID,StationId) values('" + InvoiceDate + "','" + Code + "','" + "Purchase Return Account - Bill # " + invoiceNumber + ",Party-" + PartyName + "'," + Quantity + "," + Convert.ToDecimal(invoiceNumber) + ",'" + "PurchaseReturn" + "','" + PartyCode + "','" + SysTime + "','" + CompID + "'," + StationIDLocal + ")", con, tran);
                        cmd5.ExecuteNonQuery();

                        objModule4.ClosingBalanceNewTechniqueIssue(Convert.ToDecimal(Code), Quantity, tran, con);
                        objModule4.ClosingBalanceNewTechniqueStationIssue(Convert.ToDecimal(Code), Quantity, Convert.ToInt32(StationIDLocal), tran, con);


                        Quantity = 0;
                        Rate = 0;
                        Amount = 0;
                        ItemDiscount = 0;
                        PerDiscount = 0;
                        DealRs = 0;
                        NetAmount = 0;
                        PurchaseAmount = 0;
                    }


                    ///======================================================================IME
                    foreach (var IME in saveProductViewModel.IMEItems)
                    {
                        var SysTimeIME = InvoiceDate + " " + DateTime.Now.ToString("HH:mm:ss");
                        var SNO = Convert.ToInt16(IME.SNo);
                        var Code = Convert.ToString(IME.Code);
                        PartyName = saveProductViewModel.ClientData.Name.ToString();
                        var Description = IMEDescription.PurchaseReturn;
                        var InvNo = invoiceNumber;
                        var IMEI = IME.IMEI;
                        var Rate = IME.Rate;
                        var DisPer = IME.PerDis;
                        var DisRs = IME.ItemDis;
                        var DealRs = IME.DealDis;


                        SqlCommand cmd3IME = new SqlCommand(@"Insert into InventorySerialNoFinal (sold,Date1 , ItemCode , Description , VoucherNo,  IME , PartyCode, 
                                                                Price_Cost,System_Date_Time,CompID,DisRs,DisPer,DealRs) values (" + 1 + ",'" + InvoiceDate + 
                                                                "','" + Code + "','" + Description + "','" + invoiceNumber + "','" + IMEI + "','" +
                                                                PartyCode + "'," + Rate + ",'" + SysTimeIME + "','" + CompID + 
                                                                "'," + DisRs + "," + DisPer + "," + DealRs + ")", con, tran);
                        cmd3IME.ExecuteNonQuery();
                        SqlCommand cmd4IME = new SqlCommand(@"Update InventorySerialNoFinal set Sold=1 where CompID='" + CompID + "' and IME='" + IMEI + "'", con, tran);
                        cmd4IME.ExecuteNonQuery();

                    }

                    //////////        //=========================================================General Entry-------------------------------
                    decimal a = objModule1.MaxJVNo(con, tran);

                    SqlCommand cmd6 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountDr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,CompID,StationId) values('" + PartyCode + "','" + InvoiceDate + "','" + "Purchase Return Account - Bill # " + invoiceNumber + "'," + Total + "," + a + ",'" + "JV" + "','" + SysTime + "'," + Convert.ToDecimal(invoiceNumber) + ",'" + "PurchaseReturn" + "','" + PartyCode + "','" + CompID + "'," + stationId + ")", con, tran);
                    cmd6.ExecuteNonQuery();


                    var revenueAmounts = saveProductViewModel.Products.Where(e => !string.IsNullOrWhiteSpace(e.RevenueCode)).GroupBy(c => c.RevenueCode)
                        .Select(x => new GLModel()
                        {
                            Amount = x.Sum(o => Convert.ToDecimal(o.Amount)),
                            Code = x.First().RevenueCode
                        }).ToList();

                    foreach (var revenue in revenueAmounts)
                    {
                        if (revenue.Amount > 0)
                        {
                            //SqlCommand cmd7 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Narration,CompID) values('" + revenue.Code + "','" + InvoiceDate + "','" + "Receivable-Inv#" + invoiceNumber + "'," + revenue.Amount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + "Receivable-Inv#" + invoiceNumber + "','" + CompID + "')", con, tran);
                            SqlCommand cmd7 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,CompID,StationId) values('" + revenue.Code + "'," + "'" + InvoiceDate + "','" + "Accounts Payable - Bill # " + invoiceNumber + "'," + revenue.Amount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + Convert.ToDecimal(invoiceNumber) + ",'" + "PurchaseReturn" + "','" + PartyCode + "','" + CompID + "'," + stationId + ")", con, tran);
                            cmd7.ExecuteNonQuery();
                        }
                    }





                    //SqlCommand cmd8 = new SqlCommand("insert into PartiesLedger (code,datedr,DescriptionDr,AmountDr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,CompID) values('" + PartyCode + "','" + InvoiceDate + "','" + "Sales-Inv#" + invoiceNumber + " / " + ManualInvNo + " - Ptc:" + Particular + ",TPcs:" + "'," + Total + "," + a + ",'" + "JV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + CompID + "')", con, tran);
                    SqlCommand cmd8 = new SqlCommand("insert into PartiesLedger (code,datedr,DescriptionDr,AmountDr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,CompID,StationId) values('" + PartyCode + "','" + InvoiceDate + "','" + "Purchase Return - Bill # " + invoiceNumber + "'," + Total + "," + a + ",'" + "JV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "PurchaseReturn" + "','" + PartyCode + "','" + CompID + "'," + stationId + ")", con, tran);
                    cmd8.ExecuteNonQuery();
                    //////////        //---------------------------------------------------------Detail of bill into party ledger---------------------------------------------
                    SysTime = Convert.ToDateTime(SysTime).AddSeconds(5).ToString();
                    foreach (var item in saveProductViewModel.Products)
                    {
                        var code = item.Code;
                        var SNO = Convert.ToInt16(item.SNo);
                        var Code = Convert.ToString(item.Code);
                        var Description = Convert.ToString(item.ItemName);
                        var Quantity = Convert.ToDecimal(item.Qty);
                        var Rate = Convert.ToDecimal(item.Rate);
                        var Amount = Convert.ToDecimal(item.Amount);
                        var DisRs = Convert.ToDecimal(item.ItemDis);
                        var DisPer = Convert.ToDecimal(item.PerDis);
                        var DealRs = Convert.ToDecimal(item.DealDis);
                        var NetAmount = Convert.ToDecimal(item.NetAmount);
                        var PurchaseAmount = Convert.ToDecimal(item.PurAmount);
                        var Descrip = "Sold Inv # " + invoiceNumber + ",Party-" + PartyName;


                        //SqlCommand cmd10 = new SqlCommand("insert into PartiesLedger (datedr,Code,DescriptionDr,Qty,Rate,Amount,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,DisRs,DisPer,DealRS,CompID) values('" + InvoiceDate + "','" + PartyCode + "','" + Description + "','" + Quantity + "','" + Rate + "','" + Amount + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "'," + DisRs + "," + DisPer + "," + DealRs + ",'" + CompID + "')", con, tran);
                        SqlCommand cmd10 = new SqlCommand("insert into PartiesLedger (datedr,DescriptionDr,Code,Qty,Rate,Amount,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,CompID,DisRs,DisPer,DealRs,StationId) values('" + InvoiceDate + "','" + Description + "','" + PartyCode + "','" + Quantity + "','" + Rate + "','" + Amount + "','" + SysTime + "'," + Convert.ToDecimal(invoiceNumber) + ",'" + "PurchaseReturn" + "','" + PartyCode + "','" + CompID + "'," + DisRs + "," + DisPer + "," + DealRs + "," + stationId + ")", con, tran);
                        cmd10.ExecuteNonQuery();
                    }

                    //////////        //'--------------------------------Discount------------------------------------------------------------------------------------------------
                    SysTime = Convert.ToDateTime(SysTime).AddSeconds(5).ToString();
                    if (NetDiscount > 0)
                    {
                        a = objModule1.MaxJVNo(con, tran);                                                                                                                                                                              // 0103020300001
                                                                                                                                                                                                                               //SqlCommand cmd11 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountDr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Narration,CompID) values('" + "0103020300001" + "','" + InvoiceDate + "','" + "Accounts Receivable" + "'," + NetDiscount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + "Accounts Receivable" + "','" + CompID + "')", con, tran);
                        SqlCommand cmd11 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountDr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,CompID,StationId) values('" + DiscountOnPurchaseReturnGLCode + "','" + InvoiceDate + "','" + "Accounts Payable - Purchase Return Bill # " + invoiceNumber + "'," + NetDiscount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + Convert.ToDecimal(invoiceNumber) + ",'" + "PurchaseReturn" + "','" + PartyCode + "','" + CompID + "'," + stationId + ")", con, tran);
                        cmd11.ExecuteNonQuery();
                        //SqlCommand cmd12 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Narration,CompID) values('" + PartyCode + "','" + SysTime + "','" + "Discount Account" + "'," + NetDiscount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + "Discount Account,Inv No. " + invoiceNumber + "','" + CompID + "')", con, tran);
                        SqlCommand cmd12 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,CompID,StationId) values('" + PartyCode + "','" + InvoiceDate + "','" + "Discount Account - Purchase Return Bill # " + invoiceNumber + "'," + NetDiscount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "PurchaseReturn" + "','" + PartyCode + "','" + CompID + "'," + stationId + ")", con, tran);
                        cmd12.ExecuteNonQuery();

                        //SqlCommand cmd13 = new SqlCommand("insert into PartiesLedger (code,datedr,DescriptionDr,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,CompID) values('" + PartyCode + "','" + InvoiceDate + "','" + "Discount-Inv#" + invoiceNumber + "'," + NetDiscount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + CompID + "')", con, tran);
                        SqlCommand cmd13 = new SqlCommand("insert into PartiesLedger (code,datedr,DescriptionDr,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,CompID,StationId) values('" + PartyCode + "','" + InvoiceDate + "','" + "Discount Account - Purchase Return Bill # " + invoiceNumber + "'," + NetDiscount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + Convert.ToDecimal(invoiceNumber) + ",'" + "PurchaseReturn" + "','" + PartyCode + "','" + CompID + "'," + stationId + ")", con, tran);
                        cmd13.ExecuteNonQuery();


                    }
                    //////////        //'--------------------------------Cash Received-------------------------------------------------------------------------------------------------------------------------------- 103= A/R,


                    if (Paid > 0)
                    {
                        SysTime = Convert.ToDateTime(SysTime).AddSeconds(5).ToString();

                        decimal b = objModule1.MaxCRV(con, tran);

                        //SqlCommand cmd14 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountDr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Narration,Code1,CompID) values('" + "0101010100001" + "','" + InvoiceDate + "','" + "Receivable-" + PartyName + ",Inv#" + invoiceNumber + "'," + Paid + "," + b + ",'" + "CRV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + PartyName + ",Inv#" + invoiceNumber + "','" + CompID + "03010100001" + "','" + CompID + "')", con, tran);
                        SqlCommand cmd14 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountDr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,CompID,Code1,StationId) values('" + CashAccountGLCode + "','" + InvoiceDate + "','" + "Accounts Payable - Purchase Return Bill # " + invoiceNumber + "'," + Paid + "," + b + ",'" + "CRV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "PurchaseReturn" + "','" + PartyCode + "','" + CompID + "','" + PurchaseReturnGLCode + "'," + stationId + ")", con, tran);
                        cmd14.ExecuteNonQuery();

                        //SqlCommand cmd15 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Code1,Narration,CompID) values('" + PartyCode + "','" + InvoiceDate + "','" + "Cash Account-Inv#" + invoiceNumber + "'," + Paid + "," + b + ",'" + "CRV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + "0101010100001" + "','" + "Cash Account-Inv#" + invoiceNumber + "','" + CompID + "')", con, tran);
                        SqlCommand cmd15 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,CompID,Code1,StationId) values('" + PartyCode + "','" + InvoiceDate + "','" + "Cash Account - Purchase Return Bill # " + invoiceNumber + "'," + Paid + "," + b + ",'" + "CRV" + "','" + SysTime + "'," + Convert.ToDecimal(invoiceNumber) + ",'" + "PurchaseReturn" + "','" + PartyCode + "','" + CompID + "','" + PurchaseReturnGLCode + "'," + stationId + ")", con, tran);
                        cmd15.ExecuteNonQuery();

                        //SqlCommand cmd16 = new SqlCommand("insert into PartiesLedger (code,datedr,DescriptionDr,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,CompID) values('" + PartyCode + "','" + InvoiceDate + "','" + "Cash - Inv#" + invoiceNumber + "'," + Paid + "," + b + ",'" + "CRV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + CompID + "')", con, tran);
                        SqlCommand cmd16 = new SqlCommand("insert into PartiesLedger (code,datedr,DescriptionDr,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,CompID,StationId) values('" + PartyCode + "','" + InvoiceDate + "','" + "Cash Account - Purchase Return Bill # " + invoiceNumber + "'," + Paid + "," + b + ",'" + "CRV" + "','" + SysTime + "'," + Convert.ToDecimal(invoiceNumber) + ",'" + "PurchaseReturn" + "','" + PartyCode + "','" + CompID + "'," + stationId + ")", con, tran);
                        cmd16.ExecuteNonQuery();
                    }
                    //////////        //--------------------------------UpDate Party Balance-------------------------------------------------------


                    if (NorBalance == 1)
                    {

                        SqlCommand cmdUpDatePartyBalance = new SqlCommand("Update PartyCode set Balance=" + (PreviousBalance + Total - NetDiscount - Paid) + " where CompID='" + CompID + "' and Code='" + PartyCode + "'", con, tran);
                        cmdUpDatePartyBalance.ExecuteNonQuery();
                        SqlCommand cmdUpDatePartyBalance2 = new SqlCommand("Update GLCode set Balance=" + (PreviousBalance + Total - NetDiscount - Paid) + " where CompID='" + CompID + "' and Code='" + PartyCode + "'", con, tran);
                        cmdUpDatePartyBalance2.ExecuteNonQuery();
                    }
                    else
                        if (NorBalance == 2)
                    {
                        SqlCommand cmdUpDatePartyBalance = new SqlCommand("Update PartyCode set Balance=" + (PreviousBalance - Total + NetDiscount + Paid) + " where CompID='" + CompID + "' and Code='" + PartyCode + "'", con, tran);
                        cmdUpDatePartyBalance.ExecuteNonQuery();
                        SqlCommand cmdUpDatePartyBalance2 = new SqlCommand("Update GLCode set Balance=" + (PreviousBalance - Total + NetDiscount + Paid) + " where CompID='" + CompID + "' and Code='" + PartyCode + "'", con, tran);
                        cmdUpDatePartyBalance2.ExecuteNonQuery();
                    }


                    tran.Commit();
                    con.Close();
                    return new BaseModel() { Success = true, Message = "New Purchase Return Saved Successfully", LastInvoiceNumber = invoiceNumber.ToString() };
                }
                else
                {

                    return new BaseModel() { Success = false, Message = "Your session has been expired, login again and continue to Save this page.",LoginAgain=true };

                }
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
            finally
            {
                con.Close();
            }
        }

    }
}