﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Transactions
{
    public partial class SaleReturnNew : System.Web.UI.Page
    {
        Int32 OperatorID = 0;
        static string CompID = "01";
        string UserSession = "001";

        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;

        ModItem objModItem = new ModItem();
        Module1 objModule1 = new Module1();
        ModViewInventory objModViewInventory = new ModViewInventory();

        Module4 objModule4 = new Module4();
        ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
   

        protected void Page_Load(object sender, EventArgs e)
        {

            txtInvoiceNo.Text = (objModule1.MaxSaleReturnInvNoVerify()).ToString();
            

            txtDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
            ViewState["PageName"] = "Sale Return";

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(SaveProductViewModel saveProductViewModel)
        {

           try
            {
                var stationId = string.Empty;
                if (HttpContext.Current.Session["StationId"] != null)
                {
                    stationId = HttpContext.Current.Session["StationId"].ToString();
                }
                if (stationId != string.Empty)
                {
                    Module1 objModule1 = new Module1();
                    Module4 objModule4 = new Module4();
                    ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
                    if (con.State==ConnectionState.Closed) { con.Open(); }
                    tran = con.BeginTransaction();

                    var invoiceNumber = objModule1.MaxSaleReturnInvNo();

                    /////PG = "objModPartyCodeAgainstName.PartyGroupIdAgainstCode(PartyCode);


                    //if (lstSaleMan.Text != "") {SaleManID = 1; //objModPartyCodeAgainstName.SaleManCodeAgainstSaleManName(lstSaleMan.Text); }


                    var PartyName = saveProductViewModel.ClientData.Name.ToString();
                    var SaleManName = saveProductViewModel.ClientData.SalesManName;
                    var SaleManID = saveProductViewModel.ClientData.SalesManId;
                    var selectedDate = saveProductViewModel.ClientData.Date;
                    var convertedDate = DateTime.ParseExact(selectedDate + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                         System.Globalization.CultureInfo.InvariantCulture);
                    var InvoiceDate = convertedDate.ToString("yyyy-MM-dd");
                    var PartyCode = saveProductViewModel.ClientData.Code.ToString();
                    var Particular = saveProductViewModel.ClientData.Particular.ToString();
                    var Addresss = saveProductViewModel.ClientData.Address.ToString();

                    var ManualInvNo = saveProductViewModel.ClientData.ManInv.ToString();
                    var PartyPhone = saveProductViewModel.ClientData.PhoneNumber.ToString();
                    var SysTime = InvoiceDate + " " + DateTime.Now.ToString("HH:mm:ss");
                    var CompID = "01";
                    var NorBalance = Convert.ToInt32(saveProductViewModel.ClientData.NorBalance);
                    decimal Total = 0;
                    decimal Paid = 0;
                    decimal FlatDisRs = 0;
                    decimal FlatDisRate = 0;
                    decimal Balance = 0;
                    decimal NetDiscount = 0;
                    decimal PreviousBalance = 0;
                    
                    Total = Convert.ToDecimal(saveProductViewModel.TotalData.GrossTotal);
                    Paid = Convert.ToDecimal(saveProductViewModel.TotalData.Recieved);
                    FlatDisRs = Convert.ToDecimal(saveProductViewModel.TotalData.FlatDiscount);
                    Balance = Convert.ToDecimal(saveProductViewModel.TotalData.Balance);
                    NetDiscount = Convert.ToDecimal(saveProductViewModel.TotalData.NetDiscount);
                    PreviousBalance = Convert.ToDecimal(saveProductViewModel.TotalData.PreviousBalance);
                    FlatDisRate = Convert.ToDecimal(saveProductViewModel.TotalData.FlatPer);
                    SqlCommand cmd = new SqlCommand("insert into SaleReturn1 (Date1,InvNo,PartyCode,Name,SalesMan,Total,FlatDiscount,Paid,Particular,Address,Discount,CompID,System_Date_Time,OperatorName,StationId) values('" + InvoiceDate + "'," + Convert.ToDecimal(invoiceNumber) + ",'" + PartyCode + "','" + PartyName + "','" + SaleManID + "'," + Total + "," + FlatDisRs + "," + Paid + ",'" + Particular + "','" + Addresss + "'," + NetDiscount + ",'" + CompID + "','" + SysTime + "','" + "OperatorName" + "'," + stationId + ")", con, tran);
                    cmd.ExecuteNonQuery();



                    //----------------------------------------------------------------------Invoice 2
                    foreach (var item in saveProductViewModel.Products)
                    {
                        var code = item.Code;
                        var SNO = Convert.ToInt16(item.SNo);
                        var Code = Convert.ToString(item.Code);
                        var Description = Convert.ToString(item.ItemName);
                        var Quantity = Convert.ToDecimal(item.Qty);
                        var Rate = Convert.ToDecimal(item.Rate);
                        var Amount = Convert.ToDecimal(item.Amount);
                        var ItemDiscount = Convert.ToDecimal(item.ItemDis);
                        var PerDiscount = Convert.ToDecimal(item.PerDis);
                        var DealRs = Convert.ToDecimal(item.DealDis);
                        var NetAmount = Convert.ToDecimal(item.NetAmount);
                        var PurchaseAmount = Convert.ToDecimal(item.PurAmount);
                        var Descrip = "Sale Return Bill  # " + invoiceNumber + ",Party-" + PartyName;
                        var StationIDLocal = Convert.ToInt32(item.ItemStationId);

                        /////StationIDLocal = Convert.ToInt32(objModViewInventory.StationIDAgainstStationName(Convert.ToString(dset.Tables[0].Rows[i]["Station"])));
                        SqlCommand cmd3 = new SqlCommand("insert into SaleReturn2 (Date1,InvNo,SN0,Code,Description ,Rate,Qty,Amount,Purchase_Amount,ItemDis,Per_Dis,CompID,DealRs,StationId)values('" + InvoiceDate + "'," + invoiceNumber + ",'" + SNO + "','" + Code + "','" + Description + "'," + Rate + "," + Quantity + "," + Amount + "," + PurchaseAmount + "," + ItemDiscount + "," + PerDiscount + ",'" + CompID + "'," + DealRs + "," + StationIDLocal + ")", con, tran);
                        cmd3.ExecuteNonQuery();
                        SqlCommand cmd5 = new SqlCommand("insert into inventory (Dat,Icode,Description,Received,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,System_Date_Time,CompID,StationId) values('" + InvoiceDate + "','" + Code + "','" + Descrip + "'," + Quantity + "," + Convert.ToDecimal(invoiceNumber) + ",'" + "SaleReturn" + "','" + PartyCode + "','" + InvoiceDate + "','" + SysTime + "','" + CompID + "'," + StationIDLocal + ")", con, tran);
                        cmd5.ExecuteNonQuery();

                        objModule4.ClosingBalanceNewTechniqueReceived(Convert.ToDecimal(Code), Quantity, tran, con);
                        objModule4.ClosingBalanceNewTechniqueStationReceived(Convert.ToDecimal(Code), Quantity, Convert.ToInt32(StationIDLocal), tran, con);


                        Quantity = 0;
                        Rate = 0;
                        Amount = 0;
                        ItemDiscount = 0;
                        PerDiscount = 0;
                        DealRs = 0;
                        NetAmount = 0;
                        PurchaseAmount = 0;
                    }
                    ///======================================================================IME
                    foreach (var IME in saveProductViewModel.IMEItems)
                    {
                        var SysTimeIME = InvoiceDate + " " + DateTime.Now.ToString("HH:mm:ss");
                        var SNO = Convert.ToInt16(IME.SNo);
                        var Code = Convert.ToString(IME.Code);
                        PartyName = saveProductViewModel.ClientData.Name.ToString();
                        var Description = IMEDescription.SaleReturn;
                        var InvNo = invoiceNumber;
                        var IMEI = IME.IMEI;
                        var Rate = IME.Rate;
                        var DisPer = IME.PerDis;
                        var DisRs = IME.ItemDis;
                        var DealRs = IME.DealDis;


                        SqlCommand cmd3IME = new SqlCommand(@"Insert into InventorySerialNoFinal (Date1 , ItemCode , Description ,VoucherNo,IME , PartyCode,Price_Cost,
                                                                System_Date_Time,CompID,DisRs,DisPer,DealRs,Sold) values ('" + InvoiceDate + "','" + Code + "','" +
                                                                Description + "','" + InvNo + "','" + IMEI + "','" + PartyCode + "'," + 
                                                                Rate + ",'" + SysTimeIME + "','" + CompID + "'," + DisRs + "," +
                                                                DisPer + "," + DealRs + "," + 0 + ")", con, tran);
                        cmd3IME.ExecuteNonQuery();
                        
                    }

                    //////////        //=========================================================General Entry-------------------------------
                    decimal a = objModule1.MaxJVNo(con, tran);

                    var revenueAmounts = saveProductViewModel.Products.Where(e => !string.IsNullOrWhiteSpace(e.RevenueCode)).GroupBy(c => c.RevenueCode)
                        .Select(x => new GLModel()
                        {
                            Amount = x.Sum(o => Convert.ToDecimal(o.Amount)),
                            Code = x.First().RevenueCode
                        }).ToList();

                    foreach (var revenue in revenueAmounts)
                    {
                        if (revenue.Amount > 0)
                        {
                            //SqlCommand cmd7 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Narration,CompID) values('" + revenue.Code + "','" + InvoiceDate + "','" + "Receivable-Inv#" + invoiceNumber + "'," + revenue.Amount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + "Receivable-Inv#" + invoiceNumber + "','" + CompID + "')", con, tran);
                            SqlCommand cmd7 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountDr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,CompID,StationId) values('" + revenue.Code + "','" + InvoiceDate + "','" + "Account Receivable - Bill # " + invoiceNumber + "'," + revenue.Amount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + Convert.ToDecimal(invoiceNumber) + ",'" + "SaleReturn" + "','" + PartyCode + "','" + InvoiceDate + "','" + CompID + "'," + stationId + ")", con, tran);

                            cmd7.ExecuteNonQuery();
                        }
                    }




                    SqlCommand cmd6 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,CompID,StationId) values('" + PartyCode + "','" + InvoiceDate + "','" + "Sale Return - Bill # " + invoiceNumber + "'," + Total + "," + a + ",'" + "JV" + "','" + SysTime + "'," + Convert.ToDecimal(invoiceNumber) + ",'" + "SaleReturn" + "','" + PartyCode + "','" + InvoiceDate + "','" + CompID + "'," + stationId + ")", con, tran);
                    cmd6.ExecuteNonQuery();



                    SqlCommand cmd8 = new SqlCommand("insert into PartiesLedger (code,datedr,DescriptionDr,AmountCr,V_NO,V_type,AgainstInvNo,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,CompID,StationId) values('" + PartyCode + "','" + InvoiceDate + "','" + "Sales Return - Bill # " + invoiceNumber + "'," + Total + "," + a + ",'" + "JV" + "'," + Convert.ToDecimal(invoiceNumber) + ",'" + SysTime + "'," + Convert.ToDecimal(invoiceNumber) + ",'" + "SaleReturn" + "','" + PartyCode + "','" + InvoiceDate + "','" + CompID + "'," + stationId + ")", con, tran);
                    cmd8.ExecuteNonQuery();
                    //////////        //---------------------------------------------------------Detail of bill into party ledger---------------------------------------------
                    SysTime = Convert.ToDateTime(SysTime).AddSeconds(5).ToString();
                    foreach (var item in saveProductViewModel.Products)
                    {
                        var code = item.Code;
                        var SNO = Convert.ToInt16(item.SNo);
                        var Code = Convert.ToString(item.Code);
                        var Description = Convert.ToString(item.ItemName);
                        var Quantity = Convert.ToDecimal(item.Qty);
                        var Rate = Convert.ToDecimal(item.Rate);
                        var Amount = Convert.ToDecimal(item.Amount);
                        var DisRs = Convert.ToDecimal(item.ItemDis);
                        var DisPer = Convert.ToDecimal(item.PerDis);
                        var DealRs = Convert.ToDecimal(item.DealDis);
                        var NetAmount = Convert.ToDecimal(item.NetAmount);
                        var PurchaseAmount = Convert.ToDecimal(item.PurAmount);
                        var Descrip = "Sale Return Inv # " + invoiceNumber + ",Party-" + PartyName;


                        SqlCommand cmd10 = new SqlCommand("insert into PartiesLedger (datedr,Code,DescriptionDr,Qty,Rate,Amount,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,DisRs,DisPer,DealRS,CompID,StationId) values('" + InvoiceDate + "','" + PartyCode + "','" + Description + "','" + Quantity + "','" + Rate + "','" + Amount + "','" + SysTime + "'," + invoiceNumber + ",'" + "SaleReturn" + "','" + PartyCode + "','" + InvoiceDate + "'," + DisRs + "," + DisPer + "," + DealRs + ",'" + CompID + "'," + stationId + ")", con, tran);
                        //SqlCommand cmd10 = new SqlCommand("insert into PartiesLedger (datedr,Code,DescriptionDr,Qty,Rate,Amount,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,CompID,DealRs,DisRs,DisPer) values('" & da & "','" & txtClientCode.Text & "','" & Description & "','" & Qty & "','" & Rate & "','" & Amount & "','" & AddedTime & "'," & CDbl(txtBillNo.Text) & ",'" & "SaleReturn" & "','" & txtClientCode.Text & "','" & DTCurrentDate.Value & "','" & logon.CompID & "'," & DealRs & "," & DisRs & "," & PerDis & ")", con, tran);

                        cmd10.ExecuteNonQuery();
                    }

                    //////////        //'--------------------------------Discount------------------------------------------------------------------------------------------------
                    SysTime = Convert.ToDateTime(SysTime).AddSeconds(5).ToString();
                    if (NetDiscount > 0)
                    {
                        a = objModule1.MaxJVNo(con, tran);
                        SqlCommand cmd11 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountDr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,CompID,StationId) values('" + PartyCode + "','" + InvoiceDate + "','" + "Discount Accounts Sale Return Bill # " + invoiceNumber + "'," + NetDiscount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + Convert.ToDecimal(invoiceNumber) + ",'" + "SaleReturn" + "','" + PartyCode + "','" + InvoiceDate + "','" + CompID + "'," + stationId + ")", con, tran);
                        cmd11.ExecuteNonQuery();
                        SqlCommand cmd12 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,CompID,StationId) values('" + "0103030100001" + "','" + InvoiceDate + "','" + "Discount -Sale return Bill # " + invoiceNumber + "'," + NetDiscount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + Convert.ToDecimal(invoiceNumber) + ",'" + "SaleReturn" + "','" + PartyCode + "','" + InvoiceDate + "','" + CompID + "'," + stationId + ")", con, tran);
                        cmd12.ExecuteNonQuery();

                        SqlCommand cmd13 = new SqlCommand("insert into PartiesLedger (code,datedr,DescriptionDr,AmountDr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,CompID,StationId) values('" + PartyCode + "','" + InvoiceDate + "','" + "Discount Account - Sale return Bill # " + invoiceNumber + "'," + NetDiscount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + Convert.ToDecimal(invoiceNumber) + ",'" + "SaleReturn" + "','" + PartyCode + "','" + InvoiceDate + "','" + CompID + "'," + stationId + ")", con, tran);
                        cmd13.ExecuteNonQuery();


                    }
                    //////////        //'--------------------------------Cash Received-------------------------------------------------------------------------------------------------------------------------------- 103= A/R,


                    if (Paid > 0)
                    {
                        SysTime = Convert.ToDateTime(SysTime).AddSeconds(5).ToString();

                        decimal b = objModule1.MaxCRV(con, tran);

                        SqlCommand cmd14 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountDr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,CompID,StationId) values('" + PartyCode + "','" + InvoiceDate + "','" + "Cash Book-Sale return Bill # " + invoiceNumber + "'," + Paid + "," + b + ",'" + "CPV" + "','" + SysTime + "'," + Convert.ToDecimal(invoiceNumber) + ",'" + "SaleReturn" + "','" + PartyCode + "','" + InvoiceDate + "','" + CompID + "'," + stationId + ")", con, tran);
                        cmd14.ExecuteNonQuery();

                        SqlCommand cmd15 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountCr,V_NO,V_type,System_Date_Time,SecondDescription,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Code1,CompID,StationId) values('" + "0101010100001" + "','" + InvoiceDate + "','" + "Account Receivable -Sale Return Bill # " + invoiceNumber + "'," + Paid + "," + b + ",'" + "CPV" + "','" + SysTime + "','" + "SecondDescription" + "'," + Convert.ToDecimal(invoiceNumber) + ",'" + "SaleReturn" + "','" + PartyCode + "','" + InvoiceDate + "','" + "0101010100001" + "','" + CompID + "'," + stationId + ")", con, tran);
                        cmd15.ExecuteNonQuery();

                        SqlCommand cmd16 = new SqlCommand("insert into PartiesLedger (code,datedr,DescriptionDr,AmountDr,V_NO,V_type,AgainstInvNo,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,CompID,StationId) values('" + PartyCode + "','" + InvoiceDate + "','" + "Cash Account - Sale return Bill # " + invoiceNumber + "'," + Paid + "," + b + ",'" + "CPV" + "'," + Convert.ToDecimal(invoiceNumber) + ",'" + SysTime + "'," + Convert.ToDecimal(invoiceNumber) + ",'" + "SaleReturn" + "','" + PartyCode + "','" + InvoiceDate + "','" + CompID + "'," + stationId + ")", con, tran);
                        cmd16.ExecuteNonQuery();
                    }
                    SysTime = Convert.ToDateTime(SysTime).AddSeconds(5).ToString();
                    decimal TotalPurAmount = Convert.ToDecimal(saveProductViewModel.TotalData.TotalAverageCost);
                    if (TotalPurAmount > 0)
                    {
                        //////////AddedTime = AddedTime.AddSeconds(5);


                        var CGSAmounts = saveProductViewModel.Products.Where(e => !string.IsNullOrWhiteSpace(e.RevenueCode)).GroupBy(c => c.CGSCode)
                       .Select(x => new GLModel()
                       {
                           Amount = x.Sum(o => Convert.ToDecimal(o.PurAmount)),
                           Code = x.First().CGSCode
                       }).ToList();

                        a = objModule1.MaxJVNo(con, tran);
                        foreach (var CostGS in CGSAmounts)
                        {
                            if (CostGS.Amount > 0)
                            {
                                //SqlCommand cmd44 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountDr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Narration,CompID) values('" + CostGS.Code + "','" + InvoiceDate + "','" + "Inv#" + invoiceNumber + "'," + CostGS.Amount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + "Inv#" + invoiceNumber + "','" + CompID + "')", con, tran);
                                SqlCommand cmd44 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountDr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Narration,CompID,StationId) values('" + CostGS.Code + "','" + InvoiceDate + "','" + "Sale return Bill # " + invoiceNumber + "'," + CostGS.Amount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + Convert.ToDecimal(invoiceNumber) + ",'" + "SaleReturn" + "','" + PartyCode + "','" + InvoiceDate + "','" + "Sale return Bill # " + invoiceNumber + "','" + CompID + "'," + stationId + ")", con, tran);
                                cmd44.ExecuteNonQuery();
                            }

                        }
                        //SqlCommand cmd45 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Narration,CompID) values('" + Inventory + "','" + InvoiceDate + "','" + "Receivable-Inv#" + invoiceNumber + "'," + TotalPurAmount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + "Inv#" + invoiceNumber + "','" + CompID + "')", con, tran);
                        SqlCommand cmd45 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Narration,CompID,StationId) values('" + "0104010100001" + "','" + InvoiceDate + "','" + "Sale return Bill # " + invoiceNumber + "'," + TotalPurAmount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + Convert.ToDecimal(invoiceNumber) + ",'" + "SaleReturn" + "','" + PartyCode + "','" + InvoiceDate + "','" + "Sale return Bill # " + invoiceNumber + "','" + CompID + "'," + stationId + ")", con, tran);
                        cmd45.ExecuteNonQuery();


                    }
                    //////////        //--------------------------------UpDate Party Balance-------------------------------------------------------


                    if (NorBalance == 1)
                    {

                        SqlCommand cmdUpDatePartyBalance = new SqlCommand("Update PartyCode set Balance=" + (PreviousBalance - Total + NetDiscount + Paid) + " where CompID='" + CompID + "' and Code='" + PartyCode + "'", con, tran);
                        cmdUpDatePartyBalance.ExecuteNonQuery();
                        SqlCommand cmdUpDatePartyBalance2 = new SqlCommand("Update GLCode set Balance=" + (PreviousBalance - Total + NetDiscount + -Paid) + " where CompID='" + CompID + "' and Code='" + PartyCode + "'", con, tran);
                        cmdUpDatePartyBalance2.ExecuteNonQuery();
                    }
                    else
                        if (NorBalance == 2)
                    {
                        SqlCommand cmdUpDatePartyBalance = new SqlCommand("Update PartyCode set Balance=" + (PreviousBalance + Total - NetDiscount - Paid) + " where CompID='" + CompID + "' and Code='" + PartyCode + "'", con, tran);
                        cmdUpDatePartyBalance.ExecuteNonQuery();
                        SqlCommand cmdUpDatePartyBalance2 = new SqlCommand("Update GLCode set Balance=" + (PreviousBalance + Total - NetDiscount - Paid) + " where CompID='" + CompID + "' and Code='" + PartyCode + "'", con, tran);
                        cmdUpDatePartyBalance2.ExecuteNonQuery();
                    }


                    tran.Commit();
                    con.Close();
                    return new BaseModel() { Success = true, Message = "Sale Return Saved" + NorBalance, LastInvoiceNumber = invoiceNumber.ToString() };
                }
                else
                {

                    return new BaseModel() { Success = false, Message = "Your session has been expired, login again and continue to Save this page.", LoginAgain = true };

                }

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
            finally
            {
                con.Close();
            }
        }


    }
}