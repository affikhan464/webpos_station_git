﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="StockAdjustment.aspx.cs" Inherits="WebPOS.Transactions.StockAdjustment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  
    <div class="reports searchAppSection">
				<div class="contentContainer">
				 <h2 class="text-align-center"><i class=" fas fa-file-alt" aria-hidden="true"></i> Stock Adjustment</h2>
					<div class="BMSearchWrapper clearfix row">
						<!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->
                       
                            <div  class="col col-12 col-sm-6 col-md-2">
								<span class="userlLabel">Voucher#</span>
                                <input type="text" id="VoucherNumber" />
							</div>
							<div  class="col col-12 col-sm-6 col-md-3">
                             <span class="userlLabel">Date</span>
                             <input type="text" id="VoucherDate" class="datetimepicker Date" />

							</div>
                            <div  class="col col-12 col-sm-6 col-md-3">
                                 <input type="text" id="SearchBox" data-id="Stocks" data-type="Item" data-function="GetItems"  class="autocomplete AllVouchersTitle descritionTxbx" placeholder="Enter Description" />
                            </div>
                      
                        
                        <div class="col col-12 col-sm-3 col-md-2">
                            <a href="#" id="saveBtn" class="btn btn-bm"><i class="fas fa-save"></i>  Save</a>
						</div>
                        <div class="col col-12 col-sm-3 col-md-2">
                            <a href="#" id="get" class="btn btn-bm"><i class="fas fa-sync"></i>  Get</a>
						</div>
					</div><!-- searchAppSection -->
				</div>
			</div>
    <div class="BMtable">
                
				<div class="contentContainer">					

					<div class="reports recommendedSection allItemsView">


								<div class="itemsHeader">
                                  
									<div class="seven phCol">
										<a href="javascript:void(0)"> 
											Serial No.
										</a>
									</div>
									<div class="seven  phCol">                                                                    
										<a href="javascript:void(0)"> 
											Code
										</a>
									</div>
                                    <div class="sixteen  phCol">                                                                    
										<a href="javascript:void(0)"> 
											Description
										</a>
									</div>
                                    <div class="eleven  phCol">                                                                    
										<a href="javascript:void(0)"> 
											Stock in Computer
										</a>
									</div>
									<div class="ten phCol">
										<a href="javascript:void(0)"> 
											Stock on Shelf
										</a>
									</div>
								
									<div class="ten phCol">
										<a href="javascript:void(0)"> 
										    Adjustment
										</a>
									</div>
                                    <div class="ten phCol">
										<a href="javascript:void(0)"> 
										    Cost Excess
										</a>
									</div>
                                    <div class="ten phCol">
										<a href="javascript:void(0)"> 
										    Cost Short
										</a>
									</div>
                                    <div class="ten phCol">
										<a href="javascript:void(0)"> 
										    Unit Cost
										</a>
									</div>
								</div><!-- itemsHeader -->	
                         <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
								<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow'>
								<div class='seven'><span></span></div>
								<div class='seven '><span></span></div>
								<div class='sixteen '><span></span></div>
								<div class='eleven '><input type="number"  disabled="disabled" value="0"  name='totalStockInComputer'/></div>
								<div class='ten'><input type="number"  disabled="disabled" value="0"      name='totalStockOnShelf'/></div>
								<div class='ten '><input type="number"  disabled="disabled" value="0"     name='totalAdjustment'/></div>
								<div class='ten '><input  type="number" disabled="disabled" value="0"     name='totalCostExcess'/></div>
								<div class='ten'><input  type="number" disabled="disabled" value="0"      name='totalCostShort'/></div>
								<div class='ten'><input  type="number" disabled="disabled" value="0"      name='totalUnitCost'/></div>
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
      
	   </asp:Content>
<asp:Content ContentPlaceHolderID="Scripts" runat="server">
    <script src="/Script/Autocomplete.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="/Script/StockAdjustment.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
        <script type="text/javascript">

       var counter = 0;
       
       $(document).ready(function () {
          
                initializeDatePicker();
                resizeTable();
           
            });

            $(window).resize(function () {
                resizeTable();
              
            });

           
            $("#toTxbx").on("keypress", function (e) {
                if (e.key==13) {
                    $("#currentPage").remove();
                    $("<input hidden id='currentPage' value='0' />").appendTo("body");
                    clearInvoice();
                    counter = 0;
                    getReport();
                    
                }
            });
         </script>
</asp:Content>
