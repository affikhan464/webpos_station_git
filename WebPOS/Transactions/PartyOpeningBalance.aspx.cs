﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using WebPOS.Model;

namespace WebPOS
{
    public partial class PartyOpeningBalance : System.Web.UI.Page
    {
        Int32 OperatorID = 0;
        static string CompID = "01";
        static Module1 objModule1 = new Module1();
        static Module4 objModule4 = new Module4();
        static Module7 objModule7 = new Module7();
        static ModGLCode objModGLCode = new ModGLCode();
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(SaveVoucherViewModel saveVoucherViewModal)
        {
            decimal OpeningBalance = 0;
            string PartyCode = saveVoucherViewModal.PartyCode;

            SqlTransaction tran;
            if (con.State==ConnectionState.Closed) { con.Open(); }
            tran = con.BeginTransaction();
            try
            {
                if (saveVoucherViewModal.OpeningBalance != "") { OpeningBalance = Convert.ToDecimal(saveVoucherViewModal.OpeningBalance); }


                SqlCommand cmd1 = new SqlCommand("update  PartyOpBalance set OpBalance=" + OpeningBalance + " where CompID='" + CompID + "' and Code='" + PartyCode + "' and Dat='" + objModule7.StartDate() + "'", con, tran);
                cmd1.ExecuteNonQuery();
                SqlCommand cmd2 = new SqlCommand("update GLOpeningBalance set OpeningBal=" + OpeningBalance + " where  CompID='" + CompID + "' and Code='" + PartyCode + "' and  Date1='" + objModule7.StartDate() + "'", con, tran);
                cmd2.ExecuteNonQuery();

                tran.Commit();
                con.Close();
                objModule4.ClosingBalanceParties(PartyCode);
                objModGLCode.ClosingBalanceGL(PartyCode);

                return new BaseModel() { Success = true, Message = "Saved Successfully." };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }


        }


    }
}