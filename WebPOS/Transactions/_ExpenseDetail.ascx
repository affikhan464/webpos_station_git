﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="_ExpenseDetail.ascx.cs" Inherits="WebPOS.Transactions._ExpenseDetail" %>

<div class="modal fade" id="ExpenseDetailModal" tabindex="-1" role="dialog" aria-labelledby="ExpenseDetailModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close position-absolute rt-10" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body p-0">
                <div id="generic_price_table" class="m-0 ">
                    <section>
                        <div class="row">


                            <div class="col-12">

                                <!--PRICE CONTENT START-->
                                <div class="generic_content active clearfix">

                                    <!--HEAD PRICE DETAIL START-->
                                    <div class="generic_head_price clearfix m-0 pb-3">

                                        <!--HEAD CONTENT START-->
                                        <div class="generic_head_content clearfix">

                                            <!--HEAD START-->
                                            <div class="head_bg"></div>
                                            <div class="head">
                                                <span>Expense Detail</span>
                                            </div>
                                            <!--//HEAD END-->

                                        </div>
                                        <!--//HEAD CONTENT END-->

                                        <!--PRICE START-->
                                        <div class="generic_price_tag clearfix">
                                            <span class="price">
                                                <span class="sign subName"></span>
                                            </span>
                                            <span class="price">
                                                <span class="sign">Rs.</span>
                                                <span class="currency expenseAmount"></span>
                                                <span class="cent"></span>
                                            </span>
                                        </div>
                                        <div class="generic_price_btn clearfix m-0">
                                            <a class="separateReport font-size-14" target="_blank"><i class="fas fa-list"></i> Open Report Separately <i class="fas fa-arrow-right"></i> </a>
                                        </div>
                                        <!--//PRICE END-->

                                    </div>
                                    <!--//HEAD PRICE DETAIL END-->

                                    <!--FEATURE LIST START-->
                                    <div class="generic_feature_list pb-2">
                                        <table class="w-100">
                                            <thead>
                                                <tr class="p-1">
                                                    <th id="dateHead">Date</th>
                                                    <th>Voucher No.</th>
                                                    <th>Expense Head</th>
                                                    <th>Details/Narration</th>
                                                    <th>Expense Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody class="expensesRows">
                                            </tbody>

                                        </table>
                                    </div>
                                    <!--//FEATURE LIST END-->

                                    <!--BUTTON START-->
                                    <%-- <div class="generic_price_btn clearfix">
                                        <a class="" href="">Sign up</a>
                                    </div>--%>
                                    <!--//BUTTON END-->

                                </div>
                                <!--//PRICE CONTENT END-->

                            </div>

                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>


