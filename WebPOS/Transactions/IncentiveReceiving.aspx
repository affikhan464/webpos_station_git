﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="IncentiveReceiving.aspx.cs" Inherits="WebPOS.IncentiveReceiving" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container">
        <div class="mt-4">

            <section class="form">
                <h2 class="form-header fa-money-bill-alt">Incentive Receiving Form</h2>
                <div class="slider-wrapper ">
                    <a class="fas fa-chevron-up fa-chevron-down slider l-0 r-0 b-0 mx-auto position-absolute text-center p-1"></a>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4">
                            <span class="input input--hoshi">

                                <input id="txtVoucherNo" class="VoucherNo input__field input__field--hoshi" autocomplete="off" type="text" />

                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Voucher</span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                            <span class="input input--hoshi">
                                <a class="btn btn-3 btn-bm btn-3e w-100" id="btnGet" onclick="EditVoucher()"><i class="fas fa-edit"></i>Edit</a>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">
                                <input id="txtCurrentDate" class="datetimepicker Date input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Date</span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">

                                <input id="txtPartyName" data-type="Party" data-id="txtPartyName" data-nextfocus=".CashPaid" data-function="GetParty" name="PartyName" class=" PartyBox PartyName autocomplete empt input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Party Name</span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">

                                <input id="txtPartyCode" disabled="disabled" name="PartyCode" class="input__field input__field--hoshi PartyCode empt" type="text" autocomplete="off" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Party Code</span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">

                                <input id="txtAddress" class="input__field input__field--hoshi PartyAddress empt" type="text" autocomplete="off" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Address</span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">

                                <input id="txtCashPaid" class="input__field input__field--hoshi CashPaid empt" type="text" autocomplete="off" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Incentive Received</span>
                                </label>
                            </span>
                        </div>

                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">

                                <input id="txtBalance" class="input__field input__field--hoshi PartyBalance empt" type="text" autocomplete="off" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Balance</span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">

                                <input id="OldBalance" class="input__field input__field--hoshi PartyBalanceActual empt" name="PartyBalance" type="text" autocomplete="off" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Actual Balance</span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">

                                <input id="txtNarration" class="input__field input__field--hoshi Narration empt" type="text" autocomplete="off" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Narration</span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">
                                <a id="btnSave" onclick="SaveBefore()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                            </span>
                        </div>
                    </div>
                </div>

            </section>
        </div>
    </div>


    <div class="container mt-3 vouchers">
        <ul class="nav nav-tabs w-100 d-flex p-0">
            <li class="flex-1 mb-0">
                <a data-toggle="tab" href="#CurrentSession" class="active shadow text-center w-100 d-block">
                    <span class="light"></span>
                    Current Session Vouchers
                </a>
            </li>
            <li class="flex-1 mb-0">
                <a data-toggle="tab" href="#Last5Days" class="shadow text-center w-100 d-block">
                    <span class="light"></span>
                    Last 5 Days Vouchers
                </a>
            </li>
            <li class="flex-1 mb-0">
                <a data-toggle="tab" href="#PartyLast5Days" onclick="getPartiesLastVouchers()" class="shadow text-center w-100 d-block">
                    <span class="light"></span>
                    Selected Party Last 15 Days Vouchers
                </a>
            </li>
        </ul>

        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade p-0 show active" id="CurrentSession" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="card sameheight-item items">
                    <ul class="item-list striped">
                        <li class="item item-list-header">
                            <div class="item-row">
                                <div class="item-col item-col-header flex-1">
                                    <div>
                                        <span>Sr #</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Voucher#</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Date</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-2">
                                    <div>
                                        <span>Party Name</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Cash Paid</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-3">
                                    <div>
                                        <span>Narration</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Edit</span>
                                    </div>
                                </div>
                            </div>
                        </li>

                    </ul>
                    <ul class="item-list contentList striped" style="max-height: 400px; overflow: auto">
                    </ul>
                </div>
            </div>
            <div class="tab-pane fade p-0" id="Last5Days" role="tabpanel" aria-labelledby="nav-profile-tab">
                <div class="card sameheight-item items">
                    <ul class="item-list striped">
                        <li class="item item-list-header">
                            <div class="item-row">
                                <div class="item-col item-col-header flex-1">
                                    <div>
                                        <span>Sr #</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Voucher#</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Date</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-2">
                                    <div>
                                        <span>Party Name</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Cash Paid</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-3">
                                    <div>
                                        <span>Narration</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Edit</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <ul class="item-list contentList striped" style="max-height: 400px; overflow: auto">
                    </ul>
                </div>
            </div>
            <div class="tab-pane fade p-0" id="PartyLast5Days" role="tabpanel" aria-labelledby="nav-profile-tab">
                <div class="card sameheight-item items">
                    <ul class="item-list striped">
                        <li class="item item-list-header">
                            <div class="item-row">
                                <div class="item-col item-col-header flex-1">
                                    <div>
                                        <span>Sr #</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Voucher#</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Date</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-2">
                                    <div>
                                        <span>Party Name</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Cash Paid</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-3">
                                    <div>
                                        <span>Narration</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-1">
                                    <div>
                                        <span>Edit</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <ul class="item-list contentList striped" style="max-height: 400px; overflow: auto">
                    </ul>
                </div>
            </div>
        </div>


    </div>

</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    <script src="../Script/Autocomplete.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>

    <script src="../Script/Voucher/IncentiveReceiving_Save.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script type="text/javascript"> 

        $(document).on('input', '.CashPaid', function () {
            calculation();
        });
        function EditVoucher() {
            var voucherId = $("#txtVoucherNo").val();
            window.location.href = "/Correction/IncentiveReceivingEdit.aspx?v=" + voucherId;
        }
    </script>

</asp:Content>
