﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using WebPOS.Model;

namespace WebPOS
{
    public partial class WithDrawFromBank : System.Web.UI.Page
    {
        static string CashAccountGLCode = "0101010100001";
        Int32 OperatorID = 0;
        static string CompID = "01";
        Module1 objModule1 = new Module1();
        Module7 objModule7 = new Module7();
        Module4 objModule4 = new Module4();
        ModGLCode objModGLCode = new ModGLCode();
        static SqlTransaction tran;
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel SaveWithDrawFromBank(ModelPaymentVoucher ModelPaymentVoucher)
        {

            Module1 objModule1 = new Module1();
            Module4 objModule4 = new Module4();

            try
            {


                var stationId = string.Empty;
                if (HttpContext.Current.Session["StationId"] != null)
                {
                    stationId = HttpContext.Current.Session["StationId"].ToString();
                }
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                string SecondDescription = "CashWithDrawForPettyCash";
                var VoucherNo = Convert.ToString(objModule1.MaxCRV(con, tran));

                var VoucherDate = DateTime.ParseExact(ModelPaymentVoucher.Date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture).ToShortDateString();
                string GLCode = ModelPaymentVoucher.PartyCode;
                string GLTitle = ModelPaymentVoucher.PartyName;
                var SysTime = VoucherDate + " " + DateTime.Now.ToString("HH:mm:ss");

                decimal CashPaid = Convert.ToDecimal(ModelPaymentVoucher.CashPaid);
                string Narration = ModelPaymentVoucher.Narration;


                SqlCommand cmd1 = new SqlCommand("insert into GeneralLedger (Code,Description,AmountDr,V_No,V_Type,bankcode,datedr,Narration,SecondDescription,System_Date_Time,Code1,CompId, StationId) values ('" + CashAccountGLCode + "','" + Narration + "'," + Convert.ToDecimal(CashPaid) + "," + VoucherNo + ",'" + "CRV" + "','" + GLCode + "','" + Convert.ToDateTime(VoucherDate) + "','" + Narration + "','" + SecondDescription + "','" + SysTime + "','" + CashAccountGLCode + "','" + CompID + "'," + stationId + ")", con, tran);
                cmd1.ExecuteNonQuery();

                SqlCommand cmd2 = new SqlCommand("insert into GeneralLedger(Code, Description, AmountCr, V_No, V_Type, bankcode, datedr, Narration, SecondDescription, System_Date_Time, Code1,CompId, StationId) values('" + GLCode + "', '" + Narration + "', " + Convert.ToDecimal(CashPaid) + ", " + VoucherNo + ",'" + "CRV" + "', '" + GLCode + "', '" + Convert.ToDateTime(VoucherDate) + "', '" + Narration + "', '" + SecondDescription + "', '" + SysTime + "', '" + CashAccountGLCode + "','" + CompID + "'," + stationId + ")", con, tran);
                cmd2.ExecuteNonQuery();


                objModule4.ClosingBalancePartiesNew(GLCode, con, tran);
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Cash WidthDraw From Back Saved Successfully.", LastInvoiceNumber = VoucherNo.ToString() };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }



        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel GetLastVouchers()
        {

            try
            {



                var stationId = string.Empty;
                if (HttpContext.Current.Session["StationId"] != null)
                {
                    stationId = HttpContext.Current.Session["StationId"].ToString();
                }
                if (con.State == ConnectionState.Closed) { con.Open(); }

                var cmd = new SqlCommand(
                    "Select " +
                    "AmountCr," +
                    "V_No," +
                    "datedr," +
                    "Narration, " +
                    "GLCode.Title as PartyName " +
                    "from GeneralLedger " +
                    " join GLCode on GLCode.Code=GeneralLedger.bankcode" +
                    " where  GeneralLedger.CompID='" + CompID + "' and " +
                    "StationId='" + stationId + "' and " +
                    "V_Type='CRV' and " +
                    "SecondDescription='CashWithDrawForPettyCash' and " +
                    "AmountCr > 0 and " +
                    " datedr>='" + DateTime.Now.AddDays(-15).ToString("MM/dd/yyyy") + "' and datedr<='" + DateTime.Now.ToString("MM/dd/yyyy") + "' " +
                    " Order by datedr asc", con);
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];



                var reports = new List<ReportViewModel>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string CashPaid = data.Rows[i]["AmountCr"].ToString();
                    string VoucherNumber = data.Rows[i]["V_No"].ToString();
                    string Date = Convert.ToDateTime(data.Rows[i]["datedr"]).ToString("dd/MM/yyyy");
                    string Narration = data.Rows[i]["Narration"].ToString();
                    string PartyName = data.Rows[i]["PartyName"].ToString();
                    var party = new ReportViewModel()
                    {
                        Date = Date,
                        PartyName = PartyName,
                        CashPaid = CashPaid,
                        Narration = Narration,
                        VoucherNumber = VoucherNumber

                    };
                    reports.Add(party);
                }


                con.Close();
                return new BaseModel() { Success = true, Reports = reports };
            }
            catch (Exception ex)
            {

                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel GetBankLastVouchers(string partyCode)
        {

            try
            {
                var stationId = string.Empty;
                if (HttpContext.Current.Session["StationId"] != null)
                {
                    stationId = HttpContext.Current.Session["StationId"].ToString();
                }
                if (con.State == ConnectionState.Closed) { con.Open(); }

                var cmd = new SqlCommand(
                    "Select " +
                    "AmountCr," +
                    "V_No," +
                    "datedr," +
                    "Narration, " +
                    "GLCode.Title as PartyName " +
                    "from GeneralLedger " +
                    " join GLCode on GLCode.Code=GeneralLedger.bankcode" +
                    " where  GeneralLedger.CompID='" + CompID + "' and " +
                    "StationId='" + stationId + "' and " +
                    "GeneralLedger.bankcode='" + partyCode + "' and " +
                    "V_Type='CRV' and " +
                    "SecondDescription='CashWithDrawForPettyCash' and " +
                    "AmountCr > 0 and " +
                    " datedr>='" + DateTime.Now.AddDays(-15).ToString("MM/dd/yyyy") + "' and datedr<='" + DateTime.Now.ToString("MM/dd/yyyy") + "' " +
                    " Order by datedr asc", con);
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];



                var reports = new List<ReportViewModel>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string CashPaid = data.Rows[i]["AmountCr"].ToString();
                    string VoucherNumber = data.Rows[i]["V_No"].ToString();
                    string Date = Convert.ToDateTime(data.Rows[i]["datedr"]).ToString("dd/MM/yyyy");
                    string Narration = data.Rows[i]["Narration"].ToString();
                    string PartyName = data.Rows[i]["PartyName"].ToString();
                    var party = new ReportViewModel()
                    {
                        Date = Date,
                        PartyName = PartyName,
                        CashPaid = CashPaid,
                        Narration = Narration,
                        VoucherNumber = VoucherNumber

                    };
                    reports.Add(party);
                }


                con.Close();
                return new BaseModel() { Success = true, Reports = reports };
            }
            catch (Exception ex)
            {

                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }


    }
}