﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DefaultLocal.aspx.cs" Inherits="WebPOS.DefaultLocal" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Business Manager</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">

    <!-- Place favicon.ico and apple-touch-icon(s) in the root directory -->
    <%--<link rel="stylesheet" href="css/style.css">--%>
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link href="css/jquery.datetimepicker.css" rel="stylesheet" />
    <link href="css/loginStyleSheet/style.css" rel="stylesheet" />
    <%-- <link href="css/allitems.css" rel="stylesheet" />
    <link href="css/BMStyle.css" rel="stylesheet" />--%>
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <link href="css/fontawesome-all.css" rel="stylesheet" />
    <script src="Script/Vendor/jquery.min.js"></script>
    <link href="css/sweetalert.css" rel="stylesheet" />
    <link href="css/allitems.css" rel="stylesheet" />
    <link href="css/schoolStyle.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400" rel="stylesheet" />
</head>
<body>


    <main class="login-main">
        <div class="position-absolute t-5 lr-0 mx-auto w-100">
            <asp:Label ID="errorMsg" runat="server" Visible="false" CssClass="alert alert-danger lr-0 w-50 mx-auto position-absolute left-0 right-0">
                
            </asp:Label>
        </div>
        <div class="container-fluid">
           
        <div class="row hide"> 
          
            

            <div class="col-12 col-sm-12  col-md-12  col-lg-6 login-col mb">
                <div class="display-table">
                    <div class="display-cell">
                        <div class="form-Wrapper">
                            <div class="form-header  mb-2">

                                <div class="login-icon  col">

                                    <img src="images/logo.svg" />
                                </div>
                                <div class="text-Wrapper  col">
                                    <h2>Welcome</h2>
                                    <h4 class="text-center">&#1582;&#1608;&#1588; &#1570;&#1605;&#1583;&#1740;&#1583;</h4>
                                </div>
                            </div>

                            <form runat="server" autocomplete="off">
                                 <div class="input-group  mb-4">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-user"></i></div>
                                    </div>
                                    <input class="form-control" placeholder="User Name" type="text" id="txtuser" name="txtuser" value="">
                                </div>
                                <div class="input-group mb-4">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><i class="fas fa-key"></i></div>
                                    </div>
                                    <input type="password" class="form-control" placeholder="Password" id="txtpwd" name="txtpwd">
                                </div>
                                <a class="btn btn-outline-primary form-control login-btn mb-5" id="LogInbtn"><i class="fas fa-sign-in-alt"></i> Let’s Go!</a>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            </div>
    </main>
    <!-- wrapper -->

    <%--<script src="Script/Vendor/modernizr-2.8.3.js"></script>--%>
    <%--<script src="Script/Vendor/main.js"></script>--%>

    <script src="Script/Vendor/sweetalert.min.js"></script>
    <script src="Script/Vendor/ddslick.js"></script>
    <script src="Script/Dropdown.js"></script>
    <script src="Script/Vendor/jquery.datetimepicker.js"></script>
    <script src="Script/InitializeDateTimePicker.js"></script>
    <script src="Script/GetQueryString.js"></script>
    <script>

        $(document).ready(function () { 
            
            const smallSwal = swal.mixin({
                toast: true,
                position: 'top',
                showConfirmButton: false,
                timer: 3000
            });
            initializeDatePicker();
            $("#LogInbtn").on("click", function () {

                var loginViewModel = {
                    UserName: $("[name=txtuser]").val(),
                    Password: $("[name=txtpwd]").val()
                }
                $.ajax({
                    url: '/DefaultLocal.aspx/Login',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ loginViewModel: loginViewModel }),
                    success: function (response) {
                        if (response.d.Success) {
                            if (getQueryString("c") == "1") {
                                window.close();
                            }
                            else if (getQueryString("ReturnUrl")) {
                                   window.location.href = getQueryString("ReturnUrl");
                            }
                            else {
                                   window.location.href = '/Home.aspx';
                            }

                        }
                        else {

                            sweetAlert("Error", response.d.Message, "error");
                            $(".IsSoftwareExpired").remove();
                            $("body").append("<input class='IsSoftwareExpired' value='" + response.d.IsSoftwareExpired + "' hidden/>");

                            //verticalAlignCall();
                            if (response.d.IsSoftwareExpired) {
                                  alignForm();
                            }
                          
                        }

                    },
                    error: function (error) { swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                    }
                });
            });

           

        });
       

    </script>
</body>
</html>
