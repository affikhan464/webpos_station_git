﻿<%@ Page Title="Purchase Return Edit" Language="C#" MasterPageFile="~/Transactions/TransactionsMasterPage.master" AutoEventWireup="true" CodeBehind="PurchaseReturnEditNew.aspx.cs" Inherits="WebPOS.Correction.PurchaseReturnEditNew" %>

<asp:Content ContentPlaceHolderID="StyleContentPlaceHolder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="HeaderAndGridContentPlaceHolder" runat="server">

    <input id="prevInvoiceNumber" hidden="hidden" />
    <input id="nextInvoiceNumber" hidden="hidden" />
    <input id="currentInvoiceNumber" hidden="hidden" />
    <input id="BillStationId" hidden="hidden" />
    <asp:TextBox ID="TextBox1" Style="display: none" CssClass="defaultPartyCode" runat="server"></asp:TextBox>
    <div class="searchAppSection">
        <div class="container-fluid transactionPage">
            <div class="sale row partySection clearfix">
                <div class="invDiv col  col-12 col-sm-4 col-md-4  col-lg-3 display-flex">
                    <%-- <span class="userlLabel">Invoice #</span>
                    <asp:TextBox ID="txtInvoiceNo" CssClass="InvoiceNumber form-control" name="Invoice" runat="server"></asp:TextBox>
                    --%>
                    <span class="input input--hoshi w-50">
                        <asp:TextBox ID="txtInvoiceNo" CssClass="InvoiceNumber input__field input__field--hoshi" ClientIDMode="Static" name="Invoice" runat="server" autocomplete="off"></asp:TextBox>
                        <label class="input__label input__label--hoshi">
                            <span class="input__label-content input__label-content--hoshi">Invoice Number </span>
                        </label>
                    </span>
                    <div class="leftRightButtons display-flex">
                        
                        <a id="deleteInvoice" onclick="deleteVoucher()" data-toggle="tooltip" title="Delete Invoice"><i class="mt-1 fas fa-trash"></i></a>
                        <a id="printInvoice" data-toggle="tooltip" title="Print Invoice"><i class="mt-1 fas fa-print"></i></a>
                        <a class=" invoiceLeftBtn" id="previousInvoice" data-toggle="tooltip" title="Previous Invoice"><i class="mt-1 fas fa-chevron-circle-left"></i></a>
                        <a class="getInvoiceBtn" data-toggle="tooltip" title="Get Invoice" onclick="GetInvoice()"><i class="mt-1 fas fa-spinner"></i></a>
                        <a class=" invoiceRightBtn" id="nextInvoice" data-toggle="tooltip" title="Next Invoice"><i class="mt-1 fas fa-chevron-circle-right"></i></a>

                    </div>
                </div>
                <div class="manInv  col-12 col-sm-4 col-md-4  col-lg-3">
                    <div class="w-100 display-flex">
                        <span class="input input--hoshi mr-1 w-30">
                            <input name="ManInv" type="text" class="input__field input__field--hoshi ManInv p-1 " />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Manual Inv. </span>
                            </label>
                        </span>
                        <select id="salesManDD" class="w-100">
                        </select>
                    </div>

                    <%--<asp:DropDownList ID="lstSaleMan" CssClass="form-control" AutoPostBack="true" runat="server"></asp:DropDownList>
                    --%>
                    <span id="txtNorBal" data-toggle="tooltip" title="Nor Balance" class="px-2 pt-2 badge badge-success PartyNorBalance badge-bm inv1"></span>


                </div>
                <div class="searchDiv  col  col-12 col-sm-4 col-md-4  col-lg-3 display-flex">
                    <span class="input input--hoshi mr-1">
                        <input id="PartySearchBox" data-id="PartyTextbox" data-nextfocus="#SearchBox" data-type="Party" data-function="GetParty" class="input__field input__field--hoshi p-0 clientInput PartyName clearable PartySearchBox autocomplete" name="PartyName" type="text" />
                        <label class="input__label input__label--hoshi">
                            <span class="input__label-content input__label-content--hoshi"><i class="mr-1 fas fa-search"></i>Search a Party </span>
                        </label>
                    </span>
                    <span id="txtDealApplyNo" data-toggle="tooltip" title="Party Deal Apply Number!" class="px-2 pt-2 badge badge-success badge-bm PartyDealApplyNo inv1"></span>
                    <%    
                        var isAdmin = HttpContext.Current.Session["UserId"].ToString() == "0";
                        var listPages = HttpContext.Current.Session["UserPermittedPages"] as List<WebPOS.Model.MenuPage>;
                        if (isAdmin || listPages.Any(a => a.Url.Contains("PartyRegistration")))
                        {
                    %>
                    <div class=" ml-1 leftRightButtons display-flex w-10">
                        <a class="w-100" onclick="$('#partyRegistrationModal').modal()" data-toggle="tooltip" title="Add Party"><i class="pt-1 fas fa-plus" aria-hidden="true"></i></a>
                    </div>

                    <%
                        }
                    %>
                </div>
                <div class=" col  col-12 col-sm-4 col-md-4  col-lg-3">
                    <span class="input input--hoshi mr-1">
                        <input class="input__field input__field--hoshi clientInput PartyCode inv1" type="text" id="txtPartyCode" disabled="disabled" name="PartyCode" />
                        <label class="input__label input__label--hoshi">
                            <span class="input__label-content input__label-content--hoshi"><i class="mr-1 fas fa-user"></i>Party Code </span>
                        </label>
                    </span>
                </div>
                <div class=" col  col-12 col-sm-4 col-md-4  col-lg-3">
                    <%--<span class="userlLabel">Date</span>--%>
                    <%--<input class="form-control datetimepicker" name="Date" type="text" placeholder="01/01/2016" />--%>
                    <span class="input input--hoshi mr-1">
                        <asp:TextBox ID="txtDate" AutoPostBack="false" runat="server" name="Date" Enabled="true" CssClass="input__field input__field--hoshi datetimepicker Date"></asp:TextBox>
                        <label class="input__label input__label--hoshi">
                            <span class="input__label-content input__label-content--hoshi"><i class="mr-1 fas fa-calendar-day"></i>Date </span>
                        </label>
                    </span>
                </div>
                <div class=" col  col-12 col-sm-4 col-md-4  col-lg-3">
                    <span class="input input--hoshi mr-1">
                        <input id="txtBarCodePR" class="input__field input__field--hoshi clientInput" type="text" name="BC" />
                        <label class="input__label input__label--hoshi">
                            <span class="input__label-content input__label-content--hoshi"><i class="mr-1 fas fa-barcode"></i>Bar Code </span>
                        </label>
                    </span>
                    <%--<span class="userlLabel">Bar Code</span>--%>
                </div>
                <div class=" col  col-12 col-sm-4 col-md-4  col-lg-3">
                    <span class="input input--hoshi mr-1">
                        <input type="text" class="input__field input__field--hoshi clientInput PartyAddress" id="txtAddresss" name="PartyAddress" />
                        <label class="input__label input__label--hoshi">
                            <span class="input__label-content input__label-content--hoshi"><i class="mr-1 fas fa-map-marker-alt"></i>Address </span>
                        </label>
                    </span>
                </div>

                <div class="col  col-12 col-sm-4 col-md-4 col-lg-3">
                    <%--<span class="userlLabel">Phone No.</span>--%>
                    <span class="input input--hoshi mr-1">
                        <input type="text" id="txtPhone" class="input__field input__field--hoshi PartyPhoneNumber inv1 clientInput" name="PartyPhoneNumber" />
                        <label class="input__label input__label--hoshi">
                            <span class="input__label-content input__label-content--hoshi"><i class="mr-1 fas fa-mobile-alt"></i>Phone No. </span>
                        </label>
                    </span>
                </div>

                <div class=" col  col-12 col-sm-4 col-md-4  col-lg-3">
                    <%--<span class="userlLabel">Balance</span>--%>
                    <span class="input input--hoshi mr-1">
                        <input type="text" disabled="disabled" class="PartyBalance inv1 clientInput input__field input__field--hoshi" id="txtPreviousBalance" name="PartyBalance" />
                        <label class="input__label input__label--hoshi">
                            <span class="input__label-content input__label-content--hoshi"><i class="fas fa-hand-holding-usd mr-1 "></i>Balance </span>
                        </label>
                    </span>
                </div>

                <div class="searchDiv col   col-12 col-sm-4 col-md-4 col-lg-3 d-flex">
                    <span class="input input--hoshi mr-1">
                        <input id="SearchBox" data-id="itemTextbox" data-type="Item" data-function="GetPurchaseReturnRecords" class="input__field input__field--hoshi clientInput SearchBox autocomplete" type="text" />
                        <label class="input__label input__label--hoshi">
                            <span class="input__label-content input__label-content--hoshi"><i class="mr-1 fas fa-search"></i>Search an item name </span>
                        </label>
                    </span> 
                    <%    
                       
                        if (isAdmin || listPages.Any(a => a.Url.Contains("/StockMovement.aspx")))
                        {
                    %>
                    <div class=" ml-1 leftRightButtons w-10">
                        <a class="w-100" target="_blank" href="/Transactions/StockMovement.aspx" data-toggle="tooltip" title="Stock Movement"><i class="pt-1 fas fa-people-carry" aria-hidden="true"></i></a>
                    </div>

                    <%
                        }
                    %>
                    <%    
                       
                        if (isAdmin || listPages.Any(a => a.Url.Contains("/ItemRegistrationNew.aspx")))
                        {
                    %>
                    <div class=" ml-1 leftRightButtons w-10">
                        <a class="w-100" target="_blank" href="/Registration/ItemRegistrationNew.aspx" data-toggle="tooltip" title="New Item Registration"><i class="pt-1 fas fa-plus" aria-hidden="true"></i></a>
                    </div>

                    <%
                        }
                    %>
                </div>

                <div class=" col   col-12 col-sm-4 col-md-4  col-lg-3">
                    <%--<span class="userlLabel">Particular</span>--%>
                    <span class="input input--hoshi mr-1">
                        <input type="text" class="input__field input__field--hoshi clientInput PartyParticular" name="PartyParticular" />
                        <label class="input__label input__label--hoshi">
                            <span class="input__label-content input__label-content--hoshi"><i class="mr-1 fas fa-file-alt"></i>Particular </span>
                        </label>
                    </span>
                </div>
                <div class=" col   col-12 col-sm-4 col-md-4  col-lg-3">
                    <%--<span class="userlLabel">Credit Limit</span>--%>
                    <%--<input class="form-control PartyCreditLimit clientInput" type="text" name="PartyCreditLimit" />--%>
                    <span class="input input--hoshi mr-1">
                        <input type="text" class="input__field input__field--hoshi clientInput PartyParticular" name="PartyParticular" />
                        <label class="input__label input__label--hoshi">
                            <span class="input__label-content input__label-content--hoshi"><i class="mr-1 fas fa-credit-card"></i>Credit Limit </span>
                        </label>
                    </span>
                </div>

            </div>
        </div>
    </div>



    <div class="businessManagerSellSection businessManager BMtable">

        <div class="container-fluid">

            <div class="recommendedSection allItemsView table-responsive-md">
                <div id="mainTable" class="table-wrapper">


                    <div class="itemsHeader d-flex pr-4">
                        <div class="three flex-1 pl-0 phCol">
                            <a href="javascript:void(0)">S#
                            </a>
                        </div>
                        <div class="three flex-2 phCol">
                            <a href="javascript:void(0)">Code
                            </a>
                        </div>
                        <div class="fourteen flex-6 phCol">
                            <a href="javascript:void(0)">Description
                            </a>
                        </div>
                        <div class="three flex-1 phCol">
                            <a href="javascript:void(0)">Qty
                            </a>
                        </div>
                        <div class="three flex-3 phCol">
                            <a href="javascript:void(0)">Rate
											
                            </a>
                        </div>
                        <div class="three flex-3 phCol">
                            <a href="javascript:void(0)">G. Amount
											
                            </a>
                        </div>
                        <div class="three flex-1 phCol">
                            <a href="javascript:void(0)">Item D
											
                            </a>
                        </div>
                        <div class="three flex-1  phCol">
                            <a href="javascript:void(0)">Dis.%
											
                            </a>
                        </div>
                        <div class="three flex-1 phCol">
                            <a href="javascript:void(0)">Deal <span>Rs.</span>

                            </a>
                        </div>
                        <div class="three flex-3 phCol">
                            <a href="javascript:void(0)">N<span>et</span> Amount
											
                            </a>
                        </div>
                        <div class="three flex-3 phCol">
                            <a href="javascript:void(0)">Ac<span>tual </span>Cost
                            </a>
                        </div>
                        <div class="twelve flex-3 phCol">
                            <a href="javascript:void(0)">Station
											
                            </a>
                        </div>
                        <div class="three flex-3 phCol">
                            <a href="javascript:void(0)"></a>
                        </div>
                    </div>
                    <!-- itemsHeader -->
                    <div class="itemsSection">
                        <div>



                            <table class="TFtable w-100" id="myTable">

                                <tbody>
                                </tbody>

                            </table>



                        </div>

                    </div>
                    <div class="itemsFooter">
                        <div class='itemRow newRow d-flex pr-4'>
                            <div class='three flex-1'><span></span></div>
                            <div class='three flex-2'><span></span></div>
                            <div class='fourteen flex-6'><span></span></div>
                            <div class='three flex-1'>
                                <input disabled="disabled" class="form-control" name='QtyTotal' />
                            </div>
                            <div class='three flex-3'><span></span></div>
                            <div class='three flex-3'>
                                <input disabled="disabled" class="form-control" name='AmountTotal' />
                            </div>
                            <div class='three flex-1'>
                                <input disabled="disabled" class="form-control" name='ItemDisTotal' />
                            </div>
                            <div class='three flex-1'><span></span></div>
                            <div class='three flex-1'>
                                <input disabled="disabled" class="form-control" name='DealDisTotal' />
                            </div>
                            <div class='three flex-3'>
                                <input disabled="disabled" class="form-control" name='NetAmountTotal' />
                            </div>
                            <div class='three flex-3'>
                                <input disabled="disabled" class="form-control" name='PurAmountTotal' />
                            </div>
                            <div class='twelve flex-3'>
                            </div>
                            <div class='three flex-3'>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- recommendedSection -->



    </div>

    <footer class="footerTotalSection">
        <div class="container-fluid ">
            <div class="footerTotalTextSection row">

                <div class="col-6 col-md-2  mt-2">


                    <label>T. pcs 0:</label>
                    <label>Brand:</label>
                    <label>Present Qty:</label>
                    <label>SP:</label>
                    <label>Selling Price:</label>

                </div>
                <div class="col-6 col-md-2  mt-2">

                    <label>
                        <input onchange="DealDisApplyOnAllitems()" id="chkAutoDeal" type="checkbox" class="checkbox rounded" /><span></span>Auto Deal Dis</label>
                    <label>
                        <input onchange="PerDisApplyOnAllitems()" id="chkAutoPerDis" type="checkbox" class="checkbox rounded" /><span></span>Auto % Dis</label>
                    <label>
                        <input id="BarCodeScanning" type="checkbox" class="checkbox rounded" /><span></span>BarCode Scanning</label>
                    <label>
                        <input id="chkAutoPrint" name="chkAutoPrint" type="checkbox" class="checkbox rounded" /><span></span>Auto Print</label>

                    <label>
                        <input id="ShowSellingprice" type="checkbox" class="checkbox rounded" /><span></span>Show Selling price</label>

                </div>
                <div class="col-12 col-md-2  mt-2">
                    <a class="btn btn-3 btn-bm btn-3c fa-save w-100 btnSave" onclick="SaveBefore()"><i class="mr-1 fas fa-save"></i>Save</a>
                </div>

                <div class="itemMeta clearfix col-12 col-md-6  mt-2">
                    <div class="row">

                        <div class="col-12 col-md-6 display-flex">
                            <%--<input id="txtTotPercentageDis" class="inv3 form-control " name="txtTotPercentageDis" disabled="disabled" type="text" placeholder="Net Discount" />--%>
                            <span class="input input--hoshi">
                                <input disabled id="txtTotPercentageDis" name="txtTotPercentageDis" class="inv3 input__field input__field--hoshi" type="text" autocomplete="off">
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">% Discount </span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-md-6 display-flex">
                            <%-- <span class="userlLabel">Total</span>
                            <input id="txtTotal" class="GrossTotal inv3 form-control " name="GrossTotal" disabled="disabled" type="text" placeholder="Total" />
                            --%>
                            <span class="input input--hoshi">
                                <input disabled id="txtTotal" name="GrossTotal" class="GrossTotal inv3 input__field input__field--hoshi" type="text" autocomplete="off">
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Total </span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-md-6 display-flex">
                            <%-- <span class="userlLabel">Deal Rs.</span>
                            <input class="DealRs inv3 form-control " id="txtTotDeal" disabled="disabled" name="DealRs" type="text" placeholder="Deal Rs." />
                            --%>
                            <span class="input input--hoshi">
                                <input disabled id="txtTotDeal" name="DealRs" class="DealRs inv3 input__field input__field--hoshi" type="text" autocomplete="off">
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Deal Rs. </span>
                                </label>
                            </span>
                        </div>

                        <div class="splitDiv  col-12 col-md-6 display-flex">
                            <%--<span class="userlLabel">Flat Disc %</span>--%>
                            <%--<input name="flatPer" type="text" class=" form-control FlatPer inv3 calcu firstSplitInput" id="txtFlatDiscountPer" placeholder=" %" />--%>
                            <%--<input name="flatDiscount" type="text" class="form-control FlatDiscount inv3 calcu" id="txtFlatDiscount" placeholder="Flat Discount" />--%>


                            <span class="input input--hoshi mr-3">
                                <input id="txtFlatDiscountPer" name="flatPer" class="FlatPer inv3 calcu input__field input__field--hoshi" type="text" autocomplete="off">
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Flat Disc % </span>
                                </label>
                            </span>
                            <span class="input input--hoshi">
                                <input id="txtFlatDiscount" name="flatDiscount" class="FlatDiscount inv3 calcu input__field input__field--hoshi" type="text" autocomplete="off">
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Flat Disc Rs. </span>
                                </label>
                            </span>
                        </div>

                        <div class="col-12 col-md-6 display-flex">
                            <%--<span class="userlLabel">Net Discount</span>--%>
                            <%--<input name="netDiscount" class="form-control NetDiscount inv3" id="txtTotalDiscount" disabled="disabled" type="text" placeholder="Net Discount" />--%>

                            <span class="input input--hoshi">
                                <input disabled id="txtTotalDiscount" name="netDiscount" class="NetDiscount inv3 input__field input__field--hoshi" type="text" autocomplete="off">
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Net Discount </span>
                                </label>
                            </span>
                        </div>
                        <div class="totalReceived col-12 col-md-6 display-flex">
                            <span class="input input--hoshi">
                                <input id="txtPaid" name="Recieved" class="Recieved inv3 calcu input__field input__field--hoshi" type="text" autocomplete="off">
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi"><i class="fas fa-money-bill mr-1"></i> Cash Received </span>
                                </label>
                            </span>
                        </div>


                        <div class="col-12 col-md-6 display-flex">
                            <%--<span class="userlLabel">Bill Total</span>--%>
                            <%--<input type="text" class="form-control BillTotal inv3" id="txtBillTotal" name="billTotal" disabled="disabled" placeholder="Bill Total" />--%>

                            <span class="input input--hoshi">
                                <input disabled id="txtBillTotal" name="billTotal" class="BillTotal inv3 input__field input__field--hoshi" type="text" autocomplete="off">
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Bill Total </span>
                                </label>
                            </span>

                        </div>

                        <div class="totalBalance col-12 col-md-6 display-flex">
                            <%--<span class="userlLabel">Balance</span>--%>
                            <%--<input name="balance" class="form-control Balance inv3" id="txtBalance" disabled="disabled" type="text" placeholder="Balance" />--%>

                            <span class="input input--hoshi">
                                <input disabled id="txtBalance" name="balance" class="Balance inv3 input__field input__field--hoshi" type="text" autocomplete="off">
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi"><i class="fas fa-hand-holding-usd mr-1 "></i>Balance </span>
                                </label>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </footer>

    <div class="IMEPortion  mt-2">
        <div class="container-fluid ">
            <div class="Ime-header  rounded text-center mb-3 p-2">
                <h4><i class="mr-1 fas fa-barcode"></i>IMEI's</h4>

            </div>
            <label class="btn btn-bm active checkbox-btn" style="width: 180px;">
                <input type="checkbox" name="isIMEVerifiedChkbx" />
                <i class="mr-1 fas fa-check mr-2"></i>
                Is IME Verified?
            </label>
            <div>

                <table id="IMEIRow">
                    <tr data-rownumber="1">
                        <td>
                            <input name="ItemRowNumber" type="hidden" />
                            <input name="Code" type="text" class="form-control" value="" /></td>
                        <td class="name">
                            <input name="ItemName" class="form-control" type="text" value="" /></td>
                        <td>
                            <input class="form-control" name="Qty" type="text" value="" /></td>
                        <td>
                            <input name="Rate" class="form-control calcu" type="text" value="" /></td>
                        <td>
                            <input class="form-control" name="Amount" type="text" value="" /></td>
                        <td>
                            <input class="form-control" type="text" value="" name="ItemDis" /></td>
                        <td>
                            <input name="PerDis" class="form-control calcu" type="text" value="" /></td>
                        <td>
                            <input name="DealDis" class="form-control" type="text" value="" /></td>
                        <td>
                            <input class="form-control" type="text" value="" name="NetAmount" /></td>
                        <td>
                            <input class="form-control" type="text" value="" name="PurAmount" /></td>
                        <td>
                            <input class="form-control" type="text" style="width: 200px; text-align: left" name="ImeiTxbx" placeholder="Enter IME" />
                        </td>
                    </tr>
                </table>
            </div>


            <div class="imei-table">
                <a class="btn btn-bm w-auto" onclick="reArrangeIMETabeRows()"><i class="mr-1 fas fa-list-ol"></i>Re-Arrange Table</a>
                <div class="businessManagerSellSection businessManager BMtable">


                    <div class="recommendedSection allItemsView">
                        <div class="itemsHeader">
                            <div class="three pl-0 phCol">
                                <a href="javascript:void(0)">S#
                                </a>
                            </div>
                            <div class="four  phCol">
                                <a href="javascript:void(0)">Code
                                </a>
                            </div>
                            <div class="fifteen  phCol">
                                <a href="javascript:void(0)">Description
                                </a>
                            </div>
                            <div class="thirteen phCol">
                                <a href="javascript:void(0)">IMEI
                                </a>
                            </div>
                            <div class="five phCol">
                                <a href="javascript:void(0)">Qty
                                </a>
                            </div>
                            <div class="eight phCol">
                                <a href="javascript:void(0)">Rate
											
                                </a>
                            </div>
                            <div class="eight phCol">
                                <a href="javascript:void(0)">G. Amount
											
                                </a>
                            </div>
                            <div class="eight phCol">
                                <a href="javascript:void(0)">Item D
											
                                </a>
                            </div>
                            <div class="six  phCol">
                                <a href="javascript:void(0)">Dis.%
											
                                </a>
                            </div>
                            <div class="eight phCol">
                                <a href="javascript:void(0)">Deal <span>Rs.</span>

                                </a>
                            </div>
                            <div class="eight phCol">
                                <a href="javascript:void(0)">N<span>et</span> Amount
											
                                </a>
                            </div>
                            <div class="eight phCol">
                                <a href="javascript:void(0)">Ac<span>tual </span>Cost
											
                                </a>
                            </div>
                            <div class="deleteRow three phCol">
                                <a href="javascript:void(0)"></a>
                            </div>
                            <div class="deleteRow three phCol">
                                <a href="javascript:void(0)"></a>
                            </div>
                        </div>

                        <div class="itemsSection">
                            <div class="imei-body">
                                <table class="TFtable" id="IMEITable">

                                    <tbody>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                        <div class="itemsFooter">
                            <div class='itemRow newRow'>
                                <div class='three'><span></span></div>
                                <div class='four'><span></span></div>
                                <div class='fifteen  '><span></span></div>
                                <div class='thirteen '><span></span></div>

                                <div class='five '>
                                    <input disabled="disabled" class="form-control" name='QtyTotal' />
                                </div>
                                <div class='eight '><span></span></div>
                                <div class='eight '>
                                    <input disabled="disabled" class="form-control" name='AmountTotal' />
                                </div>
                                <div class='eight '>
                                    <input disabled="disabled" class="form-control" name='ItemDisTotal' />
                                </div>
                                <div class='six'><span></span></div>
                                <div class='eight '>
                                    <input disabled="disabled" class="form-control" name='DealDisTotal' />
                                </div>
                                <div class='eight '>
                                    <input disabled="disabled" class="form-control" name='NetAmountTotal' />
                                </div>
                                <div class='eight '>
                                    <input disabled="disabled" class="form-control" name='PurAmountTotal' />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


        </div>

    </div>

    <div style="display: none">


        <table id="Table_OldData">

            <tbody>
            </tbody>

        </table>

    </div>
    
    
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="ScriptsContentPlaceHolder" runat="server">
    <script src="/Script/PartyRegistration.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>

    <script type="text/javascript">

        $(document).ready(function () {



             appendAttribute.init("", "IncomeAccount");
             appendAttribute.init("", "COGSAccount");


        });
    </script>

    <script src="../Script/PurchaseReturn/InsertDataInPurchaseReturn.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="../Script/PurchaseReturn/calculationPurchaseReturn.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="../Script/PurchaseReturnEdit/Save.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    

    <script type="text/javascript">

        
        $(document).ready(function () {
            $(".page-name").text("Edit Purchase Return");
            $("#app").addClass("hide");
            resizeTable();
             appendAttribute.init("salesManDD", "SaleManList");
            var invCode = getQueryString("InvNo");
            if (invCode != "" && invCode != null) {

                GetInvoiceByInvCode(invCode);
                $(".InvoiceNumber").val(invCode);
           } else {
               invCode= $(".InvoiceNumber").val();
                 GetInvoiceByInvCode(invCode);
            }
        });

        $(document).on('input', '.calcu', function () {
           calculationSale(this);
        })
        $(".InvoiceNumber").on("keypress", function (e) {
            if (e.keyCode == 13) {
                GetInvoice();
            }
        });
        function LoadInvoice(InvoiceData) {
            var date = $(".Date").val();

            clearInvoice();
            //$(".InvoiceNumber").val(BaseModel.d.LastInvoiceNumber);
            $(".Date").val(date);




            var ClientData = InvoiceData.ClientData;
            var Products = InvoiceData.Products;
            var TotalData = InvoiceData.TotalData;
            var IMEsList = InvoiceData.IMEItems;
            var InvoiceNumbers = InvoiceData.NextPreviousInvoiceNumbers;

            var Date = ClientData.Date;
            var PartyName = ClientData.Name;
            var PartyCode = ClientData.Code;
            var Address = ClientData.Address;
            var Particular = ClientData.Particular;
            var NorBalance = ClientData.NorBalance;
            var DealApplyNo = ClientData.DealApplyNo;
            var Phone = ClientData.PhoneNumber;
            var CreditLimit = ClientData.CreditLimit;
            var PrevBalance = ClientData.Balance;
            var StationName = ClientData.StationName;
            var StationId = ClientData.StationId;

            $("#BillStationId").val(StationId);

            $("header .StationOfBill").text("Station of Bill: " + StationName);

            $("#currentInvoiceNumber").val(InvoiceNumbers.CurrentInvoice);
            $("#prevInvoiceNumber").val(InvoiceNumbers.PreviousInvoice);
            $("#nextInvoiceNumber").val(InvoiceNumbers.NextInvoice);

            //Disable Previous and Next Button

            if ($("#prevInvoiceNumber").val().trim() === "") {
                $("#previousInvoice").addClass("disableClick");
            }
            else {
                $("#previousInvoice").removeClass("disableClick");
            }


            if ($("#nextInvoiceNumber").val().trim() === "") {
                $("#nextInvoice").addClass("disableClick");
            }
            else {
                $("#nextInvoice").removeClass("disableClick");
            }
            $(".Date").val(Date);
            $(".PartyName").val(PartyName);
            $(".PartyCode").val(PartyCode);
            $(".PartyAddress").val(Address);
            $(".PartyParticular").val(Particular);
            $(".PartyNorBalance").text(NorBalance);
            $(".PartyDealApplyNo").text(DealApplyNo);
            $(".PartyNorBalance").val(NorBalance);
            $(".PartyDealApplyNo").val(DealApplyNo);
            $(".PartyBalance").val(PrevBalance);
            $(".PartyPhoneNumber").val(Phone);
            $(".PartyCreditLimit").val(CreditLimit);


            $("#myTable tbody tr").remove();
            $("#Table_OldData tbody tr").remove();
            var stationIds = "";
            var selectedStationIds = "";
            for (var i = 0; i < Products.length; i++) {

                var Sno = i + 1;
                var ItemCode = Products[i].Code;
                var ItemName = Products[i].ItemName;
                var Qty = Products[i].Qty;
                var Rate = Products[i].Rate;

                var Amount = Products[i].Amount;
                var ItemDis = Products[i].ItemDis;
                var PerDis = Products[i].PerDis;
                var DealDis = Products[i].DealDis;

                var NetAmount = (Number(Amount) - Number(ItemDis) - Number(DealDis));
                var PurAmount = Products[i].PurAmount;
                var avrgCost = Products[i].AverageCost;
                var revCode = Products[i].RevenueCode;
                var cgsCode = Products[i].CGSCode;
                var stationId = Products[i].ItemStationId;
                insertPRRow(Sno, ItemCode, ItemName, Qty, Rate, Amount, ItemDis, PerDis, DealDis, NetAmount, PurAmount, avrgCost, revCode, cgsCode);

                var oldrows = "<tr id='Row_" + Sno + "' data-rownumber='" + Sno + "'><td class='SNoOld'>" + Sno + "</td><td><input  class='" + Sno + "_CodeOld' name='CodeOld' type='text' value='" + ItemCode + "'  /></td><td class='name'><input  class='" + Sno + "_ItemName' name='ItemName' type='text' value='" + ItemName + "'  /></td><td><input  class='" + Sno + "_QtyOld' name='QtyOld' type='text' value='" + Qty + "'  /></td><td><input  class='" + Sno + "_StationIdOld' name='StationIdOld' type='text' value='" + stationId + "'  /></td></tr>";
                $(oldrows).appendTo("#Table_OldData tbody");
                stationIds += Sno + "_StationId,";
                selectedStationIds += stationId + ",";
            }
                 appendAttribute.init(stationIds, "Station", selectedStationIds);

            var FlatPer = TotalData.FlatPer;
            var Recieved = TotalData.Recieved;

            $(".FlatPer").val(FlatPer);
            $(".Recieved").val(Recieved);


            calculationSale();

            $("#IMEITable tbody tr").remove();
            if (IMEsList.length > 0) {
                $(".IMEPortion").fadeIn();

            } else
                $(".IMEPortion").fadeOut();

            for (var i = 0; i < IMEsList.length; i++) {
                var ItemCode = IMEsList[i].Code;
                   var Rate = IMEsList[i].Rate;
                var PerDis = IMEsList[i].PerDis;
                var DealDis = IMEsList[i].DealDis;
                var item = getItem(ItemCode, Products, Rate, PerDis, DealDis);

                if (item != undefined && item != null) {
                    var Sno = i + 1;

                    var ItemName = item.ItemName;
                    var IME = IMEsList[i].IMEI;
                    var Qty = 1;
                    var Rate = item.Rate;

                    var Amount = item.Amount;
                    var ItemDis = item.ItemDis;
                    var PerDis = item.PerDis;
                    var DealDis = item.DealDis;


                    var NetAmount = (Number(Amount) - Number(ItemDis) - Number(DealDis));

                    var averageCost = item.AverageCost;
                    averageCost = isNaN(averageCost) ? 0 : Number(averageCost);
                    if (averageCost == 0) {
                        averageCost = Rate;
                    }

                    var PurAmount = (averageCost * Number(item.Qty)).toFixed(2);



                    //var rows = "<tr data-itemcode='" + ItemCode + "' id='Row_" + Sno + "' data-rownumber='" + Sno + "'><td class='SNo'>" + Sno + "</td><td><input  class='" + Sno + "_Code' name='Code' type='text' value='" + ItemCode + "'  /></td><td class='name'><input  class='" + Sno + "_ItemName' name='ItemName' type='text' value='" + ItemName + "'  /></td><td><input  class='" + Sno + "_IMEI IMEI' name='IMEI' type='text' value='" + IME + "'  /></td><td><input  class='" + Sno + "_Qty calcu' data-code='" + ItemCode + "'  name='Qty' type='text' value='" + Qty + "'  /></td><td><input name='Rate'  class='" + Sno + "_Rate calcu'  type='text' value='" + Rate + "' /></td><td><input class='" + Sno + "_Amount' name='Amount'  type='text' value='" + Amount + "' /></td><td><input class='" + Sno + "_ItemDis' type='text' value='" + ItemDis + "' name='ItemDis' /></td><td><input  name='PerDis'  class='" + Sno + "_PerDis calcu' type='text' value='" + PerDis + "' /></td><td><input name='DealDis'  class='" + Sno + "_DealDis' type='text' value='" + DealDis + "' /></td><td><input class='" + Sno + "_NetAmount' type='text' value='" + NetAmount + "' name='NetAmount' /></td><td><input class='" + Sno + "_PurAmount' type='text' value='" + PurAmount + "' name='PurAmount' /></td>  <td><i class='fas fa-times'></i> </td></tr>";
                    var rows = createIMERow(item.ItemRowNumber, Sno, ItemCode, ItemName, IME, Qty,
                        Rate, Amount, ItemDis, PerDis, DealDis, NetAmount, PurAmount);
                    $(rows).appendTo("#IMEITable tbody");

                }

            }
            
            calculationSale();
            reArrangeIMETabeRows();
            rerenderIMEITableSerialNumber();



        }

        function getItem(Code, Products, Rate, PerDis, DealDis) {
            var item;
            for (var i = 0; i < Products.length; i++) {
                if (Products[i].Code == Code && Products[i].Rate == Rate && Products[i].PerDis == PerDis  && Products[i].DealDis == DealDis) {
                    item = Products[i];
                    var rowNumer = itemAlreadyExist(Products[i].Code, Products[i].Rate, Products[i].PerDis, Products[i].DealDis);
                    item.ItemRowNumber = rowNumer;
                }
            }
            return item;
        }

        function GetInvoice() {
            var InvCode = $(".InvoiceNumber").val().trim();
            if (InvCode != "") {
                $(".getInvoiceBtn i").addClass("fa-spin");

                GetInvoiceByInvCode(InvCode)
            }
            else {
                swal("", "Enter Invoice Number", "error");
            }




        }
        function GetInvoiceByInvCode(InvCode) {
            $(".getInvoiceBtn i").addClass("fa-spin");

            $.ajax({
                url: '/WebPOSService.asmx/GetPurchaseReturnInvoiceData',
                type: "Post",

                data: { "InvoiceNo": InvCode },


                success: function (InvoiceData) {
                    if (InvoiceData.Success) {
                        $(".getInvoiceBtn i").removeClass("fa-spin");
                        LoadInvoice(InvoiceData);
                        calculationSale();
                    } else {
                        $(".getInvoiceBtn i").removeClass("fa-spin");
                        swal("Error", InvoiceData.Message, "error");
                    }


                },
                fail: function (jqXhr, exception) {
                    $(".getInvoiceBtn i").removeClass("fa-spin");
                }
            });


        }


        $("#previousInvoice").on("click", function (e) {


            var invoiceNumber = $("#prevInvoiceNumber").val();
            if ($(".InvoiceNumber").val().trim() === "" && $("#prevInvoiceNumber").val().trim() !== "") {
                invoiceNumber = $("#currentInvoiceNumber").val();
            }
            clearInvoice();

            $(".InvoiceNumber").val(invoiceNumber);
            GetInvoiceByInvCode(invoiceNumber);

        });
        $("#nextInvoice").on("click", function (e) {

            var invoiceNumber = $("#nextInvoiceNumber").val();
            if ($(".InvoiceNumber").val().trim() === "" && $("#nextInvoiceNumber").val().trim() !== "") {
                invoiceNumber = $("#currentInvoiceNumber").val();
            }
            clearInvoice();
            $(".InvoiceNumber").val(invoiceNumber);
            GetInvoiceByInvCode(invoiceNumber);

        });
    </script>
</asp:Content>
