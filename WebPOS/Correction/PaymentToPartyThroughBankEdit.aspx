﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="PaymentToPartyThroughBankEdit.aspx.cs" Inherits="WebPOS.PaymentToPartyThroughBankEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <input type="hidden" id="isLastVoucher" />
    <div class="container-fluid">
        <div class="mt-4">

            <section class="form">
                <h2 class="form-header fa-edit">Edit Bank Payment To Party</h2>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-4 display-flex">
                        <span class="input input--hoshi">
                            <input id="txtVoucherNo" class="VoucherNo input__field input__field--hoshi" autocomplete="off" type="text" />

                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Voucher</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 d-flex">
                        <span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100" id="PreviousBtn" onclick="GetPreviousVoucherData()" data-toggle="tooltip" title="Previous Voucher"><i class="fas fa-chevron-circle-left"></i></a>
                        </span><span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100" data-toggle="tooltip" title="Get Voucher" onclick="GetVoucherData()"><i class="fas fa-spinner"></i></a>
                        </span>
                        <span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100" id="NextBtn" onclick="GetNextVoucherData()" data-toggle="tooltip" title="Next Voucher"><i class="fas fa-chevron-circle-right"></i></a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <input id="txtCurrentDate" class="datetimepicker Date input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Date</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                        <span class="input input--hoshi">

                            <input id="expenceTextbox" data-type="ExpenceHead" data-id="expenceTextbox" data-function="GLList" data-nextfocus=".PartyName" data-glcode="01010102" class=" autocomplete ExpenceHeadTitle BankTitle empt input__field input__field--hoshi" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Bank</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                        <span class="input input--hoshi">

                            <input id="txtCode" class="input__field input__field--hoshi ExpenceHeadCode empt BankCode" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Bank Code</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                        <span class="input input--hoshi">

                            <input id="txtChequeNo" class="input__field input__field--hoshi CheqNo empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Cheque #</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3">
                        <span class="input input--hoshi">

                            <input id="txtAccountNo" class="input__field input__field--hoshi ExpenceHeadAccountNo AccountNo empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Account #</span>
                            </label>
                        </span>
                    </div>
                </div>
                <hr />
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtPartyName" data-type="Party" data-id="txtPartyName" data-nextfocus=".CashPaid" data-function="GetParty" name="PartyName" class=" PartyBox PartyName autocomplete empt input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Party Name</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtPartyCode" disabled="disabled" name="PartyCode" class="input__field input__field--hoshi PartyCode empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Party Code</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <span class="input input--hoshi">

                            <input id="txtAddress" class="input__field input__field--hoshi PartyAddress empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Address</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtCashPaid" class="input__field input__field--hoshi CashPaid empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Cash Paid</span>
                            </label>
                        </span>
                    </div>

                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtBalance" class="input__field input__field--hoshi PartyBalance empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Balance</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <span class="input input--hoshi">

                            <input id="txtNarration" class="input__field input__field--hoshi Narration empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Narration</span>
                            </label>
                        </span>
                    </div>

                </div>

                <hr />

                <div class="row  justify-content-end">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a onclick="deleteVoucher()" class=" btn btn-3 btn-bm btn-3e fa-trash w-100">Delete</a>
                        </span>

                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="SaveBefore()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                        </span>
                    </div>
                </div>

            </section>
        </div>
    </div>

</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">

    <script src="../Script/Autocomplete.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>

    <script src="../Script/Voucher/PaymentToPartyBankEdit_Save.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>


    <script type="text/javascript">

        $(document).ready(function () {
            var InvoiceNumber = getQueryString("v");

            if (InvoiceNumber != "" || InvoiceNumber != null) {
                $(".VoucherNo").val(InvoiceNumber);
                GetVoucherData();
            }
            $("#txtChequeNo").keypress(function (event) {
                if (event.which == 13) {
                    event.preventDefault();
                    $("#txtAccountNo").focus();
                }
            });
            $("#txtAccountNo").keypress(function (event) {
                if (event.which == 13) {
                    event.preventDefault();
                    $("#txtPartyName").focus();
                }
            });

            

        });





    </script>

</asp:Content>
