﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using WebPOS.Model;
namespace WebPOS
{
    public partial class PartyRegistrationEditOld : System.Web.UI.Page
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        ModGLCode objModGLCode = new ModGLCode();
        ModItem objModItem = new ModItem();
        Module1 objModule1 = new Module1();
        Module7 objModule7 = new Module7();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {





            }
        }



        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(PartyModel PartyModel)
        {

            var CompID = "01";
            try
            {
                Module1 objModule1 = new Module1();
                Module7 objModule7 = new Module7();
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();
                int NorBalance = Convert.ToInt16(PartyModel.NorBalance);
                string PartyCode = PartyModel.PartyCode;
                var PartyName = PartyModel.PartyName;
                var ContactPerson = PartyModel.ContactPerson;
                var MobileNo = PartyModel.MobileNo;
                var FaxNo = PartyModel.FaxNo;
                var LandLineNo = PartyModel.LandLineNo;
                var Address = PartyModel.Address;
                var Email = PartyModel.Email;
                int lstSellingPriceNo = Convert.ToInt16(PartyModel.lstSellingPriceNo);
                int lstDealApplyNo = Convert.ToInt16(PartyModel.lstDealApplyNo);
                decimal CreditLimit = Convert.ToDecimal(PartyModel.CreditLimit);
                int chkCreditLimitApply = Convert.ToInt16(PartyModel.CreditLimitApply);
                var OtherInfo = PartyModel.OtherInfo;
                int lstSaleMan = Convert.ToInt16(PartyModel.lstSaleMan);
                int chkDeal = Convert.ToInt16(PartyModel.chkDeal);
                int chkPerDiscount = Convert.ToInt16(PartyModel.chkPerDiscount);

                var message = "Party Updated Successfully.";
                SqlCommand cmd = new SqlCommand("Update PartyCode set GroupID=" + 1 + ",DealApplyNo=" + lstDealApplyNo + ",DisPer=" + chkPerDiscount + ",  DealRs=" + chkDeal + ", SalManCode=" + lstSaleMan + ",OtherInfo='" + OtherInfo + " ', SellingPrice=" + lstSellingPriceNo + ",  CreditLimit=" + CreditLimit + ",LimitApplicable=" + chkCreditLimitApply + ", Name='" + PartyName + "', OwnerName='" + ContactPerson + "', Address='" + Address + "', Email='" + Email + "', Phone='" + LandLineNo + "', Fax='" + FaxNo + "', Mobile='" + MobileNo + "', NorBalance=" + NorBalance + " where CompID='" + CompID + "' and Code='" + PartyCode + "'", con, tran);
                // SqlCommand cmd = new SqlCommand("Update PartyCode set OwnerName='" + ContactPerson + "',NorBalance=" + NorBalance + " where CompID='" + CompID + "' and Code='0101010300003'", con,tran);
                cmd.ExecuteNonQuery();

                SqlCommand cmd2 = new SqlCommand("Update GLCode set Title='" + PartyName + "' where  CompID='" + CompID + "' and Code='" + PartyCode + "'", con, tran);
                cmd2.ExecuteNonQuery();




                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel deleteParty(PartyModel PartyModel)
        {
            ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
            var CompID = "01";
            try
            {


                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();
                var message = "";
                string PartyCode = PartyModel.PartyCode;

                var TransactionExist = objModPartyCodeAgainstName.TransactionExistAgainstGLCode(PartyCode);

                if (TransactionExist == "No")
                {
                    message = "Party deleted Successfully.";
                    SqlCommand cmdDel1 = new SqlCommand("Delete  from GLCode where  CompID='" + CompID + "' and code='" + PartyCode + "'", con, tran);
                    cmdDel1.ExecuteNonQuery();

                    SqlCommand cmdDel2 = new SqlCommand("Delete  from GLOpeningBalance where  CompID='" + CompID + "' and code='" + PartyCode + "'", con, tran);
                    cmdDel2.ExecuteNonQuery();

                    SqlCommand cmdDel3 = new SqlCommand("Delete  from PartyCode where  CompID='" + CompID + "' and code='" + PartyCode + "'", con, tran);
                    cmdDel3.ExecuteNonQuery();

                    SqlCommand cmdDel4 = new SqlCommand("Delete  from PartyOpBalance where  CompID='" + CompID + "' and code='" + PartyCode + "'", con, tran);
                    cmdDel4.ExecuteNonQuery();

                    SqlCommand cmdDel5 = new SqlCommand("Delete  from PartyGPCode where  Code='" + PartyCode + "'", con);
                    cmdDel5.ExecuteNonQuery();
                    tran.Commit();
                    con.Close();

                }
                else
                {
                    message = "Transaction Exist";
                }


                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }


    }
}