﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="CashPaymentVoucherEdit.aspx.cs" Inherits="WebPOS.Correction.CashPaymentVoucherEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container mb-3">
        <div class="mt-4">

            <section class="form" id="form-section">
                <h2 class="form-header fa-money-bill-alt">Cash Payment Voucher</h2>
                <div class="row justify-content-center">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-3">
                        <span class="input input--hoshi">
                            <input id="txtVoucherNo" class="VoucherNumber VoucherNo empty1 input__field input__field--hoshi" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi"><i class="fas fa-file"></i> Voucher Number</span>
                            </label>
                        </span>
                    </div>
                   <div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 d-flex" >
                            <span class="input input--hoshi">
                                <a class="btn btn-3 btn-bm btn-3e w-100" id="PrintBtn" data-toggle="tooltip" title="Print"><i class="fas fa-print text-white"></i></a>
                            </span>
                        <span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100" id="PreviousBtn" onclick="GetPreviousVoucherData()" data-toggle="tooltip" title="Previous Voucher"><i class="fas fa-chevron-circle-left"></i></a>
                        </span><span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100"  data-toggle="tooltip" title="Get Voucher" onclick="GetVoucherData()"><i class="fas fa-spinner"></i></a>
                        </span>
                        <span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100" id="NextBtn" onclick="GetNextVoucherData()" data-toggle="tooltip" title="Next Voucher"><i class="fas fa-chevron-circle-right"></i></a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input class="VoucherDate input__field input__field--hoshi empty1 datetimepicker Date" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi"><i class="fas fa-calendar-day"></i> Date</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input data-id="GLList" data-type="AllVouchers" data-function="GLList" data-glcode="010402" class="empty1 input__field input__field--hoshi autocomplete AllVouchersTitle descritionTxbx" data-callback="insertCashPaymentVoucherRow" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi"><i class="fas fa-search"></i> Enter Description</span>
                            </label>
                        </span>
                    </div>

                </div>

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100 fa-sync" id="btnReset" onclick="location.href=location.href">Reset </a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="save()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                        </span>
                    </div>
                </div>

                <div class="debit-table">
                    <div class="card-header bordered">
                        <div class="header-block">
                            <h3 class="title">Debit </h3>
                        </div>
                    </div>
                    <ul class="item-list striped narrow">
                        <li class="item item-list-header">
                            <div class="item-row  pl-3 pr-4">

                                <div class="item-col item-col-header flex-4">
                                    <div>
                                        <span>GL Code</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header flex-5">
                                    <div>
                                        <span>Description</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header flex-4">
                                    <div>
                                        <span>Narration</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header flex-4">
                                    <div>
                                        <span>Amount Debit</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header item-col-date flex-3">
                                </div>
                            </div>
                        </li>

                    </ul>

                    <ul class="item-list data-list striped narrow h-10 overflow-scroll-y">
                    </ul>
                    <ul class="item-list footer-data narrow striped">
                        <li class="item item-list-header">
                            <div class="item-row pl-3 pr-4">

                                <div class="item-col item-col-header flex-4">
                                </div>
                                <div class="item-col item-col-header flex-5">
                                </div>
                                <div class="item-col item-col-header flex-4">
                                </div>
                                <div class="item-col item-col-header flex-4 ">
                                    <div>
                                        <input class="text-right px-2 py-0 form-control" disabled="disabled" value="0" name='totalDebit' />
                                    </div>
                                </div>
                                <div class="item-col item-col-header item-col-date flex-3">
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="credit-table">
                    <div class="card-header bordered">
                        <div class="header-block">
                            <h3 class="title">Credit </h3>
                        </div>
                    </div>
                    <ul class="item-list striped narrow">
                        <li class="item item-list-header">
                            <div class="item-row  px-3">

                                <div class="item-col item-col-header flex-4">
                                    <div>
                                        <span>GL Code</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header flex-5">
                                    <div>
                                        <span>Description</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header flex-4">
                                    <div>
                                        <span>Narration</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header flex-4">
                                    <div>
                                        <span>Amount Credit</span>
                                    </div>
                                </div>
                               
                            </div>
                        </li>

                    </ul>

                    <ul class="item-list data-list striped narrow">
                        <li class="item item-data">
                            <div class="item-row  px-3">
                                <div class="item-col flex-4">
                                    <div class="item-heading">Code</div>
                                    <div>
                                        <input type='text' class="CreditGLCode form-control" name="CreditGLCode" disabled />
                                    </div>
                                </div>
                                <div class="item-col flex-5 no-overflow">

                                    <div>
                                        <input type='text' class="CreditGLTitle form-control autocomplete"
                                            name="CreditGLTitle" data-placement="top"  data-id="GLList1" 
                                            data-type="CreditGL" data-nextfocus="[name=CreditNarration]" data-function="GLList"
                                            data-glcode="01010101"  />
                                    </div>


                                </div>
                                <div class="item-col flex-4 no-overflow">
                                    <div>
                                        <input type='text' class="form-control CreditNarration" name='CreditNarration' data-nextfocus="[name=Credit]" />
                                    </div>
                                </div>
                                <div class="item-col flex-4 no-overflow">
                                    <div>
                                        <input name='Credit' type='number' class="text-right px-2 form-control CreditAmount" />
                                        <input name='totalCredit' type='hidden' class="text-right px-2" />
                                    </div>
                                </div>

                            </div>
                        </li>
                    </ul>
                </div>
            </section>
        </div>

    </div>
    
</asp:Content>
<asp:Content ContentPlaceHolderID="Scripts" runat="server">
    <script src="/Script/Autocomplete.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="/Script/Voucher/CashPaymentVoucher_Edit.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
</asp:Content>
