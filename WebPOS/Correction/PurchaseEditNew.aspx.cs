﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Correction
{
    public partial class PurchaseEditNew : System.Web.UI.Page
    {
        Int32 OperatorID = 0;
        static string CompID = "01";
        static string UserSession = "001";

        static string DiscountOnPurchaseGLCode = "0101010600001";
        static string CashAccountGLCode = "0101010100001";
        static string PurchaseGLCode = "0101010600001";

        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;

        ModItem objModItem = new ModItem();
        Module1 objModule1 = new Module1();
        ModViewInventory objModViewInventory = new ModViewInventory();

        Module4 objModule4 = new Module4();
        ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
      
        protected void Page_Load(object sender, EventArgs e)
        {
            var lastInvNo = objModule1.LastInvNoPurchase();
            txtInvoiceNo.Text = lastInvNo;
            ViewState["PageName"] = "Purchase Edit";
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(SaveProductViewModel saveProductViewModel)
        {
            try
            {
                var stationId = string.Empty;
                if (HttpContext.Current.Session["StationId"] != null)
                {
                    stationId = saveProductViewModel.ClientData.StationId;
                }
                if (stationId != string.Empty)
                {
                
					var objModule1 = new Module1();
                    var objModule4 = new Module4();
                    var modQtyOfInventory = new ModQtyOfInventory();
                    ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
                    if (con.State==ConnectionState.Closed) { con.Open(); }
                    tran = con.BeginTransaction();

                    var invoiceNumber = saveProductViewModel.ClientData.InvoiceNumber;


                    SqlCommand cmdDel1 = new SqlCommand("Delete  from PartiesLedger where  CompID='" + CompID + "' and BillNo='" + invoiceNumber + "' and DescriptionOfBillNo='Purchase'", con, tran);
                    cmdDel1.ExecuteNonQuery();
                    SqlCommand cmdDel2 = new SqlCommand("Delete  from GeneralLedger where  CompID='" + CompID + "' and BillNo='" + invoiceNumber + "' and DescriptionOfBillNo='Purchase'", con, tran);
                    cmdDel2.ExecuteNonQuery();
                    SqlCommand cmdDel3 = new SqlCommand("Delete  from Inventory where  CompID='" + CompID + "' and BillNo='" + invoiceNumber + "' and DescriptionOfBillNo='Purchase'", con, tran);
                    cmdDel3.ExecuteNonQuery();

                    SqlCommand cmdDel4 = new SqlCommand("Delete  from Purchase1 where  CompID='" + CompID + "' and InvNo='" + invoiceNumber + "'", con, tran);
                    cmdDel4.ExecuteNonQuery();
                    SqlCommand cmdDel5 = new SqlCommand("Delete  from Purchase2 where  CompID='" + CompID + "' and InvNo='" + invoiceNumber + "'", con, tran);
                    cmdDel5.ExecuteNonQuery();
                    SqlCommand cmdDel6 = new SqlCommand("Delete  from Purchase3 where  CompID='" + CompID + "' and InvNo='" + invoiceNumber + "'", con, tran);
                    cmdDel6.ExecuteNonQuery();
                    SqlCommand cmdDel6IME = new SqlCommand("Delete  from InventorySerialNoFinal where  CompID='" + CompID + "' and Description='Purchase' and VoucherNo='" + invoiceNumber + "'", con, tran);
                    cmdDel6IME.ExecuteNonQuery();


                    foreach (var itemsOld in saveProductViewModel.ProductsOld)
                    {
                        modQtyOfInventory.QtyAvailableOne(itemsOld.Code, tran, con);

                        var CodeOld = Convert.ToDecimal(itemsOld.Code);
                        var Quantity = Convert.ToDecimal(itemsOld.Qty);
                        var ItemStationId = Convert.ToInt32(itemsOld.ItemStationId);
                        
                        objModule4.ClosingBalanceNewTechniqueStationIssue(CodeOld, Quantity, ItemStationId, tran, con);
                        
                        objModule4.ClosingBalanceNewTechniqueIssue(CodeOld, Quantity, tran, con);

                    }


                    //if (lstSaleMan.Text != "") {SaleManID = 1; //objModPartyCodeAgainstName.SaleManCodeAgainstSaleManName(lstSaleMan.Text); }


                    var PartyName = saveProductViewModel.ClientData.Name.ToString();
                    var SaleManName = saveProductViewModel.ClientData.SalesManName;
                    var SaleManID = saveProductViewModel.ClientData.SalesManId;
                    var selectedDate = saveProductViewModel.ClientData.Date;
                    var convertedDate = DateTime.ParseExact(selectedDate + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                         System.Globalization.CultureInfo.InvariantCulture);
                    var InvoiceDate = convertedDate.ToString("yyyy-MM-dd");
                    var PartyCode = saveProductViewModel.ClientData.Code.ToString();
                    var Particular = saveProductViewModel.ClientData.Particular.ToString();
                    var Addresss = saveProductViewModel.ClientData.Address.ToString();

                    var ManualInvNo = saveProductViewModel.ClientData.ManInv.ToString();
                    var PartyPhone = saveProductViewModel.ClientData.PhoneNumber.ToString();
                    var SysTime = InvoiceDate + " " + DateTime.Now.ToString("HH:mm:ss");

                    var NorBalance = Convert.ToInt32(saveProductViewModel.ClientData.NorBalance);
                    SqlCommand cmd = new SqlCommand("insert into Purchase1 (InvNo,Vender,dat,VenderCode,Particular,Address,CompID,Phone,StationId) values('" + invoiceNumber + "','" + PartyName + "','" + InvoiceDate + "','" + PartyCode + "','" + Particular + "','" + Addresss + "','" + CompID + "','" + PartyPhone + "'," + stationId + ")", con, tran);
                    cmd.ExecuteNonQuery();



                    //----------------------------------------------------------------------Purchase 2
                    foreach (var item in saveProductViewModel.Products)
                    {
                        var code = item.Code;
                        var SNO = Convert.ToInt16(item.SNo);
                        var Code = Convert.ToString(item.Code);
                        var Description = Convert.ToString(item.ItemName);
                        var Quantity = Convert.ToDecimal(item.Qty);
                        var Rate = Convert.ToDecimal(item.Rate);
                        var Amount = Convert.ToDecimal(item.Amount);
                        var ItemDiscount = Convert.ToDecimal(item.ItemDis);
                        var PerDiscount = Convert.ToDecimal(item.PerDis);
                        var DealRs = Convert.ToDecimal(item.DealDis);
                        var NetAmount = Convert.ToDecimal(item.NetAmount);
                        var PurAmount = Convert.ToDecimal(item.PurAmount);
                        var SellingPrice = Convert.ToDecimal(item.SellingPrice);
                        var Descrip = "Sold Inv # " + invoiceNumber + ",Party-" + PartyName;
                        var StationIDLocal = Convert.ToInt32(item.ItemStationId);

                        SqlCommand cmd3 = new SqlCommand("insert into Purchase2 (InvNo,SNO,Code,Description,Qty,cost,amount,VenderCode,Date1,System_Date_Time,Balance_Qty,DiscountPercentageRate,Item_Discount,CompID,CostAfterDiscount,DealRs,StationId) values('" + invoiceNumber + "'," + Convert.ToInt16(SNO) + ",'" + Code.Trim() + "','" + Description + "'," + Quantity + "," + Rate + "," + Amount + ",'" + PartyCode + "','" + InvoiceDate + "','" + SysTime + "'," + Quantity + "," + PerDiscount + "," + ItemDiscount + ",'" + CompID + "'," + PurAmount + "," + DealRs + "," + StationIDLocal + ")", con, tran);
                        cmd3.ExecuteNonQuery();

                        SqlCommand cmd5 = new SqlCommand("insert into Inventory (ICode,Description,Received,Dat,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,System_Date_Time,CompID,StationId) values('" + Code + "','" + "Purchased Bill # " + invoiceNumber + ",Party-" + PartyName + "'," + Quantity + ",'" + InvoiceDate + "','" + invoiceNumber + "','" + "Purchase" + "','" + PartyCode + "','" + InvoiceDate + "','" + SysTime + "','" + CompID + "'," + StationIDLocal + ")", con, tran);
                        cmd5.ExecuteNonQuery();

                        SqlCommand updateSellingPrice = new SqlCommand("Update InvCode Set SellingCost=" + SellingPrice + " where  Code='" + Code + "' and CompId='" + CompID + "'", con, tran);
                        updateSellingPrice.ExecuteNonQuery();

                        objModule4.ClosingBalanceNewTechniqueReceived(Convert.ToDecimal(Code), Quantity, tran, con);
                        objModule4.ClosingBalanceNewTechniqueStationReceived(Convert.ToDecimal(Code), Quantity, Convert.ToInt32(StationIDLocal), tran, con);


                        Quantity = 0;
                        Rate = 0;
                        Amount = 0;
                        ItemDiscount = 0;
                        PerDiscount = 0;
                        DealRs = 0;
                        NetAmount = 0;
                        PurAmount = 0;
                    }
                    decimal Total = 0;
                    decimal Paid = 0;
                    decimal FlatDisRs = 0;
                    decimal FlatDisRate = 0;
                    decimal Balance = 0;
                    decimal NetDiscount = 0;
                    decimal PreviousBalance = 0;
                    decimal OtherCharges = 0;




                    Total = Convert.ToDecimal(saveProductViewModel.TotalData.GrossTotal);
                    Paid = Convert.ToDecimal(saveProductViewModel.TotalData.Recieved);
                    FlatDisRs = Convert.ToDecimal(saveProductViewModel.TotalData.FlatDiscount);
                    Balance = Convert.ToDecimal(saveProductViewModel.TotalData.Balance);
                    NetDiscount = Convert.ToDecimal(saveProductViewModel.TotalData.NetDiscount);
                    PreviousBalance = Convert.ToDecimal(saveProductViewModel.TotalData.PreviousBalance);
                    FlatDisRate = Convert.ToDecimal(saveProductViewModel.TotalData.FlatPer);


                    //SqlCommand cmd4 = new SqlCommand("insert into invoice3 (InvNo,Total,CashPaid,Balance,Balance1,Discount1,Date1,TotalPcs,PrevBalance,FlatDiscount,Flat_Discount_Per,CompID,StationId) values(" + invoiceNumber + "," + Total + "," + Paid + "," + Balance + "," + Balance + "," + NetDiscount + ",'" + InvoiceDate + "','" + TotalPcs + "'," + PreviousBalance + "," + FlatDisRs + "," + FlatDisRate + ",'" + CompID + "')", con, tran);
                    SqlCommand cmd4 = new SqlCommand("insert into Purchase3(InvNo, Total, Discount, Paid, Balance, VenderCode, Balance1, Date1, FlatDiscount, Flat_Discount_Per, CompID, OtherCharges) values('" + invoiceNumber + "'," + Total + "," + NetDiscount + ", " + Paid + ", " + Balance + ",'" + PartyCode + "'," + Balance + ",'" + InvoiceDate + "'," + FlatDisRs + ", " + FlatDisRate + ", '" + CompID + "'," + OtherCharges + ")", con, tran);

                    cmd4.ExecuteNonQuery();
                    ///======================================================================IME
                    foreach (var IME in saveProductViewModel.IMEItems)
                    {
                        var SysTimeIME = InvoiceDate + " " + DateTime.Now.ToString("HH:mm:ss");
                        var SNO = Convert.ToInt16(IME.SNo);
                        var Code = Convert.ToString(IME.Code);
                        PartyName = saveProductViewModel.ClientData.Name.ToString();
                        var Description = IMEDescription.Purchase;
                        var InvNo = invoiceNumber;
                        var IMEI = IME.IMEI;
                        var Rate = IME.Rate;
                        var DisPer = IME.PerDis;
                        var DisRs = IME.ItemDis;
                        var DealRs = IME.DealDis;


                        SqlCommand cmd3IME = new SqlCommand(@"Insert into InventorySerialNoFinal (Date1 , ItemCode , Description , VoucherNo,  IME , PartyCode, Price_Cost, 
                                                                    System_Date_Time,CompID,DisRs,DisPer,DealRs ) values ('" + InvoiceDate + "'," +
                                                                Code + ",'" + Description + "','" + InvNo + "','" + IMEI + "','" +
                                                                PartyCode + "'," + Rate + ",'" + SysTimeIME + "','" + CompID + "'," + DisRs + "," +
                                                                DisPer + "," + DealRs + ")", con, tran);
                        cmd3IME.ExecuteNonQuery();
                    }
                        //////////        //=========================================================General Entry-------------------------------
                        decimal a = objModule1.MaxJVNo(con, tran);

                    var revenueAmounts = saveProductViewModel.Products.Where(e => !string.IsNullOrWhiteSpace(e.RevenueCode)).GroupBy(c => c.RevenueCode)
                        .Select(x => new GLModel()
                        {
                            Amount = x.Sum(o => Convert.ToDecimal(o.Amount)),
                            Code = x.First().RevenueCode
                        }).ToList();

                    foreach (var revenue in revenueAmounts)
                    {
                        if (revenue.Amount > 0)
                        {
                            //SqlCommand cmd7 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Narration,CompID,StationId) values('" + revenue.Code + "','" + InvoiceDate + "','" + "Receivable-Inv#" + invoiceNumber + "'," + revenue.Amount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + "Receivable-Inv#" + invoiceNumber + "','" + CompID + "')", con, tran);
                            SqlCommand cmd7 = new SqlCommand("insert into GeneralLedger(code, datedr, Description, AmountDr, V_NO, V_type, System_Date_Time, BillNo, DescriptionOfBillNo, VenderCode, PurchaseDate, Narration, CompID,System_Date_Time_Actual,StationId) values('" + revenue.Code + "','" + InvoiceDate + "','" + "Payable - Purch, Bill#" + invoiceNumber + "-" + PartyCode + "'," + revenue.Amount + "," + a + ",'" + "JV" + "','" + SysTime + "','" + invoiceNumber + "','" + "Purchase" + "','" + PartyCode + "','" + InvoiceDate + "','" + "Purchases-" + PartyName + " (Bill-" + invoiceNumber + ")" + "','" + CompID + "','" + DateTime.Now + "'," + stationId + ")", con, tran);
                            cmd7.ExecuteNonQuery();
                        }
                    }


                    //SqlCommand cmd6 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountDr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Narration,CompID,StationId) values('" + PartyCode + "','" + InvoiceDate + "','" + "Sales-Inv# " + invoiceNumber + " / " + ManualInvNo + "'," + Total + "," + a + ",'" + "JV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + "Sales-Inv# " + invoiceNumber  + "','" + CompID + "')", con, tran);
                    SqlCommand cmd6 = new SqlCommand("insert into GeneralLedger(code, datedr, Description, AmountCr, V_NO, V_type, System_Date_Time, BillNo, DescriptionOfBillNo, VenderCode, PurchaseDate, Narration, CompID, System_Date_Time_Actual,StationId) values('" + PartyCode + "','" + InvoiceDate + "','" + "Payable-Purch,Bill#" + invoiceNumber + "-" + PartyCode + "'," + Total + "," + a + ",'" + "JV" + "','" + SysTime + "','" + invoiceNumber + "','" + "Purchase" + "','" + PartyCode + "','" + InvoiceDate + "','" + "Payable-Purch,Bill#" + invoiceNumber + "-" + PartyCode + "','" + CompID + "','" + DateTime.Now + "'," + stationId + ")", con, tran);
                    cmd6.ExecuteNonQuery();



                    //SqlCommand cmd8 = new SqlCommand("insert into PartiesLedger (code,datedr,DescriptionDr,AmountDr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,CompID,StationId) values('" + PartyCode + "','" + InvoiceDate + "','" + "Sales-Inv#" + invoiceNumber + " / " + ManualInvNo + " - Ptc:" + Particular + ",TPcs:" + "'," + Total + "," + a + ",'" + "JV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + CompID + "')", con, tran);
                    SqlCommand cmd8 = new SqlCommand("insert into PartiesLedger (code,datedr,DescriptionDr,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,CompID,StationId) values('" + PartyCode + "','" + InvoiceDate + "','" + "Purchases,Bill#" + invoiceNumber.Trim() + ",Part." + Particular + "'," + Total + "," + a + ",'" + "JV" + "','" + SysTime + "','" + invoiceNumber + "','" + "Purchase" + "','" + PartyCode + "','" + InvoiceDate + "','" + CompID + "'," + stationId + ")", con, tran);
                    cmd8.ExecuteNonQuery();
                    //////////        //---------------------------------------------------------Detail of bill into party ledger---------------------------------------------
                    SysTime = Convert.ToDateTime(SysTime).AddSeconds(5).ToString();
                    foreach (var item in saveProductViewModel.Products)
                    {
                        var code = item.Code;
                        var SNO = Convert.ToInt16(item.SNo);
                        var Code = Convert.ToString(item.Code);
                        var Description = Convert.ToString(item.ItemName);
                        var Quantity = Convert.ToDecimal(item.Qty);
                        var Rate = Convert.ToDecimal(item.Rate);
                        var Amount = Convert.ToDecimal(item.Amount);
                        var DisRs = Convert.ToDecimal(item.ItemDis);
                        var DisPer = Convert.ToDecimal(item.PerDis);
                        var DealRs = Convert.ToDecimal(item.DealDis);
                        var NetAmount = Convert.ToDecimal(item.NetAmount);
                        var PurAmount = Convert.ToDecimal(item.PurAmount);
                        var Descrip = "Sold Inv # " + invoiceNumber + ",Party-" + PartyName;


                        //SqlCommand cmd10 = new SqlCommand("insert into PartiesLedger (datedr,Code,DescriptionDr,Qty,Rate,Amount,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,DisRs,DisPer,DealRS,CompID,StationId) values('" + InvoiceDate + "','" + PartyCode + "','" + Description + "','" + Quantity + "','" + Rate + "','" + Amount + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "'," + DisRs + "," + DisPer + "," + DealRs + ",'" + CompID + "')", con, tran);
                        SqlCommand cmd10 = new SqlCommand("insert into PartiesLedger (datedr,DescriptionDr,Code,Qty,Rate,Amount,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,CompID,DisRs,DisPer,DealRs,StationId) values('" + InvoiceDate + "','" + Description + "','" + PartyCode + "','" + Quantity + "','" + Rate + "','" + Amount + "','" + SysTime + "','" + invoiceNumber + "','" + "Purchase" + "','" + PartyCode + "','" + InvoiceDate + "','" + CompID + "'," + DisRs + "," + DisPer + "," + DealRs + "," + stationId + ")", con, tran);
                        cmd10.ExecuteNonQuery();
                    }

                    //////////        //'--------------------------------Discount------------------------------------------------------------------------------------------------
                    SysTime = Convert.ToDateTime(SysTime).AddSeconds(5).ToString();
                    if (NetDiscount > 0)
                    {
                        a = objModule1.MaxJVNo(con, tran);                                                                                                                                                                              // 0103020300001
                                                                                                                                                                                                                               //SqlCommand cmd11 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountDr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Narration,CompID,StationId) values('" + "0103020300001" + "','" + InvoiceDate + "','" + "Accounts Receivable" + "'," + NetDiscount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + "Accounts Receivable" + "','" + CompID + "')", con, tran);
                        SqlCommand cmd11 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountDr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Narration,CompID,StationId) values('" + PartyCode + "','" + InvoiceDate + "','" + "Discount(Purchas-Bill#" + invoiceNumber + ")" + "," + PartyName + ")" + "'," + NetDiscount + "," + a + ",'" + "JV" + "','" + SysTime + "','" + invoiceNumber + "','" + "Purchase" + "','" + PartyCode + "','" + InvoiceDate + "','" + "Discount(Purchas-Bill#" + invoiceNumber + ")" + "','" + CompID + "'," + stationId + ")", con, tran);
                        cmd11.ExecuteNonQuery();
                        //SqlCommand cmd12 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Narration,CompID,StationId) values('" + PartyCode + "','" + SysTime + "','" + "Discount Account" + "'," + NetDiscount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + "Discount Account,Inv No. " + invoiceNumber + "','" + CompID + "')", con, tran);
                        SqlCommand cmd12 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Narration,CompID,StationId) values('" + DiscountOnPurchaseGLCode + "','" + InvoiceDate + "','" + "Bill Discount(Pruchas-Bill#" + invoiceNumber + "," + PartyName + ")" + "'," + NetDiscount + "," + a + ",'" + "JV" + "','" + SysTime + "','" + invoiceNumber + "','" + "Purchase" + "','" + PartyCode + "','" + InvoiceDate + "','" + "Bill Discount (Pruchas-Bill#" + invoiceNumber + ")" + "','" + CompID + "'," + stationId + ")", con, tran);
                        cmd12.ExecuteNonQuery();

                        //SqlCommand cmd13 = new SqlCommand("insert into PartiesLedger (code,datedr,DescriptionDr,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,CompID,StationId) values('" + PartyCode + "','" + InvoiceDate + "','" + "Discount-Inv#" + invoiceNumber + "'," + NetDiscount + "," + a + ",'" + "JV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + CompID + "')", con, tran);
                        SqlCommand cmd13 = new SqlCommand("insert into PartiesLedger (code,datedr,DescriptionDr,AmountDr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,CompID,StationId) values('" + PartyCode + "','" + InvoiceDate + "','" + "Bill Discount - Bill # " + invoiceNumber + "'," + NetDiscount + "," + a + ",'" + "JV" + "','" + SysTime + "','" + invoiceNumber + "','" + "Purchase" + "','" + PartyCode + "','" + InvoiceDate + "','" + CompID + "'," + stationId + ")", con, tran);
                        cmd13.ExecuteNonQuery();


                    }
                    //////////        //'--------------------------------Cash Received-------------------------------------------------------------------------------------------------------------------------------- 103= A/R,


                    if (Paid > 0)
                    {
                        SysTime = Convert.ToDateTime(SysTime).AddSeconds(5).ToString();

                        decimal b = objModule1.MaxCRV( con, tran);

                        //SqlCommand cmd14 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountDr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Narration,Code1,CompID,StationId) values('" + "0101010100001" + "','" + InvoiceDate + "','" + "Receivable-" + PartyName + ",Inv#" + invoiceNumber + "'," + Paid + "," + b + ",'" + "CRV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + PartyName + ",Inv#" + invoiceNumber + "','" + CompID + "03010100001" + "','" + CompID + "')", con, tran);
                        SqlCommand cmd14 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountDr,V_NO,V_type,Code1,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Narration,CompID,StationId) values('" + PartyCode + "','" + InvoiceDate + "','" + "Cash-Purchase,Bill#" + invoiceNumber + ", " + PartyName + "'," + Paid + "," + a + ",'" + "CPV" + "','" + CashAccountGLCode + "','" + SysTime + "','" + invoiceNumber + "','" + "Purchase" + "','" + PartyCode + "','" + InvoiceDate + "','" + "Cash-Purchase,Bill#" + invoiceNumber + "','" + CompID + "'," + stationId + ")", con, tran);
                        cmd14.ExecuteNonQuery();

                        //SqlCommand cmd15 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Code1,Narration,CompID,StationId) values('" + PartyCode + "','" + InvoiceDate + "','" + "Cash Account-Inv#" + invoiceNumber + "'," + Paid + "," + b + ",'" + "CRV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + "0101010100001" + "','" + "Cash Account-Inv#" + invoiceNumber + "','" + CompID + "')", con, tran);
                        SqlCommand cmd15 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountCr,V_NO,V_type,Code1,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,Narration,CompID,StationId) values  ('" + CashAccountGLCode + "','" + InvoiceDate + "','" + "Payable-Purch,Bill#" + invoiceNumber + "-" + PartyCode + "'," + Paid + "," + a + ",'" + "CPV" + "','" + PurchaseGLCode + "','" + SysTime + "','" + invoiceNumber + "','" + "Purchase" + "','" + PartyCode + "','" + InvoiceDate + "','" + "Payable-Purch,Bill#" + invoiceNumber + "-" + PartyCode + "','" + CompID + "'," + stationId + ")", con, tran);
                        cmd15.ExecuteNonQuery();

                        //SqlCommand cmd16 = new SqlCommand("insert into PartiesLedger (code,datedr,DescriptionDr,AmountCr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,CompID,StationId) values('" + PartyCode + "','" + InvoiceDate + "','" + "Cash - Inv#" + invoiceNumber + "'," + Paid + "," + b + ",'" + "CRV" + "','" + SysTime + "'," + invoiceNumber + ",'" + "Sale" + "','" + PartyCode + "','" + InvoiceDate + "','" + CompID + "')", con, tran);
                        SqlCommand cmd16 = new SqlCommand("insert into PartiesLedger(code,datedr,DescriptionDr,AmountDr,V_NO,V_type,System_Date_Time,BillNo,DescriptionOfBillNo,VenderCode,PurchaseDate,CompID,StationId) values('" + PartyCode + "','" + InvoiceDate + "','" + "Cash Paid - Purchases(Bill#" + invoiceNumber + ")" + "'," + Paid + "," + a + ",'" + "CPV" + "','" + SysTime + "','" + invoiceNumber + "','" + "Purchase" + "','" + PartyCode + "','" + InvoiceDate + "','" + CompID + "'," + stationId + ")", con, tran);
                        cmd16.ExecuteNonQuery();
                    }
                    //////////        //--------------------------------UpDate Party Balance-------------------------------------------------------


                    if (NorBalance == 1)
                    {

                        SqlCommand cmdUpDatePartyBalance = new SqlCommand("Update PartyCode set Balance=" + (PreviousBalance - Total + NetDiscount + Paid) + " where CompID='" + CompID + "' and Code='" + PartyCode + "'", con, tran);
                        cmdUpDatePartyBalance.ExecuteNonQuery();
                        SqlCommand cmdUpDatePartyBalance2 = new SqlCommand("Update GLCode set Balance=" + (PreviousBalance - Total + NetDiscount + Paid) + " where CompID='" + CompID + "' and Code='" + PartyCode + "'", con, tran);
                        cmdUpDatePartyBalance2.ExecuteNonQuery();
                    }
                    else
                        if (NorBalance == 2)
                    {
                        SqlCommand cmdUpDatePartyBalance = new SqlCommand("Update PartyCode set Balance=" + (PreviousBalance + Total - NetDiscount - Paid) + " where CompID='" + CompID + "' and Code='" + PartyCode + "'", con, tran);
                        cmdUpDatePartyBalance.ExecuteNonQuery();
                        SqlCommand cmdUpDatePartyBalance2 = new SqlCommand("Update GLCode set Balance=" + (PreviousBalance + Total - NetDiscount - Paid) + " where CompID='" + CompID + "' and Code='" + PartyCode + "'", con, tran);
                        cmdUpDatePartyBalance2.ExecuteNonQuery();
                    }


                    tran.Commit();
                    con.Close();
                    return new BaseModel() { Success = true, Message = "Purchase Updated Successfully", LastInvoiceNumber = invoiceNumber.ToString() };
                }
                else
                {

                    return new BaseModel() { Success = false, Message = "Your session has been expired, login again and continue to Save this page.", LoginAgain = true };

                }
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
            finally
            {
                con.Close();
            } 
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Delete(SaveProductViewModel saveProductViewModel)
        {
            try
            {
                var stationId = string.Empty;
                if (HttpContext.Current.Session["StationId"] != null)
                {
                    stationId = saveProductViewModel.ClientData.StationId;
                }
                if (stationId != string.Empty)
                {

                    var objModule1 = new Module1();
                    var objModule4 = new Module4();
                    var modQtyOfInventory = new ModQtyOfInventory();
                    ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
                    if (con.State == ConnectionState.Closed) { con.Open(); }
                    tran = con.BeginTransaction();

                    var invoiceNumber = saveProductViewModel.ClientData.InvoiceNumber;


                    SqlCommand cmdDel1 = new SqlCommand("Delete  from PartiesLedger where  CompID='" + CompID + "' and BillNo='" + invoiceNumber + "' and DescriptionOfBillNo='Purchase'", con, tran);
                    cmdDel1.ExecuteNonQuery();
                    SqlCommand cmdDel2 = new SqlCommand("Delete  from GeneralLedger where  CompID='" + CompID + "' and BillNo='" + invoiceNumber + "' and DescriptionOfBillNo='Purchase'", con, tran);
                    cmdDel2.ExecuteNonQuery();
                    SqlCommand cmdDel3 = new SqlCommand("Delete  from Inventory where  CompID='" + CompID + "' and BillNo='" + invoiceNumber + "' and DescriptionOfBillNo='Purchase'", con, tran);
                    cmdDel3.ExecuteNonQuery();

                    SqlCommand cmdDel4 = new SqlCommand("Delete  from Purchase1 where  CompID='" + CompID + "' and InvNo='" + invoiceNumber + "'", con, tran);
                    cmdDel4.ExecuteNonQuery();
                    SqlCommand cmdDel5 = new SqlCommand("Delete  from Purchase2 where  CompID='" + CompID + "' and InvNo='" + invoiceNumber + "'", con, tran);
                    cmdDel5.ExecuteNonQuery();
                    SqlCommand cmdDel6 = new SqlCommand("Delete  from Purchase3 where  CompID='" + CompID + "' and InvNo='" + invoiceNumber + "'", con, tran);
                    cmdDel6.ExecuteNonQuery();
                    SqlCommand cmdDel6IME = new SqlCommand("Delete  from InventorySerialNoFinal where  CompID='" + CompID + "' and Description='Purchase' and VoucherNo='" + invoiceNumber + "'", con, tran);
                    cmdDel6IME.ExecuteNonQuery();


                    foreach (var itemsOld in saveProductViewModel.ProductsOld)
                    {
                        modQtyOfInventory.QtyAvailableOne(itemsOld.Code, tran, con);

                        var CodeOld = Convert.ToDecimal(itemsOld.Code);
                        var Quantity = Convert.ToDecimal(itemsOld.Qty);
                        var ItemStationId = Convert.ToInt32(itemsOld.ItemStationId);

                        objModule4.ClosingBalanceNewTechniqueStationIssue(CodeOld, Quantity, ItemStationId, tran, con);

                        objModule4.ClosingBalanceNewTechniqueIssue(CodeOld, Quantity, tran, con);

                    }
                    
                    tran.Commit();
                    con.Close();
                    return new BaseModel() { Success = true, Message = "Transaction Deleted Successfully", LastInvoiceNumber = invoiceNumber.ToString() };
                }
                else
                {

                    return new BaseModel() { Success = false, Message = "Your session has been expired, login again and continue to Save this page.", LoginAgain = true };

                }
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
            finally
            {
                con.Close();
            }
        }



    }
}