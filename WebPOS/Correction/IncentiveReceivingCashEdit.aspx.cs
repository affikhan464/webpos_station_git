﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using WebPOS.Model;
namespace WebPOS
{
    public partial class IncentiveReceivingCashEdit : System.Web.UI.Page
    {
        static string CompID = "01";
        static string IncentiveIncomeGLCode = "0103050100001";
        static string CashAccountGLCode = "0101010100001";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
        Module4 objModule4 = new Module4();
        Module1 objModule1 = new Module1();
        ModGLCode objModGLCode = new ModGLCode();
        static SqlTransaction tran;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel SaveIncentiveInComeCashEdit(ModelPaymentVoucher ModelPaymentVoucher)
        {

            Module1 objModule1 = new Module1();
            Module4 objModule4 = new Module4();

            try
            {
                var stationId = string.Empty;
                if (HttpContext.Current.Session["StationId"] != null)
                {
                    stationId = HttpContext.Current.Session["StationId"].ToString();
                }

                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                string SecondDescription = "CashIncentiveReceived";
                var VoucherNo = ModelPaymentVoucher.VoucherNo;

                SqlCommand cmdDel1 = new SqlCommand("Delete  from GeneralLedger where SecondDescription='" + SecondDescription + "' and BillNo='" + VoucherNo + "'", con, tran);
                cmdDel1.ExecuteNonQuery();

                SqlCommand cmdDel2 = new SqlCommand("Delete  from CashIncentive where Nature='" + SecondDescription + "' and InvNo =" + VoucherNo, con, tran);
                cmdDel2.ExecuteNonQuery();


                var VoucherDate = DateTime.ParseExact(ModelPaymentVoucher.Date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture).ToShortDateString();
                string PartyCode = ModelPaymentVoucher.PartyCode;
                string PartyName = ModelPaymentVoucher.PartyName;
                var SysTime = VoucherDate + " " + DateTime.Now.ToString("HH:mm:ss");

                decimal CashPaid = Convert.ToDecimal(ModelPaymentVoucher.CashPaid);
                string Narration = ModelPaymentVoucher.Narration;

                string Desc = "Cash Incentive Received-(" + PartyName + ")" + ", VNo." + VoucherNo + ", Nar: " + Narration;

                SqlCommand cmd1 = new SqlCommand("insert into GeneralLedger (Code,Description,AmountDr,V_No,V_Type,datedr,Code1,System_Date_Time,SecondDescription,BillNo,Narration,CompId, StationId) values('" + CashAccountGLCode + "','" + Desc + "'," + Convert.ToDecimal(CashPaid) + "," + VoucherNo + ",'" + "CRV" + "','" + VoucherDate + "','" + CompID + "03050100003" + "','" + SysTime + "','" + SecondDescription + "','" + VoucherNo + "','" + Desc + "','" + CompID + "'," + stationId + ")", con, tran);
                cmd1.ExecuteNonQuery();

                SqlCommand cmd2 = new SqlCommand("insert into GeneralLedger (Code,Description,AmountCr,V_No,V_Type,datedr,Narration,Code1,System_Date_Time,SecondDescription,BillNo,CompId, StationId) values('" + CompID + "03050100003" + "','" + Desc + "'," + Convert.ToDecimal(CashPaid) + "," + VoucherNo + ",'" + "CRV" + "','" + VoucherDate + "','" + Desc + "','" + CashAccountGLCode + "','" + SysTime + "','" + SecondDescription + "','" + VoucherNo + "','" + CompID + "'," + stationId + ")", con, tran);
                cmd2.ExecuteNonQuery();

                SqlCommand cmd3 = new SqlCommand("insert into  CashIncentive (Date1,InvNo,PartyCode,Name,Amount,Narration,Nature,System_Date_Time,CompID) values('" + VoucherDate + "'," + VoucherNo + ",'" + PartyCode + "','" + PartyName + "'," + Convert.ToDecimal(CashPaid) + ",'" + Narration + "','" + "CashIncentiveReceived" + "','" + SysTime + "','" + CompID + "')", con, tran);
                cmd3.ExecuteNonQuery();


                objModule4.ClosingBalancePartiesNew(PartyCode, con, tran);
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Edit Cash Incentive Received Saved Successfully.", LastInvoiceNumber = VoucherNo.ToString() };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Delete(ModelPaymentVoucher ModelPaymentVoucher)
        {

            Module1 objModule1 = new Module1();
            Module4 objModule4 = new Module4();

            try
            {
               

                if (con.State == ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                string SecondDescription = "CashIncentiveReceived";
                var VoucherNo = ModelPaymentVoucher.VoucherNo;

                SqlCommand cmdDel1 = new SqlCommand("Delete  from GeneralLedger where SecondDescription='" + SecondDescription + "' and BillNo='" + VoucherNo + "'", con, tran);
                cmdDel1.ExecuteNonQuery();

                SqlCommand cmdDel2 = new SqlCommand("Delete  from CashIncentive where Nature='" + SecondDescription + "' and InvNo =" + VoucherNo, con, tran);
                cmdDel2.ExecuteNonQuery();
                
                string PartyCode = ModelPaymentVoucher.PartyCode;
             


                objModule4.ClosingBalancePartiesNew(PartyCode, con, tran);
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Deleted Successfully.", LastInvoiceNumber = VoucherNo.ToString() };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

    }
}
