﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Correction
{
    public partial class BankReceiveVoucherEdit : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        static string CompID = "01";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(VoucherViewModel model)
        {
            try
            {
                if (con.State == ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                string CompID = "01";
                var VoucherNo = model.VoucherNumber;

                SqlCommand cmdDelOld = new SqlCommand("Delete  from GeneralLedger where CompID='" + CompID + "' and V_No=" + Convert.ToDecimal(VoucherNo) + " and V_Type='BRV'", con, tran);
                cmdDelOld.ExecuteNonQuery();
                
                SqlCommand cmdDelOld2 = new SqlCommand("Delete  from PartiesLedger where CompID='" + CompID + "' and V_No=" + Convert.ToDecimal(VoucherNo) + " and V_Type='BRV'", con, tran);
                cmdDelOld2.ExecuteNonQuery();

                Module1 objModule1 = new Module1();
                ModGLCode objModGLCode = new ModGLCode();
                Module4 objModule4 = new Module4();
                var selectedDate = model.VoucherDate;
                var convertedDate = DateTime.ParseExact(selectedDate + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);

                var VoucherDate = convertedDate.ToString("yyyy-MM-dd");

                foreach (var item in model.Rows)
                {
                    var GLCode = item.Code;
                    var Description = item.Title;
                    var ChqNo = item.ChequeNumber;
                    var Narration = item.Narration;
                    var AmountDr = item.Debit;
                    var Narration1 = model.CreditGLTitle + " - " + Narration + " - " + "BRV No.=" + VoucherNo;
                    var BankCode = model.CreditGLCode;
                    var BankName = model.CreditGLTitle;

                    SqlCommand cmdDr = new SqlCommand("insert into GeneralLedger(code,datedr,Description, ChequeNo,Narration,AmountCr,V_NO,V_type,System_Date_Time,BankCode,CompID,Code1)values('" + GLCode + "','" + VoucherDate + "','" + BankName + "-" + Narration + "','" + ChqNo + "','" + Narration + "'," + AmountDr + "," + VoucherNo + ",'" + "BRV" + "','" + convertedDate + "','" + BankCode + "','" + CompID + "','" + BankCode + "')", con, tran);
                    cmdDr.ExecuteNonQuery();

                    SqlCommand cmdCr = new SqlCommand("insert into GeneralLedger (code,datedr,Description,ChequeNo,Narration,AmountDr,V_NO,V_type,System_Date_Time,BankCode,CompID) values('" + BankCode + "','" + VoucherDate + "','" + Description + "','" + ChqNo + "','" + Description + " - " + Narration + "'," + AmountDr + "," + VoucherNo + ",'" + "BRV" + "','" + convertedDate + "','" + BankCode + "','" + CompID + "')", con, tran);
                    cmdCr.ExecuteNonQuery();
                    if (GLCode.Substring(0, 8) == "01020101" || GLCode.Substring(0, 8) == "01010103")
                    {

                        SqlCommand cmdPart = new SqlCommand("insert into PartiesLedger (Code,DescriptionDr,AmountCr,V_No,V_Type,datedr,System_Date_Time,DescriptionOfBillNo,VenderCode,CompID) values('" + GLCode + "','" + BankName + "-" + Narration + ",BRV.No.=" + VoucherNo + "'," + AmountDr + "," + VoucherNo + ",'" + "BRV" + "','" + VoucherDate + "','" + convertedDate + "','" + "FromParties" + "','" + BankCode + "','" + CompID + "')", con, tran);
                        cmdPart.ExecuteNonQuery();
                        objModule4.ClosingBalancePartiesNew(GLCode, con, tran);
                    }

                }




                //Save code for JournalVoucher

                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Saved Successfully!", LastVoucherNumber = Convert.ToString(VoucherNo) };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Delete(VoucherViewModel ModelPaymentVoucher)
        {

            Module1 objModule1 = new Module1();
            Module4 objModule4 = new Module4();

            try
            {


                if (con.State == ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                var VoucherNo = ModelPaymentVoucher.VoucherNumber;


                SqlCommand cmdDelOld = new SqlCommand("Delete  from GeneralLedger where CompID='" + CompID + "' and V_No=" + Convert.ToDecimal(VoucherNo) + " and V_Type='BRV'", con, tran);
                cmdDelOld.ExecuteNonQuery();

                string GLCode = ModelPaymentVoucher.CreditGLCode;
                SqlCommand cmdDelOld2 = new SqlCommand("Delete  from PartiesLedger where CompID='" + CompID + "' and V_No=" + Convert.ToDecimal(VoucherNo) + " and V_Type='BRV'", con, tran);
                cmdDelOld2.ExecuteNonQuery();

                objModule4.ClosingBalancePartiesNew(GLCode, con, tran);
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Deleted Successfully.", LastInvoiceNumber = VoucherNo.ToString() };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }
    }
}