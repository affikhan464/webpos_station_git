﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using WebPOS.Model;

namespace WebPOS
{
    public partial class PaymentToPartyThroughBankEdit : System.Web.UI.Page
    {
        static string CompID = "01";
        static string CashAccountGLCode = "0101010100001";

        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
        Module4 objModule4 = new Module4();
        Module1 objModule1 = new Module1();
        ModGLCode objModGLCode = new ModGLCode();
        ModGL objModGL = new ModGL();
        static SqlTransaction tran;

        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(ModelPaymentVoucher ModelPaymentVoucher)
        {

            Module1 objModule1 = new Module1();
            Module4 objModule4 = new Module4();

            try
            {

                var stationId = string.Empty;
                if (HttpContext.Current.Session["StationId"] != null)
                {
                    stationId = HttpContext.Current.Session["StationId"].ToString();
                }
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                string SecondDescription = "CashDepositIntoBank";
                var VoucherNo = ModelPaymentVoucher.VoucherNo;

                var VoucherDate = DateTime.ParseExact(ModelPaymentVoucher.Date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture).ToShortDateString();
                string PartyCode = ModelPaymentVoucher.PartyCode;
                string PartyName = ModelPaymentVoucher.PartyName;
                string BankCode = ModelPaymentVoucher.BankCode;
                string BankName = ModelPaymentVoucher.BankName;
                string ChqNo = ModelPaymentVoucher.ChqNo;

                var SysTime = VoucherDate + " " + DateTime.Now.ToString("HH:mm:ss");

                decimal CashPaid = Convert.ToDecimal(ModelPaymentVoucher.CashPaid);
                string Narration = ModelPaymentVoucher.Narration;


                SqlCommand cmdDelOld1 = new SqlCommand("Delete  from GeneralLedger where  CompID='" + CompID + "' and V_NO=" + Convert.ToDecimal(VoucherNo) + "and V_type='BPV' and SecondDescription='CashPaidToPartyThroughBank'", con, tran);
                cmdDelOld1.ExecuteNonQuery();
                SqlCommand cmdDelOld2 = new SqlCommand("Delete  from PartiesLedger where CompID ='" + CompID + "' and V_NO=" + Convert.ToDecimal(VoucherNo) + "and V_type='BPV' and DescriptionOfBillNo='CashPaidToPartyThroughBank'", con, tran);
                cmdDelOld2.ExecuteNonQuery();



                SqlCommand cmd1 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,ChequeNo,Narration,AmountDr,V_NO,V_type,System_Date_Time,BankCode,SecondDescription,VenderCode,CompId, StationId) values('" + PartyCode + "','" + VoucherDate + "','" + Narration + "','" + ChqNo + "','" + Narration + "'," + Convert.ToDecimal(CashPaid) + "," + Convert.ToDecimal(VoucherNo) + ",'" + "BPV" + "','" + SysTime + "','" + BankCode + "','" + "CashPaidToPartyThroughBank" + "','" + PartyCode + "','" + CompID + "'," + stationId + ")", con, tran);
                cmd1.ExecuteNonQuery();

                SqlCommand cmd2 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,ChequeNo,Narration,AmountCr,V_NO,V_type,System_Date_Time,BankCode,SecondDescription,VenderCode,CompId, StationId) values('" + BankCode + "','" + VoucherDate + "','" + Narration + "','" + ChqNo + "','" + Narration + "'," + Convert.ToDecimal(CashPaid) + "," + Convert.ToDecimal(VoucherNo) + ",'" + "BPV" + "','" + SysTime + "','" + BankCode + "','" + "CashPaidToPartyThroughBank" + "','" + PartyCode + "','" + CompID + "'," + stationId + ")", con, tran);
                cmd2.ExecuteNonQuery();

                SqlCommand cmd3 = new SqlCommand("insert into PartiesLedger (Code,DescriptionDr,AmountDr,V_No,V_Type,datedr,System_Date_Time,DescriptionOfBillNo,VenderCode,CompId, StationId) values('" + PartyCode + "','" + Narration + ",BPV.No.=" + VoucherNo + "(" + BankName + ")" + "'," + Convert.ToDecimal(CashPaid) + "," + VoucherNo + ",'" + "BPV" + "','" + VoucherDate + "','" + SysTime + "','" + "CashPaidToPartyThroughBank" + "','" + BankCode + "','" + CompID + "'," + stationId + ")", con, tran);
                cmd3.ExecuteNonQuery();

                objModule4.ClosingBalancePartiesNew(PartyCode, con, tran);
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Edit Payment To Party Through Bank Saved Successfully.", LastInvoiceNumber = VoucherNo.ToString() };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Delete(ModelPaymentVoucher ModelPaymentVoucher)
        {

            Module1 objModule1 = new Module1();
            Module4 objModule4 = new Module4();

            try
            {

              
                if (con.State == ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                string SecondDescription = "CashDepositIntoBank";
                var VoucherNo = ModelPaymentVoucher.VoucherNo;

               string PartyCode = ModelPaymentVoucher.PartyCode;

                decimal CashPaid = Convert.ToDecimal(ModelPaymentVoucher.CashPaid);
                string Narration = ModelPaymentVoucher.Narration;


                SqlCommand cmdDelOld1 = new SqlCommand("Delete  from GeneralLedger where  CompID='" + CompID + "' and V_NO=" + Convert.ToDecimal(VoucherNo) + "and V_type='BPV' and SecondDescription='CashPaidToPartyThroughBank'", con, tran);
                cmdDelOld1.ExecuteNonQuery();
                SqlCommand cmdDelOld2 = new SqlCommand("Delete  from PartiesLedger where CompID ='" + CompID + "' and V_NO=" + Convert.ToDecimal(VoucherNo) + "and V_type='BPV' and DescriptionOfBillNo='CashPaidToPartyThroughBank'", con, tran);
                cmdDelOld2.ExecuteNonQuery();
                
                objModule4.ClosingBalancePartiesNew(PartyCode, con, tran);
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Deleted Successfully.", LastInvoiceNumber = VoucherNo.ToString() };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }







    }
}
