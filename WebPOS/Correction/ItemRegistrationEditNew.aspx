﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="ItemRegistrationEditNew.aspx.cs" Inherits="WebPOS.ItemRegistrationEditNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div class="container">
        <div class="mt-4">

            <section class="form">
                <h2 class="form-header fa-pen-square">Edit Item Registration</h2>
               <ul class="nav nav-tabs w-100 d-flex p-0 px-5" id="myTab">
                    <li class="flex-1 mb-0">
                        <a data-toggle="tab" href="#home" class="active shadow text-center w-100 d-block">
                            <span class="light"></span>
                           Main Item Entries
                        </a>
                    </li>
                    <li class="flex-1 mb-0">
                        <a data-toggle="tab" href="#profile" class="shadow text-center w-100 d-block">
                            <span class="light"></span>
                          Other Details
                        </a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                <span class="input input--hoshi">
                                    <input id="txtItemCode" class="ItemCode input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Item Code</span>
                                    </label>
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <span class="input input--hoshi">
                                    <input id="txtBarCode" class="BarCode empty1 input__field input__field--hoshi" type="text" autocomplete="off" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Bar Code </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstBarCodeCategory" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">BarCode Category </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                                <span class="input input--hoshi">
                                    <input id="txtItemName" data-id="txtItemName" data-type="Item" data-function="GetSimpleItem" class="autocomplete SearchBox ItemDescription empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Item Name </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <span class="input input--hoshi">
                                    <a class="btn btn-3 btn-bm btn-3e w-100" href="" id="btnSave1"><i class="fas fa-sync"></i>Reset </a>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <input id="txtReference1" class="Reference1 empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Refernce-1 </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <input id="txtReference2" class="Reference1 empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Refernce-2 </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstUnit" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Unit </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <span class="input input--hoshi">
                                    <a class="btn btn-3 btn-bm btn-3e w-100" data-toggle="modal" data-target="#attributeModal" data-attributename="AccountUnit"><i class="fas fa-plus-circle"></i>Add Unit </a>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <input id="txtSellingPrice" class="SellingPrice empty1 input__field input__field--hoshi" min="0" type="number" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Sale Price/Unit  </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <input id="txtMSP" class="MSP empty1 input__field input__field--hoshi" min="0" type="number" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Market Selling Price (MSP)  </span>
                                    </label>
                                </span>
                            </div>
                            
                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <span class="input input--hoshi">
                                    <input id="PerPieceCommission" class="PerPieceCommission empty1 input__field input__field--hoshi" min="0" type="number" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Per Piece Commission  </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <span class="input input--hoshi">
                                    <input value="0" id="txtMinLimit" class="MinLimit empty1 input__field input__field--hoshi" min="0" type="number" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Min Limit </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <span class="input input--hoshi">
                                    <input value="0" id="txtMaxLimit" class="MaxLimit empty1 input__field input__field--hoshi" min="0" type="number" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Max Limit </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <input value="1" id="txtPiecesInPacking" class="PiecesInPacking empty1 input__field input__field--hoshi" min="0" type="number" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Pieces in Packing  </span>
                                    </label>
                                </span>
                            </div>

                            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstBrand" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Brand Name </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                                <span class="input input--hoshi">
                                    <a class="btn btn-3 btn-bm btn-3e w-100" data-toggle="modal" data-target="#attributeModal" data-attributename="Brand"><i class="fas fa-plus-circle"></i>Add Brand </a>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <input id="txtReOrdLevel" class="ReOrdLevel empty1 input__field input__field--hoshi" min="0" type="number" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Reorder Point  </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <input value="1" id="txtReorderQty" class="ReorderQty empty1 input__field input__field--hoshi" min="0" type="number" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Reorder Quantity </span>
                                    </label>
                                </span>
                            </div>

                            <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi checkbox--hoshi">
                                    <label>
                                        <input id="chkItemIsActive" checked="checked" class="checkbox" type="checkbox" />
                                        <span></span>
                                        Is Active Item?
                                    </label>
                                </span>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi checkbox--hoshi">
                                    <label>
                                        <input id="ChkVisiable" checked="checked" class="checkbox" type="checkbox" />
                                        <span></span>
                                        Is Visible?
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                <span class="input input--hoshi">
                                    <input id="txtOtherInFo" class="OtherInFo empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Other Information </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstGroup" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Group </span>
                                    </label>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                        <div class="row">

                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstManufacturer" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Manufacturer</span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <a class="btn btn-3 btn-bm btn-3e w-100" data-toggle="modal" data-target="#attributeModal" data-attributename="Manufacturer"><i class="fas fa-plus-circle"></i>Add Manufacturer </a>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstPacking" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Packing </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <a class="btn btn-3 btn-bm btn-3e w-100" data-toggle="modal" data-target="#attributeModal" data-attributename="Packing"><i class="fas fa-plus-circle"></i>Add Packing </a>
                                </span>
                            </div>


                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstCategory" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Category </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <a class="btn btn-3 btn-bm btn-3e w-100" data-toggle="modal" data-target="#attributeModal" data-attributename="Category"><i class="fas fa-plus-circle"></i>Add Category </a>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstClass" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Class/Warranty  </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <a class="btn btn-3 btn-bm btn-3e w-100" data-toggle="modal" data-target="#attributeModal" data-attributename="Class"><i class="fas fa-plus-circle"></i>Add Class/Warranty </a>
                                </span>
                            </div>


                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstGoDown" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Godown </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <a class="btn btn-3 btn-bm btn-3e w-100" data-toggle="modal" data-target="#attributeModal" data-attributename="Godown"><i class="fas fa-plus-circle"></i>Add Godown  </a>
                                </span>
                            </div>

                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <input value="0" id="txtOrderQTY" class="OrderQTY  empty1 input__field input__field--hoshi" min="0" type="number" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Order Quantity  </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <input value="0" id="txtBonusQTY" class="BonusQTY  empty1 input__field input__field--hoshi" min="0" type="number" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Bonus Quantity </span>
                                    </label>
                                </span>
                            </div>
                        </div>
                        <div class="row justify-content-start">
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <input value="0" id="txtGST_Rate" class="GST_Rate   empty1 input__field input__field--hoshi" min="0" type="number" />
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">GST % </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi checkbox--hoshi">
                                    <label>
                                        <input id="chkGSTApply" class="checkbox" type="checkbox" />
                                        <span></span>
                                        Apply GST?
                                    </label>
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstItemNature" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Item Nature </span>
                                    </label>
                                </span>
                            </div>

                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstIncomeAccount" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Income Account </span>
                                    </label>
                                </span>
                            </div>

                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstCOGS" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">CGS Account </span>
                                    </label>
                                </span>
                            </div>


                        </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstWidth" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Width </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <a class="btn btn-3 btn-bm btn-3e w-100" data-toggle="modal" data-target="#attributeModal" data-attributename="Width"><i class="fas fa-plus-circle"></i>Add Width  </a>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstHeight" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Height </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <a class="btn btn-3 btn-bm btn-3e w-100" data-toggle="modal" data-target="#attributeModal" data-attributename="Height"><i class="fas fa-plus-circle"></i>Add Height  </a>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstColor" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Color </span>
                                    </label>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi">
                                    <a class="btn btn-3 btn-bm btn-3e w-100" data-toggle="modal" data-target="#attributeModal" data-attributename="Color"><i class="fas fa-plus-circle"></i>Add Color  </a>
                                </span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                                <span class="input input--hoshi  input--filled">
                                    <select id="lstItemType" class="round input__field--hoshi">
                                    </select>
                                    <label class="input__label input__label--hoshi">
                                        <span class="input__label-content input__label-content--hoshi">Item Type </span>
                                    </label>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <hr />

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a onclick="DeleteItem()" class=" btn btn-3 btn-bm btn-3e fa-trash w-100 btn-danger">
                                <span>Delete</span>
                            </a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="save()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">

                                <span>Save</span>
                            </a>
                        </span>
                    </div>
                </div>

            </section>
        </div>
    </div>

    <%-- Modals --%>

    <div class="modal fade" id="attributeModal" tabindex="-1" role="dialog" aria-labelledby="attributeModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="attributeModalLabel">New message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div id="attributeModalForm" class="form" data-saveattribute="">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                    <span class="input input--hoshi">
                                        <input id="attributeName" name="AttributeName" required="required" class="AttributeName empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
                                        <label class="input__label input__label--hoshi">
                                            <span class="input__label-content input__label-content--hoshi">Name </span>
                                        </label>
                                    </span>
                                </div>

                                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                    <span class="input input--hoshi">
                                        <input id="attributeCode" name="AttributeCode" class="AttributeCode empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
                                        <label class="input__label input__label--hoshi">
                                            <span class="input__label-content input__label-content--hoshi">Code </span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                            <div class="row brandCheckBoxes">
                                <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                    <span class="input input--hoshi checkbox--hoshi">
                                        <label>
                                            <input id="chkBlocked" class="checkbox" type="checkbox" />
                                            <span></span>
                                            Blocked
                                        </label>
                                    </span>
                                </div>
                                <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                    <span class="input input--hoshi checkbox--hoshi">
                                        <label>
                                            <input id="chkVerify" class="checkbox" type="checkbox" />
                                            <span></span>
                                            Verify
                                        </label>
                                    </span>
                                </div>
                            </div>
                            <hr />
                            <div class="row">

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <span class="input input--hoshi">
                                        <a class=" btn btn-3 btn-bm btn-3e fa-times w-100" data-dismiss="modal">Close</a>
                                    </span>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <span class="input input--hoshi">
                                        <a onclick="saveAttribute()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-12 col-sm-12 col-md-12 col-lg-12-col-xl-12">

                                    <div class="allAttributes">

                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead class="thead-inverse">
                                                    <tr>
                                                        <th>Sr.</th>
                                                        <th class="text-left">Code</th>

                                                        <th class="text-left">Name</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    <script src="/Script/Attribute.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>

    <script src="../Script/Autocomplete.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>




    <script type="text/javascript">


        function empty1()
        {
            $(".empty1").val("");
        }

        function GetItemData()
        {

            var ItCode = $(".ItemCode").val();

            empty1();
            $(".ItemCode").val(ItCode);
            var ItemCode = $(".ItemCode").val();
            $.ajax({
                url: '/WebPOSService.asmx/GeItemDataAgainstItemCode',
                type: "Post",
                data: { "ItemCode": ItemCode },
                success: function (ItemModel)
                {

                    document.getElementById("chkItemIsActive").checked = false;
                    var Active = ItemModel.Active;
                    if (Active == 1) { document.getElementById("chkItemIsActive").checked = true; }

                    document.getElementById("ChkVisiable").checked = false;
                    var Visiable = ItemModel.Visiable;

                    if (Visiable == 1) { document.getElementById("ChkVisiable").checked = true; }



                    document.getElementById("chkGSTApply").checked = false;
                    var GST_Apply = ItemModel.GST_Apply;
                    if (GST_Apply == 1) { document.getElementById("chkGSTApply").checked = true; }


                    //document.getElementById("chkCreditLimitApply").checked = false;
                    //if (PartyModel.CreditLimitApply == 1) { document.getElementById("chkCreditLimitApply").checked = true; }
                    //document.getElementById("chkDeal").checked = false;
                    //if (PartyModel.chkDeal == 1) { document.getElementById("chkDeal").checked = true; }

                    //document.getElementById("chkPerDiscount").checked = false;

                    //if (PartyModel.chkPerDiscount == 1) { document.getElementById("chkPerDiscount").checked = true; }



                    $(".SearchBox").val(ItemModel.Description);
                    $(".SellingPrice").val(ItemModel.SellingCost1);
                    $(".Reference1").val(ItemModel.Reference);
                    $(".Reference2").val(ItemModel.Reference2);
                    $(".MinLimit").val(ItemModel.MinLimit);
                    $(".MaxLimit").val(ItemModel.MaxLimit);
                    $(".PiecesInPacking").val(ItemModel.PiecesInPacking);
                    $(".ReOrdLevel").val(ItemModel.ReorderLevel);
                    $(".ReorderQty").val(ItemModel.ReorderQty);
                    $(".OtherInFo").val(ItemModel.OtherInFo);
                    $(".MSP").val(ItemModel.MSP);
                    $(".PerPieceCommission").val(ItemModel.PerPieceCommission);
                    $(".OrderQTY").val(ItemModel.OrderQTY);
                    $(".BonusQTY").val(ItemModel.BonusQTY);
                    $(".GST_Rate").val(ItemModel.GST_Rate);

                    var BarCodeCategory = ItemModel.BarCodeCategory;
                    $("#lstBarCodeCategory").val(BarCodeCategory);


                    var AccountUnit = ItemModel.AccountUnit;

                    $("#lstUnit").val(AccountUnit);

                    var Brand = ItemModel.Brand;
                    $("#lstBrand").val(Brand);

                    var ItemType = ItemModel.ItemType;
                    $("#lstItemType").val(ItemType);

                    var Manufacturer = ItemModel.Manufacturer;
                    $("#lstManufacturer").val(Manufacturer);

                    var Packing = ItemModel.Packing;
                    $("#lstPacking").val(Packing);

                    var Category = ItemModel.Category;
                    $("#lstCategory").val(Category);

                    var Class = ItemModel.Class;
                    $("#lstClass").val(Class);


                    var Godown = ItemModel.Godown;
                    $("#lstGoDown").val(Godown);

                    var Group = ItemModel.Group;
                    $("#lstGroup").val(Group);

                    var Nature = ItemModel.Nature;
                    $("#lstItemNature").val(Nature);

                    var Revenue_Code = ItemModel.Revenue_Code;
                    $("#lstIncomeAccount").val(Revenue_Code);



                    var Color = ItemModel.Color;
                    $("#lstColor").val(Color);


                    var Height = ItemModel.Height;
                    $("#lstHeight").val(Height);



                    var Width = ItemModel.Length;

                    $("#lstWidth").val(Width);

                    updateInputStyle();
                },
                fail: function (jqXhr, exception)
                {
                }
            });
        }
        function DeleteItem()
        {
            var ItemCode = $(".ItemCode").val();


            var ItemModel = {
                Code: ItemCode,

            }
            swal({
                title: 'Are you sure to delete?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Yes Delete It',
                showLoaderOnConfirm: true,
                preConfirm: (text) =>
                {
                    return new Promise((resolve) =>
                    {
                        deleteAttribute(ItemModel);
                        resolve();
                    })
                },
                allowOutsideClick: () => !swal.isLoading()
            })




        }
        function deleteAttribute(ItemModel)
        {
            $.ajax({
                url: "/Correction/ItemRegistrationEditNew.aspx/DeleteItem",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ ItemModel: ItemModel }),
                success: function (BaseModel)
                {
                    debugger
                    if (BaseModel.d.Success)
                    {
                        $(".empty1").val("");
                        alert(BaseModel.d.Message);
                        updateInputStyle();
                    }
                    else
                    {
                        alert(BaseModel.d.Message);
                        updateInputStyle();
                    }


                }

            });
        }
        function save()
        {
            var ItemCode = $(".ItemCode").val();
            var BarCode = $(".BarCode").val();
            var Description = $("#txtItemName").val();
            var Reference1 = $(".Reference1").val();
            var Reference2 = $(".Reference2").val();
            var PiecesInPacking = $(".PiecesInPacking").val();
            var Brand = $("#lstBrand").val();
            var chkVisiable = document.getElementById("ChkVisiable").checked ? "1" : "0";

            var ReOrdLevel = $(".ReOrdLevel").val();
            var Manufacturer = $("#lstManufacturer").val();
            var Packing = $("#lstPacking").val();
            var Category = $("#lstCategory").val();
            var Class = $("#lstClass").val();
            var GoDown = $("#lstGoDown").val();
            var ReOrdQuantity = $(".ReorderQty").val();
            var OrderQty = $(".OrderQTY").val();
            var BonusQty = $(".BonusQTY").val();
            var ItemNature = $("#lstItemNature").val();
            var AccountUnit = $("#lstUnit").val();
            var SellingCost = $(".SellingPrice").val();
            var Revenue_Code = $("#lstIncomeAccount").val();
            var chkActive = document.getElementById("chkItemIsActive").checked ? "1" : "0";
            var GSTRate = $(".GST_Rate").val();
            var chkGSTApply = document.getElementById("chkGSTApply").checked ? "1" : "0";
            var MinLimit = $(".MinLimit").val();
            var MaxLimit = $(".MaxLimit").val();
            var lstCGS_Code = $("#lstCOGS").val();
            var lstItemType = $("#lstItemType").val();
            var lstWidth = $("#lstWidth").val();
            var lstHeight = $("#lstHeight").val();
            var lstBarCodeCategory = $("#lstBarCodeCategory").val();
            var lstColor = $("#lstColor").val();
            var OtherInFo = $(".OtherInFo").val();
            var MSP = $(".MSP").val();
            var PerPieceCommission = $(".PerPieceCommission").val();
            var lstGroup = $("#lstGroup").val();


            if (PiecesInPacking == "") { PiecesInPacking = 1 }
            if (ReOrdLevel == "") { ReOrdLevel = 1 }
            if (ReOrdQuantity == "") { ReOrdQuantity = 1 }
            if (OrderQty == "") { OrderQty = 0 }
            if (BonusQty == "") { BonusQty = 0 }
            if (SellingCost == "") { SellingCost = 0 }
            if (GSTRate == "") { GSTRate = 0 }
            if (MinLimit == "") { MinLimit = 0 }
            if (MaxLimit == "") { MaxLimit = 0 }

            PiecesInPacking = isNaN(PiecesInPacking) ? 1 : PiecesInPacking;
            ReOrdLevel = isNaN(ReOrdLevel) ? 1 : ReOrdLevel;
            ReOrdQuantity = isNaN(ReOrdQuantity) ? 1 : ReOrdQuantity;
            OrderQty = isNaN(OrderQty) ? 0 : OrderQty;
            BonusQty = isNaN(BonusQty) ? 0 : BonusQty;
            SellingCost = isNaN(SellingCost) ? 0 : SellingCost;
            GSTRate = isNaN(GSTRate) ? 0 : GSTRate;
            MinLimit = isNaN(MinLimit) ? 0 : MinLimit;
            MaxLimit = isNaN(MaxLimit) ? 0 : MaxLimit;

            var ItemModel = {
                Code: ItemCode,
                BarCode: BarCode,
                Description: Description,

                Reference: Reference1,
                Reference2: Reference2,
                PiecesInPacking: PiecesInPacking,
                Brand: Brand,
                Visiable: chkVisiable,
                ReorderLevel: ReOrdLevel,
                Manufacturer: Manufacturer,
                Packing: Packing,
                Category: Category,
                Class: Class,
                Godown: GoDown,
                ReorderQty: ReOrdQuantity,
                OrderQTY: OrderQty,
                BonusQTY: BonusQty,
                Nature: ItemNature,
                AccountUnit: AccountUnit,
                SellingCost1: SellingCost,
                Revenue_Code: Revenue_Code,
                Active: chkActive,
                GST_Rate: GSTRate,
                GST_Apply: chkGSTApply,
                MinLimit: MinLimit,
                MaxLimit: MaxLimit,
                CGS_Code: lstCGS_Code,
                ItemType: lstItemType,
                Length: lstWidth,
                Height: lstHeight,
                BarCodeCategory: lstBarCodeCategory,
                Color: lstColor,
                OtherInFo: OtherInFo,
                Group: lstGroup,
                MSP: MSP,
                PerPieceCommission: PerPieceCommission
            }


            debugger
            $.ajax({
                url: "/Correction/ItemRegistrationEditNew.aspx/Save",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ ItemModel: ItemModel }),
                success: function (BaseModel)
                {
                    debugger
                    if (BaseModel.d.Success)
                    {
                        $(".empty1").val("");
                        swal("Updated", BaseModel.d.Message, "success");
                        updateInputStyle();
                    }
                    else
                    {
                        swal("Failed", BaseModel.d.Message, "error");
                    }


                }

            });

        }
        $(document).ready(function ()
        {
            GetGoDownList();
            GetManufacturerList();
            GetCategoryList();
            GetPackingList();
            GetClassList();
            GetItemNatureList();
            GetColorList();
            GetBarCodeCategoryList();
            HeightList();
            WidthList();
            AccountUnitList();
            ItemTypeList();
            COGSList();
            Income_List();
            GetBrandList2();
            GetGroupList();

        });




    </script>

</asp:Content>
