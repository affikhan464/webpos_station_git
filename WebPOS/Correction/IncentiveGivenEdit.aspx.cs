﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using WebPOS.Model;

namespace WebPOS
{
    public partial class IncentiveGivenEdit : System.Web.UI.Page
    {
        static string CompID = "01";
        static string IncentiveLossGLCode = "0104020200005";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
        static Module4 objModule4 = new Module4();
        static Module1 objModule1 = new Module1();
        static ModGLCode objModGLCode = new ModGLCode();
        static Module8 objModule8 = new Module8();
        //private object txtCurrentDate;
        //private object txtPartyCode;
        //private object txtBalance;
        //private object lstPartyName;
        //private object txtCashPaid;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(ModelPaymentVoucher ModelPaymentVoucher)
        {

            SqlTransaction tran;
            if (con.State==ConnectionState.Closed) { con.Open(); }
            tran = con.BeginTransaction();
            string PartyCode = "";
            PartyCode = ModelPaymentVoucher.PartyCode;
            string PartyCodeOld = "";
            PartyCodeOld = ModelPaymentVoucher.OldPartyCode;

            try
            {
                var stationId = string.Empty;
                if (HttpContext.Current.Session["StationId"] != null)
                {
                    stationId = HttpContext.Current.Session["StationId"].ToString();
                }
                SqlCommand cmdDelOld1 = new SqlCommand("Delete  from GeneralLedger where  CompID='" + CompID + "' and V_No=" + Convert.ToDecimal(ModelPaymentVoucher.VoucherNo) + " and V_Type='JV' and DescriptionOfBillNo='IncentiveLoss'", con, tran);
                cmdDelOld1.ExecuteNonQuery();
                SqlCommand cmdDelOld2 = new SqlCommand("Delete  from PartiesLedger where  CompID='" + CompID + "' and V_No=" + Convert.ToDecimal(ModelPaymentVoucher.VoucherNo) + " and V_Type='JV' and DescriptionOfBillNo='IncentiveLoss'", con, tran);
                cmdDelOld2.ExecuteNonQuery();

                string SysTime = Convert.ToString(DateTime.Now);
                var VoucherDate = DateTime.ParseExact(ModelPaymentVoucher.Date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture).ToShortDateString();

                decimal CashPaid = 0;
                if (!string.IsNullOrEmpty(ModelPaymentVoucher.CashPaid)) { CashPaid = Convert.ToDecimal(ModelPaymentVoucher.CashPaid); }

                SqlCommand cmd1 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountDr,V_NO,V_type,System_Date_Time,DescriptionOfBillNo,VenderCode,Narration,CompId, StationId) values('" + IncentiveLossGLCode + "','" + VoucherDate + "','" + "Receivables-(" + ModelPaymentVoucher.PartyName + ")" + ModelPaymentVoucher.Narration + "'," + CashPaid + "," + ModelPaymentVoucher.VoucherNo + ",'" + "JV" + "','" + SysTime + "','" + "IncentiveLoss" + "','" + ModelPaymentVoucher.PartyCode + "','" + ModelPaymentVoucher.Narration + "','" + CompID + "'," + stationId + ")", con, tran);
                cmd1.ExecuteNonQuery();
                SqlCommand cmd2 = new SqlCommand("insert into GeneralLedger (code,datedr,Description,AmountCr,V_NO,V_type,System_Date_Time,DescriptionOfBillNo,VenderCode,Narration,CompId, StationId) values('" + ModelPaymentVoucher.PartyCode + "','" + VoucherDate + "','" + "Incentive-(" + ModelPaymentVoucher.PartyName + ")" + ModelPaymentVoucher.Narration + "'," + CashPaid + "," + ModelPaymentVoucher.VoucherNo + ",'" + "JV" + "','" + SysTime + "','" + "IncentiveLoss" + "','" + ModelPaymentVoucher.PartyCode + "','" + ModelPaymentVoucher.Narration + "','" + CompID + "'," + stationId + ")", con, tran);
                cmd2.ExecuteNonQuery();
                SqlCommand cmd3 = new SqlCommand("insert into PartiesLedger (code,datedr,DescriptionDr,AmountCr,V_NO,V_type,System_Date_Time,DescriptionOfBillNo,VenderCode,BillNo,CompId, StationId) values('" + ModelPaymentVoucher.PartyCode + "','" + VoucherDate + "','" + "Incentive(Loss) -JV # " + ModelPaymentVoucher.VoucherNo + "-" + ModelPaymentVoucher.Narration + "'," + CashPaid + "," + ModelPaymentVoucher.VoucherNo + ",'" + "JV" + "','" + SysTime + "','" + "IncentiveLoss" + "','" + ModelPaymentVoucher.PartyCode + "','" + ModelPaymentVoucher.Narration + "','" + CompID + "'," + stationId + ")", con, tran);
                cmd3.ExecuteNonQuery();

                objModule4.ClosingBalancePartiesNew(PartyCode, con, tran);
                if (PartyCodeOld != PartyCode)
                {
                    objModule4.ClosingBalancePartiesNew(PartyCodeOld, con, tran);

                }

                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Saved Successfully." };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Delete(ModelPaymentVoucher ModelPaymentVoucher)
        {

            SqlTransaction tran;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            tran = con.BeginTransaction();
            string PartyCode = "";
            PartyCode = ModelPaymentVoucher.PartyCode;
            string PartyCodeOld = "";
            PartyCodeOld = ModelPaymentVoucher.OldPartyCode;

            try
            {
              
                SqlCommand cmdDelOld1 = new SqlCommand("Delete  from GeneralLedger where  CompID='" + CompID + "' and V_No=" + Convert.ToDecimal(ModelPaymentVoucher.VoucherNo) + " and V_Type='JV' and DescriptionOfBillNo='IncentiveLoss'", con, tran);
                cmdDelOld1.ExecuteNonQuery();
                SqlCommand cmdDelOld2 = new SqlCommand("Delete  from PartiesLedger where  CompID='" + CompID + "' and V_No=" + Convert.ToDecimal(ModelPaymentVoucher.VoucherNo) + " and V_Type='JV' and DescriptionOfBillNo='IncentiveLoss'", con, tran);
                cmdDelOld2.ExecuteNonQuery();
                

                objModule4.ClosingBalancePartiesNew(PartyCode, con, tran);
                if (PartyCodeOld != PartyCode)
                {
                    objModule4.ClosingBalancePartiesNew(PartyCodeOld, con, tran);

                }

                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Deleted Successfully." };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }


    }
}
