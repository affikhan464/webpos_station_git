﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using WebPOS.Model;
namespace WebPOS
{
    public partial class ItemRegistrationEditNew : System.Web.UI.Page
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        ModGLCode objModGLCode = new ModGLCode();
        ModItem objModItem = new ModItem();
        Module1 objModule1 = new Module1();
        Module7 objModule7 = new Module7();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {





            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel DeleteItem(ItemModel ItemModel)
        {
            Module1 objModule1 = new Module1();
            ModQtyOfInventory objModQtyOfInventory = new ModQtyOfInventory();
            ModGLCode objModGLCode = new ModGLCode();
            Module7 objModule7 = new Module7();
            var CompID = "01";
            var message = "";
            try
            {
                if (con.State == ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();
                decimal ItemCode = Convert.ToDecimal(ItemModel.Code);
                var TransactionExistOfItem = objModQtyOfInventory.TransactionExistOFItem(ItemCode);

                if (TransactionExistOfItem == "No")
                {

                    SqlCommand cmddel1 = new SqlCommand("Delete  from  InvCode where Code=" + ItemCode, con, tran);
                    cmddel1.ExecuteNonQuery();

                    SqlCommand cmddel2 = new SqlCommand("Delete  from InventoryOpeningBalance where ICode=" + ItemCode, con, tran);
                    cmddel2.ExecuteNonQuery();

                    SqlCommand cmddel3 = new SqlCommand("Delete  from WarrantyOpeningBalance where ICode=" + ItemCode, con, tran);
                    cmddel3.ExecuteNonQuery();

                    SqlCommand cmddel4 = new SqlCommand("Delete  from WorkshopOpeningBalance where ICode=" + ItemCode, con, tran);
                    cmddel4.ExecuteNonQuery();

                    SqlCommand cmddel5 = new SqlCommand("Delete  from InvBarCode where ItemCode=" + ItemCode, con, tran);
                    cmddel5.ExecuteNonQuery();

                    message = "Item Deleted Successfully";


                }
                else
                {
                    message = "Transaction exist can not be deleted";
                }


                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(ItemModel ItemModel)
        {
            Module1 objModule1 = new Module1();
            ModItem objModItem = new ModItem();
            ModGLCode objModGLCode = new ModGLCode();
            Module7 objModule7 = new Module7();
            var CompID = "01";
            try
            {
                if (con.State == ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();
                var ItemCode = ItemModel.Code;
                var BarCode = ItemModel.BarCode;
                var Description = ItemModel.Description.Trim();
                var Reference = ItemModel.Reference == string.Empty ? "" : ItemModel.Reference;
                var Reference2 = ItemModel.Reference2 == string.Empty ? "" : ItemModel.Reference2;
                var PiecesInPacking = ItemModel.PiecesInPacking;
                var Brand = ItemModel.Brand;
                var chkVisiable = ItemModel.Visiable;
                var ReOrdLevel = ItemModel.ReorderLevel;
                int Manufacturer = Convert.ToInt16(ItemModel.Manufacturer);
                int PackingID = Convert.ToInt16(ItemModel.Packing);
                int CategoryID = Convert.ToInt16(ItemModel.Category);
                int ClassID = Convert.ToInt16(ItemModel.Class);
                int GoDownID = Convert.ToInt16(ItemModel.Godown);
                decimal ReOrdQuantity = Convert.ToDecimal(ItemModel.ReorderQty);
                decimal OrderQty = Convert.ToDecimal(ItemModel.OrderQTY);
                decimal BonusQty = Convert.ToDecimal(ItemModel.BonusQTY);
                int ItemNatureID = Convert.ToInt16(ItemModel.Nature);
                int AccountUnitID = Convert.ToInt16(ItemModel.AccountUnit);
                decimal SellingPrice = Convert.ToDecimal(ItemModel.SellingCost1);
                var Revenue_Code = ItemModel.Revenue_Code;
                int chkActive = Convert.ToInt16(ItemModel.Active);
                decimal GSTRate = Convert.ToDecimal(ItemModel.GST_Rate);
                int chkGSTApply = Convert.ToInt16(ItemModel.GST_Apply);
                decimal MinLimit = Convert.ToDecimal(ItemModel.MinLimit);
                decimal MaxLimit = Convert.ToDecimal(ItemModel.MaxLimit);
                var CGS_Code = ItemModel.CGS_Code;
                int lstItemType = Convert.ToInt16(ItemModel.ItemType);
                int lstWidth = Convert.ToInt16(ItemModel.Length);
                int lstHeight = Convert.ToInt16(ItemModel.Height);
                int lstBarCodeCategory = Convert.ToInt16(ItemModel.BarCodeCategory);
                int lstColor = Convert.ToInt16(ItemModel.Color);
                string otherInFo = ItemModel.OtherInFo;
                
                string PerPieceCommission = ItemModel.PerPieceCommission == null || ItemModel.PerPieceCommission == string.Empty ? "0" : ItemModel.PerPieceCommission;
                string Year1 = ItemModel.Year1;
                string group = ItemModel.Group;
                string mSP = ItemModel.MSP == null || ItemModel.MSP == string.Empty ? "0" : ItemModel.MSP;

                SqlCommand cmd = new SqlCommand("Update InvCode set MSP='" + mSP + "', GroupId='" + group + "', OtherInFo='" + otherInFo + "', Color=" + lstColor + ", BarCodeCategory=" + lstBarCodeCategory + ", ItemType=" + lstItemType + ", CGS_Code='" + CGS_Code + "', Description='" + Description + "', Manufacturer=" + Manufacturer + ",ReorderLevel = " + ReOrdLevel + ", Brand = " + Convert.ToInt16(Brand) + ", Reference = '" + Reference + "',Reference2='" + Reference2 + "', Visiable=" + chkVisiable + ", PiecesInPacking=" + PiecesInPacking + ",length=" + lstWidth + ", Height=" + lstHeight + ", MinLimit=" + MinLimit + ",Maxlimit=" + MaxLimit + ", GST_Apply=" + chkGSTApply + ", GST_Rate=" + GSTRate + ", Active=" + chkActive + ",Revenue_Code='" + Revenue_Code + "', RegNo='" + "txtRegNo" + "', SellingCost=" + Convert.ToDecimal(SellingPrice) + ",  Nature=" + ItemNatureID + ", Packing=" + PackingID + ", Category=" + CategoryID + ", Class=" + ClassID + ",Godown=" + GoDownID + ",ReorderQty=" + ReOrdQuantity + ",OrderQTY=" + OrderQty + ",BonusQTY=" + BonusQty + ",AccountUnit=" + AccountUnitID + ",PerPieceCommission=" + PerPieceCommission + ",Year1='" + Year1 + "'  where CompId='" + CompID + "' and  code=" + ItemCode, con, tran);
                // ReorderLevel = " +  txtReorderLevel.Text & ", Brand = '" & brandcode & "', Reference = '" & txtReference1.Text & "' where CompID = '" & logon.CompID & "' and code = " & txtItemCode.Text
                //Reference2='" & txtReference2.Text & "', Visiable=" & Visiable & ", PiecesInPacking=" & txtPiecesInPacking.Text & " where

                cmd.ExecuteNonQuery();
                if (BarCode == "") { BarCode = Convert.ToString(ItemCode); }
                SqlCommand cmd2 = new SqlCommand("Insert into InvBarCode(ItemCode,BarCode,CompID) values(" + ItemCode + ",'" + BarCode + "','" + CompID + "')", con, tran);
                cmd2.ExecuteNonQuery();


                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Item Updated successfully" };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }
    }
}
