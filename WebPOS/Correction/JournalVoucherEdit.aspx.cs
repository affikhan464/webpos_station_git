﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Correction
{
    public partial class JournalVoucherEdit : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static string CompID = "01";
        static SqlTransaction tran;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(VoucherViewModel model)
        {
            try
            {
                if (con.State == ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                Module1 objModule1 = new Module1();
                ModGLCode objModGLCode = new ModGLCode();
                Module4 objModule4 = new Module4();
                var VoucherNo = model.VoucherNumber;

                var CompID = "01";


                SqlCommand cmdDelOld = new SqlCommand("Delete  from GeneralLedger where CompID='" + CompID + "' and V_No=" + Convert.ToDecimal(VoucherNo) + " and V_Type='JV'", con, tran);
                cmdDelOld.ExecuteNonQuery();
                
                SqlCommand cmdDelOld2 = new SqlCommand("Delete  from PartiesLedger where CompID='" + CompID + "' and V_No=" + Convert.ToDecimal(VoucherNo) + " and V_Type='JV'", con, tran);
                cmdDelOld2.ExecuteNonQuery();


                var selectedDate = model.VoucherDate;
                var convertedDate = DateTime.ParseExact(selectedDate + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);

                var VoucherDate = convertedDate.ToString("yyyy-MM-dd");

                foreach (var item in model.Rows)
                {
                    var GLCode = item.Code;
                    var Description = item.Title;
                    var Narration = item.Narration;
                    decimal AmountDr = Convert.ToDecimal(item.Debit);



                    if (AmountDr != 0)
                    {
                        SqlCommand cmdDr = new SqlCommand("insert into GeneralLedger (code,datedr,Description,Narration,AmountDr,V_NO,V_type,System_Date_Time,DescriptionOfBillNo,CompID) values('" + GLCode + "','" + VoucherDate + "','" + Narration + "','" + Narration + "'," + AmountDr + "," + VoucherNo + ",'" + "JV" + "','" + convertedDate + "','" + "JVThroughVoucher" + "','" + CompID + "')", con, tran);
                        cmdDr.ExecuteNonQuery();


                        if (GLCode.Substring(0, 8) == "01020101" || GLCode.Substring(0, 8) == "01010103")
                        {
                            SqlCommand cmdPart = new SqlCommand("insert into PartiesLedger (Code,DescriptionDr,AmountDr,V_No,V_Type,datedr,System_Date_Time,DescriptionOfBillNo,CompID) values('" + GLCode + "','" + Narration + "'," + AmountDr + "," + VoucherNo + ",'" + "JV" + "','" + VoucherDate + "','" + convertedDate + "','" + "JVThroughVoucher" + "','" + CompID + "')", con, tran);
                            cmdPart.ExecuteNonQuery();
                            objModule4.ClosingBalancePartiesNew(GLCode, con, tran);

                        }
                    }

                }

                foreach (var item in model.Rows)
                {
                    var GLCode = item.Code;
                    var Description = item.Title;
                    var Narration = item.Narration;
                    decimal AmountCr = Convert.ToDecimal(item.Credit);


                    if (AmountCr != 0)
                    {
                        SqlCommand cmdCr = new SqlCommand("insert into GeneralLedger (code,datedr,Description,Narration,AmountCr,V_NO,V_type,System_Date_Time,DescriptionOfBillNo,CompID) values('" + GLCode + "','" + VoucherDate + "','" + Narration + "','" + Narration + "'," + AmountCr + "," + VoucherNo + ",'" + "JV" + "','" + convertedDate + "','" + "JVThroughVoucher" + "','" + CompID + "')", con, tran);
                        cmdCr.ExecuteNonQuery();


                        if (GLCode.Substring(0, 8) == "01020101" || GLCode.Substring(0, 8) == "01010103")
                        {
                            SqlCommand cmdPartCr = new SqlCommand("insert into PartiesLedger (Code,DescriptionDr,AmountCr,V_No,V_Type,datedr,System_Date_Time,DescriptionOfBillNo,CompID) values('" + GLCode + "','" + Narration + "'," + AmountCr + "," + VoucherNo + ",'" + "JV" + "','" + VoucherDate + "','" + convertedDate + "','" + "JVThroughVoucher" + "','" + CompID + "')", con, tran);
                            cmdPartCr.ExecuteNonQuery();
                            objModule4.ClosingBalancePartiesNew(GLCode, con, tran);

                        }
                    }

                }


                //Save code for JournalVoucher

                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Saved Successfully!", LastVoucherNumber = Convert.ToString(VoucherNo) };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Delete(VoucherViewModel ModelPaymentVoucher)
        {

            Module1 objModule1 = new Module1();
            Module4 objModule4 = new Module4();

            try
            {


                if (con.State == ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                var VoucherNo = ModelPaymentVoucher.VoucherNumber;


                SqlCommand cmdDelOld = new SqlCommand("Delete  from GeneralLedger where CompID='" + CompID + "' and V_No=" + Convert.ToDecimal(VoucherNo) + " and V_Type='JV'", con, tran);
                cmdDelOld.ExecuteNonQuery();

                string GLCode = ModelPaymentVoucher.CreditGLCode;
                SqlCommand cmdDelOld2 = new SqlCommand("Delete  from PartiesLedger where CompID='" + CompID + "' and V_No=" + Convert.ToDecimal(VoucherNo) + " and V_Type='JV'", con, tran);
                cmdDelOld2.ExecuteNonQuery();

                objModule4.ClosingBalancePartiesNew(GLCode, con, tran);
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Deleted Successfully.", LastInvoiceNumber = VoucherNo.ToString() };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }
    }
}