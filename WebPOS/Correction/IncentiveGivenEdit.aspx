﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="IncentiveGivenEdit.aspx.cs" Inherits="WebPOS.IncentiveGivenEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <input type="hidden" id="isLastVoucher" />

    <div class="container">
        <div class="mt-4">

            <section class="form">
                <h2 class="form-header fa-money-bill-alt">Edit Incentive Expence Form</h2>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-4">
                        <span class="input input--hoshi">
                            <input id="txtVoucherNo" class="VoucherNo input__field input__field--hoshi" autocomplete="off" type="text" />

                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Voucher</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 d-flex">
                        <span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100" id="PreviousBtn" onclick="GetPreviousVoucherData()" data-toggle="tooltip" title="Previous Voucher"><i class="fas fa-chevron-circle-left"></i></a>
                        </span><span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100" data-toggle="tooltip" title="Get Voucher" onclick="GetVoucherData()"><i class="fas fa-spinner"></i></a>
                        </span>
                        <span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100" id="NextBtn" onclick="GetNextVoucherData()" data-toggle="tooltip" title="Next Voucher"><i class="fas fa-chevron-circle-right"></i></a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <input id="txtCurrentDate" class="datetimepicker Date input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Date</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtPartyName" data-type="Party" data-id="txtPartyName" data-nextfocus=".CashPaid" data-function="GetParty" name="PartyName" class=" PartyBox PartyName autocomplete empt input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Party Name</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <input id="txtOldPartyCode" hidden="hidden" disabled="disabled" name="OldPartyCode" class="input__field input__field--hoshi OldPartyCode empt" type="text" autocomplete="off" />

                            <input id="txtPartyCode" disabled="disabled" name="PartyCode" class="input__field input__field--hoshi PartyCode empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Party Code</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtAddress" class="input__field input__field--hoshi PartyAddress empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Address</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtCashPaid" class="input__field input__field--hoshi CashPaid empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Incentive Given</span>
                            </label>
                        </span>
                    </div>

                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtBalance" class="input__field input__field--hoshi PartyBalance empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Balance</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="OldBalance" class="input__field input__field--hoshi PartyBalanceActual empt" name="PartyBalance" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Actual Balance</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtNarration" class="input__field input__field--hoshi Narration empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Narration</span>
                            </label>
                        </span>
                    </div>

                </div>

                <hr />

                <div class="row  justify-content-end">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a onclick="deleteVoucher()" class=" btn btn-3 btn-bm btn-3e fa-trash w-100 btn-danger">Delete</a>
                        </span>

                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="SaveBefore()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                        </span>
                    </div>
                </div>

            </section>
        </div>
    </div>
</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    <script src="../Script/Autocomplete.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="../Script/Voucher/IncentiveGivenEdit_Save.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>


    <script type="text/javascript">
        $(document).ready(function () {
            var InvoiceNumber = getQueryString("v");

            if (InvoiceNumber != "" || InvoiceNumber != null) {
                $(".VoucherNo").val(InvoiceNumber);
                GetVoucherData();
            }
        });
        $(document).on('input', '.CashPaid', function () {
            calculation();
        })
    </script>

</asp:Content>
