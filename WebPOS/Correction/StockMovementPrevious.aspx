﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="StockMovementPrevious.aspx.cs" Inherits="WebPOS.Correction.StockMovementPrevious" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <input type="hidden" id="isLastVoucher" />

    <div class="container-fluid mb-3 header-inputs">
        <div class="mt-4">
            <section class="form">
                <h2 class="form-header ">All Previous Stock Movements</h2>
                <div class="row ">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 d-flex">
                        <span class="input input--hoshi">
                            <asp:TextBox ID="txtVoucherNo" ClientIDMode="Static" CssClass="InvoiceNumber input__field input__field--hoshi" name="Invoice" runat="server"></asp:TextBox>
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Bill #</span>
                            </label>
                        </span>
                        <span class="input input--hoshi w-15">
                            <a class="btn btn-3 btn-bm btn-3e w-100" id="PrintBtn" data-toggle="tooltip" title="Print"><i class="fas fa-print text-white"></i></a>
                        </span>
                        <span class="input input--hoshi w-15">
                            <a class="btn btn-3 btn-bm btn-3e w-100" href="#" id="PreviousBtn" onclick="GetPreviousVoucherData()" data-toggle="tooltip" title="Previous Voucher"><i class="fas fa-chevron-circle-left"></i></a>
                        </span>
                        <span class="input input--hoshi w-15">
                            <a class="btn btn-3 btn-bm btn-3e w-100" data-toggle="tooltip" title="Get Voucher" onclick="GetVoucherData()"><i class="fas fa-spinner"></i></a>
                        </span>
                        <span class="input input--hoshi w-15">
                            <a class="btn btn-3 btn-bm btn-3e w-100" id="NextBtn" onclick="GetNextVoucherData()" data-toggle="tooltip" title="Next Voucher"><i class="fas fa-chevron-circle-right"></i></a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                        <div class=" input--hoshi input--hoshi--dropdown input--filled">
                            <select id="MoveFromDD" class="round input__field--hoshi">
                            </select>
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">From </span>
                            </label>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                        <div class=" input--hoshi input--hoshi--dropdown input--filled">
                            <select id="MoveToDD" class="round input__field--hoshi">
                            </select>
                            <label class="input__label  input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">To </span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <span class="input input--hoshi">
                            <input class="datetimepicker Date input__field input__field--hoshi" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Date</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-10 col-sm-10 col-md-4 col-lg-4 col-xl-4">
                        <span class="input input--hoshi">
                            <input class="BC empty1 input__field input__field--hoshi empt" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">BC</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-2 col-sm-2 col-md-3 col-lg-2 col-xl-2">
                        <span class="input input--hoshi checkbox--hoshi">
                            <label>
                                <input class="checkbox" id="chkBox" type="checkbox" />
                                <span></span>

                            </label>
                        </span>
                    </div>
                </div>
                <div class="row">

                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <span class="input input--hoshi">
                            <input class="DR empty1 input__field input__field--hoshi" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">DR</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <span class="input input--hoshi">
                            <input id="SearchBox" data-id="itemTextbox" data-type="Item" autocomplete="off" data-function="GetItemsStockMovement" class=" input__field input__field--hoshi autocomplete" type="text" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi"><i class="fas fa-search"></i>Search Item </span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <span class="input input--hoshi">
                            <input class="Narration empty1 input__field input__field--hoshi" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Narration</span>
                            </label>
                        </span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a href="/Transactions/StockMovement.aspx" class="btn btn-3 btn-bm btn-3e w-100 fa-people-carry">Move New Stock </a>
                        </span>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="container-fluid">
        <div class="card sameheight-item items">
            <ul class="item-list striped">
                <li class="item item-list-header">
                    <div class="item-row">
                        <div class="item-col item-col-header flex-1">
                            <div>
                                <span>Sr #</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header  flex-2">
                            <div>
                                <span>Code</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header  flex-4">
                            <div>
                                <span>Description</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header  flex-1">
                            <div>
                                <span>Qty</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header  flex-2">
                            <div>
                                <span>Rate</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header  flex-2">
                            <div>
                                <span>Amount</span>
                            </div>
                        </div>
                    </div>
                </li>

            </ul>
            <ul class="item-list contentList striped" style="min-height:200px; overflow: auto">
            </ul>
        </div>

    </div>

</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    <script src="/Script/Autocomplete.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>

    <script src="../Script/Dropdown.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>

    <script type="text/javascript">

        $(document).ready(function () {
            var invCode = getQueryString("v");
            if (invCode != "" && invCode != null) {
                GetVoucherData(invCode);
                $(".InvoiceNumber").val(invCode);
            } else {
                invCode = $(".InvoiceNumber").val();
                GetVoucherData(invCode);

            }
            appendAttribute.init("MoveFromDD", "Station", 1);
            appendAttribute.init("MoveToDD", "Station", 2);
            resizeContentList();
        });
        function resizeContentList() {
            var headerHeight = $(".header-inputs").height() + $(".header").height() + 120;
            $(".contentList").css("height", "calc(100vh - " + headerHeight + "px)")
        }
        $(document).on('keydown', function (e) {
            if ((e.ctrlKey || e.metaKey) && (e.key == "p" || e.charCode == 16 || e.charCode == 112 || e.keyCode == 80)) { //Ctrl + P
                e.preventDefault();
                printStockMovement();
            }
        });
        function printStockMovement() {
            swal({
                title: 'Enter Your Password',
                type: 'info',
                input: 'password',
                showCancelButton: true,
                confirmButtonText: 'Verify !!',
                showLoaderOnConfirm: true,
                preConfirm: (text) => {
                    return new Promise((resolve) => {
                        if (text) {
                            $.ajax({
                                url: "/Default.aspx/VerifyUser",
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                data: JSON.stringify({ enteredPassword: text }),
                                success: function (BaseModel) {
                                    if (BaseModel.d.Success) {
                                        window.open(
                                            '/Transactions/TransactionPrints/StockMovementCR_A4.aspx?InvNo=' + $(".InvoiceNumber").val() + '&p=' + BaseModel.d.Data.p,
                                            '_blank'
                                        );
                                        resolve();
                                    } else if (BaseModel.d.Success == false && BaseModel.d.LoginAgain == true) {
                                        swal({
                                            title: "Login Again",
                                            html:
                                                `<a target="_blank" href="/?c=1" style="display:block;color:black;font-weight:700">Login Here and print it again.</a> `,

                                            type: "warning"

                                        });
                                    }
                                    else {
                                        swal.showValidationError(BaseModel.d.Message);
                                        resolve();
                                    }
                                }
                            });


                        } else {
                            swal.showValidationError("Enter some text");
                            resolve();
                        }
                    })
                },
                allowOutsideClick: () => !swal.isLoading()
            });
        }
        $(document).on('click', '#PrintBtn', function () {
            printStockMovement() 
    
        })
        function rerenderSerialNumber() {
            var rows = $(".item-list.contentList .item-data .SrNo");
            var sno = 0;
            rows.each(function (index, element) {
                sno += 1;
                $(element).html(sno);
            })
        }
        function itemAlreadyExist(code) {
            var rowNo = $(".item-list.contentList [data-itemcode=" + code + "]").data("rownumber");
            return rowNo;
        }
        function insertDataInStockMovement(data) {


            var code = $(".selectedItemCode").val();
            var name = $(".selectedItemDescription").val();
            var availableQty = $(".selectedItemStationFromQty").val();
            if (availableQty == "0") {
                playError();
                return false;
            }
            var qty = 1;
            var rate = Number($(".selectedItemRate").val());
            var amount = qty * rate;
            var lastRowNumber = $(".item-list.contentList li.item-data:last-child").length > 0 ? $(".item-list.contentList  li.item-data:last-child").data("rownumber") : 0;
            var SrNo = Number(lastRowNumber) + 1;

            if (data != undefined) {
                code = data.Code;
                name = data.Name;
                qty = data.Qty;
                rate = data.Rate;
                amount = data.Amount;
            }
            var itemExist = itemAlreadyExist(code);
            if (itemExist > 0) {
                Sno = itemExist;
                var qty = Number($("." + Sno + "_Qty").val()) + 1;
                $("." + Sno + "_Qty").val(qty).focus().select();

            } else {
                $(".item-list.contentList").append(`
                            <li data-rownumber='${SrNo}' data-itemcode="${code}" class="item item-data">
                                <div class="item-row">
                                    <div class="item-col flex-1">
                                        <div class="item-heading">Sr. No.</div>
                                        <div class='SrNo ${SrNo}_SrNo'></div>
                                    </div>
                                    <div class="item-col flex-2">
                                        <div class="item-heading">Code</div>
                                        <div class="${SrNo}_Code" >${code}</div>
                                    </div>
                                    <div class="item-col flex-4 no-overflow">
                                        <div>
                                            <a class="">
                                                <h4 class="item-title no-wrap ${SrNo}_Name">${name} </h4>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="item-col flex-1">
                                        <div class="item-heading">Qty</div>
                                        <div>${qty}</div>
                                    </div>
                                    <div class="item-col flex-2">
                                        <div class="item-heading">Rate</div>
                                        <div class="${SrNo}_Rate">${rate} </div>
                                    </div>
                                    <div class="item-col flex-2">
                                        <div class="item-heading">Amount</div>
                                        <div class="${SrNo}_Amount">${amount} </div>
                                    </div>
                                </div>
                            </li>`);
                $("." + SrNo + "_Qty").focus().select();
                rerenderSerialNumber();
            }

        }
        $(document).on("input", "[name=Qty]", function () {
            var srno = $(this).parents("li.item-data").data("rownumber");
            var rate = Number($("." + srno + "_Rate").text());
            var qty = Number($("." + srno + "_Qty").val());
            var amount = rate * qty;
            $("." + srno + "_Amount").text(amount)

        })
        $(document).on("keypress", "[name=Qty]", function (e) {
            if (e.keyCode == "13") {
                $("#SearchBox").focus().select();
            }
        })

     
      
        function clearInputs() {
            $("li.item-data").remove();
        }
        function GetVoucherData(InvCode) {
            $(".selected").remove();
            InvCode = InvCode == undefined ? $(".InvoiceNumber").val() : InvCode;
            $.ajax({
                url: '/WebPOSService.asmx/GetStockMovementVoucher',
                type: "Post",
                data: { "id": InvCode },
                success: function (InvoiceData) {
                    if (InvoiceData.length) {
                        clearInputs();
                        for (var i = 0; i < InvoiceData.length; i++) {
                            insertDataInStockMovement(InvoiceData[i]);
                        }

                        $("#isLastVoucher").val(InvoiceData[0].IsLastVoucherNo);
                        appendAttribute.init("MoveFromDD", "Station", InvoiceData[0].StationIdFrom);
                        appendAttribute.init("MoveToDD", "Station", InvoiceData[0].StationIdTo);
                        $(".Date").val(InvoiceData[0].Date);
                        $(".Narration").val(InvoiceData[0].Narration);
                        $("#SearchBox").select().focus();
                        manageNextPreviousBtn();
                        $(".fa-spinner").removeClass('fa-spin');
                        updateInputStyle();

                    } else {
                        smallSwal('No Voucher found', '', 'error');
                    }
                },
                fail: function (jqXhr, exception) {

                }
            });


        }


    </script>

</asp:Content>
