﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using WebPOS.Model;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS.Correction
{
    public partial class StockMovementEdit : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        protected void Page_Load(object sender, EventArgs e)
        {
            var objModule1 = new Module1();
            var voucherNo = objModule1.MaxStockMovementVoucher() - 1;
            txtVoucherNo.Text = voucherNo.ToString();
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(List<SaveVoucherViewModel> models)
        {
            var CompID = "01";
            try
            {
                var objModule4 = new Module4();
                var voucherNo = models.First().VoucherNumber;

                if (con.State == ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                var delcmd1 = new SqlCommand("Delete From InventoryMovement where SMONo=" + voucherNo, con, tran);
                delcmd1.ExecuteNonQuery();
                var delcmd2 = new SqlCommand("Delete From inventory where BillNo='" + voucherNo+"'", con, tran);
                delcmd2.ExecuteNonQuery();

                foreach (var model in models)
                {
                    var ItemCode = Convert.ToInt32(model.Code);
                    var Qty = Convert.ToInt32(model.Qty);
                    var ItemName = model.Name;
                    var FromStationCode = Convert.ToInt32(model.StationIdFrom);
                    var ToStationCode = Convert.ToInt32(model.StationIdTo);

                    var Amount = model.Amount;
                    var Rate = model.Rate;

                    var convertedDate = DateTime.ParseExact(model.Date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                       System.Globalization.CultureInfo.InvariantCulture);

                    var cmd1 = new SqlCommand("Insert into " +
                        "InventoryMovement (SMONo,Dat,ItemCode,ItemName,QTY,FromStation,ToStation,Narration,FromStationCode,ToStationCode,SNO,Amount,Rate,CompId) " +
                        "Values(" + voucherNo + ",'" + convertedDate + "'," + ItemCode + ",'" + ItemName + "'," + Qty + ",'" + model.StationNameFrom + "','" + model.StationNameTo + "','" + model.Narration + "'," + FromStationCode + "," + ToStationCode + "," + model.SNo + "," + Amount + "," + Rate + ",'" + CompID + "')", con, tran);
                    cmd1.ExecuteNonQuery();

                    var cmd2 = new SqlCommand("insert into inventory (Dat,Icode,Description,Issued,BillNo,DescriptionOfBillNo,System_Date_Time,StationID,CompID,Mutual)  " +
                        " values('" + convertedDate + "'," + ItemCode + ",'" + "Stock Move to " + model.StationNameTo + ",SMO #" + voucherNo + ",Narat:" + model.Narration + " '," + Qty + "," + voucherNo + ",'" + "StockMovement" + "','" + DateTime.Now + "'," + FromStationCode + ",'" + CompID + "'," + 1 + ")", con, tran);
                    cmd2.ExecuteNonQuery();

                    var cmd3 = new SqlCommand("insert into inventory (Dat,Icode,Description,Received,BillNo,DescriptionOfBillNo,System_Date_Time,StationID,CompID,Mutual) " +
                        "values('" + convertedDate + "'," + ItemCode + ",'" + "Stock Move From " + model.StationNameFrom + ",SMO #" + voucherNo + ",Narat:" + model.Narration + " '," + Qty + "," + voucherNo + ",'" + "StockMovement" + "','" + DateTime.Now + "'," + ToStationCode + ",'" + CompID + "'," + 1 + ")", con, tran);
                    cmd3.ExecuteNonQuery();

                    objModule4.ClosingBalanceNewTechniqueStationIssue(ItemCode, Qty, FromStationCode, tran, con);
                    objModule4.ClosingBalanceNewTechniqueStationReceived(ItemCode, Qty, ToStationCode, tran, con);

                    var cmd4 = new SqlCommand("insert into PrintingTable(Text_1,Text_2,Text_3) values('" + voucherNo + "','" + model.StationNameFrom + "','" + model.StationNameTo + "')", con, tran);
                    cmd4.ExecuteNonQuery();
                }

                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Saved Successfully!!", LastInvoiceNumber = voucherNo.ToString() };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }



    }

}