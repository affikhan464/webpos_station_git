﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using WebPOS.Model;

namespace WebPOS
{
    /// <summary>
    /// Summary description for WebPOSService
    /// </summary>
    [WebService(Namespace = "http://webpos.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class WebPOSService : System.Web.Services.WebService
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static string CashAccountGLCode = CompID + ConfigurationManager.AppSettings["CashAccountGLCode"].ToString();

        SqlConnection con = new SqlConnection(strConn);
        SqlCommand cmd = new SqlCommand();

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetItemDiscountPer(string itemCode, string partyCode)
        {
            try
            {

                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@ItemCode", itemCode.Trim());
                cmd.Parameters.AddWithValue("@PartyCode", partyCode.Trim());
                cmd.CommandText = @"
            Select ISNULL(BrandWiseDiscount.Disc_Rate, 0) as 'DiscountPer' from InvCode
			inner join BrandWiseDiscount on InvCode.Brand = BrandWiseDiscount.BrandCode
			inner join PartyCode on BrandWiseDiscount.PartyCode = PartyCode.Code
			where PartyCode.Code = @PartyCode and InvCode.code = @ItemCode ";

                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];

                List<Product> items = new List<Product>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string discountPer = data.Rows[i][0].ToString();

                    var product = new Product()
                    {
                        PerDis = discountPer

                    };
                    items.Add(product);
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(items));
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }
        }



        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetItemFullDetail(string itemCode)
        {
            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@ItemCode", itemCode.Trim());
                cmd.CommandText = @"
			
			Select 
					Code,
					BarCode,
					Description,
					[SellingCost],
					[Manufacturer],
					[ReorderLevel],
					[ReorderQty],
					[Brand],
					[Reference],
					[Reference2],
					[Visiable],
					[PiecesInPacking],
					[Packing],
					[Category],
					[Class],
					[Godown],
					[OrderQTY],
					[BonusQTY],
					[Nature],
					[AccountUnit],
					[Active],
					[Color],
					CompId,
					RegNo,
					MinLimit,
					[Ingridients],
					[GST_Rate], 
					GST_Apply,
					[Revenue_Code],  
					[CGS_Code],   
					[Height], 
					Length, 
					ItemType,                 
					MaxLimit
			From InvCode
			where Code = @ItemCode ";

                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];

                List<AddItemModel> items = new List<AddItemModel>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    for (int j = 0; j < 34; j++)
                    {
                        if (data.Rows[i][j] == null)
                        {
                        }
                    }
                    var isGST_Apply = data.Rows[i]["GST_Apply"].ToString() != "" ? data.Rows[i]["GST_Apply"] : false;
                    var isActive = data.Rows[i]["Active"].ToString() != "" ? data.Rows[i]["Active"] : false;

                    var product = new AddItemModel()
                    {
                        ItemCode = data.Rows[i]["Code"].ToString(),
                        BarCode = data.Rows[i]["BarCode"].ToString(),
                        Name = data.Rows[i]["Description"].ToString().Trim(),
                        SellingPriceOrUnit = data.Rows[i]["SellingCost"].ToString(),
                        ManufacturerId = data.Rows[i]["Manufacturer"].ToString(),
                        ReOrderPoint = data.Rows[i]["ReorderLevel"].ToString(),
                        ReOrderQty = data.Rows[i]["ReorderQty"].ToString(),
                        BrandId = data.Rows[i]["Brand"].ToString(),
                        Reference = data.Rows[i]["Reference"].ToString(),
                        Reference2 = data.Rows[i]["Reference2"].ToString(),
                        IsVisible = Convert.ToBoolean(data.Rows[i]["Visiable"]),
                        PiecesInPacking = data.Rows[i]["PiecesInPacking"].ToString(),
                        PackingId = data.Rows[i]["Packing"].ToString(),
                        CategoryId = data.Rows[i]["Category"].ToString(),
                        ClassId = data.Rows[i]["Class"].ToString(),
                        GoDownId = data.Rows[i]["Godown"].ToString(),
                        OrderQuantity = data.Rows[i]["OrderQTY"].ToString(),
                        BonusQuantity = data.Rows[i]["BonusQTY"].ToString(),
                        ItemNatureId = data.Rows[i]["Nature"].ToString(),
                        UnitOfMeasureId = data.Rows[i]["AccountUnit"].ToString(),
                        IsActive = Convert.ToBoolean(isActive),
                        ColorId = data.Rows[i]["Color"].ToString(),
                        Registration = data.Rows[i]["RegNo"].ToString(),
                        MinPrice = data.Rows[i]["MinLimit"].ToString(),
                        IssueIngridient = data.Rows[i]["Ingridients"].ToString() == string.Empty || data.Rows[i]["Ingridients"].ToString() == "0" ? false : true,
                        GST = data.Rows[i]["GST_Rate"].ToString(),
                        GST_Apply = Convert.ToBoolean(isGST_Apply),
                        IncomeAccount = data.Rows[i]["Revenue_Code"].ToString(),
                        COGSAccount = data.Rows[i]["CGS_Code"].ToString(),
                        HeightId = data.Rows[i]["Height"].ToString(),
                        LengthId = data.Rows[i]["Length"].ToString(),
                        ItemTypeId = data.Rows[i]["ItemType"].ToString(),
                        MaxPrice = data.Rows[i]["MaxLimit"].ToString()


                    };
                    items.Add(product);
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(items));
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                con.Close();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetRecords(string key, string stationId)
        {
            try
            {
                var module7 = new Module7();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@Description", key.Trim());
                cmd.CommandText = @"Select 
                    TOP 40
                    Description,
                    code,
                    qty as qty,
                    IsNull(( SELECT Sum(Current_QTY)
                        FROM InventoryOpeningBalance
                        Where ICode=InvCode.code and Dat='" + module7.StartDate().ToString("MM/dd/yyyy") + @"'
                    ),0) 
                    as tqty,
                    IsNull(( SELECT Current_QTY
                        FROM InventoryOpeningBalance
                        Where StationID=" + stationId + @" and ICode=InvCode.code and Dat='" + module7.StartDate().ToString("MM/dd/yyyy") + @"'
                    ),0) 
                    as stationqty,
                    SellingCost,
                    Ave_Cost,
                    IsNull(DealRs, 0) as DealRs,
                    IsNull(DealRs2, 0) as DealRs2,
                    IsNull(DealRs3, 0) as DealRs3,
                    Revenue_Code,
                    CGS_Code,
                    IsNull(PerPieceCommission, 0) as PerPieceCommission
                    
                from InvCode
                where Description like '%' + @Description + '%' or code  like '%' + @Description + '%' order by Description ";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];
                var modItem = new ModItem();
                var IsShowItemQtyAtZero = modItem.IsShowItemQtyAtZero(con);

                List<SearchItemModelStandard> items = new List<SearchItemModelStandard>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string Description = data.Rows[i]["Description"].ToString().Trim();
                    string Code = data.Rows[i]["code"].ToString();
                    string Qty = data.Rows[i]["tqty"].ToString();
                    string stationQty = data.Rows[i]["stationqty"].ToString();
                    string Rate = data.Rows[i]["SellingCost"].ToString();
                    string AverageCost = data.Rows[i]["Ave_Cost"].ToString();
                    string DealRs = data.Rows[i]["DealRs"].ToString();
                    string DealRs2 = data.Rows[i]["DealRs2"].ToString();
                    string DealRs3 = data.Rows[i]["DealRs3"].ToString();
                    string PerPieceCommission = data.Rows[i]["PerPieceCommission"].ToString();
                    string RevenueCode = data.Rows[i]["Revenue_Code"].ToString() == string.Empty ? "0103010100001" : data.Rows[i]["Revenue_Code"].ToString();
                    string CGSCode = data.Rows[i]["CGS_Code"].ToString() == string.Empty ? "0101010600001" : data.Rows[i]["CGS_Code"].ToString();
                    var searchItem = new SearchItemModelStandard()
                    {
                        Code = Code,
                        Description = Description,
                        TotalQty = Qty,
                        ThisStationQty = stationQty,
                        Rate = Rate,
                        AverageCost_Hide = AverageCost,
                        DealRs = DealRs,
                        DealRs2 = DealRs2,
                        DealRs3 = DealRs3,
                        RevenueCode_Hide = RevenueCode,
                        CGSCode_Hide = CGSCode,
                        ActualSellingPrice_Hide = Rate,

                        PerPieceCommission_Hide = PerPieceCommission

                    };



                    if (IsShowItemQtyAtZero)
                    {
                        items.Add(searchItem);
                    }
                    else if (Convert.ToInt32(searchItem.TotalQty) > 0 || Convert.ToInt32(searchItem.ThisStationQty) > 0)
                    {
                        items.Add(searchItem);
                    }
                }

                JavaScriptSerializer js = new JavaScriptSerializer();

                Context.Response.Clear();
                Context.Response.ContentType = "application/json";

                Context.Response.Write(js.Serialize(items.OrderBy(a => a.Description)));
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetPurchaseRecords(string key, string stationId)
        {
            try
            {
                if (con.State == ConnectionState.Closed) { con.Open(); }
                var modItem = new ModItem();
                var IsSelectLastPurchaseRate = modItem.IsSelectLastPurchaseRate(con);
                var LastPurchaseRate = string.Empty;
                if (IsSelectLastPurchaseRate)
                {
                    LastPurchaseRate =
                        @",
                    IsNull((SELECT Top 1 Purchase2.Cost
                        FROM Purchase2
                        Where CompID='" + CompID + "' and Purchase2.Code=InvCode.code order by Purchase2.System_Date_Time Desc" +
                            "),0) as LastPurchaseRate ";

                }
                var module7 = new Module7();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@Description", key.Trim());
                cmd.CommandText = @"Select 
                    TOP 40
                    Description,
                    code,
                    qty as qty,
                    IsNull(( SELECT Sum(Current_QTY)
                        FROM InventoryOpeningBalance
                        Where ICode=InvCode.code and Dat='" + module7.StartDate().ToString("MM/dd/yyyy") + @"'
                    ),0) 
                    as tqty,
                    IsNull(( SELECT Current_QTY
                        FROM InventoryOpeningBalance
                        Where StationID=" + stationId + @" and ICode=InvCode.code and Dat='" + module7.StartDate().ToString("MM/dd/yyyy") + @"'
                    ),0) 
                    as stationqty,
                    PurchasingCost,
                    SellingCost,
                    Ave_Cost,
                    IsNull(DealRs, 0) as DealRs,
                    IsNull(DealRs2, 0) as DealRs2,
                    IsNull(DealRs3, 0) as DealRs3,
                    Revenue_Code,
                    CGS_Code
                    " + LastPurchaseRate + @",
                    IsNull(PerPieceCommission, 0) as PerPieceCommission
                    
                from InvCode
                where Description like '%' + @Description + '%' or code  like '%' + @Description + '%' order by Description";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];


                List<SearchItemModelStandard> items = new List<SearchItemModelStandard>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string Description = data.Rows[i]["Description"].ToString().Trim();
                    string Code = data.Rows[i]["code"].ToString();
                    string Qty = data.Rows[i]["tqty"].ToString();
                    string stationQty = data.Rows[i]["stationqty"].ToString();

                    string Rate = data.Rows[i]["PurchasingCost"].ToString();
                    string SellingPrice = data.Rows[i]["SellingCost"].ToString();
                    string LastPRate = IsSelectLastPurchaseRate ? data.Rows[i]["LastPurchaseRate"].ToString() : "0";

                    string AverageCost = data.Rows[i]["Ave_Cost"].ToString();
                    string DealRs = data.Rows[i]["DealRs"].ToString();
                    string DealRs2 = data.Rows[i]["DealRs2"].ToString();
                    string DealRs3 = data.Rows[i]["DealRs3"].ToString();
                    string PerPieceCommission = data.Rows[i]["PerPieceCommission"].ToString();
                    string RevenueCode = data.Rows[i]["Revenue_Code"].ToString() == string.Empty ? "0103010100001" : data.Rows[i]["Revenue_Code"].ToString();
                    string CGSCode = data.Rows[i]["CGS_Code"].ToString() == string.Empty ? "0101010600001" : data.Rows[i]["CGS_Code"].ToString();
                    var searchItem = new SearchItemModelStandard()
                    {
                        Code = Code,
                        Description = Description,
                        TotalQty = Qty,
                        ThisStationQty = stationQty,
                        Rate = Rate,
                        AverageCost_Hide = AverageCost,
                        DealRs = DealRs,
                        DealRs2 = DealRs2,
                        DealRs3 = DealRs3,
                        RevenueCode_Hide = RevenueCode,
                        CGSCode_Hide = CGSCode,
                        ActualSellingPrice_Hide = SellingPrice,

                        PerPieceCommission_Hide = PerPieceCommission,

                    };

                    if (IsSelectLastPurchaseRate)
                    {
                        searchItem.Rate = Convert.ToInt32(LastPRate) > 0 ? LastPRate : Rate;
                        searchItem.IsRateLastPurchaseRate_Hide = Convert.ToInt32(LastPRate) > 0 && LastPRate != Rate;

                        searchItem.OriginalRate_Hide = Rate;
                    }
                    items.Add(searchItem);
                }

                JavaScriptSerializer js = new JavaScriptSerializer();

                Context.Response.Clear();
                Context.Response.ContentType = "application/json";

                Context.Response.Write(js.Serialize(items.OrderBy(a => a.Description)));

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }

        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetSaleReturnRecords(string key, string stationId)
        {
            try
            {
                if (con.State == ConnectionState.Closed) { con.Open(); }
                string PartyCode = key.Split(',')[1];
                string itemName = key.Split(',')[0];
                var modItem = new ModItem();
                //var IsSelectLastPurchaseRate = modItem.IsSelectLastPurchaseRate(con);

                var module7 = new Module7();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@Description", itemName.Trim());
                cmd.CommandText = @"Select 
                   TOP 40
                    Description,
                    code,
                    qty as qty,
                    IsNull(( SELECT Sum(Current_QTY)
                        FROM InventoryOpeningBalance
                        Where ICode=InvCode.code and Dat='" + module7.StartDate().ToString("yyyy/MM/dd") + @"'
                    ),0) 
                    as tqty,
                    IsNull(( SELECT Current_QTY
                        FROM InventoryOpeningBalance
                        Where StationID=" + stationId + @" and ICode=InvCode.code and Dat='" + module7.StartDate().ToString("yyyy/MM/dd") + @"'
                    ),0) 
                    as stationqty,
                    SellingCost,
                    Ave_Cost,
                    IsNull(DealRs, 0) as DealRs,
                    IsNull(DealRs2, 0) as DealRs2,
                    IsNull(DealRs3, 0) as DealRs3,
                    Revenue_Code,
                    CGS_Code,
                    IsNull((SELECT Top 1 Invoice2.Rate
                        FROM Invoice2
                        Join Invoice1 on Invoice2.InvNo=Invoice1.InvNo
                        Where Invoice1.CompID='" + CompID + "' and Invoice1.StationId=" + stationId + " and Invoice1.PartyCode='" + PartyCode + @"' and Invoice2.Code=InvCode.code order by Invoice1.System_Date_Time Desc),0) as LastSaleRate
                    ,
                    IsNull(PerPieceCommission, 0) as PerPieceCommission
                    
                from InvCode
                where Description like '%' + @Description + '%' or code  like '%' + @Description + '%' order by Description";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];


                List<SearchItemModelStandard> items = new List<SearchItemModelStandard>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string Description = data.Rows[i]["Description"].ToString().Trim();
                    string Code = data.Rows[i]["code"].ToString();
                    string Qty = data.Rows[i]["tqty"].ToString();
                    string stationQty = data.Rows[i]["stationqty"].ToString();
                    string Rate = data.Rows[i]["SellingCost"].ToString();
                    string AverageCost = data.Rows[i]["Ave_Cost"].ToString();
                    string DealRs = data.Rows[i]["DealRs"].ToString();
                    string DealRs2 = data.Rows[i]["DealRs2"].ToString();
                    string DealRs3 = data.Rows[i]["DealRs3"].ToString();
                    string PerPieceCommission = data.Rows[i]["PerPieceCommission"].ToString();
                    string RevenueCode = data.Rows[i]["Revenue_Code"].ToString() == string.Empty ? "0103010100001" : data.Rows[i]["Revenue_Code"].ToString();
                    string CGSCode = data.Rows[i]["CGS_Code"].ToString() == string.Empty ? "0101010600001" : data.Rows[i]["CGS_Code"].ToString();
                    string LastSRate = data.Rows[i]["LastSaleRate"].ToString();

                    var searchItem = new SearchItemModelStandard()
                    {
                        Code = Code,
                        Description = Description,
                        TotalQty = Qty,
                        ThisStationQty = stationQty,
                        Rate = Convert.ToInt32(LastSRate) > 0 ? LastSRate : Rate,
                        AverageCost_Hide = AverageCost,
                        DealRs = DealRs,
                        DealRs2 = DealRs2,
                        DealRs3 = DealRs3,
                        RevenueCode_Hide = RevenueCode,
                        CGSCode_Hide = CGSCode,
                        ActualSellingPrice_Hide = Rate,
                        PerPieceCommission_Hide = PerPieceCommission,
                        IsRateLastSoldRate_Hide = Convert.ToInt32(LastSRate) > 0 && LastSRate != Rate,
                        OriginalRate_Hide = Rate
                    };
                    items.Add(searchItem);
                }

                JavaScriptSerializer js = new JavaScriptSerializer();

                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(items.OrderBy(a => a.Description)));
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetPurchaseReturnRecords(string key, string stationId)
        {
            try
            {
                if (con.State == ConnectionState.Closed) { con.Open(); }
                string PartyCode = key.Split(',')[1];
                string itemName = key.Split(',')[0];
                var modItem = new ModItem();
                var IsSelectLastPurchaseRate = modItem.IsSelectLastPurchaseRate(con);
                var LastPurchaseRate = string.Empty;

                var module7 = new Module7();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@Description", itemName.Trim());
                cmd.CommandText = @"Select 
                    TOP 40
                    Description,
                    code,
                    qty as qty,
                    IsNull(( SELECT Sum(Current_QTY)
                        FROM InventoryOpeningBalance
                        Where ICode=InvCode.code and Dat='" + module7.StartDate().ToString("yyyy/MM/dd") + @"'
                    ),0) 
                    as tqty,
                    IsNull(( SELECT Current_QTY
                        FROM InventoryOpeningBalance
                        Where StationID=" + stationId + @" and ICode=InvCode.code and Dat='" + module7.StartDate().ToString("yyyy/MM/dd") + @"'
                    ),0) 
                    as stationqty,
                    PurchasingCost,
                    SellingCost,
                    Ave_Cost,
                    IsNull(DealRs, 0) as DealRs,
                    IsNull(DealRs2, 0) as DealRs2,
                    IsNull(DealRs3, 0) as DealRs3,
                    Revenue_Code,
                    CGS_Code,
                    IsNull((SELECT Top 1 Purchase2.Cost
                        FROM Purchase2
                        Join Purchase1 on Purchase2.InvNo=Purchase1.InvNo
                        Where Purchase1.CompID='" + CompID + "' and Purchase1.StationId=" + stationId + " and Purchase1.VenderCode='" + PartyCode + @"' and Purchase2.Code=InvCode.code order by Purchase2.System_Date_Time Desc),0) as LastPurchaseRate
                    ,
                    IsNull(PerPieceCommission, 0) as PerPieceCommission
                    
                from InvCode
                where Description like '%' + @Description + '%' or code  like '%' + @Description + '%' order by Description";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];


                List<SearchItemModelStandard> items = new List<SearchItemModelStandard>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string Description = data.Rows[i]["Description"].ToString().Trim();
                    string Code = data.Rows[i]["code"].ToString();
                    string Qty = data.Rows[i]["tqty"].ToString();
                    string stationQty = data.Rows[i]["stationqty"].ToString();
                    string Rate = data.Rows[i]["PurchasingCost"].ToString();
                    string LastPRate = data.Rows[i]["LastPurchaseRate"].ToString();
                    string SellingPrice = data.Rows[i]["SellingCost"].ToString();
                    string AverageCost = data.Rows[i]["Ave_Cost"].ToString();
                    string DealRs = data.Rows[i]["DealRs"].ToString();
                    string DealRs2 = data.Rows[i]["DealRs2"].ToString();
                    string DealRs3 = data.Rows[i]["DealRs3"].ToString();
                    string PerPieceCommission = data.Rows[i]["PerPieceCommission"].ToString();
                    string RevenueCode = data.Rows[i]["Revenue_Code"].ToString() == string.Empty ? "0103010100001" : data.Rows[i]["Revenue_Code"].ToString();
                    string CGSCode = data.Rows[i]["CGS_Code"].ToString() == string.Empty ? "0101010600001" : data.Rows[i]["CGS_Code"].ToString();
                    var searchItem = new SearchItemModelStandard()
                    {
                        Code = Code,
                        Description = Description,
                        TotalQty = Qty,
                        ThisStationQty = stationQty,
                        Rate = Convert.ToInt32(LastPRate) > 0 ? LastPRate : Rate,
                        AverageCost_Hide = AverageCost,
                        DealRs = DealRs,
                        DealRs2 = DealRs2,
                        DealRs3 = DealRs3,
                        RevenueCode_Hide = RevenueCode,
                        CGSCode_Hide = CGSCode,
                        ActualSellingPrice_Hide = SellingPrice,
                        PerPieceCommission_Hide = PerPieceCommission,
                        IsRateLastPurchaseRate_Hide = Convert.ToInt32(LastPRate) > 0 && LastPRate != Rate,
                        OriginalRate_Hide = Rate

                    };
                    items.Add(searchItem);
                }

                JavaScriptSerializer js = new JavaScriptSerializer();

                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(items.OrderBy(a => a.Description)));
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetRecordsAgainstIME(string key)
        {
            try
            {

                var modItem = new ModItem();
                string PartyCode = key.Split(',')[1];
                string IME = key.Split(',')[0];
                var itemCode = modItem.ItemCodeAgainstIME(IME);
                string DealApplyNo = key.Split(',')[2];
                var barCodeCategory = modItem.BarcodeCategoryAgainstItemCode(itemCode);



                ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();


                var items = new List<SearchItemModelStandardIME>();
                if (barCodeCategory == 1)  // 1 mean mobile
                {
                    //check length and available for sale and already selected=no
                    var check = modItem.isIMESold(itemCode.ToString(), IME);


                    if (!check.isSold && check.isItemMatch)
                    {


                        var cmd = new SqlCommand(@"Select 
                    TOP 40
                    Description,
                    code,
                    qty as tqty,
                    SellingCost,
                    Ave_Cost,
                    IsNull(DealRs, 0) as DealRs,
                    IsNull(DealRs2, 0) as DealRs2,
                    IsNull(DealRs3, 0) as DealRs3,
                    Revenue_Code,
                    CGS_Code,
                    IsNull(PerPieceCommission, 0) as PerPieceCommission
                    
                from InvCode
                where code=" + itemCode + "  order by Description", con);
                        DataSet objDs = new DataSet();
                        SqlDataAdapter dAdapter = new SqlDataAdapter();
                        dAdapter.SelectCommand = cmd;

                        dAdapter.Fill(objDs);

                        var data = objDs.Tables[0];

                        int i = 0;
                        string Description = data.Rows[i]["Description"].ToString().Trim();
                        string Code = data.Rows[i]["code"].ToString();
                        string Qty = "1";
                        string Rate = data.Rows[i]["SellingCost"].ToString();
                        string AverageCost = data.Rows[i]["Ave_Cost"].ToString();

                        //if autopercentagechk dealrs=true then dealrs=
                        decimal PerDisRate = objModPartyCodeAgainstName.BrandWiseDiscountRateAgainstPartyCode(PartyCode, Convert.ToString(itemCode));
                        decimal DealDisc = modItem.DealRsAgainstItemCode(Convert.ToString(itemCode), DealApplyNo);

                        string RevenueCode = data.Rows[i]["Revenue_Code"].ToString() == string.Empty ? "0103010100001" : data.Rows[i]["Revenue_Code"].ToString();
                        string CGSCode = data.Rows[i]["CGS_Code"].ToString() == string.Empty ? "0101010600001" : data.Rows[i]["CGS_Code"].ToString();
                        string PerPieceCommission = data.Rows[i]["PerPieceCommission"].ToString();

                        var searchItem = new SearchItemModelStandardIME()
                        {
                            Code = Code,
                            Description = Description,
                            Qty = Qty,
                            Rate = Rate,
                            avrgCost = AverageCost,
                            ItemDis = 0,
                            PerDis = PerDisRate,

                            DealDis = DealDisc,

                            revCode = RevenueCode,
                            cgsCode = CGSCode,
                            actualSellingPrice = Rate,
                            isIME = true,
                            isItemAvailble = check.isItemAvailable,
                            BarCodeCategory = barCodeCategory,
                            PerPieceCommission = PerPieceCommission
                        };
                        items.Add(searchItem);
                        con.Close();
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        Context.Response.Clear();
                        Context.Response.ContentType = "application/json";
                        Context.Response.Write(js.Serialize(items));
                    }
                    else
                    {
                        var searchItem = new SearchItemModelStandardIME()
                        {

                            isIME = true,
                            isItemAvailble = check.isItemAvailable
                        };
                        items.Add(searchItem);

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        Context.Response.Clear();
                        Context.Response.ContentType = "application/json";
                        Context.Response.Write(js.Serialize(items));
                    }
                }
                else if (barCodeCategory == 2)
                {

                    var cmd = new SqlCommand(@"Select 
                    TOP 40
                    Description,
                    code,
                    qty as tqty,
                    SellingCost,
                    Ave_Cost,
                    IsNull(DealRs, 0) as DealRs,
                    IsNull(DealRs2, 0) as DealRs2,
                    IsNull(DealRs3, 0) as DealRs3,
                    Revenue_Code,
                    CGS_Code,
                    IsNull(PerPieceCommission, 0) as PerPieceCommission
                    
                from InvCode
                where code=" + itemCode, con);
                    DataSet objDs = new DataSet();
                    SqlDataAdapter dAdapter = new SqlDataAdapter();
                    dAdapter.SelectCommand = cmd;

                    dAdapter.Fill(objDs);

                    var data = objDs.Tables[0];

                    int i = 0;
                    string Description = data.Rows[i]["Description"].ToString().Trim();
                    string Code = data.Rows[i]["code"].ToString();
                    string Qty = "1";
                    string Rate = data.Rows[i]["SellingCost"].ToString();
                    string AverageCost = data.Rows[i]["Ave_Cost"].ToString();

                    //if autopercentagechk dealrs=true then dealrs=
                    decimal PerDisRate = objModPartyCodeAgainstName.BrandWiseDiscountRateAgainstPartyCode(PartyCode, Convert.ToString(itemCode));
                    decimal DealDisc = modItem.DealRsAgainstItemCode(Convert.ToString(itemCode), DealApplyNo);

                    string RevenueCode = data.Rows[i]["Revenue_Code"].ToString() == string.Empty ? "0103010100001" : data.Rows[i]["Revenue_Code"].ToString();
                    string CGSCode = data.Rows[i]["CGS_Code"].ToString() == string.Empty ? "0101010600001" : data.Rows[i]["CGS_Code"].ToString();

                    string PerPieceCommission = data.Rows[i]["PerPieceCommission"].ToString();

                    var searchItem = new SearchItemModelStandardIME()
                    {
                        Code = Code,
                        Description = Description,
                        Qty = Qty,
                        Rate = Rate,
                        avrgCost = AverageCost,
                        ItemDis = 0,
                        PerDis = PerDisRate,

                        DealDis = DealDisc,

                        revCode = RevenueCode,
                        cgsCode = CGSCode,
                        actualSellingPrice = Rate,
                        isIME = true,
                        isItemAvailble = true,
                        BarCodeCategory = barCodeCategory,
                        PerPieceCommission = PerPieceCommission




                    };
                    items.Add(searchItem);
                    con.Close();
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    Context.Response.Clear();
                    Context.Response.ContentType = "application/json";
                    Context.Response.Write(js.Serialize(items));



                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }




        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetRecordSR(string key, string stationId)
        {
            try
            {
                var modItem = new ModItem();
                string PartyCode = key.Split(',')[1];
                string IME = key.Split(',')[0];
                var itemCode = modItem.ItemCodeAgainstIME(IME);
                string DealApplyNo = key.Split(',')[2];
                var barCodeCategory = modItem.BarcodeCategoryAgainstItemCode(itemCode);
                ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();

                if (barCodeCategory == 1)  // 1 mean mobile
                {

                    var check = modItem.WeHaveSoldSaleReturn(itemCode.ToString(), IME, PartyCode, stationId);
                    var items = new List<SearchItemModelStandardIME>();

                    if (check.isSold)
                    {

                        var cmd = new SqlCommand(@"Select 
                    TOP 40
                    Description,
                    code,
                    qty as tqty,
                    SellingCost,
                    Ave_Cost,
                    IsNull(DealRs, 0) as DealRs,
                    IsNull(DealRs2, 0) as DealRs2,
                    IsNull(DealRs3, 0) as DealRs3,
                    Revenue_Code,
                    CGS_Code
                    
                from InvCode
                where code=" + itemCode + " order by Description", con);
                        DataSet objDs = new DataSet();
                        SqlDataAdapter dAdapter = new SqlDataAdapter();
                        dAdapter.SelectCommand = cmd;

                        dAdapter.Fill(objDs);

                        var data = objDs.Tables[0];

                        int i = 0;
                        string Description = data.Rows[i]["Description"].ToString().Trim();
                        string Code = data.Rows[i]["code"].ToString();
                        string Qty = "1";
                        string Rate = Convert.ToString(check.Rate);
                        string AverageCost = data.Rows[i]["Ave_Cost"].ToString();


                        decimal PerDisRate = check.DisPer;
                        decimal DealDisc = check.DealRs;

                        string RevenueCode = data.Rows[i]["Revenue_Code"].ToString() == string.Empty ? "0103010100001" : data.Rows[i]["Revenue_Code"].ToString();
                        string CGSCode = data.Rows[i]["CGS_Code"].ToString() == string.Empty ? "0101010600001" : data.Rows[i]["CGS_Code"].ToString();

                        var searchItem = new SearchItemModelStandardIME()
                        {
                            Code = Code,
                            Description = Description,
                            Qty = Qty,
                            Rate = Rate,
                            avrgCost = AverageCost,
                            ItemDis = 0,
                            PerDis = PerDisRate,

                            DealDis = DealDisc,

                            revCode = RevenueCode,
                            cgsCode = CGSCode,
                            actualSellingPrice = Rate,
                            isIMESR = true,
                            isItemAvailble = check.isItemAvailable

                        };
                        items.Add(searchItem);
                        con.Close();
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        Context.Response.Clear();
                        Context.Response.ContentType = "application/json";
                        Context.Response.Write(js.Serialize(items));
                    }
                    else
                    {
                        var searchItem = new SearchItemModelStandardIME()
                        {

                            isIMESR = true,
                            isItemAvailble = check.isItemAvailable
                        };
                        items.Add(searchItem);

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        Context.Response.Clear();
                        Context.Response.ContentType = "application/json";
                        Context.Response.Write(js.Serialize(items));
                    }
                }
                else if (barCodeCategory == 2)
                {

                }


            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }


        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetRecordLPReceiving(string key, string stationdId)
        {
            try
            {
                var modItem = new ModItem();
                string PartyCode = key.Split(',')[1];
                string IME = key.Split(',')[0];
                var itemCode = modItem.ItemCodeAgainstIME(IME);
                var item = modItem.GetItemByCode(itemCode);
                decimal LPRate = modItem.LPRateAgainstItemCode(Convert.ToDecimal(itemCode));
                ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
                string AlreadyLPPaid = modItem.AlreadyLPPaid(IME);
                var check = modItem.WeHaveSoldSaleReturn(itemCode.ToString(), IME, PartyCode, stationdId);
                var items = new List<SearchItemModelStandardIME>();
                if (check.isSold && AlreadyLPPaid == "No")
                {
                    var searchItem = new SearchItemModelStandardIME()
                    {
                        Code = item.Code,
                        Description = item.Description,
                        BrandName = item.BrandName,
                        BrandCode = item.BrandCode,
                        IME = IME,
                        LP = item.LP
                    };
                    items.Add(searchItem);
                    con.Close();
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    Context.Response.Clear();
                    Context.Response.ContentType = "application/json";
                    Context.Response.Write(js.Serialize(items));
                }
                else
                {
                    //var searchItem = new SearchItemModelStandardIME()
                    //{

                    //    isIMESR = true,
                    //    isItemAvailble = check.isItemAvailable
                    //};
                    //items.Add(searchItem);

                    //JavaScriptSerializer js = new JavaScriptSerializer();
                    //Context.Response.Clear();
                    //Context.Response.ContentType = "application/json";
                    //Context.Response.Write(js.Serialize(items));
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetRecordPR(string key, string stationId)
        {
            try
            {
                var modItem = new ModItem();
                string PartyCode = key.Split(',')[1];
                string IME = key.Split(',')[0];
                var itemCode = modItem.ItemCodeAgainstIME(IME);
                string DealApplyNo = key.Split(',')[2];
                var barCodeCategory = modItem.BarcodeCategoryAgainstItemCode(itemCode);
                ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();

                if (barCodeCategory == 1)  // 1 mean mobile
                {

                    var check = modItem.WeHavePurchasedFrom(itemCode.ToString(), IME, PartyCode, stationId);
                    var items = new List<SearchItemModelStandardIME>();

                    if (check.isPurchased)
                    {

                        var cmd = new SqlCommand(@"Select 
                    TOP 40
                    Description,
                    code,
                    qty as tqty,
                    SellingCost,
                    Ave_Cost,
                    IsNull(DealRs, 0) as DealRs,
                    IsNull(DealRs2, 0) as DealRs2,
                    IsNull(DealRs3, 0) as DealRs3,
                    Revenue_Code,
                    CGS_Code
                    
                from InvCode
                where code=" + itemCode + " order by Description", con);
                        DataSet objDs = new DataSet();
                        SqlDataAdapter dAdapter = new SqlDataAdapter();
                        dAdapter.SelectCommand = cmd;

                        dAdapter.Fill(objDs);

                        var data = objDs.Tables[0];

                        int i = 0;
                        string Description = data.Rows[i]["Description"].ToString().Trim();
                        string Code = data.Rows[i]["code"].ToString();
                        string Qty = "1";
                        string Rate = Convert.ToString(check.Rate);
                        string AverageCost = data.Rows[i]["Ave_Cost"].ToString();


                        decimal PerDisRate = check.DisPer;
                        decimal DealDisc = check.DealRs;

                        string RevenueCode = data.Rows[i]["Revenue_Code"].ToString() == string.Empty ? "0103010100001" : data.Rows[i]["Revenue_Code"].ToString();
                        string CGSCode = data.Rows[i]["CGS_Code"].ToString() == string.Empty ? "0101010600001" : data.Rows[i]["CGS_Code"].ToString();

                        var searchItem = new SearchItemModelStandardIME()
                        {
                            Code = Code,
                            Description = Description,
                            Qty = Qty,
                            Rate = Rate,
                            avrgCost = AverageCost,
                            ItemDis = 0,
                            PerDis = PerDisRate,

                            DealDis = DealDisc,

                            revCode = RevenueCode,
                            cgsCode = CGSCode,
                            actualSellingPrice = Rate,
                            isIMEPR = true,
                            isItemAvailble = check.isItemAvailable

                        };
                        items.Add(searchItem);
                        con.Close();
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        Context.Response.Clear();
                        Context.Response.ContentType = "application/json";
                        Context.Response.Write(js.Serialize(items));
                    }
                    else
                    {
                        var searchItem = new SearchItemModelStandardIME()
                        {

                            isIMEPR = true,
                            isItemAvailble = check.isItemAvailable
                        };
                        items.Add(searchItem);

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        Context.Response.Clear();
                        Context.Response.ContentType = "application/json";
                        Context.Response.Write(js.Serialize(items));
                    }
                }
                else if (barCodeCategory == 2)
                {

                }


            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }


        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetRecordP(string key)
        {
            try
            {
                var modItem = new ModItem();
                string PartyCode = key.Split(',')[1];
                string IME = key.Split(',')[0];
                var itemCode = modItem.ItemCodeAgainstIME(IME);
                string DealApplyNo = key.Split(',')[2];
                var barCodeCategory = modItem.BarcodeCategoryAgainstItemCode(itemCode);
                ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();

                if (barCodeCategory == 1)  // 1 mean mobile
                {

                    var check = modItem.isIMEPurchased(itemCode.ToString(), IME);
                    var items = new List<SearchItemModelStandardIME>();

                    if (!check.isPurchased)
                    {

                        var cmd = new SqlCommand(@"Select 
                    TOP 40
                    Description,
                    code,
                    qty as tqty,
                    SellingCost,
                    Ave_Cost,
                    IsNull(DealRs, 0) as DealRs,
                    IsNull(DealRs2, 0) as DealRs2,
                    IsNull(DealRs3, 0) as DealRs3,
                    Revenue_Code,
                    CGS_Code
                    
                from InvCode
                where code=" + itemCode + " order by Description", con);
                        DataSet objDs = new DataSet();
                        SqlDataAdapter dAdapter = new SqlDataAdapter();
                        dAdapter.SelectCommand = cmd;

                        dAdapter.Fill(objDs);

                        var data = objDs.Tables[0];

                        int i = 0;
                        string Description = data.Rows[i]["Description"].ToString().Trim();
                        string Code = data.Rows[i]["code"].ToString();
                        string Qty = "1";
                        string Rate = Convert.ToString(check.Rate);
                        string AverageCost = data.Rows[i]["Ave_Cost"].ToString();


                        decimal PerDisRate = check.DisPer;
                        decimal DealDisc = check.DealRs;

                        string RevenueCode = data.Rows[i]["Revenue_Code"].ToString() == string.Empty ? "0103010100001" : data.Rows[i]["Revenue_Code"].ToString();
                        string CGSCode = data.Rows[i]["CGS_Code"].ToString() == string.Empty ? "0101010600001" : data.Rows[i]["CGS_Code"].ToString();

                        var searchItem = new SearchItemModelStandardIME()
                        {
                            Code = Code,
                            Description = Description,
                            Qty = Qty,
                            Rate = Rate,
                            avrgCost = AverageCost,
                            ItemDis = 0,
                            PerDis = PerDisRate,

                            DealDis = DealDisc,

                            revCode = RevenueCode,
                            cgsCode = CGSCode,
                            actualSellingPrice = Rate,
                            isIMEP = true,
                            isItemAvailble = check.isItemAvailable

                        };
                        items.Add(searchItem);
                        con.Close();
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        Context.Response.Clear();
                        Context.Response.ContentType = "application/json";
                        Context.Response.Write(js.Serialize(items));
                    }
                    else
                    {
                        var searchItem = new SearchItemModelStandardIME()
                        {
                            isIMEP = true,
                            isItemAvailble = check.isItemAvailable
                        };
                        items.Add(searchItem);

                        JavaScriptSerializer js = new JavaScriptSerializer();
                        Context.Response.Clear();
                        Context.Response.ContentType = "application/json";
                        Context.Response.Write(js.Serialize(items));
                    }
                }
                else if (barCodeCategory == 2)
                {

                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetSimpleItem(string key)
        {
            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@Description", key.Trim());
                cmd.CommandText = "Select TOP 40 Description,code,qty,SellingCost,Ave_Cost,IsNull(DealRs,0) as DealRs,IsNull(DealRs2,0) as DealRs2,IsNull(DealRs3,0) as DealRs3,Revenue_Code,CGS_Code from InvCode where Description like '%'+@Description+'%' OR  Code like '%'+@Description+'%' order by Description ";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];


                var items = new List<SearchSimpleItem>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string Description = data.Rows[i]["Description"].ToString().Trim();
                    string Code = data.Rows[i]["code"].ToString();
                    string Qty = data.Rows[i]["qty"].ToString();
                    string Rate = data.Rows[i]["SellingCost"].ToString();
                    string AverageCost = data.Rows[i]["Ave_Cost"].ToString();
                    var searchItem = new SearchSimpleItem()
                    {
                        Code = Code,
                        Description = Description,
                        Qty = Qty,
                        Rate = Rate,
                        AverageCost = AverageCost

                    };
                    items.Add(searchItem);
                }

                JavaScriptSerializer js = new JavaScriptSerializer();

                Context.Response.Clear();
                Context.Response.ContentType = "application/json";

                Context.Response.Write(js.Serialize(items.OrderBy(a => a.Description)));

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }


        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetItemsStockMovement(string key, string stationIdFrom, string stationIdTo)
        {
            try
            {
                var module7 = new Module7();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@Description", key.Trim());
                cmd.CommandText = @" 
                        Select TOP 40
                            
                            Description,
                            code,
                            qty as tqty,

                            IsNull( ( SELECT Sum(Current_QTY)
                                FROM InventoryOpeningBalance
                                Where StationID=" + stationIdFrom + @" and ICode=InvCode.code and Dat='" + module7.StartDate() + @"'
                            ),0) 
                            as StationFromQty,
                            IsNull( ( SELECT Sum(Current_QTY)
                                FROM InventoryOpeningBalance
                                Where StationID=" + stationIdTo + @" and ICode=InvCode.code and Dat='" + module7.StartDate() + @"'
                            ),0) 
                            as StationToQty,
                            SellingCost,
                            Ave_Cost,
                            Reference
                            
                        from InvCode
                        where Description like '%' + @Description + '%' or code  like '%' + @Description + '%'
                        order by Description";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];


                var items = new List<SearchItemsStockMovement>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string Description = data.Rows[i]["Description"].ToString().Trim();
                    string Code = data.Rows[i]["code"].ToString();
                    string Qty = data.Rows[i]["tqty"].ToString();
                    string StationToQty = data.Rows[i]["StationToQty"].ToString();
                    string StationFromQty = data.Rows[i]["StationFromQty"].ToString();
                    string Rate = data.Rows[i]["SellingCost"].ToString();
                    string AverageCost = data.Rows[i]["Ave_Cost"].ToString();
                    string Reference = data.Rows[i]["Reference"].ToString();
                    var searchItem = new SearchItemsStockMovement()
                    {
                        Code = Code,
                        Description = Description,
                        Rate = Rate,
                        StationFromQty = StationFromQty,
                        StationToQty = StationToQty,
                        TotalQty = Qty,
                        AverageCost = AverageCost,
                        Reference1 = Reference,

                    };
                    items.Add(searchItem);
                }

                JavaScriptSerializer js = new JavaScriptSerializer();

                Context.Response.Clear();
                Context.Response.ContentType = "application/json";

                Context.Response.Write(js.Serialize(items.OrderBy(a => a.Description)));
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetExpenceHead(string expenceHead)
        {
            try
            {
                var GLCode = CompID + "04";
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@Title", expenceHead.Trim());

                cmd.CommandText = "select Code , Title,Balance from GLCode Where Title like '%'+@Title+'%' and Code like '" + GLCode + "%' and lvl=5 and CompID='" + CompID + "' order by Title";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];

                var expenceHeads = new List<SearchExpenceHeadModel>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string Code = data.Rows[i]["Code"].ToString();
                    string Title = data.Rows[i]["Title"].ToString();
                    string Balance = data.Rows[i]["Balance"].ToString();

                    var head = new SearchExpenceHeadModel()
                    {
                        Code = Code,
                        Name = Title,
                        Balance = Balance
                    };
                    expenceHeads.Add(head);
                }

                JavaScriptSerializer js = new JavaScriptSerializer();

                Context.Response.Clear();
                Context.Response.ContentType = "application/json";

                Context.Response.Write(js.Serialize(expenceHeads));

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }

        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetPartyGroup(string item)
        {

            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@GroupName", item.Trim());
                cmd.CommandText = "Select GroupName,GroupID from PartyGroup where GroupName like '%'+@GroupName+'%' ";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];

                var PGs = new List<SearchPartyGroup>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string Name = data.Rows[i]["GroupName"].ToString();
                    string Code = data.Rows[i]["GroupID"].ToString();


                    var searchPG = new SearchPartyGroup()
                    {
                        Code = Code,
                        Name = Name
                    };
                    PGs.Add(searchPG);
                }

                JavaScriptSerializer js = new JavaScriptSerializer();

                Context.Response.Clear();
                Context.Response.ContentType = "application/json";

                Context.Response.Write(js.Serialize(PGs));

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }


        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public void GetAllPartyGroup()
        {

            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "Select GroupName,GroupID from PartyGroup";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];

            var PGs = new List<SearchPartyGroup>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string Name = data.Rows[i]["GroupName"].ToString();
                string Code = data.Rows[i]["GroupID"].ToString();


                var searchPG = new SearchPartyGroup()
                {
                    Code = Code,
                    Name = Name
                };
                PGs.Add(searchPG);
            }

            JavaScriptSerializer js = new JavaScriptSerializer();

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            Context.Response.Write(js.Serialize(PGs));



        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]

        public void GetAllClass()
        {

            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "Select Name,id from Class";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];

            var PGs = new List<SearchClass>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string Name = data.Rows[i]["Name"].ToString();
                string Code = data.Rows[i]["id"].ToString();


                var searchPG = new SearchClass()
                {
                    Code = Code,
                    Name = Name
                };
                PGs.Add(searchPG);
            }

            JavaScriptSerializer js = new JavaScriptSerializer();

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            Context.Response.Write(js.Serialize(PGs));



        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetInvoice(string invoiceNumber, string type)
        {
            try
            {
                var model = new SaveProductViewModel();

                if (type == "SaleInvoice")
                {
                    model = SaleInvoice(invoiceNumber);
                }
                else if (type == "PurchaseInvoice")
                {
                    model = PurchaseInvoice(invoiceNumber);
                }
                //else if (type == "PurchaseReturnInvoice")
                //{
                //    model = PurchaseReturnInvoice(invoiceNumber);
                //}
                //else if (type == "SaleReturnInvoice")
                //{
                //    model = SaleReturnInvoice(invoiceNumber);
                //}
                JavaScriptSerializer js = new JavaScriptSerializer();

                Context.Response.Clear();
                Context.Response.ContentType = "application/json";

                Context.Response.Write(js.Serialize(model));

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }


        }

        public SaveProductViewModel SaleInvoice(string invoiceNumber)
        {
            try
            {
                var model = new SaveProductViewModel();

                SqlDataAdapter adptr = new SqlDataAdapter(@"
			   Select Invoice1.Date1,Invoice1.PartyCode,Invoice1.Name,Invoice1.Particular,Invoice1.address ,Invoice1.Manual_InvNo
			   ,Invoice1.InvNo,Invoice1.SalesMan,Invoice1.SaleManCode,Invoice1.Phone 
			   ,PartyCode.DealRs as IsDealRs, PartyCode.DisPer as IsDisPer, PartyCode.Balance as PartyBalance,PartyCode.OtherInfo
			   from Invoice1
			   LEFT OUTER JOIN PartyCode ON Invoice1.PartyCode = PartyCode.Code 
				where Invoice1.CompID='" + CompID + "' And Invoice1.InvNo = " + Convert.ToInt32(invoiceNumber), con);
                DataTable dt = new DataTable();
                adptr.Fill(dt);

                var clientData = new Client();
                var objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
                if (dt.Rows.Count > 0)
                {
                    clientData.Code = Convert.ToString(dt.Rows[0]["PartyCode"]);
                    clientData.Date = Convert.ToDateTime(dt.Rows[0]["Date1"]).ToString("dd/MM/yyyy");
                    clientData.Name = Convert.ToString(dt.Rows[0]["Name"]);
                    clientData.Particular = Convert.ToString(dt.Rows[0]["Particular"]);
                    clientData.ManInv = Convert.ToString(dt.Rows[0]["Manual_InvNo"]);
                    clientData.Address = Convert.ToString(dt.Rows[0]["address"]);
                    clientData.PhoneNumber = Convert.ToString(dt.Rows[0]["Phone"]);
                    clientData.SalesManName = Convert.ToString(dt.Rows[0]["SalesMan"]);
                    clientData.Balance = Convert.ToString(dt.Rows[0]["PartyBalance"]);
                    clientData.DealRs = Convert.ToString(dt.Rows[0]["IsDealRs"]);
                    clientData.DisPer = Convert.ToString(dt.Rows[0]["IsDisPer"]);
                    clientData.OtherInfo = Convert.ToString(dt.Rows[0]["OtherInfo"]);
                    clientData.SalesManId = Convert.ToString(dt.Rows[0]["SaleManCode"]);
                }


                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = @"SELECT
								Invoice2.Code, Invoice2.DeptId,Invoice2.DealRs,Invoice2.Item_Discount,Invoice2.Amount,Invoice2.Description,Invoice2.Qty,Invoice2.Rate,Invoice2.PerDiscount,Invoice2.Purchase_Amount,
							InvCode.Revenue_Code,InvCode.CGS_Code,	InvCode.Ave_Cost,InvCode.DealRs as DealRsFromDb,InvCode.qty as InHandQty
								FROM Invoice2
								LEFT OUTER JOIN invCode ON Invoice2.Code = InvCode.Code 
								where Invoice2.CompID='" + CompID + "' and Invoice2.InvNo='" + invoiceNumber + "'";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];

                var objModViewInventory = new ModViewInventory();
                var modItem = new ModItem();
                List<Product> products = new List<Product>();
                if (data.Rows.Count > 0)
                {

                    for (int i = 0; i < data.Rows.Count; i++)
                    {
                        var itemCode = data.Rows[i]["Code"].ToString();
                        var dealRs = Convert.ToDecimal(data.Rows[i]["DealRs"]);
                        var itemDiscount = Convert.ToDecimal(data.Rows[i]["Item_Discount"]);
                        var amount = Convert.ToDecimal(data.Rows[i]["Amount"]);
                        string RevenueCode = data.Rows[i]["Revenue_Code"].ToString();
                        string CGSCode = data.Rows[i]["CGS_Code"].ToString();
                        var product = new Product()
                        {
                            Code = itemCode,
                            Description = data.Rows[i]["Description"].ToString().Trim(),
                            Qty = data.Rows[i]["Qty"].ToString(),
                            Rate = data.Rows[i]["Rate"].ToString(),
                            Amount = amount.ToString(),
                            ItemDis = itemDiscount.ToString(),
                            PerDis = data.Rows[i]["PerDiscount"].ToString(),
                            DealDis = dealRs.ToString(),
                            NetAmount = (amount - (itemDiscount + dealRs)).ToString(),
                            PurAmount = data.Rows[i]["Purchase_Amount"].ToString(),
                            DealRsFromDb = data.Rows[i]["DealRsFromDb"].ToString(),
                            AverageCost = data.Rows[i]["Ave_Cost"].ToString(),
                            InHandQty = data.Rows[i]["InHandQty"].ToString(),
                            DepartmentId = data.Rows[i]["DeptId"].ToString(),
                            RevenueCode = RevenueCode,
                            CGSCode = CGSCode
                        };
                        products.Add(product);
                    }


                }


                var totalData = new TotalData();
                SqlDataAdapter cmdInv3 = new SqlDataAdapter("Select Total,CashPaid,Discount1,FlatDiscount,Flat_Discount_Per from Invoice3 where CompID='" + CompID + "' and InvNo=" + invoiceNumber, con);
                DataTable dt3 = new DataTable();
                cmdInv3.Fill(dt3);

                if (dt3.Rows.Count > 0)
                {
                    totalData.GrossTotal = Convert.ToString(dt3.Rows[0]["Total"]);
                    totalData.Recieved = Convert.ToString(dt3.Rows[0]["CashPaid"]);
                    totalData.NetDiscount = Convert.ToString(dt3.Rows[0]["Discount1"]);
                    totalData.FlatDiscount = Convert.ToString(dt3.Rows[0]["FlatDiscount"]);
                    totalData.FlatPer = Convert.ToString(dt3.Rows[0]["Flat_Discount_Per"]);
                }


                SqlDataAdapter invoiceAdptr = new SqlDataAdapter(@"select
	  InvNo
	, prev_invNo = (
		select top 1 invNo 
		from Invoice1 p 
		where p.InvNo < i.InvNo and
  CompId='" + CompID + @"'
		order by InvNo desc
		)
	, next_invNo = (
		select top 1 invNo 
		from Invoice1 n 

		where n.InvNo > i.InvNo and
        CompId='" + CompID + @"'
        order by InvNo asc
		)
  from Invoice1 as i
																		where i.InvNo = " + invoiceNumber + "", con);
                DataTable invoiceDataTable = new DataTable();
                invoiceAdptr.Fill(invoiceDataTable);

                var invoices = new Invoices()
                {
                    PreviousInvoice = Convert.ToString(invoiceDataTable.Rows[0]["prev_invNo"]),
                    NextInvoice = Convert.ToString(invoiceDataTable.Rows[0]["next_invNo"]),
                    CurrentInvoice = invoiceNumber

                };



                model.ClientData = clientData;
                model.TotalData = totalData;
                model.Products = products;
                model.NextPreviousInvoiceNumbers = invoices;

                return model;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }
        }

        public SaveProductViewModel PurchaseInvoice(string invoiceNumber)
        {

            try
            {
                var model = new SaveProductViewModel();

                SqlDataAdapter adptr = new SqlDataAdapter(@"
			   Select Purchase1.dat,Purchase1.VenderCode,Purchase1.SaleManCode,Purchase1.Vender,Purchase1.Particular,Purchase1.address ,Purchase1.Auto_Number
			   ,Purchase1.InvNo,Purchase1.Phone 
			   ,PartyCode.DealRs as IsDealRs, PartyCode.DisPer as IsDisPer, PartyCode.Balance as PartyBalance,PartyCode.OtherInfo
			   from Purchase1
			   LEFT OUTER JOIN PartyCode ON Purchase1.VenderCode = PartyCode.Code 
				where Purchase1.CompID='" + CompID + "' And Purchase1.InvNo = '" + invoiceNumber + "'", con);
                DataTable dt = new DataTable();
                adptr.Fill(dt);

                var clientData = new Client();
                var objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
                if (dt.Rows.Count > 0)
                {
                    clientData.Code = Convert.ToString(dt.Rows[0]["VenderCode"]);
                    clientData.Date = Convert.ToDateTime(dt.Rows[0]["dat"]).ToString("dd/MM/yyyy");
                    clientData.Name = Convert.ToString(dt.Rows[0]["Vender"]);
                    clientData.Particular = Convert.ToString(dt.Rows[0]["Particular"]);
                    clientData.ManInv = Convert.ToString(dt.Rows[0]["Auto_Number"]);
                    clientData.Address = Convert.ToString(dt.Rows[0]["address"]);
                    clientData.PhoneNumber = Convert.ToString(dt.Rows[0]["Phone"]);
                    clientData.Balance = Convert.ToString(dt.Rows[0]["PartyBalance"]);
                    clientData.DealRs = Convert.ToString(dt.Rows[0]["IsDealRs"]);
                    clientData.DisPer = Convert.ToString(dt.Rows[0]["IsDisPer"]);
                    clientData.SalesManId = Convert.ToString(dt.Rows[0]["SaleManCode"]);
                    clientData.OtherInfo = Convert.ToString(dt.Rows[0]["OtherInfo"]);
                }


                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = @"SELECT
					Purchase2.Code
					,Purchase2.Description
					,Purchase2.Qty
					,Purchase2.cost
					,Purchase2.amount
					,Purchase2.DiscountPercentageRate
					,Purchase2.CTN
					,Purchase2.Pcs
					,Purchase2.Item_Discount
					,Purchase2.CompID
					,Purchase2.CostAfterDiscount
					,Purchase2.DealRs, 
                    Purchase2.deptId,

					InvCode.SellingCost, InvCode.Ave_Cost,InvCode.DealRs as DealRsFromDb,InvCode.qty as InHandQty
								FROM Purchase2
								LEFT OUTER JOIN invCode ON Purchase2.Code = InvCode.Code 
								where Purchase2.CompID='" + CompID + "' and Purchase2.InvNo='" + invoiceNumber + "'";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];

                var objModViewInventory = new ModViewInventory();
                var modItem = new ModItem();
                List<Product> products = new List<Product>();
                if (data.Rows.Count > 0)
                {

                    for (int i = 0; i < data.Rows.Count; i++)
                    {
                        var itemCode = data.Rows[i]["Code"].ToString();
                        var dealRs = Convert.ToDecimal(data.Rows[i]["DealRs"]);
                        var itemDiscount = Convert.ToDecimal(data.Rows[i]["Item_Discount"]);
                        var amount = Convert.ToDecimal(data.Rows[i]["Amount"]);
                        var product = new Product()
                        {
                            Code = itemCode,
                            Description = data.Rows[i]["Description"].ToString().Trim(),
                            Qty = data.Rows[i]["Qty"].ToString(),
                            Rate = data.Rows[i]["cost"].ToString(),
                            Amount = amount.ToString(),
                            ItemDis = itemDiscount.ToString(),
                            PerDis = data.Rows[i]["DiscountPercentageRate"].ToString(),
                            DealDis = dealRs.ToString(),
                            NetAmount = (amount - (itemDiscount + dealRs)).ToString(),
                            DealRsFromDb = data.Rows[i]["DealRsFromDb"].ToString(),
                            AverageCost = data.Rows[i]["Ave_Cost"].ToString(),
                            SellingPrice = data.Rows[i]["SellingCost"].ToString(),
                            InHandQty = data.Rows[i]["InHandQty"].ToString(),
                            PurAmount = data.Rows[i]["CostAfterDiscount"].ToString(),
                            DepartmentId = data.Rows[i]["deptId"].ToString()
                        };
                        products.Add(product);
                    }


                }


                var totalData = new TotalData();
                SqlDataAdapter cmdInv3 = new SqlDataAdapter("Select Total,Paid,Discount,FlatDiscount,Flat_Discount_Per,OtherCharges from Purchase3 where CompID='" + CompID + "' and InvNo='" + invoiceNumber + "'", con);
                DataTable dt3 = new DataTable();
                cmdInv3.Fill(dt3);

                if (dt3.Rows.Count > 0)
                {
                    totalData.GrossTotal = Convert.ToString(dt3.Rows[0]["Total"]);
                    totalData.Recieved = Convert.ToString(dt3.Rows[0]["Paid"]);
                    totalData.NetDiscount = Convert.ToString(dt3.Rows[0]["Discount"]);
                    totalData.FlatDiscount = Convert.ToString(dt3.Rows[0]["FlatDiscount"]);
                    totalData.FlatPer = Convert.ToString(dt3.Rows[0]["Flat_Discount_Per"]);
                    totalData.OtherCharges = Convert.ToString(dt3.Rows[0]["OtherCharges"]);
                }

                var security = new Security.Security();
                if (con.State == ConnectionState.Closed) { con.Open(); }
                if (security.isTableExist("TempTable" + CompID, con))
                {
                    var dropTableCommand = new SqlCommand("drop Table TempTable" + CompID, con);
                    dropTableCommand.ExecuteNonQuery();
                }
                var invoiceAdptr = new SqlDataAdapter(@"SELECT
                rownum = IDENTITY(INT, 1,1),
                p.InvNo
                INTO TempTable" + CompID + @"
                FROM Purchase1 p
                Where CompId=" + CompID + @"
                GROUP BY p.InvNo
                ORDER BY Max(dat)  desc;
                SELECT
                prev.InvNo prev_invNo,
                TT.InvNo,
                nex.InvNo next_invNo
                FROM TempTable" + CompID + @" TT
                LEFT JOIN TempTable" + CompID + @" prev ON prev.rownum = TT.rownum - 1
                LEFT JOIN TempTable" + CompID + @" nex ON nex.rownum = TT.rownum + 1
                where TT.InvNo='" + invoiceNumber + "'", con);
                DataTable invoiceDataTable = new DataTable();
                invoiceAdptr.Fill(invoiceDataTable);
                Invoices invoices = null;
                if (dt3.Rows.Count > 0)
                {
                    invoices = new Invoices()
                    {
                        PreviousInvoice = Convert.ToString(invoiceDataTable.Rows[0]["next_invNo"]),
                        NextInvoice = Convert.ToString(invoiceDataTable.Rows[0]["prev_invNo"]),

                        CurrentInvoice = invoiceNumber

                    };
                }


                model.ClientData = clientData;
                model.TotalData = totalData;
                model.Products = products;
                model.NextPreviousInvoiceNumbers = invoices;
                con.Close();
                return model;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public void GetAllInvoices()
        {

            try
            {
                SqlDataAdapter adptr = new SqlDataAdapter(@"
				 Select Top 10
				Invoice.InvNo as InvoiceNumber,Invoice.Date1 as Date, Invoice.Name as PartyName,Invoice.Manual_InvNo as ManualInvoiceNumber,Invoice.SalesMan, 
				PartyCode.Mobile,
				Invoice3.Total, Invoice3.Discount1 as NetDiscount, Invoice3.CashPaid as Paid,
				(Invoice3.Total-Invoice3.Discount1 ) as BillTotal,Invoice3.FlatDiscount,
				
(   Select Sum(Invoice2.Qty)
					from Invoice2
					where Invoice2.CompID=Invoice.CompID and Invoice2.InvNo=Invoice.InvNo
					group by Invoice2.InvNo)
				as Quantity,
					
				(   Select Sum(Invoice2.DealRs)
					from Invoice2
					where Invoice2.CompID=Invoice.CompID and Invoice2.InvNo=Invoice.InvNo
					group by Invoice2.InvNo)
			as DealRs

				from Invoice1 as Invoice

				LEFT OUTER JOIN PartyCode ON Invoice.PartyCode = PartyCode.Code 
				LEFT OUTER JOIN invoice3 ON Invoice.InvNo = Invoice3.InvNo
				  
					
				   
				where Invoice.CompID='" + CompID + "' order by Invoice.InvNo desc", con);
                DataTable dt = new DataTable();
                adptr.Fill(dt);

                var model = new List<InvoiceViewModel>();




                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var invoice = new InvoiceViewModel();
                    invoice.InvoiceNumber = dt.Rows[i]["InvoiceNumber"].ToString();
                    invoice.Date = Convert.ToDateTime(dt.Rows[i]["Date"]).ToString("dd/MM/yyyy");
                    invoice.PartyName = Convert.ToString(dt.Rows[i]["PartyName"]);
                    invoice.DealRs = Convert.ToString(dt.Rows[i]["DealRs"]);
                    invoice.ManualInvoiceNumber = Convert.ToString(dt.Rows[i]["ManualInvoiceNumber"]);
                    invoice.PhoneNumber = Convert.ToString(dt.Rows[i]["Mobile"]);
                    invoice.SalesManName = Convert.ToString(dt.Rows[i]["SalesMan"]);
                    invoice.FlatDiscount = Convert.ToString(dt.Rows[i]["FlatDiscount"]);
                    invoice.Quantity = Convert.ToString(dt.Rows[i]["Quantity"]);
                    invoice.BillTotal = Convert.ToString(dt.Rows[i]["BillTotal"]);
                    invoice.Total = Convert.ToString(dt.Rows[i]["Total"]);
                    invoice.Paid = Convert.ToString(dt.Rows[i]["Paid"]);
                    invoice.NetDiscount = Convert.ToString(dt.Rows[i]["NetDiscount"]);
                    model.Add(invoice);
                }



                JavaScriptSerializer js = new JavaScriptSerializer();

                Context.Response.Clear();
                Context.Response.ContentType = "application/json";

                Context.Response.Write(js.Serialize(model));

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }


        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetSalesManName()
        {

            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;

                cmd.CommandText = "Select Name,ID from SaleManList ";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];



                List<Client> items = new List<Client>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    /*Name*/
                    string name = data.Rows[i][0].ToString();
                    /*ID*/
                    string code = data.Rows[i][1].ToString();

                    var product = new Client()
                    {
                        Name = name,
                        Code = code

                    };
                    items.Add(product);
                }
                //var json = JsonConvert.SerializeObject(items);.


                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";

                Context.Response.Write(js.Serialize(items));

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetPartyByCode(string partyCode)
        {

            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@Code", partyCode.Trim());

                cmd.CommandText = "Select Name,Code,Balance,Address,Mobile from PartyCode Where Code =@Code";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];



                List<SearchPartyModel> parties = new List<SearchPartyModel>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string Name = data.Rows[i]["Name"].ToString();
                    string Code = data.Rows[i]["Code"].ToString();
                    string Balance = data.Rows[i]["Balance"].ToString();
                    string Address = data.Rows[i]["Address"].ToString();
                    string Mobile = data.Rows[i]["Mobile"].ToString();

                    var party = new SearchPartyModel()
                    {
                        Name = Name,
                        Balance = Balance,
                        Address = Address,
                        PhoneNumber = Mobile,
                        Code = Code

                    };
                    parties.Add(party);
                }
                //var json = JsonConvert.SerializeObject(items);.


                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";

                Context.Response.Write(js.Serialize(parties));

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetParty(string key)
        {

            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@Name", key.Trim());

                cmd.CommandText = "Select Top 40 Name,Code,CreditLimit, Balance,Address,Mobile,OtherInfo,NorBalance,DealApplyNo,DealRs,DisPer from PartyCode Where Name like '%'+@Name+'%' OR Code like '%'+@Name+'%' OR OwnerName like '%'+@Name+'%' OR Mobile like '%'+@Name+'%' OR Phone like '%'+@Name+'%' OR Address like '%'+@Name+'%' and CompId='" + CompID + "' order by Name";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];



                List<SearchPartyModel> parties = new List<SearchPartyModel>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string Name = data.Rows[i]["Name"].ToString();
                    string Code = data.Rows[i]["Code"].ToString();
                    string Balance = data.Rows[i]["Balance"].ToString();
                    string Address = data.Rows[i]["Address"].ToString().Length > 25 ? data.Rows[i]["Address"].ToString().Substring(0, 25) + "..." : data.Rows[i]["Address"].ToString();
                    string Mobile = data.Rows[i]["Mobile"].ToString();
                    string OInfo = data.Rows[i]["OtherInfo"].ToString();
                    string NorBalance = data.Rows[i]["NorBalance"].ToString();
                    string CreditLimit = data.Rows[i]["CreditLimit"].ToString();
                    string DealApplyNo = data.Rows[i]["DealApplyNo"].ToString();
                    string AutoDealRs = data.Rows[i]["DealRs"].ToString();
                    string AutoDisPer = data.Rows[i]["DisPer"].ToString();
                    var party = new SearchPartyModel()
                    {
                        Name = Name,
                        Balance = Balance,
                        Address = Address,
                        PhoneNumber = Mobile,
                        OtherInfo_Hide = OInfo,
                        NorBalance_Hide = NorBalance,
                        Code = Code,
                        CreditLimit_Hide = CreditLimit,
                        DealApplyNo = DealApplyNo,
                        AutoDealRs_Hide = AutoDealRs,
                        AutoDisPer_Hide = AutoDisPer,

                    };
                    parties.Add(party);
                }
                //var json = JsonConvert.SerializeObject(items);.


                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";

                Context.Response.Write(js.Serialize(parties));

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetCashSaleParty(string key)
        {

            try
            {
                cmd.Connection = con;

                cmd.CommandText = @" 
                Select 
                    Top 50
                    Invoice1.InvNo,
                    Invoice1.PartyCode,
                    Invoice1.System_Date_Time,
                    Invoice1.Name as Name,
                    Invoice1.Address as Address,
                    Invoice1.Phone as Phone,
                    Invoice3.PrevBalance,
                    Invoice3.CashPaid

                from Invoice1
                inner join Invoice3 on invoice1.InvNo = Invoice3.InvNo
                where Invoice1.Compid = '" + CompID + "'and " +
                    "Invoice1.Name like '%Cash Sale%' and " +
                    "(Invoice1.Address like '%" + key.Trim() + "%' OR  " +
                    "Invoice1.Phone like '%" + key.Trim() + "%') " +
                    "Order by System_Date_Time Desc";

                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];



                var parties = new List<SearchCashSalePartyModel>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string Code = data.Rows[i]["PartyCode"].ToString();
                    string InvNo = data.Rows[i]["InvNo"].ToString();
                    string System_Date_Time = data.Rows[i]["System_Date_Time"].ToString();
                    string Name = data.Rows[i]["Name"].ToString();
                    string Address = data.Rows[i]["Address"].ToString().Length > 25 ? data.Rows[i]["Address"].ToString().Substring(0, 25) + "..." : data.Rows[i]["Address"].ToString();
                    string PhoneNumber = data.Rows[i]["Phone"].ToString();
                    string PrevBalance = data.Rows[i]["PrevBalance"].ToString();
                    string CashPaid = data.Rows[i]["CashPaid"].ToString();
                    var party = new SearchCashSalePartyModel()
                    {
                        Code = Code,
                        Name = Name,
                        PrevBalance = PrevBalance,
                        Address = Address,
                        PhoneNumber = PhoneNumber,
                        InvNo = InvNo,
                        Date = System_Date_Time,

                    };
                    parties.Add(party);
                }
                //var json = JsonConvert.SerializeObject(items);.


                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";

                Context.Response.Write(js.Serialize(parties));

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetBank(string BankName)
        {

            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.AddWithValue("@Title", BankName.Trim());
            cmd.CommandText = "Select Top 40 Title,Code,Balance from GLCode  Where Title like '%'+@Title+'%' AND Code like '%01010102%' and Lvl=5 and CompId='" + CompID + "' order by Title";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];



            List<SearchBankModel> banks = new List<SearchBankModel>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string Title = data.Rows[i]["Title"].ToString();
                string Code = data.Rows[i]["Code"].ToString();
                string Balance = data.Rows[i]["Balance"].ToString();

                var bank = new SearchBankModel()
                {
                    Title = Title,
                    Balance = Balance,
                    Code = Code

                };
                banks.Add(bank);
            }
            //var json = JsonConvert.SerializeObject(items);.


            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            Context.Response.Write(js.Serialize(banks));

        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetVoucher(string voucher, string type, string dayLimit, string partyCode, string bankCode)
        {
            var voucherNumber = "";
            dayLimit = dayLimit.Trim() == string.Empty ? "0" : dayLimit;
            int n;
            if (voucher.ToLower() != "showallvoucher" && int.TryParse(voucher, out n))
            {
                voucherNumber = "and V_No = " + voucher + "";
            }
            var secondDescription = "";
            var amountType = "";
            if (type == "CPV")
            {
                secondDescription = "CashPaidToParty";
                amountType = "AmountCr";
                GetCashVoucher(voucher, type, secondDescription, amountType, voucherNumber);
            }
            else if (type == "CRV")
            {
                secondDescription = "FromParties";
                amountType = "AmountDr";
                GetCashVoucher(voucher, type, secondDescription, amountType, voucherNumber);
            }
            else if (type == "CPV_Expences")
            {
                GetExpencesVoucher(voucher, voucherNumber);
            }
            else if (type == "BRV_CashDepositInToBank" || type == "CRV_CashWithDrawFromBank")
            {
                GetBankPaymentVoucher(voucher, voucherNumber, Convert.ToInt32(dayLimit), partyCode, bankCode, type);
            }
            else if (type == "BPV" || type == "BRV")
            {
                GetBankPaymentVoucher(voucher, voucherNumber, Convert.ToInt32(dayLimit), partyCode, bankCode, type);
            }
        }

        public void GetCashVoucher(string voucher, string type, string secondDescription, string amountType, string voucherNumber)
        {


            string CashAccountGLCode = CompID + ConfigurationManager.AppSettings["CashAccountGLCode"].ToString();

            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;

            cmd.CommandText = "Select TOP 250 V_No,dateDr," + amountType + ",VenderCode,Narration,PartyCode.Name,PartyCode.Balance,PartyCode.Address,PartyCode.NorBalance from GeneralLedger " +
            " inner join PartyCode on GeneralLedger.VenderCode = PartyCode.Code where  GeneralLedger.CompID ='" + CompID + "' and V_Type='" + type + "' " + voucherNumber + " and GeneralLedger.Code='" + CashAccountGLCode + "' and SecondDescription='" + secondDescription + "' Order By V_No Desc";

            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];



            List<SearchVoucherModel> parties = new List<SearchVoucherModel>();
            for (int i = 0; i < data.Rows.Count; i++)
            {



                string CashPaid = data.Rows[i][amountType].ToString();

                string VoucherNumber = data.Rows[i]["V_No"].ToString();
                string Date = Convert.ToDateTime(data.Rows[i]["dateDr"]).ToString("dd/MM/yyyy");

                string VenderCode = data.Rows[i]["VenderCode"].ToString();
                string Address = data.Rows[i]["Address"].ToString();
                string Name = data.Rows[i]["Name"].ToString();
                string NorBalance = data.Rows[i]["NorBalance"].ToString();
                string Balance = data.Rows[i]["Balance"].ToString();
                string Narration = data.Rows[i]["Narration"].ToString();
                var party = new SearchVoucherModel()
                {
                    VoucherNumber = VoucherNumber,
                    PartyName = Name,
                    Date = Date,
                    CashPaid = CashPaid,
                    PartyCode_Hide = VenderCode,
                    PartyAddress = Address,
                    NorBalance_Hide = NorBalance,
                    Balance_Hide = Balance,
                    Narration_Hide = Narration

                };
                parties.Add(party);
            }
            //var json = JsonConvert.SerializeObject(items);.


            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            Context.Response.Write(js.Serialize(parties));
        }
        public void GetExpencesVoucher(string voucher, string voucherNumber)
        {
            var secondDescription = "OperationalExpencesThroughCash";
            var amountType = "AmountCr";

            var type = "CPV";

            string CashAccountGLCode = CompID + ConfigurationManager.AppSettings["CashAccountGLCode"].ToString();

            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;

            cmd.CommandText = "Select TOP 250 V_No,dateDr," + amountType + ",GLCode.Code as AccountCode,GLCode.Title as AccountTitle,Narration From GeneralLedger " +
            " inner join GLCode on GeneralLedger.Code1 = GLCode.Code where  GeneralLedger.CompID ='" + CompID + "' and V_Type='" + type + "' " + voucherNumber + " and GeneralLedger.Code='" + CashAccountGLCode + "' and SecondDescription='" + secondDescription + "' Order By V_No,dateDr Desc";

            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];

            List<SearchVoucherModel> parties = new List<SearchVoucherModel>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string CashPaid = data.Rows[i][amountType].ToString();

                string VoucherNumber = data.Rows[i]["V_No"].ToString();
                string Date = Convert.ToDateTime(data.Rows[i]["dateDr"]).ToString("dd/MM/yyyy");
                string AccountCode = data.Rows[i]["AccountCode"].ToString();
                string AccountTitle = data.Rows[i]["AccountTitle"].ToString();
                string Narration = data.Rows[i]["Narration"].ToString();

                var party = new SearchVoucherModel()
                {
                    VoucherNumber = VoucherNumber,
                    PartyName = AccountTitle,
                    Date = Date,
                    CashPaid = CashPaid,
                    PartyCode_Hide = AccountCode,
                    Narration_Hide = Narration
                };
                parties.Add(party);
            }


            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            Context.Response.Write(js.Serialize(parties));

        }

        public void GetBankPaymentVoucher(string voucher, string voucherNumber, int daysLimit, string partyCode, string bankCode, string voucherType)
        {
            try
            {
                var type = voucherType;
                var secondDescription = "";
                var amountType = "";
                if (type == "BPV")
                {
                    secondDescription = "CashPaidToPartyThroughBank";
                    amountType = "AmountDr";
                }
                else if (type == "BRV")
                {
                    secondDescription = "FromPartiesThroughBank";
                    amountType = "AmountCr";
                }
                else if (type == "BRV_CashDepositInToBank")
                {
                    secondDescription = "CashDepositInToBank";
                    amountType = "AmountCr";
                    type = "BRV";
                }
                else if (type == "CRV_CashWithDrawFromBank")
                {
                    secondDescription = "CashWithDrawForPettyCash";
                    amountType = "AmountDr";
                    type = "CRV";
                }


                var andPartyCode = "";
                var andBankCode = "";
                if (partyCode.Trim() != string.Empty)
                {
                    andPartyCode = " and  PartyCode.Code='" + partyCode + "'";
                }
                if (bankCode.Trim() != string.Empty)
                {
                    andBankCode = " and  BankCode='" + bankCode + "'";
                }
                string CashAccountGLCode = CompID + ConfigurationManager.AppSettings["CashAccountGLCode"].ToString();

                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;

                if (voucherType == "BRV_CashDepositInToBank" || voucherType == "CRV_CashWithDrawFromBank")
                {
                    cmd.CommandText = @"Select TOP 250 
								Title ,
								V_No,
								BankCode,
								ChequeNo,
								dateDr, " + amountType + ", Narration,GLCode.Balance as BankBalance  From GeneralLedger Inner Join GLCode on GLCode.Code = BankCode  where  GeneralLedger.Code='" + CashAccountGLCode + "' and GeneralLedger.CompID = '" + CompID + "' and V_Type = '" + type + "' " + voucherNumber + " and SecondDescription = '" + secondDescription + "'  " + andPartyCode + " " + andBankCode + " Order By dateDr Desc";
                }
                else
                {
                    cmd.CommandText = @"Select TOP 250 
								
								PartyCode.Code as PartyCode,
								Title ,
								PartyCode.Name,
								PartyCode.Address,
								PartyCode.Balance, 
								V_No,
								BankCode,
								ChequeNo,
								dateDr, " + amountType + ", Narration,GLCode.Balance as BankBalance  From GeneralLedger INNER JOIN PartyCode on GeneralLedger.Code = PartyCode.Code Inner Join GLCode on GLCode.Code = BankCode  where GeneralLedger.CompID = '" + CompID + "' and V_Type = '" + type + "' " + voucherNumber + " and SecondDescription = '" + secondDescription + "' and dateDr>= '" + DateTime.Now.AddDays(-daysLimit) + "'  " + andPartyCode + " " + andBankCode + " Order By V_No Desc";

                }
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];
                var searchBankDepositAndWithdrawVoucherModel = new List<SearchBankDepositAndWithdrawVoucherModel>();
                var searchBankPaymentVoucherModel = new List<SearchBankPaymentVoucherModel>();
                if (voucherType == "BRV_CashDepositInToBank" || voucherType == "CRV_CashWithDrawFromBank")
                {
                    for (int i = 0; i < data.Rows.Count; i++)
                    {

                        string VoucherNumber = data.Rows[i]["V_No"].ToString();
                        string Date = Convert.ToDateTime(data.Rows[i]["dateDr"]).ToString("dd/MM/yyyy");
                        string CashPaid = data.Rows[i][amountType].ToString();
                        string Narration = data.Rows[i]["Narration"].ToString();
                        string BankName = data.Rows[i]["Title"].ToString();
                        string BankCode = data.Rows[i]["BankCode"].ToString();
                        string ChequeNo = data.Rows[i]["ChequeNo"].ToString();

                        var party = new SearchBankDepositAndWithdrawVoucherModel()
                        {
                            Date = Date,
                            VoucherNumber = VoucherNumber,
                            Narration = Narration,
                            BankName = BankName,
                            CashPaid = CashPaid,
                            BankCode_Hide = BankCode,
                            ChequeNumber_Hide = ChequeNo
                        };
                        searchBankDepositAndWithdrawVoucherModel.Add(party);
                    }
                }
                else
                {
                    for (int i = 0; i < data.Rows.Count; i++)
                    {

                        string VoucherNumber = data.Rows[i]["V_No"].ToString();
                        string Date = Convert.ToDateTime(data.Rows[i]["dateDr"]).ToString("dd/MM/yyyy");
                        string CashPaid = data.Rows[i][amountType].ToString();
                        string Narration = data.Rows[i]["Narration"].ToString();
                        string BankName = data.Rows[i]["Title"].ToString();
                        string PartyName = data.Rows[i]["Name"].ToString();
                        string PartyCode = data.Rows[i]["PartyCode"].ToString();
                        string Address = data.Rows[i]["Address"].ToString();
                        string Balance = data.Rows[i]["Balance"].ToString();
                        string ChequeNumber = data.Rows[i]["ChequeNo"].ToString();
                        string BankCode = data.Rows[i]["BankCode"].ToString();
                        string BankBalance = data.Rows[i]["BankBalance"].ToString();


                        var party = new SearchBankPaymentVoucherModel()
                        {
                            Date = Date,

                            PartyName = PartyName,
                            VoucherNumber = VoucherNumber,
                            Narration = Narration,
                            BankName = BankName,
                            CashPaid = CashPaid,
                            PartyCode_Hide = PartyCode,
                            PartyAddress_Hide = Address,
                            PartyBalance_Hide = Balance,
                            ChequeNumber_Hide = ChequeNumber,
                            BankCode_Hide = BankCode,
                            BankBalance_Hide = BankBalance
                        };
                        searchBankPaymentVoucherModel.Add(party);
                    }
                }



                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";

                if (voucherType == "BRV_CashDepositInToBank" || voucherType == "CRV_CashWithDrawFromBank")
                {
                    Context.Response.Write(js.Serialize(searchBankDepositAndWithdrawVoucherModel));

                }
                else
                {
                    Context.Response.Write(js.Serialize(searchBankPaymentVoucherModel));

                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }

        }
        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]


        //public void GetClientData(string partyCode, string itemsCode)
        //{
        //    var codeArray = itemsCode.Trim('_').Split('_');
        //    var discPer = GetArrayofItemDiscountPer(codeArray, partyCode);

        //    cmd.Connection = con;
        //    cmd.CommandType = System.Data.CommandType.Text;
        //    cmd.Parameters.AddWithValue("@Code", partyCode);
        //    cmd.CommandText = "Select Code,Address,Balance,DealRs,DisPer,Mobile,OtherInfo from PartyCode  where Code=@Code";
        //    DataSet objDs = new DataSet();
        //    SqlDataAdapter dAdapter = new SqlDataAdapter();
        //    dAdapter.SelectCommand = cmd;
        //    if (con.State==ConnectionState.Closed) { con.Open(); }
        //    dAdapter.Fill(objDs);
        //    con.Close();
        //    var data = objDs.Tables[0];




        //    var clients = new List<Client>();
        //    var client = new Client();

        //    for (int i = 0; i < data.Rows.Count; i++)
        //    {

        //        string code = data.Rows[i][0].ToString();
        //        string address = data.Rows[i][1].ToString();
        //        string balance = data.Rows[i][2].ToString();
        //        string dealRs = data.Rows[i][3].ToString();
        //        string disPer = data.Rows[i][4].ToString();
        //        string phoneNumber = data.Rows[i][5].ToString();
        //        string otherInfo = data.Rows[i][6].ToString();

        //        client = new Client()
        //        {
        //            Code = code,
        //            Address = address,
        //            Balance = balance,
        //            DealRs = dealRs,
        //            DisPer = disPer,
        //            PhoneNumber = phoneNumber,
        //            OtherInfo = otherInfo
        //        };
        //        clients.Add(client);
        //    }
        //    var clientWithDiscountPer = new ClientWithDiscountPer()
        //    {
        //        Clients = clients,
        //        DiscountPer = discPer
        //    };
        //    JavaScriptSerializer js = new JavaScriptSerializer();
        //    Context.Response.Clear();
        //    Context.Response.ContentType = "application/json";

        //    Context.Response.Write(js.Serialize(clientWithDiscountPer));

        //}


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetItems(string key)
        {


            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@Description", key.Trim());
                cmd.CommandText = "Select TOP 40 Description,code,qty,SellingCost from InvCode where Description like '%'+@Description+'%' order by Description  ";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];


                var items = new List<ItemViewModel>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string Description = data.Rows[i]["Description"].ToString().Trim();
                    string Code = data.Rows[i]["code"].ToString();
                    string Qty = data.Rows[i]["qty"].ToString();
                    string Rate = data.Rows[i]["SellingCost"].ToString();
                    var searchItem = new ItemViewModel()
                    {
                        Code = Code,
                        Description = Description,
                        Qty = Qty,
                        Rate = Rate
                    };
                    items.Add(searchItem);
                }

                JavaScriptSerializer js = new JavaScriptSerializer();

                Context.Response.Clear();
                Context.Response.ContentType = "application/json";

                Context.Response.Write(js.Serialize(items));


            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }

        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetItemRecord(string clientCode, string itemsCode)
        {
            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@itemsCode", itemsCode.Trim());
                cmd.Parameters.AddWithValue("@clientCode", clientCode.Trim());
                cmd.CommandText = @"
								Select  Top 20
										Invoice2.Code as ItemCode, 
										PartyCode.Name as PartyName,
										Invoice1.SalesMan,  
										Invoice2.Rate,
										Invoice2.Qty,
										Invoice2.Amount,
										Invoice2.DealRs,
										Invoice2.Item_Discount,
										Invoice2.PerDiscount,
										PartyCode.Code as PartyCode,
										ISNULL(BrandName.Name,'No Brand') as Brand,
										Invoice1.Date1 as SoldDate
								from Invoice2
								LEFT OUTER JOIN invoice1 ON Invoice2.InvNo = Invoice1.InvNo
								LEFT OUTER JOIN invCode ON Invoice2.Code = InvCode.Code 
								
								LEFT OUTER JOIN BrandName ON InvCode.Brand = BrandName.Code
								LEFT OUTER JOIN PartyCode ON Invoice1.PartyCode = PartyCode.Code  
								where Invoice2.Code=@itemsCode AND Invoice1.PartyCode = @clientCode
								order by Invoice1.System_Date_Time desc  ";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];





                var listItemRecord = new List<ItemRecord>();

                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string Rate = data.Rows[i]["Rate"].ToString();
                    string SalesMan = data.Rows[i]["SalesMan"].ToString();
                    string Qty = data.Rows[i]["Qty"].ToString();
                    string Amount = data.Rows[i]["Amount"].ToString();
                    string DealRs = data.Rows[i]["DealRs"].ToString();
                    string Item_Discount = data.Rows[i]["Item_Discount"].ToString();
                    string PerDiscount = data.Rows[i]["PerDiscount"].ToString();
                    string ItemCode = data.Rows[i]["ItemCode"].ToString();


                    string PartyName = data.Rows[i]["PartyName"].ToString();
                    string PartyCode = data.Rows[i]["PartyCode"].ToString();
                    string Brand = data.Rows[i]["Brand"].ToString();
                    string SoldDate = data.Rows[i]["SoldDate"].ToString();


                    var ItemRecord = new ItemRecord()
                    {
                        ItemCode = ItemCode,
                        PartyName = PartyName,
                        PartyCode = PartyCode,
                        Brand = Brand,
                        DateSold = Convert.ToDateTime(SoldDate).ToString("dd/MM/yyyy"),
                        Rate = Rate,
                        Qty = Qty,
                        Amount = Amount,
                        DealRs = DealRs,
                        Item_Discount = Item_Discount,
                        PerDiscount = PerDiscount,
                        SalesMan = SalesMan
                    };
                    listItemRecord.Add(ItemRecord);

                }

                var itemRecordsModel = new ItemRecordsModel();
                itemRecordsModel.LastRecord = listItemRecord.FirstOrDefault();
                itemRecordsModel.AllRecords = listItemRecord.OrderBy(a => a.DateSold).ToList();
                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";

                Context.Response.Write(js.Serialize(itemRecordsModel));
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetItemsDiscountPer(string itemCodes, string partyCode)
        {
            try
            {
                var itemWithDisPer = GetArrayofItemDiscountPer(itemCodes.Trim(','), partyCode);
                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";

                Context.Response.Write(js.Serialize(itemWithDisPer));

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }
        }
        public List<Product> GetArrayofItemDiscountPer(string itemCodes, string partyCode)
        {
            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;


                cmd.CommandText = @"
								Select  
								InvCode.code,ISNULL(BrandWiseDiscount.Disc_Rate,0) as 'Discount' 
								from InvCode 
								inner join BrandWiseDiscount on InvCode.Brand=BrandWiseDiscount.BrandCode
								inner join PartyCode on BrandWiseDiscount.PartyCode=PartyCode.Code
								where 
								PartyCode.Code='" + partyCode + "' and InvCode.code IN (" + itemCodes + ")";


                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];

                var items = new List<Product>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string itemCode = data.Rows[i]["code"].ToString();
                    string discountPer = data.Rows[i]["Discount"].ToString();

                    var item = new Product()
                    {
                        Code = itemCode,
                        PerDis = discountPer
                    };
                    items.Add(item);

                }



                return items;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetAllBrands()
        {

            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "Select Distinct Name ,Code from BrandName Where CompID='" + CompID + "' order by name";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var dt = objDs.Tables[0];



            var brands = new List<Brand>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var brand = new Brand();


                brand.Name = Convert.ToString(dt.Rows[i][0]);
                brand.Code = Convert.ToString(dt.Rows[i][1]);

                brands.Add(brand);
            }

            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            Context.Response.Write(js.Serialize(brands));


        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetPartyBrandRate(string PartyCode)
        {


            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;


                cmd.CommandText = "Select Disc_Rate,BrandCode from BrandWiseDiscount Where PartyCode='" + PartyCode + "' and CompID='" + CompID + "'";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var dt = objDs.Tables[0];



                var brands = new List<Brand>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var brand = new Brand();


                    brand.Code = Convert.ToString(dt.Rows[i]["BrandCode"]);
                    brand.Rate = Convert.ToString(dt.Rows[i]["Disc_Rate"]);

                    brands.Add(brand);
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";

                Context.Response.Write(js.Serialize(brands));


            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetAttribute(string attribute, string code = "0")
        {
            try
            {
                var id = "ID";
                var fromTable = attribute;
                var returnAttribute = attribute;
                var Where = "Where CompID = '" + CompID + "' order by  " + attribute + ""; ;
                if (attribute == "Group")
                {
                    attribute = "GroupName";
                    id = "GroupId";
                    fromTable = "GroupItem";
                    Where = "Where CompID = '" + CompID + "' order by  " + attribute + "";
                }
                if (attribute == "MeasureUnit")
                {
                    attribute = "Name";
                    id = "Code";
                    fromTable = "AccountUnit";
                    Where = "Where CompID = '" + CompID + "' order by  " + attribute + "";
                }
                if (attribute == "SaleManList")
                {
                    attribute = "Name";
                    id = "Id";
                    fromTable = "SaleManList";
                    Where = "Where CompID = '" + CompID + "' order by  " + attribute + "";
                }
                else if (attribute == "PartyGroup")
                {
                    attribute = "GroupName";
                    id = "GroupID";
                    Where = "Where CompID = '" + CompID + "' order by  " + attribute + "";
                }
                else if (attribute == "ItemType" || attribute == "BrandName")
                {
                    attribute = "Name";
                    id = "Code";
                    Where = "Where CompID = '" + CompID + "' order by  " + attribute + "";
                }
                else if (attribute == "Manufacturers" || attribute == "Packing" || attribute == "Category" || attribute == "Class" || attribute == "GoDown" || attribute == "Dept")
                {
                    attribute = "Name";
                    Where = "Where CompID = '" + CompID + "' order by  " + attribute + "";
                }
                else if (attribute == "Station")
                {
                    attribute = "Name";
                    Where = "Where CompID = '" + CompID + "' order by  ID asc";
                }
                else if (attribute == "AllStation")
                {
                    fromTable = "Station";
                    attribute = "Name";
                    Where = "Where CompID = '" + CompID + "' order by  ID asc";
                }
                else if (attribute == "ItemNature")
                {
                    id = "Code";
                    Where = "Where CompID = '" + CompID + "' order by  " + attribute + "";
                }
                else if (attribute == "IncomeAccount" || attribute == "COGSAccount")
                {
                    var attributeCode = attribute == "IncomeAccount" ? CompID + "030101" : CompID + "010106";
                    id = "Code";
                    attribute = "Title";
                    fromTable = "GLCode";
                    Where = "Where CompID = '" + CompID + "' and lvl=5 and Code like '" + attributeCode + "%'   order by  " + attribute + "";
                }
                else if (attribute == "Model")
                {
                    attribute = "Name";
                    id = "Code";
                    fromTable = "Model";
                    Where = "Where CompID = '" + CompID + "' order by  " + attribute + "";
                }
                else if (attribute == "Color")
                {
                    attribute = "Color";
                    id = "ID";
                    fromTable = "Color";
                    Where = "Where CompID = '" + CompID + "' order by  " + attribute + "";
                }
                else if (attribute == "BankList")
                {
                    attribute = "BankName";
                    id = "BankCode";
                    fromTable = "BankCode";
                    Where = "Where CompID = '" + CompID + "' order by  " + attribute + "";
                }
                else if (attribute == "ExpenceListt")
                {
                    attribute = "Title";
                    id = "Code";
                    fromTable = "GLCode";
                    Where = "Where CompID = '" + CompID + "' and code like '010402%' and lvl=5 order by  " + attribute + "";

                }
                else if (attribute == "GLCode" && code != "0")
                {
                    attribute = "Title";
                    id = "Code";
                    fromTable = "GLCode";
                    Where = "Where CompID = '" + CompID + "' and code like '" + code + "%' order by  " + attribute + "";

                }

                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;

                cmd.CommandText = "Select " + id + " , " + attribute + "  from  " + fromTable + " " + Where + "";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var dt = objDs.Tables[0];

                var Attributes = new List<AttributeModel>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    var Attribute = new AttributeModel();

                    Attribute.Code = Convert.ToString(dt.Rows[i][0]);
                    Attribute.Name = Convert.ToString(dt.Rows[i][1]);

                    Attribute.Attribute = returnAttribute;

                    Attributes.Add(Attribute);

                }
                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";

                Context.Response.Write(js.Serialize(Attributes));
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                con.Close();
            }

        }

        [WebMethod]
        public object AddNewAttribute(string Code, string Name, string attribute)
        {


            SqlTransaction transaction;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            transaction = con.BeginTransaction();
            try
            {
                var id = "ID";
                var table = attribute;
                var returnAttribute = attribute;
                if (attribute == "MeasureUnit")
                {
                    attribute = "Name";
                    id = "Code";
                    table = "AccountUnit";
                }
                else if (attribute == "Group")
                {
                    attribute = "GroupName";
                    id = "GroupId";
                    table = "GroupItem";
                }
                else if (attribute == "ItemType" || attribute == "BrandName")
                {
                    attribute = "Name";
                    id = "Code";
                }
                else if (attribute == "Manufacturers" || attribute == "Packing" || attribute == "Category" || attribute == "Class" || attribute == "GoDown")
                {
                    attribute = "Name";
                }
                else if (attribute == "ItemNature")
                {
                    id = "Code";
                }


                //var cmd =new SqlCommand("Update  " + fromTable + " set " + id + "="+model.ItemCode+" , " + attribute + "= '" + model.Name + "'",con,transaction);
                var cmd = new SqlCommand("Insert Into  " + table + "  (" + id + " , " + attribute + ",CompId) values(" + Code + ",'" + Name + "','" + CompID + "')", con, transaction);
                cmd.ExecuteNonQuery();


                transaction.Commit();
                con.Close();


                //baseModel.AddItemModel = model;
                return new BaseModel { Success = true, Message = "Added Successfully!!" };
            }
            catch (Exception ex)
            {

                transaction.Rollback();
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }


        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void MakeItemDefault(string attrCode, string attributeType)
        {
            if (attrCode != "1")
            {
                var command = new SqlCommand("Select * From InvCode where Code = attrCode ", con);
                var isItemAvialble = command.ExecuteScalar();
            }
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.AddWithValue("@ItemCode", attrCode.Trim());
            cmd.CommandText = @"Select * From InvCode where Code = @ItemCode ";

            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];

            List<AddItemModel> items = new List<AddItemModel>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                for (int j = 0; j < 34; j++)
                {
                    if (data.Rows[i][j] == null)
                    {
                    }
                }
                var isGST_Apply = data.Rows[i]["GST_Apply"].ToString() != "" ? data.Rows[i]["GST_Apply"] : false;
                var isActive = data.Rows[i]["Active"].ToString() != "" ? data.Rows[i]["Active"] : false;

                var product = new AddItemModel()
                {
                    ItemCode = data.Rows[i]["Code"].ToString(),
                    BarCode = data.Rows[i]["BarCode"].ToString(),
                    Name = data.Rows[i]["Description"].ToString().Trim(),
                    SellingPriceOrUnit = data.Rows[i]["SellingCost"].ToString(),
                    ManufacturerId = data.Rows[i]["Manufacturer"].ToString(),
                    ReOrderPoint = data.Rows[i]["ReorderLevel"].ToString(),
                    ReOrderQty = data.Rows[i]["ReorderQty"].ToString(),
                    BrandId = data.Rows[i]["Brand"].ToString(),
                    Reference = data.Rows[i]["Reference"].ToString(),
                    Reference2 = data.Rows[i]["Reference2"].ToString(),
                    IsVisible = Convert.ToBoolean(data.Rows[i]["Visiable"]),
                    PiecesInPacking = data.Rows[i]["PiecesInPacking"].ToString(),
                    PackingId = data.Rows[i]["Packing"].ToString(),
                    CategoryId = data.Rows[i]["Category"].ToString(),
                    ClassId = data.Rows[i]["Class"].ToString(),
                    GoDownId = data.Rows[i]["Godown"].ToString(),
                    OrderQuantity = data.Rows[i]["OrderQTY"].ToString(),
                    BonusQuantity = data.Rows[i]["BonusQTY"].ToString(),
                    ItemNatureId = data.Rows[i]["Nature"].ToString(),
                    UnitOfMeasureId = data.Rows[i]["AccountUnit"].ToString(),
                    IsActive = Convert.ToBoolean(isActive),
                    ColorId = data.Rows[i]["Color"].ToString(),
                    Registration = data.Rows[i]["RegNo"].ToString(),
                    MinPrice = data.Rows[i]["MinLimit"].ToString(),
                    IssueIngridient = data.Rows[i]["Ingridients"].ToString() == string.Empty || data.Rows[i]["Ingridients"].ToString() == "0" ? false : true,
                    GST = data.Rows[i]["GST_Rate"].ToString(),
                    GST_Apply = Convert.ToBoolean(isGST_Apply),
                    IncomeAccount = data.Rows[i]["Revenue_Code"].ToString(),
                    COGSAccount = data.Rows[i]["CGS_Code"].ToString(),
                    HeightId = data.Rows[i]["Height"].ToString(),
                    LengthId = data.Rows[i]["Length"].ToString(),
                    ItemTypeId = data.Rows[i]["ItemType"].ToString(),
                    MaxPrice = data.Rows[i]["MaxLimit"].ToString()


                };
                items.Add(product);
            }

            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(js.Serialize(items));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetAllDepartments()
        {


            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;

            cmd.CommandText = "Select ID,Name  from  Dept ";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var dt = objDs.Tables[0];

            var Attributes = new List<AttributeModel>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var Attribute = new AttributeModel();

                Attribute.Code = Convert.ToString(dt.Rows[i][0]);
                Attribute.Name = Convert.ToString(dt.Rows[i][1]);

                Attributes.Add(Attribute);

            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            Context.Response.Write(js.Serialize(Attributes));

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetDataForGoolgleChart1()
        {


            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;

            cmd.CommandText = "SELECT Sum(qty),BrandName.Name From Invoice2 Join BrandName on Invoice2.BrandName = BrandName.Code Group by BrandName.Name";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var dt = objDs.Tables[0];

            var dataList = new List<GoogleChartModel>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var googleChartModel = new GoogleChartModel();

                googleChartModel.Qty = dt.Rows[i][0].ToString();
                googleChartModel.Name = dt.Rows[i][1].ToString();

                dataList.Add(googleChartModel);

            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            Context.Response.Write(js.Serialize(dataList));

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetDataForGoolgleChart2()
        {


            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;

            cmd.CommandText = "SELECT Sum(Total) as Sale,(SELECT Sum(Total) From Purchase3) as Purchase,(SELECT Sum(Total) From SaleReturn1) as SaleReturn,(SELECT Sum(Total) From PurchaseReturn1) as PurchaseReturn From Invoice3";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var dt = objDs.Tables[0];

            var dataList = new List<GoogleChartModel>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var googleChartModel = new GoogleChartModel();

                googleChartModel.Sale = dt.Rows[i]["Sale"].ToString();
                googleChartModel.SaleReturn = dt.Rows[i]["SaleReturn"].ToString();
                googleChartModel.Purchase = dt.Rows[i]["Purchase"].ToString();
                googleChartModel.PurchaseReturn = dt.Rows[i]["PurchaseReturn"].ToString();

                dataList.Add(googleChartModel);

            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            Context.Response.Write(js.Serialize(dataList));

        }




        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Brand_PartyWiseDisc_Rate(string PartyCode)
        {

            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            //cmd.Parameters.AddWithValue("@Description", item);
            cmd.CommandText = @"Select BrandWiseDiscount.BrandCode,BrandWiseDiscount.Disc_Rate from BrandWiseDiscount  where PartyCode='" + PartyCode + "'";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];

            //'''''''''''''''Return--------------
            var listBrandWiseDis = new List<BrandWiseDiscountModel>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string BrandCode = data.Rows[i]["BrandCode"].ToString();

                string DiscountRate = data.Rows[i]["Disc_Rate"].ToString();

                var brandWithDiscount = new BrandWiseDiscountModel()
                {

                    BrandCode = BrandCode,
                    DiscountRate = DiscountRate
                };
                listBrandWiseDis.Add(brandWithDiscount);
            }



            JavaScriptSerializer js = new JavaScriptSerializer();

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            Context.Response.Write(js.Serialize(listBrandWiseDis));



        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Brand_List()
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            //cmd.Parameters.AddWithValue("@Description", item);
            cmd.CommandText = "Select Code,Name,Blocked,VerifyIME from BrandName where Compid='01'";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];


            var listBrand = new List<Brand>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string BrandCode = data.Rows[i]["Code"].ToString();
                string BrandName = data.Rows[i]["Name"].ToString();
                string BrandBlocked = data.Rows[i]["Blocked"].ToString();
                string BrandVerifyIME = data.Rows[i]["VerifyIME"].ToString();
                var brand = new Brand()
                {
                    Code = BrandCode,
                    Name = BrandName,
                    Blocked = BrandBlocked,
                    VerifyIME = BrandVerifyIME

                };

                listBrand.Add(brand);
            }

            JavaScriptSerializer js = new JavaScriptSerializer();

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(js.Serialize(listBrand));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetInvoiceData(string InvoiceNo)
        {
            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "Select Invoice1.SaleManCode,Invoice1.Date1,Invoice1.Manual_InvNo,Invoice1.PartyCode,Invoice1.Name as Name,Invoice1.Particular,Invoice1.Address as Address,Invoice1.Phone as Phone,PartyCode.NorBalance as NorBalance,PartyCode.DealApplyNo as DealApplyNo,PartyCode.CreditLimit, Station.Name as StationName,invoice1.StationID,invoice1.StationID from Invoice1 inner join PartyCode on invoice1.PartyCode=PartyCode.Code left join Station on invoice1.StationID=Station.ID where Invoice1.Compid='01' and Invoice1.InvNo=" + InvoiceNo;
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];


                var Name = data.Rows[0]["Name"].ToString();
                var PartyCode = data.Rows[0]["PartyCode"].ToString();
                var Address = data.Rows[0]["Address"].ToString();
                var Date1 = Convert.ToDateTime(data.Rows[0]["Date1"]).ToString("dd/MM/yyyy");
                var Particular = data.Rows[0]["Particular"].ToString();
                var Phone = data.Rows[0]["Phone"].ToString();
                var NorBalance = data.Rows[0]["NorBalance"].ToString();
                var DealApplyNo = data.Rows[0]["DealApplyNo"].ToString();
                var CreditLimit = data.Rows[0]["CreditLimit"].ToString();
                var StationName = data.Rows[0]["StationName"].ToString();
                var StationID = data.Rows[0]["StationID"].ToString();

                var ClientData = new Client()
                {
                    Date = Date1,
                    Code = PartyCode,
                    Name = Name,
                    Address = Address,
                    PhoneNumber = Phone,
                    Particular = Particular,
                    NorBalance = NorBalance,
                    DealApplyNo = DealApplyNo,
                    CreditLimit = CreditLimit,
                    Balance = "0",
                    StationName = StationName,
                    StationId = StationID,
                    SalesManId = Convert.ToString(data.Rows[0]["SaleManCode"])


                };


                cmd.CommandText =
                "Select Invoice2.SN0 as SN0," +
                    "Invoice2.Code as Code , " +
                    "Invoice2.Description as Description, " +
                    "Invoice2.Rate as Rate, " +
                    "Invoice2.Qty as Qty, " +
                    "Invoice2.Discount as Discount ," +
                    "Invoice2.Amount as Amount, " +
                    "InvCode.Revenue_Code as Revenue_Code , " +
                    "InvCode.CGS_Code as CGS_Code," +
                    "Invoice2.Item_Discount," +
                    "Invoice2.PerDiscount," +
                    "Invoice2.DealRs," +
                    "Invoice2.Purchase_Amount," +
                    "isnull(InvCode.Ave_Cost,0) as Ave_Cost," +
                    "ActualSellingPrice," +
                    "isnull(Invoice2.PerPieceCommission,0) as PerPieceCommission, " +
                    "StationId " +
                "from Invoice2 " +
                "inner join invCode on invoice2.Code=invcode.code " +
                "where Invoice2.Compid='01' and Invoice2.InvNo=" + InvoiceNo;

                var objDs2 = new DataSet();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs2);
                con.Close();
                var dataInv2 = objDs2.Tables[0];
                var Products = new List<Product>();

                for (int i = 0; i < dataInv2.Rows.Count; i++)
                {

                    string sno = dataInv2.Rows[i]["SN0"].ToString();
                    string Code = dataInv2.Rows[i]["Code"].ToString();
                    string Description = dataInv2.Rows[i]["Description"].ToString().Trim();
                    string Rate = dataInv2.Rows[i]["Rate"].ToString();
                    string Qty = dataInv2.Rows[i]["Qty"].ToString();

                    string Revenue_Code = dataInv2.Rows[i]["Revenue_Code"].ToString();
                    string CGS_Code = dataInv2.Rows[i]["CGS_Code"].ToString();
                    string Amount = dataInv2.Rows[i]["Amount"].ToString();
                    string Item_Discount = dataInv2.Rows[i]["Item_Discount"].ToString();
                    string PerDiscount = dataInv2.Rows[i]["PerDiscount"].ToString();
                    string DealRs = dataInv2.Rows[i]["DealRs"].ToString();
                    string Purchase_Amount = dataInv2.Rows[i]["Purchase_Amount"].ToString();
                    string Ave_Cost = dataInv2.Rows[i]["Ave_Cost"].ToString();
                    string ActualSellingPrice = dataInv2.Rows[i]["ActualSellingPrice"].ToString();
                    string PerPieceCommission = dataInv2.Rows[i]["PerPieceCommission"].ToString();
                    string StationId = dataInv2.Rows[i]["StationId"].ToString();

                    var product = new Product()
                    {
                        SNo = sno,
                        Code = Code,
                        ItemName = Description,
                        Rate = Rate,
                        Qty = Qty,
                        Amount = Amount,
                        ItemDis = Item_Discount,
                        PerDis = PerDiscount,
                        DealDis = DealRs,
                        PurAmount = Purchase_Amount,
                        CGSCode = CGS_Code,
                        RevenueCode = Revenue_Code,
                        AverageCost = Ave_Cost,
                        ActualSellingPrice = ActualSellingPrice,
                        PerPieceCommission = PerPieceCommission,
                        ItemStationId = StationId

                    };
                    Products.Add(product);
                }



                cmd.CommandText = "Select PrevBalance,Total,DiscountRate,Discount1,CashPaid,Balance,FlatDiscount,Flat_Discount_Per from Invoice3 where Compid='01' and InvNo=" + InvoiceNo;
                var objDs3 = new DataSet();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs3);
                con.Close();
                var dataInv3 = objDs3.Tables[0];


                var Total = dataInv3.Rows[0]["Total"].ToString();
                var DiscountRate = dataInv3.Rows[0]["DiscountRate"].ToString();
                var Discount1 = dataInv3.Rows[0]["Discount1"].ToString();
                var CashPaid = dataInv3.Rows[0]["CashPaid"].ToString();
                var Balance = dataInv3.Rows[0]["Balance"].ToString();
                var FlatDiscount = dataInv3.Rows[0]["FlatDiscount"].ToString();
                var Flat_Discount_Per = dataInv3.Rows[0]["Flat_Discount_Per"].ToString();
                var PrevBalance = dataInv3.Rows[0]["PrevBalance"].ToString();

                var TotalData = new TotalData()
                {
                    GrossTotal = Total,
                    FlatDiscount = FlatDiscount,
                    FlatPer = Flat_Discount_Per,
                    Recieved = CashPaid,
                    PreviousBalance = PrevBalance,
                };



                cmd.CommandText = @"Select ItemCode, Description, IME, Price_Cost,DisPer,DealRs
                                    from InventorySerialNoFinal 
                                    where Compid='" + CompID + "' and Description='" + IMEDescription.Sale + "' and VoucherNo='" + InvoiceNo + "'";
                var objDs4 = new DataSet();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs4);
                con.Close();
                var dataInv4 = objDs4.Tables[0];
                var IMEs = new List<Product>();

                for (int i = 0; i < dataInv4.Rows.Count; i++)
                {

                    string ItemCode = dataInv4.Rows[i]["ItemCode"].ToString();
                    string Description = dataInv4.Rows[i]["Description"].ToString().Trim();
                    string IME = dataInv4.Rows[i]["IME"].ToString();
                    string Price_Cost = dataInv4.Rows[i]["Price_Cost"].ToString();
                    string PerDis = dataInv4.Rows[i]["DisPer"].ToString();
                    string DealDis = dataInv4.Rows[i]["DealRs"].ToString();


                    var product = new Product()
                    {

                        Code = ItemCode,
                        ItemName = Description,
                        Rate = Price_Cost,
                        IMEI = IME,
                        PerDis = PerDis,
                        DealDis = DealDis
                    };
                    IMEs.Add(product);
                }

                SqlDataAdapter invoiceAdptr = new SqlDataAdapter(@"select
	                InvNo
	                , prev_invNo = (
		            select top 1 invNo 
		            from Invoice1 p 
		            where p.InvNo < i.InvNo and
                    CompId='" + CompID + @"'
		            order by InvNo desc
		            )
	                , next_invNo = (
		            select top 1 invNo 
		            from Invoice1 n 

		            where n.InvNo > i.InvNo and
                    CompId='" + CompID + @"'
                    order by InvNo asc
		            )
                    from Invoice1 as i
		            where i.InvNo = " + InvoiceNo, con);
                DataTable invoiceDataTable = new DataTable();
                invoiceAdptr.Fill(invoiceDataTable);

                var invoices = new Invoices()
                {
                    PreviousInvoice = Convert.ToString(invoiceDataTable.Rows[0]["prev_invNo"]),
                    NextInvoice = Convert.ToString(invoiceDataTable.Rows[0]["next_invNo"]),
                    CurrentInvoice = InvoiceNo

                };



                var SaveProductViewModel = new SaveProductViewModel()
                {
                    NextPreviousInvoiceNumbers = invoices,
                    TotalData = TotalData,
                    ClientData = ClientData,
                    Products = Products,
                    IMEItems = IMEs,
                    Success = true
                };
                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(SaveProductViewModel));
            }
            catch (Exception ex)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var SaveProductViewModel = new SaveProductViewModel()
                {

                    Success = false,
                    Message = ex.Message

                };
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(SaveProductViewModel));
            }

            finally
            {
                con.Close();
            }

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetPurchaseInvoiceData(string InvoiceNo)
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            try
            {

                //cmd.CommandText = "Select Invoice1.Date1,Invoice1.Manual_InvNo,Invoice1.PartyCode,Invoice1.Name as Name,Invoice1.Particular,Invoice1.Address as Address,Invoice1.Phone as Phone,PartyCode.NorBalance as NorBalance,PartyCode.DealApplyNo as DealApplyNo,PartyCode.CreditLimit from Invoice1 inner join PartyCode on invoice1.PartyCode=PartyCode.Code where Invoice1.Compid='01' and Invoice1.InvNo=" + InvoiceNo;
                cmd.CommandText = @"Select 
                    Purchase1.Vender  ,   
                    Purchase1.VenderCode  , 
                    Purchase1.dat  , 
                    Purchase1.InvNo  , 
                    Purchase1.Particular, 
                    Purchase1.Address  , 
                    Purchase1.Phone , 
                    PartyCode.NorBalance as NorBalance,
                    PartyCode.DealApplyNo as DealApplyNo,
                    PartyCode.CreditLimit, 
                    Station.Name as StationName,
                    Purchase1.StationID 
                    from Purchase1 inner 
join PartyCode on Purchase1.VenderCode=PartyCode.Code left join Station on Purchase1.StationID=Station.ID where Purchase1.CompID = '" + CompID + "' and Purchase1.InvNo = '" + InvoiceNo + "'";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];

                var Name = data.Rows[0]["Vender"].ToString();
                var PartyCode = data.Rows[0]["VenderCode"].ToString();
                var Address = data.Rows[0]["Address"].ToString();
                var Date1 = Convert.ToDateTime(data.Rows[0]["dat"].ToString()).ToString("dd/MM/yyyy");
                var Particular = data.Rows[0]["Particular"].ToString();
                var Phone = data.Rows[0]["Phone"].ToString();
                var NorBalance = data.Rows[0]["NorBalance"].ToString();
                var DealApplyNo = data.Rows[0]["DealApplyNo"].ToString();
                var CreditLimit = data.Rows[0]["CreditLimit"].ToString();
                var StationName = data.Rows[0]["StationName"].ToString();
                var StationID = data.Rows[0]["StationID"].ToString();

                var ClientData = new Client()
                {
                    Date = Date1,
                    Code = PartyCode,
                    Name = Name,
                    Address = Address,
                    PhoneNumber = Phone,
                    Particular = Particular,
                    NorBalance = NorBalance,
                    DealApplyNo = DealApplyNo,
                    CreditLimit = CreditLimit,
                    Balance = "0",
                    StationName = StationName,
                    StationId = StationID

                };
                cmd.CommandText = "Select " +
                    "Purchase2.SNO, " +
                    "Purchase2.Code, " +
                    "Purchase2.Description, " +
                    "Purchase2.Qty, " +
                    "Purchase2.Cost, " +
                    "Purchase2.Amount, " +
                    "Purchase2.DiscountPercentageRate, " +
                    "Purchase2.Item_Discount, " +
                    "Purchase2.CostAfterDiscount, " +
                    "Purchase2.DealRs, " +
                    "InvCode.Revenue_Code as Revenue_Code , " +
                    "InvCode.CGS_Code as CGS_Code, " +
                    "Purchase2.StationId, " +
                    "InvCode.SellingCost " +
                    "from  Purchase2 " +
                    "inner join invCode on Purchase2.Code=invcode.code where Purchase2.CompID = '" + CompID + "' and Purchase2.InvNo = '" + InvoiceNo + "' order by SNO";
                var objDs2 = new DataSet();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs2);
                con.Close();
                var dataInv2 = objDs2.Tables[0];
                var Products = new List<Product>();
                for (int i = 0; i < dataInv2.Rows.Count; i++)
                {
                    string sno = dataInv2.Rows[i]["SNO"].ToString();
                    string Code = dataInv2.Rows[i]["Code"].ToString();
                    string Description = dataInv2.Rows[i]["Description"].ToString().Trim();
                    string Rate = dataInv2.Rows[i]["Cost"].ToString();
                    string Qty = dataInv2.Rows[i]["Qty"].ToString();
                    string Revenue_Code = dataInv2.Rows[i]["Revenue_Code"].ToString();
                    string CGS_Code = dataInv2.Rows[i]["CGS_Code"].ToString();
                    string Amount = dataInv2.Rows[i]["Amount"].ToString();
                    string Item_Discount = dataInv2.Rows[i]["Item_Discount"].ToString();
                    string PerDiscount = dataInv2.Rows[i]["DiscountPercentageRate"].ToString();
                    string DealRs = dataInv2.Rows[i]["DealRs"].ToString();
                    string Purchase_Amount = "0"; // dataInv2.Rows[i]["Purchase_Amount"].ToString();
                    string Ave_Cost = "0"; // dataInv2.Rows[i]["Ave_Cost"].ToString();
                    string StationId = dataInv2.Rows[i]["StationId"].ToString();
                    string SellingPrice = dataInv2.Rows[i]["SellingCost"].ToString();
                    var product = new Product()
                    {
                        SNo = sno,
                        Code = Code,
                        ItemName = Description,
                        Rate = Rate,
                        Qty = Qty,
                        Amount = Amount,
                        ItemDis = Item_Discount,
                        PerDis = PerDiscount,
                        DealDis = DealRs,
                        PurAmount = Purchase_Amount,
                        CGSCode = CGS_Code,
                        RevenueCode = Revenue_Code,
                        AverageCost = Ave_Cost,
                        ItemStationId = StationId,
                        SellingPrice = SellingPrice,


                    };
                    Products.Add(product);
                }
                //cmd.CommandText = "Select Total,DiscountRate,Discount1,CashPaid,Balance,FlatDiscount,Flat_Discount_Per,CheqAmount from Invoice3 where Compid='01' and InvNo=" + InvoiceNo;
                cmd.CommandText = "Select " +
                    "Total," +
                    "Paid," +
                    "Discount," +
                    "FlatDiscount," +
                    "Flat_Discount_Per," +
                    "OtherCharges " +
                    "from Purchase3 where  CompID='" + CompID + "' and InvNo='" + InvoiceNo + "'";
                var objDs3 = new DataSet();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs3);
                con.Close();
                var dataInv3 = objDs3.Tables[0];
                var Total = dataInv3.Rows[0]["Total"].ToString();
                var DiscountRate = dataInv3.Rows[0]["Flat_Discount_Per"].ToString();
                var Discount1 = dataInv3.Rows[0]["FlatDiscount"].ToString();
                var CashPaid = dataInv3.Rows[0]["Paid"].ToString();
                var FlatDiscount = dataInv3.Rows[0]["FlatDiscount"].ToString();
                var Flat_Discount_Per = dataInv3.Rows[0]["Flat_Discount_Per"].ToString();

                var TotalData = new TotalData()
                {
                    GrossTotal = Total,
                    FlatDiscount = FlatDiscount,
                    FlatPer = Flat_Discount_Per,
                    Recieved = CashPaid,
                };

                cmd.CommandText = @"Select ItemCode, Description, IME, Price_Cost,DisPer,DealRs
                                    from InventorySerialNoFinal 
                                    where Compid='" + CompID + "' and Description='" + IMEDescription.Purchase + "' and VoucherNo='" + InvoiceNo + "'";
                var objDs4 = new DataSet();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs4);
                con.Close();
                var dataInv4 = objDs4.Tables[0];
                var IMEs = new List<Product>();

                for (int i = 0; i < dataInv4.Rows.Count; i++)
                {

                    string ItemCode = dataInv4.Rows[i]["ItemCode"].ToString();
                    string Description = dataInv4.Rows[i]["Description"].ToString().Trim();
                    string IME = dataInv4.Rows[i]["IME"].ToString();
                    string Price_Cost = dataInv4.Rows[i]["Price_Cost"].ToString();
                    string PerDis = dataInv4.Rows[i]["DisPer"].ToString();
                    string DealDis = dataInv4.Rows[i]["DealRs"].ToString();

                    var product = new Product()
                    {

                        Code = ItemCode,
                        ItemName = Description,
                        Rate = Price_Cost,
                        IMEI = IME,
                        PerDis = PerDis,
                        DealDis = DealDis

                    };
                    IMEs.Add(product);
                }
                var security = new Security.Security();

                var loggedUserTempId = Application["UserTempId"].ToString();

                if (security.isTableExist("TempTable" + CompID, con))
                {
                    var dropTableCommand = new SqlCommand("drop Table TempTable" + CompID, con);
                    if (con.State == ConnectionState.Closed)
                    {
                        if (con.State == ConnectionState.Closed) { con.Open(); }
                    }
                    dropTableCommand.ExecuteNonQuery();
                    con.Close();
                }

                var invoiceAdptr = new SqlDataAdapter(@"SELECT
                rownum = IDENTITY(INT, 1,1),
                p.InvNo,
                UserTempId='" + loggedUserTempId + @"'
                INTO TempTable" + CompID + @"
                FROM Purchase1 p
                Where CompId=" + CompID + @"
                GROUP BY p.InvNo
                ORDER BY Max(dat)  desc;

                SELECT
                prev.InvNo prev_invNo,
                TT.InvNo,
                nex.InvNo next_invNo
                FROM TempTable" + CompID + @" TT
                LEFT JOIN TempTable" + CompID + @" prev ON prev.rownum = TT.rownum - 1
                LEFT JOIN TempTable" + CompID + @" nex ON nex.rownum = TT.rownum + 1
                where TT.InvNo='" + InvoiceNo + @"' and
                TT.UserTempId='" + loggedUserTempId + "'", con);


                DataTable invoiceDataTable = new DataTable();
                invoiceAdptr.Fill(invoiceDataTable);
                Invoices invoices = null;
                if (invoiceDataTable.Rows.Count > 0)
                {
                    invoices = new Invoices()
                    {
                        PreviousInvoice = Convert.ToString(invoiceDataTable.Rows[0]["next_invNo"]),
                        NextInvoice = Convert.ToString(invoiceDataTable.Rows[0]["prev_invNo"]),

                        CurrentInvoice = InvoiceNo

                    };
                }

                if (security.isTableExist("TempTable" + CompID, con))
                {
                    var dropTableCommand = new SqlCommand("drop Table TempTable" + CompID, con);
                    if (con.State == ConnectionState.Closed)
                    {
                        if (con.State == ConnectionState.Closed) { con.Open(); }
                    }
                    dropTableCommand.ExecuteNonQuery();
                    con.Close();
                }


                var SaveProductViewModel = new SaveProductViewModel()
                {
                    NextPreviousInvoiceNumbers = invoices,
                    TotalData = TotalData,
                    ClientData = ClientData,
                    Products = Products,
                    IMEItems = IMEs,
                    Success = true
                };
                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(SaveProductViewModel));
            }
            catch (Exception ex)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var SaveProductViewModel = new SaveProductViewModel()
                {
                    Success = false,
                    Message = ex.Message
                };
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(SaveProductViewModel));
            }
            finally
            {
                con.Close();
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetPurchaseReturnInvoiceData(string InvoiceNo)
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            try
            {

                //cmd.CommandText = "Select Invoice1.Date1,Invoice1.Manual_InvNo,Invoice1.PartyCode,Invoice1.Name as Name,Invoice1.Particular,Invoice1.Address as Address,Invoice1.Phone as Phone,PartyCode.NorBalance as NorBalance,PartyCode.DealApplyNo as DealApplyNo,PartyCode.CreditLimit from Invoice1 inner join PartyCode on invoice1.PartyCode=PartyCode.Code where Invoice1.Compid='01' and Invoice1.InvNo=" + InvoiceNo;
                cmd.CommandText = "select PurchaseReturn1.Phone,PurchaseReturn1.PartyCode,PurchaseReturn1.Name,PurchaseReturn1.Date1,PurchaseReturn1.Particular,PurchaseReturn1.Address,PartyCode.NorBalance as NorBalance,PartyCode.DealApplyNo as DealApplyNo,PartyCode.CreditLimit, Station.Name as StationName,PurchaseReturn1.StationID from PurchaseReturn1 inner join PartyCode on PurchaseReturn1.PartyCode=PartyCode.Code left join Station on PurchaseReturn1.StationID=Station.ID where  PurchaseReturn1.CompID='" + CompID + "' and InvNo=" + Convert.ToDecimal(InvoiceNo);
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];

                var Name = data.Rows[0]["Name"].ToString();
                var PartyCode = data.Rows[0]["PartyCode"].ToString();
                var Address = data.Rows[0]["Address"].ToString();
                var Date1 = Convert.ToDateTime(data.Rows[0]["Date1"].ToString()).ToString("dd/MM/yyyy");
                var Particular = data.Rows[0]["Particular"].ToString();
                var Phone = data.Rows[0]["Phone"].ToString();
                var NorBalance = data.Rows[0]["NorBalance"].ToString();
                var DealApplyNo = data.Rows[0]["DealApplyNo"].ToString();
                var CreditLimit = data.Rows[0]["CreditLimit"].ToString();
                var StationName = data.Rows[0]["StationName"].ToString();
                var StationID = data.Rows[0]["StationID"].ToString();

                var ClientData = new Client()
                {
                    Date = Date1,
                    Code = PartyCode,
                    Name = Name,
                    Address = Address,
                    PhoneNumber = Phone,
                    Particular = Particular,
                    NorBalance = NorBalance,
                    DealApplyNo = DealApplyNo,
                    CreditLimit = CreditLimit,
                    Balance = "0",
                    StationName = StationName,
                    StationId = StationID

                };
                //cmd.CommandText = "Select Purchase2.SNO, Purchase2.Code, Purchase2.Description, Purchase2.Qty, Purchase2.Cost, Purchase2.Amount, Purchase2.DiscountPercentageRate, Purchase2.Item_Discount, Purchase2.CostAfterDiscount, Purchase2.DealRs, InvCode.Revenue_Code as Revenue_Code , InvCode.CGS_Code as CGS_Code from  Purchase2 inner join invCode on Purchase2.Code=invcode.code where Purchase2.CompID = '" + CompID + "' and Purchase2.InvNo = '" + InvoiceNo + "' order by SNO";
                cmd.CommandText = "select PurchaseReturn2.SN0,PurchaseReturn2.Code,PurchaseReturn2.Description,PurchaseReturn2.Qty,PurchaseReturn2.Rate,PurchaseReturn2.Amount,PurchaseReturn2.PerDiscount,PurchaseReturn2.Item_Discount,PurchaseReturn2.DealRs,InvCode.Revenue_Code as Revenue_Code , InvCode.CGS_Code as CGS_Code, PurchaseReturn2.StationId from PurchaseReturn2 inner join invCode on PurchaseReturn2.Code=invcode.code where  PurchaseReturn2.CompID='" + CompID + "' and PurchaseReturn2.InvNo=" + Convert.ToDecimal(InvoiceNo) + "  order by PurchaseReturn2.SN0";

                var objDs2 = new DataSet();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs2);
                con.Close();
                var dataInv2 = objDs2.Tables[0];
                var Products = new List<Product>();
                for (int i = 0; i < dataInv2.Rows.Count; i++)
                {

                    string sno = dataInv2.Rows[i]["SN0"].ToString();
                    string Code = dataInv2.Rows[i]["Code"].ToString();
                    string Description = dataInv2.Rows[i]["Description"].ToString().Trim();
                    string Rate = dataInv2.Rows[i]["Rate"].ToString();
                    string Qty = dataInv2.Rows[i]["Qty"].ToString();
                    string Revenue_Code = dataInv2.Rows[i]["Revenue_Code"].ToString();
                    string CGS_Code = dataInv2.Rows[i]["CGS_Code"].ToString();
                    string Amount = dataInv2.Rows[i]["Amount"].ToString();
                    string Item_Discount = dataInv2.Rows[i]["Item_Discount"].ToString();
                    string PerDiscount = dataInv2.Rows[i]["PerDiscount"].ToString();
                    string DealRs = dataInv2.Rows[i]["DealRs"].ToString();
                    string Purchase_Amount = "0"; // dataInv2.Rows[i]["Purchase_Amount"].ToString();
                    string Ave_Cost = "0"; // dataInv2.Rows[i]["Ave_Cost"].ToString();
                    string StationId = dataInv2.Rows[i]["StationId"].ToString();

                    var product = new Product()
                    {
                        SNo = sno,
                        Code = Code,
                        ItemName = Description,
                        Rate = Rate,
                        Qty = Qty,
                        Amount = Amount,
                        ItemDis = Item_Discount,
                        PerDis = PerDiscount,
                        DealDis = DealRs,
                        PurAmount = Purchase_Amount,
                        CGSCode = CGS_Code,
                        RevenueCode = Revenue_Code,
                        AverageCost = Ave_Cost,
                        ItemStationId = StationId

                    };
                    Products.Add(product);
                }
                //cmd.CommandText = "Select Total,DiscountRate,Discount1,CashPaid,Balance,FlatDiscount,Flat_Discount_Per,CheqAmount from Invoice3 where Compid='01' and InvNo=" + InvoiceNo;
                cmd.CommandText = "select PurchaseReturn1.Flat_Discount_Per as Flat_Discount_Per,PurchaseReturn1.Phone,PurchaseReturn1.Total,PurchaseReturn1.Discount,PurchaseReturn1.Paid,PurchaseReturn1.FlatDiscount,PartyCode.NorBalance as NorBalance,PartyCode.DealApplyNo as DealApplyNo,PartyCode.CreditLimit from PurchaseReturn1 inner join PartyCode on PurchaseReturn1.PartyCode=PartyCode.Code where  PurchaseReturn1.CompID='" + CompID + "' and InvNo=" + Convert.ToDecimal(InvoiceNo);
                var objDs3 = new DataSet();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs3);
                con.Close();
                var dataInv3 = objDs3.Tables[0];
                var Total = dataInv3.Rows[0]["Total"].ToString();

                var Discount1 = dataInv3.Rows[0]["FlatDiscount"].ToString();
                var CashPaid = dataInv3.Rows[0]["Paid"].ToString();
                var Flat_Discount_Per = dataInv3.Rows[0]["Flat_Discount_Per"].ToString();

                var TotalData = new TotalData()
                {
                    GrossTotal = Total,
                    FlatDiscount = Discount1,
                    FlatPer = Flat_Discount_Per,
                    Recieved = CashPaid,
                };
                cmd.CommandText = @"Select ItemCode, Description, IME, Price_Cost,DisPer,DealRs
                                    from InventorySerialNoFinal 
                                    where Compid='" + CompID + "' and Description='" + IMEDescription.PurchaseReturn + "' and VoucherNo='" + InvoiceNo + "'";
                var objDs4 = new DataSet();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs4);
                con.Close();
                var dataInv4 = objDs4.Tables[0];
                var IMEs = new List<Product>();

                for (int i = 0; i < dataInv4.Rows.Count; i++)
                {

                    string ItemCode = dataInv4.Rows[i]["ItemCode"].ToString();
                    string Description = dataInv4.Rows[i]["Description"].ToString().Trim();
                    string IME = dataInv4.Rows[i]["IME"].ToString();
                    string Price_Cost = dataInv4.Rows[i]["Price_Cost"].ToString();

                    string PerDis = dataInv4.Rows[i]["DisPer"].ToString();
                    string DealDis = dataInv4.Rows[i]["DealRs"].ToString();

                    var product = new Product()
                    {

                        Code = ItemCode,
                        ItemName = Description,
                        Rate = Price_Cost,
                        IMEI = IME,
                        PerDis = PerDis,
                        DealDis = DealDis

                    };
                    IMEs.Add(product);
                }
                var security = new Security.Security();

                var loggedUserTempId = Application["UserTempId"].ToString();

                if (security.isTableExist("TempTable" + CompID, con))
                {
                    var dropTableCommand = new SqlCommand("drop Table TempTable" + CompID, con);
                    if (con.State == ConnectionState.Closed)
                    {
                        if (con.State == ConnectionState.Closed) { con.Open(); }
                    }
                    dropTableCommand.ExecuteNonQuery();
                    con.Close();
                }


                var invoiceAdptr = new SqlDataAdapter(@"SELECT
                rownum = IDENTITY(INT, 1,1),
                p.InvNo,
                UserTempId='" + loggedUserTempId + @"'
                INTO TempTable" + CompID + @"
                FROM PurchaseReturn1 p
                Where CompId=" + CompID + @"
                GROUP BY p.InvNo
                ORDER BY Max(Date1)  desc;

                SELECT
                prev.InvNo prev_invNo,
                TT.InvNo,
                nex.InvNo next_invNo
                FROM TempTable" + CompID + @" TT
                LEFT JOIN TempTable" + CompID + @" prev ON prev.rownum = TT.rownum - 1
                LEFT JOIN TempTable" + CompID + @" nex ON nex.rownum = TT.rownum + 1
                where TT.InvNo='" + InvoiceNo + @"' and
                TT.UserTempId='" + loggedUserTempId + "'", con);


                DataTable invoiceDataTable = new DataTable();
                invoiceAdptr.Fill(invoiceDataTable);
                Invoices invoices = null;
                if (invoiceDataTable.Rows.Count > 0)
                {
                    invoices = new Invoices()
                    {
                        PreviousInvoice = Convert.ToString(invoiceDataTable.Rows[0]["next_invNo"]),
                        NextInvoice = Convert.ToString(invoiceDataTable.Rows[0]["prev_invNo"]),

                        CurrentInvoice = InvoiceNo

                    };
                }

                if (security.isTableExist("TempTable" + CompID, con))
                {
                    var dropTableCommand = new SqlCommand("drop Table TempTable" + CompID, con);
                    if (con.State == ConnectionState.Closed)
                    {
                        if (con.State == ConnectionState.Closed) { con.Open(); }
                    }
                    dropTableCommand.ExecuteNonQuery();
                    con.Close();
                }
                var SaveProductViewModel = new SaveProductViewModel()
                {
                    NextPreviousInvoiceNumbers = invoices,
                    TotalData = TotalData,
                    ClientData = ClientData,
                    Products = Products,
                    IMEItems = IMEs,
                    Success = true
                };
                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(SaveProductViewModel));
            }
            catch (Exception ex)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var SaveProductViewModel = new SaveProductViewModel()
                {
                    Success = false,
                    Message = ex.Message
                };
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(SaveProductViewModel));
            }
            finally
            {
                con.Close();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string PurchaseBillAlreadyExist(string InvoiceNo)
        {
            ModItem objModItem = new ModItem();
            var PurchaseBillAlreadyExist = objModItem.PurchaseBillAlreadyExist(InvoiceNo);

            return PurchaseBillAlreadyExist;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetInvoiceDataSaleReturn(string InvoiceNo)
        {

            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            //cmd.CommandText = "Select Invoice1.Date1,Invoice1.Manual_InvNo,Invoice1.PartyCode,Invoice1.Name as Name,Invoice1.Particular,Invoice1.Address as Address,Invoice1.Phone as Phone,PartyCode.NorBalance as NorBalance,PartyCode.DealApplyNo as DealApplyNo,PartyCode.CreditLimit from Invoice1 inner join PartyCode on invoice1.PartyCode=PartyCode.Code where Invoice1.Compid='01' and Invoice1.InvNo=" + InvoiceNo;
            cmd.CommandText = "Select SaleReturn1.SalesMan,SaleReturn1.Phone , SaleReturn1.Date1 , SaleReturn1.PartyCode , SaleReturn1.Name , SaleReturn1.SalesMan , SaleReturn1.Particular , SaleReturn1.Total , SaleReturn1.Discount , SaleReturn1.Paid , SaleReturn1.FlatDiscount , SaleReturn1.Address , PartyCode.NorBalance as NorBalance , PartyCode.DealApplyNo as DealApplyNo , PartyCode.CreditLimit, Station.Name as StationName,SaleReturn1.StationID from SaleReturn1 inner join PartyCode on SaleReturn1.PartyCode=PartyCode.Code left join Station on SaleReturn1.StationID=Station.ID where SaleReturn1.CompID='" + CompID + "' and  SaleReturn1.invno=" + InvoiceNo;

            try
            {
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];

                var Date = Convert.ToDateTime(data.Rows[0]["Date1"]).ToString("dd/MM/yyyy");
                var Name = data.Rows[0]["Name"].ToString();
                var PartyCode = data.Rows[0]["PartyCode"].ToString();
                var Address = data.Rows[0]["Address"].ToString();
                var Date1 = data.Rows[0]["Date1"].ToString();
                var Particular = data.Rows[0]["Particular"].ToString();
                var Phone = data.Rows[0]["Phone"].ToString();
                var NorBalance = data.Rows[0]["NorBalance"].ToString();
                var DealApplyNo = data.Rows[0]["DealApplyNo"].ToString();
                var CreditLimit = data.Rows[0]["CreditLimit"].ToString();
                var StationName = data.Rows[0]["StationName"].ToString();
                var StationID = data.Rows[0]["StationID"].ToString();
                var SalesManCode = data.Rows[0]["SalesMan"].ToString();

                var ClientData = new Client()
                {
                    Date = Date,
                    Code = PartyCode,
                    Name = Name,
                    Address = Address,
                    PhoneNumber = Phone,
                    Particular = Particular,
                    NorBalance = NorBalance,
                    DealApplyNo = DealApplyNo,
                    CreditLimit = CreditLimit,
                    Balance = "0",
                    StationName = StationName,
                    StationId = StationID,
                    SalesManId = SalesManCode

                };


                //cmd.CommandText = "Select Invoice2.SN0 as SN0,Invoice2.Code as Code , Invoice2.Description as Description,Invoice2.Rate as Rate, Invoice2.Qty as Qty,Invoice2.Discount as Discount ,Invoice2.Amount as Amount, InvCode.Revenue_Code as Revenue_Code , InvCode.CGS_Code as CGS_Code,Invoice2.Item_Discount,Invoice2.PerDiscount,Invoice2.DealRs,Invoice2.Purchase_Amount,isnull(InvCode.Ave_Cost,0) as Ave_Cost from Invoice2 inner join invCode on invoice2.Code=invcode.code where Invoice2.Compid='01' and Invoice2.InvNo=" + InvoiceNo;
                cmd.CommandText = "select SaleReturn2.SN0,SaleReturn2.Code,SaleReturn2.Description,SaleReturn2.Qty,SaleReturn2.Rate,SaleReturn2.Amount,SaleReturn2.Purchase_Amount,SaleReturn2.ItemDis,SaleReturn2.Per_Dis,SaleReturn2.DealRs, isnull(InvCode.Ave_Cost,0) as Ave_Cost, InvCode.Revenue_Code as Revenue_Code , InvCode.CGS_Code as CGS_Code,SaleReturn2.StationId from SaleReturn2 inner join invCode on SaleReturn2.Code=invcode.code where SaleReturn2.CompID='" + CompID + "' and SaleReturn2.invno=" + InvoiceNo;

                var objDs2 = new DataSet();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs2);
                con.Close();
                var dataInv2 = objDs2.Tables[0];
                var Products = new List<Product>();

                for (int i = 0; i < dataInv2.Rows.Count; i++)
                {

                    string sno = dataInv2.Rows[i]["SN0"].ToString();
                    string Code = dataInv2.Rows[i]["Code"].ToString();
                    string Description = dataInv2.Rows[i]["Description"].ToString().Trim();
                    string Rate = dataInv2.Rows[i]["Rate"].ToString();
                    string Qty = dataInv2.Rows[i]["Qty"].ToString();

                    string Revenue_Code = dataInv2.Rows[i]["Revenue_Code"].ToString();
                    string CGS_Code = dataInv2.Rows[i]["CGS_Code"].ToString();
                    string Amount = dataInv2.Rows[i]["Amount"].ToString();
                    string Item_Discount = dataInv2.Rows[i]["ItemDis"].ToString();
                    string PerDiscount = dataInv2.Rows[i]["Per_Dis"].ToString();
                    string DealRs = dataInv2.Rows[i]["DealRs"].ToString();
                    string Purchase_Amount = dataInv2.Rows[i]["Purchase_Amount"].ToString();
                    string Ave_Cost = dataInv2.Rows[i]["Ave_Cost"].ToString();
                    string StationId = dataInv2.Rows[i]["StationId"].ToString();

                    var product = new Product()
                    {
                        SNo = sno,
                        Code = Code,
                        ItemName = Description,
                        Rate = Rate,
                        Qty = Qty,
                        Amount = Amount,
                        ItemDis = Item_Discount,
                        PerDis = PerDiscount,
                        DealDis = DealRs,
                        PurAmount = Purchase_Amount,
                        CGSCode = CGS_Code,
                        RevenueCode = Revenue_Code,
                        AverageCost = Ave_Cost,
                        ItemStationId = StationId

                    };
                    Products.Add(product);
                }



                //cmd.CommandText = "Select Total,DiscountRate,Discount1,CashPaid,Balance,FlatDiscount,Flat_Discount_Per,CheqAmount from Invoice3 where Compid='01' and InvNo=" + InvoiceNo;
                cmd.CommandText = "Select  Total , Discount , Paid , FlatDiscount  from SaleReturn1  where CompID='" + CompID + "' and  invno=" + InvoiceNo;
                var objDs3 = new DataSet();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs3);
                con.Close();
                var dataInv3 = objDs3.Tables[0];


                var Total = dataInv3.Rows[0]["Total"].ToString();
                var Discount1 = dataInv3.Rows[0]["Discount"].ToString();
                var CashPaid = dataInv3.Rows[0]["Paid"].ToString();
                var FlatDiscount = dataInv3.Rows[0]["FlatDiscount"].ToString();
                var Flat_Discount_Per = "0"; // dataInv3.Rows[0]["Flat_Discount_Per"].ToString();

                var TotalData = new TotalData()
                {
                    GrossTotal = Total,
                    FlatDiscount = FlatDiscount,
                    FlatPer = Flat_Discount_Per,
                    Recieved = CashPaid,

                };


                cmd.CommandText = @"Select ItemCode, Description, IME, Price_Cost,DisPer,DealRs
                                    from InventorySerialNoFinal 
                                    where Compid='" + CompID + "' and Description='" + IMEDescription.SaleReturn + "' and VoucherNo='" + InvoiceNo + "'";
                var objDs4 = new DataSet();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs4);
                con.Close();
                var dataInv4 = objDs4.Tables[0];
                var IMEs = new List<Product>();

                for (int i = 0; i < dataInv4.Rows.Count; i++)
                {

                    string ItemCode = dataInv4.Rows[i]["ItemCode"].ToString();
                    string Description = dataInv4.Rows[i]["Description"].ToString().Trim();
                    string IME = dataInv4.Rows[i]["IME"].ToString();
                    string Price_Cost = dataInv4.Rows[i]["Price_Cost"].ToString();
                    string PerDis = dataInv4.Rows[i]["DisPer"].ToString();
                    string DealDis = dataInv4.Rows[i]["DealRs"].ToString();


                    var product = new Product()
                    {

                        Code = ItemCode,
                        ItemName = Description,
                        Rate = Price_Cost,
                        IMEI = IME,
                        PerDis = PerDis,
                        DealDis = DealDis

                    };
                    IMEs.Add(product);
                }



                SqlDataAdapter invoiceAdptr = new SqlDataAdapter(@"select
	    InvNo
	    , prev_invNo = (
		select top 1 invNo 
		from SaleReturn1 p 
		where p.InvNo < i.InvNo and
        CompId='" + CompID + @"'
		order by InvNo desc
		)
	    , next_invNo = (
		select top 1 invNo  
		from SaleReturn1 n 

		where n.InvNo > i.InvNo and
        CompId='" + CompID + @"'
        order by InvNo asc
		)
        from SaleReturn1 as i
		where i.InvNo = " + InvoiceNo, con);
                DataTable invoiceDataTable = new DataTable();
                invoiceAdptr.Fill(invoiceDataTable);

                var invoices = new Invoices()
                {
                    PreviousInvoice = Convert.ToString(invoiceDataTable.Rows[0]["prev_invNo"]),
                    NextInvoice = Convert.ToString(invoiceDataTable.Rows[0]["next_invNo"]),
                    CurrentInvoice = InvoiceNo

                };



                var SaveProductViewModel = new SaveProductViewModel()
                {
                    NextPreviousInvoiceNumbers = invoices,
                    TotalData = TotalData,
                    ClientData = ClientData,
                    Products = Products,
                    IMEItems = IMEs,
                    Success = true

                };




                JavaScriptSerializer js = new JavaScriptSerializer();

                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(SaveProductViewModel));
            }
            catch (Exception ex)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var SaveProductViewModel = new SaveProductViewModel()
                {

                    Success = false,
                    Message = ex.Message

                };
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(SaveProductViewModel));
            }
            finally
            {
                con.Close();
            }
        }




        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void BarCodeCategoryList()
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "Select id,Name from BarCodeCategory where Compid='01'";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];
            var list_BarCodeCategory = new List<Brand>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string Name = data.Rows[i]["Name"].ToString();
                string id = data.Rows[i]["id"].ToString();
                Name = Name + "*" + id;
                var BarCodeCategory = new Brand()
                {
                    Name = Name,
                };
                list_BarCodeCategory.Add(BarCodeCategory);
            }
            var itemComboList = new ItemComboList()
            {
                BarCodeCategory = list_BarCodeCategory,
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(js.Serialize(itemComboList));
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetPartyDataAgainstPartyCode(string PartyCode)
        {
            try
            {
                ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "Select Name,OwnerName,Address,Email,Phone,PostalCode,City,Country,Fax,Mobile,NorBalance,CreditLimit,LimitApplicable,SellingPrice,OtherInfo,SalManCode,GroupID,DealApplyNo,DealRs,DisPer from PartyCode where Code='" + PartyCode + "'";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];
                int i = 0;
                string PartyName = data.Rows[i]["Name"].ToString();
                string ContactPerson = data.Rows[i]["OwnerName"].ToString();
                string Address = data.Rows[i]["Address"].ToString();
                string Email = data.Rows[i]["Email"].ToString();
                string LandLineNo = data.Rows[i]["Phone"].ToString();
                string DealRs = data.Rows[i]["DealRs"].ToString();
                string DisPer = data.Rows[i]["DisPer"].ToString();
                string FaxNo = data.Rows[i]["Fax"].ToString();
                string MobileNo = data.Rows[i]["Mobile"].ToString();
                string NorBalance = data.Rows[i]["NorBalance"].ToString();
                string CreditLimit = data.Rows[i]["CreditLimit"].ToString();
                string chkCreditLimitApply = data.Rows[i]["LimitApplicable"].ToString();
                string lstSellingPriceNo = data.Rows[i]["SellingPrice"].ToString();
                string OtherInfo = data.Rows[i]["OtherInfo"].ToString();
                string lstSaleMan = data.Rows[i]["SalManCode"].ToString(); // objModPartyCodeAgainstName.SaleManNameAgainstSaleManCode(Convert.ToInt16( data.Rows[i]["SalManCode"]));
                string lstDealApplyNo = data.Rows[i]["DealApplyNo"].ToString();




                var PartyModel = new PartyModel()
                {
                    NorBalance = NorBalance,
                    PartyName = PartyName,
                    ContactPerson = ContactPerson,
                    MobileNo = MobileNo,
                    FaxNo = FaxNo,
                    LandLineNo = LandLineNo,
                    Address = Address,
                    Email = Email,
                    lstSellingPriceNo = lstSellingPriceNo,
                    lstDealApplyNo = lstDealApplyNo,
                    CreditLimit = CreditLimit,
                    CreditLimitApply = chkCreditLimitApply,
                    OtherInfo = OtherInfo,
                    lstSaleMan = lstSaleMan,
                    chkDeal = DealRs,
                    chkPerDiscount = DisPer

                };



                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(PartyModel));


            }
            catch (Exception)
            {

                throw;
            }

            finally
            {
                con.Close();
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GeItemDataAgainstItemCode(string ItemCode)
        {

            try
            {
                ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "Select Description,SellingCost as SellingCost1,SellingCost2,SellingCost3,SellingCost4,SellingCost5,FixedCost,RegNo,Manufacturer,ReorderLevel,ReorderQty,pkg,Brand,Reference,Reference2,Visiable,PiecesInPacking,Packing,Category,Class,Godown,OrderQTY,BonusQTY,Nature,AccountUnit,GST_Rate,GST_Apply,Active,Revenue_Code,CGS_Code,Ave_Cost,MinLimit,MaxLimit,Color,DealRs,ItemType,Length,Height,BarCodeCategory,DealRs2,OtherInFo,DealRs3,Year1,GroupId,MSP,PerPieceCommission from InvCode where Code=" + ItemCode;
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];
                int i = 0;
                string Description = data.Rows[i]["Description"].ToString().Trim();
                string SellingCost1 = data.Rows[i]["SellingCost1"].ToString();
                string Reference = data.Rows[i]["Reference"].ToString();
                string Reference2 = data.Rows[i]["Reference2"].ToString();
                string MinLimit = data.Rows[i]["MinLimit"].ToString();
                string MaxLimit = data.Rows[i]["MaxLimit"].ToString();
                string PiecesInPacking = data.Rows[i]["PiecesInPacking"].ToString();
                string ReorderLevel = data.Rows[i]["ReorderLevel"].ToString();
                string ReorderQty = data.Rows[i]["ReorderQty"].ToString();
                string OtherInFo = data.Rows[i]["OtherInFo"].ToString();
                string OrderQTY = data.Rows[i]["OrderQTY"].ToString();
                string BonusQTY = data.Rows[i]["BonusQTY"].ToString();
                string GST_Rate = data.Rows[i]["GST_Rate"].ToString();
                string BarCodeCategory = data.Rows[i]["BarCodeCategory"].ToString();
                string AccountUnit = data.Rows[i]["AccountUnit"].ToString();
                string Brand = data.Rows[i]["Brand"].ToString();
                string ItemType = data.Rows[i]["ItemType"].ToString();
                string Manufacturer = data.Rows[i]["Manufacturer"].ToString();
                string Packing = data.Rows[i]["Packing"].ToString();
                string Category = data.Rows[i]["Category"].ToString();
                string Class = data.Rows[i]["Class"].ToString();
                string Godown = data.Rows[i]["Godown"].ToString();
                string Nature = data.Rows[i]["Nature"].ToString();
                string Revenue_Code = data.Rows[i]["Revenue_Code"].ToString();
                string Color = data.Rows[i]["Color"].ToString();
                string Height = data.Rows[i]["Height"].ToString();
                string Active = data.Rows[i]["Active"].ToString();
                string Visiable = data.Rows[i]["Visiable"].ToString();
                string PerPieceCommission = data.Rows[i]["PerPieceCommission"].ToString();
                string Year1 = data.Rows[i]["Year1"].ToString();
                string GST_Apply = data.Rows[i]["GST_Apply"].ToString();
                string Length = data.Rows[i]["Length"].ToString();
                string Group = data.Rows[i]["GroupId"].ToString();
                string MSP = data.Rows[i]["MSP"].ToString();




                //$("#lstSaleMan [value=" + 2 + " ]").attr("selected", "selected");

                var ItemModel = new ItemModel()
                {
                    Description = Description,
                    SellingCost1 = SellingCost1,
                    Reference = Reference,
                    Reference2 = Reference2,
                    MinLimit = MinLimit,
                    MaxLimit = MaxLimit,
                    PiecesInPacking = PiecesInPacking,
                    ReorderLevel = ReorderLevel,
                    ReorderQty = ReorderQty,
                    OtherInFo = OtherInFo,
                    OrderQTY = OrderQTY,
                    BonusQTY = BonusQTY,
                    GST_Rate = GST_Rate,
                    BarCodeCategory = BarCodeCategory,
                    AccountUnit = AccountUnit,
                    Brand = Brand,
                    ItemType = ItemType,
                    Manufacturer = Manufacturer,
                    Packing = Packing,
                    Category = Category,
                    Class = Class,
                    Godown = Godown,
                    Nature = Nature,
                    Revenue_Code = Revenue_Code,
                    Color = Color,
                    Height = Height,
                    Active = Active,
                    Visiable = Visiable,
                    GST_Apply = GST_Apply,
                    Length = Length,
                    Group = Group,
                    MSP = MSP,
                    Year1 = Year1,
                    PerPieceCommission = PerPieceCommission

                };
                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ItemModel));
            }
            catch (Exception)
            {

                throw;
            }

            finally
            {
                con.Close();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void HeightList()
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;

            cmd.CommandText = "Select id,Height as Name from Height where Compid='01'";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];


            var list_Height = new List<Brand>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string Name = data.Rows[i]["Name"].ToString();
                string id = data.Rows[i]["id"].ToString();
                //Name = Name + "*" + id;
                var Height = new Brand()
                {
                    Code = id,
                    Name = Name

                };

                list_Height.Add(Height);
            }

            var itemComboList = new ItemComboList()
            {
                Height = list_Height,
            };

            JavaScriptSerializer js = new JavaScriptSerializer();

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            Context.Response.Write(js.Serialize(itemComboList));
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void WidthList()
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;

            cmd.CommandText = "Select id,Length as Name from Length where Compid='01'";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];


            var list_Width = new List<Brand>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string Name = data.Rows[i]["Name"].ToString();
                string id = data.Rows[i]["id"].ToString();
                Name = Name + "*" + id;
                var Width = new Brand()
                {
                    Name = Name,

                };

                list_Width.Add(Width);
            }

            var itemComboList = new ItemComboList()
            {
                Width = list_Width,
            };

            JavaScriptSerializer js = new JavaScriptSerializer();

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            Context.Response.Write(js.Serialize(itemComboList));
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void AccountUnitList()
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;

            cmd.CommandText = "Select code,Name from AccountUnit where Compid='01'";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];


            var list_Unit = new List<Brand>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string Name = data.Rows[i]["Name"].ToString();
                string code = data.Rows[i]["code"].ToString();

                Name = Name + "-" + code;

                var Unit = new Brand()
                {
                    Name = Name,

                };

                list_Unit.Add(Unit);
            }

            var itemComboList = new ItemComboList()
            {
                Unit = list_Unit,
            };

            JavaScriptSerializer js = new JavaScriptSerializer();

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            Context.Response.Write(js.Serialize(itemComboList));
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void ItemTypeList()
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;

            cmd.CommandText = "Select Code,Name from ItemType where  CompID='" + CompID + "' order by Code";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];


            var list_ItemType = new List<Brand>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string Name = data.Rows[i]["Name"].ToString();
                string Code = data.Rows[i]["Code"].ToString();
                Name = Name + "*" + Code;
                var ItemType = new Brand()
                {
                    Name = Name,

                };

                list_ItemType.Add(ItemType);
            }

            var itemComboList = new ItemComboList()
            {
                ItemType = list_ItemType,
            };

            JavaScriptSerializer js = new JavaScriptSerializer();

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            Context.Response.Write(js.Serialize(itemComboList));
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void COGSList()
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;

            cmd.CommandText = "select Code,Title from GLCode where CompID = '" + CompID + "' and Code like '" + CompID + "010106" + "%' and lvl = 5 ";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];


            var list_COGS = new List<Brand>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string Code = data.Rows[i]["Code"].ToString();
                string Name = data.Rows[i]["Title"].ToString();
                Name = Name + "*" + Code;
                var COGS = new Brand()
                {
                    Name = Name,

                };

                list_COGS.Add(COGS);
            }

            var itemComboList = new ItemComboList()
            {
                COGSAccount = list_COGS,
            };

            JavaScriptSerializer js = new JavaScriptSerializer();

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            Context.Response.Write(js.Serialize(itemComboList));
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void InComeList()
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;

            cmd.CommandText = "Select Code,Title as Name,Code from GLCode where  CompID='" + CompID + "' and Code like '" + CompID + "03%' and lvl=5 ";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];


            var list_InComeAccount = new List<Brand>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string Code = data.Rows[i]["Code"].ToString();
                string Name = data.Rows[i]["Name"].ToString();
                Name = Name + "-" + Code;
                var InComeAccount = new Brand()
                {
                    Name = Name,

                };

                list_InComeAccount.Add(InComeAccount);
            }

            var itemComboList = new ItemComboList()
            {
                InComeAccount = list_InComeAccount,
            };

            JavaScriptSerializer js = new JavaScriptSerializer();

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            Context.Response.Write(js.Serialize(itemComboList));
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void ExpenceList()
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;

            cmd.CommandText = "Select Code,Title as Name,Code from GLCode where  CompID='" + CompID + "' and Code like '" + CompID + "0402%' and lvl=5 ";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];


            var list_InComeAccount = new List<Brand>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string Code = data.Rows[i]["Code"].ToString();
                string Name = data.Rows[i]["Name"].ToString();
                Name = Name + "-" + Code;
                var InComeAccount = new Brand()
                {
                    Name = Name,

                };

                list_InComeAccount.Add(InComeAccount);
            }

            var itemComboList = new ItemComboList()
            {
                InComeAccount = list_InComeAccount,
            };

            JavaScriptSerializer js = new JavaScriptSerializer();

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            Context.Response.Write(js.Serialize(itemComboList));
        }



        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void ItemNatureList()
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;

            cmd.CommandText = "select Code,ItemNature from ItemNature where CompID='" + CompID + "'  order by ItemNature";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];


            var list_ItemNature = new List<Brand>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string Code = data.Rows[i]["Code"].ToString();
                string Name = data.Rows[i]["ItemNature"].ToString();
                Name = Name + "-" + Code;
                var ItemNature = new Brand()
                {
                    Name = Name,

                };

                list_ItemNature.Add(ItemNature);
            }

            var itemComboList = new ItemComboList()
            {
                ItemNature = list_ItemNature,
            };

            JavaScriptSerializer js = new JavaScriptSerializer();

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            Context.Response.Write(js.Serialize(itemComboList));
        }



        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void ItemData_AgainstItemCode(string ItemCode)
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "Select Description,isnull(SellingCost,0) as SellingCost1 , isnull(SellingCost2,0) as SellingCost2 , isnull(SellingCost3,0) as SellingCost3, isnull(SellingCost4,0) as SellingCost4, isnull(SellingCost5,0) as SellingCost5, isnull(DealRs,0) as DealRs1, isnull(DealRs2,0) as DealRs2 ,isnull(DealRs3,0) as DealRs3 , isnull(Ave_Cost,0) as Ave_Cost, isnull(MinLimit,0) as MinLimit,isnull(MaxLimit,0) as MaxLimit ,isnull(FixedCost,0) as FixedCost  from InvCode where CompID='01' and Code=" + ItemCode;
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];


            var itemModel = new ItemModel();
            var i = 0;
            string Description = data.Rows[i]["Description"].ToString().Trim();
            string SellingCost1 = data.Rows[i]["SellingCost1"].ToString();
            string SellingCost2 = data.Rows[i]["SellingCost2"].ToString();
            string SellingCost3 = data.Rows[i]["SellingCost3"].ToString();
            string SellingCost4 = data.Rows[i]["SellingCost4"].ToString();
            string SellingCost5 = data.Rows[i]["SellingCost5"].ToString();
            string DealRs1 = data.Rows[i]["DealRs1"].ToString();
            string DealRs2 = data.Rows[i]["DealRs2"].ToString();
            string DealRs3 = data.Rows[i]["DealRs3"].ToString();
            string Ave_Cost = data.Rows[i]["Ave_Cost"].ToString();
            string MinLimit = data.Rows[i]["MinLimit"].ToString();
            string MaxLimit = data.Rows[i]["MaxLimit"].ToString();
            string FixedCost = data.Rows[i]["FixedCost"].ToString();

            itemModel.Description = Description;
            itemModel.SellingCost1 = SellingCost1;
            itemModel.SellingCost2 = SellingCost2;
            itemModel.SellingCost3 = SellingCost3;
            itemModel.SellingCost4 = SellingCost4;
            itemModel.SellingCost5 = SellingCost5;
            itemModel.DealRs1 = DealRs1;
            itemModel.DealRs2 = DealRs2;
            itemModel.DealRs3 = DealRs3;
            itemModel.Ave_Cost = Ave_Cost;
            itemModel.MinLimit = MinLimit;
            itemModel.MaxLimit = MaxLimit;
            itemModel.FixedCost = FixedCost;

            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(js.Serialize(itemModel));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void ListItemsByBrandId(string brandId)
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "Select Code,Description,isnull(SellingCost,0) as SellingCost1 , isnull(SellingCost2,0) as SellingCost2 , isnull(SellingCost3,0) as SellingCost3, isnull(SellingCost4,0) as SellingCost4, isnull(SellingCost5,0) as SellingCost5, isnull(DealRs,0) as DealRs1, isnull(DealRs2,0) as DealRs2 ,isnull(DealRs3,0) as DealRs3 , isnull(Ave_Cost,0) as Ave_Cost, isnull(MinLimit,0) as MinLimit,isnull(MaxLimit,0) as MaxLimit ,isnull(FixedCost,0) as FixedCost  from InvCode where CompID='" + CompID + "' and Brand=" + brandId;
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];


            var itemModelList = new List<ItemModel>();


            var modItem = new ModItem();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                var itemModel = new ItemModel();
                string Code = data.Rows[i]["Code"].ToString();
                string Description = data.Rows[i]["Description"].ToString().Trim();
                string SellingCost1 = data.Rows[i]["SellingCost1"].ToString();
                string SellingCost2 = data.Rows[i]["SellingCost2"].ToString();
                string SellingCost3 = data.Rows[i]["SellingCost3"].ToString();
                string SellingCost4 = data.Rows[i]["SellingCost4"].ToString();
                string SellingCost5 = data.Rows[i]["SellingCost5"].ToString();
                string DealRs1 = data.Rows[i]["DealRs1"].ToString();
                string DealRs2 = data.Rows[i]["DealRs2"].ToString();
                string DealRs3 = data.Rows[i]["DealRs3"].ToString();
                string Ave_Cost = data.Rows[i]["Ave_Cost"].ToString();
                string MinLimit = data.Rows[i]["MinLimit"].ToString();
                string MaxLimit = data.Rows[i]["MaxLimit"].ToString();
                string FixedCost = data.Rows[i]["FixedCost"].ToString();
                var LastPurchase = modItem.LastPurchasePrice(Code);

                itemModel.Code = Code;
                itemModel.Description = Description;
                itemModel.SellingCost1 = SellingCost1;
                itemModel.SellingCost2 = SellingCost2;
                itemModel.SellingCost3 = SellingCost3;
                itemModel.SellingCost4 = SellingCost4;
                itemModel.SellingCost5 = SellingCost5;
                itemModel.DealRs1 = DealRs1;
                itemModel.DealRs2 = DealRs2;
                itemModel.DealRs3 = DealRs3;
                itemModel.Ave_Cost = Ave_Cost;
                itemModel.MinLimit = MinLimit;
                itemModel.MaxLimit = MaxLimit;
                itemModel.FixedCost = FixedCost;
                itemModel.LastPurchase = LastPurchase.ToString();


                itemModelList.Add(itemModel);

            }

            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(js.Serialize(itemModelList));
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Level5_List(string GLCode)
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            // GLCode = "01040201";
            cmd.CommandText = "Select Code,Title from GLCode where Code Like '" + GLCode + "%' and  Compid='01' and lvl=5";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];
            var listBrand = new List<Brand>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string Code = data.Rows[i]["Code"].ToString();
                string Title = data.Rows[i]["Title"].ToString();
                var brand = new Brand()
                {
                    Code = Code,
                    Name = Title,
                };

                listBrand.Add(brand);
            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(js.Serialize(listBrand.OrderBy(a => a.Name)));
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Level5_ListBank(string GLCode)
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            // GLCode = "01040201";
            cmd.CommandText = "Select BankCode,BankName,AccountNo,Title,Address,Phone,Fax from BankCode where Compid='01'";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];
            var listBank = new List<BankModel>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string BankCode = data.Rows[i]["BankCode"].ToString();
                string BankName = data.Rows[i]["BankName"].ToString();
                string AccountNo = data.Rows[i]["AccountNo"].ToString();
                string Title = data.Rows[i]["Title"].ToString();
                string Address = data.Rows[i]["Address"].ToString();
                string Phone = data.Rows[i]["Phone"].ToString();
                string Fax = data.Rows[i]["Fax"].ToString();

                var Bank = new BankModel()
                {
                    BankCode = BankCode,
                    BankName = BankName,
                    AccountNo = AccountNo,
                    Title = Title,
                    Address = Address,
                    Phone = Phone,
                    Fax = Fax

                };

                listBank.Add(Bank);
            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(js.Serialize(listBank.OrderBy(a => a.BankName)));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Level4_List()
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;

            cmd.CommandText = "Select Code,Title from GLCode where Code Like '010402%' and  Compid='01' and lvl=4";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];
            var listBrand = new List<Brand>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string Code = data.Rows[i]["Code"].ToString();
                string Title = data.Rows[i]["Title"].ToString();
                var brand = new Brand()
                {
                    Code = Code,
                    Name = Title,
                };

                listBrand.Add(brand);
            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(js.Serialize(listBrand.OrderBy(a => a.Name)));
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Category_List()
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            //cmd.Parameters.AddWithValue("@Description", item);
            cmd.CommandText = "Select ID,Name from Category where Compid='01'";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];
            var listBrand = new List<Brand>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string BrandCode = data.Rows[i]["ID"].ToString();
                string BrandName = data.Rows[i]["Name"].ToString();
                var brand = new Brand()
                {
                    Code = BrandCode,
                    Name = BrandName,
                };

                listBrand.Add(brand);
            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(js.Serialize(listBrand.OrderBy(a => a.Name)));
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetCashPaymentVoucherData(string VoucherNo)
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            Module8 objModule8 = new Module8();
            ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
            cmd.CommandText = "Select dateDr,AmountCr,VenderCode,Narration from GeneralLedger where  CompID='" + CompID + "' and SecondDescription='CashPaidToParty' and V_Type='CPV' and V_No=" + VoucherNo + " and Code='" + "0101010100001" + "'";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();


            ModelPaymentVoucher ModelPaymentVoucher;
            var objModule1 = new Module1();
            var lastVoucherNo = (objModule1.MaxCPV() - 1);
            var isMaxBRVVoucherNo = lastVoucherNo == Convert.ToDecimal(VoucherNo) || lastVoucherNo < Convert.ToDecimal(VoucherNo);


            if (objDs.Tables[0].Rows.Count > 0)
            {
                var data = objDs.Tables[0];
                int i = 0;

                string Date = Convert.ToDateTime(data.Rows[i]["dateDr"].ToString()).ToString("dd/MM/yyyy");
                string CashPaid = data.Rows[i]["AmountCr"].ToString();
                string PartyCode = data.Rows[i]["VenderCode"].ToString();
                string PartyName = objModule8.PartyNameAgainstCode(PartyCode);
                string Address = objModPartyCodeAgainstName.PartyAddress(PartyName);
                string Narration = data.Rows[i]["Narration"].ToString();
                string Balance = Convert.ToString(objModPartyCodeAgainstName.PartyBalance(PartyCode));

                ModelPaymentVoucher = new ModelPaymentVoucher()
                {
                    Date = Date,
                    CashPaid = CashPaid,
                    PartyCode = PartyCode,
                    PartyName = PartyName,
                    Address = Address,
                    Narration = Narration,
                    Balance = Balance,
                    Success = true,
                    IsLastVoucherNo = isMaxBRVVoucherNo
                };
            }
            else
            {
                ModelPaymentVoucher = new ModelPaymentVoucher()
                {
                    IsLastVoucherNo = isMaxBRVVoucherNo,
                    Success = false,
                    Message = "No Voucher found.."
                };
            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(js.Serialize(ModelPaymentVoucher));
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetCashReceiptVoucherData(string VoucherNo)
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            Module8 objModule8 = new Module8();
            ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
            //cmd.CommandText = "Select dateDr,AmountCr,VenderCode,Narration from GeneralLedger where  CompID='" + CompID + "' and SecondDescription='CashPaidToParty' and V_Type='CPV' and V_No=" + VoucherNo + " and Code='" + "0101010100001" + "'";
            cmd.CommandText = "Select dateDr,AmountDr,VenderCode,Narration from GeneralLedger where  CompID='" + CompID + "' and V_Type='CRV' and V_No=" + VoucherNo + " and Code='" + "0101010100001" + "' and SecondDescription='FromParties';";
            //cmd.CommandText = "Select dateDr,AmountDr,VenderCode,Narration from GeneralLedger where  CompID='" + CompID + "' and V_Type='CRV' and V_No=" + VoucherNo + " and Code='" + "0101010100001" + "' and SecondDescription='FromParties';";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();


            ModelPaymentVoucher ModelPaymentVoucher;

            var objModule1 = new Module1();
            var lastVoucherNo = (objModule1.MaxCashReceiptVoucher() - 1);
            var isMaxBRVVoucherNo = lastVoucherNo == Convert.ToDecimal(VoucherNo) || lastVoucherNo < Convert.ToDecimal(VoucherNo);

            if (objDs.Tables[0].Rows.Count > 0)
            {
                var data = objDs.Tables[0];
                int i = 0;

                string Date = Convert.ToDateTime(data.Rows[i]["dateDr"].ToString()).ToString("dd/MM/yyyy");
                string CashPaid = data.Rows[i]["AmountDr"].ToString();
                string PartyCode = data.Rows[i]["VenderCode"].ToString();
                string PartyName = objModule8.PartyNameAgainstCode(PartyCode);
                string Address = objModPartyCodeAgainstName.PartyAddress(PartyName);
                string Narration = data.Rows[i]["Narration"].ToString();
                string Balance = Convert.ToString(objModPartyCodeAgainstName.PartyBalance(PartyCode));

                ModelPaymentVoucher = new ModelPaymentVoucher()
                {
                    Date = Date,
                    CashPaid = CashPaid,
                    PartyCode = PartyCode,
                    PartyName = PartyName,
                    Address = Address,
                    Narration = Narration,
                    Balance = Balance,
                    Success = true,
                    IsLastVoucherNo = isMaxBRVVoucherNo
                };
            }
            else
            {
                ModelPaymentVoucher = new ModelPaymentVoucher()
                {
                    IsLastVoucherNo = isMaxBRVVoucherNo,
                    Success = false,
                    Message = "No Voucher found.."
                };
            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(js.Serialize(ModelPaymentVoucher));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetBankReceiptVoucherData(string VoucherNo)
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            Module8 objModule8 = new Module8();
            ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
            //cmd.CommandText = "Select dateDr,AmountCr,VenderCode,Narration from GeneralLedger where  CompID='" + CompID + "' and SecondDescription='CashPaidToParty' and V_Type='CPV' and V_No=" + VoucherNo + " and Code='" + "0101010100001" + "'";
            cmd.CommandText = @"SELECT GeneralLedger.dateDr, GeneralLedger.AmountDr, GeneralLedger.V_No, GeneralLedger.Description, GeneralLedger.VenderCode, GeneralLedger.Narration,
                BankCode.BankCode, 
                BankCode.BankName, 
                BankCode.AccountNo, 
                GeneralLedger.BankCode,GeneralLedger.ChequeNo 
                FROM GeneralLedger
                Join BankCode On GeneralLedger.BankCode = BankCode.BankCode
                WHERE AmountDr > 0 and V_Type = 'BRV' AND SecondDescription = 'FromPartiesThroughBank' and V_No = " + VoucherNo;

            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            ModelPaymentVoucher ModelPaymentVoucher;

            var objModule1 = new Module1();
            var lastVoucherNo = (objModule1.MaxBRV() - 1);
            var isMaxBRVVoucherNo = lastVoucherNo == Convert.ToDecimal(VoucherNo) || lastVoucherNo < Convert.ToDecimal(VoucherNo);

            if (objDs.Tables[0].Rows.Count > 0)
            {
                var data = objDs.Tables[0];
                int i = 0;

                string Date = Convert.ToDateTime(data.Rows[i]["dateDr"].ToString()).ToString("dd/MM/yyyy");
                string CashPaid = data.Rows[i]["AmountDr"].ToString();
                string PartyCode = data.Rows[i]["VenderCode"].ToString();
                string PartyName = objModule8.PartyNameAgainstCode(PartyCode);
                string Address = objModPartyCodeAgainstName.PartyAddress(PartyName);
                string Narration = data.Rows[i]["Narration"].ToString();
                string BankName = data.Rows[i]["BankName"].ToString();
                string BankCode = data.Rows[i]["BankCode"].ToString();
                string AccountNo = data.Rows[i]["AccountNo"].ToString();
                string ChqNo = data.Rows[i]["ChequeNo"].ToString();
                string Balance = Convert.ToString(objModPartyCodeAgainstName.PartyBalance(PartyCode));

                ModelPaymentVoucher = new ModelPaymentVoucher()
                {
                    Date = Date,
                    CashPaid = CashPaid,
                    PartyCode = PartyCode,
                    PartyName = PartyName,
                    Address = Address,
                    Narration = Narration,
                    Balance = Balance,
                    BankName = BankName,
                    BankCode = BankCode,
                    ChqNo = ChqNo,
                    AccountNo = AccountNo,
                    Success = true,
                    IsLastVoucherNo = isMaxBRVVoucherNo
                };
            }
            else
            {
                ModelPaymentVoucher = new ModelPaymentVoucher()
                {
                    IsLastVoucherNo = isMaxBRVVoucherNo,
                    Success = false,
                    Message = "No Voucher found.."
                };
            }


            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(js.Serialize(ModelPaymentVoucher));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetCashDepositInToBankVoucherData(string VoucherNo)
        {
            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                Module8 objModule8 = new Module8();
                ModGLCode objModGLCode = new ModGLCode();

                //cmd.CommandText = "Select dateDr,AmountDr,VenderCode,Narration from GeneralLedger where  CompID='" + CompID + "' and V_Type='CRV' and V_No=" + VoucherNo + " and Code='" + "0101010100001" + "' and SecondDescription='FromParties'";
                cmd.CommandText = @"SELECT dateDr, AmountDr, AmountCr, V_Type, V_No, Description, VenderCode, Narration, 
                     BankCode.BankCode, 
                BankCode.BankName, 
                BankCode.AccountNo, 
                GeneralLedger.BankCode
                FROM GeneralLedger
                Join BankCode On GeneralLedger.BankCode = BankCode.BankCode
                    WHERE AmountDr>0 and V_Type = 'brv' AND SecondDescription = 'CashDepositInToBank' and V_No = " + VoucherNo;
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();

                ModelPaymentVoucher ModelPaymentVoucher;
                var objModule1 = new Module1();
                var lastVoucherNo = (objModule1.MaxCashDepositInToBankVoucher() - 1);
                var isMaxBRVVoucherNo = lastVoucherNo == Convert.ToDecimal(VoucherNo) || lastVoucherNo < Convert.ToDecimal(VoucherNo);

                if (objDs.Tables[0].Rows.Count > 0)
                {
                    var data = objDs.Tables[0];
                    int i = 0;

                    string Date = Convert.ToDateTime(data.Rows[i]["dateDr"].ToString()).ToString("dd/MM/yyyy");
                    string CashPaid = data.Rows[i]["AmountDr"].ToString();
                    string BankName = data.Rows[i]["BankName"].ToString();
                    string BankCode = data.Rows[i]["BankCode"].ToString();
                    string AccountNo = data.Rows[i]["AccountNo"].ToString();
                    string Narration = data.Rows[i]["Narration"].ToString();


                    ModelPaymentVoucher = new ModelPaymentVoucher()
                    {
                        Date = Date,
                        CashPaid = CashPaid,
                        PartyCode = BankCode,
                        PartyName = BankName,
                        AccountNo = AccountNo,
                        Narration = Narration,

                        Success = true,
                        IsLastVoucherNo = isMaxBRVVoucherNo
                    };
                }
                else
                {
                    ModelPaymentVoucher = new ModelPaymentVoucher()
                    {
                        IsLastVoucherNo = isMaxBRVVoucherNo,
                        Success = false,
                        Message = "No Voucher found.."
                    };
                }
                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));
            }
            catch (Exception ex)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var ModelPaymentVoucher = new ModelPaymentVoucher()
                {
                    Success = false,
                    Message = ex.Message
                };
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetPaymentToPartyBankVoucherData(string VoucherNo)
        {
            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                Module8 objModule8 = new Module8();
                ModGLCode objModGLCode = new ModGLCode();
                ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
                //cmd.CommandText = "Select dateDr,AmountDr,VenderCode,Narration from GeneralLedger where  CompID='" + CompID + "' and V_Type='CRV' and V_No=" + VoucherNo + " and Code='" + "0101010100001" + "' and SecondDescription='FromParties'";
                cmd.CommandText = @"SELECT dateDr, AmountDr, AmountCr, V_Type, V_No, Description, VenderCode, Narration, 
                            BankCode.BankCode, 
                            BankCode.BankName, 
                            BankCode.AccountNo, 
                            PartyCode.Address,
                            PartyCode.Name,
                            GeneralLedger.BankCode,GeneralLedger.ChequeNo
                            FROM GeneralLedger
                            Join BankCode On GeneralLedger.BankCode = BankCode.BankCode
                            Join PartyCode On GeneralLedger.VenderCode = PartyCode.Code
                            WHERE AmountDr>0 and V_Type ='BPV' AND SecondDescription = 'CashPaidToPartyThroughBank' and V_No =" + VoucherNo;
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();

                ModelPaymentVoucher ModelPaymentVoucher;
                var objModule1 = new Module1();
                var lastVoucherNo = (objModule1.MaxBPV() - 1);
                var isMaxBRVVoucherNo = lastVoucherNo == Convert.ToDecimal(VoucherNo) || lastVoucherNo < Convert.ToDecimal(VoucherNo);

                if (objDs.Tables[0].Rows.Count > 0)
                {
                    var data = objDs.Tables[0];
                    int i = 0;

                    string Date = Convert.ToDateTime(data.Rows[i]["dateDr"].ToString()).ToString("dd/MM/yyyy");
                    string CashPaid = data.Rows[i]["AmountDr"].ToString();
                    //string BankCode = data.Rows[i]["BankCode"].ToString();
                    //string BankName = objModGLCode.GLTitleAgainstCode(BankCode);
                    string Narration = data.Rows[i]["Narration"].ToString();
                    string PartyCode = data.Rows[i]["VenderCode"].ToString();
                    string PartyName = data.Rows[i]["Name"].ToString();
                    string PartyAddress = data.Rows[i]["Address"].ToString();
                    string ChequeNo = data.Rows[i]["ChequeNo"].ToString();
                    string BankName = data.Rows[i]["BankName"].ToString();
                    string BankCode = data.Rows[i]["BankCode"].ToString();
                    string AccountNo = data.Rows[i]["AccountNo"].ToString();

                    ModelPaymentVoucher = new ModelPaymentVoucher()
                    {
                        Date = Date,
                        BankCode = BankCode,
                        BankName = BankName,
                        ChqNo = ChequeNo,
                        CashPaid = CashPaid,
                        PartyCode = PartyCode,
                        PartyName = PartyName,

                        Address = PartyAddress,
                        Narration = Narration,
                        AccountNo = AccountNo,
                        Success = true,
                        IsLastVoucherNo = isMaxBRVVoucherNo
                    };
                }
                else
                {
                    ModelPaymentVoucher = new ModelPaymentVoucher()
                    {
                        IsLastVoucherNo = isMaxBRVVoucherNo,
                        Success = false,
                        Message = "No Voucher found.."
                    };
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));
            }
            catch (Exception ex)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var ModelPaymentVoucher = new ModelPaymentVoucher()
                {
                    Success = false,
                    Message = ex.Message
                };
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));

            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetIncentiveReceivingVoucherData(string VoucherNo)
        {

            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                Module8 objModule8 = new Module8();
                ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
                string IncentiveIncomeGLCode = "0103050100001";
                //cmd.CommandText = "Select dateDr,AmountDr,VenderCode,Narration from GeneralLedger where  CompID='" + CompID + "' and V_Type='CRV' and V_No=" + VoucherNo + " and Code='" + "0101010100001" + "' and SecondDescription='FromParties'";
                cmd.CommandText = "Select dateDr,AmountCr,VenderCode,Narration from GeneralLedger where  CompID='" + CompID + "' and Code='" + IncentiveIncomeGLCode + "' and V_Type = 'JV' and DescriptionOfBillNo='IncentiveIncome' and V_No=" + VoucherNo;
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();

                ModelPaymentVoucher ModelPaymentVoucher;
                var objModule1 = new Module1();
                var lastVoucherNo = (objModule1.MaxJVNo() - 1);
                var isMaxBRVVoucherNo = lastVoucherNo == Convert.ToDecimal(VoucherNo) || lastVoucherNo < Convert.ToDecimal(VoucherNo);
                if (objDs.Tables[0].Rows.Count > 0)
                {
                    var data = objDs.Tables[0];
                    int i = 0;

                    string Date = Convert.ToDateTime(data.Rows[i]["dateDr"].ToString()).ToString("dd/MM/yyyy");
                    string CashPaid = data.Rows[i]["AmountCr"].ToString();
                    string PartyCode = data.Rows[i]["VenderCode"].ToString();
                    string PartyName = objModPartyCodeAgainstName.PartyNameAgainstCode(PartyCode);
                    string Address = objModPartyCodeAgainstName.PartyAddress(PartyName);
                    string Balance = Convert.ToString(objModPartyCodeAgainstName.PartyBalance(PartyCode));
                    string Narration = data.Rows[i]["Narration"].ToString();

                    ModelPaymentVoucher = new ModelPaymentVoucher()
                    {
                        Date = Date,
                        CashPaid = CashPaid,
                        PartyCode = PartyCode,
                        PartyName = PartyName,
                        Narration = Narration,
                        Address = Address,
                        Balance = Balance,
                        Success = true,
                        IsLastVoucherNo = isMaxBRVVoucherNo
                    };
                }
                else
                {
                    ModelPaymentVoucher = new ModelPaymentVoucher()
                    {
                        IsLastVoucherNo = isMaxBRVVoucherNo,
                        Success = false,
                        Message = "No Voucher found.."
                    };
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));
            }
            catch (Exception ex)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var ModelPaymentVoucher = new ModelPaymentVoucher()
                {
                    Success = false,
                    Message = ex.Message
                };
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));

            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetIncentiveReceivingVoucherDataCash(string VoucherNo)
        {

            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                Module8 objModule8 = new Module8();
                ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
                //cmd.CommandText = "Select dateDr,AmountDr,VenderCode,Narration from GeneralLedger where  CompID='" + CompID + "' and V_Type='CRV' and V_No=" + VoucherNo + " and Code='" + "0101010100001" + "' and SecondDescription='FromParties'";
                cmd.CommandText = "Select Date1,PartyCode,Name,Amount,Narration from CashIncentive where InvNo=" + VoucherNo + " and Nature='CashIncentiveReceived'";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();

                ModelPaymentVoucher ModelPaymentVoucher;
                var objModule1 = new Module1();
                var lastVoucherNo = (objModule1.MaxCIRV() - 1);
                var isMaxBRVVoucherNo = lastVoucherNo == Convert.ToDecimal(VoucherNo) || lastVoucherNo < Convert.ToDecimal(VoucherNo);
                if (objDs.Tables[0].Rows.Count > 0)
                {
                    var data = objDs.Tables[0];
                    int i = 0;

                    string Date = Convert.ToDateTime(data.Rows[i]["Date1"].ToString()).ToString("dd/MM/yyyy");
                    string CashPaid = data.Rows[i]["Amount"].ToString();
                    string PartyCode = data.Rows[i]["PartyCode"].ToString();
                    string PartyName = data.Rows[i]["Name"].ToString();
                    string Address = objModPartyCodeAgainstName.PartyAddress(PartyName);
                    string Balance = Convert.ToString(objModPartyCodeAgainstName.PartyBalance(PartyCode));
                    string Narration = data.Rows[i]["Narration"].ToString();

                    ModelPaymentVoucher = new ModelPaymentVoucher()
                    {
                        Date = Date,
                        CashPaid = CashPaid,
                        PartyCode = PartyCode,
                        PartyName = PartyName,
                        Narration = Narration,
                        Address = Address,
                        Balance = Balance,
                        Success = true,
                        IsLastVoucherNo = isMaxBRVVoucherNo
                    };
                }
                else
                {
                    ModelPaymentVoucher = new ModelPaymentVoucher()
                    {
                        IsLastVoucherNo = isMaxBRVVoucherNo,
                        Success = false,
                        Message = "No Voucher found.."
                    };
                }
                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));
            }
            catch (Exception ex)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var ModelPaymentVoucher = new ModelPaymentVoucher()
                {
                    Success = false,
                    Message = ex.Message
                };
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetIncentiveGivenVoucherDataCash(string VoucherNo)
        {

            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                Module8 objModule8 = new Module8();
                ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
                //cmd.CommandText = "Select dateDr,AmountDr,VenderCode,Narration from GeneralLedger where  CompID='" + CompID + "' and V_Type='CRV' and V_No=" + VoucherNo + " and Code='" + "0101010100001" + "' and SecondDescription='FromParties'";
                cmd.CommandText = "Select Date1,PartyCode,Name,Amount,Narration from CashIncentive where InvNo=" + VoucherNo + " and Nature='CashIncentiveGiven'";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();

                ModelPaymentVoucher ModelPaymentVoucher;
                var objModule1 = new Module1();
                var lastVoucherNo = (objModule1.MaxCIGV() - 1);
                var isMaxBRVVoucherNo = lastVoucherNo == Convert.ToDecimal(VoucherNo) || lastVoucherNo < Convert.ToDecimal(VoucherNo);
                if (objDs.Tables[0].Rows.Count > 0)
                {
                    var data = objDs.Tables[0];
                    int i = 0;

                    string Date = Convert.ToDateTime(data.Rows[i]["Date1"].ToString()).ToString("dd/MM/yyyy");
                    string CashPaid = data.Rows[i]["Amount"].ToString();
                    string PartyCode = data.Rows[i]["PartyCode"].ToString();
                    string PartyName = data.Rows[i]["Name"].ToString();
                    string Address = objModPartyCodeAgainstName.PartyAddress(PartyName);
                    string Balance = Convert.ToString(objModPartyCodeAgainstName.PartyBalance(PartyCode));
                    string Narration = data.Rows[i]["Narration"].ToString();

                    ModelPaymentVoucher = new ModelPaymentVoucher()
                    {
                        Date = Date,
                        CashPaid = CashPaid,
                        PartyCode = PartyCode,
                        PartyName = PartyName,
                        Narration = Narration,
                        Address = Address,
                        Balance = Balance,
                        Success = true,
                        IsLastVoucherNo = isMaxBRVVoucherNo
                    };
                }
                else
                {
                    ModelPaymentVoucher = new ModelPaymentVoucher()
                    {
                        IsLastVoucherNo = isMaxBRVVoucherNo,
                        Success = false,
                        Message = "No Voucher found.."
                    };
                }
                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));
            }
            catch (Exception ex)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var ModelPaymentVoucher = new ModelPaymentVoucher()
                {
                    Success = false,
                    Message = ex.Message
                };
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetIncentiveGivenVoucherData(string VoucherNo)
        {

            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                Module8 objModule8 = new Module8();
                ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
                //cmd.CommandText = "Select dateDr,AmountDr,VenderCode,Narration from GeneralLedger where  CompID='" + CompID + "' and V_Type='CRV' and V_No=" + VoucherNo + " and Code='" + "0101010100001" + "' and SecondDescription='FromParties'";
                cmd.CommandText = "Select dateDr,AmountDr,VenderCode,Narration from GeneralLedger where  CompID = '" + CompID + "' and V_Type = 'JV' and DescriptionOfBillNo = 'IncentiveLoss' and V_No = " + VoucherNo;
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();

                ModelPaymentVoucher ModelPaymentVoucher;
                var objModule1 = new Module1();
                var lastVoucherNo = (objModule1.MaxIncentiveGivenVoucher() - 1);
                var isMaxBRVVoucherNo = lastVoucherNo == Convert.ToDecimal(VoucherNo) || lastVoucherNo < Convert.ToDecimal(VoucherNo);
                if (objDs.Tables[0].Rows.Count > 0)
                {
                    var data = objDs.Tables[0];
                    int i = 0;

                    string Date = Convert.ToDateTime(data.Rows[i]["dateDr"].ToString()).ToString("dd/MM/yyyy");
                    string CashPaid = data.Rows[i]["AmountDr"].ToString();
                    string PartyCode = data.Rows[i]["VenderCode"].ToString();
                    string PartyName = objModPartyCodeAgainstName.PartyNameAgainstCode(PartyCode);
                    string Address = objModPartyCodeAgainstName.PartyAddress(PartyName);
                    string Balance = Convert.ToString(objModPartyCodeAgainstName.PartyBalance(PartyCode));
                    string Narration = data.Rows[i]["Narration"].ToString();

                    ModelPaymentVoucher = new ModelPaymentVoucher()
                    {
                        Date = Date,
                        CashPaid = CashPaid,
                        PartyCode = PartyCode,
                        PartyName = PartyName,
                        Narration = Narration,
                        Address = Address,
                        Balance = Balance,
                        Success = true,
                        IsLastVoucherNo = isMaxBRVVoucherNo
                    };
                }
                else
                {
                    ModelPaymentVoucher = new ModelPaymentVoucher()
                    {
                        IsLastVoucherNo = isMaxBRVVoucherNo,
                        Success = false,
                        Message = "No Voucher found.."
                    };
                }
                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));
            }
            catch (Exception ex)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var ModelPaymentVoucher = new ModelPaymentVoucher()
                {
                    Success = false,
                    Message = ex.Message
                };
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));

            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetCashWithDrawFromBankVoucherData(string VoucherNo)
        {
            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                Module8 objModule8 = new Module8();
                ModGLCode objModGLCode = new ModGLCode();

                //cmd.CommandText = "Select dateDr,AmountDr,VenderCode,Narration from GeneralLedger where  CompID='" + CompID + "' and V_Type='CRV' and V_No=" + VoucherNo + " and Code='" + "0101010100001" + "' and SecondDescription='FromParties'";
                cmd.CommandText = @"SELECT dateDr, AmountDr, AmountCr, V_Type, V_No, Description, VenderCode, Narration,
                   BankCode.BankCode, 
                BankCode.BankName, 
                BankCode.AccountNo, 
                GeneralLedger.BankCode
                FROM GeneralLedger
                Join BankCode On GeneralLedger.BankCode = BankCode.BankCode
                    WHERE AmountDr>0 and V_Type = 'CRV' AND SecondDescription = 'CashWithDrawForPettyCash' and V_No = " + VoucherNo;
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();


                ModelPaymentVoucher ModelPaymentVoucher;
                var objModule1 = new Module1();
                var lastVoucherNo = (objModule1.MaxCashWithDrawForPettyCash() - 1);
                var isMaxBRVVoucherNo = lastVoucherNo == Convert.ToDecimal(VoucherNo) || lastVoucherNo < Convert.ToDecimal(VoucherNo);

                if (objDs.Tables[0].Rows.Count > 0)
                {

                    var data = objDs.Tables[0];
                    int i = 0;

                    string Date = Convert.ToDateTime(data.Rows[i]["dateDr"].ToString()).ToString("dd/MM/yyyy");
                    string CashPaid = data.Rows[i]["AmountDr"].ToString();
                    string BankName = data.Rows[i]["BankName"].ToString();
                    string BankCode = data.Rows[i]["BankCode"].ToString();
                    string AccountNo = data.Rows[i]["AccountNo"].ToString();
                    string Narration = data.Rows[i]["Narration"].ToString();

                    ModelPaymentVoucher = new ModelPaymentVoucher()
                    {
                        Date = Date,
                        CashPaid = CashPaid,
                        PartyCode = BankCode,
                        PartyName = BankName,
                        Narration = Narration,
                        AccountNo = AccountNo,
                        Success = true,
                        IsLastVoucherNo = isMaxBRVVoucherNo
                    };
                }
                else
                {
                    ModelPaymentVoucher = new ModelPaymentVoucher()
                    {
                        IsLastVoucherNo = isMaxBRVVoucherNo,
                        Success = false,
                        Message = "No Voucher found.."
                    };
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));
            }
            catch (Exception ex)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var ModelPaymentVoucher = new ModelPaymentVoucher()
                {
                    Success = false,
                    Message = ex.Message
                };
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));

            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetCashExpenceVoucherData(string VoucherNo)
        {
            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                ModGLCode objModGLCode = new ModGLCode();
                ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();

                cmd.CommandText = "Select dateDr,AmountCr,Code1,Narration,StationId from GeneralLedger where CompID='" + CompID + "' and V_Type='CPV' and V_No=" + VoucherNo + " and Code='" + "0101010100001" + "' and SecondDescription='OperationalExpencesThroughCash'";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();

                ModelPaymentVoucher ModelPaymentVoucher;
                var objModule1 = new Module1();
                var lastVoucherNo = (objModule1.MaxCashExpenceVoucher() - 1);
                var isMaxBRVVoucherNo = lastVoucherNo == Convert.ToDecimal(VoucherNo) || lastVoucherNo < Convert.ToDecimal(VoucherNo);


                if (objDs.Tables[0].Rows.Count > 0)
                {
                    var data = objDs.Tables[0];
                    int i = 0;

                    string Date = Convert.ToDateTime(data.Rows[i]["dateDr"].ToString()).ToString("dd/MM/yyyy");
                    string CashPaid = data.Rows[i]["AmountCr"].ToString();
                    string GLCode = data.Rows[i]["Code1"].ToString();
                    string GLTitle = objModGLCode.GLTitleAgainstGLCode(GLCode);
                    string Narration = data.Rows[i]["Narration"].ToString();
                    string StationId = data.Rows[i]["StationId"].ToString();

                    ModelPaymentVoucher = new ModelPaymentVoucher()
                    {
                        Date = Date,
                        CashPaid = CashPaid,
                        PartyCode = GLCode,
                        PartyName = GLTitle,
                        Narration = Narration,
                        StationId = StationId,
                        Success = true,
                        Success1 = true,
                        IsLastVoucherNo = isMaxBRVVoucherNo
                    };
                }
                else
                {
                    ModelPaymentVoucher = new ModelPaymentVoucher()
                    {
                        IsLastVoucherNo = isMaxBRVVoucherNo,

                        Success = false,
                        Message = "No Voucher found.."
                    };
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));
            }
            catch (Exception ex)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var ModelPaymentVoucher = new ModelPaymentVoucher()
                {

                    Success1 = false,
                    Success = false,
                    Message = ex.Message

                };
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetExpenseDetails(string from, string to, string stationId)
        {
            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                ModGLCode objModGLCode = new ModGLCode();
                ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();

                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture).ToShortDateString();
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture).ToShortDateString();
                var stationWhereClause = string.Empty;
                if (stationId != "0")
                {
                    stationWhereClause = " and StationId=" + stationId;
                }
                cmd.CommandText = "Select V_No,dateDr,AmountCr,Code1,Narration,StationId,Station.Name as StationName,GLCode.Title from GeneralLedger join Station on Station.Id=GeneralLedger.StationId join GLCode on GeneralLedger.Code1=GLCode.Code where GeneralLedger.CompID='" + CompID + "' and V_Type='CPV'  and GeneralLedger.Code='" + "0101010100001" + "' and SecondDescription='OperationalExpencesThroughCash' and dateDr>='" + fromDate + "' and dateDr<='" + toDate + "' " + stationWhereClause + " order by dateDr desc";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();

                ModelPaymentVoucher ModelPaymentVoucher;


                var data = objDs.Tables[0];

                var ModelPaymentVouchers = new List<ModelPaymentVoucher>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string Date = Convert.ToDateTime(data.Rows[i]["dateDr"].ToString()).ToString("dd/MM/yyyy");
                    string CashPaid = data.Rows[i]["AmountCr"].ToString();
                    string V_No = data.Rows[i]["V_No"].ToString();
                    string Narration = data.Rows[i]["Narration"].ToString();
                    string StationId = data.Rows[i]["StationId"].ToString();
                    string StationName = data.Rows[i]["StationName"].ToString();
                    string Title = data.Rows[i]["Title"].ToString();

                    ModelPaymentVoucher = new ModelPaymentVoucher()
                    {
                        VoucherNo = V_No,
                        Date = Date,
                        CashPaid = CashPaid,
                        Narration = Narration,
                        StationId = StationId,
                        StationName = StationName,
                        ExpenseHead = Title,
                        Success = true,
                        Success1 = true,
                    };
                    ModelPaymentVouchers.Add(ModelPaymentVoucher);
                }


                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVouchers));
            }
            catch (Exception ex)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                var ModelPaymentVoucher = new ModelPaymentVoucher()
                {

                    Success1 = false,
                    Success = false,
                    Message = ex.Message

                };
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(ModelPaymentVoucher));
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Saleman_List()
        {
            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                //cmd.Parameters.AddWithValue("@Description", item);
                cmd.CommandText = "Select ID,Name from SaleManList where Compid='01'";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];
                var listBrand = new List<Brand>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string BrandCode = data.Rows[i]["ID"].ToString();
                    string BrandName = data.Rows[i]["Name"].ToString();
                    var brand = new Brand()
                    {
                        Code = BrandCode,
                        Name = BrandName,
                    };

                    listBrand.Add(brand);
                }
                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(listBrand.OrderBy(a => a.Name)));
            }
            catch (Exception)
            {

                throw;
            }

            finally
            {
                con.Close();
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void SaleMan_List()
        {

            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "Select ID,Name from SaleManList where Compid='01'";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];
                var listBrand = new List<Brand>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string BrandCode = data.Rows[i]["ID"].ToString();
                    string BrandName = data.Rows[i]["Name"].ToString();
                    var brand = new Brand()
                    {
                        Code = BrandCode,
                        Name = BrandName,
                    };
                    listBrand.Add(brand);
                }
                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(listBrand.OrderBy(a => a.Name)));
            }
            catch (Exception)
            {

                throw;
            }

            finally
            {
                con.Close();
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GoDown_List()
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            //cmd.Parameters.AddWithValue("@Description", item);
            cmd.CommandText = "Select ID,Name from GoDown where Compid='01'";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];


            var listBrand = new List<Brand>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string GoDownCode = data.Rows[i]["ID"].ToString();
                string GoDownName = data.Rows[i]["Name"].ToString();

                var brand = new Brand()
                {
                    Code = GoDownCode,
                    Name = GoDownName
                };

                listBrand.Add(brand);
            }

            JavaScriptSerializer js = new JavaScriptSerializer();

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(js.Serialize(listBrand.OrderBy(a => a.Name)));
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Color_List()
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            //cmd.Parameters.AddWithValue("@Description", item);
            cmd.CommandText = "Select ID,Color from Color where Compid='01'";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];


            var listBrand = new List<Brand>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string ColorCode = data.Rows[i]["ID"].ToString();
                string ColorName = data.Rows[i]["Color"].ToString();

                var brand = new Brand()
                {
                    Code = ColorCode,
                    Name = ColorName
                };

                listBrand.Add(brand);
            }

            JavaScriptSerializer js = new JavaScriptSerializer();

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            //Context.Response.Write(js.Serialize(listBrand.OrderBy(a => a.Name)));
            Context.Response.Write(js.Serialize(listBrand));
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Manufacturer_List()
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            //cmd.Parameters.AddWithValue("@Description", item);
            cmd.CommandText = "Select ID,Name from Manufacturers where Compid='01' order by name";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];


            var listBrand = new List<Brand>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string BrandCode = data.Rows[i]["ID"].ToString();
                string BrandName = data.Rows[i]["Name"].ToString();

                var brand = new Brand()
                {
                    Code = BrandCode,
                    Name = BrandName,

                };

                listBrand.Add(brand);
            }

            JavaScriptSerializer js = new JavaScriptSerializer();

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(js.Serialize(listBrand.OrderBy(a => a.Name)));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Group_List()
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            //cmd.Parameters.AddWithValue("@Description", item);
            cmd.CommandText = "Select GroupID,GroupName from GroupItem where Compid='01' order by GroupName";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];


            var listBrand = new List<Brand>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string Code = data.Rows[i]["GroupID"].ToString();
                string Name = data.Rows[i]["GroupName"].ToString();

                var brand = new Brand()
                {
                    Code = Code,
                    Name = Name,

                };

                listBrand.Add(brand);
            }

            JavaScriptSerializer js = new JavaScriptSerializer();

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(js.Serialize(listBrand.OrderBy(a => a.Name)));
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Packing_List()
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            //cmd.Parameters.AddWithValue("@Description", item);
            cmd.CommandText = "Select ID,Name from Packing where Compid='01' order by name";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];


            var listBrand = new List<Brand>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string BrandCode = data.Rows[i]["ID"].ToString();
                string BrandName = data.Rows[i]["Name"].ToString();

                var brand = new Brand()
                {
                    Code = BrandCode,
                    Name = BrandName,

                };

                listBrand.Add(brand);
            }

            JavaScriptSerializer js = new JavaScriptSerializer();

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(js.Serialize(listBrand.OrderBy(a => a.Name)));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Expiry_List()
        {
            var security = new Security.Security();
            var uniqueSystemId = security.GetUUID();

            SqlCommand cmd = new SqlCommand("Select CertificateHash,Id from Security", con);
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlDataReader reader = cmd.ExecuteReader();
            var toBeDecrypt = new List<ExpiryViewModel>();
            while (reader.Read())
            {
                var item = new ExpiryViewModel()
                {
                    HashKey = reader["CertificateHash"].ToString(),
                    Id = Convert.ToInt32(reader["Id"])
                };
                toBeDecrypt.Add(item);

            }
            reader.Close();

            var expiries = new List<ExpiryViewModel>();

            foreach (var item in toBeDecrypt)
            {
                var dateAndSystmId = security.DecryptKey(item.HashKey, true).Split('_');
                if (dateAndSystmId[1] == uniqueSystemId)
                {
                    var expiryIn = Math.Floor((Convert.ToDateTime(dateAndSystmId[0]) - DateTime.Now).TotalDays);

                    var newItem = new ExpiryViewModel()
                    {
                        ExpiryDate = Convert.ToDateTime(dateAndSystmId[0]).ToString("MM/dd/yyyy"),
                        ExpiryDateText = Convert.ToDateTime(dateAndSystmId[0]).ToString("MMM dd, yyyy"),
                        ExpiryIn = expiryIn.ToString(),
                        SystemId = dateAndSystmId[1].ToString(),
                        Id = item.Id,
                        IsExpired = expiryIn < 0
                    };

                    expiries.Add(newItem);
                }
            }
            var acitveExpiry = expiries.OrderByDescending(a => a.Id).FirstOrDefault();
            expiries.Where(x => x.Id != acitveExpiry.Id).ToList().ForEach(a => a.IsExpiryClosed = !a.IsExpired);

            JavaScriptSerializer js = new JavaScriptSerializer();

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(js.Serialize(expiries.OrderByDescending(x => x.Id)));
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Station_List()
        {

            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "Select ID,Name from Station where Compid='01' order by name";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];


                var listBrand = new List<Brand>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string code = data.Rows[i]["ID"].ToString();
                    string name = data.Rows[i]["Name"].ToString();

                    var brand = new Brand()
                    {
                        Code = code,
                        Name = name,

                    };

                    listBrand.Add(brand);
                }

                JavaScriptSerializer js = new JavaScriptSerializer();

                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(listBrand.OrderBy(a => a.Name)));
            }
            catch (Exception)
            {

                throw;
            }

            finally
            {
                con.Close();
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Class_List()
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            //cmd.Parameters.AddWithValue("@Description", item);
            cmd.CommandText = "Select ID,Name from Class where Compid='01'";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];


            var listBrand = new List<Brand>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string ClassCode = data.Rows[i]["ID"].ToString();
                string ClassName = data.Rows[i]["Name"].ToString();
                //string BrandBlocked = data.Rows[i]["Blocked"].ToString();
                //string BrandVerifyIME = data.Rows[i]["VerifyIME"].ToString();
                var brand = new Brand()
                {
                    Code = ClassCode,
                    Name = ClassName,
                    //Blocked = BrandBlocked,
                    //VerifyIME = BrandVerifyIME

                };

                listBrand.Add(brand);
            }

            JavaScriptSerializer js = new JavaScriptSerializer();

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(js.Serialize(listBrand.OrderBy(a => a.Name)));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void Department_List()
        {
            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.CommandText = "Select ID,Name from Dept where Compid='" + CompID + "'";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];
            var listBrand = new List<Brand>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string BrandCode = data.Rows[i]["ID"].ToString();
                string BrandName = data.Rows[i]["Name"].ToString();
                var brand = new Brand()
                {
                    Code = BrandCode,
                    Name = BrandName,
                };
                listBrand.Add(brand);
            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(js.Serialize(listBrand.OrderBy(a => a.Name)));
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void BrandWiseDisRateAgainstPartyCode(string ItemCode, String PartyCode)
        {
            ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
            decimal dealRs = 0;
            dealRs = objModPartyCodeAgainstName.BrandWiseDiscountRateAgainstPartyCode(PartyCode, ItemCode);





            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            Context.Response.Write(js.Serialize(dealRs));
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void DealRsAgainstItemCode(string ItemCode, string DealApplyNo)
        {
            ModItem objModItem = new ModItem();
            decimal dealRs = 0;
            dealRs = objModItem.DealRsAgainstItemCode(ItemCode, DealApplyNo);





            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            Context.Response.Write(js.Serialize(dealRs));
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GLCodeAgainstGLTitle(string GLTitle)
        {
            ModGLCode objModGLCode = new ModGLCode();
            string GLCode = "";
            GLCode = objModGLCode.GLCodeAgainstGLTitle(GLTitle);





            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            Context.Response.Write(js.Serialize(GLCode));
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetItemsDealRS(string dealApplyNo, string itemCodes)
        {


            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;

                cmd.CommandText = @"Select  Code,
                                        DealRs,
                                        DealRs2,
                                        DealRs3 
                                From InvCode    
                                Where Code IN (" + itemCodes.Trim(',') + ") and CompId='" + CompID + "' ";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];

                var items = new List<Product>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string Code = data.Rows[i]["Code"].ToString();
                    string DealRs = data.Rows[i]["DealRs"].ToString();
                    if (dealApplyNo == "2")
                    {
                        DealRs = data.Rows[i]["DealRs2"].ToString();
                    }
                    else if (dealApplyNo == "3")
                    {
                        DealRs = data.Rows[i]["DealRs3"].ToString();
                    }

                    var item = new Product()
                    {
                        Code = Code,
                        DealDis = DealRs
                    };
                    items.Add(item);
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(items));
            }
            catch (Exception)
            {

                throw;
            }

            finally
            {
                con.Close();
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GLList(string TitleName, string GLCode)
        {

            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.Parameters.AddWithValue("@TitleName", TitleName.Trim());

                //GLCode == "01010102" for banks
                var isBank = GLCode == "01010102";
                if (isBank)
                {
                    cmd.CommandText = "Select BankCode,BankName,GLCode.Title,AccountNo,GLCode.Balance from BankCode join  GLCode on BankCode.BankCode=GLCode.Code Where (BankName like '%'+@TitleName+'%' OR GLCode.Title like '%'+@TitleName+'%') and GLCode.CompId='" + CompID + "' order by BankName";
                }
                else
                {
                    cmd.CommandText = "Select Top 40 Code,Title,Balance from GLCode Where Title like '%'+@TitleName+'%' and Code like '%" + GLCode + "%' and Lvl=5 and CompId='" + CompID + "' order by Title";

                }
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];


                List<SearchBankModel> BankList = new List<SearchBankModel>();
                List<SearchExpenceModel> GLList = new List<SearchExpenceModel>();


                if (isBank)
                {
                    for (int i = 0; i < data.Rows.Count; i++)
                    {
                        string Code = data.Rows[i]["BankCode"].ToString();
                        string BankName = data.Rows[i]["BankName"].ToString();
                        string Title = data.Rows[i]["Title"].ToString();
                        string AccountNo = data.Rows[i]["AccountNo"].ToString();
                        string Balance = data.Rows[i]["Balance"].ToString();

                        var GLTitle = new SearchBankModel()
                        {
                            Code = Code,
                            Title = Title,
                            BankName = BankName,
                            Balance = Balance,
                            AccountNo = AccountNo
                        };
                        BankList.Add(GLTitle);
                    }
                }
                else
                {

                    for (int i = 0; i < data.Rows.Count; i++)
                    {
                        string Code = data.Rows[i]["Code"].ToString();
                        string Title = data.Rows[i]["Title"].ToString();
                        string Balance = data.Rows[i]["Balance"].ToString();

                        var GLTitle = new SearchExpenceModel()
                        {
                            Code = Code,
                            Title = Title,
                            Balance = Balance
                        };
                        GLList.Add(GLTitle);
                    }
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                if (isBank)
                {
                    Context.Response.Write(js.Serialize(BankList));
                }
                else
                {
                    Context.Response.Write(js.Serialize(GLList));
                }
            }
            finally
            {
                con.Close();
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetAllAccounts()
        {

            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                cmd.CommandText = "Select Code,Title,NorBalance,Lvl,GroupDet,Header_Detail,Balance from GLCode";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];

                var chartOfAccounts = new List<ChartOfAccounts>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string Code = data.Rows[i]["Code"].ToString();
                    string Title = data.Rows[i]["Title"].ToString();
                    string NorBalance = data.Rows[i]["NorBalance"].ToString();
                    string Level = data.Rows[i]["Lvl"].ToString();
                    string Group = data.Rows[i]["GroupDet"].ToString();
                    string Type = data.Rows[i]["Header_Detail"].ToString();
                    string OpBalance = data.Rows[i]["Balance"].ToString();
                    var chartOfAccount = new ChartOfAccounts()
                    {
                        Code = Code,
                        Title = Title,
                        NorBalance = NorBalance,
                        Level = Level,
                        OpeningBalance = OpBalance,
                        Group = Group,
                        Type = Type
                    };
                    chartOfAccounts.Add(chartOfAccount);
                }
                var model = new List<ChartOfAccountsViewModel>();

                for (int i = 1; i <= 2; i++)
                {
                    var accounts = chartOfAccounts.Where(a => a.Level == i.ToString());

                    foreach (var account in accounts)
                    {
                        var chartOfAccount = new ChartOfAccountsViewModel()
                        {
                            Code = account.Code,
                            Title = account.Title,
                            NorBalance = account.NorBalance,
                            Level = account.Level,
                            OpeningBalance = account.OpeningBalance,
                            Group = account.Group,
                            Type = account.Type,
                            children = new List<ChartOfAccountsViewModel>()
                            //chartOfAccounts.Where(a => a.Level == (i + 1).ToString() && a.Code.Contains(account.Title)).ToList()
                        };
                        var lvl = (i - 1) < 1 ? "1" : (i - 1).ToString();
                        if (model.Where(a => a.Level == lvl).Any() && model.Where(a => a.Level == lvl && a.Code.Contains(chartOfAccount.Code.Substring(0, Convert.ToInt32(lvl) * 2))).Any())
                        {
                            var rrr = model.FirstOrDefault(a => a.Level == lvl && a.Code.Contains(chartOfAccount.Code.Substring(0, Convert.ToInt32(lvl) * 2)));
                            var mmm = rrr.children;
                            mmm.Add(chartOfAccount);
                        }
                        else
                        {
                            var newChartOfAccount = new ChartOfAccountsViewModel()
                            {
                                Code = account.Code,
                                Title = account.Title,
                                NorBalance = account.NorBalance,
                                Level = account.Level,
                                OpeningBalance = account.OpeningBalance,
                                Group = account.Group,
                                Type = account.Type,



                                children = new List<ChartOfAccountsViewModel>()
                                //chartOfAccounts.Where(a => a.Level == (i + 1).ToString() && a.Code.Contains(account.Title)).ToList()
                            };
                            model.Add(newChartOfAccount);
                        }

                    }
                }


                var lvl2List = model.SelectMany(a => a.children).ToList();

                var lvl3AddedChilds = addChild(lvl2List, chartOfAccounts);
                var lvl3List = lvl3AddedChilds.SelectMany(a => a.children).ToList();

                var lvl4AddedChilds = addChild(lvl3List, chartOfAccounts);
                var lvl4List = lvl4AddedChilds.SelectMany(a => a.children).ToList();

                var lvl5AddedChilds = addChild(lvl4List, chartOfAccounts);
                var lvl5List = lvl5AddedChilds.SelectMany(a => a.children).ToList();

                con.Close();
                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";

                Context.Response.Write(js.Serialize(model));
            }
            finally
            {
                con.Close();
            }

        }
        private List<ChartOfAccountsViewModel> addChild(List<ChartOfAccountsViewModel> lvlList, List<ChartOfAccounts> chartOfAccounts)
        {
            foreach (var item in lvlList)
            {
                var lvl = (Convert.ToInt32(item.Level) + 1).ToString();
                var accounts = chartOfAccounts.Where(a => a.Level == lvl && a.Code.Substring(0, Convert.ToInt32(item.Level) * 2) == item.Code);
                foreach (var account in accounts)
                {
                    var chartOfAccount = new ChartOfAccountsViewModel()
                    {
                        Code = account.Code,
                        Title = account.Title,
                        NorBalance = account.NorBalance,
                        Level = account.Level,
                        OpeningBalance = account.OpeningBalance,
                        Group = account.Group,
                        Type = account.Type,

                        children = new List<ChartOfAccountsViewModel>()
                        //chartOfAccounts.Where(a => a.Level == (i + 1).ToString() && a.Code.Contains(account.Title)).ToList()
                    };
                    //var item = lvl2List.FirstOrDefault(a => a.Level == lvl && a.Code.Contains(chartOfAccount.Code.Substring(0, Convert.ToInt32(lvl) * 2)));
                    var children = item.children;
                    children.Add(chartOfAccount);
                }
            }
            return lvlList;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetIngredients(string mainItemCOde)
        {

            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.Parameters.AddWithValue("@ItemCode", mainItemCOde.Trim());
            cmd.CommandText = "Select * from Ingridients_Cost Join InvCode on Ingridient_Code=InvCode.Code  where  ItemCode = @ItemCode ";
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];


            List<Product> items = new List<Product>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string Code = data.Rows[i]["Ingridient_Code"].ToString();
                string Description = data.Rows[i]["Description"].ToString().Trim();
                string Qty = data.Rows[i]["Ingridient_Qty"].ToString();
                string Rate = data.Rows[i]["Ingridient_Rate"].ToString();
                string Amount = data.Rows[i]["Amount"].ToString();

                var searchItem = new Product()
                {
                    Code = Code,
                    Description = Description,
                    Qty = Qty,
                    Rate = Rate,
                    Amount = Amount
                };
                items.Add(searchItem);
            }

            JavaScriptSerializer js = new JavaScriptSerializer();

            Context.Response.Clear();
            Context.Response.ContentType = "application/json";

            Context.Response.Write(js.Serialize(items));



        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void IsItemIMEVerifiedAgainstItemCode(string itemCode)
        {
            try
            {
                var modItem = new ModItem();
                var brandCode = modItem.BrandCodeAgainstItemCode(Convert.ToInt32(itemCode));
                var isIMEVerified = modItem.IMEVerifiedAgainstBrandCode(brandCode);
                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(isIMEVerified));
            }
            finally
            {
                con.Close();
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void IsItemIMELegal(string itemCode, string imeCode)
        {
            try
            {
                var modItem = new ModItem();
                var isLengthCorrect = modItem.IsIMELengthCorrect(imeCode);
                var model = new IMEModel();

                if (isLengthCorrect)
                {
                    model = modItem.isIMESold(itemCode, imeCode);
                    model.isLengthCorrect = true;
                }

                //var isIMEVerified = modItem.IMEVerifiedAgainstBrandCode(brandCode);
                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(model));
            }
            finally
            {
                con.Close();
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void IsIMELengthLegalForPurchase(string imeCode)
        {

            try
            {
                var modItem = new ModItem();
                var isLengthCorrect = modItem.IsIMELengthCorrect(imeCode);
                var model = new IMEModel();

                if (isLengthCorrect)
                {
                    model.isLengthCorrect = true;
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(model));
            }
            finally
            {
                con.Close();
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void IsItemPurchasedIMELegal(string itemCode, string imeCode)
        {
            try
            {
                var modItem = new ModItem();
                var isLengthCorrect = modItem.IsIMELengthCorrect(imeCode);
                var model = new IMEModel();

                if (isLengthCorrect)
                {
                    model = modItem.isIMEPurchased(itemCode, imeCode);
                    model.isLengthCorrect = true;
                }

                //var isIMEVerified = modItem.IMEVerifiedAgainstBrandCode(brandCode);
                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(model));
            }
            finally
            {
                con.Close();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetPartyOpeningBalance(string PartyCode)
        {
            var objModule7 = new Module7();
            var date = objModule7.StartDate();
            //if (StartYear != "" || StartYear != null)
            //{
            //    date = Convert.ToDateTime(objModule7.StartDateWithOutYear() + "/" + StartYear);
            //}
            var cmd = new SqlDataAdapter("Select IsNull( OpBalance,0) as OpBalance from PartyOpBalance where  CompID='" + CompID + "' and Code='" + PartyCode + "' and Dat='" + date + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            string OpBalance = "0";

            if (dt.Rows.Count > 0)
            {
                OpBalance = Convert.ToString(dt.Rows[0]["OpBalance"]);

            }

            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(js.Serialize(OpBalance));
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetInventoryOpening(decimal ItemCode)
        {
            var objModule7 = new Module7();
            var objModItem = new ModItem();
            var objModule1 = new Module1();
            var objModViewInventory = new ModViewInventory();
            var objModQtyOfInventory = new ModQtyOfInventory();
            var objModule4 = new Module4();
            var objModPartyCodeAgainstName = new ModPartyCodeAgainstName();

            var opnInv = new OpeningInventoryViewModel();


            opnInv.OpeningUnits = objModItem.ItemOpeningUnits(ItemCode, objModule7.StartDate());
            opnInv.ReceivedUnits = objModViewInventory.ItemQtyReceivedFromToNormal(ItemCode, objModule7.StartDate(), objModule7.EndDate(DateTime.Today));
            opnInv.IssuedUnits = objModViewInventory.ItemQtyIssuedFromToNormal(ItemCode, objModule7.StartDate(), objModule7.EndDate(DateTime.Today));
            opnInv.ClosingUnits = objModViewInventory.ClosingBalanceAgainstItemCode(ItemCode);
            opnInv.SellingCost = objModItem.ItemSellingPrice_1_AgainstItemCode(ItemCode);

            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Clear();
            Context.Response.ContentType = "application/json";
            Context.Response.Write(js.Serialize(opnInv));

        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetUsersList(string stationId)
        {
            JavaScriptSerializer js = new JavaScriptSerializer();

            cmd.Connection = con;
            cmd.CommandType = System.Data.CommandType.Text;
            //cmd.Parameters.AddWithValue("@Description", item);
            cmd.CommandText = "Select Sno,Name,UserName,Password from Userss where Compid='" + CompID + "' and StationId=" + stationId;
            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var data = objDs.Tables[0];


            var listBrand = new List<User>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string Code = data.Rows[i]["Sno"].ToString();
                string Name = data.Rows[i]["Name"].ToString();
                string UserName = data.Rows[i]["UserName"].ToString();
                string Password = data.Rows[i]["Password"].ToString();
                var brand = new User()
                {
                    Code = Code,
                    Name = Name,
                    UserName = UserName,
                    Password = (bool)HttpContext.Current.Session["IsAdmin"] ? Password : "--"
                };

                listBrand.Add(brand);
            }

            var model = js.Serialize(listBrand);
            return model;
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetUserPermittedPagesList(string userId, string stationId)
        {
            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                //cmd.Parameters.AddWithValue("@Description", item);
                cmd.CommandText = "Select PageId from UserMenuPages where  UserId='" + userId + "' and StationId=" + stationId + " and CompId='" + CompID + "'";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];


                var listBrand = new List<Brand>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string id = data.Rows[i]["PageId"].ToString();

                    var brand = new Brand()
                    {
                        Id = id
                    };

                    listBrand.Add(brand);
                }

                JavaScriptSerializer js = new JavaScriptSerializer();

                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(listBrand));
            }
            finally
            {
                con.Close();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetUserPermissions(string userId, string stationId)
        {
            try
            {
                var cmd1 = new SqlCommand(@"Select Type,Value from UserPermissions where  UserId='" + userId + "' and StationId=" + stationId + " and CompId='" + CompID + "'", con);

                DataSet objDs1 = new DataSet();
                SqlDataAdapter dAdapter1 = new SqlDataAdapter();
                dAdapter1.SelectCommand = cmd1;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter1.Fill(objDs1);
                con.Close();


                var data1 = objDs1.Tables[0];

                var userPermisions = new Permisions();
                for (int i = 0; i < data1.Rows.Count; i++)
                {
                    var Type = data1.Rows[i]["Type"].ToString();
                    var Value = (bool)(data1.Rows[i]["Value"]);

                    if (Type == "AllowedOtherStationsSelect" && Value)
                    {
                        userPermisions.AllowedOtherStationsSelect = true;
                    }
                    if (Type == "UserAllowedToSetSellingPrice" && Value)
                    {
                        userPermisions.UserAllowedToSetSellingPrice = true;
                    }
                }


                JavaScriptSerializer js = new JavaScriptSerializer();

                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(userPermisions));
            }
            finally
            {
                con.Close();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetStockMovementVoucher(string id)
        {
            try
            {


                var objModule1 = new Module1();
                var objModule4 = new Module4();
                var lastVoucherNo = objModule1.MaxStockMovementVoucher() - 1;
                var isMaxBRVVoucherNo = lastVoucherNo == Convert.ToDecimal(id) || lastVoucherNo < Convert.ToDecimal(id);

                var cmd = new SqlCommand(
                    "Select " +
                        "SMONo," +
                        "Dat," +
                        "ItemCode," +
                        "ItemName," +
                        "QTY," +
                        "FromStation," +
                        "ToStation," +
                        "Narration," +
                        "FromStationCode," +
                        "ToStationCode," +
                        "SNO," +
                        "Amount," +
                        "Rate," +
                        "CompId  " +
                    "from InventoryMovement " +
                    "where  SMONo=" + id + " and CompId='" + CompID + "'", con);
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];


                var items = new List<SaveVoucherViewModel>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string SMONo = data.Rows[i]["SMONo"].ToString();
                    var Date = Convert.ToDateTime(data.Rows[i]["Dat"].ToString());
                    string ItemCode = data.Rows[i]["ItemCode"].ToString();
                    string ItemName = data.Rows[i]["ItemName"].ToString();
                    string QTY = data.Rows[i]["QTY"].ToString();
                    string Rate = data.Rows[i]["Rate"].ToString();
                    string Amount = data.Rows[i]["Amount"].ToString();
                    string Narration = data.Rows[i]["Narration"].ToString();
                    string ToStationCode = data.Rows[i]["ToStationCode"].ToString();
                    string FromStationCode = data.Rows[i]["FromStationCode"].ToString();
                    string ToStation = data.Rows[i]["ToStation"].ToString();
                    string FromStation = data.Rows[i]["FromStation"].ToString();

                    var brand = new SaveVoucherViewModel()
                    {
                        VoucherNumber = SMONo,
                        Code = ItemCode,
                        Name = ItemName,
                        Qty = QTY,
                        Amount = Amount,
                        Rate = Rate,
                        Date = Date.ToString("dd/MM/yyyy"),
                        StationIdFrom = FromStationCode,
                        StationIdTo = ToStationCode,
                        IsLastVoucherNo = isMaxBRVVoucherNo,
                        StationNameFrom = FromStation,
                        StationNameTo = ToStation,
                        Narration = Narration
                    };

                    items.Add(brand);
                }

                JavaScriptSerializer js = new JavaScriptSerializer();

                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(items));
            }
            finally
            {
                con.Close();
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetPartyItemSellingPrice(string partyCode, string itemCode)
        {
            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                //cmd.Parameters.AddWithValue("@Description", item);
                cmd.CommandText = "Select top 20 Description,Invoice2.Rate,Invoice2.PerDiscount,Invoice2.DealRs,Invoice1.System_Date_Time from Invoice1" +
                    " join invoice2 on Invoice1.InvNo=Invoice2.InvNo where  Invoice2.Code='" + itemCode + "' and  Invoice1.PartyCode='" + partyCode + "' and Invoice1.CompId='" + CompID + "' order by Invoice1.System_Date_Time desc";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];



                var products = new List<Product>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string Rate = data.Rows[i]["Rate"].ToString();
                    string Description = data.Rows[i]["Description"].ToString().Trim();
                    string PerDis = data.Rows[i]["PerDiscount"].ToString();
                    string DealDis = data.Rows[i]["DealRs"].ToString();
                    var product = new Product()
                    {
                        Rate = Rate,
                        SellingDate = Convert.ToDateTime(data.Rows[i]["System_Date_Time"]).ToString("dd/MM/yyyy"),
                        Description = Description,
                        DealDis = DealDis,
                        PerDis = PerDis
                    };
                    products.Add(product);
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(products));
            }
            finally
            {
                con.Close();
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetItemPurchaseRate(string itemCode)
        {

            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                //cmd.Parameters.AddWithValue("@Description", item);
                cmd.CommandText =
                    "Select top 20 " +
                    "InvCode.Description,Purchase2.Cost as Rate, PartyCode.Name as VenderName,BrandName.Name, Purchase2.System_Date_Time" +
                    " from Purchase2" +
                    " join Purchase1 on Purchase2.InvNo=Purchase1.InvNo " +
                    " join InvCode on Purchase2.Code=InvCode.Code " +
                    " join PartyCode on PartyCode.Code=Purchase1.VenderCode " +
                    " join BrandName on InvCode.Brand=BrandName.Code " +
                    "where  InvCode.Code='" + itemCode + "' and Purchase2.CompId='" + CompID + "' order by Purchase2.System_Date_Time desc";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];



                var products = new List<Product>();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string Rate = data.Rows[i]["Rate"].ToString();
                    string Description = data.Rows[i]["Description"].ToString().Trim();
                    string BrandName = data.Rows[i]["Name"].ToString();
                    string VenderName = data.Rows[i]["VenderName"].ToString();
                    var product = new Product()
                    {
                        Rate = Rate,
                        SellingDate = Convert.ToDateTime(data.Rows[i]["System_Date_Time"]).ToString("dd/MM/yyyy"),
                        Description = Description,
                        BrandName = BrandName,
                        VendorName = VenderName
                    };
                    products.Add(product);
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(products));
            }
            finally
            {
                con.Close();
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void UpdateSellingPrice(string itemCode, string price)
        {
            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                //cmd.Parameters.AddWithValue("@Description", item);
                cmd.CommandText =
                    "Update InvCode Set SellingCost=" + price + " where  Code='" + itemCode + "' and CompId='" + CompID + "'";
                if (con.State == ConnectionState.Closed) { con.Open(); }
                cmd.ExecuteNonQuery();
                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(new { success = true }));
            }
            finally
            {
                con.Close();
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetAllConfigurations()
        {
            try
            {
                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                //cmd.Parameters.AddWithValue("@Description", item);
                cmd.CommandText = "Select *  from Configuration where  CompId='" + CompID + "'";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];



                var configurations = new ConfigurationsModel();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    string Description = data.Rows[i]["Description"].ToString().Trim();
                    string Value = data.Rows[i]["ValueNo"].ToString();

                    if (Description == Configurations.SelectLastPurchaseRate)
                    {
                        configurations.SelectLastPurchaseRate = Convert.ToBoolean(Convert.ToInt32(Value));
                    }
                    if (Description == Configurations.ShowItemQtyAtZero)
                    {
                        configurations.ShowItemQtyAtZero = Convert.ToBoolean(Convert.ToInt32(Value));
                    }

                    if (Description == Configurations.InvoiceFormat)
                    {
                        configurations.InvoiceFormat = Convert.ToInt32(Convert.ToInt32(Value));
                    }
                }

                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(configurations));
            }
            finally
            {
                con.Close();
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void GetLPReceivingIMEData(string PartyCode, string IME)
        {
            try
            {


                cmd.Connection = con;
                cmd.CommandType = System.Data.CommandType.Text;
                //cmd.Parameters.AddWithValue("@Description", item);
                cmd.CommandText = "Select *  from Configuration where  CompId='" + CompID + "'";
                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                con.Close();
                var data = objDs.Tables[0];
                var configurations = new ConfigurationsModel();

                string Description = data.Rows[0]["Description"].ToString().Trim();
                string Value = data.Rows[0]["ValueNo"].ToString();

                if (Description == Configurations.SelectLastPurchaseRate)
                {
                    configurations.SelectLastPurchaseRate = Convert.ToBoolean(Convert.ToInt32(Value));
                }
                var item = new Product()
                {
                    Code = "123",
                    Description = "Nokia 1",
                    BrandName = "Nokia",
                    BrandCode = "1",
                    IMEI = "1111111111"
                };


                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Clear();
                Context.Response.ContentType = "application/json";
                Context.Response.Write(js.Serialize(item));
            }
            finally
            {
                con.Close();
            }
        }
    }
}
