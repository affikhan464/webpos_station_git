﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="_Menu.ascx.cs" Inherits="WebPOS.Registration._Menu" %>

<li data-parent="1">
    <a><i class="bm-Registration"></i>Registration <i class="fa arrow"></i></a>
    <ul class="sidebar-nav">
        <li data-parent="2" data-parentname="Registration">
            <a>Item Managment <i class="fa arrow"></i></a>
            <ul class="sidebar-nav">
                <li data-page="1">
                    <a target="_blank" href="/Registration/ItemRegistrationNew.aspx">Add New Item </a>
                </li>
                <li data-parent="3" data-parentname="Item Managment">
                    <a>Others <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="2">
                            <a target="_blank" href="/Registration/BrandManagement.aspx">Brand Management </a>
                        </li>
                        <li data-page="3">
                            <a target="_blank" href="/Registration/ClassManagment.aspx">Class Management </a>
                        </li>
                        <li data-page="4">
                            <a target="_blank" href="/Registration/CategoryManagment.aspx">Category Management </a>
                        </li>
                        <li data-page="5">
                            <a target="_blank" href="/Registration/GoDownManagment.aspx">GoDown Management </a>
                        </li>
                        <li data-page="6">
                            <a target="_blank" href="/Registration/ManufacturerManagment.aspx">Manufacturer Management </a>
                        </li>
                        <li data-page="7">
                            <a target="_blank" href="/Registration/ColorManagment.aspx">Color Management </a>
                        </li>
                        <li data-page="8">
                            <a target="_blank" href="/Registration/PackingManagment.aspx">Packing Management </a>
                        </li>
                    </ul>
                </li>
                <li data-page="9">
                    <a target="_blank" href="/Registration/UpdateSellingPrice.aspx">Set Selling Price </a>
                </li>
                <li data-page="10">
                    <a target="_blank" href="/Registration/BrandWiseDiscount.aspx">Brand Wise Discount </a>
                </li>
                <li data-page="11">
                    <a target="_blank" href="/Registration/BrandWiseDiscount2.aspx">Brand Wise Discount 2</a>
                </li>
                <li data-page="12">
                    <a target="_blank" href="/Registration/BrandWiseSellingPrice.aspx">Brand Wise Selling Price</a>
                </li>
                <li data-page="13">
                    <a target="_blank" href="/Registration/ChartOfAccounts.aspx">Chart Of Accounts </a>
                </li>
            </ul>
        </li>
        <li data-page="14">
            <a target="_blank" href="/Registration/PartyRegistration.aspx">Add New Party </a>
        </li>

        <li data-page="15">
            <a target="_blank" href="/Registration/ExpenceHeadManagment.aspx">Expence Head </a>
        </li>

        <li data-page="16">
            <a target="_blank" href="/Registration/BankAccountManagment.aspx">Bank Account </a>
        </li>
        <li data-page="17">
            <a target="_blank" href="/Registration/SaleManManagment.aspx">Salesman Management </a>
        </li>
        <li data-page="18">
            <a target="_blank" href="/Registration/DepartmentManagment.aspx">Department Management </a>
        </li>
        <li data-page="220">
            <a target="_blank" href="/Registration/UserManagement.aspx">User Management </a>
        </li>
        <li data-page="215">
            <a target="_blank" href="/Registration/UserPermissionManagement.aspx">User Permission Management </a>
        </li>
    </ul>
</li>
<li data-parent="1">
    <a><i class="fas fa-file-alt"></i>Reports <i class="fa arrow"></i></a>
    <ul class="sidebar-nav">
        <li data-parent="2" data-parentname="Reports">
            <a>Sale Reports <i class="fa arrow"></i></a>
            <ul class="sidebar-nav">
                <li data-parent="3" data-parentname="Sale Reports">
                    <a>Category Wise Sale Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="19">
                            <a target="_blank" href="/Reports/SaleReports/AllCategorySaleSummary.aspx">All Category Sale Summary </a>
                        </li>
                        <li data-page="20">
                            <a target="_blank" href="/Reports/SaleReports/CategoryWiseSaleSummary.aspx">Category Wise Sale Summary </a>
                        </li>
                        <li data-page="237">
                            <a target="_blank" href="/Reports/SaleReports/AllCategorySaleSummary2.aspx">All Category Sale Summary 2 </a>
                        </li>
                        <li data-page="238">
                            <a target="_blank" href="/Reports/SaleReports/CategoryAndAllPartySaleSummary.aspx">Category And All Party Sale Summary </a>
                        </li>
                        <li data-page="239">
                            <a target="_blank" href="/Reports/SaleReports/PartyAndAllCategorySaleSummary.aspx">Party And All Category Sale Summary </a>
                        </li>

                        <li data-page="240">
                            <a target="_blank" href="/Reports/SaleReports/CategoryAndPartyWiseSaleSummary.aspx">Category And Party Wise Sale Summary </a>
                        </li>
                    </ul>
                </li>
                <li data-parent="3" data-parentname="Sale Reports">
                    <a>Brand Wise Sale Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="21">
                            <a target="_blank" href="/Reports/SaleReports/AllBrandSaleSummary.aspx">All Brand Sale Summary </a>
                        </li>
                        <li data-page="22">
                            <a target="_blank" href="/Reports/SaleReports/AllBrandSaleSummary2.aspx">All Brand Sale Summary 2 </a>
                        </li>
                        <li data-page="23">
                            <a target="_blank" href="/Reports/SaleReports/BrandAndAllPartySaleSummary.aspx">Brand & All Party Sale Summary </a>
                        </li>
                        <li data-page="410">
                            <a target="_blank" href="/Reports/SaleReports/BrandWiseItemGP.aspx">Brand Wise Item GP</a>
                        </li>
                        <li data-page="24">
                            <a target="_blank" href="/Reports/SaleReports/PartyAndAllBrandSaleSummary.aspx">Party & All Brand SaleSummary </a>
                        </li>
                        <li data-page="25">
                            <a target="_blank" href="/Reports/SaleReports/BrandAndPartyWiseSaleSummary.aspx">Brand & Party Wise Sale Summary </a>
                        </li>
                        <li data-page="26">
                            <a target="_blank" href="/Reports/SaleReports/BrandWiseSaleSummary.aspx">Brand Wise Sale Summary </a>
                        </li>
                        <li data-page="27">
                            <a target="_blank" href="/Reports/SaleReports/BrandWiseSaleGroupByParty.aspx">Brand Wise Sale Group By Party </a>
                        </li>
                        <li data-page="28">
                            <a target="_blank" href="/Reports/SaleReports/BrandWiseSaleReport1.aspx">Brand Wise Sale Report 1 </a>
                        </li>
                        <li data-page="29">
                            <a target="_blank" href="/Reports/SaleReports/BrandWiseSaleReport2.aspx">Brand Wise Sale Report 2 </a>
                        </li>
                    </ul>
                </li>
                <li data-parent="3" data-parentname="Sale Reports">
                    <a>Class Or Warrenty Wise <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="241">
                            <a target="_blank" href="/Reports/SaleReports/AllClassSaleSummary.aspx">All Class Sale Summary </a>
                        </li>
                        <li data-page="242">
                            <a target="_blank" href="/Reports/SaleReports/ClassWiseSaleSummary.aspx">Class Wise Sale Summary </a>
                        </li>
                        <li data-page="243">
                            <a target="_blank" href="/Reports/SaleReports/ClassAndPartyWiseSaleSummary.aspx">Class And Party Wise Sale Summary</a>
                        </li>
                        <li data-page="244">
                            <a target="_blank" href="/Reports/SaleReports/PartyAndAllClassSaleSummary.aspx">Party And All Class Sale Summary</a>
                        </li>
                        <li data-page="245">
                            <a target="_blank" href="/Reports/SaleReports/ClassAndAllPartySaleSummary.aspx">Class And All Party Sale Summary</a>
                        </li>
                        <li data-page="30">
                            <a target="_blank" href="/Reports/SaleReports/ClassOrWarrentyWiseSaleDetail.aspx">Class Or Warrenty Wise Sale Detail </a>
                        </li>
                        <li data-page="31">
                            <a target="_blank" href="/Reports/SaleReports/ClassOrWarrentyWiseSaleSummary.aspx">Class Or Warrenty Wise Sale Summary </a>
                        </li>
                    </ul>
                </li>
                <li data-parent="3" data-parentname="Sale Reports">
                    <a>Color Wise Sale Report <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="246">
                            <a target="_blank" href="/Reports/SaleReports/Color_AllColorSaleSummary.aspx">All Color Sale Summary</a>
                        </li>
                        <li data-page="247">
                            <a target="_blank" href="/Reports/SaleReports/Color_ColorWiseSaleSummary.aspx">Color Wise Sale Summary</a>
                        </li>
                        <li data-page="248">
                            <a target="_blank" href="/Reports/SaleReports/Color_ColorAndAllPartySaleSummary.aspx">Color And All Party Sale Summary</a>
                        </li>
                        <li data-page="249">
                            <a target="_blank" href="/Reports/SaleReports/Color_PartyAndAllColorSaleSummary.aspx">Party And All Color Sale Summary</a>
                        </li>
                        <li data-page="250">
                            <a target="_blank" href="/Reports/SaleReports/Color_ColorAndPartyWiseSaleSummary.aspx">Color And Party Wise Sale Summary</a>
                        </li>
                    </ul>
                </li>
                <li data-parent="3" data-parentname="Sale Reports">
                    <a>Model Wise <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="251">
                            <a target="_blank" href="/Reports/SaleReports/Model_AllModelSaleSumma4ry.aspx">All Model Sale Summary</a>
                        </li>
                        <li data-page="252">
                            <a target="_blank" href="/Reports/SaleReports/Model_ModelAndAllPartySaleSummary.aspx">Model And All Party Sale Summary</a>
                        </li>
                        <li data-page="253">
                            <a target="_blank" href="/Reports/SaleReports/Model_ModelAndPartyWiseSaleSummary.aspx">Model And Party Wise Sale Summary</a>
                        </li>
                        <li data-page="254">
                            <a target="_blank" href="/Reports/SaleReports/Model_ModelWiseSaleSummary.aspx">Model Wise Sale Summary</a>
                        </li>
                        <li data-page="255">
                            <a target="_blank" href="/Reports/SaleReports/Model_PartyAndAllModelSaleSummary.aspx">Party And All Model Sale Summary</a>
                        </li>
                    </ul>
                </li>
                <li data-parent="3" data-parentname="Sale Reports">
                    <a>Day Wise Sale Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="32">
                            <a target="_blank" href="/Reports/SaleReports/DayCashAndCreditSale.aspx">Day Cash & Credit Sale </a>
                        </li>
                        <li data-page="460">
                            <a target="_blank" href="/Reports/SaleReports/DayCashAndCreditSaleWithExpense.aspx">Day Cash & Credit Sale With Expense </a>
                        </li>
                        <li data-page="236">
                            <a target="_blank" href="/Reports/SaleReports/DayCashAndCreditSalePartyWise.aspx">Day Cash & Credit Sale Party Wise </a>
                        </li>
                        <li data-page="33">
                            <a target="_blank" href="/Reports/SaleReports/DaySaleGroupByItem.aspx">Day Sale Group By Item </a>
                        </li>
                        <li data-page="34">
                            <a target="_blank" href="/Reports/SaleReports/DaySaleGroupByItem2.aspx">Day Sale Group By Item 2 </a>
                        </li>
                        <li data-page="35">
                            <a target="_blank" href="/Reports/SaleReports/DateWiseSaleSummary.aspx">Date Wise Sale Summary </a>
                        </li>
                    </ul>
                </li>
                <li data-parent="3" data-parentname="Sale Reports">
                    <a>Item and Party Wise Sale Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="36">
                            <a target="_blank" href="/Reports/SaleReports/ItemAndPartyWiseSaleDetail.aspx">Item and Party Wise Sale Detail </a>
                        </li>
                        <li data-page="37">
                            <a target="_blank" href="/Reports/SaleReports/ItemWiseSaleDetail.aspx">Item Wise Sale Detail </a>
                        </li>
                        <li data-page="38">
                            <a target="_blank" href="/Reports/SaleReports/ItemWiseSaleSummary.aspx">Item Wise Sale Summary </a>
                        </li>
                        <li data-page="39">
                            <a target="_blank" href="/Reports/SaleReports/ItemWiseSaleGroupByParty.aspx">Item Wise Sale Group By Party </a>
                        </li>
                    </ul>
                </li>

                <li data-parent="3" data-parentname="Sale Reports">
                    <a>Operator Or Salesman <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="40">
                            <a target="_blank" href="/Reports/SaleReports/OperatorOrSalesmanWiseSaleReport.aspx">Operator Or Salesman Wise Sale Report
                            </a>
                        </li>
                        <li data-page="406">
                            <a target="_blank" href="/Reports/SaleReports/OperatorOrSalesmanSaleCommissionReport.aspx">Operator Or Salesman Sale Commission Report </a>
                        </li>
                        <li data-page="462">
                            <a target="_blank" href="/Reports/SaleReports/OperatorOrSalesmanSaleCommissionReportAdvanced.aspx">Operator Or Salesman Sale Commission Report (Advanced) </a>
                        </li>
                    </ul>
                </li>
                <li data-parent="3" data-parentname="Sale Reports">
                    <a>Party Wise Sale Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="41">
                            <a target="_blank" href="/Reports/SaleReports/PartyWiseSaleSummary.aspx">Party Wise Sale Summary </a>
                        </li>
                        <li data-page="42">
                            <a target="_blank" href="/Reports/SaleReports/PartyWiseItemSummary.aspx">Party Wise Item Summary </a>
                        </li>
                        <li data-page="43">
                            <a target="_blank" href="/Reports/SaleReports/PartyGroupWiseSale.aspx">Party Group Wise Sale </a>
                        </li>
                    </ul>
                </li>
                <li data-page="221">
                    <a target="_blank" href="/Reports/SaleReports/StationsSaleAndExpenceReport.aspx">Stations Sale And Expence Report</a>
                </li>
            </ul>
        </li>

        <li data-parent="2" data-parentname="Reports">
            <a>Sale Return Reports <i class="fa arrow"></i></a>
            <ul class="sidebar-nav">
                <li data-parent="3" data-parentname="Sale Return Reports">
                    <a>Category Wise Sale Return Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="44">
                            <a target="_blank" href="/Reports/SaleReturnReports/AllCategorySaleReturnSummary.aspx">All Category Sale Return Summary </a>
                        </li>
                        <li data-page="45">
                            <a target="_blank" href="/Reports/SaleReturnReports/CategoryWiseSaleReturnSummary.aspx">Category Wise Sale Return Summary </a>
                        </li>
                    </ul>
                </li>
                <li data-parent="3" data-parentname="Sale Return Reports">
                    <a>Brand Wise Sale Return Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="46">
                            <a target="_blank" href="/Reports/SaleReturnReports/AllBrandSaleReturnSummary.aspx">All Brand Sale Return Summary </a>
                        </li>
                        <li data-page="47">
                            <a target="_blank" href="/Reports/SaleReturnReports/AllBrandSaleReturnSummary2.aspx">All Brand Sale Return Summary 2 </a>
                        </li>
                        <li data-page="48">
                            <a target="_blank" href="/Reports/SaleReturnReports/BrandAndAllPartySaleReturnSummary.aspx">Brand & All Party Sale Return Return Summary </a>
                        </li>
                        <li data-page="49">
                            <a target="_blank" href="/Reports/SaleReturnReports/PartyAndAllBrandSaleReturnSummary.aspx">Party & All Brand Sale Return Summary </a>
                        </li>
                        <li data-page="50">
                            <a target="_blank" href="/Reports/SaleReturnReports/BrandAndPartyWiseSaleReturnSummary.aspx">Brand & Party Wise Sale Return Summary </a>
                        </li>
                        <li data-page="51">
                            <a target="_blank" href="/Reports/SaleReturnReports/BrandWiseSaleReturnSummary.aspx">Brand Wise Sale Return Summary </a>
                        </li>
                        <li data-page="52">
                            <a target="_blank" href="/Reports/SaleReturnReports/BrandWiseSaleReturnGroupByParty.aspx">Brand Wise Sale Return Group By Party </a>
                        </li>
                        <li data-page="53">
                            <a target="_blank" href="/Reports/SaleReturnReports/BrandWiseSaleReturnReport1.aspx">Brand Wise Sale Return Report 1 </a>
                        </li>
                        <li data-page="54">
                            <a target="_blank" href="/Reports/SaleReturnReports/BrandWiseSaleReturnReport2.aspx">Brand Wise Sale Return Report 2 </a>
                        </li>
                    </ul>
                </li>
                <li data-parent="3" data-parentname="Sale Return Reports">
                    <a>Category <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="301">
                            <a target="_blank" href="/Reports/SaleReturnReports/PartyAndAllCategorySaleReturnSummary.aspx">Party And All Category Sale Return Summary </a>
                        </li>
                        <li data-page="302">
                            <a target="_blank" href="/Reports/SaleReturnReports/CategoryAndPartyWiseSaleReturnSummary.aspx">Category And Party Wise Sale Return Summary </a>
                        </li>
                        <li data-page="303">
                            <a target="_blank" href="/Reports/SaleReturnReports/CategoryWiseSaleReturnSummary.aspx">Category Wise Sale Return Summary </a>
                        </li>
                    </ul>
                </li>
                <li data-parent="3" data-parentname="Sale Return Reports">
                    <a>Class Or Warrenty Wise Sale Return Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="55">
                            <a target="_blank" href="/Reports/SaleReturnReports/ClassOrWarrentyWiseSaleReturnDetail.aspx">Class Or Warrenty Wise Sale Return Detail </a>
                        </li>
                        <li data-page="304">
                            <a target="_blank" href="/Reports/SaleReturnReports/ClassAndAllPartySaleReturnSummary.aspx">Class And All Party Sale Return Summary </a>
                        </li>
                        <li data-page="305">
                            <a target="_blank" href="/Reports/SaleReturnReports/ClassAndPartyWiseSaleReturnSummary.aspx">Class And Party Wise Sale Return Summary </a>
                        </li>
                        <li data-page="306">
                            <a target="_blank" href="/Reports/SaleReturnReports/ClassOrWarrentyWiseSaleReturnDetail.aspx">Class Or Warrenty Wise Sale Return Detail </a>
                        </li>
                        <li data-page="56">
                            <a target="_blank" href="/Reports/SaleReturnReports/ClassOrWarrentyWiseSaleReturnSummary.aspx">Class Or Warrenty Wise Sale Return Summary </a>
                        </li>
                        <li data-page="307">
                            <a target="_blank" href="/Reports/SaleReturnReports/ClassWiseSaleReturnSummary.aspx">Class Wise Sale Return Summary </a>
                        </li>
                    </ul>
                </li>
                <li data-parent="3" data-parentname="Sale Return Reports">
                    <a>Color Wise <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="308">
                            <a target="_blank" href="/Reports/SaleReturnReports/Color_AllColorSaleReturnSummary.aspx">All Color Sale Return Summary</a>
                        </li>
                        <li data-page="309">
                            <a target="_blank" href="/Reports/SaleReturnReports/Color_ColorAndAllPartySaleReturnSummary.aspx">Color And All Party Sale Return Summary</a>
                        </li>
                        <li data-page="310">
                            <a target="_blank" href="/Reports/SaleReturnReports/Color_ColorAndPartyWiseSaleReturnSummary.aspx">Color And Party Wise Sale Return Summary </a>
                        </li>
                        <li data-page="311">
                            <a target="_blank" href="/Reports/SaleReturnReports/Color_ColorWiseSaleReturnSummary.aspx">Color Wise Sale Return Summary </a>
                        </li>
                        <li data-page="312">
                            <a target="_blank" href="/Reports/SaleReturnReports/Color_PartyAndAllColorSaleReturnSummary.aspx">Party And All Color Sale Return Summary </a>
                        </li>
                    </ul>
                </li>
                <li data-parent="3" data-parentname="Sale Return Reports">
                    <a>Day Wise Sale Return Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="60">
                            <a target="_blank" href="/Reports/SaleReturnReports/DateWiseSaleReturnSummary.aspx">Date Wise Sale Return Summary</a>
                        </li>
                        <li data-page="57">
                            <a target="_blank" href="/Reports/SaleReturnReports/DayCashAndCreditSaleReturn.aspx">Day Cash & Credit Sale Return</a>
                        </li>
                        <li data-page="313">
                            <a target="_blank" href="/Reports/SaleReturnReports/DayCashAndCreditSaleReturnPartyWise.aspx">Day Cash & Credit Sale Return Party Wise </a>
                        </li>
                        <li data-page="58">
                            <a target="_blank" href="/Reports/SaleReturnReports/DaySaleReturnGroupByItem.aspx">Day Sale Return Group By Item </a>
                        </li>
                        <li data-page="59">
                            <a target="_blank" href="/Reports/SaleReturnReports/DaySaleReturnGroupByItem2.aspx">Day Sale Return Group By Item 2 </a>
                        </li>
                    </ul>
                </li>
                <li data-parent="3" data-parentname="Sale Return Reports">
                    <a>Item and Party Wise Sale Return Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="61">
                            <a target="_blank" href="/Reports/SaleReturnReports/ItemAndPartyWiseSaleReturnDetail.aspx">Item and Party Wise Sale Return Detail </a>
                        </li>
                        <li data-page="62">
                            <a target="_blank" href="/Reports/SaleReturnReports/ItemWiseSaleReturnDetail.aspx">Item Wise Sale Return Detail </a>
                        </li>
                        <li data-page="63">
                            <a target="_blank" href="/Reports/SaleReturnReports/ItemWiseSaleReturnSummary.aspx">Item Wise Sale Return Summary </a>
                        </li>
                        <li data-page="64">
                            <a target="_blank" href="/Reports/SaleReturnReports/ItemWiseSaleReturnGroupByParty.aspx">Item Wise Sale Return Group By Party </a>
                        </li>
                    </ul>
                </li>
                <li data-page="65">
                    <a target="_blank" href="/Reports/SaleReturnReports/OperatorOrSalesmanWiseSaleReturnReport.aspx">Operator Or Salesman Wise Sale Return Report
                    </a>
                </li>
                <li data-parent="3" data-parentname="Sale Return Reports">
                    <a>Party Wise Sale Return Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="314">
                            <a target="_blank" href="/Reports/SaleReturnReports/PartyAndAllClassSaleReturnSummary.aspx">Party And All Class Sale Return Summary </a>
                        </li>
                        <li data-page="66">
                            <a target="_blank" href="/Reports/SaleReturnReports/PartyWiseSaleReturnSummary.aspx">Party Wise Sale Return Summary </a>
                        </li>
                        <li data-page="405">
                            <a target="_blank" href="/Reports/SaleReturnReports/PartyAndAllCategorySaleReturnSummary.aspx">Party And All Category Sale Return Summary</a>
                        </li>
                        <li data-page="67">
                            <a target="_blank" href="/Reports/SaleReturnReports/PartyWiseItemSummarySaleReturn.aspx">Party Wise Item Summary Sale Return </a>
                        </li>
                        <li data-page="68">
                            <a target="_blank" href="/Reports/SaleReturnReports/PartyGroupWiseSaleReturn.aspx">Party Group Wise Sale Return </a>
                        </li>
                        <li data-page="315">
                            <a target="_blank" href="/Reports/SaleReturnReports/PartyAndAllBrandSaleReturnSummary.aspx">Party And All Brand Sale Return Summary </a>
                        </li>

                        <li data-page="316">
                            <a target="_blank" href="/Reports/SaleReturnReports/PartyWiseItemReturnSummary.aspx">Party Wise Item Return Summary</a>
                        </li>
                        <li data-page="317">
                            <a target="_blank" href="/Reports/SaleReturnReports/PartyWiseItemReturnDetail.aspx">Party Wise Item Return Detail</a>
                        </li>
                    </ul>
                </li>
                <li data-parent="3" data-parentname="Sale Return Reports">
                    <a>Model Wise <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="318">
                            <a target="_blank" href="/Reports/SaleReturnReports/Model_AllModelSaleReturnSummary.aspx">All Model Sale Return Summary </a>
                        </li>
                        <li data-page="319">
                            <a target="_blank" href="/Reports/SaleReturnReports/Model_ModelAndAllPartySaleReturnSummary.aspx">Model And All Party Sale Return Summary </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li data-parent="2" data-parentname="Reports">
            <a>Purchase Reports <i class="fa arrow"></i></a>
            <ul class="sidebar-nav">
                <li data-parent="3" data-parentname="Purchase Reports">
                    <a>Category Wise Purchase Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="69">
                            <a target="_blank" href="/Reports/PurchaseReports/Category_AllCategoryPurchaseSummary.aspx">All Category Purchase Summary</a>
                        </li>
                        <li data-page="320">
                            <a target="_blank" href="/Reports/PurchaseReports/Category_CategoryAndAllPartyPurchaseSummary.aspx">Category And All Party Purchase Summary </a>
                        </li>
                        <li data-page="321">
                            <a target="_blank" href="/Reports/PurchaseReports/Category_CategoryAndPartyWisePurchaseSummary.aspx">Category And Party Wise Purchase Summary</a>
                        </li>
                        <li data-page="322">
                            <a target="_blank" href="/Reports/PurchaseReports/Category_CategoryWisePurchaseReport1.aspx">Category Wise Purchase Report1</a>
                        </li>
                        <li data-page="323">
                            <a target="_blank" href="/Reports/PurchaseReports/Category_CategoryWisePurchaseReport2.aspx">Category Wise Purchase Report 2</a>
                        </li>
                        <li data-page="70">
                            <a target="_blank" href="/Reports/PurchaseReports/Category_CategoryWisePurchaseSummary.aspx">Category Wise Purchase Summary</a>
                        </li>
                        <li data-page="324">
                            <a target="_blank" href="/Reports/PurchaseReports/Category_PartyAndAllCategoryPurchaseSummary.aspx">Party And All Category Purchase Summary</a>
                        </li>
                    </ul>
                </li>
                <li data-parent="3" data-parentname="Purchase Reports">
                    <a>Brand Wise Purchase Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="71">
                            <a target="_blank" href="/Reports/PurchaseReports/Brand_AllBrandPurchaseSummary.aspx">All Brand Purchase Summary </a>
                        </li>
                        <li data-page="73">
                            <a target="_blank" href="/Reports/PurchaseReports/Brand_BrandAndAllPartyPurchaseSummary.aspx">Brand & All Party Purchase Summary </a>
                        </li>

                        <li data-page="74">
                            <a target="_blank" href="/Reports/PurchaseReports/Brand_PartyAndAllBrandPurchaseSummary.aspx">Party & All Brand Purchase Summary </a>
                        </li>
                        <li data-page="76">
                            <a target="_blank" href="/Reports/PurchaseReports/Brand_BrandWisePurchaseSummary.aspx">Brand Wise Purchase Summary</a>
                        </li>
                        <li data-page="75">
                            <a target="_blank" href="/Reports/PurchaseReports/Brand_BrandAndPartyWisePurchaseSummary.aspx">Brand & Party Wise Purchase Summary </a>
                        </li>

                        <li data-page="78">
                            <a target="_blank" href="/Reports/PurchaseReports/Brand_BrandWisePurchaseReport1.aspx">Brand Wise Purchase Report 1 </a>
                        </li>
                        <li data-page="79">
                            <a target="_blank" href="/Reports/PurchaseReports/Brand_BrandWisePurchaseReport2.aspx">Brand Wise Purchase Report2</a>
                        </li>
                        <li data-page="72">
                            <a target="_blank" href="/Reports/PurchaseReports/AllBrandPurchaseSummary2.aspx">All Brand Purchase Summary 2 </a>
                        </li>
                        <li data-page="77">
                            <a target="_blank" href="/Reports/PurchaseReports/BrandWisePurchaseGroupByParty.aspx">Brand Wise Purchase Group By Party </a>
                        </li>
                    </ul>
                </li>
                <li data-parent="3" data-parentname="Purchase Reports">
                    <a>Class Or Warrenty Wise Purchase Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="325">
                            <a target="_blank" href="/Reports/PurchaseReports/Class_AllClassPurchaseSummary.aspx">All Class Purchase Summary </a>
                        </li>
                        <li data-page="326">
                            <a target="_blank" href="/Reports/PurchaseReports/Class_ClassAndAllPartyPurchaseSummary.aspx">Class And All Party Purchase Summary </a>
                        </li>
                        <li data-page="80">
                            <a target="_blank" href="/Reports/PurchaseReports/Class_ClassAndPartyWisePurchaseSummary.aspx">Class And Party Wise Purchase Summary </a>
                        </li>
                        <li data-page="327">
                            <a target="_blank" href="/Reports/PurchaseReports/Class_ClassWisePurchaseReport1.aspx">Class Wise Purchase Report 1 </a>
                        </li>
                        <li data-page="328">
                            <a target="_blank" href="/Reports/PurchaseReports/Class_ClassWisePurchaseReport2.aspx">Class Wise Purchase Report 2 </a>
                        </li>
                        <li data-page="329">
                            <a target="_blank" href="/Reports/PurchaseReports/Class_ClassWisePurchaseSummary.aspx">Class Wise Purchase Summary </a>
                        </li>
                        <li data-page="330">
                            <a target="_blank" href="/Reports/PurchaseReports/Class_PartyAndAllClassPurchaseSummary.aspx">Party And All Class Purchase Summary </a>
                        </li>
                        <li data-page="81">
                            <a target="_blank" href="/Reports/PurchaseReports/ClassOrWarrentyWisePurchaseSummary.aspx">Class Or Warrenty Wise Purchase Summary </a>
                        </li>
                    </ul>
                </li>
                <li data-parent="3" data-parentname="Purchase Reports">
                    <a>Model <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="331">
                            <a target="_blank" href="/Reports/PurchaseReports/Model_AllModelPurchaseSummary.aspx">All Model Purchase Summary </a>
                        </li>
                        <li data-page="332">
                            <a target="_blank" href="/Reports/PurchaseReports/Model_ModelAndAllPartyPurchaseSummary.aspx">Model And All Party Purchase Summary </a>
                        </li>
                        <li data-page="333">
                            <a target="_blank" href="/Reports/PurchaseReports/Model_ModelAndPartyWisePurchaseSummary.aspx">Model And Party Wise Purchase Summary</a>
                        </li>
                        <li data-page="334">
                            <a target="_blank" href="/Reports/PurchaseReports/Model_ModelWisePurchaseReport1.aspx">Model Wise Purchase Report 1</a>
                        </li>
                        <li data-page="335">
                            <a target="_blank" href="/Reports/PurchaseReports/Model_ModelWisePurchaseReport2.aspx">Model Wise Purchase Report 2</a>
                        </li>
                        <li data-page="336">
                            <a target="_blank" href="/Reports/PurchaseReports/Model_ModelWisePurchaseSummary.aspx">Model Wise Purchase Summary</a>
                        </li>
                        <li data-page="337">
                            <a target="_blank" href="/Reports/PurchaseReports/Model_PartyAndAllModelPurchaseSummary.aspx">Party And All Model Purchase Summary</a>
                        </li>

                    </ul>
                </li>
                <li data-parent="3" data-parentname="Purchase Reports">
                    <a>Day Wise Purchase Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="82">
                            <a target="_blank" href="/Reports/PurchaseReports/DayCashAndCreditPurchase.aspx">Day Cash & Credit Purchase </a>
                        </li>
                        <li data-page="83">
                            <a target="_blank" href="/Reports/PurchaseReports/DayPurchaseGroupByItem.aspx">Day Purchase Group By Item </a>
                        </li>
                        <li data-page="84">
                            <a target="_blank" href="/Reports/PurchaseReports/DayPurchaseGroupByItem2.aspx">Day Purchase Group By Item 2 </a>
                        </li>
                        <li data-page="85">
                            <a target="_blank" href="/Reports/PurchaseReports/DateWisePurchaseSummary.aspx">Date Wise Purchase Summary </a>
                        </li>

                    </ul>
                </li>
                <li data-parent="3" data-parentname="Purchase Reports">
                    <a>Item and Party Wise Purchase Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="86">
                            <a target="_blank" href="/Reports/PurchaseReports/ItemAndPartyWisePurchaseDetail.aspx">Item and Party Wise Purchase Detail </a>
                        </li>
                        <li data-page="87">
                            <a target="_blank" href="/Reports/PurchaseReports/ItemWisePurchaseDetail.aspx">Item Wise Purchase Detail </a>
                        </li>
                        <li data-page="88">
                            <a target="_blank" href="/Reports/PurchaseReports/ItemWisePurchaseSummary.aspx">Item Wise Purchase Summary </a>
                        </li>
                        <li data-page="89">
                            <a target="_blank" href="/Reports/PurchaseReports/ItemWisePurchaseGroupByParty.aspx">Item Wise Purchase Group By Party </a>
                        </li>
                    </ul>
                </li>
                <li data-parent="3" data-parentname="Purchase Reports">
                    <a>Party Wise Purchase Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="90">
                            <a target="_blank" href="/Reports/PurchaseReports/PartyWisePurchaseSummary.aspx">Party Wise Purchase Summary </a>
                        </li>
                        <li data-page="91">
                            <a target="_blank" href="/Reports/PurchaseReports/PartyWiseItemSummaryPurchase.aspx">Party Wise Item Summary Purchase </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>

        <li data-parent="2" data-parentname="Reports">
            <a>Purchase Return Reports <i class="fa arrow"></i></a>
            <ul class="sidebar-nav">
                <li data-parent="3" data-parentname="Purchase Return Reports">
                    <a>Category Wise Purchase Return Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="92">
                            <a target="_blank" href="/Reports/PurchaseReturnReports/AllCategoryPurchaseReturnSummary.aspx">All Category Purchase Return Summary </a>
                        </li>
                        <li data-page="93">
                            <a target="_blank" href="/Reports/PurchaseReturnReports/CategoryWisePurchaseReturnSummary.aspx">Category Wise Purchase Return Summary </a>
                        </li>
                    </ul>
                </li>
                <li data-parent="3" data-parentname="Purchase Return Reports">
                    <a>Brand Wise Purchase Return Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="94">
                            <a target="_blank" href="/Reports/PurchaseReturnReports/Brand_AllBrandPurchaseReturnSummary.aspx">All Brand Purchase Return Summary </a>
                        </li>
                        <li data-page="95">
                            <a target="_blank" href="/Reports/PurchaseReturnReports/AllBrandPurchaseReturnSummary2.aspx">All Brand Purchase Return Summary 2 </a>
                        </li>
                        <li data-page="96">
                            <a target="_blank" href="/Reports/PurchaseReturnReports/Brand_BrandAndAllPartyPurchaseReturnSummary.aspx">Brand And All Party Purchase Return Summary</a>
                        </li>

                        <li data-page="98">
                            <a target="_blank" href="/Reports/PurchaseReturnReports/Brand_BrandAndPartyWisePurchaseReturnSummary.aspx">Brand & Party Wise Purchase Return Summary</a>
                        </li>
                        <li data-page="99">
                            <a target="_blank" href="/Reports/PurchaseReturnReports/Brand_BrandWisePurchaseReturnSummary.aspx">Brand Wise Purchase Return Summary </a>
                        </li>
                        <li data-page="97">
                            <a target="_blank" href="/Reports/PurchaseReturnReports/Brand_PartyAndAllBrandPurchaseReturnSummary.aspx">Party And All Brand Purchase Return Summary </a>
                        </li>
                        <li data-page="101">
                            <a target="_blank" href="/Reports/PurchaseReturnReports/Brand_BrandWisePurchaseReturnReport1.aspx">Brand Wise Purchase Return Report 1 </a>
                        </li>
                        <li data-page="102">
                            <a target="_blank" href="/Reports/PurchaseReturnReports/BrandWisePurchaseReturnReport2.aspx">Brand Wise Purchase Return Report 2 </a>
                        </li>

                        <li data-page="100">
                            <a target="_blank" href="/Reports/PurchaseReturnReports/BrandWisePurchaseReturnGroupByParty.aspx">Brand Wise Purchase Return Group By Party </a>
                        </li>

                    </ul>
                </li>
                <li data-parent="3" data-parentname="Purchase Return Reports">
                    <a>Class Or Warrenty Wise Purchase Return Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="103">
                            <a target="_blank" href="/Reports/PurchaseReturnReports/ClassOrWarrentyWisePurchaseReturnDetail.aspx">Class Or Warrenty Wise Purchase Return Detail </a>
                        </li>
                        <li data-page="104">
                            <a target="_blank" href="/Reports/PurchaseReturnReports/ClassOrWarrentyWisePurchaseReturnSummary.aspx">Class Or Warrenty Wise Purchase Return Summary </a>
                        </li>
                    </ul>
                </li>
                <li data-parent="3" data-parentname="Purchase Return Reports">
                    <a>Day Wise Purchase Return Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="338">
                            <a target="_blank" href="/Reports/PurchaseReturnReports/DateWisePurchaseReturnSummary.aspx">Date Wise Purchase Return Summary</a>
                        </li>
                        <li data-page="105">
                            <a target="_blank" href="/Reports/PurchaseReturnReports/DayCashAndCreditPurchaseReturn.aspx">Day Cash & Credit Purchase Return</a>
                        </li>
                        <li data-page="106">
                            <a target="_blank" href="/Reports/PurchaseReturnReports/DayPurchaseReturnGroupByItem.aspx">Day Purchase Return Group By Item </a>
                        </li>
                        <li data-page="107">
                            <a target="_blank" href="/Reports/PurchaseReturnReports/DayPurchaseReturnGroupByItem2.aspx">Day Purchase Return Group By Item 2 </a>
                        </li>
                        <li data-page="108">
                            <a target="_blank" href="/Reports/PurchaseReturnReports/DateWisePurchaseReturnSummary.aspx">Date Wise Purchase Return Summary </a>
                        </li>
                    </ul>
                </li>
                <li data-parent="3" data-parentname="Purchase Return Reports">
                    <a>Item and Party Wise Purchase Return Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="109">
                            <a target="_blank" href="/Reports/PurchaseReturnReports/ItemAndPartyWisePurchaseReturnDetail.aspx">Item and Party Wise Purchase Return Detail </a>
                        </li>
                        <li data-page="110">
                            <a target="_blank" href="/Reports/PurchaseReturnReports/ItemWisePurchaseReturnDetail.aspx">Item Wise Purchase Return Detail </a>
                        </li>
                        <li data-page="111">
                            <a target="_blank" href="/Reports/PurchaseReturnReports/ItemWisePurchaseReturnSummary.aspx">Item Wise Purchase Return Summary </a>
                        </li>
                        <li data-page="112">
                            <a target="_blank" href="/Reports/PurchaseReturnReports/ItemWisePurchaseReturnGroupByParty.aspx">Item Wise Purchase Return Group By Party </a>
                        </li>
                    </ul>
                </li>
                <li data-page="216">
                    <a target="_blank" href="/Reports/PurchaseReturnReports/OperatorOrSalesmanWisePurchaseReturnReport.aspx">Operator Or Salesman Wise Purchase Return Report
                    </a>
                </li>
                <li data-parent="3" data-parentname="Purchase Return Reports">
                    <a>Party Wise Purchase Return Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="113">
                            <a target="_blank" href="/Reports/PurchaseReturnReports/PartyWisePurchaseReturnSummary.aspx">Party Wise Purchase Return Summary </a>
                        </li>
                        <li data-page="114">
                            <a target="_blank" href="/Reports/PurchaseReturnReports/PartyWiseItemSummaryPurchaseReturn.aspx">Party Wise Item Summary Purchase Return </a>
                        </li>
                        <li data-page="115">
                            <a target="_blank" href="/Reports/PurchaseReturnReports/PartyGroupWisePurchaseReturn.aspx">Party Group Wise Purchase Return </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li data-parent="2" data-parentname="Reports">
            <a>Receipts and Capital Reports <i class="fa arrow"></i></a>
            <ul class="sidebar-nav">
                <li data-page="118">
                    <a target="_blank" href="/Reports/ReceiptandCapitalReports/BankWiseReceiptDetail.aspx">Date Wise Bank Receipt Detail</a>
                </li>
                <li data-page="119">
                    <a target="_blank" href="/Reports/ReceiptandCapitalReports/DateWiseReceiptDetail.aspx">Date Wise Receipt Detail</a>
                </li>
                <li data-page="339">
                    <a target="_blank" href="/Reports/ReceiptandCapitalReports/DateWiseReceiptSummary.aspx">Date Wise Receipt Summary</a>
                </li>
                <li data-page="340">
                    <a target="_blank" href="/Reports/ReceiptandCapitalReports/PartyWiseReceiptDetail.aspx">Party Wise Receipt Detail</a>
                </li>
            </ul>
        </li>
        <li data-parent="2" data-parentname="Reports">
            <a>Receivable or Payable <i class="fa arrow"></i></a>
            <ul class="sidebar-nav">
                <li data-parent="3" data-parentname="Receivable or Payable">
                    <a>Ledger <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="120">
                            <a target="_blank" href="/Reports/ReceivablePayable/PartyWiseLedgerSlim.aspx">Party Wise Ledger Slim </a>
                        </li>
                        <li data-page="121">
                            <a target="_blank" href="/Reports/ReceivablePayable/PartyLedgerMedium.aspx">Party Wise Ledger Medium </a>
                        </li>
                        <li data-page="341">
                            <a target="_blank" href="/Reports/ReceivablePayable/PartyWiseLedgerSummary.aspx">Party Wise Ledger Summary </a>
                        </li>
                    </ul>
                </li>
                <li data-parent="3" data-parentname="Receivable or Payable">
                    <a>Party Balance <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="122">
                            <a target="_blank" href="/Reports/ReceivablePayable/PartyBalanceSummary.aspx">Party Balance Summary </a>
                        </li>
                        <li data-page="123">
                            <a target="_blank" href="/Reports/ReceivablePayable/GroupWisePartyBalanceSummary.aspx">Party Group Wise Balance </a>
                        </li>
                        <li data-page="342">
                            <a target="_blank" href="/Reports/ReceivablePayable/UpToDatePartyBalances.aspx">Up To Date Party Balance </a>
                        </li>
                        <li data-page="343">
                            <a target="_blank" href="/Reports/ReceivablePayable/SaleManWisePartyBalance.aspx">Sale Man Wise Party Balance </a>
                        </li>
                        <li data-page="344">
                            <a target="_blank" href="/Reports/ReceivablePayable/PartyOpeningBalance.aspx">Party Opening Balance </a>
                        </li>
                    </ul>
                </li>
                <li data-parent="3" data-parentname="Receivable or Payable">
                    <a>Agging Report <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="124">
                            <a target="_blank" href="/Reports/ReceivablePayable/AggingReportForAllClients.aspx">Agging Report For All Client </a>
                        </li>
                        <li data-page="125">
                            <a target="_blank" href="/Reports/ReceivablePayable/AggingReportForAllVendors.aspx">Agging Report For All Client </a>
                        </li>
                        <li data-page="126">
                            <a target="_blank" href="/Reports/ReceivablePayable/AggingReportForAllClientPartyGroupWise.aspx">Party Group Wise Agging Report For All Clients </a>
                        </li>
                        <li data-page="127">
                            <a target="_blank" href="/Reports/ReceivablePayable/AggingReportForAllVendorsPartyGroupWise.aspx">Party Group Wise Agging Report For All Vendors </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li data-parent="2" data-parentname="Reports">
            <a>Payments Expence and Drawings <i class="fa arrow"></i></a>
            <ul class="sidebar-nav">
                <li data-page="128">
                    <a target="_blank" href="/Reports/PaymentExpenceDrawings/DateWisePaymentDetail.aspx">Date Wise Payment Detail </a>
                </li>
                <li data-page="134">
                    <a target="_blank" href="/Reports/PaymentExpenceDrawings/DateWisePaymentSummary.aspx">Date Wise Payment Summary </a>
                </li>
                <li data-page="129">
                    <a target="_blank" href="/Reports/PaymentExpenceDrawings/PartyWisePaymenyDetail.aspx">Party Wise Paymeny Detail Cash </a>
                </li>
                <li data-page="130">
                    <a target="_blank" href="/Reports/PaymentExpenceDrawings/PartyWisePaymenyDetailThroughBank.aspx">Party Wise Paymeny Detail Bank </a>
                </li>
                <li data-page="131">
                    <a target="_blank" href="/Reports/PaymentExpenceDrawings/DateWiseBankPaymentsDetail.aspx">Date Wise Bank Payments Detail </a>
                </li>
                <li data-page="132">
                    <a target="_blank" href="/Reports/PaymentExpenceDrawings/PartyWisePaymenyDetailCashAndBank.aspx">Party Wise Paymeny Detail Cash And Bank</a>
                </li>
                <li data-page="133">
                    <a target="_blank" href="/Reports/PaymentExpenceDrawings/DateWiseBankPaymentsSummary.aspx">Date Wise Bank Payments Summary</a>
                </li>

                <li data-page="135">
                    <a target="_blank" href="/Reports/PaymentExpenceDrawings/ExpenceWiseDetail.aspx">Expence Wise Detail </a>
                </li>
                <li data-page="345">
                    <a target="_blank" href="/Reports/PaymentExpenceDrawings/StationWiseExpenceDetail.aspx">Station Wise Expence Detail </a>
                </li>
                <li data-page="346">
                    <a target="_blank" href="/Reports/PaymentExpenceDrawings/DateWiseExpenceDetail.aspx">Date Wise Expence Detail </a>
                </li>
                <li data-page="347">
                    <a target="_blank" href="/Reports/PaymentExpenceDrawings/DateWiseExpenceSummary.aspx">Date Wise Expence Summary </a>
                </li>
            </ul>
        </li>
        <li data-parent="2" data-parentname="Reports">
            <a>Stock Reports <i class="fa arrow"></i></a>
            <ul class="sidebar-nav">
                <li data-page="136">
                    <a target="_blank" href="/Reports/StockReports/AggingStockReport.aspx">Agging Report </a>
                </li>
                <li data-page="137">
                    <a target="_blank" href="/Reports/StockReports/AllOpeningStock.aspx">All Opening Stock </a>
                </li>
                <li data-page="138">
                    <a target="_blank" href="/Reports/StockReports/AllStock.aspx">All Stock </a>
                </li>
                <li data-page="217">
                    <a target="_blank" href="/Reports/StockReports/AllStockBrandAndStationWise.aspx">Brand & Staion Wise Stock </a>
                </li>
                <li data-page="348">
                    <a target="_blank" href="/Reports/StockReports/GroupByItemStock.aspx">Group By Item </a>
                </li>
                <li data-page="139">
                    <a target="_blank" href="/Reports/StockReports/BrandWiseOpeningStock.aspx">Brand Wise Opening Stock </a>
                </li>
                <li data-page="219">
                    <a target="_blank" href="/Reports/StockReports/BrandAndItemWiseStockSummery.aspx">Brand And Item Wise Stock Summery </a>
                </li>
                <li data-page="140">
                    <a target="_blank" href="/Reports/StockReports/BrandWiseStockSummery.aspx">Brand Wise Stock Summery </a>
                </li>
                <li data-page="349">
                    <a target="_blank" href="/Reports/StockReports/BrandWiseStockReport_1.aspx">Brand Wise Stock Report 1 </a>
                </li>
                <li data-page="141">
                    <a target="_blank" href="/Reports/StockReports/CategoryWiseStockReport.aspx">Category Wise Stock </a>
                </li>
                <li data-page="142">
                    <a target="_blank" href="/Reports/StockReports/ItemWiseLedger.aspx">Item Wise Ledger </a>
                </li>
                <li data-page="350">
                    <a target="_blank" href="/Reports/StockReports/ItemWiseLedgerSummary.aspx">Item Wise Ledger Summary </a>
                </li>
                <li data-page="408">
                    <a target="_blank" href="/Reports/StockReports/StationStockReportBrandWise.aspx">Station Stock Report Brand Wise </a>
                </li>
                <li data-page="143">
                    <a target="_blank" href="/Reports/StockReports/StockAtReorderPoint.aspx">Stock At Reorder Point </a>
                </li>
                <li data-page="144">
                    <a target="_blank" href="/Reports/StockReports/StockAtReorderPointBrand.aspx">Stock At Reorder Point Brand </a>
                </li>
                <li data-page="145">
                    <a target="_blank" href="/Reports/StockReports/StockEqualToZero.aspx">Stock Equal To Zero </a>
                </li>
                <li data-page="351">
                    <a target="_blank" href="/Reports/StockReports/StockEqualToZeroBrandWise.aspx">Stock Equal To Zero Brand Wise </a>
                </li>
                <li data-page="146">
                    <a target="_blank" href="/Reports/StockReports/StockLessThenZero.aspx">Stock Less Then Zero </a>
                </li>
                <li data-page="352">
                    <a target="_blank" href="/Reports/StockReports/StockLessThenZero.aspx">Stock Less Then Zero Brand Wise </a>
                </li>
                <li data-page="147">
                    <a target="_blank" href="/Reports/StockReports/TotalStockWithAmount.aspx">Total Stock With Amount </a>
                </li>
                <li data-page="148">
                    <a target="_blank" href="/Reports/StockReports/TotalStockWithOutAmount.aspx">Total Stock With Out Amount </a>
                </li>
                <li data-page="353">
                    <a target="_blank" href="/Reports/StockReports/BrandWiseWithAmount.aspx">Brand Wise With Amount </a>
                </li>
                <li data-page="354">
                    <a target="_blank" href="/Reports/StockReports/ModelWiseStock.aspx">Model Wise Stock </a>
                </li>
                <li data-page="355">
                    <a target="_blank" href="/Reports/StockReports/ProcessedItemReceivingDetail.aspx">Processed Item Receiving Detail </a>
                </li>
                <li data-page="356">
                    <a target="_blank" href="/Reports/StockReports/ProcessedItemReceivingSummary.aspx">Processed Item Receiving Summary </a>
                </li>
            </ul>
        </li>
        <li data-parent="2" data-parentname="Reports">
            <a>General Ledger Reports <i class="fa arrow"></i></a>
            <ul class="sidebar-nav">
                <li data-page="149">
                    <a target="_blank" href="/Reports/GeneralLedger/SingleLedger.aspx">Single Ledger </a>
                </li>
                <li data-page="357">
                    <a target="_blank" href="/Reports/GeneralLedger/GeneralLedger.aspx">General Ledger </a>
                </li>
                <li data-page="150">
                    <a target="_blank" href="/Reports/GeneralLedger/frmGeneralLedgerSearch.aspx">Double Entry Verification </a>
                </li>
                <li data-page="151">
                    <a target="_blank" href="/Reports/GeneralLedger/frmGeneralLedgerSearchText.aspx">GL Search </a>
                </li>
                <li data-page="152">
                    <a target="_blank" href="/Reports/GeneralLedger/frmUpToDateBankBalances.aspx">Bank Balances </a>
                </li>
            </ul>
        </li>
        <li data-parent="2" data-parentname="Reports">
            <a>Incentive Reports <i class="fa arrow"></i></a>
            <ul class="sidebar-nav">
                <li data-parent="3" data-parentname="Incentive Reports">
                    <a>Incentive Received Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="153">
                            <a target="_blank" href="/Reports/Incentive/DateWiseIncentiveReceived.aspx">Date Wise Incentive Received</a>
                        </li>
                        <li data-page="154">
                            <a target="_blank" href="/Reports/Incentive/PartyWiseIncentiveReceived.aspx">Party Wise Incentive Received</a>
                        </li>
                        <li data-page="358">
                            <a target="_blank" href="/Reports/Incentive/AllPartyIncentiveReceivedSummary.aspx">All Party Incentive Received Summary</a>
                        </li>
                    </ul>
                </li>
                <li data-parent="3" data-parentname="Incentive Reports">
                    <a>Incentive Given Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="155">
                            <a target="_blank" href="/Reports/Incentive/DateWiseIncentiveGiven.aspx">Date Wise Incentive Given Reports</a>
                        </li>
                        <li data-page="156">
                            <a target="_blank" href="/Reports/Incentive/PartyWiseIncentiveGiven.aspx">Party Wise Incentive Given</a>
                        </li>
                        <li data-page="363">
                            <a target="_blank" href="/Reports/Incentive/AllPartyIncentiveGivenSummary.aspx">All Party Incentive Given Summary</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li data-parent="2" data-parentname="Reports">
            <a>Rebate Reports <i class="fa arrow"></i></a>
            <ul class="sidebar-nav">
                <li data-parent="3" data-parentname="Rebate Reports">
                    <a>Rebate Income Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="157">
                            <a target="_blank" href="/Reports/Rebate/frmDateWiseRebateReceived.aspx">Date Wise Rebate Income Reports</a>
                        </li>
                        <li data-page="158">
                            <a target="_blank" href="/Reports/Rebate/frmItemWiseRebateReceived.aspx">Item Wise Rebate Income Reports</a>
                        </li>
                        <li data-page="159">
                            <a target="_blank" href="/Reports/Rebate/frmItemAndPartyWiseRebateReceived.aspx">Item and Party Wise Rebate Income Reports</a>
                        </li>
                        <li data-page="160">
                            <a target="_blank" href="/Reports/Rebate/frmPartyWiseRebateReceived.aspx">Party Wise Rebate Income Reports</a>
                        </li>
                        <li data-page="161">
                            <a target="_blank" href="/Reports/Rebate/frmBrandWiseRebateReceivedSummary.aspx">Brand Wise Rebate Income Reports</a>
                        </li>
                    </ul>
                </li>
                <li data-parent="3" data-parentname="Rebate Reports">
                    <a>Rebate Expence Reports <i class="fa arrow"></i></a>
                    <ul class="sidebar-nav">
                        <li data-page="162">
                            <a target="_blank" href="/Reports/Rebate/frmDateWiseRebateGiven.aspx">Date Wise Rebate Expence Reports </a>
                        </li>
                        <li data-page="163">
                            <a target="_blank" href="/Reports/Rebate/frmItemWiseRebateGiven.aspx">Item Wise Rebate Expence Reports</a>
                        </li>
                        <li data-page="164">
                            <a target="_blank" href="/Reports/Rebate/frmItemAndPartyWiseRebateGiven.aspx">Item and Party Wise Rebate Expence Reports</a>
                        </li>
                        <li data-page="165">
                            <a target="_blank" href="/Reports/Rebate/frmPartyWiseRebateGiven.aspx">Party Wise Rebate Expence Reports</a>
                        </li>
                        <li data-page="166">
                            <a target="_blank" href="/Reports/Rebate/frmBrandWiseRebateGivenSummary.aspx">Brand Wise Rebate Expence Reports</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li data-parent="2" data-parentname="Reports">
            <a>Serial Number <i class="fa arrow"></i></a>
            <ul class="sidebar-nav">
                <li data-page="167">
                    <a target="_blank" href="/Reports/SerialNo/ItemWiseSoldSerialNo.aspx">Item Wise Sold </a>
                </li>
                <li data-page="168">
                    <a target="_blank" href="/Reports/SerialNo/ItemPartyWiseSoldSerialNo.aspx">Item Party Wise Sold </a>
                </li>
                <li data-page="169">
                    <a target="_blank" href="/Reports/SerialNo/ItemPurchaseBillWiseSerialNo.aspx">Item Purchase Bill Wise</a>
                </li>
                <li data-page="170">
                    <a target="_blank" href="/Reports/SerialNo/FindIME.aspx">Find IME</a>
                </li>
                <li data-page="171">
                    <a target="_blank" href="/Reports/SerialNo/AllIME.aspx?sold=1">All Sold IME</a>
                </li>
                <li data-page="172">
                    <a target="_blank" href="/Reports/SerialNo/AllIME.aspx?sold=0">All UnSold IME</a>
                </li>
                <li data-page="173">
                    <a target="_blank" href="/Reports/SerialNo/PartyWisePurchaseIME.aspx">Party Wise Purchase IME</a>
                </li>
            </ul>
        </li>
        <li data-parent="2" data-parentname="Reports">
            <a>Cash Book <i class="fa arrow"></i></a>
            <ul class="sidebar-nav">
                <li data-page="360">
                    <a target="_blank" href="/Reports/CashBook/DateWiseCashBookOld1.aspx">Cash Book - Old 1</a>
                </li>
                <li data-page="361">
                    <a target="_blank" href="/Reports/CashBook/DateWiseCashBookOld2.aspx">Cash Book - Old 2</a>
                </li>
                <li data-page="362">
                    <a target="_blank" href="/Reports/CashBook/DateWiseCashBookNew.aspx">Date Wise Cash Book New</a>
                </li>
            </ul>
        </li>
        <li data-parent="2" data-parentname="Reports">
            <a>Profit & Loss <i class="fa arrow"></i></a>
            <ul class="sidebar-nav">
                <li data-page="175">
                    <a target="_blank" href="/Reports/ProfitLoss/ProftiAndLoss1.aspx">Income Statement - 1</a>
                </li>
                <li data-page="176">
                    <a target="_blank" href="/Reports/ProfitLoss/ProftiAndLoss2.aspx">Income Statement - 2</a>
                </li>
            </ul>
        </li>
    </ul>
</li>
<li data-parent="1">
    <a><i class="bm-Transaction"></i>Transactions <i class="fa arrow"></i></a>
    <ul class="sidebar-nav">
        <li data-page="177">
            <a target="_blank" href="/Transactions/PurchaseNewItems.aspx">New Purchase </a>
        </li>
        <li data-page="178">
            <a target="_blank" href="/Transactions/PurchaseReturnNew.aspx">New Purchase Return </a>
        </li>
        <li data-page="179">
            <a target="_blank" href="/Transactions/SaleNewItems.aspx">New Sale </a>
        </li>
        <li data-page="180">
            <a target="_blank" href="/Transactions/SaleReturnNew.aspx">New Sale Return </a>
        </li>
        <li data-page="680">
            <a target="_blank" href="/Transactions/LPReceiving.aspx">LP Receiving  </a>
        </li>
        <li data-parent="2" data-parentname="Transactions">
            <a>Payment To Party <i class="fa arrow"></i></a>
            <ul class="sidebar-nav">
                <li data-page="181">
                    <a target="_blank" href="/Transactions/PaymentToPartyThroughCash.aspx">Through Cash </a>
                </li>
                <li data-page="182">
                    <a target="_blank" href="/Transactions/PaymentToPartyThroughBank.aspx">Through Bank </a>
                </li>
            </ul>
        </li>
        <li data-parent="2" data-parentname="Transactions">
            <a>Receipt From Party <i class="fa arrow"></i></a>
            <ul class="sidebar-nav">
                <li data-page="183">
                    <a target="_blank" href="/Transactions/ReceiptFromPartyThroughCash.aspx">Through Cash </a>
                </li>
                <li data-page="184">
                    <a target="_blank" href="/Transactions/ReceiptFromPartyThroughBank.aspx">Through Bank </a>
                </li>
            </ul>
        </li>
        <li data-parent="2" data-parentname="Transactions">
            <a>Bank Transactions <i class="fa arrow"></i></a>
            <ul class="sidebar-nav">
                <li data-page="185">
                    <a target="_blank" href="/Transactions/DepositInToBank.aspx">Deposit Transaction</a>
                </li>
                <li data-page="186">
                    <a target="_blank" href="/Transactions/WithDrawFromBank.aspx">Withdraw Transaction </a>
                </li>
            </ul>
        </li>
        <li data-page="187">
            <a target="_blank" href="/Transactions/ExpenseEntry.aspx">Expence Entry Transaction</a>
        </li>
        <li data-parent="2" data-parentname="Transactions">
            <a>Vouchers <i class="fa arrow"></i></a>
            <ul class="sidebar-nav">
                <li data-page="451">
                    <a target="_blank" href="/Transactions/CashPaymentVoucher.aspx">Cash Payment Voucher</a>
                </li>
                <li data-page="452">
                    <a target="_blank" href="/Transactions/BankPaymentVoucher.aspx">Bank Payment Voucher </a>
                </li>
                <li data-page="453">
                    <a target="_blank" href="/Transactions/CashReceiveVoucher.aspx">Cash Receive Voucher</a>
                </li>
                <li data-page="454">
                    <a target="_blank" href="/Transactions/BankReceiveVoucher.aspx">Bank Receive Voucher </a>
                </li>
                <li data-page="455">
                    <a target="_blank" href="/Transactions/JournalVoucher.aspx">Journal Voucher</a>
                </li>
            </ul>
        </li>
        
        <li data-parent="2" data-parentname="Transactions">
            <a>Opening Balance <i class="fa arrow"></i></a>
            <ul class="sidebar-nav">
                <li data-page="188">
                    <a target="_blank" href="/Transactions/PartyOpeningBalance.aspx">Party Opening Balance </a>
                </li>
                <li data-page="189">
                    <a target="_blank" href="/Transactions/OpenningInventory.aspx">Inventory Opening Balance </a>
                </li>
                <li data-page="190">
                    <a target="_blank" href="/Transactions/GLOpeningBalance.aspx">GL Opening Balance </a>
                </li>
            </ul>
        </li>
        <li data-parent="2" data-parentname="Transactions">
            <a>Incentive Transaction <i class="fa arrow"></i></a>
            <ul class="sidebar-nav">
                <li data-page="191">
                    <a target="_blank" href="/Transactions/IncentiveReceiving.aspx">Income Transaction</a>
                </li>
                <li data-page="192">
                    <a target="_blank" href="/Transactions/IncentiveReceivingCash.aspx">Income Cash Transaction</a>
                </li>
                <li data-page="193">
                    <a target="_blank" href="/Transactions/IncentiveGiven.aspx">Expence Transaction</a>
                </li>
                <li data-page="194">
                    <a target="_blank" href="/Transactions/IncentiveGivenCash.aspx">Expence Cash Transaction</a>
                </li>
            </ul>
        </li>
        <li data-page="195">
            <a target="_blank" href="/Transactions/Assembling.aspx">Assembling </a>
        </li>
        <li data-page="196">
            <a target="_blank" href="/Transactions/Voucher.aspx">Journal Voucher </a>
        </li>
        <li data-page="197">
            <a target="_blank" href="/Transactions/StockAdjustment.aspx">Stock Adjustment </a>
        </li>
        <li data-page="218">
            <a target="_blank" href="/Transactions/StockMovement.aspx">Stock Movement </a>
        </li>
    </ul>
</li>
<li data-parent="1">
    <a><i class="far fa-edit"></i>Correction <i class="fa arrow"></i></a>
    <ul class="sidebar-nav">
        <li data-parent="2" data-parentname="Correction">
            <a>Payment <i class="fa arrow"></i></a>
            <ul class="sidebar-nav">
                <li data-page="198">
                    <a target="_blank" href="/Correction/PaymentToPartyThroughCashEdit.aspx">Through Cash</a>
                </li>
                <li data-page="199">
                    <a target="_blank" href="/Correction/PaymentToPartyThroughBankEdit.aspx">Through Bank</a>
                </li>
            </ul>
        </li>
        <li data-parent="2" data-parentname="Correction">
            <a>Receipt Edit <i class="fa arrow"></i></a>
            <ul class="sidebar-nav">
                <li data-page="200">
                    <a target="_blank" href="/Correction/ReceiptFromPartyThroughCashEdit.aspx">Through Cash Edit</a>
                </li>
                <li data-page="201">
                    <a target="_blank" href="/Correction/ReceiptFromPartyThroughBankEdit.aspx">Through Bank Edit</a>
                </li>
            </ul>
        </li>
        <li data-page="202">
            <a target="_blank" href="/Correction/ExpenseEntryEdit.aspx">Expence Entry</a>
        </li>
        <li data-parent="2" data-parentname="Correction">
            <a>Bank Transactions Edit <i class="fa arrow"></i></a>
            <ul class="sidebar-nav">
                <li data-page="203">
                    <a target="_blank" href="/Correction/DepositInToBankEdit.aspx">Deposit</a>
                </li>
                <li data-page="204">
                    <a target="_blank" href="/Correction/WithDrawFromBankEdit.aspx">WithDraw</a>
                </li>
            </ul>
        </li>
        <li data-parent="2" data-parentname="Correction">
            <a>Incentive Transaction Edit <i class="fa arrow"></i></a>
            <ul class="sidebar-nav">
                <li data-page="205">
                    <a target="_blank" href="/Correction/IncentiveReceivingEdit.aspx">Edit Income Transaction</a>
                </li>
                <li data-page="206">
                    <a target="_blank" href="/Correction/IncentiveReceivingCashEdit.aspx">Edit Income - Cash Transaction</a>
                </li>
                <li data-page="207">
                    <a target="_blank" href="/Correction/IncentiveGivenEdit.aspx">Edit Expence Transaction</a>
                </li>
                <li data-page="208">
                    <a target="_blank" href="/Correction/IncentiveGivenCashEdit.aspx">Edit Expence - Cash Transaction</a>
                </li>
            </ul>
        </li>
        <li data-page="209">
            <a target="_blank" href="/Correction/PurchaseEditNew.aspx">Edit Purchase </a>
        </li>
        <li data-page="210">
            <a target="_blank" href="/Correction/PurchaseReturnEditNew.aspx">Edit Purchase Return </a>
        </li>
        <li data-page="211">
            <a target="_blank" href="/Correction/SaleEditNew.aspx">Edit Sale </a>
        </li>
        <li data-page="212">
            <a target="_blank" href="/Correction/SaleReturnEditNew.aspx">Edit Sale Return </a>
        </li>
        <li data-page="213">
            <a target="_blank" href="/Correction/PartyRegistrationEdit.aspx">Edit Party Registration </a>
        </li>
        <li data-page="300">
            <a target="_blank" href="/Correction/ItemRegistrationEditNew.aspx">Edit Item Registration </a>
        </li>
        <li data-page="222">
            <a target="_blank" href="/Correction/StockMovementEdit.aspx">Edit Stock Movement </a>
        </li>
        <li data-page="461">
            <a target="_blank" href="/Correction/StockMovementPrevious.aspx">Previous Stock Movements</a>
        </li>
    </ul>
</li>

<li data-parent="1">
    <a><i class="bm-Registration"></i>Other <i class="fa arrow"></i></a>
    <ul class="sidebar-nav">
        <li data-page="400">
            <a target="_blank" href="/Other/UpdateInventoriesBalance.aspx">Update Inventories Balance </a>
        </li>
        <li data-page="401">
            <a target="_blank" href="/Other/UpdatePartiesBalances.aspx">Update Parties Balances </a>
        </li>
        <li data-page="450">
            <a target="_blank" href="/Registration/StationManagment.aspx">Station Management </a>
        </li>
    </ul>
</li>
<li data-parent="1">
    <a><i class="fas fa-cog"></i>Setting <i class="fa arrow"></i></a>
    <ul class="sidebar-nav">
        <li data-page="402">
            <a target="_blank" href="/Settings/EmptyDBAll.aspx">Empty DB All </a>
        </li>
        <li data-page="403">
            <a target="_blank" href="/Settings/DeleteAllItems.aspx">Delete All Items </a>
        </li>
        <li data-page="404">
            <a target="_blank" href="/Settings/DeleteAllParties.aspx">Delete All Parties </a>
        </li>
        <li data-page="409">
            <a target="_blank" href="/Settings/Configurations.aspx">Configurations </a>
        </li>
    </ul>
</li>

<%-- Last Id 462 --%>