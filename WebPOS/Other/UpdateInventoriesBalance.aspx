﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="UpdateInventoriesBalance.aspx.cs" Inherits="WebPOS.Other.UpdateInventoriesBalance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container mb-3">
        <div class="mt-4">

            <section class="form">
                <h2 class="form-header fa-cart">Update Inventories Balances </h2>
                <div class="row">

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mb-4">
                        <label>
                            <input name="stationCheck" value="1" type="radio" class="radio rounded" checked />
                            <span></span>
                            Update for all Stations
                        </label>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mb-4">
                        <label>
                            <input name="stationCheck" value="2" type="radio" class="radio rounded" />
                            <span></span>
                            For Specific Station
                        </label>

                        <select id="StationDD" class="dropdown" data-type="Station">
                        </select>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <input class="pinCode empty1 input__field input__field--hoshi" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Enter Pin</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="save()" class=" btn btn-3 btn-bm btn-3e fa-database w-100"><span>Update Balances</span></a>
                        </span>
                    </div>
                </div>

                <hr />

                <div class="row">
                </div>

            </section>
        </div>

    </div>
    <div class="container">
        <%@ Register Src="~/Registration/_OtherManagment.ascx" TagName="_OtherManagment" TagPrefix="uc" %>
        <uc:_othermanagment id="_OtherManagment1" runat="server" />
    </div>

</asp:Content>

<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    <script src="../Script/Dropdown.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>

    <script type="text/javascript">

        $(document).ready(function () {
            appendAttribute.init("StationDD", "Station", $("#AppStationId").val());
        });

        function save() {
            var pinCode = $(".pinCode").val();
            if (pinCode.trim()) {
                var confirmMsg = 'Yes Update All Stations Inventories Balances!!';
                var url = "/Other/UpdateInventoriesBalance.aspx/UpdateAllStationsBalances";
                var stationId = 0;
                var ajaxData = { pin: pinCode };
                if ($("[name=stationCheck]:checked").val() == "2") {

                    confirmMsg = 'Yes Update Inventories Balances For ' + $("#StationDD .dd-selected-text").text() + ' Station Only!!';
                    stationId = $("[name=StationDD]").val();
                    url = "/Other/UpdateInventoriesBalance.aspx/UpdateSingleStationBalances";
                    ajaxData.stationId = stationId;

                }
                swal({
                    title: 'Update Inventories Balances?',
                    html: 'Please do not close your window while updating, it may take longer.',
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonText: confirmMsg,
                    showLoaderOnConfirm: true,
                    preConfirm: (text) => {
                        return new Promise((resolve) => {
                            $.ajax({
                                url: url,
                                async: true,
                                timeout: 3000000,
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                data: JSON.stringify(ajaxData),
                                success: function (BaseModel) {
                                    if (BaseModel.d.Success) {
                                        $(".pinCode").val("");
                                        //updateInputStyle();
                                        //resolve();
                                        swal("", BaseModel.d.Message, "success");
                                    }
                                    else {
                                        swal("Error", BaseModel.d.Message, "error");
                                    }

                                }
                            });
                        })
                    },
                    allowOutsideClick: () => !swal.isLoading()
                })

            } else {
                swal("Enter Pin Please.", '', 'error');
            }

        }

    </script>
</asp:Content>
