﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using WebPOS.Model;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS.Other
{
    public partial class UpdateInventoriesBalance : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlTransaction tran;



        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel UpdateAllStationsBalances(string pin)
        {

            Module8 objModule8 = new Module8();
            ModQtyOfInventory modQtyOfInventory = new ModQtyOfInventory();

            try
            {
                if (con.State == ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                var itemCmd = new SqlCommand("Select Code from InvCode where CompId='" + CompID + "'", con, tran);
                itemCmd.CommandTimeout = 100000000;
                var itemobjDs = new DataSet();
                SqlDataAdapter itemdAdapter = new SqlDataAdapter(itemCmd);
                if (con.State == ConnectionState.Closed) { con.Open(); }
                itemdAdapter.Fill(itemobjDs);
                var itemData = itemobjDs.Tables[0];
                var selectCmd = new SqlCommand("Select Id from Station where CompId='" + CompID + "'", con, tran);
                selectCmd.CommandTimeout = 100000000;
                var objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter(selectCmd);
                if (con.State == ConnectionState.Closed) { con.Open(); }
                dAdapter.Fill(objDs);
                var data = objDs.Tables[0];
                for (int j = 0; j < itemData.Rows.Count; j++)
                {
                    var itemCode = itemData.Rows[j]["Code"].ToString();
                    modQtyOfInventory.QtyAvailableOne(itemCode, tran, con);
                }
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    var stationId = data.Rows[i]["Id"].ToString();
                    for (int j = 0; j < itemData.Rows.Count; j++)
                    {
                        var itemCode = itemData.Rows[j]["Code"].ToString();
                        modQtyOfInventory.QtyAvailableOneStation(itemCode, stationId, tran, con);
                    }
                }

                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Balances Updated successfully." };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel UpdateSingleStationBalances(string pin, string stationId)
        {

            Module8 objModule8 = new Module8();
            ModQtyOfInventory modQtyOfInventory = new ModQtyOfInventory();

            try
            {
                if (con.State == ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                var itemCmd = new SqlCommand("Select Code from InvCode where CompId='" + CompID + "'", con, tran);
                var itemobjDs = new DataSet();
                SqlDataAdapter itemdAdapter = new SqlDataAdapter(itemCmd);
                if (con.State == ConnectionState.Closed) { con.Open(); }
                itemdAdapter.Fill(itemobjDs);
                var itemData = itemobjDs.Tables[0];
                
                for (int j = 0; j < itemData.Rows.Count; j++)
                {
                    var itemCode = itemData.Rows[j]["Code"].ToString();
                    modQtyOfInventory.QtyAvailableOne(itemCode, tran, con);
                }

                for (int j = 0; j < itemData.Rows.Count; j++)
                {
                    var itemCode = itemData.Rows[j]["Code"].ToString();
                    modQtyOfInventory.QtyAvailableOneStation(itemCode, stationId, tran, con);
                }


                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Balances Updated successfully." };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }
        
    }
}