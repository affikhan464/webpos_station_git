﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="Configurations.aspx.cs" Inherits="WebPOS.Registration.Configurations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container mb-3">
        <div class="mt-4">
            <section class="form">
                <h2 class="form-header ">Configurations</h2>
                <div class="row">
                    <div class="col-12 col-md-4">
                        <label class="d-flex">
                            <input name="SelectLastPurchaseRate" type="checkbox" class="checkbox Configurations" /><span data-toggle="tooltip" title="This will allow selected salesman to select station against item in transaction pages."></span>
                            <i>Select Last Purchase Rate (In Purchase)</i>
                        </label>
                    </div>
                    <div class="col-12 col-md-4">
                        <label class="d-flex">
                            <input name="ShowItemQtyAtZero" type="checkbox" class="checkbox Configurations" /><span data-toggle="tooltip" title="This will show Items At Zero Qty (In Sale Page)."></span>
                            <i>Show Items At Zero Qty (In Sale)</i>
                        </label>
                    </div>
                    <div class="col-12 col-md-4">
                        <label class="d-flex">
                            <label>Invoice Format</label>
                            <select class="form-control" name="InvoiceFormat">
                                <option value="1">
                                    Format 1 (A4 Papper)
                                </option>
                                <option value="11">
                                    Format 11 (Thermal Print)
                                </option>
                            </select>
                        </label>
                    </div>
                </div>
                <div class="row justify-content-end mt-5">

                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="save()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                        </span>
                    </div>
                </div>
            </section>
        </div>

    </div>

    <div class="container">
        <hr />
        <%@ Register Src="~/Registration/_OtherManagment.ascx" TagName="_OtherManagment" TagPrefix="uc" %>
        <uc:_OtherManagment ID="_OtherManagment1" runat="server" />
    </div>

</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    <script src="/Script/Dropdown.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="/Script/Configurations.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
</asp:Content>
