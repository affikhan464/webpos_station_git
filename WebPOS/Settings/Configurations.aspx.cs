﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using WebPOS.Model;
namespace WebPOS.Registration
{
    public partial class Configurations : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(ConfigurationsModel model)
        {

            var isAdmin = HttpContext.Current.Session["UserId"].ToString() == "0";  //0 mean admin
            
            var listPages = HttpContext.Current.Session["UserPermittedPages"] as List<MenuPage>;

            var ids = "";
            if (listPages != null)
            {
                if (listPages.Count > 0)
                {
                    ids = string.Join(",", listPages.Select(a => a.Id));
                }
            }
            
            var isAllowedUser = listPages.Any(a => a.Url.ToLower().Contains("/Configurations.aspx"));
            
            if (isAdmin || isAllowedUser)
            {

                try
                {
                    if (con.State == ConnectionState.Closed) { con.Open(); }
                    tran = con.BeginTransaction();

                    var cmdInsertText = "Update Configuration " +
                        "set ValueNo='" + Convert.ToInt32(model.SelectLastPurchaseRate) + "' " +
                        "where Description='SelectLastPurchaseRate' and CompId='" + CompID + "';";
                    cmdInsertText += "Update Configuration " +
                        "set ValueNo='" + Convert.ToInt32(model.ShowItemQtyAtZero) + "' " +
                        "where Description='ShowItemQtyAtZero' and CompId='" + CompID + "';";  
                    
                    cmdInsertText += "Update Configuration " +
                        "set ValueNo='" + Convert.ToInt32(model.InvoiceFormat) + "' " +
                        "where Description='InvoiceFormat' and CompId='" + CompID + "';";

                     var cmdInsert = new SqlCommand(cmdInsertText, con, tran);
                    cmdInsert.ExecuteNonQuery();



                    tran.Commit();
                    HttpContext.Current.Session["InvoiceFormat"] = Convert.ToInt32(model.InvoiceFormat);
                    con.Close();
                    return new BaseModel() { Success = true, Message = "Configurations updated Successfully" };
                }
                catch (Exception ex)
                {
                    tran.Rollback();
                    con.Close();
                    return new BaseModel() { Success = false, Message = ex.Message };
                }
            }
            else
            {
                return new BaseModel() { Success = false, Message = "You do not have access to do this." };
            }
        }
    }

}