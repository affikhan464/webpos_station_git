﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="SetExpiry.aspx.cs" Inherits="WebPOS.Registration.SetExpiry" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="/css/style.css" type="text/css" />
    <link rel="stylesheet" href="/css/reset.css" type="text/css" />
    <link href="/css/allitems.css?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>" rel="stylesheet" />
    <link href="/css/sweetalert.css?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>" rel="stylesheet" />
    <link href="/css/jquery.datetimepicker.css?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>" rel="stylesheet" />
    <link href="/css/GridStyle.css?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>" rel="stylesheet" />

    <link href="/css/jquery.powertip-light.css?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>" rel="stylesheet" />



    <!-- Place favicon.ico in the root directory -->
    <link rel="stylesheet" href="/css/vendor.min.css">
    <link rel="stylesheet" href="/css/app.css?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>">
    <link rel="icon" href="/images/logoicon1.png" />
    <!-- Theme initialization -->
    <link rel="stylesheet" id="theme-style" href="/css/app-gray.css?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>">
</head>

<body>
    <form id="form1" runat="server">

        <div class="container mb-3">
            <div class="mt-4">
                <section class="form mb-4">
                    <h2 class="form-header ">Add New Expiry</h2>
                    <div class="row">

                        <div class="col-12 col-md-4">
                            <span class="input input--hoshi">
                                <input class="input__field input__field--hoshi Date datetimepicker" type="text" id="expiryDate" name="expiryDate" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Expiry Date </span>
                                </label>
                            </span>
                        </div>

                        <div class="col-12 col-md-4">
                            <span class="input input--hoshi">
                                <input class="input__field input__field--hoshi" type="password" id="pin" name="pin" />
                                <label class="input__label input__label--hoshi">
                                    <span class="input__label-content input__label-content--hoshi">Pin Code </span>
                                </label>
                            </span>
                        </div>
                        <div class="col-12 col-md-4">
                            <span class="input input--hoshi">
                                <a id="btnSave" onclick="save()" class=" btn btn-3 btn-bm btn-3e fa-plus w-100">Add New Expiry</a>
                            </span>
                        </div>
                    </div>
                </section>
                <div class="card sameheight-item items" data-exclude="xs,sm,lg">
                    <div class="card-header bordered">
                        <div class="header-block">
                            <h3 class="title">Previous Expiries </h3>
                        </div>

                    </div>
                    <ul class="item-list striped">
                        <li class="item item-list-header">
                            <div class="item-row">
                                <div class="item-col item-col-header flex-1">
                                    <div>
                                        <span>Sr. No.</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-2">
                                    <div>
                                        <span>Expiry Date</span>
                                    </div>
                                </div>
                                <div class="item-col item-col-header  flex-4">
                                    <div>
                                        <span>Expire In</span>
                                    </div>
                                </div>

                                <div class="item-col item-col-header  flex-3">
                                </div>
                            </div>
                        </li>

                    </ul>
                </div>
            </div>

        </div>
        <audio id="errorMsg" hidden src="/Other/error.mp3" controls="controls"></audio>
        <audio id="successMsg" hidden src="/Other/success.mp3" controls="controls"></audio>
        <script src="/js/vendor.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
        <script src="/js/app.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
        <script src="/Script/jquery-ui.min.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
        <script src="/Script/Vendor/jquery.datetimepicker.js"></script>
        <script src="/Script/InitializeDateTimePicker.js"></script>
        <script src="/Script/Vendor/sweetalert.min.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
        <script src="/js/counter.js"></script>
        <script type="text/javascript">
            var countdown;
            var WPOSUtil = {
                btnBlock: function (btnId) {
                    var text = $(btnId).html();
                    $(btnId).attr("data-btnname", text);
                    $(btnId).addClass("disableClick");
                    $(btnId).html(`Please Wait ... <i class="fas fa-circle-notch fa-spin"></i>`);
                },
                btnRelease: function (btnId) {
                    var text = $(btnId).attr("data-btnname");
                    $(btnId).removeClass("disableClick");
                    $(btnId).html(text);
                }
            }
            $(document).ready(function () {
                initializeDatePicker();
                GetAllStationList();
                
            });
            function startCounter() {

                if ($("[name=validTill]").length > 0) 
                    Countdown.init($("[name=validTill]").val(), ".remainingTimer");

            }
            function GetAllStationList() {

                $.ajax({
                    url: '/WebPOSService.asmx/Expiry_List',
                    type: "Post",
                    success: function (list) {
                        $(".item-list .item-data").remove();
                        for (var i = 0; i < list.length; i++) {
                            var Id = list[i].Id;
                            var ExpiryDateText = list[i].ExpiryDateText;
                            var ExpiryDate = list[i].ExpiryDate;
                            var ExpiryIn =
                                `<div class="d-flex align-items-center">
                                    <span class="svg-icon svg-icon-md svg-icon-primary pr-1 pt-1">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="0" y="0" width="24" height="24"></rect>
                                                <path d="M12,22 C7.02943725,22 3,17.9705627 3,13 C3,8.02943725 7.02943725,4 12,4 C16.9705627,4 21,8.02943725 21,13 C21,17.9705627 16.9705627,22 12,22 Z" fill="#000000" opacity="0.3"></path>
                                                <path d="M11.9630156,7.5 L12.0475062,7.5 C12.3043819,7.5 12.5194647,7.69464724 12.5450248,7.95024814 L13,12.5 L16.2480695,14.3560397 C16.403857,14.4450611 16.5,14.6107328 16.5,14.7901613 L16.5,15 C16.5,15.2109164 16.3290185,15.3818979 16.1181021,15.3818979 C16.0841582,15.3818979 16.0503659,15.3773725 16.0176181,15.3684413 L11.3986612,14.1087258 C11.1672824,14.0456225 11.0132986,13.8271186 11.0316926,13.5879956 L11.4644883,7.96165175 C11.4845267,7.70115317 11.7017474,7.5 11.9630156,7.5 Z" fill="#000000"></path>
                                            </g>
                                        </svg>
                                    </span>
                                    <input type="hidden" value="${ExpiryDate}" name="validTill">
                                    <span class="remainingTimer font-weight-bolder badge badge-pill badge-primary mr-1 font-size-14">
                                        00:00:00
                                    </span>
                                    <span class="badge badge-pill badge-success font-size-14">
                                        Active Expiry
                                    </span>
                                </div>

                            `;
                            if (list[i].IsExpired) {
                                ExpiryIn = `
                                    <span class="badge badge-pill badge-danger font-size-14">
                                        Expired
                                    </span>`;
                            }
                            else if (list[i].IsExpiryClosed) {
                                ExpiryIn = `
                                    <span class="badge badge-pill badge-warning font-size-14">
                                        Expiry Closed
                                    </span>`;
                            }
                            var deleteAttr = '';
                            deleteAttr = ` <i data-code="${Id}"  onclick="deleteItem(this)"  class="fas fa-trash mr-3"></i> `;
                            $(".item-list").append(
                                ` <li class="item item-data">
                                    <div class="item-row">
                                        <div class="item-col flex-1">
                                            <div>${(i + 1)}</div>
                                        </div>
                                        <div class="item-col flex-2">
                                            <div>${ExpiryDateText}</div>
                                        </div>
                                        <div class="item-col flex-4 no-overflow">
                                            ${ExpiryIn}
                                        </div>
                                        <div class="item-col item-col-date flex-3">
                                            <div class="text-right">                                                
                                                ${deleteAttr}
                                            </div>
                                        </div>
                                    </div>
                                </li>`);

                        }
                        if (countdown) {
                            clearInterval(countdown);
                        }
                        startCounter();

                    },
                    fail: function (jqXhr, exception) {

                    }
                });



            }
            function empty1() {
                $(".Name").val("");
                $(".Code").val("");
            }
            function edit(element) {
                empty1();
                var code = element.dataset.brandcode;
                var name = element.dataset.brandname;
                $(".Name").val(name);
                $(".Code").val(code);

                updateInputStyle();
            }


            function save() {
                var expiryDate = $("[name=expiryDate]").val();
                var pin = $("[name=pin]").val();

                if (pin.trim() == "") {
                    swal("Enter Pin Please!!", "", "error");
                    return false;
                }

                WPOSUtil.btnBlock("#btnSave")

                $.ajax({
                    url: "/Default.aspx/ExtendExpiry",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ expiryDate: expiryDate, pin: pin }),
                    success: function (BaseModel) {
                        debugger
                        if (BaseModel.d.Success) {
                            GetAllStationList();
                            initializeDatePicker();
                            $("#pin").val("");
                            WPOSUtil.btnRelease("#btnSave")
                            swal(BaseModel.d.Message, '', "success");
                            if (countdown) {
                                clearInterval(countdown);
                            }
                            startCounter();
                        }
                        else {
                            WPOSUtil.btnRelease("#btnSave")
                            swal("Failed", BaseModel.d.Message, "error");
                        }
                    }

                });



            }


            function deleteItem(element) {
                var id = element.dataset.code;
                

                swal({
                    title: 'Enter Pin to Delete.',
                    type: 'info',
                    input: 'password',
                    showCancelButton: true,
                    confirmButtonText: 'Yes Delete It',
                    showLoaderOnConfirm: true,
                    preConfirm: (text) => {
                        return new Promise((resolve) => {

                            $.ajax({
                                url: "/Default.aspx/DeleteExpiry",
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                data: JSON.stringify({ id: id, pin: text}),
                                success: function (BaseModel) {
                                    debugger
                                    if (BaseModel.d.Success) {
                                        GetAllStationList();
                                        swal(BaseModel.d.Message, '', "success");

                                    }
                                    else {
                                        swal("Failed", BaseModel.d.Message, "error");
                                    }
                                }

                            });
                        })
                    },
                    allowOutsideClick: () => !swal.isLoading()
                })


            }




        </script>
    </form>
</body>
</html>
