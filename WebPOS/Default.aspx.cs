﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using WebPOS.Data;
using WebPOS.Model;
using WebPOS.Security;

namespace WebPOS
{
    public partial class Default : System.Web.UI.Page
    {
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                try
                {
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                        errorMsg.Visible = false;
                    }
                    if (Session["UserId"] != null)
                    {
                        Response.Redirect("~/Home.aspx");
                    }
                }
                catch (Exception ex)
                {
                    errorMsg.Visible = true;
                    errorMsg.Text = "error = " + ex.Message;
                }

            }


        }
        static bool isServer(SqlConnection con)
        {
            Match match = Regex.Match(con.DataSource, @"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}");
            if (match.Success)
            {
                return true;
            }
            return false;
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Login(LoginViewModel loginViewModel)
        {
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                var security = new Security.Security();
                if (!security.isTableExist("Security", con))
                {
                    var isCreated = createSecurityTable();
                    if (!isCreated)
                    {
                        return new BaseModel { Success = false, Message = "Your connection string is not correct." };
                    }
                }


                SqlCommand cmd = new SqlCommand("Select CertificateHash,Id from Security", con);

                var uniqueSystemId = security.GetUUID();

                SqlDataReader reader = cmd.ExecuteReader();


                var toBeDecrypt = new List<ExpiryViewModel>();

                while (reader.Read())
                {
                    var item = new ExpiryViewModel()
                    {
                        HashKey = reader["CertificateHash"].ToString(),
                        Id = Convert.ToInt32(reader["Id"])
                    };
                    toBeDecrypt.Add(item);
                }
                reader.Close();
                var dateAndSystemIds = new List<DateAndSystemId>();
                foreach (var item in toBeDecrypt)
                {
                    var dateAndSystmId = security.DecryptKey(item.HashKey, true).Split('_');
                    var dateAndSystemId = new DateAndSystemId()
                    {
                        ExpiryDate = Convert.ToDateTime(dateAndSystmId[0]),
                        SystemUId = dateAndSystmId[1],
                        Id = item.Id
                    };
                    dateAndSystemIds.Add(dateAndSystemId);
                }

                var dAndSI = dateAndSystemIds.OrderByDescending(a => a.Id).FirstOrDefault(x => x.SystemUId == uniqueSystemId);
                if (dAndSI != null)
                {


                    if (dAndSI.ExpiryDate > DateTime.Now)
                    {
                        SqlCommand userCmd = new SqlCommand(
                            "Select userss.Sno,userss.Name," +
                            " Station.Address as StationAddress ," +
                            " Station.PhoneNumber as StationPhoneNumber ,userss.UserName,Password,userss.StationID," +
                            " Station.STRN,Station.NTN from userss " +
                            "join Station on userss.StationID = Station.ID " +
                            "where userss.CompID='" + CompID + "' and UserName='" + loginViewModel.UserName + "' and StationID=" + loginViewModel.StationId + "", con);
                        SqlDataReader userReader = userCmd.ExecuteReader();
                        var userName = string.Empty;
                        var userId = string.Empty;
                        var password = string.Empty;
                        var name = string.Empty;
                        var StationPhoneNumber = string.Empty;
                        var StationAddress = string.Empty;
                        var NTN = string.Empty;
                        var STRN = string.Empty;

                        var userStationId = 0;

                        while (userReader.Read())
                        {
                            name = userReader["Name"].ToString();
                            userName = userReader["UserName"].ToString();
                            userId = userReader["Sno"].ToString();
                            password = userReader["Password"].ToString();
                            StationPhoneNumber = userReader["StationPhoneNumber"].ToString();
                            StationAddress = userReader["StationAddress"].ToString();
                            userStationId = Convert.ToInt32(userReader["StationID"].ToString());
                            STRN = userReader["STRN"].ToString();
                            NTN = userReader["NTN"].ToString();
                        }
                        userReader.Close();

                        var isAdmin = userName == "0";

                        if (userName == loginViewModel.UserName && password == loginViewModel.Password && (userStationId == Convert.ToInt32(loginViewModel.StationId) || isAdmin))
                        {
                            var userPermittedPages = GetUserPermittedPagesList(userId, loginViewModel.StationId);
                            var userPermissions = GetUserOtherPermissionsList(userId, loginViewModel.StationId);
                            var modItem = new ModItem();
                            var InvoiceFormat = modItem.GetInvoiceFormat(con);
                            HttpContext.Current.Session["UserId"] = userId;
                            HttpContext.Current.Session["IsAdmin"] = isAdmin;
                            HttpContext.Current.Session["UserFullName"] = name;
                            HttpContext.Current.Session["StationId"] = userStationId;
                            HttpContext.Current.Session["StationName"] = loginViewModel.StationName;
                            HttpContext.Current.Session["StationPhoneNumber"] = StationPhoneNumber;
                            HttpContext.Current.Session["StationAddress"] = StationAddress;
                            HttpContext.Current.Session["UserPermittedPages"] = userPermittedPages;
                            HttpContext.Current.Session["UserPermissions"] = userPermissions;
                            HttpContext.Current.Session["STRN"] = STRN;
                            HttpContext.Current.Session["NTN"] = NTN;
                            HttpContext.Current.Session["InvoiceFormat"] = InvoiceFormat;
                            var userTempId = Guid.NewGuid().ToString();
                            var mySession = new AppSession();
                            mySession.Add("UserTempId", userTempId);
                            con.Close();
                            return new BaseModel { Success = true, Message = "", ReturnUrl = HttpContext.Current.Request.QueryString["ReturnUrl"] };

                        }
                        else
                        {
                            con.Close();
                            if (userName != loginViewModel.UserName || password != loginViewModel.Password)
                            {
                                return new BaseModel { Success = false, Message = "Invalid UserName or Password" };
                            }
                            else
                            {
                                return new BaseModel { Success = false, Message = "Select your own station, you are not allowed to enter using this station." };
                            }

                        }
                    }
                    else
                    {
                        con.Close();
                        return new BaseModel { Success = false, Message = @"Your Software has been expired, please call at +923335101642
                                     آپ کا سافٹوئر ایکسپائر ہو چکا ہے، براۓ مہربانی اپر نمبر پر کال کریں۔", IsSoftwareExpired = true };
                    }
                }
                else
                {
                    con.Close();
                    return new BaseModel { Success = false, Message = @"You have no access to this Software please call at +923335101642
                                     آپ اس سافٹوئر کو نہیں استعمال کر سکھتے براۓ مہربانی اپر نمبر پر کال کریں۔", IsSoftwareExpired = true };

                }
            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message, IsSoftwareExpired = false };
            }
            finally
            {
                con.Close();
            }
        }

        public static List<MenuPage> GetUserPermittedPagesList(string userId, string stationId)
        {

            var cmd = new SqlCommand("Select UserMenuPages.PageId,MenuPages.Url,MenuPages.Name from MenuPages join UserMenuPages on MenuPages.Id=UserMenuPages.PageId where  UserMenuPages.UserId='" + userId + "' and UserMenuPages.StationId=" + stationId + " and MenuPages.CompId='" + CompID + "'", con);
            var objDs = new DataSet();
            var dAdapter = new SqlDataAdapter(cmd);

            dAdapter.Fill(objDs);

            var data = objDs.Tables[0];


            var listIds = new List<MenuPage>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string id = data.Rows[i]["PageId"].ToString();
                string Url = data.Rows[i]["Url"].ToString();
                string Name = data.Rows[i]["Name"].ToString();

                var mp = new MenuPage()
                {
                    Id = id,
                    Name = Name,
                    Url = Url,
                    UserId = userId
                };
                listIds.Add(mp);
            }

            return listIds;
        }


        public static UserPermisions GetUserOtherPermissionsList(string userId, string stationId)
        {

            var cmd = new SqlCommand("Select Type,Value from UserPermissions where  UserId='" + userId + "' and StationId=" + stationId + " and CompId='" + CompID + "'", con);
            var objDs = new DataSet();
            var dAdapter = new SqlDataAdapter(cmd);

            dAdapter.Fill(objDs);

            var data = objDs.Tables[0];


            var permision = new UserPermisions();

            for (int i = 0; i < data.Rows.Count; i++)
            {
                var Type = data.Rows[i]["Type"].ToString();
                var Value = (bool)(data.Rows[i]["Value"]);

                if (Type == "AllowedOtherStationsSelect" && Value)
                {
                    permision.AllowedOtherStationsSelect = true;
                }
                if (Type == "UserAllowedToSetSellingPrice" && Value)
                {
                    permision.UserAllowedToSetSellingPrice = true;
                }

            }

            return permision;
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getSecurityInfo()
        {

            try
            {
                if (HttpContext.Current.Request.IsLocal && isServer(con))
                {
                    return new BaseModel { Success = false, Message = "Run application using DefaultLocal.aspx page to login for Server Database.", IsSoftwareExpired = true };
                }
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                var migrations = new CustomMigrations();
                migrations.RunMigration(con);

                var security = new Security.Security();
                if (security.isTableExist("Security", con))
                {

                    SqlCommand cmd = new SqlCommand("Select CertificateHash from Security", con);

                    var uniqueSystemId = security.GetUUID();

                    SqlDataReader reader = cmd.ExecuteReader();


                    var toBeDecrypt = new List<string>();

                    while (reader.Read())
                    {
                        toBeDecrypt.Add(reader["CertificateHash"].ToString());

                    }
                    reader.Close();
                    var dateAndSystemIds = new List<DateAndSystemId>();
                    foreach (var item in toBeDecrypt)
                    {
                        var dateAndSystmId = security.DecryptKey(item, true).Split('_');
                        var dateAndSystemId = new DateAndSystemId()
                        {
                            ExpiryDate = Convert.ToDateTime(dateAndSystmId[0]),
                            SystemUId = dateAndSystmId[1]
                        };
                        dateAndSystemIds.Add(dateAndSystemId);
                    }

                    var dAndSI = dateAndSystemIds.FirstOrDefault(x => x.SystemUId == uniqueSystemId);
                    if (dAndSI != null)
                    {

                        if (dAndSI.ExpiryDate > DateTime.Now)
                        {
                            con.Close();
                            return new BaseModel { Success = true, Message = "", IsSoftwareExpired = false };
                        }
                        else
                        {
                            con.Close();
                            return new BaseModel { Success = false, Message = "", IsSoftwareExpired = true };
                        }
                    }
                    else
                    {

                        con.Close();
                        return new BaseModel { Success = false, Message = "", IsSoftwareExpired = true };
                    }
                }
                else
                {
                    createSecurityTable();
                    con.Close();
                    return new BaseModel { Success = false, Message = "" };
                }

            }
            catch (Exception)
            {
                con.Close();
                return new BaseModel { Success = false, Message = "", IsSoftwareExpired = true };

            }
            finally
            {
                con.Close();
            }



        }
        void login(bool isUserLoggedIn)
        {
            if (isUserLoggedIn)
            {
                Session["User"] = "hellp";
            }

        }

        static public bool createSecurityTable()
        {
            try
            {
                var cmd = new SqlCommand(
                  "CREATE TABLE Security (Id int identity(1,1) NOT NULL,CertificateHash nvarchar(500) NOT NULL)", con);
                cmd.ExecuteNonQuery();
                return true;

            }
            catch (Exception)
            {

                return false;
            }

        }


        /*Encrypt*/
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel ExtendExpiry(string expiryDate, string pin)
        {
            try
            {


                if (pin == "1410164896883" || pin == "3740502400441" || pin == "6110169020747")
                {
                    var security = new Security.Security();
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    SqlCommand cmd = new SqlCommand("Select CertificateHash,Id from Security", con);
                    var uniqueSystemId = security.GetUUID();
                    SqlDataReader reader = cmd.ExecuteReader();
                    var toBeDecrypt = new List<ExpiryViewModel>();
                    while (reader.Read())
                    {
                        var item = new ExpiryViewModel()
                        {
                            HashKey = reader["CertificateHash"].ToString(),
                            Id = Convert.ToInt32(reader["Id"])
                        };
                        toBeDecrypt.Add(item);
                    }
                    reader.Close();
                    var dateAndSystemIds = new List<DateAndSystemId>();
                    foreach (var item in toBeDecrypt)
                    {
                        var dateAndSystmId = security.DecryptKey(item.HashKey, true).Split('_');

                        var dateAndSystemId = new DateAndSystemId()
                        {
                            ExpiryDate = Convert.ToDateTime(dateAndSystmId[0]),
                            SystemUId = dateAndSystmId[1],
                            Id= item.Id
                        };
                        dateAndSystemIds.Add(dateAndSystemId);
                    }

                    var dAndSI = dateAndSystemIds.OrderByDescending(a=>a.Id).FirstOrDefault(x => x.SystemUId == uniqueSystemId);


                    if (dAndSI != null)
                    {
                        var existing = dAndSI.ExpiryDate + "_" + dAndSI.SystemUId;
                        var existingEncryptedHash = security.EncryptKey(existing, true);

                        expiryDate = DateTime.ParseExact(expiryDate + " " + DateTime.Now.ToString("HH:mm:ss"), "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture).ToString();
                        var uniqueIdToEncrypt = expiryDate + "_" + uniqueSystemId;
                        var uniqueIdEncryptedHash = security.EncryptKey(uniqueIdToEncrypt, true);
                        if (con.State == ConnectionState.Closed)
                        {
                            con.Open();
                        }
                        //SqlCommand cmd1 = new SqlCommand("Delete Security where CertificateHash='" + existingEncryptedHash + "'", con);
                        //cmd1.ExecuteNonQuery();
                        SqlCommand cmd2 = new SqlCommand("Insert into Security (CertificateHash) Values('" + uniqueIdEncryptedHash + "')", con);
                        cmd2.ExecuteNonQuery();
                        con.Close();
                        return new BaseModel { Success = true, Message = "Expiry Added Successfully!!", IsSoftwareExpired = false };
                    }
                    else
                    {
                        expiryDate = DateTime.ParseExact(expiryDate + " " + DateTime.Now.ToString("HH:mm:ss"), "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture).ToString();
                        var uniqueIdToEncrypt = expiryDate + "_" + uniqueSystemId;
                        var uniqueIdEncryptedHash = security.EncryptKey(uniqueIdToEncrypt, true);
                        if (con.State == ConnectionState.Closed)
                        {
                            con.Open();
                        }

                        SqlCommand cmd2 = new SqlCommand("Insert into Security (CertificateHash) Values('" + uniqueIdEncryptedHash + "')", con);
                        cmd2.ExecuteNonQuery();
                        con.Close();
                        return new BaseModel { Success = true, Message = "", IsSoftwareExpired = false };
                    }


                }
                else
                {

                    return new BaseModel { Success = false, Message = "Invalid Pin", IsSoftwareExpired = true };
                }
            }
            catch (Exception ex)
            {

                con.Close();
                return new BaseModel { Success = false, Message = ex.Message, IsSoftwareExpired = true };
            }

        }

        /*Encrypt*/
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel DeleteExpiry(int id, string pin)
        {
            try
            {


                if (pin == "1410164896883" || pin == "3740502400441" || pin == "6110169020747")
                {
                   
                        if (con.State == ConnectionState.Closed)
                        {
                            con.Open();
                        }

                        SqlCommand cmd1 = new SqlCommand("Delete Security where Id=" + id, con);
                        cmd1.ExecuteNonQuery();
                        
                        con.Close();
                        return new BaseModel { Success = true, Message = "Expiry Deleted Successfully!!", IsSoftwareExpired = false };
                    
                }
                else
                {

                    return new BaseModel { Success = false, Message = "Invalid Pin", IsSoftwareExpired = true };
                }
            }
            catch (Exception ex)
            {

                con.Close();
                return new BaseModel { Success = false, Message = ex.Message, IsSoftwareExpired = true };
            }

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static void Logout()
        {

            HttpContext.Current.Session["UserId"] = null;
            HttpContext.Current.Session["IsAdmin"] = null;
            HttpContext.Current.Session["UserFullName"] = null;
            HttpContext.Current.Session["StationId"] = null;
            HttpContext.Current.Session["StationName"] = null;
            HttpContext.Current.Session["StationPhoneNumber"] = null;
            HttpContext.Current.Session["StationAddress"] = null;
            HttpContext.Current.Session["UserPermittedPages"] = null;
            HttpContext.Current.Session["STRN"] = null;
            HttpContext.Current.Session["NTN"] = null;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel VerifyUser(string enteredPassword)
        {
            try
            {
                if (HttpContext.Current.Session["UserId"] != null)
                {
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();
                    }
                    var security = new Security.Security();
                    var userId = HttpContext.Current.Session["UserId"].ToString();
                    var userStationId = HttpContext.Current.Session["StationId"].ToString();
                    SqlCommand userCmd = new SqlCommand(
                        "Select" +
                        " userss.UserName,Password" +
                        " from userss " +
                        " where userss.CompID='" + CompID + "' and Sno=" + userId + " and StationID=" + userStationId + "", con);
                    SqlDataReader userReader = userCmd.ExecuteReader();


                    var password = string.Empty;
                    var userName = string.Empty;
                    var StationPhoneNumber = string.Empty;
                    var StationAddress = string.Empty;
                    var NTN = string.Empty;
                    var STRN = string.Empty;


                    while (userReader.Read())
                    {
                        userName = userReader["UserName"].ToString();
                        password = userReader["Password"].ToString();
                    }
                    userReader.Close();


                    if (password == enteredPassword)
                    {
                        var date = DateTime.Now.AddSeconds(15).ToString();
                        var encryptedKey = security.EncryptKey(date, true);
                        con.Close();
                        return new BaseModel { Success = true, Message = "", Data = new { p = encryptedKey } };
                    }
                    else
                    {
                        con.Close();
                        return new BaseModel { Success = false, Message = "Invalid Password!" };

                    }
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "Your session has been expired, login again and continue to Save this page.", LoginAgain = true };
                }


            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message, IsSoftwareExpired = false };
            }
            finally
            {
                con.Close();
            }
        }

    }
}