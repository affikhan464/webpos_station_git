﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace WebPOS.Data
{
    public class CustomMigrations
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        public bool RunMigration(SqlConnection con)
        {
            try
            {
                string cmdtext;
                var filePath = HttpContext.Current.Server.MapPath("~/Data/Migrations.sql");
                var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                {
                    cmdtext = streamReader.ReadToEnd();
                }
                var cmd = new SqlCommand(cmdtext, con);
                cmd.ExecuteNonQuery();
                return true;

            }
            catch (Exception)
            {

                return false;
            }

        }
    }
}