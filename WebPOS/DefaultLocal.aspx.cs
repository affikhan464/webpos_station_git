﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using WebPOS.Data;
using WebPOS.Model;
using WebPOS.Security;

namespace WebPOS
{
    public partial class DefaultLocal : System.Web.UI.Page
    {
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                try
                {
                    if (con.State==ConnectionState.Closed)
                    {
                        con.Open();
                        errorMsg.Visible = false;

                    }
                    if (Session["UserId"] != null)
                    {
                        Response.Redirect("~/Home.aspx");

                    }
                }
                catch (Exception ex)
                {
                    errorMsg.Visible=true;
                    errorMsg.Text="error = " + ex.Message;

                }

            }


        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Login(LoginViewModel loginViewModel)
        {
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
               


                

                    
                        SqlCommand userCmd = new SqlCommand(
                            "Select userss.Sno,userss.Name, " +
                            "userss.UserName,Password from userss " +
                            "where userss.CompID='" + CompID + "' and UserName='" + loginViewModel.UserName + "' ", con);
                        SqlDataReader userReader = userCmd.ExecuteReader();
                        var userName = string.Empty;
                        var userId = string.Empty;
                        var password = string.Empty;
                        var name = string.Empty;

                        while (userReader.Read())
                        {
                            name = userReader["Name"].ToString();
                            userName = userReader["UserName"].ToString();
                            userId = userReader["Sno"].ToString();
                            password = userReader["Password"].ToString();
                        }
                        userReader.Close();

                        var isAdmin = userName == "0";

                        if (userName == loginViewModel.UserName && password == loginViewModel.Password)
                        {
                            var userPermittedPages = GetUserPermittedPagesList(userId);

                            HttpContext.Current.Session["UserId"] = userId;
                            HttpContext.Current.Session["IsAdmin"] = isAdmin;
                            HttpContext.Current.Session["UserFullName"] = name;
                            HttpContext.Current.Session["UserPermittedPages"] = userPermittedPages;
                            var userTempId = Guid.NewGuid().ToString();
                            var mySession = new AppSession();
                            mySession.Add("UserTempId", userTempId);
                            con.Close();
                            return new BaseModel { Success = true, Message = "", ReturnUrl = HttpContext.Current.Request.QueryString["ReturnUrl"] };

                        }
                        else
                        {
                            con.Close();
                            return new BaseModel { Success = false, Message = "Invalid UserName or Password" };
                        }
                   
               
            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message, IsSoftwareExpired = false };
            }
            finally
            {
                con.Close();
            }
        }

        public static List<MenuPage> GetUserPermittedPagesList(string userId)
        {

            var cmd = new SqlCommand("Select UserMenuPages.PageId,MenuPages.Url,MenuPages.Name from MenuPages join UserMenuPages on MenuPages.Id=UserMenuPages.PageId where  UserMenuPages.UserId='" + userId + "'  and MenuPages.CompId='" + CompID + "'", con);
            var objDs = new DataSet();
            var dAdapter = new SqlDataAdapter(cmd);

            dAdapter.Fill(objDs);

            var data = objDs.Tables[0];


            var listIds = new List<MenuPage>();
            for (int i = 0; i < data.Rows.Count; i++)
            {
                string id = data.Rows[i]["PageId"].ToString();
                string Url = data.Rows[i]["Url"].ToString();
                string Name = data.Rows[i]["Name"].ToString();

                var mp = new MenuPage()
                {
                    Id = id,
                    Name = Name,
                    Url = Url,
                    UserId = userId
                };
                listIds.Add(mp);
            }

            return listIds;
        }


       
        void login(bool isUserLoggedIn)
        {
            if (isUserLoggedIn)
            {
                Session["User"] = "hellp";
            }

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static void Logout()
        {

            HttpContext.Current.Session["UserId"] = null;
            HttpContext.Current.Session["IsAdmin"] = null;
            HttpContext.Current.Session["UserFullName"] = null;
            HttpContext.Current.Session["UserPermittedPages"] = null;

        }
    }
}