﻿
$('#attributeModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var modal = $(this)
    emptyField();
    var attributename = button.data('attributename')
    modal.find('.form .brandCheckBoxes').hide();
    updateAttributesModalData(attributename, modal);

    modal.find('.modal-title').text('Add new ' + attributename);
    modal.find('.form').attr('data-saveattribute', attributename);
});

function saveAttribute() {

    var modal = $('#attributeModal');

    var attributename = modal.find('.form').attr('data-saveattribute');
    var AttributeName = $('#attributeModal #attributeModalForm [name=AttributeName]').val();
    var AttributeCode = Number($('#attributeModal #attributeModalForm [name=AttributeCode]').val());
    var Blocked = false;
    var VerifyIME = false;
    if (attributename == "Brand") {

        Blocked = document.getElementById("chkBlocked").checked;
        VerifyIME = document.getElementById("chkVerify").checked;
    }
    var model = {
        AttributeName: AttributeName,
        AttributeCode: AttributeCode,
        Blocked: Blocked,
        VerifyIME: VerifyIME,

    }
    if (AttributeName.trim() == "") {
        smallSwal("Enter " + attributename + " Name", "", "error");

    } else {
        $.ajax({
            url: '/Services/AttributeService.asmx/Save' + attributename,

            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ model: model }),
            success: function (data) {

                if (data.d.Success) {
                    smallSwal("Saved", data.d.Message, "success");
                    updateAttributesModalData(attributename, modal);
                    updateAttributesDropdownData(attributename);
                } else {

                    smallSwal("Error", data.d.Message, "error");

                }


            },
            fail: function (jqXhr, exception) {

            }
        });
    }




}

function updateAttributesModalData(attributename, modal) {
    $("#attributeModal .allAttributes tbody").empty();
    if (attributename == "Godown") {
        GetGoDownList("Modal");
    }
    else if (attributename == "Manufacturer") {
        GetManufacturerList("Modal");
    }
    else if (attributename == "Category") {
        GetCategoryList("Modal");
    }
    else if (attributename == "Packing") {
        GetPackingList("Modal");
    }
    else if (attributename == "Class") {
        GetClassList("Modal");
    }
    else if (attributename == "ItemNature") {
        GetItemNatureList("Modal");
    }
    else if (attributename == "Color") {
        GetColorList("Modal");
    }
    else if (attributename == "BarCodeCategory") {
        GetBarCodeCategoryList("Modal");
    }
    else if (attributename == "Height") {
        HeightList("Modal");
    }
    else if (attributename == "Width") {
        WidthList("Modal");
    }
    else if (attributename == "AccountUnit") {
        AccountUnitList("Modal");
    }
    else if (attributename == "ItemType") {
        ItemTypeList("Modal");
    }
    else if (attributename == "COGS") {
        COGSList("Modal");
    }
    else if (attributename == "Income") {
        Income_List("Modal");
    }
    else if (attributename == "Brand") {

        GetBrandList2("Modal");
        modal.find('.form .brandCheckBoxes').show();
    }

}
function updateAttributesDropdownData(attributename) {

    if (attributename == "Godown") {
        GetGoDownList();
    }
    else if (attributename == "Manufacturer") {
        GetManufacturerList();
    }
    else if (attributename == "Category") {
        GetCategoryList();
    }
    else if (attributename == "Packing") {
        GetPackingList();
    }
    else if (attributename == "Class") {
        GetClassList();
    }
    else if (attributename == "ItemNature") {
        GetItemNatureList();
    }
    else if (attributename == "Color") {
        GetColorList();
    }
    else if (attributename == "BarCodeCategory") {
        GetBarCodeCategoryList();
    }
    else if (attributename == "Height") {
        HeightList();
    }
    else if (attributename == "Width") {
        WidthList();
    }
    else if (attributename == "AccountUnit") {
        AccountUnitList();
    }
    else if (attributename == "ItemType") {
        ItemTypeList();
    }
    else if (attributename == "COGS") {
        COGSList();
    }
    else if (attributename == "Income") {
        Income_List();
    }
    else if (attributename == "Brand") {

        GetBrandList2();
    }

    else if (attributename == "Group") {

        GetGroupList();
    }
}

function Income_List(loadFor) {

    $.ajax({
        url: '/WebPOSService.asmx/InComeList',
        type: "Post",

        //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


        success: function (DataList) {
            if (loadFor == undefined) {
                $("#lstIncomeAccount").empty();
            }
            for (var i = 0; i < DataList.InComeAccount.length; i++) {

                var name = DataList.InComeAccount[i].Name.split("-")[0];
                var code = DataList.InComeAccount[i].Name.split("-")[1];
                if (loadFor == undefined) {

                    if (code == "0103010100001") {

                        $(' <option value="' + code + '"selected>' + name + '</option>').appendTo("#lstIncomeAccount");
                    }
                    else {
                        $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstIncomeAccount");
                    }

                } else {
                    appendRowToModalTable(i, code, name);
                }
            }


        },
        fail: function (jqXhr, exception) {

        }
    });

}


function COGSList(loadFor) {

    $.ajax({
        url: '/WebPOSService.asmx/COGSList',
        type: "Post",

        //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


        success: function (DataList) {

            if (loadFor == undefined) {
                $("#lstCOGS").empty();
            }
            for (var i = 0; i < DataList.COGSAccount.length; i++) {

                var name = DataList.COGSAccount[i].Name.split("*")[0];
                var code = DataList.COGSAccount[i].Name.split("*")[1];
                if (loadFor == undefined) {

                    if (code == "0104010100001") {
                        $(' <option value="' + code + '"selected>' + name + '</option>').appendTo("#lstCOGS");
                    }
                    else {
                        $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstCOGS");
                    }

                } else {
                    appendRowToModalTable(i, code, name);
                }
            }

        },
        fail: function (jqXhr, exception) {

        }
    });

}
function ItemTypeList(loadFor) {

    $.ajax({
        url: '/WebPOSService.asmx/ItemTypeList',
        type: "Post",

        //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


        success: function (DataList) {

            if (loadFor == undefined) {
                $("#lstItemType").empty();
            }
            for (var i = 0; i < DataList.ItemType.length; i++) {

                var name = DataList.ItemType[i].Name.split("*")[0];
                var code = DataList.ItemType[i].Name.split("*")[1];
                if (loadFor == undefined) {

                    if (code == "1") {
                        $(' <option value="' + code + '"selected>' + name + '</option>').appendTo("#lstItemType");
                    }
                    else {
                        $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstItemType");
                    }
                } else {
                    appendRowToModalTable(i, code, name);
                }
            }

        },
        fail: function (jqXhr, exception) {

        }
    });

}
function AccountUnitList(loadFor) {

    $.ajax({
        url: '/WebPOSService.asmx/AccountUnitList',
        type: "Post",

        //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


        success: function (DataList) {

            if (loadFor == undefined) {
                $("#lstUnit").empty();
            }
            for (var i = 0; i < DataList.Unit.length; i++) {

                var name = DataList.Unit[i].Name.split("-")[0];
                var code = DataList.Unit[i].Name.split("-")[1];
                if (loadFor == undefined) {

                    if (code == "1") {
                        $(' <option value="' + code + '" selected>' + name + '</option>').appendTo("#lstUnit");
                    }
                    else {
                        $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstUnit");
                    }
                } else {
                    appendRowToModalTable(i, code, name);
                }
            }

        },
        fail: function (jqXhr, exception) {

        }
    });

}
function GetBrandList2(loadFor) {

    $.ajax({
        url: '/WebPOSService.asmx/Brand_List',
        type: "Post",

        //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


        success: function (DataList) {

            if (loadFor == undefined) {
                $("#lstBrand").empty();
            }
            for (var i = 0; i < DataList.length; i++) {


                var code = DataList[i].Code;
                var name = DataList[i].Name;
                var blocked = DataList[i].Blocked;
                var verifyIME = DataList[i].VerifyIME;
                if (loadFor == undefined) {

                    if (code == "1") {
                        $(' <option value="' + code + '" selected>' + name + '</option>').appendTo("#lstBrand");
                    } else {
                        $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstBrand");
                    }
                } else {
                    appendRowToModalTable(i, code, name, blocked, verifyIME);
                }
            }
        },
        fail: function (jqXhr, exception) {
        }
    });
}

function WidthList(loadFor) {
    $.ajax({
        url: '/WebPOSService.asmx/WidthList',
        type: "Post",

        //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


        success: function (DataList) {

            if (loadFor == undefined) {
                $("#lstWidth").empty();
            }
            for (var i = 0; i < DataList.Width.length; i++) {

                var name = DataList.Width[i].Name.split("*")[0];
                var code = DataList.Width[i].Name.split("*")[1];
                if (loadFor == undefined) {
                    if (code == "1") {

                        $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstWidth");
                    }
                    else {
                        $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstWidth");
                    }
                } else {
                    appendRowToModalTable(i, code, name);
                }
            }

        },
        fail: function (jqXhr, exception) {

        }
    });

}
function HeightList(loadFor) {
    $.ajax({
        url: '/WebPOSService.asmx/HeightList',
        type: "Post",

        //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


        success: function (DataList) {

            if (loadFor == undefined) {
                $("#lstHeight").empty();
            }
            for (var i = 0; i < DataList.Height.length; i++) {

                var name = DataList.Height[i].Name;
                var code = DataList.Height[i].Code;
                if (loadFor == undefined) {

                    if (code == "1") {
                        $(' <option value="' + code + '" selected>' + name + '</option>').appendTo("#lstHeight");
                    }
                    else {
                        $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstHeight");
                    }
                } else {
                    appendRowToModalTable(i, code, name);
                }
            }

        },
        fail: function (jqXhr, exception) {

        }
    });

}
function GetBarCodeCategoryList(loadFor) {
    $.ajax({
        url: '/WebPOSService.asmx/BarCodeCategoryList',
        type: "Post",

        //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


        success: function (DataList) {

            if (loadFor == undefined) {
                $("#lstBarCodeCategory").empty();
            }
            for (var i = 0; i < DataList.BarCodeCategory.length; i++) {

                var name = DataList.BarCodeCategory[i].Name.split("*")[0];
                var code = DataList.BarCodeCategory[i].Name.split("*")[1];
                if (loadFor == undefined) {

                    if (code == 1) {
                        $(' <option value="' + code + '" selected>' + name + '</option>').appendTo("#lstBarCodeCategory");
                    }
                    else {
                        $(' <option value="' + code + '" >' + name + '</option>').appendTo("#lstBarCodeCategory");
                    }
                } else {

                    appendRowToModalTable(i, code, name);
                }
            }

        },
        fail: function (jqXhr, exception) {

        }
    });

}

function GetColorList(loadFor) {

    $.ajax({
        url: '/WebPOSService.asmx/Color_List',
        type: "Post",

        //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


        success: function (DataList) {

            if (loadFor == undefined) {
                $("#lstColor").empty();
            }
            for (var i = 0; i < DataList.length; i++) {

                var code = DataList[i].Code;
                var name = DataList[i].Name;
                if (loadFor == undefined) {

                    if (code == 1) {
                        $(' <option value="' + code + '" selected>' + name + '</option>').appendTo("#lstColor");
                    }
                    else {
                        $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstColor");
                    }
                } else {
                    appendRowToModalTable(i, code, name);
                }
            }

        },
        fail: function (jqXhr, exception) {

        }
    });

}

function GetItemNatureList(loadFor) {
    $.ajax({
        url: '/WebPOSService.asmx/ItemNatureList',
        type: "Post",

        //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


        success: function (DataList) {

            if (loadFor == undefined) {
                $("#lstItemNature").empty();
            }
            for (var i = 0; i < DataList.ItemNature.length; i++) {

                var name = DataList.ItemNature[i].Name.split("-")[0];
                var code = DataList.ItemNature[i].Name.split("-")[1];
                if (loadFor == undefined) {

                    if (code == "1") {
                        $(' <option value="' + code + '" selected>' + name + '</option>').appendTo("#lstItemNature");
                    }
                    else {
                        $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstItemNature");
                    }
                } else {
                    appendRowToModalTable(i, code, name);
                }
            }
        },
        fail: function (jqXhr, exception) {

        }
    });

}

function GetGoDownList(loadFor) {
    $.ajax({
        url: '/WebPOSService.asmx/GoDown_List',
        type: "Post",

        //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


        success: function (DataList) {

            if (loadFor == undefined) {
                $("#lstGoDown").empty();
            }
            for (var i = 0; i < DataList.length; i++) {

                var code = DataList[i].Code;
                var name = DataList[i].Name;
                if (loadFor == undefined) {

                    if (code == "1") {
                        $(' <option value="' + code + '" selected>' + name + '</option>').appendTo("#lstGoDown");
                    }
                    else {
                        $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstGoDown");
                    }
                } else {
                    appendRowToModalTable(i, code, name);
                }
            }

        },
        fail: function (jqXhr, exception) {

        }
    });

}
function GetClassList(loadFor) {
    $.ajax({
        url: '/WebPOSService.asmx/Class_List',
        type: "Post",

        //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


        success: function (DataList) {

            if (loadFor == undefined) {
                $("#lstClass").empty();
            }
            for (var i = 0; i < DataList.length; i++) {

                var name = DataList[i].Name;
                var code = DataList[i].Code;
                if (loadFor == undefined) {

                    if (code == 1) {
                        $(' <option value="' + code + '"selected>' + name + '</option>').appendTo("#lstClass");
                    }
                    else {
                        $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstClass");
                    }
                } else {
                    appendRowToModalTable(i, code, name);
                }
            }

        },
        fail: function (jqXhr, exception) {

        }
    });

}

function GetPackingList(loadFor) {
    $.ajax({
        url: '/WebPOSService.asmx/Packing_List',
        type: "Post",

        //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


        success: function (DataList) {

            if (loadFor == undefined) {
                $("#lstPacking").empty();
            }
            for (var i = 0; i < DataList.length; i++) {

                var name = DataList[i].Name;
                var code = DataList[i].Code;
                if (loadFor == undefined) {

                    if (code == "1") {
                        $(' <option value="' + code + '"selected>' + name + '</option>').appendTo("#lstPacking");
                    }
                    else {
                        $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstPacking");
                    }
                } else {
                    appendRowToModalTable(i, code, name);
                }

            }

        },
        fail: function (jqXhr, exception) {

        }
    });

}
function GetCategoryList(loadFor) {
    $.ajax({
        url: '/WebPOSService.asmx/Category_List',
        type: "Post",

        //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


        success: function (DataList) {


            if (loadFor == undefined) {
                $("#Category_List").empty();
            }
            for (var i = 0; i < DataList.length; i++) {

                var name = DataList[i].Name;
                var code = DataList[i].Code;
                if (loadFor == undefined) {
                    if (code == "1") {
                        $(' <option value="' + code + '"selected>' + name + '</option>').appendTo("#lstCategory");
                    }
                    else {
                        $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstCategory");
                    }
                }
                else {
                    appendRowToModalTable(i, code, name);
                }
            }

        },
        fail: function (jqXhr, exception) {

        }
    });

}
function GetManufacturerList(loadFor) {
    $.ajax({
        url: '/WebPOSService.asmx/Manufacturer_List',
        type: "Post",

        //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


        success: function (DataList) {

            if (loadFor == undefined) {
                $("#lstManufacturer").empty();
            }

            for (var i = 0; i < DataList.length; i++) {



                var code = DataList[i].Code;
                var name = DataList[i].Name;
                if (loadFor == undefined) {

                    if (code == "1") {
                        $(' <option value="' + code + '"selected>' + name + '</option>').appendTo("#lstManufacturer");
                    }
                    else {
                        $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstManufacturer");
                    }
                } else {
                    appendRowToModalTable(i, code, name);
                }
            }

        },
        fail: function (jqXhr, exception) {

        }
    });

}
function GetGroupList(loadFor) {
    $.ajax({
        url: '/WebPOSService.asmx/Group_List',
        type: "Post",

        //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


        success: function (DataList) {

            if (loadFor == undefined) {
                $("#lstGroup").empty();
            }

            for (var i = 0; i < DataList.length; i++) {



                var code = DataList[i].Code;
                var name = DataList[i].Name;
                if (loadFor == undefined) {

                    if (code == "1") {
                        $(' <option value="' + code + '"selected>' + name + '</option>').appendTo("#lstGroup");
                    }
                    else {
                        $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstGroup");
                    }
                } else {
                    appendRowToModalTable(i, code, name);
                }
            }

        },
        fail: function (jqXhr, exception) {

        }
    });

}
function deleteAttr(element) {
    debugger
    emptyField();
    var code = element.dataset.attributecode;
    swal({
        title: 'Are you sure to delete?',
        type: 'question',
        showCancelButton: true,
        confirmButtonText: 'Yes Delete It',
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {
                deleteAttribute(code);
                resolve();
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    })

}

function deleteAttribute( code) {
    var modal = $('#attributeModal');
    var attributename = modal.find('.form').attr('data-saveattribute');
    $.ajax({
        url: '/Services/AttributeService.asmx/Delete' + attributename,

        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ id: code }),
        success: function (data) {

            if (data.d.Success) {
                smallSwal("Deleted", data.d.Message, "success");
                updateAttributesModalData(attributename, modal);
                updateAttributesDropdownData(attributename);
            } else {

                smallSwal("Error", data.d.Message, "error");

            }


        },
        fail: function (jqXhr, exception) {

        }
    });
}

function edit(element) {

    emptyField();
    var code = element.dataset.attributecode;
    var name = element.dataset.attributename;
    var Blocked = element.dataset.brandblocked;
    var Verify = element.dataset.brandverify;

    $(".AttributeName").val(name);
    $(".AttributeCode").val(code);
    if (Blocked != undefined) {

        if (Blocked == 1) {
            document.getElementById("chkBlocked").checked = true;
        } else {
            document.getElementById("chkBlocked").checked = false;
        }
        if (Verify == 1) {
            document.getElementById("chkVerify").checked = true;
        } else {
            document.getElementById("chkVerify").checked = false;
        }
    }
    updateInputStyle();

}
function emptyField() {
    $(".AttributeName").val("");
    $(".AttributeCode").val("");

    document.getElementById("chkBlocked").checked = false;
    document.getElementById("chkVerify").checked = false;


}
function appendRowToModalTable(i, code, name, blocked, verifyIME) {
    var brandAttribute = "";
    if (blocked != undefined) {
        brandAttribute = ' data-brandverify="' + verifyIME + '"  data-brandblocked="' + blocked + '" ';
    }
    $(' <tr><td> ' + (i + 1) + ' </td><td class="text-left">' + code + '</td><td class="text-left">' + name + '</td><td><div class="actionBtns "><i class="fa fa-edit" data-toggle="tooltip" data-placement="left" title="edit" ' + brandAttribute + ' data-attributecode="' + code + '" data-attributename="' + name + '" onclick="edit(this)" ></i><i class="fa fa-trash" title="Delete" data-toggle="tooltip" onclick="deleteAttr(this)"  data-attributecode="' + code + '" data-placement="left"></i></div></td></tr >').appendTo(".allAttributes tbody");
}