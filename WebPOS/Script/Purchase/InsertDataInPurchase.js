﻿function insertDataInSale() {

    var rowCount = $("#myTable tbody tr").length == 0 ? 1 : Number($("#myTable tr:last-child").data().rownumber) + 1;
    var Sno = rowCount; //document.getElementById("txtSNO").value;

    var ItemCode = $(".selectedItemCode").val();
    var ItemName = $(".selectedItemDescription").val();
    var Qty = 1;//Number($(".selectedItemQty").val());
    var Rate = Number($(".selectedItemRate").val());
    Qty = Number(Qty);
    Rate = Number(Rate);
    var Amount = Qty * Rate;
    var ItemDis = 0;
    var PerDis = 0;
    var DealDis = 0;


    if ($("#chkAutoDeal:checked").length > 0) {

        var dealApplyNo = Number($("#txtDealApplyNo").text());
        if (dealApplyNo == 2)
            DealDis = Number($(".selectedItemDealRs2").val());

        else if (dealApplyNo == 3)
            DealDis = Number($(".selectedItemDealRs3").val());

        else
            DealDis = Number($(".selectedItemDealRs").val());
    }

    var revCode = $(".selectedItemRevenueCode").val();
    var cgsCode = $(".selectedItemCGSCode").val();

    var NetAmount = 0; //Amount - ItemDis - DealDis;

    var avrgCost = Number($(".selectedItemAverageCost").val()).toFixed(2);
    var SellingPrice = $(".selectedItemActualSellingPrice").val();
    var PurAmount = 0; //avrgCost * Qty;

    var rows = "";
    //var itemExist = itemAlreadyExist(ItemCode, Rate, PerDis, DealDis);
    //if (itemExist > 0) {
    //    Sno = itemExist;
    //    var qty = Number($("#myTable ." + Sno + "_Qty").val());
    //    $("#myTable ." + Sno + "_Qty").val(qty);
    //    calculationSale();
    //}
    //else {
    insertPRow(Sno, ItemCode, ItemName, Qty, Rate, Amount, ItemDis, PerDis, DealDis, NetAmount, PurAmount, avrgCost, revCode, cgsCode, SellingPrice)
        calculationSale();
    //}
    $("#myTable ." + Sno + "_Qty").focus();
    $("#myTable ." + Sno + "_Qty").select();
    rerenderSerialNumber();
    updateDisPer();
    var stationId = $("#AppStationId").val();
     appendAttribute.init(Sno + "_StationId", "Station", stationId);
}

$(document).on("click", "#myTable .fas.fa-times", function () {

    swal({
        title: 'Are you sure?',
        type: 'question',
        showCancelButton: true,
        confirmButtonText: 'Remove',
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {
                this.parentElement.parentElement.remove();
                var itemCode = this.dataset.itemcode;
                var itemRowNumber = $(this).parents("tr").data("rownumber");
                removeIMEs(itemCode, itemRowNumber);
                calculationSale();
                rerenderIMEITableSerialNumber();
                rerenderSerialNumber();
                resolve();
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    })


});

$(document).on("click", "#IMEITable .fas.fa-times", function () {


    swal({
        title: 'Are you sure?',
        type: 'question',
        showCancelButton: true,
        confirmButtonText: 'Remove',
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {
                this.parentElement.parentElement.remove();
                rerenderIMEITableSerialNumber();
                resolve();
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    })

});
$("#myTable").keydown(function (e) {

    var A_Key = 65;
    if (e.which === A_Key && e.ctrlKey) {
        e.preventDefault();

        var rowNumber = $(e.target).parents("tr").data().rownumber;
        copyItem(rowNumber);
    }
});


function copyItem(rowNumber) {

    var lastRowNumber = $("#myTable tr:last-child").data().rownumber;
    var selectedRow = $("#myTable tr[data-rownumber=" + rowNumber + "]");
    var ItemCode = selectedRow.find("[name=Code]").val();
    var ItemName = selectedRow.find("[name=ItemName]").val();
    var Rate = selectedRow.find("[name=Rate]").val();
    var avrgCost = selectedRow.find("." + rowNumber + "_AverageCost").val();
    var revCode = selectedRow.find("." + rowNumber + "_RevCode").val();
    var cgsCode = selectedRow.find("." + rowNumber + "_CgsCode").val();
    var Qty = selectedRow.find("[name=Qty]").val();
    var Amount = selectedRow.find("[name=Amount]").val();
    var NetAmount = selectedRow.find("[name=NetAmount]").val();
    var PurAmount = selectedRow.find("[name=PurAmount]").val();
    var PerDis = selectedRow.find("[name=PerDis]").val();
    var ItemDis = selectedRow.find("[name=ItemDis]").val();
    var DealDis = selectedRow.find("[name=DealDis]").val();
    var SellingPrice = selectedRow.find("[name=SellingPrice]").val();
    var StationId = selectedRow.find("[name=" + rowNumber + "_StationId]").val();

    var Sno = Number(lastRowNumber) + 1;
    insertPRow(Sno, ItemCode, ItemName, Qty, Rate, Amount, ItemDis, PerDis, DealDis, NetAmount, PurAmount, avrgCost, revCode, cgsCode, SellingPrice)

    $("#myTable tr:last-child").find("[name=Qty]").select().focus();
    rerenderSerialNumber();
     appendAttribute.init(Sno + "_StationId", "Station", StationId);

}
function removeIMEs(itemCode, itemRowNumber) {
    $(".imei-table table tr[data-itemrownumber=" + itemRowNumber + "][data-itemcode=" + itemCode + "]").remove();
}
function itemAlreadyExist(code, rate, disPer, dealDis) {
    var rowNo = $("#myTable [data-itemcode=" + code + "][data-itemrate=" + rate + "][data-itemdisper=" + disPer + "][data-itemdealdis=" + dealDis + "]").data("rownumber");
    return rowNo;
}
function appendRowInIMEI(element, insertIntoTableAsWell) {
    insertIntoTableAsWell = insertIntoTableAsWell == undefined ? false : insertIntoTableAsWell;
    var ItemCode;
    if (insertIntoTableAsWell == false) {
        $("#IMEIRow [name=ImeiTxbx]").val("").select().focus();
        var ItemCode = $(element).parents("tr").find("input[name=Code]").val();

    } else {
        var ItemCode = element.dataset.itemcode;
    }

    $(".IMEPortion").fadeIn();

    $.ajax({
        url: '/WebPOSService.asmx/IsItemIMEVerifiedAgainstItemCode',
        type: "Post",
        data: { "itemCode": ItemCode },
        success: function (isChecked) {

            $('[name=isIMEVerifiedChkbx]').prop('checked', isChecked);
            var rowNumber = element.dataset.rownumber;
            var rowInputs = $("#myTable #Row_" + rowNumber + " input");
            rowInputs.each(function (index, input) {

                var name = $(input).attr("name");
                var value = $(input).val();
                if (name == "Qty") {
                    $("#IMEIRow [name=" + name + "]").val(1);
                } else {
                    $("#IMEIRow [name=" + name + "]").val(value);
                }


            });
            $("#IMEIRow  [name=ItemRowNumber]").val(rowNumber);
            //Scanned through Barcode Reader
            if (insertIntoTableAsWell) {
                inserIMEItem();
                $("#txtBarCode").focus().select();
            } else {
                scrollTo("#IMEIRow [name=ImeiTxbx]");

            }
        },
        fail: function (jqXhr, exception) {
        }
    });



}
function rerenderIMEITableSerialNumber() {
    var rows = $("#IMEITable tbody tr .SNo");
    var sno = 0;
    rows.each(function (index, element) {
        sno += 1;
        $(element).html("<input type='text' value='" + sno + "'  />");

    })
}

function rerenderSerialNumber() {
    var rows = $("#myTable tbody tr .SNo");
    var sno = 0;
    rows.each(function (index, element) {
        sno += 1;
        $(element).html("<input type='text' value='" + sno + "'  />");
    })
}

$(document).on("keydown", "[name=ImeiTxbx]", function (e) {

    if (e.keyCode == 13) {
        inserIMEItem();
        var itemCode = $("#IMEIRow [name=Code]").val();
        isQuantityAvailableLocal(itemCode);
    }

});
function inserIMEItem() {
    var imei = $("#IMEIRow [name=ImeiTxbx]").val();
    var itemCode = $("#IMEIRow [name=Code]").val();
    var itemRowNumber = $("#IMEIRow [name=ItemRowNumber]").val();
    if (!isDuplicateIMEI(imei) && isQuantityAvailableLocal(itemCode)) {

        $.ajax({
            async: false,
            url: '/WebPOSService.asmx/IsIMELengthLegalForPurchase',
            type: "Post",
            data: { "imeCode": imei },
            success: function (model) {

                if (model.isLengthCorrect) {
                  
                    insertIMEDataInGrid(itemRowNumber, $("#IMEIRow [name=Code]").val(), $("#IMEIRow [name=ItemName]").val(), imei, $("#IMEIRow [name=Qty]").val()
                        , $("#IMEIRow [name=Rate]").val(), $("#IMEIRow [name=Amount]").val(),
                        $("#IMEIRow [name=ItemDis]").val(), $("#IMEIRow [name=PerDis]").val(), $("#IMEIRow [name=DealDis]").val(),
                        $("#IMEIRow [name=NetAmount]").val(), $("#IMEIRow [name=PurAmount]").val());
                } else {
                    swal("Error", "IME length is not correct !!", "error");
                    $("#IMEIRow [name=ImeiTxbx]").val("");
                   
                    playError();

                    return false;
                }

            },
            fail: function (jqXhr, exception) {
                debugger;
            }
        });

    }
    else {
        playError();
        swal("IMEI Already Exist Or Quantity Exceeded", '', 'error');
        //$("#IMEIRow input").val("");
        

    }

}
function isQuantityAvailableLocal(code) {
    var rows = $("#myTable  [value=" + code + "][name=Code]");
    var qtyInMainTable = 0;
    rows.each(function (index, element) {
        var rownumber = $(element).parents("tr").data().rownumber;
        qtyInMainTable += Number($("#myTable  ." + rownumber + "_Qty").val());

    });

    var qtysInIMETable = $("#IMEITable [data-code=" + code + "]");
    var sumqtys = 0;
    qtysInIMETable.each(function (index, element) {
        var qty = Number(element.value);
        sumqtys += qty;
    });

    if (sumqtys == qtyInMainTable) {
        $("#IMEIRow input").val("");
    }
    return sumqtys < qtyInMainTable;

}
function insertIMEDataInGrid(itemRowNumber, Code, ItemName, imei, Qty, Rate, Amount, ItemDis, PerDis, DealDis, NetAmount, PurAmount ) {

    var rowCount = $("#IMEITable td").closest("tr").length == 0 ? 1 : $("#IMEITable td").closest("tr").length + 1;
    var Sno = rowCount;

    var rows = createIMERow(itemRowNumber, Sno, Code, ItemName, imei, Qty, Rate, Amount, ItemDis, PerDis, DealDis, NetAmount, PurAmount);

    var lastChildofTableCode = $("#IMEITable tbody tr:last-child").length > 0 ? $("#IMEITable tbody tr:last-child").data().itemcode : null;
    var lastChildofTableItemRowNumber = $("#IMEITable tbody tr:last-child").length > 0 ? $("#IMEITable tbody tr:last-child").data().itemrownumber : null;

    if ((lastChildofTableCode == Code && lastChildofTableItemRowNumber == itemRowNumber) || lastChildofTableCode == null) {
        $(rows).appendTo("#IMEITable tbody");
    } else {
        var rowOfSpecifiedItemCode = $("#IMEITable tbody tr[data-itemrownumber=" + itemRowNumber + "][data-itemcode=" + Code + "]");
        var isChildOfSpecifiedCodeAvailable = rowOfSpecifiedItemCode.length > 0;
        if (isChildOfSpecifiedCodeAvailable) {
            var lastChildOfSpecificCodeId = "#IMEITable #" + $("#IMEITable tbody tr[data-itemrownumber=" + itemRowNumber + "][data-itemcode=" + Code + "]").last().attr("id");
            $(rows).insertAfter(lastChildOfSpecificCodeId);
        } else {
            $(rows).appendTo("#IMEITable tbody");
        }


    }

    $("#IMEIRow [name=ImeiTxbx]").val("");
    //$("#IMEIRow input").val("");
    $("#txtBarCode").val("").select().focus();
    calculationSale();
    rerenderIMEITableSerialNumber();
    playSuccess();
    return true;
}
function isDuplicateIMEI(IMEI) {
    var isImeiAlreardyExist = $("#IMEITable [value=" + IMEI + "].IMEI").length > 0;

    return isImeiAlreardyExist;
}
function isQuantityAvailable(code) {
    var rows = $("#myTable  [value=" + code + "][name=Code]");
    var qtyInMainTable = 0;
    rows.each(function (index, element) {
        var rownumber = $(element).parents("tr").data().rownumber;
        qtyInMainTable += Number($("#myTable  ." + rownumber + "_Qty").val());

    });

    var qtysInIMETable = $("#IMEITable [data-code=" + code + "]");
    var sumqtys = 0;
    qtysInIMETable.each(function (index, element) {
        var qty = Number(element.value);
        sumqtys += qty;
    });

    return sumqtys < qtyInMainTable;

}

function updateIMETable(itemCode, rowNumber) {

    var Rate = $("#myTable ." + rowNumber + "_Rate").val();
    var PerDis = $("#myTable ." + rowNumber + "_PerDis").val();
    var DealDis = $("#myTable ." + rowNumber + "_DealDis").val();
    var ItemDis = $("#myTable ." + rowNumber + "_ItemDis").val();
    var Amount = $("#myTable ." + rowNumber + "_Amount").val();
    var NetAmount = $("#myTable ." + rowNumber + "_NetAmount").val();
    var PurAmount = $("#myTable ." + rowNumber + "_PurAmount").val();

    var rows = $("#IMEITable [data-itemrownumber=" + rowNumber + "][data-itemcode=" + itemCode + "]");

    rows.each(function (index, element) {

        $(element).find("[name=Rate]").val(Rate);
        $(element).find("[name=PerDis]").val(PerDis);
        $(element).find("[name=DealDis]").val(DealDis);
        $(element).find("[name=ItemDis]").val(ItemDis);
        $(element).find("[name=Amount]").val(Amount);
        $(element).find("[name=NetAmount]").val(NetAmount);
        $(element).find("[name=PurAmount]").val(PurAmount);

    });

}

function updateRowRateDisDeal(rowNumber) {
    var Rate = $("#myTable ." + rowNumber + "_Rate").val();
    var PerDis = $("#myTable ." + rowNumber + "_PerDis").val();
    var DealDis = $("#myTable ." + rowNumber + "_DealDis").val();
    $("#myTable [data-rownumber=" + rowNumber + "]").data('itemrate', Rate)
        .data('itemdisper', PerDis).data('itemdealdis', DealDis)
        .attr('data-itemrate', Rate).attr('data-itemdisper', PerDis).attr('data-itemdealdis', DealDis);
}

function reArrangeIMETabeRows() {
    var items = $("#myTable tr");
    var newArrangedRows;
    items.each(function (index, element) {
        var rowNumber = element.dataset.rownumber;
        var itemCode = element.dataset.itemcode;
        var IMEIRows = $(`#IMEITable tr[data-itemrownumber=${rowNumber}][data-itemcode=${itemCode}]`);
        IMEIRows.each(function (i, e) {
            newArrangedRows += e.outerHTML
        });

    });

    $(`#IMEITable tbody`).html(newArrangedRows);
    calculationSale();
    rerenderIMEITableSerialNumber();
}
function createIMERow(itemRowNumber, Sno, ItemCode, ItemName, IME, Qty, Rate, Amount, ItemDis, PerDis, DealDis, NetAmount, PurAmount) {
    var row = `
    <tr  data-itemrownumber='${itemRowNumber}' data-itemcode='${ItemCode}' id='Row_${Sno}' data-rownumber='${Sno}'>
        <td class='SNo three  '>
            ${Sno}
        </td>
        <td class='four name'>
            <input class='${Sno}_Code' name='Code' type='text' value='${ItemCode}' />
        </td>
        <td class='name fifteen '>
            <input class='${Sno}_ItemName' name='ItemName' type='text' value='${ItemName}' />
        </td>
        <td class='thirteen'>
            <input class='${Sno}_IMEI IMEI' name='IMEI' type='text' value='${IME}' />
        </td>
        <td class='five'>
            <input class='${Sno}_Qty calcu' data-code='${ItemCode}' name='Qty' type='text' value='${Qty}' />
        </td>
        <td class='eight'>
            <input name='Rate' class='${Sno}_Rate calcu' type='text' value='${Rate}' /></td>
        <td class='eight'>
            <input class='${Sno}_Amount' name='Amount' type='text' value='${Amount}' />
        </td>
        <td class='eight'>
            <input class='${Sno}_ItemDis' type='text' value='${ItemDis}' name='ItemDis' />
        </td>
        <td class='six'>
            <input name='PerDis' class='${Sno}_PerDis calcu' type='text' value='${PerDis}' />
        </td>
        <td class='eight'>
            <input name='DealDis' class='${Sno}_DealDis' type='text' value='${DealDis}' />
        </td>
        <td class='eight'>
            <input class='${Sno}_NetAmount' type='text' value='${NetAmount}' name='NetAmount' />
        </td>
        <td class='eight'>
            <input class='${Sno}_PurAmount' type='text' value='${PurAmount}' name='PurAmount' />
        </td>
        <td class='six actionBtn text-center'><i class='fas fa-times'></i> </td>
    </tr>`;
    return row;
}

function insertPRow(Sno, ItemCode, ItemName, Qty, Rate, Amount, ItemDis, PerDis, DealDis, NetAmount, PurAmount, avrgCost, revCode, cgsCode, SellingPrice) {
    
    var rows = `
        <tr  class='d-flex pl-1' id='Row_${ Sno}' data-itemcode='${ItemCode}' data-itemrate='${Rate}' data-itemdisper='${PerDis}' data-itemdealdis='${DealDis}' data-rownumber='${Sno}'>
            <td class='SNo three flex-1'><input type='text' value='${Sno}'  /></td>
            <td  class='three flex-2 name' ><input  class='${Sno}_Code' name='Code' type='text' value='${ItemCode}'  /></td>
            <td class='name fourteen flex-6'><input  class='${Sno}_ItemName ' name='ItemName' type='text' value='${ItemName}'  /></td>
            <td class='three flex-1'><input  class='${Sno}_Qty calcu' name='Qty' type='text' value='${Qty}'  /></td>
            <td class='three flex-3'><input name='Rate'  class='${Sno}_Rate calcu'  type='text' value='${Rate}' /></td>
            <td class='three flex-3'><input class='${Sno}_Amount' name='Amount'  type='text' value='${Amount}' /></td>
            <td class='three flex-1'><input class='${Sno}_ItemDis' type='text' value='${ItemDis}' name='ItemDis' /></td>
            <td class='three flex-1'><input  name='PerDis'  class='${Sno}_PerDis calcu' type='text' value='${PerDis}' /></td>
            <td class='three flex-1'><input name='DealDis'  class='${Sno}_DealDis' type='text' value='${DealDis}' /></td>
            <td class='three flex-3'><input class='${Sno}_NetAmount' type='text' value='${NetAmount}' name='NetAmount' /></td>
            <td class='three flex-3'><input class='${Sno}_SellingPrice' type='text' value='${SellingPrice}' name='SellingPrice' /></td>
            <td class='three flex-3'><input class='${Sno}_PurAmount' type='text' value='${PurAmount}' name='PurAmount' /></td>
            <td class='eight flex-3 text-left  overflow-visible'><select id='${Sno}_StationId' class='dropdown form-control'><option value='0'>Select</option></select></td>            
            <td class='three flex-3 actionBtn text-center'>
	    	    <i data-itemcode='${ItemCode}' class='fas fa-times'></i> 
		        <i data-rownumber='${Sno}' class='fas fa-barcode IMETrigerBtn' onclick='appendRowInIMEI(this)'></i> 
	        </td>
            <td hidden> <input hidden='hidden' class='${Sno}_AverageCost' value='${avrgCost}' /></td>   
            <td hidden> <input hidden='hidden' class='${Sno}_RevCode' value='${revCode}' /></td>
            <td hidden> <input hidden='hidden' class='${Sno}_CgsCode' value='${cgsCode}' /></td>  
        </tr>`;
    $(rows).appendTo("#myTable tbody");
    $(".selected").remove();
}
