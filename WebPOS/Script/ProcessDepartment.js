﻿function insertVoucherRow() {
    var rowNumber = $(".itemsSection .itemRow").length > 0 ? $(".itemsSection .itemRow").last().data().rownumber + 1 : 1;
    var type = $("[name=process]:checked").val(); //Issued or Received?
    var disabled = $("[name=process]:checked").val() == "Issued" ? "disabled" : "";
    $("<div class='itemRow' data-type='" + $("[name=process]:checked").val() + "' data-rownumber='" + rowNumber + "'>" +

        "<div class='five text-align-center'><span class='serialNumber '></span></div>" +
        "<div class='sixteen name'> <input name='Code'  id='ItemCode_" + rowNumber + "' disabled  /></div>" +
        "<div class='sixteen name'> <input name='Description'  id='ItemDescription_" + rowNumber + "' disabled   /></div>" +
        "<div class='sixteen'>      <input   type='number'  name='" + type+"Qty' id='ItemQty_" + rowNumber + "'   /></div>" +
        "<div class='sixteen'>      <input  type='number'   name='" + type + "Rate' id='ItemRate_" + rowNumber + "' " + disabled+"  /></div>" +
        "<div class='sixteen'>      <input  type='number'   name='" + type + "Amount' id='ItemAmount_" + rowNumber + "'  disabled /></div>" +
        

        "<div class='float-right'><i class='fas fa-times'></i></div>" +

         "</div>").appendTo("."+ $("[name=process]:checked").val() + ".BMtable .itemsSection");
    



    var selectedPartyProperties = $("[class*=selected" + dataType + "]");
    for (var a = 0; a < selectedPartyProperties.length; a++) {

        $("[id=" + selectedPartyProperties[a].className.split(' ')[0].replace("selected", "") + "_" + rowNumber + "]").val(selectedPartyProperties[a].value.trim());

    }
    assignSerialNumber(type);
    $("#ItemQty_" + rowNumber + "").focus();
    $("#ItemQty_" + rowNumber + "").select();
   

    var Qty = Number($("." + type+" #ItemQty_" + rowNumber + "").val());
    var Rate = Number($("." + type +" #ItemRate_" + rowNumber + "").val());
    $("." + type +" #ItemAmount_" + rowNumber + "").val(Qty * Rate);

    sum(type);
}

function save() {


    var itemsReceived = $(".Received .itemsSection .itemRow");
    var itemsIssued = $(".Issued .itemsSection .itemRow");

    if (itemsReceived.length > 0 && itemsIssued.length > 0) {
        swal({
            title: "Business Manager",
            text: "Are You Sure to save a transaction?",
            type: 'question',
            showCancelButton: true,
            onfirmButtonText: "Yes, Save it!",
            cancelButtonText: "No, cancel please!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {

                    resolve();
                    saveVoucher();
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });

  
    }
    else {
        swal("Invalid Voucher", "The Voucher is either empty, or there is missing Issued or Received Item!", "error");
    }
}

function saveVoucher() {


    var addedReceivedRows = [];
    itemsReceived.each(function (index, element) {

        var row = $(element);
        var item = {
            Code: row.find("[name=Code]").val(),
            Description: row.find("[name=Description]").val(),
            Qty: row.find("[name=ReceivedQty]").val().trim() == "" ? "0" : row.find("[name=ReceivedQty]").val(),
            Rate: row.find("[name=ReceivedRate]").val().trim() == "" ? "0" : row.find("[name=ReceivedRate]").val(),
            Amount: row.find("[name=ReceivedAmount]").val().trim() == "" ? "0" : row.find("[name=ReceivedAmount]").val()
        }
        addedReceivedRows.push(item);
    });


    var addedIssuedRows = [];
    itemsIssued.each(function (index, element) {

        var row = $(element);
        var item = {
            Code: row.find("[name=Code]").val(),
            Description: row.find("[name=Description]").val(),
            Qty: row.find("[name=IssuedQty]").val().trim() == "" ? "0" : row.find("[name=IssuedQty]").val(),
            Rate: row.find("[name=IssuedRate]").val().trim() == "" ? "0" : row.find("[name=IssuedRate]").val(),
            Amount: row.find("[name=IssuedAmount]").val().trim() == "" ? "0" : row.find("[name=IssuedAmount]").val()
        }
        addedIssuedRows.push(item);
    });


    var voucherModel = {
        Received: addedReceivedRows,
        Issued: addedIssuedRows,
        Narration: $(".Narration").val(),
        Date: $("#VoucherDate").val()
    };
    $.ajax({
        url: "/Transactions/ProcessDepartment.aspx/Save",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ model: voucherModel }),
        success: function (response) {
            if (response.d.Success) {
                swal("Success", response.d.Message, "success");
            }
            else {
                swal("Error", response.d.Message, "error");
            }
        }
    })
}
$(document).on("keydown", "[name='IssuedQty']", function (e) {
   
    if (e.keyCode == 13 ) {
        $('#SearchBox').focus();
    }


});
$(document).on("keydown", "[name='ReceivedQty']", function (e) {
    var row = this.id.split("_")[1];

    if (e.keyCode == 13) {
        $(".Received #ItemRate_" + row + "").focus();
        $(".Received #ItemRate_" + row + "").select();
    }


});
$(document).on("keydown", "[name='ReceivedRate']", function (e) {
    var row = this.id.split("_")[1];

    if (e.keyCode == 13) {
        $('#SearchBox').focus();
    }


});

$(document).on("input", "[name='ReceivedQty'],[name='ReceivedRate']", function (e) {
    var rowNumber = this.id.split("_")[1];
    var Qty = Number($(".Received #ItemQty_" + rowNumber + "").val());
    var Rate = Number($(".Received #ItemRate_" + rowNumber + "").val());
    $(".Received #ItemAmount_" + rowNumber + "").val(Qty * Rate);

    sum('Received');

}); 



$(document).on("input", "[name='IssuedQty']", function (e) {
    var rowNumber = this.id.split("_")[1];
    var Qty = Number($(".Issued #ItemQty_" + rowNumber + "").val());
    var Rate = Number($(".Issued #ItemRate_" + rowNumber + "").val());
    $(".Issued #ItemAmount_" + rowNumber + "").val(Qty * Rate);
    sum("Issued");

});
function assignSerialNumber(type) {
    var serialNumbers = $("." + type+" .serialNumber");

    serialNumbers.each(function (index, element) {
        $(element).text(index + 1);
    })

}
function sum(type) {
    var name = ["" + type + "Qty", "" + type + "Rate", "" + type +"Amount"];
   
    for (var i = 0; i < name.length; i++) {
        var sumValue = 0;
   
        var values = $("[name=" + name[i] + "]");
    values.each(function (index, element) {
        var value = $(element).val().trim() == "" ? 0 : $(element).val();
        sumValue += Number(value);
    });

    $("[name=total" + name[i] + "]").val(sumValue);
    }
    
}


$(document).on("click", "#saveBtn", function () {
    save();
});

$(document).on("keydown", function (e) {


    if (e.keyCode == 13 && e.keyCode == 16) {
        save();
    }


})
$(document).on("click", ".reports .fa-times", function () {
    var row = $(this).parents(".itemRow");
    row.remove();
    sum("Issued");
    sum("Received");
    assignSerialNumber("Issued");
    assignSerialNumber("Received");
});

function removeAutoAddedRows() {
    var row = $(".itemsSection .itemRow.autoAdded");
    row.remove();
}
$(document).on("click", "#auto", function () {
    loadIngredients();
});

function loadIngredients() {
    var Items = $(".Received .itemsSection .itemRow");
    removeAutoAddedRows();
    Items.each(function (index, itemRow) {
        var ItemCode = $(itemRow).find("[name=Code]").val();
        var receivedQty = Number($(itemRow).find("[name=ReceivedQty]").val());
       

        var type = "Issued";
         $.ajax({
                url: "/WebPOSService.asmx/GetIngredients",
                type: "POST",
                data: { mainItemCOde: ItemCode },
                success: function (response) {
                    if (response.length > 0) {
                        for (var i = 0; i < response.length; i++) {

                            var rowNumber = $(".Issued .itemsSection .itemRow").length > 0 ? $(".Issued  .itemsSection .itemRow").last().data().rownumber + 1 : 1;
                            var Qty = receivedQty * Number(response[i].Qty );
                            var Rate = Number(response[i].Rate);
                            var Amount = Qty * Rate
                            $("<div class='itemRow autoAdded' data-rownumber='" + rowNumber + "'>" +

                                "<div class='five text-align-center'><span class='serialNumber '>" + rowNumber + "</span></div>" +
                                "<div class='sixteen name'><input name='Code'  id='IngredientCode_" + rowNumber + "' disabled value='" + response[i].Code + "' /></div>" +
                                "<div class='sixteen name'><input name='Description'  id='IngredientDescription_" + rowNumber + "' disabled value='" + response[i].Description + "'   /></div>" +
                                "<div class='sixteen'><input type='number' name='IssuedQty' id='ItemQty_" + rowNumber + "' value='" + Qty + "'  /></div>" +
                                "<div class='sixteen'><input type='number' name='IssuedRate' type='text' id='ItemRate_" + rowNumber + "' disabled value='" + Rate + "'  /></div>" +
                                "<div class='sixteen'><input type='number' name='IssuedAmount' type='text' id='ItemAmount_" + rowNumber + "' disabled value='" + Amount + "'  /></div>" +

                                "<div class='float-right'><i class='fas fa-times'></i></div>" +

                                "</div>").appendTo(".Issued.BMtable .itemsSection");
                            
                        }
                        sum(type);
                    }

                }
            })
    })
    
   


}