﻿function DealDisApplyOnAllitems() {

    if ($("#chkAutoDeal:checked").length > 0) {
        updateDealRs();
    } else {
        $("[name=DealDis]").val(0);
        calculationSale();
    }
}

function PerDisApplyOnAllitems() {

    if ($("#chkAutoPerDis:checked").length > 0) {
        updateDisPer();
    } else {
        $("[name=PerDis]").val(0);
        calculationSale();
    }

}


function updateDisPer() {

    var PartyCode = document.getElementById("txtPartyCode").value;

    var dealApplyNo = Number($("#txtDealApplyNo").text());
    var itemCodes = "";
    var items = $("#myTable tr [name=Code]");
    if (items.length > 0) {

        items.each(function (i, element) {
            itemCodes += $(element).val() + ",";
        });
        console.log(itemCodes);
        if ($("#chkAutoPerDis:checked").length > 0) {
            $.ajax({
                url: '/WebPOSService.asmx/GetItemsDiscountPer',
                type: "Post",
                data: { "partyCode": PartyCode, "itemCodes": itemCodes },
                success: function (items) {
                    for (var i = 0; i < items.length; i++) {

                        if ($("#chkAutoPerDis:checked").length > 0) {

                            var code = items[i].Code;
                            var perDis = items[i].PerDis;
                            $("#myTable [data-itemcode=" + code + "] [name=PerDis]").val(perDis);

                            calculationSale();
                        }
                    }


                },
                fail: function (jqXhr, exception) {
                    debugger;
                }
            });
        }
    }
}
function updateDealRs() {
    var dealApplyNo = Number($("#txtDealApplyNo").text());
    var itemCodes = "";
    var items = $("#myTable tr [name=Code]");
    if (items.length > 0) {

        items.each(function (i, element) {
            itemCodes += $(element).val() + ",";
        });
        console.log(itemCodes);
        if ($("#chkAutoDeal:checked").length > 0) {
            $.ajax({
                url: '/WebPOSService.asmx/GetItemsDealRS',
                type: "Post",
                data: { "dealApplyNo": dealApplyNo, "itemCodes": itemCodes },
                success: function (items) {
                    for (var i = 0; i < items.length; i++) {
                        if ($("#chkAutoDeal:checked").length > 0) {

                            var code = items[i].Code;
                            var qty = Number($("#myTable [data-itemcode=" + code + "] [name=Qty]").val());
                            var dealRs = qty * Number(items[i].DealDis);
                            $("#myTable [data-itemcode=" + code + "] [name=DealDis]").val(dealRs);
                        }
                        calculationSale();
                    }


                },
                fail: function (jqXhr, exception) {
                    debugger;
                }
            });
        }
    }
}
$(document).on("keypress", "#myTable input", function (e) {

    var txbx = this;
    var txbxName = $(txbx).attr("name");
    var sno = $(txbx).parents("tr").data().rownumber;
    var ItemCode = $("#myTable ." + sno + "_Code").val();
    if (e.which == 13) {
        e.preventDefault();
        if (txbxName == "Qty") {
            $("#myTable ." + sno + "_Rate").focus();
            $("#myTable ." + sno + "_Rate").select();
        }
        else if (txbxName == "Rate") {
            $("#myTable ." + sno + "_PerDis").focus();
            $("#myTable ." + sno + "_PerDis").select();
        }
        else if (txbxName == "PerDis") {
            $("#myTable ." + sno + "_DealDis").focus();
            $("#myTable ." + sno + "_DealDis").select();
        }
        else if (txbxName == "DealDis") {

            $(".SearchBox").focus();
            $(".SearchBox").val("");
        }

        calculationSale();
        updateIMETable(ItemCode, sno);
        calculationSale();
    }



    else {
        return true;
    }
});

$(document).on("input", "#myTable input", function (e) {

    var txbx = this;
    var txbxName = $(txbx).attr("name");
    var sno = $(txbx).parents("tr").data().rownumber;
    var ItemCode = $("#myTable ." + sno + "_Code").val();

    calculationSale();
    updateIMETable(ItemCode, sno);
    calculationSale();
    updateRowRateDisDeal(sno);
});

function calculationSale(txbx) {
    
    var tableRows = $("#myTable tbody tr");
    var rowCount = tableRows.length;

    var Total = 0;
    var TotQty = 0;
    var TotDealRs = 0;
    var TotItemDis = 0;
    var FlatDisPerRate = 0
    //var TotPurchase = 0;
    tableRows.each(function (index, element) {
        var sr = $(element).data().rownumber;
        var Qty = Number($(element).find("." + sr + "_Qty").val());
        var Rate = Number($(element).find("." + sr + "_Rate").val());

        var PerDiscount = Number($(element).find("." + sr + "_PerDis").val());
        var DealRs = Number($(element).find("." + sr + "_DealDis").val());
        var NetAmount = Number($(element).find("." + sr + "_NetAmount").val());
        var ItemPurchase = Number($(element).find("." + sr + "_PurAmount").val());

        Qty = Number(Qty);
        Rate = Number(Rate);

        PerDiscount = Number(PerDiscount);
        DealRs = Number(DealRs);
        NetAmount = Number(NetAmount);
        ItemPurchase = Number(ItemPurchase);

        //DealRs = (Number(DealRs) * Number(Qty));
        //element.children[8].children[0].value = DealRs;
        sno = sr;

        Amount = (Qty * Rate).toFixed(2);

        $("#myTable ." + sno + "_Amount").val(Amount);



        $("#myTable ." + sno + "_ItemDis").val(0);
        var ItemDis = 0;
        if (PerDiscount > 0 && Amount > 0) {
            ItemDis = Number(((Amount * PerDiscount) / 100).toFixed(2));
            $("#myTable ." + sno + "_ItemDis").val(ItemDis);
        }

        var ItemDiscount = Number(ItemDis);

        NetAmount = (Amount - (ItemDis + DealRs)).toFixed(2);

        $("#myTable ." + sno + "_NetAmount").val(NetAmount);

        var averageCost = Number($("#myTable ." + sno + "_AverageCost").val());

        if (averageCost == 0 || isNaN(averageCost)) {
            averageCost = Rate;
        }
        var PurAmount = (averageCost * Qty).toFixed(2);



        Total = (Number(Total) + Number(Amount)).toFixed(2);
        TotQty = (Number(TotQty) + Number(Qty)).toFixed(2);
        TotDealRs = (Number(TotDealRs) + Number(DealRs)).toFixed(2);
        TotItemDis = (Number(TotItemDis) + Number(ItemDiscount)).toFixed(2);

        Amount = 0;
        NetAmount = 0;
        Qty = 0;
        DealRs = 0;
        ItemDiscount = 0;
        PerDiscount = 0;
        ItemPurchase = 0
    });
    calculateIMETable();
    var FlatDisPerRate = document.getElementById("txtFlatDiscountPer").value;

    //------------------------Total Grid
    var sum;
    var inputs = ["Qty", 'Amount', 'ItemDis', 'DealDis', 'NetAmount', 'PurAmount'];
    for (var i = 0; i < inputs.length; i++) {

        var currentInput = inputs[i];
        sum = 0;
        $("#myTable input[name = '" + currentInput + "']").each(function () {

            if (this.value.trim() === "") {
                sum = (Number(sum) + Number(0));
            } else {
                sum = (Number(sum) + Number(this.value));

            }

        });



        $("#mainTable [name=" + currentInput + "Total]").val('');
        $("#mainTable [name=" + currentInput + "Total]").val((sum).toFixed(2));


    }

    for (var i = 0; i < inputs.length; i++) {

        var currentInput = inputs[i];
        sum = 0;
        $(".IMEPortion input[name = '" + currentInput + "']").each(function () {

            if (this.value.trim() === "") {
                sum = (Number(sum) + Number(0));
            } else {
                sum = (Number(sum) + Number(this.value));

            }

        });



        $(".IMEPortion [name=" + currentInput + "Total]").val('');
        $(".IMEPortion [name=" + currentInput + "Total]").val((sum).toFixed(2));


    }
    //----------------------End Total Grid-----------------

    if (txbx != undefined && txbx.id == "txtFlatDiscount") {
        if (Total == 0) {
            document.getElementById("txtFlatDiscountPer").value = 0;
        } else {
            document.getElementById("txtFlatDiscountPer").value = ((100 * txbx.value) / Total).toFixed(2);
        }
    } else {
        if (FlatDisPerRate > 0 && Total > 0) {
            document.getElementById("txtFlatDiscount").value = (Total * FlatDisPerRate / 100).toFixed(2);
        } else {
            document.getElementById("txtFlatDiscount").value = (0).toFixed(2);
        }
    }
    //alert(FlatDisPerRate);
    document.getElementById("txtTotal").value = Number(Total).toFixed(2);
    document.getElementById("txtTotPercentageDis").value = Number(TotItemDis).toFixed(2);
    document.getElementById("txtTotDeal").value = Number(TotDealRs).toFixed(2);
    var FlatDiscount = Number(document.getElementById("txtFlatDiscount").value);
    var NetDiscount = 0;
    NetDiscount = (Number(TotItemDis) + Number(TotDealRs) + Number(FlatDiscount)).toFixed(2);
    document.getElementById("txtTotalDiscount").value = NetDiscount;
    var BilTotal = (Number(Total) - Number(NetDiscount)).toFixed(2);
    document.getElementById("txtBillTotal").value = BilTotal;
    var PrevBalance = Number(document.getElementById("txtPreviousBalance").value).toFixed(2);
    var Paid = Number(document.getElementById("txtPaid").value).toFixed(2);
    document.getElementById("txtBalance").value = (Number(PrevBalance) + Number(BilTotal) - Number(Paid)).toFixed(2);

    calculationActualCost("#myTable");
    calculationActualCost("#IMEITable");
    updateInputStyle();
}
function calculateIMETable() {
    var tableRows = $("#IMEITable tbody tr");
    tableRows.each(function (index, element) {
        var sr = $(element).data().rownumber;
        var Qty = Number($(element).find("." + sr + "_Qty").val());
        var Rate = Number($(element).find("." + sr + "_Rate").val());

        var PerDiscount = Number($(element).find("." + sr + "_PerDis").val());
        var DealRs = Number($(element).find("." + sr + "_DealDis").val());
        var NetAmount = Number($(element).find("." + sr + "_NetAmount").val());
        var ItemPurchase = Number($(element).find("." + sr + "_PurAmount").val());

        Qty = Number(Qty);
        Rate = Number(Rate);

        PerDiscount = Number(PerDiscount);
        DealRs = Number(DealRs);
        NetAmount = Number(NetAmount);
        ItemPurchase = Number(ItemPurchase);

        //DealRs = (Number(DealRs) * Number(Qty));
        //element.children[8].children[0].value = DealRs;
        sno = sr;

        Amount = (Qty * Rate).toFixed(2);

        $("#IMEITable ." + sno + "_Amount").val(Amount);




        $("#IMEITable ." + sno + "_ItemDis").val(0);
        var ItemDis = 0;
        if (PerDiscount > 0 && Amount > 0) {
            ItemDis = Number(((Amount * PerDiscount) / 100).toFixed(2));

            $("#IMEITable ." + sno + "_ItemDis").val(ItemDis);
        }

        var ItemDiscount = Number(ItemDis);


        NetAmount = (Amount - (ItemDis + DealRs)).toFixed(2);

        $("#IMEITable ." + sno + "_NetAmount").val(NetAmount);

        var averageCost = Number($("#IMEITable ." + sno + "_AverageCost").val());

        if (averageCost == 0 || isNaN(averageCost)) {
            averageCost = Rate;
        }


    });
}
function calculationActualCost(table) {

    var tableRows = $(table + " tbody tr");
    var rowCount = tableRows.length;
    tableRows.each(function (index, element) {
        var sr = $(element).data().rownumber;
        var Qty = Number($(element).find("." + sr + "_Qty").val());
        var Rate = Number($(element).find("." + sr + "_Rate").val());

        var ItemDiscount = Number($(element).find("." + sr + "_ItemDis").val());
        var DealRs = Number($(element).find("." + sr + "_DealDis").val());
        Amount = (Qty * Rate).toFixed(2);

        var PurAmount = 0;
        PurAmount = Rate;
        var ShareOFOtherCharges = 0;
        var ShareOFFlatDiscount = 0;
        var OtherChares = 0;
        var Total = 0;
        Total = Number($(".GrossTotal").val());

        var FlatDiscount = Number($(".FlatDiscount").val());

        ItemDiscount = (ItemDiscount + DealRs);



        if (ItemDiscount > 0 && Qty > 0) {
            PurAmount = (ItemDiscount / Qty) - Rate;
        }
        PurAmount = Math.abs(PurAmount);



        if (Amount > 0 && Total > 0) {
            ShareOFOtherCharges = (Amount / Total) * 100;
            ShareOFFlatDiscount = (Amount / Total) * 100;
        }
        ShareOFOtherCharges = (ShareOFOtherCharges * OtherChares) / 100;
        ShareOFFlatDiscount = (ShareOFFlatDiscount * FlatDiscount) / 100;

        // alert(ShareOFOtherCharges);

        ShareOFFlatDiscount = ShareOFFlatDiscount / Qty;
        PurAmount = ShareOFFlatDiscount - PurAmount;

        sno = sr;
        $(table + " ." + sno + "_PurAmount").val((Math.abs(PurAmount)).toFixed(2));
    });
}
