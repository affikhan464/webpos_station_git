﻿function SavePartyBefore() {
    var NorBalance = 0;
    NorBalance = document.getElementById("RegClient").checked ? "1" : "2";
    var PartyName = $(".PartyForm .RegPartyName").val();
    if (NorBalance == 0 && PartyName == "") {
        if (NorBalance = 0) {
            swal("Failed", "Define Nature", "error");
        }
        else
            swal("Failed", "Enter Party Name", "error");
    }
    else if (PartyName == "") {
        
        swal("Enter Party Name", "Enter a Party Name Please.", "error");
    }
    else saveParty();
}
function saveParty() {
    var NorBalance = document.getElementById("RegClient").checked ? "1" : "2";
    var PartyName = $(".PartyForm .RegPartyName").val();
    var ContactPerson = $(".PartyForm .RegContactPerson").val();
    var MobileNo = $(".PartyForm .RegMobileNo").val();
    var FaxNo = $(".PartyForm .RegFaxNo").val();
    var LandLineNo = $(".PartyForm .RegLandLineNo").val();
    var Address = $(".PartyForm .RegAddress").val();
    var Email = $(".PartyForm .RegEmail").val();
    var lstSellingPriceNo = $(".PartyForm #lstSellingPriceNo").val();
    var lstDealApplyNo = $(".PartyForm #lstDealApplyNo").val();
    var CreditLimit = $(".PartyForm #txtCreditLimit").val();
    var chkCreditLimitApply = document.getElementById("chkCreditLimitApply").checked ? "1" : "0";
    var OtherInfo = $(".PartyForm .RegOtherInfo").val();
    var lstSaleMan = $(".PartyForm #lstSaleMan").val();
    var chkDeal = document.getElementById("chkDeal").checked ? "1" : "0";
    var chkPerDiscount = document.getElementById("chkPerDiscount").checked ? "1" : "0";


    if (CreditLimit == "") { CreditLimit = 0 }
    CreditLimit = isNaN(CreditLimit) ? 0 : CreditLimit;

    var PartyModel = {
        NorBalance: NorBalance,
        PartyName: PartyName,
        ContactPerson: ContactPerson,
        MobileNo: MobileNo,
        FaxNo: FaxNo,
        LandLineNo: LandLineNo,
        Address: Address,
        Email: Email,
        lstSellingPriceNo: lstSellingPriceNo,
        lstDealApplyNo: lstDealApplyNo,
        CreditLimit: CreditLimit,
        chkCreditLimitApply: chkCreditLimitApply,
        OtherInfo: OtherInfo,
        lstSaleMan: lstSaleMan,
        chkDeal: chkDeal,
        chkPerDiscount: chkPerDiscount
    }
    debugger
    $.ajax({
        url: "/Registration/PartyRegistration.aspx/Save",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ PartyModel: PartyModel }),
        success: function (BaseModel) {
            debugger
            if (BaseModel.d.Success) {
                $(".PartyForm .empty1").val("");
                swal("Registered", BaseModel.d.Message, "success");
            }
            else {
                swal("Error", BaseModel.d.Message, "error");
            }


        }

    });

}
$(document).ready(function () {
    GetSaleManList();
    GetSellingPriceList();
    GetDealApplyNoList();
});

function GetSaleManList() {
    $.ajax({
        url: '/WebPOSService.asmx/SaleMan_List',
        type: "Post",
        success: function (BrandList) {
            for (var i = 0; i < BrandList.length; i++) {
                var code = BrandList[i].Code;
                var name = BrandList[i].Name;
                $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstSaleMan");
            }
        },
        fail: function (jqXhr, exception) {
        }
    });
}
function GetSellingPriceList() {

    for (var i = 1; i < 6; i++) {
        var code = i;
        var name = i;
        $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstSellingPriceNo");

    }
}
function GetDealApplyNoList() {

    for (var i = 1; i < 4; i++) {
        var code = i;
        var name = i;
        $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstDealApplyNo");
    }
}