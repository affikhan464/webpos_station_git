﻿$(document).ready(function () {
    
    var url = window.location.pathname.split("/")[window.location.pathname.split("/").length - 1];

    //if it is Sale, Purchase, Sale Return or Purchase Return page.... Not Edit Page... then Select Default Party
    if (url.indexOf("SaleNewItems") > -1 || url.indexOf("PurchaseNewItems") > -1) { SelectDefaultParty(); }

 
    selectDefaultSalesMan();
    
});
function SelectDefaultParty() {
    $.ajax({
        url: '/WebPOSService.asmx/GetPartyByCode',
        type: "Post",
        data: { "partyCode": $(".defaultPartyCode").val() },
        success: function (data) {

            if (data.length > 0) {
                $("#PartySearchBox").val(data[0].Name);
                $("#clientCode").val(data[0].Code);
                $("#clientCode").val(data[0].Code);
                $("#clientBalance").val(Number(data[0].Balance).toFixed(2));
                $(".clientBalance").text(Number(data[0].Balance).toFixed(2));
                $("#clientAddress").val(data[0].Address);
                $("[name=PartyPhoneNumber]").val(data[0].PhoneNumber);
                $("[name=PartyOtherInfo]").val(data[0].OtherInfo);
            }



        }, fail: function (jqXhr, exception) {

        }
    });
}
function selectDefaultSalesMan() {
    $.ajax({
        url: '/WebPOSService.asmx/GetSalesManName',
        type: "Post",
        success: function (data) {
            $($("#salesManDD .dd-options").children()[0]).remove();


            for (var i = 0; i < data.length; i++) {
                if (data[i].Code == "1") {
                    $("<li class='defaultSalesman' onclick='salesmanSelection(this)'><a class='dd-option dd-option-selected'> <input class='dd-option-value' type='hidden' value='" + data[i].Code + "'> <label class='dd-option-text'>" + data[i].Name + "</label></a></li>").appendTo("#salesManDD ul");
                    $(".defaultSalesman").trigger("click");
                } else {
                    $("<li onclick='salesmanSelection(this)'><a class='dd-option'> <input class='dd-option-value' type='hidden' value='" + data[i].Code + "'> <label class='dd-option-text'>" + data[i].Name + "</label></a></li>").appendTo("#salesManDD ul");

                }

            }
            //salesmanSelection(defaultSalesMan);
            $("#salesManDD,#salesManDD .dd-select").addClass("selectDiv ManSelectDiv");
            $("#salesManDD .dd-select").css("width", "100%");
        }, fail: function (jqXhr, exception) {

        }
    });

    $('#salesManDD').ddslick({
        width: '100%'
    });

}
//Sales Man Selection 
function salesmanSelection(element) {
    if (element != undefined) {
        var value = $($(element.children).children()[0]).attr("value");
        var text = $($(element.children).children()[1]).text();
        $("#salesManDD .dd-selected").text(text);
        $("#salesManDD .dd-selected-value").val(value);
        $("#salesManDD .dd-option").removeClass("dd-option-selected");
        $(element.children).addClass("dd-option-selected");
        $("#salesManDD .dd-options").css("display", "none");
        $("#salesManDD .dd-pointer").removeClass("dd-pointer-up");
        $(".selectedSalesman").remove();
        $("<input type='hidden' class='selectedSalesman' value='" + value + "' />").appendTo("body");
    }

}