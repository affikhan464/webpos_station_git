﻿$(document).ready(function () {
     appendAttribute.init("DDLStation", "Station", 1);
    AppendLevelParents();
    GetAllUsersList(1);

    var rows = $("ul.pages-list .item-row");
    var uls = $("ul.pages-list ul");
    uls.slideUp("slow");
    rows.each(function (i, element) {
        if ($(element).find("~ ul").length) {
            $(element).addClass("isParent");
        }

    });
});
$(document).on("click", ".isParent", function (e) {

    if (!$(this).hasClass("show")) {
        $(this).addClass("show");
        $(this).find("~ ul").slideDown("fast");
    } else {
        $(this).removeClass("show");
        $(this).find("~ ul").slideUp("fast");
    }
    e.stopPropagation();
})
function selectedChanged(selectedValue) {
    GetAllUsersList(selectedValue.value);
}
function AppendLevelParents() {
    var level1Parents = $("[data-parent=1]");
    $(".pages-list .item-data").remove();
    for (var i = 0; i < level1Parents.length; i++) {
        var name = $(level1Parents[i]).children("a").text().trim();
        var level2parents = $(`[data-parent=2][data-parentname='${name}']`);
        var uls = `<ul>`;
        level2parents.each(function (i, element) {
            var level2parentName = $(element).children("a").text().trim();

            var level3parents = $(`[data-parent=3][data-parentname='${level2parentName}']`);
            var ul3s = `<ul>`;
            level3parents.each(function (i3, element3) {
                var level3parentName = $(element3).children("a").text().trim();
                ul3s += `
                            <li class="item item-data level3" data-parentitem="${level3parentName}">
                                <div class="item-row">
                                    <div class="item-col flex-1">
                                        <div class="item-heading"></div>
                                        <strong> ${level3parentName}</strong>
                                    </div>
                                </div>
                                <label>
                                    <input onchange="parentCheckChange(this)" data-name="${level3parentName}" type="checkbox" class="checkbox" />
                                    <span></span>
                                </label>
                                <ul></ul>
                            </li>`;
            });
            ul3s += `</ul>`;

            uls += `
                        <li class="item item-data level2" data-parentitem="${level2parentName}">
                            <div class="item-row">
                                <div class="item-col flex-1"><div class="item-heading"></div><strong>${level2parentName}</strong></div></div>
                            <label>
                                <input onchange="parentCheckChange(this)" data-name="${level2parentName}" type="checkbox" class="checkbox" />
                                <span></span>
                            </label>
                            ${ul3s}
                        </li>`;
        });
        uls += `</ul>`;
        $(".pages-list").append(`
                        <li class="item item-data level1"  data-parentitem="${name}">
                            <div class="item-row"><div class="item-col flex-1"><div class="item-heading"></div><strong>${name}</strong></div></div>
                            <label>
                                <input onchange="parentCheckChange(this)" data-name="${name}" type="checkbox" class="checkbox " /><span></span>
                            </label>
                            ${uls}
                        </li>`);
    }


    AppendPages();
}
function parentCheckChange(checkbox) {
    var parentLevel = $(checkbox).parents("li").length;
    var childCheckboxes = $(checkbox).parents("li.item.item-data.level" + parentLevel).find("li .checkbox");

    if (checkbox.checked)
        checkUnCheck(childCheckboxes, true);

    else
        checkUnCheck(childCheckboxes, false);

}
function AppendPages() {
    var pages = $("[data-page]");
    pages.each(function (index, element) {
        var id = element.dataset.page;
        var name = $(element).children("a").text().trim();
        var parentitem = $(element).parent().parent().children("a").text().trim();
        var level = $(element).parents("li").length + 1;
        $(`[data-parentitem='${parentitem}'] > ul`).append(`
                    <li class="item item-data level${level}">
                        <div class="item-row">
                            <div class="item-col flex-1">
                                <div class="item-heading"></div>
                                <label>
                                    <span class="mr-2 badge badge-warning" >${id}</span> 
                                    ${name}
                                </label> 
                                
                            </div>
                        </div>
                         <label>
                             <input data-pageid="${id}" type="checkbox" name="PageSelected" class="checkbox rounded" />
                              <span></span>
                         </label>
                    </li>`);

    })
    addNewPagesIfAddedToView();

}
function SelectUser(user) {
    debugger

    $("#UserIdTextBox").remove();
    $('body').append(`<input type='hidden' id='UserIdTextBox' value='${user.dataset.userid}' />`);
    $(".item.item-data.selected").removeClass("selected");
    $(user).addClass("selected");

    checkPermittedPages(user.dataset.userid);
    checkOtherPermissions(user.dataset.userid);
}
function checkPermittedPages(userId) {

    var stationId = $("#DDLStation .dd-selected-value").val();
    $.ajax({
        url: '/WebPOSService.asmx/GetUserPermittedPagesList',
        type: "Post",

        data: { "userId": userId, "stationId": stationId },
        success: function (pageList) {

            var checkboxes = $("[type=checkbox]:checked:Not(.otherPermissions)");
            checkUnCheck(checkboxes, false);
            var chkbxsids = "";
            for (var i = 0; i < pageList.length; i++) {

                var id = pageList[i].Id;
                if (i != pageList.length - 1) {
                    chkbxsids += "[type=checkbox][data-pageid=" + id + "],"

                } else {
                    chkbxsids += "[type=checkbox][data-pageid=" + id + "]"
                }

            }
            var checkableCheckboxes = $(chkbxsids);
            checkUnCheck(checkableCheckboxes, true);
        },
        fail: function (jqXhr, exception) {

        }
    });
}
function checkOtherPermissions(userId) {

    var stationId = $("#DDLStation .dd-selected-value").val();
    $.ajax({
        url: '/WebPOSService.asmx/GetUserPermissions',
        type: "Post",

        data: { "userId": userId, "stationId": stationId },
        success: function (UserPermissions) {
            var checkboxes = $("[type=checkbox].otherPermissions");
            checkUnCheck(checkboxes, false);
            var checkBoxesName = Object.keys(UserPermissions);
            checkBoxesName.forEach(function (name, i) {
                var isChecked = Object.values(UserPermissions)[i];
                $("[type=checkbox][name=" + name + "].otherPermissions").prop("checked", isChecked);
            });
        },
        fail: function (jqXhr, exception) {

        }
    });
}
function checkUnCheck(checkboxes, checked) {


    checkboxes.each(function (i, chkbx) {
        chkbx.checked = checked;
    })
}
function save() {
    if ($("#UserIdTextBox").length > 0) {
        var pages = [];
        var selectedPages = $("[data-pageid][name=PageSelected]:checked");
        var userId = $("#UserIdTextBox").val();
        var stationId = $("#DDLStation .dd-selected-value").val();
        selectedPages.each(function (index, element) {
            var id = element.dataset.pageid;
            var pageModel = {
                Id: id,
                UserId: userId,
                StationId: stationId,
            };
            pages.push(pageModel);
        });
        var AllowedOtherStationsSelect = $("[name=AllowedOtherStationsSelect]").prop("checked");
        var UserAllowedToSetSellingPrice = $("[name=UserAllowedToSetSellingPrice]").prop("checked");

        var permisions = {
            Pages: pages,
            AllowedOtherStationsSelect: AllowedOtherStationsSelect,
            UserAllowedToSetSellingPrice: UserAllowedToSetSellingPrice,
            StationId: stationId,
            UserId: userId
        }

        var newModel = JSON.stringify({ model: permisions });
        $.ajax({
            url: "/Registration/UserPermissionManagement.aspx/Save",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: newModel,
            success: function (BaseModel) {
                if (BaseModel.d.Success) {
                    smallSwal("Saved", BaseModel.d.Message, "success");
                }
                else {
                    smallSwal("Failed", BaseModel.d.Message, "error");
                }
            }

        });


    } else
        swal("Select User First", "", "warning")
}

function addNewPagesIfAddedToView() {

    var model = [];
    var pages = $("li[data-page]");
    pages.each(function (index, element) {
        var id = element.dataset.page;
        var href = $(element).children("a").attr("href");
        var name = $(element).children("a").text().trim();
        var duplicated = model.filter(function (page) {
            return (page.Id == id) ? page.Id : 0;
        });
        if (duplicated.length) {
            console.log(duplicated[0].Id + ",");
        }
        var pagesModel = {
            Id: id,
            Name: name,
            Url: href
        };
        model.push(pagesModel);
    });

    if (model.length > 0) {
        var newModel = JSON.stringify({ model: model });
        $.ajax({
            url: "/Registration/UserPermissionManagement.aspx/AddPages",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: newModel,
            success: function (BaseModel) {
                if (!BaseModel.d.Success) {
                    smallSwal("Failed", BaseModel.d.Message, "error");
                }
            }
        });
    }
}