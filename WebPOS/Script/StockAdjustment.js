﻿function insertVoucherRow() {
    var rowNumber = $(".itemsSection .itemRow").length > 0 ? $(".itemsSection .itemRow").last().data().rownumber + 1 : 1;

    $("<div class='itemRow' data-rownumber='" + rowNumber + "'>" +

              "<div class='seven text-align-center'><span class='serialNumber '></span></div>" +
              "<div class='seven name'><input name='Code'  id='ItemCode_" + rowNumber + "' disabled  /></div>" +
              "<div class='sixteen name'><input name='Description'  id='ItemDescription_" + rowNumber + "' disabled   /></div>" +
              "<input name='Rate'  id='ItemRate_" + rowNumber + "' disabled hidden  />" +
              "<div class='eleven'><input   type='number'         name='StockInComputer' id='ItemQty_" + rowNumber + "'   /></div>" +
              "<div class='ten'><input  type='number'   name='StockOnShelf'     type='text' id='stockOnShelfTextbox_" + rowNumber + "'  /></div>" +
              "<div class='ten'><input  type='number'   name='Adjustment'       type='text' id='adjustmentTextbox_" + rowNumber + "'  /></div>" +
              "<div class='ten'><input  type='number'   name='CostExcess'       type='text' id='costExcessTextbox_" + rowNumber + "'  /></div>" +
              "<div class='ten'><input  type='number'   name='CostShort'        type='text' id='costShortTextbox_" + rowNumber + "'  /></div>" +
              "<div class='ten'><input type='number'    name='UnitCost'         id='unitCostTextbox_" + rowNumber + "'  /></div>" +

              "<div class='float-right'><i class='fas fa-times'></i></div>" +

              "</div>").appendTo(".BMtable .itemsSection");



    var selectedPartyProperties = $("[class*=selected" + dataType + "]");
    for (var a = 0; a < selectedPartyProperties.length; a++) {

        $("[id=" + selectedPartyProperties[a].className.split(' ')[0].replace("selected", "") + "_" + rowNumber + "]").val(selectedPartyProperties[a].value.trim());

    }
    assignSerialNumber();
    $("#stockOnShelfTextbox_" + rowNumber + "").focus();
    $("#stockOnShelfTextbox_" + rowNumber + "").select();

}

function save() {
    swal({
        title: "Business Manager",
        text: "Are You Sure to save a transaction?",
        type: 'question',
        showCancelButton: true,
        onfirmButtonText: "Yes, Save it!",
        cancelButtonText: "No, cancel please!",
        showLoaderOnConfirm: true,
        closeOnConfirm: false,
        closeOnCancel: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {

                resolve();
                saveVoucher();
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });
   
}

function saveVoucher() {
    {
        var items = $(".itemsSection .itemRow");

        var addedRows = [];
        items.each(function (index, element) {

            var row = $(element);
            var item = {
                Code: row.find("[name=Code]").val(),
                Description: row.find("[name=Description]").val(),
                StockInComputer: row.find("[name=StockInComputer]").val().trim() == "" ? "0" : row.find("[name=StockInComputer]").val(),
                StockOnShelf: row.find("[name=StockOnShelf]").val().trim() == "" ? "0" : row.find("[name=StockOnShelf]").val(),
                Adjustment: row.find("[name=Adjustment]").val().trim() == "" ? "0" : row.find("[name=Adjustment]").val(),
                CostExcess: row.find("[name=CostExcess]").val().trim() == "" ? "0" : row.find("[name=CostExcess]").val(),
                CostShort: row.find("[name=CostShort]").val().trim() == "" ? "0" : row.find("[name=CostShort]").val(),
                UnitCost: row.find("[name=UnitCost]").val().trim() == "" ? "0" : row.find("[name=UnitCost]").val(),
            }
            addedRows.push(item);
        });

        var voucherModel = {
            Rows: addedRows,
            Date: $("#VoucherDate").val()
        };
        $.ajax({
            url: "/Transactions/StockAdjustment.aspx/Save",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ model: voucherModel }),
            success: function (response) {
                if (response.d.Success) {
                    swal("Success", response.d.Message, "success");
                }
                else {
                    swal("Error", response.d.Message, "error");
                }
            }
        })
    }
}
$(document).on("keydown", "[name='StockOnShelf']", function (e) {
    var row = this.id.split("_")[1];

    if (e.keyCode == 13) {
        $('#SearchBox').focus();
        $('#SearchBox').select();
    }


});


$(document).on("input", "[name='StockOnShelf']", function (e) {
    var row = this.id.split("_")[1];
    var stockInComputer = Number($("#ItemQty_" + row + "").val());
    var stockOnShelf = Number($("#stockOnShelfTextbox_" + row + "").val());
    var rate = Number($("#ItemRate_" + row + "").val());
    $("#unitCostTextbox_" + row + "").val(rate);
    var adjustment = stockOnShelf - stockInComputer;
    $("#adjustmentTextbox_" + row + "").val(adjustment);
    var realAdjustmentQty = adjustment * Math.sign(adjustment);

    if (stockInComputer > stockOnShelf) {
        //Cost Short
        $("#costExcessTextbox_" + row + "").val(0);

        var calculatedValue = rate * realAdjustmentQty;
        $("#costShortTextbox_" + row + "").val(calculatedValue);
    }
    else if (stockInComputer < stockOnShelf) {
        //Cost Excess
        $("#costShortTextbox_" + row + "").val(0);

        var calculatedValue = rate * realAdjustmentQty;
        $("#costExcessTextbox_" + row + "").val(calculatedValue);


    }

    sum();

});

function assignSerialNumber() {
    var serialNumbers = $(".serialNumber");

    serialNumbers.each(function (index, element) {
        $(element).text(index + 1);
    })

}
function sum() {
    var name = ["StockInComputer", "StockOnShelf", "Adjustment", "CostExcess", "CostShort", "UnitCost"];
   
    for (var i = 0; i < name.length; i++) {
        var sumValue = 0;
   
        var values = $("[name=" + name[i] + "]");
    values.each(function (index, element) {
        var value = $(element).val().trim() == "" ? 0 : $(element).val();
        sumValue += Number(value);
    });

    $("[name=total" + name[i] + "]").val(sumValue);
    }
    
}


$(document).on("click", "#saveBtn", function () {
    save();
});
$(document).on("keydown", function (e) {


    if (e.keyCode == 13 && e.keyCode == 16) {
        save();
    }


})
$(document).on("click", ".reports .fa-times", function () {
    var row = $(this).parents(".itemRow");
    row.remove();


    sum();
    assignSerialNumber();
});