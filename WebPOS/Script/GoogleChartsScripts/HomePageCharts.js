﻿$(document).ready(function () {
    google.charts.load('current', { packages: ['corechart'] });

    $.ajax({
        url: '/' + window.location.pathname.split('/')[1] + '/WebPOSService.asmx/GetDataForGoolgleChart1',
        type: "GET",
        success: function (data) {

            drawChart1(data);
            loadChart2();

        },
        fail: function () {

        }
    });






});

function loadChart2() {


    $.ajax({
        url: '/' + window.location.pathname.split('/')[1] + '/WebPOSService.asmx/GetDataForGoolgleChart2',
        type: "GET",
        success: function (data) {

            drawChart2(data);
            initializeRecentPost();

        },
        fail: function () {

        }
    });

}


function drawChart1(response) {

    google.charts.setOnLoadCallback(drawAluminiumSaleChart);
    function drawAluminiumSaleChart() {
        var values = [];
        for (var i = 0; i < response.length; i++) {
            values.push([response[i].Name, parseInt(response[i].Qty)])
        }
        // Define the chart to be drawn.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Element');
        data.addColumn('number', 'Quantity');
        data.addRows(values);
        var options = {
            'width': 600,
            'height': 300
        };
        // Instantiate and draw the chart.
        var chart = new google.visualization.PieChart(document.getElementById('AluminiumSaldChart'));
        chart.draw(data, options);
    }
} 

function drawChart2(response) {

    google.charts.setOnLoadCallback(drawSalePurchaseAndReturnChart);


    function drawSalePurchaseAndReturnChart() {
        
        var values = [];

        values.push([Object.keys(response[0])[2], parseInt(response[0].Sale)]);
        values.push([Object.keys(response[0])[3], parseInt(response[0].Purchase)]);
        values.push([Object.keys(response[0])[4], parseInt(response[0].SaleReturn)]);
        values.push([Object.keys(response[0])[5], parseInt(response[0].PurchaseReturn)]);


        
        // Define the chart to be drawn.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Type');
        data.addColumn('number', 'Quantity');
        data.addRows(values);
        var options = {

            'width': 600,
            'height': 300
        };
        // Instantiate and draw the chart.
        var chart = new google.visualization.PieChart(document.getElementById('SalePurchaseAndReturnChart'));
        chart.draw(data, options);
    }
}
