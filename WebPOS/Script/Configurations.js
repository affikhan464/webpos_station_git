﻿
$(document).ready(function () {
    GetConfigurations();
})

function GetConfigurations() {

    $.ajax({
        url: '/WebPOSService.asmx/GetAllConfigurations',
        type: "Post",
        success: function (Configurations) {
            var checkboxes = $("[type=checkbox].Configurations");
            checkUnCheck(checkboxes, false);
            var checkBoxesName = Object.keys(Configurations);
            debugger
            checkBoxesName.forEach(function (name, i) {
                if ($("[type=checkbox][name=" + name + "].Configurations").length) {
                    var isChecked = Object.values(Configurations)[i];
                    $("[type=checkbox][name=" + name + "].Configurations").prop("checked", isChecked);
                } else {
                    $("[name=" + name + "]").val(Object.values(Configurations)[i])
                }              
            });
        },
        fail: function (jqXhr, exception) {

        }
    });
}

function checkUnCheck(checkboxes, checked) {

    checkboxes.each(function (i, chkbx) {
        chkbx.checked = checked;
    })
}
function save() {
 
        var SelectLastPurchaseRate = $("[name=SelectLastPurchaseRate]").prop("checked");
        var ShowItemQtyAtZero = $("[name=ShowItemQtyAtZero]").prop("checked");
        var InvoiceFormat = $("[name=InvoiceFormat]").val();

        var Configurations = {
            SelectLastPurchaseRate: SelectLastPurchaseRate,
            InvoiceFormat: InvoiceFormat,
            ShowItemQtyAtZero: ShowItemQtyAtZero
        }

        var newModel = JSON.stringify({ model: Configurations });
        $.ajax({
            url: "/Settings/Configurations.aspx/Save",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: newModel,
            success: function (BaseModel) {
                if (BaseModel.d.Success) {
                    smallSwal("Saved", BaseModel.d.Message, "success");
                }
                else {
                    smallSwal("Failed", BaseModel.d.Message, "error");
                }
            }

        });


  
}
