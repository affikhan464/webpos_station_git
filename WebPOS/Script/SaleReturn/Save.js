﻿function SaveBefore() {

    var SaveOk = "Yes";

    var PartyCode = $(".PartyCode").val();


    if (PartyCode == "") {
        SaveOk = "No";
        swal("Error", "Select Party", "error");
    }
    if (SaveOk == "Yes") {
        swal({
            title: "Save this transation?",
            text: "Are You Sure to Save this transaction?",
            type: 'question',
            showCancelButton: true,
            onfirmButtonText: "Yes, Save it!",
            cancelButtonText: "No, cancel please!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {
                    save();
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });

    }
}

$(document).on("keydown", function (e) {
    if (e.ctrlKey && e.which === 35) {
        e.preventDefault();
        $(".Recieved").focus().select();
    }
})
$(document).on("keydown", ".Recieved", function (e) {
    if (e.which === 13) {
        e.preventDefault();
        SaveBefore();
    }
})

function save() {
    var partyName = $(".PartyName").val();
    var Code = $(".PartyCode").val();
    var invCode = $(".InvoiceNumber").val();
    var salesManId = $("[name=salesManDD]").val();

    if (partyName.trim() == "" && Code.trim() == "") {
        swal("Error", "Select Party", "error");

    } else if (invCode.trim() == "") {
        swal("Error", "Enter Invoice Number", "error");

        $(".btnSave").removeClass("disableClick");
    } else if (salesManId=="0") {
        swal("Select Sales Man !!", "Select the Correct Sales Man", "error");

        $(".btnSave").removeClass("disableClick");
    }
     else if (!$(".btnSave").hasClass("disableClick")) {
        $(".btnSave").removeClass("disableClick");


        var Party_Data = {

            Name: $(".PartyName").val(),
            Code: $(".PartyCode").val(),
            Balance: $(".PartyBalance").val(),
            Address: $(".PartyAddress").val(),
            PhoneNumber: $(".PartyPhoneNumber").val(),
            Particular: $(".PartyParticular").val(),
            ManInv: $(".ManInv").val(),
            Date: $(".Date").val(),
            InvoiceNumber: $(".InvoiceNumber").val(),
            NorBalance: $(".PartyNorBalance").val(),
            SalesManName: $("#salesManDD .dd-selected-text").text(),
            SalesManId: $("#salesManDD .dd-selected-value").val(),
           


        }

        var Items = [];
        var ItemRows = $("#myTable tbody tr");
        for (var i = 0; i < ItemRows.length; i++) {

            var rno = $(ItemRows[i]).data().rownumber;
            var Item = {
                SNo: i + 1,
                Code: $(ItemRows[i]).find("." + rno + "_Code").val(),
                ItemName: $(ItemRows[i]).find("." + rno + "_ItemName").val(),
                Qty: $(ItemRows[i]).find("." + rno + "_Qty").val(),
                Rate: $(ItemRows[i]).find("." + rno + "_Rate").val(),
                Amount: $(ItemRows[i]).find("." + rno + "_Amount").val(),
                ItemDis: $(ItemRows[i]).find("." + rno + "_ItemDis").val(),
                PerDis: $(ItemRows[i]).find("." + rno + "_PerDis").val(),
                DealDis: $(ItemRows[i]).find("." + rno + "_DealDis").val(),
                NetAmount: $(ItemRows[i]).find("." + rno + "_NetAmount").val(),
                PurAmount: $(ItemRows[i]).find("." + rno + "_PurAmount").val(),
                RevenueCode: $(ItemRows[i]).find("." + rno + "_RevCode").val(),
                CGSCode: $(ItemRows[i]).find("." + rno + "_CgsCode").val(),
                ItemStationId: $(ItemRows[i]).find("[name=" + rno + "_StationId]").val(),
            }
            Items.push(Item);

        }
        //---------------------------------------------OldBill
        var ItemsOld = [];
        var ItemRows = $("#Table_OldData tbody tr");
        for (var i = 0; i < ItemRows.length; i++) {

            var rno = $(ItemRows[i]).attr("id").split('_')[1];

            var Item = {
                SNo: i + 1,
                Code: ItemRows.find("." + rno + "_Code").val(),
                Qty: ItemRows.find("." + rno + "_Qty").val(),
            }
            ItemsOld.push(Item);

        }
        //------------------------------------------------End Of Old Bill


        var Total_Data = {

            GrossTotal: Number($(".GrossTotal").val()),
            DealRs: Number($(".DealRs").val()),
            FlatDiscount: Number($(".FlatDiscount").val()),
            FlatPer: Number($(".FlatPer").val()),
            NetDiscount: Number($(".NetDiscount").val()),
            BillTotal: Number($(".BillTotal").val()),
            Balance: Number($(".Balance").val()),
            Recieved: Number($(".Recieved").val()),
            PreviousBalance: Number($(".PartyBalance").val()),

            TotalAverageCost: Number($(".PurAmountTotal").val()),
        }


        var IMERows = $("#IMEITable tbody tr");
        var IMEItems = [];
        for (var i = 0; i < IMERows.length; i++) {

            var rno = $(IMERows[i]).data().rownumber;

            var Item = {
                SNo: i + 1,
                Code: $(IMERows[i]).find("." + rno + "_Code").val(),
                ItemName: $(IMERows[i]).find("." + rno + "_ItemName").val(),
                IMEI: $(IMERows[i]).find("." + rno + "_IMEI").val(),
                Qty: $(IMERows[i]).find("." + rno + "_Qty").val(),
                Rate: $(IMERows[i]).find("." + rno + "_Rate").val(),
                Amount: $(IMERows[i]).find("." + rno + "_Amount").val(),
                ItemDis: $(IMERows[i]).find("." + rno + "_ItemDis").val(),
                PerDis: $(IMERows[i]).find("." + rno + "_PerDis").val(),
                DealDis: $(IMERows[i]).find("." + rno + "_DealDis").val(),
                NetAmount: $(IMERows[i]).find("." + rno + "_NetAmount").val(),
                PurAmount: $(IMERows[i]).find("." + rno + "_PurAmount").val(),

            }
            IMEItems.push(Item);

        }


        var saveProductViewModel = {
            TotalData: Total_Data,
            ClientData: Party_Data,
            Products: Items,
            ProductsOld: ItemsOld,
            IMEItems: IMEItems

        };
        debugger
        $.ajax({
            url: "/Transactions/SaleReturnNew.aspx/Save",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ saveProductViewModel: saveProductViewModel }),
            success: function (BaseModel) {
                if (BaseModel.d.Success) {
                    $(".btnSave").removeClass("disableClick");
                    var date = $(".Date").val();
                    smallSwal("Success", BaseModel.d.Message, "success");
                    clearInvoice();
                    $(".InvoiceNumber").val(BaseModel.d.LastInvoiceNumber);
                    $(".Date").val(date);
                }
                else if (BaseModel.d.Success == false && BaseModel.d.LoginAgain == true) {
                    $(".btnSave").removeClass("disableClick");
                    swal({
                        title: "Login Again",
                        html:
                            BaseModel.d.Message + '</b>, ' +
                            `<a target="_blank" href="/?c=1" style="display:block;color:black;font-weight:700">Login Here</a> `,

                        type: "warning"

                    });
                }
                else {
                    $(".btnSave").removeClass("disableClick");
                    swal("Error", BaseModel.d.Message, "error");
                }


            }



        })

    }
}

function clearInvoice() {
    $("#myTable tbody tr").remove();
    $("#Table_OldData tbody tr").remove();
    $("#IMEITable tbody tr").remove();

    $(".clientInput ").val("");
    $(".inv1").text("");
    $(".inv3").text("0");
    $(".inv1").val("");
    $(".inv3").val("0");
    calculationSale();
}
