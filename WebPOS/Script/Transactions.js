﻿
$(document).on('keydown', '#myTable [name=Rate]', function (e) {
    //F3
    if (e.keyCode == 114) {
        //smallSwal('Loading Data...', '', 'info');
        var inputField = e.target;
        var row = $(inputField).parents("tr");
        smallSwal(`Loading...`, '', 'info');
        GetPartyItemSellingPrice(row);
        e.preventDefault();
    }
    //F4
    if (e.keyCode == 115) {
        var inputField = e.target;
        var row = $(inputField).parents("tr");
        GetItemPurchaseRate(row);
        e.preventDefault();
    }
    //Ctrl + U
    if (e.which == 85 && e.ctrlKey) {
        var inputField = e.target;
        var row = $(inputField).parents("tr");
        UpdateSellingPrice(row);
        e.preventDefault();
    }
});

function GetPartyItemSellingPrice(row) {
   
    var itemCode = row.data('itemcode');
    var partyCode = $('.PartyCode').val();
    $.ajax({
        url: '/WebPOSService.asmx/GetPartyItemSellingPrice',
        type: "POST",
        data: { 'partyCode': partyCode, 'itemCode': itemCode },
        success: function (items) {
            if (items.length) {

                $("#lastSellinPriceModal .soldItems").html('');
                var lastSellingPrice = Number(items[0].Rate).toFixed(2);
                $("#lastSellinPriceModal .lastSellingPrice").text(lastSellingPrice.split('.')[0]);
                $("#lastSellinPriceModal .cent").text('.' + lastSellingPrice.split('.')[1]);
                $("#lastSellinPriceModal .itemName").text(items[0].Description);
                $("#lastSellinPriceModal .sellingDate").text(items[0].SellingDate);
                $("#lastSellinPriceModal .PerDis").text(items[0].PerDis);
                $("#lastSellinPriceModal .DealDis").text(items[0].DealDis);
                for (var i = 0; i < items.length; i++) {

                    var row = `  <tr>
                                <td>${items[i].SellingDate}</td>
                                <td>${items[i].Description}</td>
                                <td>${items[i].Rate}</td>
                                <td>${items[i].PerDis}</td>
                                <td>${items[i].DealDis}</td>
                            </tr>
                        `;
                    $("#lastSellinPriceModal .soldItems").append(row);
                }

                $("#lastSellinPriceModal").modal();

            } else {
                smallSwal(`This item is not sold to selected customer.`, '', 'error');
            }

        },
        error: function (error) {
            swal("Error", error.Message, "error"); $(".loader").hide();
        }
    });
}

function GetItemPurchaseRate(row) {

    var itemCode = row.data('itemcode');
    var selectedRowNumber = row.data('rownumber');
    $.ajax({
        url: '/WebPOSService.asmx/GetItemPurchaseRate',
        type: "POST",
        data: { 'itemCode': itemCode },
        success: function (items) {
            if (items.length) {

                $("#purchaseRateModal .soldItems").html('');
                var lastSellingPrice = Number(items[0].Rate).toFixed(2);
                $("#purchaseRateModal .lastSellingPrice").text(lastSellingPrice.split('.')[0]);
                $("#purchaseRateModal .cent").text('.' + lastSellingPrice.split('.')[1]);
                $("#purchaseRateModal .itemName").text(items[0].Description);
                $("#purchaseRateModal .sellingDate").text(items[0].SellingDate);
                $("#purchaseRateModal .BrandName").text(items[0].BrandName);
                $("#purchaseRateModal .VendorName").text(items[0].VendorName);
                for (var i = 0; i < items.length; i++) {

                    var row = `<tr data-selectedrownumber='${selectedRowNumber}' data-rate='${items[i].Rate}' class="cursor-pointer insertLastPurchased">
                                <td>${items[i].SellingDate}</td>
                                <td>${items[i].Description}</td>
                                <td>${items[i].VendorName}</td>
                                <td>${items[i].BrandName}</td>
                                <td>${items[i].Rate}</td>
                            </tr>
                        `;
                    $("#purchaseRateModal .soldItems").append(row);
                }

                $("#purchaseRateModal").modal();

            } else {
                smallSwal(`This item is not sold to selected customer.`, '', 'error');
            }

        },
        error: function (error) {
            swal("Error", error.Message, "error"); $(".loader").hide();
        }
    });
}

$(document).on('click', '.insertLastPurchased', function () {
    var tr = this;
    var selectedrownumber = tr.dataset.selectedrownumber;
    var lastPurchasedRate = tr.dataset.rate;
    $(`#mainTable .${selectedrownumber}_Rate`).val(lastPurchasedRate);
    calculationSale();
    $("#purchaseRateModal").modal('hide');

})

function UpdateSellingPrice(row) {

    var itemCode = row.data('itemcode');
    var price = row.find('[name=Rate]').val();

    $.ajax({
        url: '/WebPOSService.asmx/UpdateSellingPrice',
        type: "POST",
        data: { 'itemCode': itemCode, 'price': price },
        success: function (items) {
            if (items.success) {
                swal(`Selling Price Updated!!`, '', 'success');
            } else {
                smallSwal(`This item is not sold to selected customer.`, '', 'error');
            }

        },
        error: function (error) {
            swal("Error", error.Message, "error"); $(".loader").hide();
        }
    });
}