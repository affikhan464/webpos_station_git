﻿
function addChild(element) {
    $("#AddChildModal .field-wrap > input").val("");
    var level = Number(element.dataset.accountlevel) + 1;
    var parentTitle = element.dataset.accounttitle;
    var parentCode = element.dataset.accountcode;
    $("#AddChildModal .parentAccountTitle").val(parentTitle);
    $("#AddChildModal .parentAccountTitle").text(parentTitle);
    $("#AddChildModal .parentAccountCode").val(parentCode);
    $("#AddChildModal .accountLevel").text(level);
    if (level > 4) {
        $("#AddChildModal .TypeDD").val("Detail");
    }
    else {
        $("#AddChildModal .TypeDD").val("Header");

    }
    $("#AddChildModal").modal();
    updateInputStyle();

}
function editChild(element) {
    $("#EditChildModal input").val("");
    var accounttitle = element.dataset.accounttitle;
    var accountcode = element.dataset.accountcode;

    var level = Number(element.dataset.accountlevel);
    var openingbalance = element.dataset.openingbalance;
    var norbalance = element.dataset.norbalance;
    var type = element.dataset.type;
    var group = element.dataset.group;

    $("#EditChildModal .AccountTitle").val(accounttitle);
    $("#EditChildModal .AccountCode").val(accountcode);
    $("#EditChildModal .TypeDD").val(type);
    $("#EditChildModal .GroupDD").val(group);
    $("#EditChildModal .NatureDD").val(norbalance);
    $("#EditChildModal .OpeningBalance").val(openingbalance);

    $("#EditChildModal .accountLevel").text(level);
    $("#EditChildModal").modal();
    updateInputStyle();

}
function saveAccount() {
    if ($("#AddChildModal .AccountTitle").val().trim() != "") {
        var model =
        {
            ParentCode: $("#AddChildModal .parentAccountCode").val(),
            Level: $("#AddChildModal .accountLevel").text(),
            Title: $("#AddChildModal .AccountTitle").val(),
            Type: $("#AddChildModal .TypeDD").val(),
            Group: $("#AddChildModal .GroupDD").val(),
            NorBalance: $("#AddChildModal .NatureDD").val(),
        }

        $.ajax({
            url: '/Services/AttributeService.asmx/SaveGLCode',

            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ model: model }),
            success: function (data) {

                if (data.d.Success) {
                    smallSwal("Saved", data.d.Message, "success");
                    GetAllAccounts();

                } else {

                    smallSwal("Error", data.d.Message, "error");

                }


            },
            fail: function (jqXhr, exception) {

            }
        });
    } else
        swal("Enter Title", "Kindly enter title for the account", "error");
}
function updateAccount() {
    if ($("#EditChildModal .AccountTitle").val().trim() != "") {
        var model =
                    {
                        Code: $("#EditChildModal .AccountCode").val(),
                        Level: $("#EditChildModal .accountLevel").text(),
                        Title: $("#EditChildModal .AccountTitle").val(),
                        Type: $("#EditChildModal .TypeDD").val(),
                        Group: $("#EditChildModal .GroupDD").val(),
                        NorBalance: $("#EditChildModal .NatureDD").val(),
                        OpeningBalance: $("#EditChildModal .OpeningBalance").val()
                    }

        $.ajax({
            url: '/Services/AttributeService.asmx/SaveGLCode',

            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ model: model }),
            success: function (data) {

                if (data.d.Success) {
                    smallSwal("Saved", data.d.Message, "success");
                    GetAllAccounts();

                } else {

                    smallSwal("Error", data.d.Message, "error");

                }


            },
            fail: function (jqXhr, exception) {

            }
        });
    }
    else
        swal("Enter Title", "Kindly enter title for the account", "error");

}
$(document).on("click", ".removeAccount", function (e) {
    var code = this.dataset.accountcode;
    swal({
        title: 'Are you sure to delete?',
        type: 'question',
        showCancelButton: true,
        confirmButtonText: 'Yes Delete It',
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {
                deleteAccount(code);
                resolve();
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });

});
function deleteAccount(code) {

    $.ajax({
        url: '/Services/AttributeService.asmx/DeleteGLCode',

        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ id: code }),
        success: function (data) {

            if (data.d.Success) {
                smallSwal("Deleted", data.d.Message, "success");
                GetAllAccounts();
            } else {

                smallSwal("Error", data.d.Message, "error");

            }


        },
        fail: function (jqXhr, exception) {

        }
    });

}