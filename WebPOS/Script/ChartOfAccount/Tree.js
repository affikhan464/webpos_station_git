﻿
$(document).ready(function () {

    GetAllAccounts();
    $.expr[":"].contains = $.expr.createPseudo(function (arg) {
        return function (elem) {
            return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
        };
    });

});


function GetAllAccounts() {

    $.ajax({
        url: '/WebPOSService.asmx/GetAllAccounts',
        type: "Post",
        success: function (data) {


            var chartOfAccounts = tree.template(data);
            $("#chartOfAccounts").html(chartOfAccounts);
            appendOpenedAccounts();

        }, fail: function (jqXhr, exception) {

        }
    });
}



tree = {};

tree.template = function (model) {
    var text = "";
    for (var i = 0; i < model.length; i++) {

        text += "<ul>";
        var Title = model[i].Title;
        var Level = model[i].Level;
        var Code = model[i].Code;
        var children = "";
        var collapse = "<span class='unCollapseableTree'></span>";
        var collapsable = "";
        var searchTextbox = "";
        if (model[i].children != "" && model[i].children != undefined) {
            searchTextbox = "<div class=' GLSearchTextbox'><input placeholder='search for account' class='form-control' /></div>";
            collapse = "<span class='collapseTree'><i class='fas fa-minus first'></i><i class='fas fa-minus second'></i></span>";
            collapsable = "collapsable";
            children = addChildren(model[i].children);
        }
        text += "<li><div data-code='" + Code + "'  class='account " + collapsable + "''>" + collapse + " <span class='accountCode  ml-3'>" + Code + "</span>  <span class='accountText  ml-3'>" + Title + "</span> " + searchTextbox + " <span class='circleWrapper'><span class='innerCircle accountLevel'>" + Level + "</span> <span class='upperCircle  fa-spin  ml-3'></span></span>" + actionBtn(model[i]) + "</div>" + children + "</li>";
        text += "</ul>"



    }
    text += "<ul><li><div  class='account addRootAccount' ><a><i class='fas fa-plus'></i></a></div></li></ul>"
    return text;
}
function addChildren(children) {


    var text = "<ul style='display: none;'>"
    for (var i = 0; i < children.length; i++) {
        var Title = children[i].Title;
        var Code = children[i].Code;
        var Level = children[i].Level;
        var child = children[i].children;
        var nestedChilds = "";
        var searchTextbox = "";
        var collapse = "<span class='unCollapseableTree'></span>";
        var collapsable = "";
        if (children[i].children != "" && children[i].children != undefined) {
            searchTextbox = "<div class=' GLSearchTextbox'><input placeholder='search for account' class='form-control' /></div>";
            collapse = "<span class='collapseTree'><i class='fas fa-minus first'></i><i class='fas fa-minus second'></i></span>";
            collapsable = "collapsable";
            nestedChilds = addChildren(children[i].children);


        }
        text += "<li><div data-code='" + Code + "' class='account " + collapsable + "'>" + collapse + " <span class='accountCode  ml-3'>" + Code + "</span> <span class='accountText ml-3'>" + Title + "</span> " + searchTextbox + "<span class='circleWrapper'><span class='innerCircle accountLevel'>" + Level + "</span> <span class='upperCircle  fa-spin  ml-3'></span></span>" + actionBtn(children[i]) + "</div>" + nestedChilds + "</li>";

    }


    text += "</ul>";
    return text;


}
function actionBtn(model) {
    var cross = '<i data-accountcode="' + model.Code + '" class="fas fa-trash removeAccount"></i>';
    var add = '';
    if (model.children.length > 0) {
        cross = '';
    }
    if (model.Level!=5) {
        add='<i data-accountlevel="' + model.Level + '"  data-accounttitle="' + model.Title + '"  data-accountcode="' + model.Code + '" class="fas fa-plus addAccount" onclick="addChild(this)"></i>'
    }
    return '<div class="actionBtns"><i data-openingbalance="' + model.OpeningBalance + '" data-norbalance="' + model.NorBalance + '" data-type="' + model.Type + '" data-group="' + model.Group + '" data-accountlevel="' + model.Level + '"  data-accounttitle="' + model.Title + '"  data-accountcode="' + model.Code + '" class="fas fa-edit editAccount" onclick="editChild(this)"></i>' + cross + ' '+add+'</div>'
}
$(document).on("click", ".collapseTree", function () {
    if (!$(this).hasClass("open")) {
        $(this).addClass("open");
        $($(this).parent().parent().children()[1]).slideDown(300);
        setOpenedAccountsInLocalStorage();
    } else {
        $(this).removeClass("open");
        $($(this).parent().parent().children()[1]).slideUp(500);
        setOpenedAccountsInLocalStorage();
    }


});
function setOpenedAccountsInLocalStorage() {
    var openedTree = $(".collapseTree.open");
    var openedAccounts = [];
    openedTree.each(function (i, e) {
        var code = $(e.parentElement).data("code");
        openedAccounts.push(code);
    });
    localStorage.removeItem("OpenedAccounts");
    localStorage.setItem("OpenedAccounts", openedAccounts);
}


function appendOpenedAccounts() {

    var OpenedAccounts = localStorage.getItem('OpenedAccounts');
    if (OpenedAccounts!=null) {
        OpenedAccounts.split(',').forEach(function (code, index) {
            $("[data-code=" + code + "] .collapseTree").addClass("open")
            $("[data-code=" + code + "] ~ ul").css("display", "block");
        })

    }


}


$(document).on("dblclick", ".collapsable", function () {

    if (!$(this).children(".collapseTree").hasClass("open")) {
        $(this).children(".collapseTree").addClass("open");
        $($(this).parent().children()[1]).slideDown(300)
    } else {
        $(this).children(".collapseTree").removeClass("open");
        $($(this).parent().children()[1]).slideUp(500)
    }


});


$(document).on("input", "input", function () {
    var element = this;
    var value = element.value;

    var list = $(element).parents(".account").siblings("ul").children(":contains(" + value + ")");
    var allList = $(element).parents(".account").siblings("ul").children();

    for (var i = 0; i < allList.length; i++) {
        $(allList[i]).hide();
    }
    for (var i = 0; i < list.length; i++) {
        $(list[i]).show();
    }
})