var _windowWidth = window.innerWidth || $(window).width();
var _view = 0;
if (_windowWidth < 768) _view = 320
	else if (_windowWidth >= 768 && _windowWidth < 1024) _view = 768;
else if (_windowWidth >= 1024 && _windowWidth < 1280) _view = 1024;
else if (_windowWidth >= 1280 && _windowWidth < 1400) _view = 1280;
else _view = 1920;


/** 
* Ready Function---------------STARTS
*/
$(function() {
	
	// // Submenu Show/Hide Function Starts
	// $(".mainMenu").click(function (e) {
	// 	$(".submenu").slideToggle();
	// 	$(this).toggleClass("currentMainMenu");
	// 	e.preventDefault();
	// });

	// $(".closeIcon").click(function (e) {
	// 	$(".submenu").slideUp();
	// 	$(".mainMenu").removeClass("currentMainMenu");
	// 	e.preventDefault();
	// });
	// // Submenu Show/Hide Function Ends
	

	// // Login Container Show/Hide Function Starts
	// $(".loginBtn").click(function (e) {
	// 	$(".loginWrapp").slideToggle();
	// 	$(this).toggleClass("currentMainMenu");
	// 	e.preventDefault();
	// });

	if ($('.toggleMenu').length > 0) {
		$('nav').on('click', '.toggleMenu', function(e) {
			e.preventDefault(); 
			var $this = $(this);
			// 
			if($('.nav').is(":visible")){
				$this.next().stop().slideUp();  
				$this.find('.arrow').removeClass('open');  
			}
			else{
				$this.next().stop().slideDown();
				$this.find('.arrow').addClass('open');
			}
		}); //click ends
} //Exists

/**
* Hide the Menu when clicked outside
*/
$('body').on('click',function(e) {
	if(_view <= 768){
		if($('.nav').is(":visible")){
			var selectedElement = e.target;
			if(!selectedElement.closest('nav')){
				$('.nav').slideUp();
				$('nav').find('.arrow').removeClass('open'); 
			}

		}
	}
}); //click ends

// if ($('.toggleMenu').length > 0) {
// 	$(".currentSection").on('click', '.toggleMenu', function(e) {
// 		e.preventDefault(); 
// 		var $this = $(this);
// 		if($this.find('.open').length > 0){
// 			$this.next().stop().slideUp();  
// 			$this.find('.arrowMenu').removeClass('open');  
// 		}
// 		else{
// 			$this.next().stop().slideDown();
// 			$this.find('.arrowMenu').addClass('open');
// 		}
// }); //click ends
// } //Exists

/**
* Hide the Menu when clicked outside
*/
// $('body').on('click',function(e) {
// 	if(_view <= 768){
// 		if($('.categorySection #side').is(":visible")){
// 			var selectedElementN = $(e.target);
// 			if(!selectedElementN.closest('.currentSection').length){
// 				$('#side').slideUp();
// 				$('.currentSection').find('.arrowMenu').removeClass('open'); 
// 			}
// 		}
// 	}
// });


//// Drop down 
//if ($('.dropdown').length > 0) {	
//	$('.dropdown').each(function () {		
//		$(this).ddslick({
//			width: '100%'			
//		});
//	})	
//}//Exists

if (window.addEventListener) window.addEventListener('DOMMouseScroll', wheel, false);
window.onmousewheel = document.onmousewheel = wheel;

// var time = 10;
// var distance = 10;

function wheel(event) {
// 	// if (event.wheelDelta) delta = event.wheelDelta / 120;
// 	// else if (event.detail) delta = -event.detail / 3;

// 	// handle();
// 	// if (event.preventDefault) event.preventDefault();
// 	// event.returnValue = false;
}

function handle() {

	$('html, body').stop().animate({
		scrollTop: $(window).scrollTop() - (distance * delta)
	}, time);
}


// $(document).keydown(function (e) {

// 	switch (e.which) {
//         //up
//         case 38:
//         $('html, body').stop().animate({
//         	scrollTop: $(window).scrollTop() - distance
//         }, time);
//         break;

//             //down
//             case 40:
//             $('html, body').stop().animate({
//             	scrollTop: $(window).scrollTop() + distance
//             }, time);
//             break;
//         }
//     }); 

// hide #back-top first
$(".back-to-top").hide();

	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('.back-to-top').fadeIn();
			} else {
				$('.back-to-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('.back-to-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
	});

// PopUp
$('.loginLink').click(function(){
	$('#loginConent').simplePopup();
}); 


$('.signupLink').click(function(){
	$('#signupConent').simplePopup();
}); 

/*
*  FancyBox Implementation
*/

// $('.fancybox').fancybox({
// 	prevEffect : 'none',
// 	nextEffect : 'none',
// });

/*$('.fancybox').fancybox({
	prevEffect : 'none',
	nextEffect : 'none',
});*/

// Show Reset Div
// $('.forgotLink a').click(function(){
	
// 	$('.loginWrapp').fadeOut('fast');	
// 	$('.resetWrapp').fadeIn('fast');

verticalAlignCall();

// Read More Button About Page
// 
$(".boardCol #rmAbout").click(function(){
	var $this = $(this);
	$this.toggleClass("active");
	if($this.hasClass('active')){
		$this.text('Read Less');			
	} else {
		$this.text('Read More');
	}
	$(".boardCol .teamDetail p span").slideToggle();
});

$(".aboutContainer #rmAbout").click(function(){
	var $this = $(this);
	$this.toggleClass("active");
	if($this.hasClass('active')){
		$this.text('Read Less');			
	} else {
		$this.text('Read More');
	}
	$(".aboutContainer .teamDetail p span").slideToggle();
});



// var someImg = $("#appShowcase li img");
// var someImgLength= someImg.length;
// someImg.each(function (li) {

// 	if (someImg[li].width > someImg[li].height) {

// 		$("#appShowcase li  #"+"Image"+li+"").addClass('Landscape');
// 	}
// 	else if (someImg[li].width < someImg[li].height) {

// 		$("#appShowcase li  #" + "Image" + li + "").addClass('portrait');
// 	}
// });


    // Tool Tips placement
if (typeof powerTip=="function" ) {
    $('.north').powerTip({ placement: 'n' });
    $('.north-west-alt').powerTip({ placement: 'nw-alt' });
    $('.north-east-alt').powerTip({ placement: 'ne-alt' });
}

if (typeof easyResponsiveTabs == "function") {
    // Responsive Tabs
    // RESPONSIVEUI.responsiveTabs();
    $('#parentHorizontalTab').easyResponsiveTabs({
        type: 'default', //Types: default, vertical, accordion
        width: '767px', //auto or any width like 600px
        fit: true, // 100% fit in a container
        tabidentify: 'hor_1', // The tab groups identifier
        activate: function (event) { // Callback function if tab is switched
            var $tab = $(this);
            var $info = $('#nested-tabInfo');
            var $name = $('span', $info);
            $name.text($tab.text());
            $info.show();
        }
    });
}

$(function() {
	$('a[href*=#]:not([href=#])').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top - 80
				}, 1000);
				return false;
			}
		}
	});
});


// Header Sorting All Patients page
$(".phCol a").click(function(){
	if($('i', this).hasClass('downArrow')){
		$("i", this).removeClass("downArrow");
		$("i", this).addClass("upArrow");
	}
	else{
		$("i", this).removeClass("upArrow");
		$("i", this).addClass("downArrow");
	}	
});

// CheckBox Selection My Recommendation page
$('.appselectionWrapp input[type="checkbox"]').click(function(){
    if($(this).attr("value")=="appname"){
        $(".appname").toggle();
    }
    if($(this).attr("value")=="dates"){
        $(".appdate").toggle();
    }
});


bindButtons();


// Show/Hide patient page panel
$("#timeline").click(function(){
    $(".patientappsRecommended").hide();
    $(".patientTimeline").show();
});

$("#appRecommended").click(function(){
    $(".patientTimeline").hide();
    $(".patientappsRecommended").show();    
});


// Licence Management PopOver
	$(".lmPopover").click(function(){
		if(_view > 768){
		    $('.licencePopover').slideUp('fast');

		    if($('.licencePopover', this).is(':visible')) {
		    	$('.licencePopover', this).slideUp('fast');
		    	return false;
		    }
		    else {
		    	$('.licencePopover', this).slideDown('fast');
		    	return false;
		    }
		    
		}
		else{
			  $('.licencePopover', this).fadeToggle('fast');
	    	return false;
		}

	});

	$(document).click(function(){
			if(_view > 768){
			    $('.licencePopover', this).slideUp('fast');
			}

			else{
				 $('.licencePopover', this).fadeOut('fast');
			}

			// Graph Navigation Up/Down Arrow on Document Click
			if($('.monthSelection i').hasClass('ddIcnUp')){
				$(".monthSelection i").removeClass("ddIcnUp");
				$(".monthSelection i").addClass("ddIcn");
			}

			//$('.mobileMenu', this).fadeOut('fast');
	});	


// Graph Navigation CP
$(".monthSelection").click(function(){	
	if(_view > 320){	
    	$('.licencePopover').slideToggle('fast');
    	return false;     
	}
	else{
		  $('.licencePopover').fadeToggle('fast');
    	  return false;
	}
});

// Graph Navigation Up/Down Arrow
$(".monthSelection").click(function(){
	if($('.monthSelection i').hasClass('ddIcn')){
		$(".monthSelection i").removeClass("ddIcn");
		$(".monthSelection i").addClass("ddIcnUp");
	}
	else{
		$(".monthSelection i").removeClass("ddIcnUp");
		$(".monthSelection i").addClass("ddIcn");
	}	
});

// Graphs View
selectGraphBtn();

// native Menu for Graph Selection on Mobile
$(".graphselectMob").click(function(){	
	if(_view <= 320){	
		$('.mobileMenu').fadeToggle('fast');
    	return false;  		   
	}
	else{	
		$('.mobileMenu').show();   
	}
});


$(document).click(function(){
		if(_view <= 320){			
		     $('.mobileMenu', this).fadeOut('fast');
		}	
});	


// Graph Navigation Up/Down Arrow
$(".functionsWrapp a").click(function(){
	$(this).toggleClass("fcActive");	
});



});  // ready Function Ends



/**
* LOAD FUNCTION---------------STARTS
*/
$(window).load(function() {

	/*var previousScroll = 0,
	headerOrgOffset = $('.bannerContent').height() - 50;
	$(window).scroll(function () {
		var currentScroll = $(this).scrollTop();
		if (currentScroll > headerOrgOffset) {
			if(_view < 768){
				if (currentScroll > previousScroll) {
					$('.headerAction').slideUp();
				} else {
					$('.headerAction').slideDown();
				}
			}	
		} 
		previousScroll = currentScroll;

	});*/

	headerOrgOffset = $('.bannerContent').height() + $('header').height() + 52;
	$(window).scroll(function () {
		var currentScroll = $(this).scrollTop();
		if(_view < 768){
			if (currentScroll > headerOrgOffset) {
				$('.headerAction').slideUp();
			}
			else {
				$('.headerAction').slideDown();
			}	
		}
	});
	if (typeof mCustomScrollbar == "function") {
	    $("#appShowcaseinner, #similarApps").mCustomScrollbar({
	        axis: "x",
	        theme: "dark-3",
	        advanced: { autoExpandHorizontalScroll: true }
	    });

	    $("#downloadedApp, #prescribedApp").mCustomScrollbar({
	        axis: "y",
	        theme: "dark-3",
	        advanced: { autoExpandVerticalScroll: true }
	    });
	}
	verticalAlignCall();

	var someImg = $("#appShowcase li img");
	var someImgLength= someImg.length;
	someImg.each(function (li) {

		if (someImg[li].width > someImg[li].height) {

			$("#appShowcase li  #"+"Image_"+li+"").addClass('Landscape');
		}
		else if (someImg[li].width < someImg[li].height) {

			$("#appShowcase li  #" + "Image_" + li + "").addClass('portrait');
		}
	});

});// Load function ends


/**
 * RESIZE FUNCTION---------------STARTS
 */
 $(window).resize(function() {

 	var re_windowWidth = window.innerWidth || $(window).width();
 	var re_view = 0;
 	if (re_windowWidth < 768) re_view = 320;
 	else if (re_windowWidth >= 768 && re_windowWidth < 1024) re_view = 768;
 	else if (re_windowWidth >= 1024 && re_windowWidth < 1280) re_view = 1024;
 	else if (re_windowWidth >= 1280 && re_windowWidth < 1400) re_view = 1280;
 	else re_view = 1920;
 	if (_view != re_view) {


	/**
     * MAIN MENU ADJUSTMENT
     */
     if(re_windowWidth > 768){
     	$('ul.nav').removeAttr('style');
     	var $toggleMenu = $('.toggleMenu');
     	if($toggleMenu.find('.open').length > 0){ 
     		$toggleMenu.find('.arrow').removeClass('open');  
     	}
     }

     // if(re_windowWidth > 768){
     // 	$('#side').removeAttr('style');
     // 	var $toggleMenu = $('.toggleMenu');
     // 	if($toggleMenu.find('.open').length > 0){ 
     // 		$toggleMenu.find('.arrow').removeClass('open');  
     // 	}
     // }

	if(re_view > 320){
		$('.mobileMenu').show();   
	}
	if(re_view <= 320){
		//
		$('.mobileMenu').hide();   
	}

	/**
	 * keep View and WindowWidth variables at the end of if
	 * @type {[type]}
	 */
	 _view = re_view;
	 _windowWidth = re_windowWidth;
	} //if view != re_view

// Place the container vertically align
verticalAlignCall();



// Licence Management PopOver

	// $(".lmPopover").show().click(function(){
	// 	
	// 	if(re_windowWidth > 768){

	// 	    $('.licencePopover', this).slideToggle('fast');
	// 	    return false;
	// 	}
	// 	else{
	// 		  $('.licencePopover', this).fadeToggle('fast');
	//     	return false;
	//     	break;
	// 	}

	// });

	// $(document).click(function(){
	// 	
	// 		if(re_windowWidth > 768){
	// 		    $('.licencePopover', this).slideUp('fast');
	// 		}

	// 		else{
	// 			 $('.licencePopover', this).fadeOut('fast');
	// 		}
	// });			


// else if(re_windowWidth <= 768){
// 	setTimeout(function() {
// $(".lmPopover").show().click(function(){
// 	    $('.licencePopover', this).fadeToggle('fast');
// 	    return false;
// 	});
// 	$(document).click(function(){
// 	    $('.licencePopover', this).fadeOut('fast');
// 	});		
// 	}, 10);
// }


}); //Resize Function ends


// $("html").addClass("js");
// $(function() {
// 	$("#side").accordion({initShow: "#current", uri: "relative", splitUrl: '/'});
// 	$("html").removeClass("js");
// });

$("html").addClass("js");
$(function() {
	//$("#side").accordion({initShow: "#current", uri: "relative", splitUrl: '/'});
	$("html").removeClass("js");
});

/* Scroll Function */

$(window).scroll(function(){
	var scrollPosition = $(this).scrollTop();
	var windowHeight = $(window).height();
	var wrapperHeight = $('.wrapper').height();
	var footerHeight = $('footer').height();
	var mainHeight = (wrapperHeight - footerHeight - windowHeight) + 35;

	if(scrollPosition > mainHeight) {
		$('.back-to-top a').addClass('positionRealtive');
		$('.back-to-top p').css("display", "block");
	}
	else {
		$('.back-to-top a').removeClass('positionRealtive');
		$('.back-to-top p').css("display", "none");
	}

});

// Place the container vertically align
function verticalAlignCall() {


    var IsSoftwareExpired = $(".IsSoftwareExpired").val();
	var signupWrapp = $('.closeBetaWrapp').outerHeight();
	var topPos = (signupWrapp / 2) ;

	var winHeight = $(window).height();
	if (IsSoftwareExpired == "true") {
	    $(".expiryWrapp").css("display", "block");
	    if (winHeight <= signupWrapp) {
	        $(".closeBetaWrapp").css({ "left": "0%", "top": "0%", "margin-top": "45px", "margin-bottom": "50px", "position": "static", "margin-left": "auto", "margin-right": "auto" });

	    }
	    else {
	        $(".closeBetaWrapp").css({ "top": "45%", "margin-top": -topPos, "position": "absolute", "left": "70%", "margin-left": "-233px" });
	        $(".expiryWrapp").css({ "top": "45%", "margin-top": -topPos, "position": "absolute", "left": "30%", "margin-left": "-233px" });
	    }
	} else {
	    $(".expiryWrapp").css("display", "none");
	    if (winHeight <= signupWrapp) {
	        $(".closeBetaWrapp").css({ "left": "0%", "top": "0%", "margin-top": "45px", "margin-bottom": "50px", "position": "static", "margin-left": "auto", "margin-right": "auto" });

	    }
	    else {
	        $(".closeBetaWrapp").css({ "top": "45%", "margin-top": -topPos, "position": "absolute", "left": "50%", "margin-left": "-233px" });
	    }
	}
	

}

// YesNo Button Function Call
function bindButtons(){
	$('.yesnoBtn button').click(function(e){ 
		var thisObj = $(this);
		$('.yesnoBtn button',this).removeClass('selected');
		thisObj.removeClass('disabled').addClass('selected').siblings().addClass('disabled');
		var val = thisObj.attr('class')=='yes' ? true : false;    
	//do something with val
})
}


function selectGraphBtn(){
	$('.mobileMenu a').click(function(e){ 
		var thisObj = $(this);
		$('.chartsminNav button',this).removeClass('selected');
		thisObj.removeClass('disabled').addClass('selected').siblings().removeClass('selected');
		var val = thisObj.attr('class')=='yes' ? true : false;    
})
}

