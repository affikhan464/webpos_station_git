﻿var global_RevenuAccount;
var global_COGSAccount;
var appendAttribute =  {
    
    init: function (Dropdown_ID, TableName, SelectId, StartRow)  {

    var model = { "attribute": TableName, "code": "0" };

    var dropdown_ID = Dropdown_ID;
    if (TableName == "GLCode") {
        model.code = $("#" + dropdown_ID).data('glcode') != undefined ? $("#" + dropdown_ID).data('glcode') : '0';
    }
    SelectId = SelectId == null || SelectId == undefined ? 1 : SelectId;
    $.ajax({
        async: false,
        url: '/WebPOSService.asmx/GetAttribute',
        type: "Post",
        data:  model ,
        success: function (data) {
            var ddtype = undefined;
            var isMultipleIds = Dropdown_ID.split(',').length > 1;
            if (!isMultipleIds && $("#" + dropdown_ID).length) {
                ddtype = $("#" + dropdown_ID).data('ddtype');
            }
            if (ddtype != undefined && ddtype == "classic") {
                populateClassicDropdown(Dropdown_ID, data, SelectId);
            }
            else {
                if (Dropdown_ID != "") {
                    populateDropdown(Dropdown_ID, data, SelectId, StartRow);
            }
                else {
                var ddData = "{";
                for (var i = 0; i < data.length; i++) {
                    var code = data[i].Code
                    ddData += '"' + code + '":{"title":"' + data[i].Name + '","amount":"0"},'
                }
                ddData = ddData.substring(0, ddData.length - 1);
                ddData += "}"

                if (TableName == "COGSAccount") {

                    global_COGSAccount = JSON.parse(ddData);
                }
                else {
                    global_RevenuAccount = JSON.parse(ddData);
                }
            }
            }
             
        }, fail: function (jqXhr, exception) {

        }
    });
    },
    trigger: (Dropdown_ID) => {
            var dd = $("#" + Dropdown_ID).data('ddslick');
            var data = dd.selectedData;
            if (typeof selectedChanged == "function") {
                selectedChanged(data, Dropdown_ID);
            }        
    }
}

function populateDropdown(Dropdown_ID, data, SelectId, StartRow) {
    //For Station on Sale, Purchase, Returns and Edits Pages
    var isStation = data[0].Attribute == "Station";
    var isAdmin = $("#IsAdmin").val() == "True";
    var url = location.pathname;
    var isSale_Purchase_Returns =
        url.indexOf("PurchaseNewItems.aspx") > 0
        || url.indexOf("PurchaseReturnNew.aspx") > 0
        || url.indexOf("SaleNewItems.aspx") > 0
        || url.indexOf("SaleReturnNew.aspx") > 0
        || url.indexOf("PurchaseEditNew.aspx") > 0
        || url.indexOf("PurchaseReturnEditNew.aspx") > 0
        || url.indexOf("SaleEditNew.aspx") > 0
        || url.indexOf("SaleReturnEditNew.aspx") > 0
        || url.indexOf("ExpenseEntry.aspx") > 0
        || url.indexOf("ExpenseEntryEdit.aspx") > 0;
    var isUserAllowedOtherStationsSelect = $("#UserAllowedOtherStationsSelect").val() == "True";


    var ddData = [];

    SelectId = SelectId.toString();

    var isMultipleIds = Dropdown_ID.split(',').length > 1;
    var isMultipleSelectIds = SelectId.split(',').length > 1;
    if (StartRow) {
        ddData.push({
            text: StartRow,
            value: 0,
        });
    }
    if (isMultipleIds && !isMultipleSelectIds) {
        var ids = Dropdown_ID.split(',');
        ids.pop();
        for (var i = 0; i < data.length; i++) {
            if (data[i].Code == SelectId) {
                ddData.push({
                    text: data[i].Name,
                    value: data[i].Code,
                    selected: true,

                });
            } else if (isSale_Purchase_Returns && isStation && !isAdmin && !isUserAllowedOtherStationsSelect) {
                //do not add other stations except selected
            } else {
                ddData.push({
                    text: data[i].Name,
                    value: data[i].Code,
                    selected: false,

                });
            }


        }

        for (var i = 0; i < ids.length; i++) {
            var parent = $(document.getElementById(ids[i]).parentElement);
            $("#" + ids[i] + "").remove();
            parent.append("<select id='" + ids[i] + "' class='dropdown'> </select>");

            $("#" + ids[i] + "").ddslick({
                width: "100%",
                data: ddData,
                onSelected: function (data) {
                    if (typeof selectedChanged == "function") {
                        selectedChanged(data);
                    }
                }

            });
        }

    }
    else if (isMultipleSelectIds) {

        var ids = Dropdown_ID.split(',');
        ids.pop();
        var selectIds = SelectId.split(',');
        selectIds.pop();



        for (var x = 0; x < selectIds.length; x++) {
            var multipleDdData = [];
            for (var i = 0; i < data.length; i++) {
                if (data[i].Code == selectIds[x]) {
                    multipleDdData.push({
                        text: data[i].Name,
                        value: data[i].Code,
                        selected: true,

                    });
                } else if (isSale_Purchase_Returns && isStation && !isAdmin && !isUserAllowedOtherStationsSelect) {
                    //do not add other stations except selected
                }
                else {
                    multipleDdData.push({
                        text: data[i].Name,
                        value: data[i].Code,
                        selected: false,

                    });
                }

            }
            ddData.push(multipleDdData);

        }


        for (var i = 0; i < ids.length; i++) {
            var parent = $(document.getElementById(ids[i]).parentElement);
            $("#" + ids[i] + "").remove();
            parent.append("<select id='" + ids[i] + "' class='dropdown'> </select>");

            $("#" + ids[i] + "").ddslick({
                width: "100%",
                data: ddData[i],
                onSelected: function (data) {
                    if (typeof selectedChanged == "function") {
                        selectedChanged(data);
                    }
                }

            });
        }

    }
    else {

        for (var i = 0; i < data.length; i++) {
            if (data[i].Code == SelectId) {
                ddData.push({
                    text: data[i].Name,
                    value: data[i].Code,
                    selected: true,

                });
            } else if (isSale_Purchase_Returns && isStation && !isAdmin && !isUserAllowedOtherStationsSelect) {
                //do not add other stations except selected
            }
            else {
                ddData.push({
                    text: data[i].Name,
                    value: data[i].Code,
                    selected: false,

                });
            }


        }
        var parent = $(document.getElementById(Dropdown_ID).parentElement);
        $("#" + Dropdown_ID + "").remove();
        parent.append("<select id='" + Dropdown_ID + "' class='dropdown'> </select>");

        $("#" + Dropdown_ID + "").ddslick({
            width: "100%",
            data: ddData,
            onSelected: function (data) {
                if (typeof selectedChanged == "function") {
                    selectedChanged(data.selectedData);
                }
            }

        });
    }


}

function populateClassicDropdown(Dropdown_ID, data, SelectId) {
    var ddData = "";
    for (var i = 0; i < data.length; i++) {
        var code = data[i].Code
        var title = data[i].Name
        if (SelectId == code) {
            ddData += `<option selected="selected" value="${code}">${title}</option>`;
        } else {
            ddData += `<option value="${code}">${title}</option>`;
        }
    }
    $("#" + Dropdown_ID).html(ddData);
    return false;
}