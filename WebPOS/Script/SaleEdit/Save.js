﻿function SaveBefore() {

    var SaveOk = "Yes";
    var InvoiceNumber = $(".InvoiceNumber").val();
    var PartyCode = $(".PartyCode").val();

    if (InvoiceNumber == "") {
        SaveOk = "No";
        swal("Error", "Type Invoice/Voucher No.", "error");
        $(".InvoiceNumber").focus();
    }
    if (PartyCode == "") {
        SaveOk = "No";
        swal("Error", "Select Party", "error");
    }
    if (SaveOk == "Yes") {
        swal({
            title: "Save this transation?",
            text: "Are You Sure to Save this transaction?",
            type: 'question',
            showCancelButton: true,
            onfirmButtonText: "Yes, Save it!",
            cancelButtonText: "No, cancel please!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {
                    save();
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });

    }
}

$(document).on("keydown", function (e) {
    if (e.ctrlKey && e.which === 35) {
        e.preventDefault();
        $(".Recieved").focus().select();
    }
})
$(document).on("keydown", ".Recieved", function (e) {
    if (e.which === 13) {
        e.preventDefault();
        SaveBefore();
    }
})


function save() {
    var partyName = $(".PartyName").val();
    var Code = $(".PartyCode").val();
    var invCode = $(".InvoiceNumber").val();

    if (partyName.trim() == "" && Code.trim() == "") {
        swal("Error", "Select Party", "error");

    } else if (invCode.trim() == "") {
        swal("Error", "Enter Invoice Number", "error");

    } else if (!$(".btnSave").hasClass("disableClick")) {
        $(".btnSave").addClass("disableClick");
        var saveProductViewModel = getSaveModel();

        debugger
        $.ajax({
            url: "/Correction/SaleEditNew.aspx/Save",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ saveProductViewModel: saveProductViewModel }),
            success: function (BaseModel) {
                debugger
                if (BaseModel.d.Success) {
                    var date = $(".Date").val();
                    smallSwal("Success", BaseModel.d.Message, "success");
                    clearInvoice();
                    //$(".InvoiceNumber").val(BaseModel.d.LastInvoiceNumber);
                    $(".btnSave").removeClass("disableClick");
                    $(".Date").val(date);
                }
                else if (BaseModel.d.Success == false && BaseModel.d.LoginAgain == true) {
                    $(".btnSave").removeClass("disableClick");
                    swal({
                        title: "Login Again",
                        html:
                            BaseModel.d.Message + '</b>, ' +
                            `<a target="_blank" href="/?c=1" style="display:block;color:black;font-weight:700">Login Here</a> `,

                        type: "warning"

                    });
                }
                else {
                    $(".btnSave").removeClass("disableClick");
                    swal("Error", BaseModel.d.Message, "error");
                }
            }

        })

    }
}

function getSaveModel() {

    var Party_Data = {
        InvoiceNumber: $(".InvoiceNumber").val(),
        Name: $(".PartyName").val(),
        Code: $(".PartyCode").val(),
        Balance: $(".PartyBalance").val(),
        Address: $(".PartyAddress").val(),
        PhoneNumber: $(".PartyPhoneNumber").val(),
        Particular: $(".PartyParticular").val(),
        ManInv: $(".ManInv").val(),
        Date: $(".Date").val(),
        InvoiceNumber: $(".InvoiceNumber").val(),
        NorBalance: $(".PartyNorBalance").val(),
        SalesManName: $("#salesManDD .dd-selected-text").text(),
        SalesManId: $("#salesManDD .dd-selected-value").val(),
        StationId: $("#BillStationId").val(),


    }

    var Items = [];
    var ItemRows = $("#myTable tbody tr");
    for (var i = 0; i < ItemRows.length; i++) {

        var rno = $(ItemRows[i]).data().rownumber;
        var Item = {
            SNo: i + 1,
            Code: $(ItemRows[i]).find("." + rno + "_Code").val(),
            ItemName: $(ItemRows[i]).find("." + rno + "_ItemName").val(),
            Qty: $(ItemRows[i]).find("." + rno + "_Qty").val(),
            Rate: $(ItemRows[i]).find("." + rno + "_Rate").val(),
            Amount: $(ItemRows[i]).find("." + rno + "_Amount").val(),
            ItemDis: $(ItemRows[i]).find("." + rno + "_ItemDis").val(),
            PerDis: $(ItemRows[i]).find("." + rno + "_PerDis").val(),
            DealDis: $(ItemRows[i]).find("." + rno + "_DealDis").val(),
            NetAmount: $(ItemRows[i]).find("." + rno + "_NetAmount").val(),
            PurAmount: $(ItemRows[i]).find("." + rno + "_PurAmount").val(),
            RevenueCode: $(ItemRows[i]).find("." + rno + "_RevCode").val(),
            CGSCode: $(ItemRows[i]).find("." + rno + "_CgsCode").val(),
            PerPieceCommission: $(ItemRows[i]).find("." + rno + "_PerPieceCommission").val(),
            ActualSellingPrice: $(ItemRows[i]).find("." + rno + "_ActualSellingPrice").val(),
            ItemStationId: $(ItemRows[i]).find("[name=" + rno + "_StationId]").val(),


        }
        Items.push(Item);

    }

    var ItemsOld = [];
    var ItemRows = $("#Table_OldData tbody tr");
    for (var i = 0; i < ItemRows.length; i++) {
        var rno = $(ItemRows[i]).attr("id").split('_')[1];
        var Item = {
            SNo: i + 1,
            Code: $("#Table_OldData ." + rno + "_CodeOld").val(),
            Qty: $("#Table_OldData ." + rno + "_QtyOld").val(),
            ItemStationId: $("#Table_OldData ." + rno + "_StationIdOld").val(),
        }
        ItemsOld.push(Item);

    }
    
    var Total_Data = {

        GrossTotal: Number($(".GrossTotal").val()),
        DealRs: Number($(".DealRs").val()),
        FlatDiscount: Number($(".FlatDiscount").val()),
        FlatPer: Number($(".FlatPer").val()),
        NetDiscount: Number($(".NetDiscount").val()),
        BillTotal: Number($(".BillTotal").val()),
        Balance: Number($(".Balance").val()),
        Recieved: Number($(".Recieved").val()),
        PreviousBalance: Number($(".PartyBalance").val()),

        TotalAverageCost: Number($(".PurAmountTotal").val()),
        CashReceivedGLCode: $("#CashReceivedCategoryDD").val(),
    }
    var IMERows = $("#IMEITable tbody tr");
    var IMEItems = [];
    for (var i = 0; i < IMERows.length; i++) {

        var rno = $(IMERows[i]).data().rownumber;

        var Item = {
            SNo: i + 1,
            Code: $(IMERows[i]).find("." + rno + "_Code").val(),
            ItemName: $(IMERows[i]).find("." + rno + "_ItemName").val(),
            IMEI: $(IMERows[i]).find("." + rno + "_IMEI").val(),
            Qty: $(IMERows[i]).find("." + rno + "_Qty").val(),
            Rate: $(IMERows[i]).find("." + rno + "_Rate").val(),
            Amount: $(IMERows[i]).find("." + rno + "_Amount").val(),
            ItemDis: $(IMERows[i]).find("." + rno + "_ItemDis").val(),
            PerDis: $(IMERows[i]).find("." + rno + "_PerDis").val(),
            DealDis: $(IMERows[i]).find("." + rno + "_DealDis").val(),
            NetAmount: $(IMERows[i]).find("." + rno + "_NetAmount").val(),
            PurAmount: $(IMERows[i]).find("." + rno + "_PurAmount").val()

        }
        IMEItems.push(Item);

    }


    var saveProductViewModel = {
        TotalData: Total_Data,
        ClientData: Party_Data,
        Products: Items,
        ProductsOld: ItemsOld,
        IMEItems: IMEItems

    };
    return saveProductViewModel;
}
function deleteVoucher() {

    $(".deleteInvoice").addClass("disableClick");

    var saveProductViewModel = getSaveModel();


    swal({
        title: "Delete this transaction?",
        html: "Are You Sure to Delete this transaction?<br><br><strong class='text-center'>Type 'delete' to Delete this transation.</strong>",
        type: 'question',
        input: 'text',
        showCancelButton: true,
        onfirmButtonText: "Yes, Delete it!",
        cancelButtonText: "No, cancel please!",
        showLoaderOnConfirm: true,
        closeOnConfirm: false,
        closeOnCancel: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {

                if (text.toLowerCase() == 'delete') {
                    debugger
                    $.ajax({
                        url: "/Correction/SaleEditNew.aspx/Delete",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify({ saveProductViewModel: saveProductViewModel }),
                        success: function (BaseModel) {
                            debugger
                            if (BaseModel.d.Success) {
                                $(".deleteInvoice").removeClass("disableClick");
                                var date = $(".Date").val();
                                smallSwal("Success", BaseModel.d.Message, "success");
                                clearInvoice();
                                updateInputStyle();
                                $(".Date").val(date);
                            }
                            else {
                                $(".deleteInvoice").removeClass("disableClick");
                                swal("Error", BaseModel.d.Message, "error");
                            }
                        },
                        error: function () {
                            $(".deleteInvoice").removeClass("disableClick");
                        }
                    });
                } else {
                    Swal.showValidationError(
                        `Type correct spelling of 'delete'`
                    );
                    resolve();
                }
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });
}
function clearInvoice() {
    $("#myTable tbody tr").remove();
    $("#IMEITable tbody tr").remove();
    $("#Table_OldData tbody tr").remove();

    $(".clientInput ").val("");
    $(".inv1").text("");
    $(".inv3").text("0");
    $(".inv1").val("");
    $(".inv3").val("0");

    calculationSale();

}