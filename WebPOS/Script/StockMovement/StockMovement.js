﻿$(document).ready(function () {
    appendAttribute.init("MoveFromDD", "Station", 1);
    appendAttribute.init("MoveToDD", "Station", 2);

    $("#EditBtn").on("click", function () {
        var invoiceNumber = $(".InvoiceNumber").val();
        window.location.href = "/Correction/StockMovementEdit.aspx?v=" + invoiceNumber;
    });


});

$(document).on('click', '#PrintBtn', function () {

    printStockMovement();

})
function printStockMovement() {
    swal({
        title: 'Enter Your Password',
        type: 'info',
        input: 'password',
        showCancelButton: true,
        confirmButtonText: 'Verify !!',
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {
                if (text) {
                    $.ajax({
                        url: "/Default.aspx/VerifyUser",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify({ enteredPassword: text }),
                        success: function (BaseModel) {
                            if (BaseModel.d.Success) {
                                window.open(
                                    '/Transactions/TransactionPrints/StockMovementCR_A4.aspx?InvNo=' + $(".InvoiceNumber").val() + '&p=' + BaseModel.d.Data.p,
                                    '_blank'
                                );
                                resolve();
                            } else if (BaseModel.d.Success == false && BaseModel.d.LoginAgain == true) {
                                swal({
                                    title: "Login Again",
                                    html:
                                        `<a target="_blank" href="/?c=1" style="display:block;color:black;font-weight:700">Login Here and print it again.</a> `,

                                    type: "warning"

                                });
                            }
                            else {
                                swal.showValidationError(BaseModel.d.Message);
                                resolve();
                            }
                        }
                    });


                } else {
                    swal.showValidationError("Enter some text");
                    resolve();
                }
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });
}
function rerenderSerialNumber() {
    var rows = $(".item-list.contentList  .item-data .SrNo");
    var sno = 0;
    rows.each(function (index, element) {
        sno += 1;
        $(element).html(sno);
    })
}

function itemAlreadyExist(code) {
    var rowNo = $(".item-list.contentList [data-itemcode=" + code + "]").data("rownumber");
    return rowNo;
}
function insertDataInStockMovement() {
    debugger;
    var code = $(".selectedItemCode").val();
    var name = $(".selectedItemDescription").val();
    var qty = 1;
    var availableQty = $(".selectedItemStationFromQty").val();
    if (availableQty == "0") {
        playError();
        return false;
    }
    var rate = Number($(".selectedItemRate").val());
    var amount = qty * rate;
    var lastRowNumber = $(".item-list.contentList li.item-data:last-child").length > 0 ? $(".item-list.contentList  li.item-data:last-child").data("rownumber") : 0;
    var SrNo = Number(lastRowNumber) + 1;
    var itemExist = itemAlreadyExist(code);
    if (itemExist > 0) {
        Sno = itemExist;
        var qty = Number($("." + Sno + "_Qty").val()) + 1;
        $("." + Sno + "_Qty").val(qty).focus().select();

    } else {

        $(".item-list.contentList").append(`
                            <li data-rownumber='${SrNo}' data-itemcode="${code}" class="item item-data">
                                <div class="item-row">
                                    <div class="item-col flex-1">
                                        <div class="item-heading">Sr. No.</div>
                                        <div class='SrNo ${SrNo}_SrNo'></div>
                                    </div>
                                    <div class="item-col flex-2">
                                        <div class="item-heading">Code</div>
                                        <div class="${SrNo}_Code" >${code}</div>
                                    </div>
                                    <div class="item-col flex-4 no-overflow">
                                        <div>
                                            <a class="">
                                                <h4 class="item-title no-wrap ${SrNo}_Name">${name} </h4>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="item-col flex-1">
                                        <div class="item-heading">Qty</div>
                                        <div><input name="Qty" value="${qty}" class="${SrNo}_Qty w-100" type="number" /> </div>
                                    </div>
                                    <div class="item-col flex-2">
                                        <div class="item-heading">Rate</div>
                                        <div class="${SrNo}_Rate">${rate} </div>
                                    </div>
                                    <div class="item-col flex-2">
                                        <div class="item-heading">Amount</div>
                                        <div class="${SrNo}_Amount">${amount} </div>
                                    </div>
                                    <div class="item-col item-col-date flex-3">
                                        <div>
                                            <i data-rownumber="${SrNo}" data-code="${code}"  onclick="deleteItem(this)"  class="fas fa-times mr-3"></i>
                                        </div>
                                    </div>
                                </div>
                            </li>`);
        $("." + SrNo + "_Qty").focus();
        $("." + SrNo + "_Qty").select();
        rerenderSerialNumber();
    }

}
$(document).on("input", "[name=Qty]", function () {
    var srno = $(this).parents("li.item-data").data("rownumber");
    var rate = Number($("." + srno + "_Rate").text());
    var qty = Number($("." + srno + "_Qty").val());
    var amount = rate * qty;
    $("." + srno + "_Amount").text(amount)

})
$(document).on("keypress", "[name=Qty]", function (e) {
    if (e.keyCode == "13") {
        $("#SearchBox").focus();
        $("#SearchBox").select();
    }
})
function save() {
    if ($("[name=MoveFromDD]").val() != $("[name=MoveToDD]").val() && $(".item-list.contentList li.item-data").length > 0) {

        swal({
            title: 'Enter Your Password',
            type: 'info',
            input: 'password',
            showCancelButton: true,
            confirmButtonText: 'Verify !!',
            showLoaderOnConfirm: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {
                    if (text) {
                        $.ajax({
                            url: "/Default.aspx/VerifyUser",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            data: JSON.stringify({ enteredPassword: text }),
                            success: function (BaseModel) {
                                if (BaseModel.d.Success) {
                                    saveDetail(BaseModel.d.Data.p)

                                } else if (BaseModel.d.Success == false && BaseModel.d.LoginAgain == true) {
                                    swal({
                                        title: "Login Again",
                                        html:
                                            `<a target="_blank" href="/?c=1" style="display:block;color:black;font-weight:700">Login Here and print it again.</a> `,

                                        type: "warning"

                                    });
                                }
                                else {
                                    swal.showValidationError(BaseModel.d.Message);
                                    resolve();
                                }
                            }
                        });


                    } else {
                        swal.showValidationError("Enter some text");
                        resolve();
                    }
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        })
    }
    else {
        if ($(".item-list.contentList li.item-data").length < 1) {
            smallSwal("Add Some Items !!", '', "error");

        } else
            smallSwal("Change Station", 'Station From and To must be different !!', "error");
    }
}
function saveDetail(key) {


    var items = $("li.item-data");
    var itemsModel = [];
    for (var i = 0; i < items.length; i++) {
        var rowNo = $(items[i]).data("rownumber");
        var model = {
            SNo: Number($("." + rowNo + "_SrNo").text()),
            Code: Number($("." + rowNo + "_Code").text()),
            Name: $("." + rowNo + "_Name").text(),
            Qty: Number($("." + rowNo + "_Qty").val()),
            Date: $(".Date").val(),
            Amount: Number($("." + rowNo + "_Amount").text()),
            Rate: Number($("." + rowNo + "_Rate").text()),
            StationNameFrom: $("#MoveFromDD .dd-selected-text").text(),
            StationNameTo: $("#MoveToDD .dd-selected-text").text(),
            StationIdFrom: Number($("[name=MoveFromDD]").val()),
            StationIdTo: Number($("[name=MoveToDD]").val()),
            Narration: $(".Narration").val(),
        };
        itemsModel.push(model);
    }

    $.ajax({
        url: "/Transactions/StockMovement.aspx/Save",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ models: itemsModel, key: key }),
        success: function (BaseModel) {
            if (BaseModel.d.Success) {
                $("li.item-data").remove();
                $(".InvoiceNumber").val(BaseModel.d.LastInvoiceNumber);
                smallSwal(BaseModel.d.Message, '', "success");
                window.open(
                    '/Transactions/TransactionPrints/StockMovementCR_A4.aspx?InvNo=' + $(".InvoiceNumber").val() + '&p=' + key,
                    '_blank'
                );
            }
            else {
                smallSwal("Failed", BaseModel.d.Message, "error");
            }
        }
    });



}

function deleteItem(element) {


    swal({
        title: 'Are you sure to remove it?',
        type: 'question',
        showCancelButton: true,
        confirmButtonText: 'Yes Remove It',
        showLoaderOnConfirm: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {
                $(element).parents("li.item-data").remove();
                rerenderSerialNumber();
                resolve();
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    })






}
