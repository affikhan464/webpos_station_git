﻿function insertVoucherRow() {
    var code = $(".selectedIngredientCode").val();
    var exitedQtyTxbx = $("[data-code=" + code + "]");
    var isItemExist = exitedQtyTxbx.length > 0;
    if (isItemExist) {

        var existingQty = Number(exitedQtyTxbx.val());
        var newQty = Number($(".IngredientQty").val());
        var Qty = existingQty + newQty;
        exitedQtyTxbx.val(Qty);

        var rowNumber = exitedQtyTxbx.parents(".itemRow").data().rownumber;
        var cost = Number($("#CostTxbx_" + rowNumber + "").val());
        var amount = cost * Qty;
        $("#AmountTxbx_" + rowNumber + "").val(amount);

    } else {
        var rowNumber = $(".itemsSection .itemRow").length > 0 ? $(".itemsSection .itemRow").last().data().rownumber + 1 : 1;

        $("<div class='itemRow' data-rownumber='" + rowNumber + "'>" +

            "<div class='sixteen text-align-left'><span class='serialNumber '></span></div>" +
            "<div class='sixteen name'><input name='Code' id='IngredientCode_" + rowNumber + "' disabled  /></div>" +
            "<div class='sixteen name'><input name='Description'  id='IngredientDescription_" + rowNumber + "' disabled   /></div>" +
            "<div class='sixteen'>    <input  type='number'   data-code='" + $(".selectedIngredientCode").val() + "'     name='Qty' id='QtyTxbx_" + rowNumber + "'  /></div>" +
            "<div class='sixteen'>        <input  type='number'   name='Rate'     type='text' id='CostTxbx_" + rowNumber + "' disabled  /></div>" +
            "<div class='sixteen'>        <input  type='number'   name='Amount'       type='text' id='AmountTxbx_" + rowNumber + "' disabled  /></div>" +

            "<div class='float-right'><i class='fas fa-edit'></i></div>" +
            "<div class='float-right'><i class='fas fa-times'></i></div>" +

            "</div>").appendTo(".BMtable .itemsSection");



        var selectedPartyProperties = $("[class*=selected" + dataType + "]");
        for (var a = 0; a < selectedPartyProperties.length; a++) {

            $("[id=" + selectedPartyProperties[a].className.split(' ')[0].replace("selected", "") + "_" + rowNumber + "]").val(selectedPartyProperties[a].value.trim());

        }

        var cost = Number($(".IngredientRate").val());
        var qty = Number($(".IngredientQty").val());

        $("#QtyTxbx_" + rowNumber + "").val(qty);
        $("#CostTxbx_" + rowNumber + "").val(cost);
        $("#AmountTxbx_" + rowNumber + "").val(cost * qty);
        assignSerialNumber();
    }
    
    $(".IngredientDescription").focus();
    $(".IngredientDescription").select();
    sum();
    $(".Ingredients input").val("");
}



function save() {

    if ($(".MainItemCode").val().trim() != "") {
        swal({
            title: "Business Manager",
            text: "Are You Sure to save a transaction?",
            type: 'question',
            showCancelButton: true,
            onfirmButtonText: "Yes, Save it!",
            cancelButtonText: "No, cancel please!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {

                    resolve();
                    saveVoucher();
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });

        
    } else {
        swal("Select Main Item", "Main Item is not selected", "error")
    }
   
}
function saveVoucher() {
    var items = $(".itemsSection .itemRow");

    var addedRows = [];
    items.each(function (index, element) {

        var row = $(element);
        var item = {
            Code: row.find("[name=Code]").val(),
            Description: row.find("[name=Description]").val(),
            Qty: row.find("[name=Qty]").val().trim() == "" ? "0" : row.find("[name=Qty]").val(),
            Rate: row.find("[name=Rate]").val().trim() == "" ? "0" : row.find("[name=Rate]").val(),
            Amount: row.find("[name=Amount]").val().trim() == "" ? "0" : row.find("[name=Amount]").val()
        }
        addedRows.push(item);
    });

    var model = {
        Ingredients: addedRows,
        MainItemCode: $(".MainItemCode").val(),
        MainItemDescription: $(".MainItemDescription").val()
    };
    $.ajax({
        url: "/Transactions/Assembling.aspx/Save",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ model: model }),
        success: function (response) {
            if (response.d.Success) {
                swal("Success", response.d.Message, "success");
            }
            else {
                swal("Error", response.d.Message, "error");
            }
        }
    })
}
$(document).on("keydown",".IngredientQty", function (e) {


    if (e.keyCode == 13) {
        insertVoucherRow();
    }


})


function assignSerialNumber() {
    var serialNumbers = $(".serialNumber");

    serialNumbers.each(function (index, element) {
        $(element).text(index + 1);
    })

}
function sum() {
    var name = ["Qty", "Rate", "Amount"];
   
    for (var i = 0; i < name.length; i++) {
        var sumValue = 0;
   
        var values = $("[name=" + name[i] + "]");
    values.each(function (index, element) {
        var value = $(element).val().trim() == "" ? 0 : $(element).val();
        sumValue += Number(value);
    });

    $("[name=total" + name[i] + "]").val(sumValue);
    }
    
}


$(document).on("click", "#saveBtn", function () {
    save();
});
$(document).on("keydown", function (e) {


    if (e.keyCode == 13 && e.keyCode == 16) {
        save();
    }


})
$(document).on("keydown", "[name=Qty]", function (e) {


    if (e.keyCode == 13) {
        $(".IngredientDescription").focus();
        $(".IngredientDescription").select();
    }


})
$(document).on("input", "[name=Qty]", function (e) {

    var rowNumber = $(this).parents(".itemRow").data().rownumber;
    var cost = Number($("#CostTxbx_" + rowNumber + "").val());
    var Qty = Number($("#QtyTxbx_" + rowNumber + "").val());
    var amount = cost * Qty;
    $("#AmountTxbx_" + rowNumber + "").val(amount);
    sum();
})

function removeRows() {
    var row = $(".itemsSection .itemRow");
    row.remove();
}
$(document).on("click", ".reports .fa-times", function () {
   
    var row = $(this).parents(".itemRow");
    row.remove();

    sum();
    assignSerialNumber();
});

function focusToInGredientTxbx() {
    loadIngredients();
    $('.IngredientDescription').focus();
    $('.IngredientDescription').select();
}

function focusToQtyTxbx() {
   
    $('.IngredientQty').focus();
    $('.IngredientQty').select();

}
function loadIngredients() {
    var ItemCode = $('.MainItemCode').val();
    removeRows();
    $.ajax({
        url: "/WebPOSService.asmx/GetIngredients",
        type: "POST",
        data: { mainItemCOde: ItemCode },
        success: function (response) {
            if (response.length > 0) {
                for (var i = 0; i < response.length; i++) {

                
                var rowNumber = $(".itemsSection .itemRow").length > 0 ? $(".itemsSection .itemRow").last().data().rownumber + 1 : 1;

                $("<div class='itemRow' data-rownumber='" + rowNumber + "'>" +

                    "<div class='sixteen text-align-left'><span class='serialNumber '>" + rowNumber+"</span></div>" +
                    "<div class='sixteen name'><input name='Code'  id='IngredientCode_" + rowNumber + "' disabled value='" + response[i].Code + "' /></div>" +
                    "<div class='sixteen name'><input name='Description'  id='IngredientDescription_" + rowNumber + "' disabled value='" + response[i].Description + "'   /></div>" +
                    "<div class='sixteen'><input type='number' data-code='" + response[i].Code +"' name='Qty' id='QtyTxbx_" + rowNumber + "'  value='" + response[i].Qty + "'  /></div>" +
                    "<div class='sixteen'><input type='number' name='Rate' type='text' id='CostTxbx_" + rowNumber + "' disabled value='" + response[i].Rate + "'  /></div>" +
                    "<div class='sixteen'><input type='number' name='Amount' type='text' id='AmountTxbx_" + rowNumber + "' disabled value='" + response[i].Amount + "'  /></div>" +
                    
                    "<div class='float-right'><i class='fas fa-times'></i></div>" +

                    "</div>").appendTo(".BMtable .itemsSection");
                }
                sum();
            }
            
        }
    })
   
   
}