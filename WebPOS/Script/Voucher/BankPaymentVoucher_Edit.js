﻿$(() => {
    var InvoiceNumber = getQueryString("v");

    if (InvoiceNumber != "" || InvoiceNumber != null) {
        $(".VoucherNo").val(InvoiceNumber);
        GetVoucherData();
    }
})
function insertBankPaymentVoucherRow() {
    var glCode = $(".selectedAllVouchersCode").val();
    var glTitle = $(".selectedAllVouchersTitle").val();
    var model = {
        Code: glCode,
        Title: glTitle
    };
    appendVRow(model);
    var lastRowNumber = isNaN(Number($(".debit-table .item-list.data-list .item-data:last-child").data("rownumber"))) ? 1 : Number($(".debit-table .item-list.data-list .item-data:last-child").data("rownumber"));
    $(`#chequeNumberTextbox_${lastRowNumber}`).focus().select();

}
function appendVRow(model) {
    var glCode = model.Code;
    var glTitle = model.Title;
    var ChequeNumber = model.ChequeNumber == null ? "" : model.ChequeNumber;
    var Narration = model.Narration == null ? "" : model.Narration;
    var Debit = model.Debit == null ? "" : model.Debit; 

    var rowNumber = isNaN(Number($(".debit-table .item-list.data-list .item-data:last-child").data("rownumber"))) ? 1 : Number($(".debit-table .item-list.data-list .item-data:last-child").data("rownumber")) + 1;
    var row = `<li class="item item-data" data-rownumber='${rowNumber}'>
        <div class="item-row  pl-3 pr-2">
            <div class="item-col flex-4">
                <div class="item-heading">Code</div>
                <div>
                    ${glCode} 
                    <input type="hidden" name="Code" value="${glCode}">
                </div>
            </div>
            <div class="item-col flex-5 no-overflow">
                <div>
                    <a class="">
                        <h4 class="item-title no-wrap">
                            ${glTitle}  
                            <input type="hidden" name="Description" value="${glTitle}">
                        </h4>
                    </a>
                </div>
            </div>
            <div class="item-col flex-4 no-overflow">
                <div>
                   <input id="chequeNumberTextbox_${rowNumber}" type='text' class="form-control" value='${ChequeNumber}' name='ChequeNumber' data-nextfocus="#narrationTextbox_${rowNumber}" />
                </div>
            </div>
            <div class="item-col flex-4 no-overflow">
                <div>
                   <input  type='text' class="form-control" name='Narration' value='${Narration}' id='narrationTextbox_${rowNumber}'/>
                </div>
            </div>
            <div class="item-col flex-4 no-overflow">
                <div>
                   <input name='Debit' value='${Debit}' type='number'  class="text-right px-2 form-control"  data-nextfocus=".AllVouchersTitle" id='debitTextbox_${rowNumber}'  />
                </div>
            </div>

            <div class="item-col item-col-date flex-3">
                <div>
                    <i onclick="deleteItem(this)" class="fas fa-times mr-3"></i>
                </div>
            </div>
        </div>
    </li>`;
    $(".debit-table .item-list.data-list").append(row);



    sum("Debit");
    sum("Credit");
}
function deleteItem(btn) {
    var row = $(btn).parents(".item-data");
    row.remove();
    sum("Debit");
}
$(document).on("keydown", "[name='Narration']", function (e) {
    var row = this.id.split("_")[1];

    if (e.keyCode == 13) {
        $("#debitTextbox_" + row + "").focus();
        $("#debitTextbox_" + row + "").select();
    }

    
});

$(document).on("keydown", "[name='Debit']", function (e) {
    var row = this.id.split("_")[1];

    if (e.keyCode == 13 && $("#debitTextbox_" + row + "").val().trim() == "")
        $("#creditTextbox_" + row + "").focus();
    else if (e.keyCode == 13 && $("#debitTextbox_" + row + "").val().trim() != "" && $("#creditTextbox_" + row + "").val().trim() != "") {
        $("#creditTextbox_" + row + "").focus();
        $("#creditTextbox_" + row + "").select();
    }
    else if (e.keyCode == 13 && $("#debitTextbox_" + row + "").val().trim() != "")
    {
        $(".AllVouchersTitle").focus();
        $(".AllVouchersTitle").select();
    }

    sum("Debit");
    sum("Credit");

});
$(document).on("input", "[name='Debit']", function (e) {

    sum("Debit");

});
$(document).on("keydown", "[name='Credit']", function (e) {
 
    if (e.keyCode == 13) {
        $(".AllVouchersTitle").focus();
        $(".AllVouchersTitle").select();
    }
    sum("Credit");

})
$(document).on("input", "[name='Credit']", function () {
    
    sum("Credit");

})
function assignSerialNumber() {
    var serialNumbers = $(".serialNumber");

    serialNumbers.each(function (index, element) {
        $(element).text(index + 1);
    })

}
function sum(name) {

    var sumValue = 0;
    var values = $("[name=" + name + "]");
    values.each(function (index, element) {
        var value = $(element).val().trim() == "" ? 0 : $(element).val();
        sumValue += Number(value);
    });

    $("[name=total" + name + "]").val(sumValue);

    if (name == "Debit") {
        $("[name=totalCredit]").val(sumValue);
        $("[name=Credit]").val(sumValue);
    }

}

function save() {

    if ($("[name=totalCredit]").val() == $("[name=totalDebit]").val() && $(".CreditGLCode").val().trim().length) {

        swal({
            title: "Business Manager",
            text: "Are You Sure to save a transaction?",
            type: 'question',
            showCancelButton: true,
            onfirmButtonText: "Yes, Save it!",
            cancelButtonText: "No, cancel please!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {

                    resolve();
                    saveVoucher();
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });

    }
    else if (!$(".CreditGLCode").val().trim().length) {
        swal("Select Bank Account", "", "error");
    }
    else {
        swal("Debit amount is not equal to credit amount", "", "error");
    }
}
function saveVoucher() {
    var serialNumbers = $(".debit-table .item-list.data-list .item-data");

    var addedRows = [];
    serialNumbers.each(function (index, element) {

        var row = $(element);
      
        var rowmodel = {
            Code: row.find("[name=Code]").val(),
            Title: row.find("[name=Description]").val(),
            ChequeNumber: row.find("[name=ChequeNumber]").val(),
            Narration: row.find("[name=Narration]").val(),
            Debit: row.find("[name=Debit]").val().trim() == "" ? "0" : row.find("[name=Debit]").val(),
        }
        addedRows.push(rowmodel);
    });
    var VoucherNumber = $(".VoucherNumber").val();
    var VoucherDate = $(".VoucherDate").val();
    var CreditGLCode = $(".CreditGLCode").val();
    var CreditGLTitle = $(".CreditGLTitle").val();
    var CreditNarration = $(".CreditNarration").val();
    var CreditAmount = $(".CreditAmount").val();
    var voucherModel = {
        VoucherNumber: VoucherNumber,
        Rows: addedRows,
        VoucherDate: VoucherDate,
        CreditGLCode: CreditGLCode,
        CreditGLTitle: CreditGLTitle,
        CreditAmount: CreditAmount,
        CreditNarration: CreditNarration,
    };
    $.ajax({
        url: location.pathname + "/Save",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ model: voucherModel }),
        success: function (response) {
            if (response.d.Success) {
                $(".VoucherNumber").val(response.d.LastVoucherNumber);
                clearInputs();
                updateInputStyle();
                swal("Success", response.d.Message, "success");
            }
            else {
                swal("Error", response.d.Message, "error");

            }


        }



    })
}
$(document).on("click", "#saveBtn", function () {
    save();
});
$(document).on("keydown",  function (e) {

   
    if (e.keyCode == 13 && e.keyCode == 16) {
        save();
    }
   

})
$(document).on("click", ".reports .fa-times", function () {
    var row = $(this).parents(".itemRow");
    row.remove();


    sum("Credit");
    sum("Debit");
    assignSerialNumber();
});
function  clearInputs() {
    $(".debit-table .item-list.data-list  .item-data").remove();
    $(".credit-table input").val("");
    sum("Credit");
    sum("Debit");
}

function GetVoucherData() {
    if ($(".VoucherNumber").val().trim() != "") {
        $.ajax({
            url: '/Services/VoucherService.asmx/GetBankPaymentVoucherData',
            type: "Post",

            data: { "VoucherNo": $(".VoucherNumber").val() },
            success: function (ModelPaymentVoucher) {
                if (ModelPaymentVoucher.Success) {
                    debugger
                    clearInputs();
                    for (var i = 0; i < ModelPaymentVoucher.Rows.length; i++) {
                        appendVRow(ModelPaymentVoucher.Rows[i])
                    }
                    autoFill(ModelPaymentVoucher);
                    sum("Credit");
                    sum("Debit");
                  
                    updateInputStyle();

                } else {
                    smallSwal("Error", ModelPaymentVoucher.Message, "error");
                }
                $("#isLastVoucher").remove();
                $("<input id='isLastVoucher' value='"+ModelPaymentVoucher.IsLastVoucherNo+"'>").appendTo('body');
                manageNextPreviousBtn();
                $(".fa-spinner").removeClass('fa-spin');
            },
            fail: function (jqXhr, exception) {

            }
        });

    }
    else {
        swal("", "Enter Voucher Number", "error");
    }
}

function deleteVoucher() {
    swal({
        title: "Delete this transaction?",
        html: "Are You Sure to Delete this transaction?<br><br><strong class='text-center'>Type 'delete' to Delete this transation.</strong>",
        type: 'question',
        input: 'text',
        showCancelButton: true,
        onfirmButtonText: "Yes, Delete it!",
        cancelButtonText: "No, cancel please!",
        showLoaderOnConfirm: true,
        closeOnConfirm: false,
        closeOnCancel: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {
                if (text.toLowerCase() == 'delete') {
                    var ModelPaymentVoucher = {

                         VoucherNumber : $(".VoucherNumber").val(),
                         VoucherDate : $(".VoucherDate").val(),
                         CreditGLCode : $(".CreditGLCode").val()
                    }
                    debugger
                    $.ajax({
                        url: location.pathname + "/Delete",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify({ ModelPaymentVoucher: ModelPaymentVoucher }),
                        success: function (BaseModel) {
                            debugger
                            if (BaseModel.d.Success) {
                           
                                smallSwal("Success", BaseModel.d.Message, "success");
                                 clearInputs();
                                updateInputStyle();
                            }
                            else {
                                swal("Error", BaseModel.d.Message, "error");
                            }
                        }
                    })
                } else {
                    Swal.showValidationError(
                        `Type correct spelling of 'delete'`
                    );
                    resolve();
                }
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });
}