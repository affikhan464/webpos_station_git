﻿
function SaveBefore() {

    var SaveOk = "Yes";
    var InvoiceNumber = $(".InvoiceNumber").val();
    var Code = $(".GLHeadCode").val();
    var OpeningBalance = $(".OpeningBalance").val();

    if (Code == "") {
        SaveOk = "No";
        swal("Error", "Select GL Title again!!", "error");
    }

    if (OpeningBalance == "") {
        SaveOk = "No";
        swal("Error", "Type Opening Balance", "error");
    }

    if (SaveOk == "Yes") {

        swal({
            title: "Business Manager",
            text: "Are You Sure to save a transaction?",
            type: 'question',
            showCancelButton: true,
            onfirmButtonText: "Yes, Save it!",
            cancelButtonText: "No, cancel please!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {
                    
                    save();
                    
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });

        
    }

}




$(document).ready(function () {

    $(".CashPaid").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            $(".Narration").focus();
        }
    });
    $(".Narration").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            SaveBefore();
        }
    });
});


function save()
{
    var ModelPaymentVoucher = {
          PartyCode: $(".GLHeadCode").val(),
          PartyName: $(".GLHeadTitle").val(),
        OpeningBalance: $(".OpeningBalance").val(),
        Narration: $(".Narration").val()
    }
    $.ajax({
        url: "/Transactions/GLOpeningBalance.aspx/Save",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ ModelPaymentVoucher: ModelPaymentVoucher }),
        success: function (BaseModel) {
            if (BaseModel.d.Success) {
                smallSwal("Success", BaseModel.d.Message, "success");
                $(".empt").val("");
                updateInputStyle();
                $(".GLHeadTitle").focus();
            }
            else {
                swal("Error", BaseModel.d.Message, "error");
            }
        }
    })
}
