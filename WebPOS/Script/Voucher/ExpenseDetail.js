﻿function getExpenseDetails(btn) {

    var stationId = btn.dataset.stationid;
    var isMultiStation = false;

    if ($("#stationCheck").length) {
        if ($("#stationCheck").prop("checked")) {
            stationId = $("#StationDD .dd-selected-value").val();
            isMultiStation = false;
        } else {
            isMultiStation = true;
            stationId = 0;
        }
    }
    var expenses = btn.dataset.expenseamount;

    var from = $(".fromTxbx").val();
    var to = $(".toTxbx").val();
    $.ajax({
        url: '/WebPOSService.asmx/GetExpenseDetails',
        type: "POST",

        data: { "from": $(".fromTxbx").val(), "to": $(".toTxbx").val(), "stationId": stationId },

        success: function (items) {

            var expenseAmount = expenses.split('.');
            if (!isMultiStation) {
                $("#ExpenseDetailModal .subName").text(items[0].StationName);
            } else {
                $("#ExpenseDetailModal .subName").text("");
                $("#stationHead").remove();
                var td = `<th id='stationHead'>Station</th>`;
                if (!$("#stationHead").length) {
                    $(td).insertAfter("#dateHead");

                }
            }
            $("#ExpenseDetailModal .expenseAmount").text(expenseAmount[0]);
            $("#ExpenseDetailModal .cent").text('.' + expenseAmount[1]);
            $("#ExpenseDetailModal .separateReport").attr('href', "/Reports/PaymentExpenceDrawings/StationWiseExpenceDetail.aspx?from=" + from + "&to=" + to + "&stationId=" + stationId);
            $("#ExpenseDetailModal .expensesRows").html('');
            
            for (var i = 0; i < items.length; i++) {
                var stationName = '';
                    if (isMultiStation) {
                        stationName = `<td>${items[i].StationName}</td>`;
                    }
                    var row = `  <tr>
                                <td>${items[i].Date}</td>
                                ${stationName}
                                <td><a class='text-info'  target='_blank' href='/Correction/ExpenseEntryEdit.aspx?v=${items[i].VoucherNo}'>${items[i].VoucherNo}</a></td>
                                <td>${items[i].ExpenseHead}</td>
                                <td>${items[i].Narration}</td>
                                <td>${items[i].CashPaid}</td>
                            </tr>
                        `;
                    $("#ExpenseDetailModal .expensesRows").append(row);
                }
            $("#ExpenseDetailModal").modal();
        },
        error: function (error) {
            swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

        }
    });
}