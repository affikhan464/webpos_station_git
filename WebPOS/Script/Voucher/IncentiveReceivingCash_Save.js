﻿
function SaveBefore() {

    var SaveOk = "Yes";
    var InvoiceNumber = $(".InvoiceNumber").val();
    var Code = $(".PartyCode").val();
    var CashPaid = $(".CashPaid").val();

    if (Code == "") {
        SaveOk = "No";
        swal("Error", "Select Party", "error");
    }

    if (CashPaid == "") {
        SaveOk = "No";
        swal("Error", "Type Amount", "error");
    }

    if (SaveOk == "Yes") {

        swal({
            title: "Business Manager",
            text: "Are You Sure to save a transaction?",
            type: 'question',
            showCancelButton: true,
            onfirmButtonText: "Yes, Save it!",
            cancelButtonText: "No, cancel please!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {
                    save();
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });
    }

}





function save()
{
    var ModelPaymentVoucher = {
        VoucherNo: $(".VoucherNo").val(),
        Date: $(".Date").val(),
        PartyCode: $(".PartyCode").val(),
        PartyName: $(".PartyName").val(),
        CashPaid: $(".CashPaid").val(),
        Narration: $(".Narration").val()
    }

    $.ajax({
        url: "/Transactions/IncentiveReceivingCash.aspx/SaveIncentiveInComeCash",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ ModelPaymentVoucher: ModelPaymentVoucher }),
        success: function (BaseModel) {
            if (BaseModel.d.Success) {
                var date = $(".Date").val();
                smallSwal("Success", BaseModel.d.Message, "success");
                //alert(BaseModel.d.Message);
                $(".empt").val("");
                $(".VoucherNo").val(BaseModel.d.LastInvoiceNumber);
                $(".Date").val(date);
                $(".PartyName").focus();
                updateInputStyle();
                addRow("#CurrentSession", BaseModel.d.LastInvoiceNumber, ModelPaymentVoucher.Date, ModelPaymentVoucher.PartyName, ModelPaymentVoucher.CashPaid, ModelPaymentVoucher.Narration);
                addRow("#Last5Days", BaseModel.d.LastInvoiceNumber, ModelPaymentVoucher.Date, ModelPaymentVoucher.PartyName, ModelPaymentVoucher.CashPaid, ModelPaymentVoucher.Narration);
            }
            else {
                smallSwal("Error", BaseModel.d.Message, "error");
            }
        }
    })
}


$(document).ready(function () {

    $(".CashPaid").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            $(".Narration").focus();
        }
    });
    $(".Narration").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            SaveBefore();
        }
    });
});

function calculation() {
    
    var Balance = $(".PartyBalanceActual").val();
    var Paid = $(".CashPaid").val();
    
    Balance = isNaN(Balance) ? 0 : Number(Balance);
    Paid = isNaN(Paid) ? 0 : Number(Paid);

    var NewBalance = 0;
    if (Paid == "") { Paid = 0; }
    NewBalance = (Balance - Paid);
    //$(".PartyBalance").val(NewBalance);


}

$(document).ready(function () {
    getLastVouchers();
});
function rerenderSerialNumber() {
    var rows = $("#CurrentSession .item-list.contentList .SNo");
    var sno = 0;
    rows.each(function (index, element) {
        sno += 1;
        $(element).html(sno);
    })

    var rows = $("#Last5Days .item-list.contentList .SNo");
    var sno = 0;
    rows.each(function (index, element) {
        sno += 1;
        $(element).html(sno);
    })
    
    var rows = $("#PartyLast5Days .item-list.contentList .SNo");
    var sno = 0;
    rows.each(function (index, element) {
        sno += 1;
        $(element).html(sno);
    })
}
function getLastVouchers() {
    $.ajax({
        url: '/Transactions/IncentiveReceivingCash.aspx/GetLastVouchers',
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var reports = response.d.Reports;

            if (response.d.Success) {
                if (reports == null) {
                    $(".loader").hide();

                }
                else {
                    for (var i = 0; i < reports.length; i++) {
                        addRow("#Last5Days", reports[i].VoucherNumber, reports[i].Date, reports[i].PartyName, reports[i].CashPaid, reports[i].Narration);
                    }
                }
            }
            else {
                $(".loader").hide();
            }
        },
        error: function (error) {
            swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

        }
    });
}

function getPartiesLastVouchers() {
    $("#PartyLast5Days .item-list.contentList li").remove();
    if (!$(".PartyCode").val().length) {
        smallSwal("Error", "Select Party First!!", "error");
        $(".PartyName").focus().select();
        $(".slider ~ .row ").slideDown();
        $(".slider").addClass("fa-chevron-up");
    } else {
        $(".slider ~ .row ").slideUp();
        $(".slider").removeClass("fa-chevron-up");
        $.ajax({
            url: '/Transactions/IncentiveReceivingCash.aspx/GetPartyLastVouchers',
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ partyCode: $(".PartyCode").val() }),
            success: function (response) {
                var reports = response.d.Reports;

                if (response.d.Success) {
                    if (reports == null) {
                        $(".loader").hide();

                    }
                    else {
                        

                        for (var i = 0; i < reports.length; i++) {

                            addRow("#PartyLast5Days", reports[i].VoucherNumber, reports[i].Date, reports[i].PartyName, reports[i].CashPaid, reports[i].Narration);
                        }
                    }
                }
                else {
                    $(".loader").hide();
                }
            },
            error: function (error) {
                swal("Error", error.statusText, "error"); $(".loader").hide();

            }
        });
    }
}

$(document).on('click', '.slider', function () {
    $(this).siblings(".row").slideToggle();
    $(this).toggleClass("fa-chevron-up");
})
function addRow(table, VoucherNo, date, PartyName, CashPaid, Narration) {
    var SrNo = 0;

    var row = $(`<li data-rownumber='${SrNo}' class="item item-data">
                                <div class="item-row">
                                    <div class="item-col flex-1">
                                        <div class="item-heading">Sr. No.</div>
                                        <div class='SNo ${SrNo}_SrNo'>${SrNo}</div>
                                    </div>
                                    <div class="item-col flex-1 no-overflow">
                                        <div>
                                            <div class="item-heading">Voucher#</div>
                                            <div class="${SrNo}_VoucherNo" >${VoucherNo}</div>
                                        </div>
                                    </div>
                                    <div class="item-col flex-1">
                                        <div class="item-heading">Date</div>
                                        <div class="${SrNo}_Date" >${date}</div>
                                    </div>
                                    <div class="item-col flex-2 no-overflow">
                                        <div>
                                            <a class="">
                                                <h4 class="item-title no-wrap ${SrNo}_PartyName">${PartyName} </h4>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="item-col flex-1">
                                        <div class="item-heading">Cash Paid</div>
                                        <div class="${SrNo}_CashPaid" >${CashPaid}</div>
                                    </div>
                                    <div class="item-col flex-3">
                                        <div class="item-heading">Narration</div>
                                        <div class="${SrNo}_Narration">${Narration} </div>
                                    </div>
                                    <div class="item-col item-col-date flex-1">
                                        <div>
                                           <a target='_blank' href='/Correction/IncentiveReceivingCashEdit.aspx?v=${VoucherNo}'><i data-rownumber="${SrNo}" class="fas fa-edit mr-3"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </li>`);

    var ul = $(table + " .item-list.contentList");
    if ($(table + " .item-list.contentList li:first").length) {
        $(row).insertBefore(table + " .item-list.contentList li:first");
    } else {
        ul.append(row);
    }
    rerenderSerialNumber();

}