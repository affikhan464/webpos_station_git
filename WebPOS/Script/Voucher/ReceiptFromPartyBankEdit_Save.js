﻿function GetVoucherData() {
    if ($(".VoucherNo").val().trim() != "") {
        $.ajax({
            url: '/WebPOSService.asmx/GetBankReceiptVoucherData',
            type: "Post",

            data: { "VoucherNo": $(".VoucherNo").val() },

            success: function (ModelPaymentVoucher) {
                if (ModelPaymentVoucher.Success) {
                    $(".Date").val(ModelPaymentVoucher.Date);
                    $(".PartyCode").val(ModelPaymentVoucher.PartyCode);
                    $(".PartyCodeOld").val(ModelPaymentVoucher.PartyCode);
                    $(".PartyName").val(ModelPaymentVoucher.PartyName);
                    $(".PartyAddress").val(ModelPaymentVoucher.Address);
                    $(".CashPaid").val(Number(ModelPaymentVoucher.CashPaid).toFixed(2));
                    $(".ExpenceHeadAccountNo ").val(ModelPaymentVoucher.AccountNo);
                    $("#expenceTextbox").val(ModelPaymentVoucher.BankName);
                    $(".BankCode").val(ModelPaymentVoucher.BankCode);
                    $(".CheqNo").val(ModelPaymentVoucher.ChqNo);
                    $(".Narration").val(ModelPaymentVoucher.Narration);
                    $(".PartyBalance").val(Number(ModelPaymentVoucher.Balance).toFixed(2));


                    updateInputStyle();
                    calculation();

                } else {
                    smallSwal("Oppsss ", ModelPaymentVoucher.Message, "error")
                }
                $("#isLastVoucher").val(ModelPaymentVoucher.IsLastVoucherNo);
                manageNextPreviousBtn();
                $(".fa-spinner").removeClass('fa-spin');
            },
            fail: function (jqXhr, exception) {

            }
        });

    }
    else {
        swal("", "Enter Voucher Number", "error");
    }

}

function SaveBefore() {



    var SaveOk = "Yes";
    var BankCode = $(".BankCode").val();
    var PartyCode = $(".PartyCode").val();
    var CashPaid = $(".CashPaid").val();

    if (PartyCode == "") {
        SaveOk = "No";
        swal("Error", "Select Party", "error");
    }



    if (BankCode == "") {
        SaveOk = "No";
        swal("Error", "Select Bank", "error");
    }
    if (CashPaid == "") {
        SaveOk = "No";
        swal("Error", "Type Amount", "error");
    }

    if (SaveOk == "Yes") {

        swal({
            title: "Business Manager",
            text: "Are You Sure to save a transaction?",
            type: 'question',
            showCancelButton: true,
            onfirmButtonText: "Yes, Save it!",
            cancelButtonText: "No, cancel please!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {

                    save();
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });
    }

}





function save() {

    var ModelPaymentVoucher = {
        VoucherNo: $(".VoucherNo").val(),
        Date: $(".Date").val(),
        PartyCode: $(".PartyCode").val(),
        PartyName: $(".PartyName").val(),
        CashPaid: $(".CashPaid").val(),
        Address: $(".PartyAddress").val(),
        BankCode: $(".BankCode").val(),
        BankName: $(".BankTitle").val(),
        Narration: $(".Narration").val(),
        ChqNo: $(".CheqNo").val()

    }

    debugger
    $.ajax({
        url: "/Transactions/ReceiptFromPartyThroughBankEdit.aspx/Save",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ ModelPaymentVoucher: ModelPaymentVoucher }),
        success: function (BaseModel) {
            debugger
            if (BaseModel.d.Success) {
                var date = $(".Date").val();
                smallSwal("Success", BaseModel.d.Message, "success");
                //alert(BaseModel.d.Message);
                $(".empt").val("");
                $(".VoucherNo").val(BaseModel.d.LastInvoiceNumber);
                updateInputStyle();
                $(".Date").val(date);
                $(".BankTitle").focus();
            }
            else {
                swal("Error", BaseModel.d.Message, "error");
            }
        }
    })
}

$(document).ready(function () {

    $(".CashPaid").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            $(".Narration").focus();
        }
    });
    $(".Narration").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            SaveBefore();
        }
    });
});

function deleteVoucher() {
    swal({
        title: "Business Manager",
        text: "Are You Sure to Delete this transaction?",
        type: 'question',
        showCancelButton: true,
        onfirmButtonText: "Yes, Delete it!",
        cancelButtonText: "No, cancel please!",
        showLoaderOnConfirm: true,
        closeOnConfirm: false,
        closeOnCancel: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {

                var ModelPaymentVoucher = {
                    VoucherNo: $(".VoucherNo").val(),
                    Date: $(".Date").val(),
                    PartyCode: $(".PartyCode").val(),
                    PartyName: $(".PartyName").val(),
                    CashPaid: $(".CashPaid").val(),
                    Address: $(".PartyAddress").val(),
                    BankCode: $(".BankCode").val(),
                    BankName: $(".BankTitle").val(),
                    Narration: $(".Narration").val(),
                    ChqNo: $(".CheqNo").val()

                }

                debugger
                $.ajax({
                    url: "/Transactions/ReceiptFromPartyThroughBankEdit.aspx/Delete",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ ModelPaymentVoucher: ModelPaymentVoucher }),
                    success: function (BaseModel) {
                        debugger
                        if (BaseModel.d.Success) {
                            var date = $(".Date").val();
                            smallSwal("Success", BaseModel.d.Message, "success");
                            //alert(BaseModel.d.Message);
                            $(".empt").val("");
                            updateInputStyle();
                            $(".Date").val(date);
                $(".BankTitle").focus();
                        }
                        else {
                            swal("Error", BaseModel.d.Message, "error");
                        }
                    }
                });
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });
}
function clearInvoice() {
    $("#myTable tbody tr").remove();
    $("#Table_OldData tbody tr").remove();

    $(".inv1").val("");
    $(".inv3").val("0");


    //$("input").val("0");
    //calculationSale();
}