﻿
function SaveBefore() {



    var SaveOk = "Yes";
    var BankCode = $(".BankCode").val();
    var PartyCode = $(".PartyCode").val();
    var CashPaid = $(".CashPaid").val();

    if (PartyCode == "") {
        SaveOk = "No";
        swal("Error", "Select Party", "error");
    }



    if (BankCode == "") {
        SaveOk = "No";
        swal("Error", "Select Bank", "error");
    }
    if (CashPaid == "") {
        SaveOk = "No";
        swal("Error", "Type Amount", "error");
    }

    if (SaveOk == "Yes") {

        swal({
            title: "Business Manager",
            text: "Are You Sure to save a transaction?",
            type: 'question',
            showCancelButton: true,
            onfirmButtonText: "Yes, Save it!",
            cancelButtonText: "No, cancel please!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {

                    save();
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });
    }

}





function save() {

    var ModelPaymentVoucher = {
        VoucherNo: $(".VoucherNo").val(),
        Date: $(".Date").val(),
        PartyCode: $(".PartyCode").val(),
        PartyName: $(".PartyName").val(),
        CashPaid: $(".CashPaid").val(),
        Address: $(".PartyAddress").val(),
        BankCode: $(".BankCode").val(),
        BankName: $(".BankTitle").val(),
        Narration: $(".Narration").val(),
        ChqNo: $(".CheqNo").val()

    }

    debugger
    $.ajax({
        url: "/Correction/PaymentToPartyThroughBankEdit.aspx/Save",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ ModelPaymentVoucher: ModelPaymentVoucher }),
        success: function (BaseModel) {
            debugger
            if (BaseModel.d.Success) {
                var date = $(".Date").val();
                smallSwal("Success", BaseModel.d.Message, "success");
                //alert(BaseModel.d.Message);
                $(".empt").val("");
                $(".VoucherNo").val(BaseModel.d.LastInvoiceNumber);
                updateInputStyle();
                $(".Date").val(date);
                $(".BankTitle").focus();
            }
            else {
                swal("Error", BaseModel.d.Message, "error");
            }
        }
    })
}

$(document).ready(function () {

    $(".CashPaid").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            $(".Narration").focus();
        }
    });
    $(".Narration").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            SaveBefore();
        }
    });
});

function deleteVoucher() {
    swal({
        title: "Delete this transaction?",
        html: "Are You Sure to Delete this transaction?<br><br><strong class='text-center'>Type 'delete' to Delete this transation.</strong>",
        type: 'question',
        input: 'text',
        showCancelButton: true,
        onfirmButtonText: "Yes, Delete it!",
        cancelButtonText: "No, cancel please!",
        showLoaderOnConfirm: true,
        closeOnConfirm: false,
        closeOnCancel: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {
                if (text.toLowerCase() == 'delete') {
                    var ModelPaymentVoucher = {
                        VoucherNo: $(".VoucherNo").val(),
                        Date: $(".Date").val(),
                        PartyCode: $(".PartyCode").val(),
                        PartyName: $(".PartyName").val(),
                        CashPaid: $(".CashPaid").val(),
                        Address: $(".PartyAddress").val(),
                        BankCode: $(".BankCode").val(),
                        BankName: $(".BankTitle").val(),
                        Narration: $(".Narration").val(),
                        ChqNo: $(".CheqNo").val()

                    }

                    debugger
                    $.ajax({
                        url: "/Correction/PaymentToPartyThroughBankEdit.aspx/Delete",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify({ ModelPaymentVoucher: ModelPaymentVoucher }),
                        success: function (BaseModel) {
                            debugger
                            if (BaseModel.d.Success) {
                                var date = $(".Date").val();
                                smallSwal("Success", BaseModel.d.Message, "success");
                                //alert(BaseModel.d.Message);
                                $(".empt").val("");
                                updateInputStyle();
                                $(".Date").val(date);
                                $(".BankTitle").focus();
                            }
                            else {
                                swal("Error", BaseModel.d.Message, "error");
                            }
                        }
                    })
                } else {
                    Swal.showValidationError(
                        `Type correct spelling of 'delete'`
                    );
                    resolve();
                }
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });
}
function GetVoucherData() {
    if ($(".VoucherNo").val().trim() != "") {
        $.ajax({
            url: '/WebPOSService.asmx/GetPaymentToPartyBankVoucherData',
            type: "Post",

            data: { "VoucherNo": $(".VoucherNo").val() },
            success: function (ModelPaymentVoucher) {
                if (ModelPaymentVoucher.Success) {
                    debugger
                    $(".Date").val(ModelPaymentVoucher.Date);
                    $(".BankCode").val(ModelPaymentVoucher.BankCode);
                    $(".BankTitle").val(ModelPaymentVoucher.BankName);
                    $(".PartyCode").val(ModelPaymentVoucher.PartyCode);
                    $(".PartyName").val(ModelPaymentVoucher.PartyName);
                    $(".PartyAddress").val(ModelPaymentVoucher.Address);
                    $(".CheqNo").val(ModelPaymentVoucher.ChqNo);

                    $(".AccountNo").val(ModelPaymentVoucher.AccountNo);
                    $(".CashPaid").val(ModelPaymentVoucher.CashPaid);
                    $(".Narration").val(ModelPaymentVoucher.Narration);
                    updateInputStyle();

                } else {
                    smallSwal("Error", ModelPaymentVoucher.Message, "error");
                }
                $("#isLastVoucher").val(ModelPaymentVoucher.IsLastVoucherNo);
                manageNextPreviousBtn(); $(".fa-spinner").removeClass('fa-spin');
            },
            fail: function (jqXhr, exception) {

            }
        });

    }
    else {
        swal("", "Enter Voucher Number", "error");
    }
}