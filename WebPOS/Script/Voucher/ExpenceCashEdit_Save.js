﻿

function SaveBefore() {

    var SaveOk = "Yes";
    var InvoiceNumber = $(".InvoiceNumber").val();
    var Code = $(".Code").val();
    var CashPaid = $(".CashPaid").val();

    if (Code == "") {
        SaveOk = "No";
        swal("Error", "Select Expence Head", "error");
    }

    if (CashPaid == "") {
        SaveOk = "No";
        swal("Error", "Type Amount", "error");
    }

    if (SaveOk == "Yes") {

        swal({
            title: "Business Manager",
            text: "Are You Sure to save a transaction?",
            type: 'question',
            showCancelButton: true,
            onfirmButtonText: "Yes, Save it!",
            cancelButtonText: "No, cancel please!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {

                    save();
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });
    }

}





function save() {
    var ModelPaymentVoucher = {
        VoucherNo: $(".VoucherNo").val(),
        Date: $(".Date").val(),
        PartyCode: $(".ExpenceHeadCode").val(),
        PartyName: $(".ExpenceHeadTitle").val(),
        CashPaid: $(".CashPaid").val(),
        StationId: $("[name=StationDD]").val(),
        //Address: $(".PartyAddress").val(),
        Narration: $(".Narration").val()
    }
    debugger
    $.ajax({
        url: "/Correction/ExpenseEntryEdit.aspx/SaveExpenceVoucherEdit",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ ModelPaymentVoucher: ModelPaymentVoucher }),
        success: function (BaseModel) {
            debugger
            if (BaseModel.d.Success) {
                var date = $(".Date").val();
                smallSwal("Success", BaseModel.d.Message, "success");
                //alert(BaseModel.d.Message);
                $(".empt").val("");
                $(".VoucherNo").val(BaseModel.d.LastInvoiceNumber);
                $(".Date").val(date);
                updateInputStyle();
                $(".ExpenceHeadTitle").focus();
            }
            else {
                swal("Error", BaseModel.d.Message, "error");
            }
        }
    })
}

$(document).ready(function () {

    $(".CashPaid").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            $(".Narration").focus();
        }
    });
    $(".Narration").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            SaveBefore();
        }
    });
});

function deleteVoucher() {
    swal({
        title: "Delete this transaction?",
        html: "Are You Sure to Delete this transaction?<br><br><strong class='text-center'>Type 'delete' to Delete this transation.</strong>",
        type: 'question',
        input: 'text',
        showCancelButton: true,
        onfirmButtonText: "Yes, Delete it!",
        cancelButtonText: "No, cancel please!",
        showLoaderOnConfirm: true,
        closeOnConfirm: false,
        closeOnCancel: true,
        preConfirm: (text) => {
            return new Promise((resolve) => {
                if (text.toLowerCase() == 'delete') {
                    var ModelPaymentVoucher = {
                        VoucherNo: $(".VoucherNo").val(),
                        Date: $(".Date").val(),
                        PartyCode: $(".ExpenceHeadCode").val(),
                        PartyName: $(".ExpenceHeadTitle").val(),
                        CashPaid: $(".CashPaid").val(),
                        //Address: $(".PartyAddress").val(),
                        Narration: $(".Narration").val()
                    }
                    debugger
                    $.ajax({
                        url: "/Correction/ExpenseEntryEdit.aspx/Delete",
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: JSON.stringify({ ModelPaymentVoucher: ModelPaymentVoucher }),
                        success: function (BaseModel) {
                            debugger
                            if (BaseModel.d.Success) {
                                var date = $(".Date").val();
                                smallSwal("Success", BaseModel.d.Message, "success");
                                //alert(BaseModel.d.Message);
                                $(".empt").val("");
                                $(".Date").val(date);
                                updateInputStyle();
                                $(".ExpenceHeadTitle").focus();
                            }
                            else {
                                swal("Error", BaseModel.d.Message, "error");
                            }
                        }
                    })
                } else {
                    Swal.showValidationError(
                        `Type correct spelling of 'delete'`
                    );
                    resolve();
                }
            })
        },
        allowOutsideClick: () => !swal.isLoading()
    });
}
function clearInvoice() {
    $("#myTable tbody tr").remove();
    $("#Table_OldData tbody tr").remove();

    $(".inv1").val("");
    $(".inv3").val("0");


    //$("input").val("0");
    //calculationSale();
}


function GetVoucherData() {
    if ($(".VoucherNo").val().trim() != "") {
        $(".empt").val("");
        $.ajax({
            url: '/WebPOSService.asmx/GetCashExpenceVoucherData',
            type: "Post",

            data: { "VoucherNo": $(".VoucherNo").val() },

            success: function (ModelPaymentVoucher) {
                if (ModelPaymentVoucher.Success) {
                    $(".Date").val(ModelPaymentVoucher.Date);
                    $(".ExpenceHeadCode").val(ModelPaymentVoucher.PartyCode);
                    $(".ExpenceHeadTitle").val(ModelPaymentVoucher.PartyName);
                    $(".CashPaid").val(ModelPaymentVoucher.CashPaid);
                    $(".Narration").val(ModelPaymentVoucher.Narration);
                    $("#StationDD").ddslick("destroy");
                    appendAttribute.init("StationDD", "Station", ModelPaymentVoucher.StationId);
                    updateInputStyle();

                } else {
                    smallSwal("Error", ModelPaymentVoucher.Message, "error");
                }
                $("#isLastVoucher").val(ModelPaymentVoucher.IsLastVoucherNo);
                manageNextPreviousBtn();
                $(".fa-spinner").removeClass('fa-spin');
            },
            fail: function (jqXhr, exception) {

            }
        });

    }
    else {
        swal("", "Enter Voucher Number", "error");
    }

}
