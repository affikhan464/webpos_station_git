﻿
function SaveBefore() {

    var SaveOk = "Yes";
    var InvoiceNumber = $(".InvoiceNumber").val();
    var Code = $(".PartyCode").val();
    var OpeningBalance = $(".OpeningBalance").val();

    if (Code == "") {
        SaveOk = "No";
        swal("Error", "Select Party", "error");
    }

    if (OpeningBalance == "") {
        SaveOk = "No";
        swal("Error", "Type Opening Balance", "error");
    }

    if (SaveOk == "Yes") {

        swal({
            title: "Business Manager",
            text: "Are You Sure to save a transaction?",
            type: 'question',
            showCancelButton: true,
            onfirmButtonText: "Yes, Save it!",
            cancelButtonText: "No, cancel please!",
            showLoaderOnConfirm: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            preConfirm: (text) => {
                return new Promise((resolve) => {

         
                    save();
                })
            },
            allowOutsideClick: () => !swal.isLoading()
        });
    }

}





function save()
{
    var voucher = {
        
        PartyCode: $(".PartyCode").val(),
        PartyName: $(".PartyName").val(),
        OpeningBalance: $(".OpeningBalance").val(),
        Narration: $(".Narration").val()
    }
    debugger
    $.ajax({
        url: "/Transactions/PartyOpeningBalance.aspx/Save",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify({ saveVoucherViewModal: voucher }),
        success: function (BaseModel) {
            debugger
            if (BaseModel.d.Success) {
                smallSwal("Success", BaseModel.d.Message, "success");
                $(".empt").val("");
                updateInputStyle();
            }
            else {
                swal("Error", BaseModel.d.Message, "error");
            }
        }
    })
}

$(document).ready(function () {


    $(".OpeningBalance").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            $("#txtNarration").focus();
        }
    });
    $(".Narration").keypress(function (event) {
        if (event.which == 13) {
            event.preventDefault();
            SaveBefore();
        }
    });
});

function clearInvoice() {
    $("#myTable tbody tr").remove();
    $("#Table_OldData tbody tr").remove();
    
    $(".inv1").val("");
    $(".inv3").val("0");
    

    //$("input").val("0");
    //calculationSale();
}