﻿namespace WebPOS
{
    public class BankModel
    {
        public BankModel()
        {
        }


        public string BankCode { get; set; }
        public string CompID { get; set; }
        public string BankName { get; set; }
        public string AccountNo { get; set; }
        public string Title { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        

    }
}