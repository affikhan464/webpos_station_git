﻿namespace WebPOS.Model
{
    public class Permisions
    {
        public Permisions()
        {
        }

        public bool AllowedOtherStationsSelect { get; set; }
        public bool UserAllowedToSetSellingPrice { get; set; }
    }
}