﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebPOS.Model;

namespace WebPOS.Model
{
    public class ProcessViewModel
    {
        public List<Product> Received { get; set; }
        public List<Product> Issued { get; set; }
        public string VoucherNumber { get; set; }
        public string Date { get; set; }
        public string Narration { get; set; }

    }
}