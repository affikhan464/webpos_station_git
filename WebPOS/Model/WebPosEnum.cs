﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPOS.Model
{
    public enum WebPosEnum
    {
        PID = 1
    }
    public enum ClosingBalance_GL
    {
        add = 1,
        minus = 2
    }
    public struct MatchIMEType
    {
        public const string Left = "Left";
        public const string Right = "Right";
        public const string AnyWhere = "AnyWhere";
        public const string ExactMatch = "ExactMatch";

    }
    public struct Party
    {
        public const string Client = "Client";
        public const string Supplier = "Supplier";
        public const string All = "All";

    }
    public struct IMEDescription
    {
        public const string Purchase = "Purchase";
        public const string SaleReturn = "SaleReturn";
        public const string Sale = "Sale";
        public const string PurchaseReturn = "PurchaseReturn";

    }
}