﻿namespace WebPOS.Model
{
    public class ThickNessModel
    {
        public string Attribute { get;  set; }
        public string ID { get; set; }
        public string ThickNess { get; set; }
    }
}