﻿using System.Collections.Generic;

namespace WebPOS.Model
{
    public class ChartOfAccountsViewModel
    {
        public ChartOfAccountsViewModel()
        {
        }

        public List<ChartOfAccountsViewModel> children { get; set; }
        public string Code { get; set; }
        public string ParentCode { get; set; }
        public string Level { get; set; }
        public string Title { get; set; }
        public string NorBalance { get; set; }
        public string Group { get; set; }
        public string Type { get; set; }
        public string OpeningBalance { get; set; }

    }
}