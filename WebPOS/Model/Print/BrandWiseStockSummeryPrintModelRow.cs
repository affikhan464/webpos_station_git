﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPOS.Model
{
    public class BrandWiseStockSummeryPrintModelRow
    {
        public int SNo { get; internal set; }
        public string Code { get; internal set; }
        public string Description { get; internal set; }
        public string Rate { get; internal set; }
        public string Qty { get; internal set; }
        public string Discount { get; internal set; }
        public string Amount { get; internal set; }
    }
}