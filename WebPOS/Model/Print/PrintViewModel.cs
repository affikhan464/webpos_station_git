﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPOS.Model
{
    public class PrintViewModel<T> where T : class
    {
        public string InvoiceNumber { get; set; }
        public string PartyName { get; set; }
        public string PartyAddress { get; set; }
        public string Particular { get; set; }
        public string Date { get; set; }
        public string PhoneNumber { get; set; }
        public List<T> Rows { get; set; }
        public decimal TotalQty { get;  set; }
        public string TotalAmount { get;  set; }
        public string BrandName { get;  set; }
        public List<IDictionary<string, object>> DRows { get;  set; }
        public string ReportStationName { get;  set; }
        public string SaleManName { get;  set; }
        public string InvoiceName { get; set; }
        public decimal TotalFooterDiscount { get; set; }
        public decimal TotalFooterBillTotal { get; set; }
        public decimal TotalFooterReceived { get; set; }
        public decimal TotalFooterNetBalance { get; set; }
        public decimal PreviousBalance { get;  set; }
        public string StationNameFrom { get;  set; }
        public string StationNameTo { get;  set; }
        public string Narration { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public decimal TotalNetCommission { get; set; }
        public decimal TotalPerPieceCommission { get;  set; }
        public string TotalCost { get;  set; }
        public decimal TotalProfitOrLoss { get;  set; }
        public decimal TotalTenCommission { get;  set; }
        public decimal TotalDebit { get;  set; }
        public decimal TotalCredit { get;  set; }
        public decimal TotalTotal { get;  set; }
        public decimal TotalPaid { get;  set; }
        public decimal TotalBalance { get;  set; }
        public decimal TotalNetDiscount { get;  set; }
        public decimal TotalReturnQty { get;  set; }
    }
}