﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPOS.Model
{
    public class StockAdjustmentVoucher
    {
        public List<StockAdjustmentViewModel> Rows { get; set; }
        public string Date { get; set; }
    }
}