﻿namespace WebPOS.Model
{
    public class IMEModel
    {
        public IMEModel()
        {
        }

        public bool isItemMatch { get; set; }
        public bool isSold { get; set; }
        public bool isPurchased { get; set; }
        public bool isLengthCorrect { get; set; }
        public bool Error { get; set; }
        public string ErrorMessage { get; set; }
        public decimal Rate { get; set; }
        public string Code { get; set; }
        public decimal DisRs { get; set; }
        public decimal DisPer { get; set; }
        public decimal DealRs { get; set; }
        public bool isItemAvailable { get;  set; }
    }
}