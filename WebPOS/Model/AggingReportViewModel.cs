﻿namespace WebPOS.Model
{
    public class AggingReportViewModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        /// <summary>
        /// 1 to 15 Days Balance
        /// </summary>
        public string FirstRange { get; set; }
        /// <summary>
        /// 16 to 30 Days Balance
        /// </summary>
        public string SecondRange { get; set; }
        /// <summary>
        /// 31 to 45 Days Balance
        /// </summary>
        public string ThirdRange { get; set; }
        /// <summary>
        /// 46 to 60 Days Balance
        /// </summary>
        public string FourthRange { get; set; }
        /// <summary>
        /// 61 to 75 Days Balance
        /// </summary>
        public string FifthRange { get; set; }
        /// <summary>
        /// 76 to 90 Days Balance
        /// </summary>
        public string SixthRange { get; set; }
        /// <summary>
        /// 90++ Days Balance
        /// </summary>
        public string SeventhRange { get; set; }
        public string NetBalance { get; set; }
    }
}