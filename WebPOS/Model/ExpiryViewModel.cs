﻿using System;

namespace WebPOS.Model
{
    public class ExpiryViewModel
    {
        public ExpiryViewModel()
        {
        }

        public int Id { get; set; }
        public string ExpiryDate { get; set; }
        public string ExpiryIn { get; set; }
        public string HashKey { get; set; }
        public string SystemId { get; set; }
        public bool IsExpired { get;  set; }
        public bool IsExpiryClosed { get;  set; }
        public string ExpiryDateText { get; internal set; }
        public DateTime ExpiryDateTime { get; set; }
    }
}