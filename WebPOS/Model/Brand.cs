﻿using System.Collections.Generic;

namespace WebPOS.Model
{
    public class Brand
    {
        public Brand()
        {
        }

        public string Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Rate { get; set; }
        public string Blocked { get; set; }
        public string VerifyIME { get; set; }
        public string Code2 { get; set; }

    }
    public class PartyBrandDiscount
    {
        public List<Brand> Brands { get; set; }
        public string PartyCode { get; set; }
    }

}
