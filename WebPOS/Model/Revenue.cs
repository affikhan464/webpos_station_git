﻿namespace WebPOS.Model
{
    public class Revenue
    {
        public string RevenueCode { get; set; }
        public string RevenueAmount { get; set; }
    }
}