﻿using System.Collections.Generic;
using WebPOS;

namespace WebPOS.Model
{
    public class SaveBrandWiseDiscountModel
    {

        public List<Brand> Brands { get; set; }

        public string PartyCode { get; set; }

    }
    public class SaveBrandWiseDiscountModel2
    {

        public string BrandCode { get; set; }

        public string Discount { get; set; }

    }


}