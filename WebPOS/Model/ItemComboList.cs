﻿using System.Collections.Generic;

namespace WebPOS.Model
{
    public class ItemComboList
    {
        public List<Brand> ItemType { get; set; }
        public List<Brand> Brand { get; set; }
        public List<Brand> Height { get; set; }
        public List<Brand> Width { get; set; }
        public List<Brand> BarCodeCategory { get; set; }
        public List<Brand> Color { get; set; }
        public List<Brand> Manufacturer { get; set; }
        public List<Brand> Packing { get; set; }
        public List<Brand> Category { get; set; }
        public List<Brand> Class { get; set; }
        public List<Brand> GoDown { get; set; }
        public List<Brand> ItemNature { get; set; }
        public List<Brand> InComeAccount { get; set; }
        public List<Brand> COGSAccount { get; set; }
        public List<Brand> AssetAccount { get; set; }
        public List<Brand> Unit { get; set; }

    }

}