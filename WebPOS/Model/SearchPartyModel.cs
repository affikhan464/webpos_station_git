﻿namespace WebPOS
{
    internal class SearchPartyModel
    {
        public SearchPartyModel()
        {
        }
        public string Code { get; set; }
        public string Name { get; set; }

        public string Balance { get; set; }

        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string OtherInfo_Hide { get; set; }
        public string NorBalance_Hide { get; set; }
        public string CreditLimit_Hide { get; set; }
        //public string DisPer { get; set; }
        //public string DealRs { get; set; }
        //public string NorBalance { get; set; }
        public string DealApplyNo { get; set; }
        public string AutoDealRs_Hide { get;  set; }
        public string AutoDisPer_Hide { get;  set; }
    }
    public class SearchCashSalePartyModel
    {
        public SearchCashSalePartyModel()
        {
        }
        public string Code { get;  set; }
        public string InvNo { get;  set; }
        public string Date { get;  set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string PrevBalance { get;  set; }
    }
}