﻿using System;

namespace WebPOS
{
    public class DateAndSystemId
    {
        public DateAndSystemId()
        {
        }

        public DateTime ExpiryDate { get; set; }
        public string SystemUId { get; set; }
        public int Id { get; set; }
    }
}