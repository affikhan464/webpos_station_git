﻿namespace WebPOS
{
    public class BrandWiseDiscountModel
    {
        public BrandWiseDiscountModel()
        {
        }

        public string BrandCode { get; set; }
        public string BrandName { get; set; }
        public string DiscountRate { get; set; }
        public string PartyCode { get; set; }
    }
}