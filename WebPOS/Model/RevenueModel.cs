﻿using System.Collections.Generic;

namespace WebPOS
{
    public class GLModel
    {
        public decimal Amount { get; set; }
        public string Code { get; set; }
    }
}