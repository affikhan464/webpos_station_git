﻿namespace WebPOS
{
    public class SearchItemModelStandard
    {
        public SearchItemModelStandard()
        {
        }


        public string Code { get; set; }
        public string Description { get; set; }
        public string ThisStationQty { get; set; }
        public string TotalQty { get; set; }

        public string Rate { get; set; }
        public string AverageCost_Hide { get; set; }
        public string DealRs { get; set; }
        public string DealRs2 { get; set; }
        public string DealRs3 { get; set; }
        public string RevenueCode_Hide { get; set; }
        public string CGSCode_Hide { get; set; }
        public string ActualSellingPrice_Hide { get; set; }
        public string PerPieceCommission_Hide { get;  set; }
        public string OriginalRate_Hide { get; set; }
        public bool IsRateLastSoldRate_Hide { get; set; }//if Is Rate a Last Sold Rate true
        public bool IsRateLastPurchaseRate_Hide { get; set; }
    }
}