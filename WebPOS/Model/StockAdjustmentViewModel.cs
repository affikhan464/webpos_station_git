﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPOS.Model
{
    public class StockAdjustmentViewModel
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string StockInComputer { get; set; }
        public string StockOnShelf { get; set; }
        public string Adjustment { get; set; }
        public string CostExcess { get; set; }
        public string CostShort { get; set; }
        public string UnitCost { get; set; }


    }
}