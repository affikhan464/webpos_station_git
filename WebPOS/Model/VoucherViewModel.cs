﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPOS.Model
{
    public class VoucherViewModel
    {
        public List<Voucher> Rows { get; set; }
        public string VoucherType { get; set; }
        public string VoucherNumber { get; set; }
        public string VoucherDate { get; set; }
        public string CreditGLCode { get; set; }
        public string CreditGLTitle { get; set; }
        public string CreditAmount { get; set; }
        public string CreditNarration { get; set; }

        public string PrevBalance { get; set; }
        public string PartyName { get; set; }
        public string PartyCode { get; set; }
        public string Remarks { get; set; }
        public string LPAmount { get; set; }
        public string IME { get; set; }
        public string Txt1 { get; set; }
        public string Txt2 { get; set; }
        public string NewBalance { get; set; }
        public bool IsIMEVerify { get; set; }
        public bool Chxbx1 { get; set; }
        public string SearchOption { get; set; }


        public bool IsLastVoucherNo { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
    }
}