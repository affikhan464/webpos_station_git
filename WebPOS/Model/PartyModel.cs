﻿namespace WebPOS
{
    public class PartyModel
    {
        public PartyModel()
        {
        }

        public string NorBalance { get; set; }
        public string PartyCode{ get; set; }
        public string PartyName { get; set; }
        public string ContactPerson { get; set; }
        public string MobileNo { get; set; }
        public string FaxNo { get; set; }
        public string LandLineNo { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string lstSellingPriceNo { get; set; }
        public string lstDealApplyNo { get; set; }
        public string CreditLimit { get; set; }
        public string CreditLimitApply { get; set; }

        public string OtherInfo { get; set; }
        public string lstSaleMan { get; set; }
        public string chkPerDiscount { get; set; }
        public string chkDeal { get; set; }
        
        




    }
}