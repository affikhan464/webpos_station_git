﻿namespace WebPOS.Model
{
    public class PrintingTable
    {
        public string AttributeType { get; set; }
        public string Attribute { get; set; }
        public string PurchaseReturnQty
        {
            get; set;

        }
        public string SoldQty
        {
            get; set;

        }
        public string SaleReturnQty
        {
            get; set;

        }
        public string PurchasedQty
        {
            get; set;

        }
        public string VoucherNo
        {
            get; set;

        }
        public string ClassName
        {
            get; set;

        }
        public string ModelName
        {
            get; set;

        }
        public string Reference
        {
            get; set;

        }
        public string Reference2
        {
            get; set;

        }
       
        public string SerNo
        {
            get; set;

        }
        public string Date1
        {
            get; set;

        }
        public string Date2
        {
            get; set;

        }
        public string Date3
        {
            get; set;

        }
        public string Date4
        {
            get; set;

        }
        public string Text_1
        {
            get; set;

        }
        public string Text_2
        {
            get;

            set;
        }
        public string Text_3
        {
            get;

            set;
        }
        public string Text_4
        {
            get;

            set;
        }
        public string Text_5
        {
            get;

            set;
        }
        public string Text_6
        {
            get;

            set;
        }
        public string Text_7
        {
            get;

            set;
        }
        public string Text_8
        {
            get;

            set;
        }
        public string Text_9
        {
            get;

            set;
        }
        public string Text_10
        {
            get;

            set;
        }
        public string Text_11
        {
            get;

            set;
        }
        public string Rate
        {
            get;

            set;
        }
        public string Amount
        {
            get;

            set;
        }
        public string Text_13
        {
            get;

            set;
        }
        public string Opening
        {
            get;

            set;
        }
        public string Received
        {
            get;

            set;
        }
        public string Issued
        {
            get;

            set;
        }
        public string Balance
        {
            get;

            set;
        }
        public string ItemCode
        {
            get;

            set;
        }
        public string ItemName
        {
            get;

            set;
        }
        public string ReorderLevel
        {
            get;

            set;
        }
        public string InHandQty
        {
            get;

            set;
        }
        public string ShortQty
        {
            get;

            set;
        }
        public string ReOrderQty
        {
            get;

            set;
        }
        public string RequiredQty
        {
            get;

            set;
        }
        public string LastPurchasePrice
        {
            get;

            set;
        }
        public string AmountRequired
        {
            get;

            set;
        }
        public string BrandNAme
        {
            get;

            set;
        }
        public string Num_6
        {
            get;

            set;
        }
        public string Num_7
        {
            get;

            set;
        }
        public string Num_8
        {
            get;

            set;
        }
        public string Num_9
        {
            get;

            set;
        }
        public string Num_10
        {
            get;

            set;
        }
        public string Num_11
        {
            get;

            set;
        }
        public string Num_12
        {
            get;

            set;
        }
        public string Num_13
        {
            get;

            set;
        }
        public string Num_14
        {
            get;

            set;
        }
        public string Num_15
        {
            get;

            set;
        }
        public string Num_16
        {
            get;

            set;
        }
        public string Num_17
        {
            get;

            set;
        }
        public string Num_18
        {
            get;

            set;
        }
        public string Num_19
        {
            get;

            set;
        }
        public string Num_20
        {
            get;

            set;
        }

        public string Tot_1
        {
            get; set;

        }
        public string Tot_2
        {
            get; set;

        }
        public string Tot_3
        {
            get; set;

        }
        public string Tot_4
        {
            get; set;

        }
        public string Tot_5
        {
            get; set;

        }
        public string Tot_6
        {
            get; set;

        }
        public string Tot_7
        {
            get; set;

        }
        public string Tot_8
        {
            get; set;

        }
        public string Tot_9
        {
            get; set;

        }
        public string Tot_10
        {
            get; set;

        }
        public string Company
        {
            get;

            set;
        }
        public string PhoneNo
        {
            get;

            set;
        }
        public string Address_1
        {
            get;

            set;
        }
        public string Address_2
        {
            get;

            set;
        }
        public string Address_3
        {
            get;

            set;
        }
        public string Address_4
        {
            get;

            set;
        }
        public string Heading_1
        {
            get;

            set;
        }
        public string Heading_2
        {
            get;

            set;
        }
        public string Heading_3
        {
            get;

            set;
        }
        public string Heading_4
        {
            get;

            set;
        }
        public string Heading_5
        {
            get;

            set;
        }

    }
}