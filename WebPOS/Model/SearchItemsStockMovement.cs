﻿namespace WebPOS
{
    internal class SearchItemsStockMovement
    {
        public SearchItemsStockMovement()
        {
        }

        public string Code { get; set; }
        public string Description { get; set; }
        public string Rate { get; set; }
        public string StationFromQty { get; set; }
        public string StationToQty { get; set; }
        public string TotalQty { get; set; }
        public string AverageCost { get; set; }
        public string Reference1 { get; set; }
    }
}