﻿namespace WebPOS.Model
{
    internal class SearchVoucherModel
    {
        public string Date { get; set; }
        public string VoucherNumber { get; set; }
        public string PartyCode_Hide { get; set; }
        public string PartyName { get;  set; }
        public string PartyAddress { get; set; }
        public string CashPaid { get; set; }
       
        public string NorBalance_Hide { get; set; }
        public string Narration_Hide { get;  set; }
        public string Balance_Hide { get; set; }
    }
}