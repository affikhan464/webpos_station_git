﻿namespace WebPOS.Model
{
    internal class ItemViewModel
    {
        public ItemViewModel()
        {
        }

        public string Code { get; set; }
        public string Description { get; set; }
        public string Qty { get; set; }
        public string Rate { get; set; }
    }
}