﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebPOS.Model;

namespace WebPOS.Model
{
    public class AssemblingViewModel
    {
        public List<Product> Ingredients { get; set; }
        public string MainItemCode { get; set; }
        public string MainItemDescription { get; set; }
    }
}