﻿namespace WebPOS
{
    public class SaveVoucherViewModel
    {
        public string VoucherNumber { get; set; }
        public string PartyName { get; set; }
        public string PartyCode { get; set; }
        public string PartyNature { get; set; }
        public string Address { get; set; }
        public string Date { get; set; }
        public string OpeningBalanceCashPaid { get; set; }
        public string Balance { get; set; }
        public string Narration { get; set; }
        public string ChequeAmount { get; set; }
        public string OldPartyCode { get; set; }
        public string AccountCode { get; set; }
        public string AccountTitle { get; set; }
        public string ChequeNumber { get; set; }
        public string BankName { get; set; }
        public string BankCode { get; set; }
        public string AccountNumber { get; set; }
        public string OldBankCode { get; set; }
        public string DepartmentId { get; set; }
        public string OpeningBalance { get; set; }

        public string Code { get; set; }
        public string StationIdFrom { get; set; }
        public string StationIdTo { get; set; }
        public string Qty { get;  set; }
        public string Name { get;  set; }
        public object Amount { get;  set; }
        public object Rate { get;  set; }
        public string StationNameFrom { get;  set; }
        public string StationNameTo { get;  set; }
        public string SNo { get;  set; }
        public bool IsLastVoucherNo { get;  set; }
    }
}