﻿namespace WebPOS.Model
{
    internal class SearchBankDepositAndWithdrawVoucherModel
    {
        public SearchBankDepositAndWithdrawVoucherModel()
        {
        }


        public string BankCode_Hide { get; set; }
        public string BankName { get; set; }
        public string VoucherNumber { get; set; }
        public string Date { get; set; }
        public string Narration { get; set; }
        public string CashPaid { get; set; }
        public string ChequeNumber_Hide { get; set; }
    }
}