﻿using System.Collections.Generic;

namespace WebPOS.Model
{
    public class SaveProductViewModel
    {
        public TotalData TotalData { get; set; }
        public Client ClientData { get; set; }
        public List<Product> Products { get; set; }
        public Invoices NextPreviousInvoiceNumbers { get; set; }
        public string InitialItemCodeAndQuantity { get; set; }
        public List<Revenue> Revenue { get; set; }
        public List<Product> ProductsOld { get; set; }
        public List<Product> IMEItems { get; set; }
        public bool Success { get; set; }
        public string Message { get; set; }
    }



}