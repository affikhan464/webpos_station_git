﻿namespace WebPOS.Model
{
    public class ColorModel
    {
        public string ID { get; set; }
        public string Color { get; set; }
        public string Attribute { get;  set; }
    }
}