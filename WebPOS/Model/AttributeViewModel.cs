﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPOS.Model
{
    public class AttributeViewModel
    {
        public string AttributeCode { get; set; }
        public string AttributeName { get; set; }
        public bool Blocked { get; set; }
        public bool VerifyIME { get; set; }
    }
}