﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPOS.Model
{
    public class OpeningInventoryViewModel
    {


        public string ItemName { get; set; }
        public decimal ItemCode { get; set; }
        public decimal OpeningUnits { get; set; }
        public decimal ReceivedUnits { get; set; }
        public decimal IssuedUnits { get; set; }
        public decimal ClosingUnits { get; set; }
        public decimal RequiredClosingUnits { get; set; }
        public decimal PurchasingCost { get; set; }
        public decimal SellingCost { get; set; }
        public decimal TotalValue { get; set; }
    }
}