﻿namespace WebPOS.Model
{
    public class Invoices
    {
        public string CurrentInvoice { get; set; }
        public string NextInvoice { get; set; }
        public string PreviousInvoice { get; set; }
    }
}