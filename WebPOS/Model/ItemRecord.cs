﻿namespace WebPOS.Model
{
    public class ItemRecord
    {
        public string ItemCode { get; set; }
        public string PartyName { get; set; }
        public string PartyCode { get; set; }
        public string Brand { get; set; }
        public string DateSold { get; set; }
        public string Rate { get; set; }
        public string Qty { get; set; }
        public string Amount { get; set; }
        public string DealRs { get; set; }
        public string Item_Discount { get; set; }
        public string PerDiscount { get; set; }
        public string SalesMan { get; set; }
    }
}