﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPOS.Model
{
    public class AddItemModel
    {
        //Form 2
        public string ItemCode { get; set; }
        public string BarCode { get; set; }
        public string ManufacturerId { get; set; }
        public string PackingId { get; set; }
        public string CategoryId     { get; set; }
        public string ClassId { get; set; }
        public string GoDownId { get; set; }
        public string OrderQuantity { get; set; }
        public string BonusQuantity { get; set; }
        public string GST { get; set; }
        public bool GST_Apply { get; set; }
        public string ItemNatureId { get; set; }
        public string IncomeAccount { get; set; }
        public string AssetAccount { get; set; }
        public string COGSAccount { get; set; }
        public string ColorId { get; set; }
        public string HeightId { get; set; }
        public string LengthId { get; set; }
        public string ItemTypeId { get; set; }


        //Form 1
        public string Reference { get; set; }
        public string Reference2 { get; set; }
        public string Registration { get; set; }
        public string PiecesInPacking { get; set; }
        public string UnitOfMeasureId { get; set; }
        public string Name { get; set; }
        public string BrandId { get; set; }
        public string ReOrderPoint { get; set; }
        public string ReOrderQty { get; set; }
        public string SellingPriceOrUnit { get; set; }
        public string MinPrice { get; set; }
        public string MaxPrice { get; set; }
        public bool IsActive { get; set; }
        public bool IsVisible { get; set; }
        public bool IssueIngridient { get; set; }
        
        //Aluminium
        public string ThickNessId { get; set; }
        public string SizeId { get; set; }


    }
}