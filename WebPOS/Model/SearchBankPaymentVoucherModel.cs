﻿namespace WebPOS
{
    public class SearchBankPaymentVoucherModel
    {
        public string Date { get; set; }
        public string PartyName { get; set; }
        public string Narration { get; set; }
        public string BankName { get; set; }
        public string VoucherNumber { get; set; }
        public string CashPaid { get; set; }
        public string PartyCode_Hide { get; set; }
        public string PartyAddress_Hide { get; set; }

        public string PartyBalance_Hide { get; set; }
        public string ChequeNumber_Hide { get; set; }
        public string BankCode_Hide { get; set; }
        public string AccountNumber_Hide { get; set; }
        public string BankBalance_Hide { get; set; }

    }
}