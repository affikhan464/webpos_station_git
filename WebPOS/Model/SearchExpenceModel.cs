﻿namespace WebPOS
{
    internal class SearchExpenceModel
    {
        public SearchExpenceModel()
        {
        }
        public string Code { get; set; }
        public string Title { get; set; }
        public string Balance { get; set; }
    }
}