﻿namespace WebPOS.Model
{
    public class ConfigurationsModel
    {
        public bool SelectLastPurchaseRate { get;  set; }
        public bool ShowItemQtyAtZero { get;  set; }
        public int InvoiceFormat { get;  set; }
    }
}