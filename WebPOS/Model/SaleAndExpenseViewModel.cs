﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPOS.Model
{
    public class SaleAndExpenseViewModel
    {
        public List<InvoiceViewModel> Expenses { get; set; }
        public List<InvoiceViewModel> Sales { get; set; }
       
        public string TotalSaleAmount { get; set; }
        public string TotalExpenseAmount { get; set; }
    }
}