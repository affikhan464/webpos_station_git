﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPOS.Model
{
    public struct Configurations
    {
       public const string SelectLastPurchaseRate = "SelectLastPurchaseRate";
       public const string ShowItemQtyAtZero = "ShowItemQtyAtZero";
       public const string InvoiceFormat = "InvoiceFormat";
    }
    public struct MenuPages
    {
        public const string HomePage = "/home.aspx";
    }
    public struct UserRole
    {
        public const string Admin = "0";
    }
}