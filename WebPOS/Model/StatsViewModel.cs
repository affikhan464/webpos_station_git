﻿namespace WebPOS.Model
{
    internal class StatsViewModel
    {
        public StatsViewModel()
        {
        }

        public decimal ActiveItems { get; set; }
        public decimal ActiveItemPercentage { get; set; }
        public decimal ItemsSold { get; set; }
        public decimal ItemsSoldPercentage { get; set; }
        public decimal TotalActiveParties { get; set; }
        public decimal TotalActivePartiesPercentage { get; set; }
        public decimal TotalVendors { get; set; }
        public decimal TotalClient { get; set; }
        public decimal TotalVendorsPercentage { get; set; }
        public decimal TotalClientPercentage { get; set; }
    }
}