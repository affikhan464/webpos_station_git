﻿namespace WebPOS.Model
{
    public class LoginViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string StationId { get; set; }
        public string StationName { get; set; }
    }
}