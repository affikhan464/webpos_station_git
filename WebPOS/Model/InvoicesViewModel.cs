﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPOS.Model
{
    public class InvoiceViewModel
    {
        public string GLTitle { get; set; }
        public string VoucherType { get; set; }
        public string PartyCode { get; set; }
        public string ChequeNo { get; set; }
        public string VoucherNo { get; set; }
        public string Dis_Per_Rate { get; set; }
        public string Item_Disc { get; set; }
        public string InvoiceNumber { get; set; }
        public string Date { get; set; }
        public string PartyName { get; set; }
        public string ManualInvoiceNumber { get; set; }
        public string SalesManName { get; set; }
        public string NetDiscount { get; set; }
        public string Paid { get; set; }
        public string Total { get; set; }
        public string BillTotal { get; set; }
        public string FlatDiscount { get; set; }
        public string PhoneNumber { get; set; }
        public string Quantity { get; set; }
        public string DealRs { get; set; }
        public string Balance { get; set; }
        public string ItemCode { get; set; }
        public string Description { get; set; }
        public string Amount { get; set; }
        public string BrandName { get; set; }
        public string ReturnAmount { get; set; }
        public string ReturnQty { get; set; }
        public string NetQtySold { get; set; }
        public string NetAmountSold { get; set; }
        public string InHandQty { get; set; }
        public string Payable { get; set; }
        public string NumberOfInvoices { get; set; }
        public string Rate { get; set; }
        public string DiscountPer { get; set; }
        public string CurrentBalance { get; set; }
        public string OpeningBalance { get; set; }
        public string TotalSale { get; set; }
        public string TotalReturn { get; set; }
        public string NetSale { get; set; }
        public string TotalReceived { get; set; }
        public string TotalPayment { get; set; }
        public string Debit { get; set; }
        public string Credit { get; set; }
        public string ClosingBalance { get; set; }
        public string DepartmentName { get; set; }
        public string Code { get; set; }
        public string Particular { get; set; }
        public string IME { get; set; }
        public string Transaction { get; set; }
        public string Price { get; set; }

        public string Sale { get; set; }
        public string Cost { get; set; }
        public string Profit { get; set; }
        public string ClassType { get;  set; }
		public string Address { get; set; }
        public string MobileNumber { get; set; }
        public string Narration { get;  set; }
        public string StationId { get;  set; }
        public string StationName { get;  set; }
        public string ExpenseHead { get;  set; }
        public string TenCommission { get;  set; }
        public string PerPieceCommission { get;  set; }
        public string NetCommission { get; set; }
        public string TotalCost { get;  set; }
        public string TotalPerPieceCommission { get;  set; }
        public string SalesManCode { get;  set; }
        public string PurchaseAmount { get; set; }
        public string SaleReturnInvNo { get;  set; }
        public string SaleInvNo { get; set; }
        public bool IsReturnedItem { get;  set; }
        public bool IsUnassignedReturnedItem { get;  set; }
    }
    public class ReturnedInvoiceViewModel<T> where T : class
    {
        public List<InvoiceViewModel> Calculated { get; set; }
        public List<InvoiceViewModel> NotCalculated { get; set; }
        public List<T> CalculatedObj { get; set; }
        public List<T> NotCalculatedObj { get; set; }
        public decimal TotalQty { get; set; }
        public decimal TotalNetCommission { get; set; }
        public decimal TotalPerPieceCommission { get; set; }
        public decimal TotalCost { get; set; }
        public decimal TotalProfitOrLoss { get; set; }
        public decimal TotalTenCommission { get; set; }
        public decimal TotalAmount { get; set; }
    }
}