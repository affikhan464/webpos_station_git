﻿namespace WebPOS.Model
{
    public class MenuPage
    {
        public string Id { get;  set; }
        public string Name { get;  set; }
        public string Url { get;  set; }
        public string UserId { get; set; }
        public string StationId { get; set; }
    }
}