﻿namespace WebPOS.Model
{
    public class SearchItemModelAluminium
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string ThickNess { get; set; }
        public string Size { get; set; }
        public string Color { get; set; }
        public string Qty { get; set; }
        public string Rate { get; set; }
        public string DealRs_Hide { get; set; }
        public string AverageCost_Hide { get; set; }
        public string Category { get; set; }
    }
    public class SearchItemModelStandard
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public string Qty { get; set; }
        public string Rate { get; set; }
        public string DealRs { get; set; }
        public string AverageCost_Hide { get; set; }
        public string RevenueCode_Hide { get; set; }
        public string CGSCode_Hide { get; set; }
        public string DealRs2 { get; set; }
    }
    public class SearchItemModelStandardIME
    {

        public string Code { get; set; }
        public string Description { get; set; }
        public string Qty { get; set; }
        public string Rate { get; set; }
        public string DealRs { get; set; }
        public string avrgCost { get; set; }
        public string cgsCode { get; set; }
        public string DealRs2 { get; set; }
        public decimal ItemDis { get;  set; }
        public decimal PerDis { get;  set; }
        public decimal DealDis { get;  set; }
        public string revCode { get;  set; }
        public string actualSellingPrice { get;  set; }
        public bool isIME { get;  set; }
        public bool isIMESR { get;  set; }
        public bool isIMEPR { get;  set; }
        public bool isItemAvailble { get;  set; }
        public bool isIMEP { get; set; }
        public int BarCodeCategory { get;  set; }
        public string PerPieceCommission { get; set; }
        public string BrandName { get;  set; }
        public string BrandCode { get;  set; }
        public string LP { get; set; }
        public string IME { get;  set; }
    }
}