﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPOS.Model
{
    public class Voucher
    {
        public string Code { get; set; }
        public string Title { get; set; }
        public string ChequeNumber { get; set; }
        public string Narration { get; set; }
        public string Credit { get; set; }
        public string Debit { get; set; }
    }
}