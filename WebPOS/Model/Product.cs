﻿using System;

namespace WebPOS.Model
{
    public class Product
    {
        public string SNo { get; set; }
        public string Code { get; set; }
        public string ItemName { get; set; }
        public string Description { get; set; }
        public string Qty { get; set; }
        public string Rate { get; set; }
        public string Amount { get; set; }
        public string ItemDis { get; set; }
        public string ThickNess { get; set; }

        public string Color { get; set; }
        public string Size { get; set; }
        public string Feet { get; set; }
        public string Category { get; set; }
        public string PerDis { get; set; }
        public string Remarks { get; set; }
        public string NetAmount { get; set; }
        public string PurAmount { get; set; }
        public string DealRsFromDb { get; set; }
        public string AverageCost { get; set; }
        public string StatioName { get; set; }
        public string InHandQty { get; set; }
        public string DealDis { get; set; }
        public string DepartmentId { get; set; }
        public string ActualCost { get; set; }
        public string SellingPrice { get; set; }
        public string Cost { get; set; }
        public string CostAfterDiscount { get; set; }
        public string RevenueCode { get; set; }
        public string CGSCode { get; set; }
        public string IMEI { get; set; }
        public string BrandName { get; set; }
        public string LastDateSold { get; set; }
        public string ItemStationId { get; set; }
        public string ActualSellingPrice { get; set; }
        public string SellingDate { get; set; }
        public string VendorName { get;  set; }
        public string PerPieceCommission { get; set; }
        public string BrandCode { get; set; }
        public string LP { get; internal set; }
        public string MSP { get; internal set; }
    }
}