﻿using System.Collections.Generic;
using WebPOS.Model;

namespace WebPOS
{
    public class LPReceivingViewModel
    {
        public List<SearchItemModelStandardIME> LeftTable { get; set; }
        public List<SearchItemModelStandardIME> RightTable { get; set; }
        public string PartyCode { get; set; }
        public string LPTotalAmount { get; set; }
        public string NewBalance { get; set; }
    }
}