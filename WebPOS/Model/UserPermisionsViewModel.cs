﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebPOS.Model
{
    public class UserPermisions
    {
        public List<MenuPage> Pages { get; set; }
        public bool AllowedOtherStationsSelect { get; set; }
        public bool UserAllowedToSetSellingPrice { get; set; }
        public string UserId { get; set; }
        public string StationId { get; set; }
    }
}