﻿using System.Collections.Generic;

namespace WebPOS.Model
{
    public class BaseModel
    {
        public List<PrintingTable> PrintReport { get; set; }
        public string Message { get; set; }
        public List<InvoiceViewModel> ListofInvoices { get; set; }
        public List<ReportViewModel> Reports { get; set; }
        public ReportViewModel Report { get; set; }

        public bool Success { get; set; }
        public string LastInvoiceNumber { get; set; }
        public List<Brand> Brands { get; set; }
        public string LastVoucherNumber { get; set; }
        public bool IsSoftwareExpired { get; set; }
        public object Data { get; set; }
        public List<AggingReportViewModel> AggingReports { get; set; }
        public bool LoginAgain { get; set; }
        public string ReturnUrl { get;  set; }
        public bool IsTimeExpired { get;  set; }
        public SaleAndExpenseViewModel SaleAndExpense { get; set; }
        //public AddItemModel AddItemModel { get; set; }
    }
}