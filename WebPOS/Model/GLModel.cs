﻿namespace WebPOS.Model
{
    public class GLModel
    {
        public GLModel()
        {
        }

        public decimal Amount { get; set; }
        public string Code { get; set; }
    }
}