﻿namespace WebPOS.Model
{
    internal class GoogleChartModel
    {
        public GoogleChartModel()
        {
        }

        public string Name { get; set; }
        public string Qty { get; set; }
        public string Sale { get; set; }
        public string Purchase { get; set; }
        public string SaleReturn { get; set; }
        public string PurchaseReturn { get; set; }
    }
}