﻿namespace WebPOS.Model
{
    public class SearchSimpleItem
    {
        public SearchSimpleItem()
        {
        }

        public string Code { get; set; }
        public string Description { get; set; }
        public string Qty { get; set; }
        public string Rate { get; set; }
        public string AverageCost { get; set; }
    }
}