﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="WebPOS.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Home - Business Manager </title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="home-page">
        <section id="main-section" class="section" style="display: none;">
            <div data-page="143" style="display: none;" class="reorderAlert alert alert-danger text-danger">
                <i class="fas fa-exclamation-triangle mr-1"></i>Some of items need to be Re-Ordered, 
                <a class="text-info" href="#ReorderItem">View Items</a>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="row">

                <div data-page="179" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Transactions/SaleNewItems.aspx" class="float-left w-100">
                        <div class="custom-card">
                            <div class="custom-card-icon bluecorel">
                                <i class="fas fa-shopping-cart"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">
                                    *
                                </div>
                                <p>Sale New Item</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div data-page="177" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Transactions/PurchaseNewItems.aspx" class="float-left w-100">
                        <div class="custom-card">

                            <div class="custom-card-icon purplish">
                                <i class="fas fa-cart-arrow-down"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">*</div>
                                <p>Purchase New Item</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div data-page="180" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Transactions/SaleReturnNew.aspx" class="float-left w-100">
                        <div class="custom-card">

                            <div class="custom-card-icon lightgreen">
                                <i class="fas fa-shopping-basket"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">*</div>
                                <p>Sale Return</p>
                            </div>
                        </div>
                    </a>
                </div>

                <div data-page="178" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Transactions/PurchaseReturnNew.aspx" class="float-left w-100">
                        <div class="custom-card">

                            <div class="custom-card-icon darkpink">
                                <i class="fas fa-shipping-fast"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">*</div>
                                <p>Purchase Return</p>
                            </div>
                        </div>
                    </a>
                </div>


                <div data-page="1" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Registration/ItemRegistrationnew.aspx" class="float-left w-100">
                        <div class="custom-card">
                            <div class="custom-card-icon red">
                                <i class="fas fa-cart-plus"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">
                                    *
                                </div>
                                <p>Add New Item</p>
                            </div>
                        </div>
                    </a>
                </div>

                <div data-page="214" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Correction/ItemRegistrationEditNew.aspx" class="float-left w-100">
                        <div class="custom-card">
                            <div class="custom-card-icon green">
                                <i class="fas fa-pen-square"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">
                                    *
                                </div>
                                <p>Edit Item</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div data-page="14" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Registration/PartyRegistration.aspx" class="float-left w-100">
                        <div class="custom-card">

                            <div class="custom-card-icon blue">
                                <i class="fas fa-user-plus"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">*</div>
                                <p>Add New Party</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div data-page="213" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Correction/PartyRegistrationEdit.aspx" class="float-left w-100">
                        <div class="custom-card">

                            <div class="custom-card-icon green">
                                <i class="fas fa-user-edit"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">*</div>
                                <p>Edit Party</p>
                            </div>
                        </div>
                    </a>
                </div>

                <div data-page="218" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Transactions/StockMovement.aspx" class="float-left w-100">
                        <div class="custom-card">
                            <div class="custom-card-icon purplish">
                                <i class="fas fa-people-carry"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">*</div>
                                <p>Stock Movement </p>
                            </div>
                        </div>
                    </a>
                </div>
                <div data-page="461" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Correction/StockMovementPrevious.aspx" class="float-left w-100">
                        <div class="custom-card">
                            <div class="custom-card-icon red">
                                <i class="fas fa-cubes"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">*</div>
                                <p>Previous Stock Movement </p>
                            </div>
                        </div>
                    </a>
                </div>
                <div data-page="218" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a target="_blank" href="/Reports/SaleReports/CustomerPhoneNumbers.aspx" class="float-left w-100">
                        <div class="custom-card">
                            <div class="custom-card-icon green">
                                <i class="fas fa-portrait"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">*</div>
                                <p>Print Customers Data</p>
                            </div>
                        </div>
                    </a>
                </div>

                <div data-page="13" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a class="chartOfAccountBtn float-left w-100">
                        <div class="custom-card">
                            <div class="custom-card-icon blue">
                                <i class="fas fa-list-ul"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">
                                    *
                                </div>
                                <p>Chart Of Accounts</p>
                            </div>
                        </div>
                    </a>
                </div>
                
                <div data-page="680" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                     <a target="_blank" href="/Transactions/LPReceiving/LPReceiving.aspx" class="float-left w-100">
                        
                        <div class="custom-card">
                            <div class="custom-card-icon blue">
                                <i class="fas fa-barcode"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">
                                    *
                                </div>
                                <p>LP Receiving</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div data-page="143" class=" col-lg-6 col-xl-3 col-md-6 mb-3">
                    <a href="/Reports/StockReports/StockAtReorderPoint.aspx" target="_blank" class="float-left w-100">
                        <div class="custom-card">
                            <div class="custom-card-icon blue">
                                <i class="fas fa-list-ol"></i>
                            </div>
                            <div class="custom-card-body">
                                <div class="huge">
                                    *
                                </div>
                                <p>Stock at Reorder Point </p>
                            </div>
                        </div>
                    </a>
                </div>

            </div>
            <%@ Register Src="~/Registration/_OtherManagment.ascx" TagName="_OtherManagment" TagPrefix="uc" %>
            <uc:_othermanagment id="_OtherManagment1" runat="server" />
            <section data-page="143" class="section itemsList " id="ReorderItem">
            <div class="row ">
                <div class="col-12">
                    <div class="card items" data-exclude="xs,sm,lg">
                        <div class="card-header bordered">
                            <div class="header-block w-100">
                                <h3 class="title">Items needs to be reorder </h3>
                                <a target="_blank" href="/Transactions/PurchaseNewItems.aspx" class="w-30 float-right btn btn-bm btn-sm"><i class="fas fa-cart-arrow-down"></i>Purchase Item Now </a>
                            </div>

                        </div>
                        <ul class="item-list striped">
                            <li class="item item-list-header">
                                <div class="item-row">
                                    <div class="item-col item-col-header">
                                        <div>
                                            <span>Code</span>
                                        </div>
                                    </div>
                                    <div class="item-col item-col-header item-col-title">
                                        <div>
                                            <span>Item Name</span>
                                        </div>
                                    </div>
                                    <div class="item-col item-col-header item-col-sales">
                                        <div>
                                            <span>In Hand Qty</span>
                                        </div>
                                    </div>
                                    <div class="item-col item-col-header item-col-date">
                                        <div>
                                            <span>R. O. Point</span>
                                        </div>
                                    </div>
                                    <div class="item-col item-col-header item-col-date">
                                        <div>
                                            <span>Qty Required</span>
                                        </div>
                                    </div>
                                    <div class="item-col item-col-header item-col-date">
                                        <div>
                                            <span>Last Purchased</span>
                                        </div>
                                    </div>
                                    <div class="item-col item-col-header item-col-date">
                                        <div>
                                            <span>Amount Required</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <%-- <div class="col-xl-4">
                    <div class="card sameheight-item sales-breakdown" data-exclude="xs,sm,lg">
                        <div class="card-header">
                            <div class="header-block">
                                <h3 class="title">Sales breakdown </h3>
                            </div>
                        </div>
                        <div class="card-block">
                            <div class="dashboard-sales-breakdown-chart" id="dashboard-sales-breakdown-chart"></div>
                        </div>
                    </div>
                </div>--%>
            </div>
        </section>
        </section>

        <% if (WebPOS.Model.UserRole.Admin == HttpContext.Current.Session["UserId"].ToString())
            { %>
        <section class="section">
            <div class="row sameheight-container">
                <div class="col col-12 col-sm-12 col-md-6 col-xl-5 stats-col">
                    <div class="card sameheight-item stats" data-exclude="xs">
                        <div class="card-block">
                            <div class="title-block">
                                <h4 class="title">Stats </h4>
                                <p class="title-description">
                                    Website metrics for
                                           
                                    
                                </p>
                            </div>

                            <div class="row row-sm stats-container">
                                <div class="col-12 col-sm-6 stat-col ActiveItems">
                                    <div class="stat-icon">
                                        <i class="fas fa-rocket"></i>
                                    </div>
                                    <div class="stat">
                                        <div class="value"><i class="fas fa-spinner fa-spin  main_color_dark_shade"></i></div>
                                        <div class="name">Active items </div>
                                    </div>
                                    <div class="progress stat-progress">
                                        <div class="progress-bar" style="width: 0%;"></div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 stat-col ItemsSold">
                                    <div class="stat-icon">
                                        <i class="fas fa-shopping-cart"></i>
                                    </div>
                                    <div class="stat">
                                        <div class="value "><i class="fas fa-spinner fa-spin  main_color_dark_shade"></i></div>
                                        <div class="name">Items sold </div>
                                    </div>
                                    <div class="progress stat-progress">
                                        <div class="progress-bar" style="width: 0%;"></div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6  stat-col MonthlyIncome">
                                    <div class="stat-icon">
                                        <i class="fas fa-chart-line"></i>
                                    </div>
                                    <div class="stat">
                                        <div class="value ">N/A </div>
                                        <div class="name">Monthly income </div>
                                    </div>
                                    <div class="progress stat-progress">
                                        <div class="progress-bar" style="width: 0%;"></div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 stat-col TotalIncome">
                                    <div class="stat-icon">
                                        <i class="fas fa-dollar-sign"></i>
                                    </div>
                                    <div class="stat">
                                        <div class="value ">N/A </div>
                                        <div class="name">Total income </div>
                                    </div>
                                    <div class="progress stat-progress">
                                        <div class="progress-bar" style="width: 0%;"></div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6  stat-col TotalClient">
                                    <div class="stat-icon">
                                        <i class="fas fa-users"></i>
                                    </div>
                                    <div class="stat">
                                        <div class="value "><i class="fas fa-spinner fa-spin  main_color_dark_shade"></i></div>
                                        <div class="name">Total Clients </div>
                                    </div>
                                    <div class="progress stat-progress">
                                        <div class="progress-bar" style="width: 0%;"></div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6  stat-col TotalVendors">
                                    <div class="stat-icon">
                                        <i class="fa fa-users"></i>
                                    </div>
                                    <div class="stat">
                                        <div class="value "><i class="fas fa-spinner fa-spin  main_color_dark_shade"></i></div>
                                        <div class="name">Total Vendors </div>
                                    </div>
                                    <div class="progress stat-progress">
                                        <div class="progress-bar" style="width: 0%;"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-12 col-sm-12 col-md-6 col-xl-7 history-col">
                    <div class="card sameheight-item" data-exclude="xs" id="dashboard-history">
                        <div class="card-header card-header-sm bordered">
                            <div class="header-block">
                                <h3 class="title">History</h3>
                            </div>
                            <ul class="nav nav-tabs pull-right" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#visits" role="tab" data-toggle="tab">Visits</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#downloads" role="tab" data-toggle="tab">Downloads</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-block">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active fade show" id="visits">
                                    <p class="title-description">Number of unique visits last 30 days </p>
                                    <div id="dashboard-visits-chart"></div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="downloads">
                                    <p class="title-description">Number of downloads last 30 days </p>
                                    <div id="dashboard-downloads-chart"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <%}
        %>
    </div>
</asp:Content>
<asp:Content ContentPlaceHolderID="Scripts" runat="server">
    <script>
        $(document).ready(function () {
            $.ajax({
                url: '/Services/HomePageService.asmx/GetStats',
                type: "Post",
                success: function (stats) {
                    $(".ActiveItems .value").text(stats.ActiveItems);
                    $(".ActiveItems .progress-bar").css("width", stats.ActiveItemPercentage + "%");

                    $(".ItemsSold .value").text(stats.ItemsSold);
                    $(".ItemsSold .progress-bar").css("width", stats.ItemsSoldPercentage + "%");

                    $(".TotalClient .value").text(stats.TotalClient);
                    $(".TotalClient .progress-bar").css("width", stats.TotalClientPercentage + "%");

                    $(".TotalVendors .value").text(stats.TotalVendors);
                    $(".TotalVendors .progress-bar").css("width", stats.TotalVendorsPercentage + "%");

                    retreiveItemsAtReorderPoint()
                },
                fail: function (jqXhr, exception) {

                }
            });
        });
        function retreiveItemsAtReorderPoint() {
            $.ajax({
                url: '/Services/HomePageService.asmx/GetStockAtReorderPoint',
                type: "Post",
                success: function (items) {
                    if (items.length) {
                        $(".reorderAlert").show();
                    }
                    for (var i = 0; i < items.length; i++) {
                        $(".itemsList .item-list").append(`
                            <li class="item">
                                <div class="item-row">
                                    <div class="item-col item-col-sales">
                                        <div class="item-heading">Code</div>
                                        <div>${ items[i].ItemCode}</div>
                                    </div>
                                    <div class="item-col item-col-title no-overflow">
                                        <div>
                                            <h4 class="item-title no-wrap">${ items[i].Description} </h4>
                                        </div>
                                    </div>
                                    <div class="item-col item-col-sales">
                                        <div class="item-heading">In Hand Qty</div>
                                        <div>${ items[i].Quantity}</div>
                                    </div>
                                    <div class="item-col item-col-date">
                                        <div class="item-heading">R. O. Point</div>
                                        <div>${ items[i].ReorderLevel}  </div>
                                    </div>
                                    <div class="item-col item-col-date">
                                        <div class="item-heading">Qty Required</div>
                                        <div>${ items[i].ReorderQty}  </div>
                                    </div>
                                    <div class="item-col item-col-date">
                                        <div class="item-heading">Last Purchased</div>
                                        <div>${ items[i].Rate}  </div>
                                    </div>
                                    <div class="item-col item-col-date">
                                        <div class="item-heading">Amount Required</div>
                                        <div>${ items[i].Amount}  </div>
                                    </div>
                                </div>
                            </li>`);
                    }
                    $(".itemsList .item-list").append(`
                        <li class="item">
                            <div class="item-row">
                               <a class="text-info text-center d-block w-100" target="_blank" href="/Reports/StockReports/StockAtReorderPoint.aspx"> 
                                    <div class="alert alert-info mt-3">
                                        View More Items
                                    </div>
                                </a>
                            </div>
                        </li>`)
                },
                fail: function (jqXhr, exception) {

                }
            });
        }
    </script>
</asp:Content>
