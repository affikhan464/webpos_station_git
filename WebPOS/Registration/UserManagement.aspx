﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="UserManagement.aspx.cs" Inherits="WebPOS.Registration.UserManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container-fluid mb-3">
        <div class="mt-4">
            <section class="form">
                <h2 class="form-header ">User Managment</h2>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 ">
                        <span class="input input--hoshi">
                            <a id="btnAddUser" class=" btn btn-3 btn-bm btn-3e fa-plus w-100">Add New User</a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3">                       
                        <select id="StationDD" class="dropdown" data-type="Station">
                            <option value="0">Select Station</option>
                        </select>
                    </div>
                </div>
                <div class="card sameheight-item items" data-exclude="xs,sm,lg">
                    <div class="card-header bordered">
                        <div class="header-block">
                            <h3 class="title">List of Users of <span class="StationNameText ml-1"><%=Session["StationName"].ToString() %></span> </h3>
                        </div>
                    </div>
                    <%@ Register Src="~/Registration/_UsersList.ascx" TagName="_UsersList" TagPrefix="uc" %>
                    <uc:_UsersList ID="_UsersList1" runat="server" />
                </div>
            </section>
        </div>
        <hr />
        <%@ Register Src="~/Registration/_OtherManagment.ascx" TagName="_OtherManagment" TagPrefix="uc" %>
        <uc:_OtherManagment ID="_OtherManagment1" runat="server" />
    </div>

    <div class="modal fade" id="userRegistrationModal" tabindex="-1" role="dialog" aria-labelledby="userRegistrationModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fas fa-plus"></i>Add New User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <%@ Register Src="~/Registration/_AddUser.ascx" TagName="_AddUser" TagPrefix="uc" %>
                    <uc:_AddUser ID="_AddUserHead1" runat="server" />

                    <hr />
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">
                                <a onclick="save()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editRegistrationModal" tabindex="-1" role="dialog" aria-labelledby="userRegistrationModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><i class="fas fa-edit"></i>Edi User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <uc:_AddUser ID="_AddUser1" runat="server" />

                    <hr />
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                            <span class="input input--hoshi">
                                <a onclick="saveEdit()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    <script src="/Script/Dropdown.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            GetAllUsersList($("#AppStationId").val());
            var qStationId = getQueryString('id');
            if (qStationId) {
                appendAttribute.init("StationDD", "Station", qStationId);
                appendAttribute.trigger("StationDD");
            } else {
                appendAttribute.init("StationDD", "Station");

            }
        });
        $(document).on("click", "#btnAddUser", function () {
            $("#userRegistrationModal").modal();
        });

        function SelectUser(user) {
            $("#UserIdTextBox").remove();
            $('body').append(`<input type='hidden' id='UserIdTextBox' value='${user.dataset.userid}' />`);
            $(".item.item-data.selected").removeClass("selected");
            $(user).addClass("selected");
        }
        function openEdit(btn) {
            var name = btn.dataset.name;
            var username = btn.dataset.username;
            var password = btn.dataset.password;
            var id = btn.dataset.id;
            $("#editRegistrationModal [name=id]").val(id);
            $("#editRegistrationModal [name=name]").val(name);
            $("#editRegistrationModal [name=loginName]").val(username);
            $("#editRegistrationModal [name=password]").val(password);
            $("#editRegistrationModal").modal();
            updateInputStyle();
        }
        function save() {
            if ($("#userRegistrationModal [name=name]").val().length > 0 && $("#userRegistrationModal [name=loginName]").val().length > 0 && $("#userRegistrationModal [name=password]").val().length > 0) {
                var password = $("#userRegistrationModal [name=password]").val();
                var name = $("#userRegistrationModal [name=name]").val();
                var stationId = $("[name=StationDD]").val();
                var loginName = $("#userRegistrationModal [name=loginName]").val();

                var model = {
                    Password: password,
                    Name: name,
                    UserName: loginName,
                    StationId: stationId,
                };

                var newModel = JSON.stringify({ model: model });
                $.ajax({
                    url: "/Registration/UserManagement.aspx/Save",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: newModel,
                    success: function (BaseModel) {

                        if (BaseModel.d.Success) {
                            smallSwal("Saved", BaseModel.d.Message, "success");
                            GetAllUsersList(stationId);
                            $(".UserForm input").val('');
                            updateInputStyle();
                            $("#userRegistrationModal").modal('hide');
                        }
                        else {
                            smallSwal("Failed", BaseModel.d.Message, "error");
                        }


                    }

                });


            } else
                swal("All fields are mandatory.", "", "warning")
        }

        function saveEdit() {
            if ($("#editRegistrationModal [name=name]").val().length > 0 && $("#editRegistrationModal [name=loginName]").val().length > 0 && $("#editRegistrationModal [name=password]").val().length > 0) {
                var password = $("#editRegistrationModal [name=password]").val();
                var name = $("#editRegistrationModal [name=name]").val();
                var loginName = $("#editRegistrationModal [name=loginName]").val();
                var id = $("#editRegistrationModal [name=id]").val();
                var stationId = $("[name=StationDD]").val();

                var model = {
                    Code: id,
                    Password: password,
                    Name: name,
                    UserName: loginName,
                   StationId: stationId
                };

                var newModel = JSON.stringify({ model: model });
                $.ajax({
                    url: "/Registration/UserManagement.aspx/SaveEdit",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: newModel,
                    success: function (BaseModel) {

                        if (BaseModel.d.Success) {
                            smallSwal("Updated", BaseModel.d.Message, "success");
                           
                            GetAllUsersList(stationId);
                            $(".UserForm input").val('');
                            updateInputStyle();
                            $("#editRegistrationModal").modal('hide');

                        }
                        else {
                            smallSwal("Failed", BaseModel.d.Message, "error");
                        }


                    }

                });


            } else
                swal("All fields are mandatory.", "", "warning")
        }
        function selectedChanged(data, Dropdown_ID){
            var stationName = data.text;
            var stationId = data.value;
            $(".StationNameText").text(stationName);
            GetAllUsersList(stationId );

        }
    </script>

</asp:Content>
