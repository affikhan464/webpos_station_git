﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using WebPOS.Model;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS
{
    public partial class UpdateSellingPrice : System.Web.UI.Page
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;





        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {


            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(ItemModel ItemModel)
        {
            var CompID = "01";
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                var ItemCode = ItemModel.Code;
                var SellingCost1 = ItemModel.SellingCost1;
                var SellingCost2 = ItemModel.SellingCost2;
                var SellingCost3 = ItemModel.SellingCost3;
                var SellingCost4 = ItemModel.SellingCost4;
                var SellingCost5 = ItemModel.SellingCost5;
                var Ave_Cost = ItemModel.Ave_Cost;
                var FixedCost = ItemModel.FixedCost;
                var DealRs1 = ItemModel.DealRs1;
                var DealRs2 = ItemModel.DealRs2;
                var DealRs3 = ItemModel.DealRs3;
                var MinLimit = ItemModel.MinLimit;
                var MaxLimit = ItemModel.MaxLimit;

                var message = "";

                SqlCommand cmdUpdate = new SqlCommand("update InvCode set DealRs=" + DealRs1 + ",DealRs2=" + DealRs2 + ",DealRs3=" + DealRs3 + ", MinLimit=" + MinLimit + " , MaxLimit=" + MaxLimit + " , Ave_Cost=" + Ave_Cost + " ,  SellingCost2=" + SellingCost2 + " ,  SellingCost3=" + SellingCost3 + ", SellingCost4=" + SellingCost4 + ", SellingCost5=" + SellingCost5 + ", SellingCost=" + SellingCost1 + ",FixedCost=" + FixedCost + " where CompID='" + CompID + "' and Code=" + ItemCode, con, tran);
                cmdUpdate.ExecuteNonQuery();
                message = "Item UpDated Successfully!";





                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }





    }
}
