﻿<%@  Control Language="C#" AutoEventWireup="true" CodeBehind="_AttributeModal.ascx.cs" Inherits="WebPOS.Registration._AttributeModal" %>

<div class="modal fade" id="attributeModal" tabindex="-1" role="dialog" aria-labelledby="attributeModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="attributeModalLabel">New message</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div id="attributeModalForm" class="form" data-saveattribute="">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                    <span class="input input--hoshi">
                                        <input id="attributeName" name="AttributeName" required="required" class="AttributeName empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
                                        <label class="input__label input__label--hoshi">
                                            <span class="input__label-content input__label-content--hoshi">Name </span>
                                        </label>
                                    </span>
                                </div>

                                <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                                    <span class="input input--hoshi">
                                        <input id="attributeCode" name="AttributeCode" disabled="disabled" class="AttributeCode empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
                                        <label class="input__label input__label--hoshi">
                                            <span class="input__label-content input__label-content--hoshi">Code </span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                            <div class="row brandCheckBoxes">
                                <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                    <span class="input input--hoshi checkbox--hoshi">
                                        <label>
                                            <input id="chkBlocked" class="checkbox" type="checkbox" />
                                            <span></span>
                                            Blocked
                                        </label>
                                    </span>
                                </div>
                                <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                                    <span class="input input--hoshi checkbox--hoshi">
                                        <label>
                                            <input id="chkVerify" class="checkbox" type="checkbox" />
                                            <span></span>
                                            Verify
                                        </label>
                                    </span>
                                </div>
                            </div>
                            <hr />
                            <div class="row">

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <span class="input input--hoshi">
                                        <a class=" btn btn-3 btn-bm btn-3e fa-times w-100" data-dismiss="modal">Close</a>
                                    </span>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <span class="input input--hoshi">
                                        <a onclick="saveAttribute()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                                    </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col col-12 col-sm-12 col-md-12 col-lg-12-col-xl-12">

                                    <div class="allAttributes">

                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead class="thead-inverse">
                                                    <tr>
                                                        <th>Sr.</th>
                                                        <th class="text-left">Code</th>

                                                        <th class="text-left">Name</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>