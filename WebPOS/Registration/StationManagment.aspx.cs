﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;
using WebPOS.Model;

namespace WebPOS.Registration
{
    public partial class StationManagment : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(Brand Brand)
        {
            var CompID = "01";
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                var id = Brand.Code;
                var name = Brand.Name;

                Module1 objModule1 = new Module1();
                var message = "";
                if (Convert.ToDecimal(id) > 0)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Update Station set Name='" + name + "' where id=" + id + " and CompId='" + CompID + "' ", con, tran);
                    cmdUpdate.ExecuteNonQuery();
                    message = "Station UpDated";

                }
                else
                {
                    var NewStationId = objModule1.MaxStationCode();
                    var cmd = new SqlCommand("Insert into Station(ID,Name,CompID) Values(" + NewStationId + ",'" + name + "','" + CompID + "')", con, tran);
                    cmd.ExecuteNonQuery();
                  
                    var str = "Select ICode from InventoryOpeningBalance where CompID='" + CompID + "' group by ICode";
                    var cmd2 = new SqlCommand(str, con,tran);
                    var adapter = new SqlDataAdapter(cmd2);
                    var dt = new DataSet();
                    adapter.Fill(dt);
                    var data = dt.Tables[0];
                    var module7 = new Module7();
                    var insertStr = string.Empty;
                    for (int i = 0; i < data.Rows.Count; i++)
                    {
                        var ICode = data.Rows[i]["ICode"].ToString();
                        insertStr += "Insert into InventoryOpeningBalance(ICode,Dat,StationID,Opening,CompID) values(" + ICode + ",'" + module7.StartDate() + "'," + NewStationId + "," + 0 + ",'" + CompID + "');";
                        
                    }
                    var cmd1 = new SqlCommand(insertStr, con, tran);
                    cmd1.ExecuteNonQuery();
                    message = "New Station Registered susscesfully.";

                }



                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel DeleteStation(Brand Brand)
        {
            var CompID = "01";
            try
            {
                if (con.State == ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                ModItem objModItem = new ModItem();
                var code = Brand.Code;

                Module1 objModule1 = new Module1();

                var message = "";
                SqlCommand cmdDelete1 = new SqlCommand("select StationID from Inventory where CompID='" + CompID + "' and StationID=" + code, con, tran);
                var isTransactionExist = cmdDelete1.ExecuteScalar();
                if (isTransactionExist == null)
                {
                    SqlCommand cmdDelete2 = new SqlCommand("delete from Station where id=" + code + " and CompID='" + CompID + "'", con, tran);
                    cmdDelete2.ExecuteNonQuery();

                    SqlCommand cmdDelete3 = new SqlCommand("delete from InventoryOpeningBalance where StationID=" + code + " and CompID='" + CompID + "'", con, tran);
                    cmdDelete3.ExecuteNonQuery();

                    SqlCommand cmdDelete4 = new SqlCommand("delete from VoucherNoVerification where StationID=" + code + " and CompID='" + CompID + "'", con, tran);
                    cmdDelete4.ExecuteNonQuery();

                    message = "Station Deleted";
                }
                else
                {

                    return new BaseModel() { Success = false, Message = "Station can not be deleted, because it is assigned to item." };

                }



                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };


            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
            finally
            {
                con.Close();
            }
        }




    }
}
