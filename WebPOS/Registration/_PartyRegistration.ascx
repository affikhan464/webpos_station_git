﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="_PartyRegistration.ascx.cs" Inherits="WebPOS.Registration._PartyRegistration" %>

<div class="row PartyForm">
    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
        <span class="input input--hoshi">
            <input id="RegPartyCode" disabled="disabled" class="RegPartyCode input__field input__field--hoshi" disabled="disabled" type="text" autocomplete="off" />
            <label class="input__label input__label--hoshi">
                <span class="input__label-content input__label-content--hoshi">Party Code</span>
            </label>
        </span>
    </div>
    <div class="col-6 col-sm-6 col-md-2 col-lg-2 col-xl-2">
        <span class="input input--hoshi radio--hoshi">
            <label>
                <input id="RegClient" name="NorBalance" checked="checked" value="1" class="radio" type="radio" />
                <span></span>
                Client
            </label>
        </span>
    </div>
    <div class="col-6 col-sm-6 col-md-2 col-lg-2 col-xl-2">
        <span class="input input--hoshi radio--hoshi">
            <label>
                <input id="RegSupplyer" name="NorBalance" class="radio" value="2" type="radio" />
                <span></span>
                Supplyer
            </label>
        </span>
    </div>
    <div class="col-12 col-sm-10">
        <span class="input input--hoshi">
            <input id="txtPartyName" class="RegPartyName empty1 input__field input__field--hoshi" type="text" autocomplete="off" />
            <label class="input__label input__label--hoshi">
                <span class="input__label-content input__label-content--hoshi">Name </span>
            </label>
        </span>
    </div>
    <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 resetBtn">
        <span class="input input--hoshi">
            <a class="btn btn-3 btn-bm btn-3e w-100" href=""><i class="fas fa-sync"></i>Reset </a>
        </span>
    </div>
    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
        <span class="input input--hoshi">
            <input id="txtContactPerson" class="RegContactPerson empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
            <label class="input__label input__label--hoshi">
                <span class="input__label-content input__label-content--hoshi">Contact Person Name</span>
            </label>
        </span>
    </div>
    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
        <span class="input input--hoshi">
            <input id="txtMobileNo" class="RegMobileNo empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
            <label class="input__label input__label--hoshi">
                <span class="input__label-content input__label-content--hoshi">Mobile No. </span>
            </label>
        </span>
    </div>
    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
        <span class="input input--hoshi">
            <input id="txtFaxNo" class="RegFaxNo empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
            <label class="input__label input__label--hoshi">
                <span class="input__label-content input__label-content--hoshi">Fax No. </span>
            </label>
        </span>
    </div>
    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
        <span class="input input--hoshi">
            <input id="txtLandLineNo" class="RegLandLineNo empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
            <label class="input__label input__label--hoshi">
                <span class="input__label-content input__label-content--hoshi">Land Line No. </span>
            </label>
        </span>
    </div>
    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <span class="input input--hoshi">
            <input id="txtAddress" class="RegAddress empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
            <label class="input__label input__label--hoshi">
                <span class="input__label-content input__label-content--hoshi">Address </span>
            </label>
        </span>
    </div>
    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-4">
        <span class="input input--hoshi">
            <input id="txtEmail" class="RegEmail empty1 input__field input__field--hoshi" autocomplete="off" type="text" />
            <label class="input__label input__label--hoshi">
                <span class="input__label-content input__label-content--hoshi">E-mail </span>
            </label>
        </span>
    </div>


    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
        <span class="input input--hoshi  input--filled">
            <select id="lstSellingPriceNo" class="round input__field--hoshi">
            </select>
            <label class="input__label input__label--hoshi">
                <span class="input__label-content input__label-content--hoshi">Selling Price </span>
            </label>
        </span>
    </div>




    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
        <span class="input input--hoshi  input--filled">
            <select id="lstDealApplyNo" class="round input__field--hoshi">
            </select>
            <label class="input__label input__label--hoshi">
                <span class="input__label-content input__label-content--hoshi">Deal Apply No. </span>
            </label>
        </span>
    </div>

    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
        <span class="input input--hoshi">
            <input id="txtCreditLimit" class="RegCreditLimit empty1 input__field input__field--hoshi" min="0" type="number" />
            <label class="input__label input__label--hoshi">
                <span class="input__label-content input__label-content--hoshi">Credit Limit  </span>
            </label>
        </span>
    </div>
    <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
        <span class="input input--hoshi checkbox--hoshi">
            <label>
                <input id="chkCreditLimitApply" class="checkbox" type="checkbox" />
                <span></span>
                Credit Limit Apply
            </label>
        </span>
    </div>

    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
        <span class="input input--hoshi">
            <input id="txtOtherInfo" class="RegOtherInfo empty1 input__field input__field--hoshi" type="text" />
            <label class="input__label input__label--hoshi">
                <span class="input__label-content input__label-content--hoshi">Other Info </span>
            </label>
        </span>
    </div>



    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-3">
        <span class="input input--hoshi  input--filled">
            <select id="lstSaleMan" class="round input__field--hoshi">
            </select>
            <label class="input__label input__label--hoshi">
                <span class="input__label-content input__label-content--hoshi">SaleMan </span>
            </label>
        </span>
    </div>

    <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
        <span class="input input--hoshi checkbox--hoshi">
            <label>
                <input id="chkDeal" class="checkbox" type="checkbox" />
                <span></span>
                Deal
            </label>
        </span>
    </div>

    <div class="col-6 col-sm-6 col-md-3 col-lg-3 col-xl-3">
        <span class="input input--hoshi checkbox--hoshi">
            <label>
                <input id="chkPerDiscount" class="checkbox" type="checkbox" />
                <span></span>
                Per. Discount
            </label>
        </span>
    </div>

</div>
<hr />
<div class="row">
    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <span class="input input--hoshi">
            <a id="btnEdit" href="/Correction/PartyRegistrationEdit.aspx" target="_blank" class="btnEdit btn btn-3 btn-bm btn-3e fa-edit w-100">Edit</a>
        </span>
    </div>
    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <span class="input input--hoshi">
            <a id="btnSave" onclick="SavePartyBefore()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
        </span>
    </div>
</div>
