﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using WebPOS.Model;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS.Registration
{
    public partial class BankAccountManagment : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(BankModel BankModel)
        {
            var CompID = "01";
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();
                var BankCode = BankModel.BankCode;
                var BankName = BankModel.BankName;
                var AccountNo = BankModel.AccountNo;
                var Title = BankModel.Title;
                var Address = BankModel.Address;
                var Phone = BankModel.Phone;
                var Fax = BankModel.Fax;

                Module7 objModule7 = new Module7();
                var message = "";
                if (BankName != "" && BankCode != "")
                {
                    SqlCommand cmdUpdate = new SqlCommand("Update GLCode set Title='" + BankName + "' where Code='" + BankCode + "'", con, tran);
                    cmdUpdate.ExecuteNonQuery();

                    SqlCommand cmdUpdate1 = new SqlCommand("Update BankCode set BankName='" + BankName + "', AccountNo='" + AccountNo + "',Title='" + Title + "',Address='" + Address + "',Phone='" + Phone + "',Fax='" + Fax + "' where BankCode ='" + BankCode + "'", con, tran);
                    cmdUpdate1.ExecuteNonQuery();


                    message = "Bank Name UpDated";

                }
                else
                if (BankName != "" && BankCode == "")
                {
                    ModGLCode objModGLCode = new ModGLCode();
                    BankCode = objModGLCode.MaxGLCodeLevel5("01010102");
                    SqlCommand cmdNew = new SqlCommand("Insert into GLCode (Code,Title,NorBalance,Lvl,GroupDet,Header_Detail,CompID) Values('" + BankCode + "','" + BankName + "," + AccountNo + "'," + 1 + "," + 5 + ",'" + "Balance Sheet" + "','" + "Detail" + "','" + CompID + "')", con, tran);
                    cmdNew.ExecuteNonQuery();

                    SqlCommand cmdNew1 = new SqlCommand("insert into GLOpeningBalance(Code,Date1,CompID)values('" + BankCode + "','" + objModule7.StartDate() + "','" + CompID + "')", con, tran);
                    cmdNew1.ExecuteNonQuery();

                    SqlCommand cmdNew2 = new SqlCommand("Insert into BankCode(BankCode,BankName,AccountNo,Title,Address,CompID,Phone,Fax) Values('" + BankCode + "','" + BankName + "','" + AccountNo + "','" + Title + "','" + Address + "','" + CompID + "','" + Phone + "','" + Fax + "')", con, tran);
                    cmdNew2.ExecuteNonQuery();
                    message = "New Back Registered susscesfully.";
                }



                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel DeleteHead(Brand Brand)
        {
            var CompID = "01";
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();


                var GLCodeOld = Brand.Code;
                var BankTitle = Brand.Name;
                ModGLCode objModGLCode = new ModGLCode();
                //var TransactionExistAgainstGLCode = objModGLCode.TransactionExistAgainstGLCode(GLCodeOld);
                var message = "";
                ////if (TransactionExistAgainstGLCode == "No")
                ////{   
                ////    objModGLCode.DeleteGLCode(GLCodeOld);
                ////    message = "GL Head Deleted";
                ////}
                ////else
                ////{
                ////    message = "GL Head can not be deleted Transaction Exist";

                ////}
                message = objModGLCode.DeleteGLCodeBank(GLCodeOld);

                tran.Commit();
                con.Close();

                return new BaseModel() { Success = true, Message = message };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }


    }

}