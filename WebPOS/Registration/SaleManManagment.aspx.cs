﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using WebPOS.Model;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS.Registration
{
    public partial class SaleManManagment : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(Brand Brand)
        {
            var CompID = "01";
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                var ID = Brand.Code;
                var Name = Brand.Name;

                Module1 objModule1 = new Module1();
                var message = "";
                if (Convert.ToDecimal(ID) > 0)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Update SaleManList set Name='" + Name + "' where id=" + ID, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                    message = "Saleman UpDated";
                }
                else
                {
                    var MaxSaleManID = objModule1.MaxSaleManCode();
                    SqlCommand cmdNew = new SqlCommand("Insert into SaleManList(ID,Name,CompID) Values(" + MaxSaleManID + ",'" + Name + "','" + CompID + "')", con, tran);
                    cmdNew.ExecuteNonQuery();
                    message = "New Saleman Registered susscesfully.";
                }



                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel DeleteSaleMan(Brand Brand)
        {

            try
            {
                var SaleManID = Brand.Code;
                ModItem objModItem = new ModItem();
                var AlreadyAsignedToInVoice = objModItem.SaleManAlreadyAssignedToSaleInvoice(SaleManID);
                var message = "";
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();
                if (!AlreadyAsignedToInVoice)
                {
                    SqlCommand cmdDelete1 = new SqlCommand("delete from SaleManList where id=" + SaleManID, con, tran);
                    cmdDelete1.ExecuteNonQuery();

                    message = "Saleman Deleted";
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "Saleman can not be deleted assigned to sale invoice" };

                }
                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
            finally
            {
                con.Close();
            }

        }
    }

}