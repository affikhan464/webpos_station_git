﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using WebPOS.Model;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS.Registration
{
    public partial class ColorManagment : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(Brand Brand)
        {
            var CompID = "01";
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                var ColorID = Brand.Code;
                var ColorName = Brand.Name;

                Module1 objModule1 = new Module1();
                var message = "";
                if (Convert.ToDecimal(ColorID) > 0)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Update Color set Color='" + ColorName + "' where id=" + ColorID, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                    message = "Color UpDated";

                }
                else
                {
                    var MaxColorID = objModule1.MaxColorCode();
                    SqlCommand cmdDelete = new SqlCommand("Insert into Color(ID,Color,CompID) Values(" + MaxColorID + ",'" + ColorName + "','" + CompID + "')", con, tran);
                    cmdDelete.ExecuteNonQuery();
                    message = "New Color Registered susscesfully.";
                }



                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel DeleteColor(Brand Brand)
        {
            var CompID = "01";
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                ModItem objModItem = new ModItem();
                var ColorCode = Brand.Code;


                var AlreadyAsignedToItem = objModItem.ColorAssignedToItem(ColorCode);
                var message = "";
                if (!AlreadyAsignedToItem)
                {
                    SqlCommand cmdDelete1 = new SqlCommand("delete from Color where id=" + ColorCode + " and CompID='" + CompID + "'", con, tran);
                    cmdDelete1.ExecuteNonQuery();
                    message = "Color Deleted";
                }
                else
                {
                    message = "Color can not be deleted assigned";
                    return new BaseModel() { Success = false, Message = message };
                }



                tran.Commit();
                con.Close();

                return new BaseModel() { Success = true, Message = message };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
            finally
            {
                con.Close();
            }
        }


    }

}