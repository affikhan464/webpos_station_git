﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using WebPOS.Model;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS.Registration
{
    public partial class ExpenceHeadManagment : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(Brand Brand)
        {
            var CompID = "01";
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();
                var Levl4GLCode = Brand.Code;
                var GLCodeOld = Brand.Code2;
                var NewGLTitle = Brand.Name;

                Module7 objModule7 = new Module7();
                var message = "";
                if (NewGLTitle != "" && GLCodeOld != "")
                {
                    SqlCommand cmdUpdate = new SqlCommand("Update GLCode set Title='" + NewGLTitle + "' where Code='" + GLCodeOld + "'", con, tran);
                    cmdUpdate.ExecuteNonQuery();
                    message = "Expence Name UpDated";

                }
                else
                if (NewGLTitle != "" && GLCodeOld == "")
                {
                    ModGLCode objModGLCode = new ModGLCode();
                    var GLCodeLevl5 = objModGLCode.MaxGLCodeLevel5(Levl4GLCode);
                    SqlCommand cmdNew = new SqlCommand("Insert into GLCode (Code,Title,NorBalance,Lvl,GroupDet,Header_Detail,CompID) Values('" + GLCodeLevl5 + "','" + NewGLTitle + "'," + 1 + "," + 5 + ",'" + "Profit and Loss" + "','" + "Detail" + "','" + CompID + "')", con, tran);
                    cmdNew.ExecuteNonQuery();

                    SqlCommand cmdNew1 = new SqlCommand("insert into GLOpeningBalance(Code,Date1,CompID)values('" + GLCodeLevl5 + "','" + objModule7.StartDate() + "','" + CompID + "')", con, tran);
                    cmdNew1.ExecuteNonQuery();
                    message = "New Expence Head Registered susscesfully.";
                }



                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel DeleteHead(Brand Brand)
        {
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                var GLCodeOld = Brand.Code;
                ModGLCode objModGLCode = new ModGLCode();
                var isDeleted = objModGLCode.DeleteGLCode(GLCodeOld);

                tran.Commit();
                con.Close();
                if (isDeleted)
                {
                    return new BaseModel() { Success = true, Message = "GL Head Deleted!!" };
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "GL Head can not be deleted,there is transaction exist against this GL Head." };
                }

            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }


    }

}