﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using WebPOS.Model;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS.Registration
{
    public partial class GoDownManagment : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(Brand Brand)
        {
            var CompID = "01";
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                var GoDownID = Brand.Code;
                var GoDownName = Brand.Name;

                Module1 objModule1 = new Module1();
                var message = "";
                if (Convert.ToDecimal(GoDownID) > 0)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Update GoDown set Name='" + GoDownName + "' where id=" + GoDownID, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                    message = "GoDown UpDated";

                }
                else
                {
                    var MaxGoDownID = objModule1.MaxGodown();
                    SqlCommand cmdDelete = new SqlCommand("Insert into GoDown(ID,Name,CompID) Values(" + MaxGoDownID + ",'" + GoDownName + "','" + CompID + "')", con, tran);
                    cmdDelete.ExecuteNonQuery();
                    message = "New GoDown Registered susscesfully.";
                }



                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel DeleteGoDown(Brand Brand)
        {
            var CompID = "01";
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                ModItem objModItem = new ModItem();
                var GoDownCode = Brand.Code;

                Module1 objModule1 = new Module1();
                var AlreadyAsignedToItem = objModItem.GoDownAssignedToItem(GoDownCode);
                var message = "";
                if (!AlreadyAsignedToItem)
                {
                    SqlCommand cmdDelete1 = new SqlCommand("delete from GoDown where id=" + GoDownCode + " and CompID='" + CompID + "'", con, tran);
                    cmdDelete1.ExecuteNonQuery();
                    message = "GoDown Deleted";
                }
                else
                {
                    message = "GoDown can not be deleted assigned";
                    return new BaseModel() { Success = false, Message = message };
                }



                tran.Commit();
                con.Close();

                return new BaseModel() { Success = true, Message = message };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
            finally
            {
                con.Close();
            }
        }


    }

}