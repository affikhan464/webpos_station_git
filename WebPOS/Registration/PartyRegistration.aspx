﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="PartyRegistration.aspx.cs" Inherits="WebPOS.PartyRegistration" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    

<div class="container-fluid">
    <div class="mt-4">
        <section class="form">
            <h2 class="form-header fa-user-plus">Party Registration</h2>
             <%@ Register Src="~/Registration/_PartyRegistration.ascx" TagName="_PartyRegistration" TagPrefix="uc" %>
            <uc:_PartyRegistration ID="_PartyRegistrationHead1" runat="server" />
        </section>
    </div>
</div>
</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">

    <script src="/Script/PartyRegistration.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
</asp:Content>
