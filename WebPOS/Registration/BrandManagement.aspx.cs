﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using WebPOS.Model;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS.Registration
{
    public partial class BrandManagement : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(Brand Brand)
        {
            var CompID = "01";
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                var BrandCode = Brand.Code;
                var BrandName = Brand.Name;
                var Blocked = Brand.Blocked;
                var Verify = Brand.VerifyIME;
                Module1 objModule1 = new Module1();
                var message = "";

                if (Convert.ToDecimal(BrandCode) > 0)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Update BrandName set VerifyIME=" + Verify + ",   Blocked='" + Blocked + "', Name='" + BrandName + "' where Code=" + BrandCode, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                    message = "Brand UpDated";

                }
                else
                {
                    var MaxBrandCode = objModule1.MaxBrandCode();
                    SqlCommand cmdDelete = new SqlCommand("Insert into BrandName(Code,Name,Blocked,VerifyIME,CompID) Values(" + MaxBrandCode + ",'" + BrandName + "'," + Blocked + "," + Verify + ",'" + CompID + "')", con, tran);
                    cmdDelete.ExecuteNonQuery();
                    message = "New Brand Registered susscesfully.";
                }


                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel DeleteBrand(string Code)
        {
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                ModItem objModItem = new ModItem();
                var BrandCode = Code;

                Module1 objModule1 = new Module1();
                var AlreadyAsignedToItem = objModItem.BrandAssignedToItem(BrandCode);
                var message = "";
                if (AlreadyAsignedToItem)
                {
                    SqlCommand cmdDelete1 = new SqlCommand("delete from BrandName where Code=" + BrandCode, con, tran);
                    cmdDelete1.ExecuteNonQuery();
                    SqlCommand cmdDelete2 = new SqlCommand("delete  from VoucherNoVerification where V_No='" + BrandCode + "' and V_Type='MaxBrandCode'", con, tran);
                    cmdDelete2.ExecuteNonQuery();
                    message = "Brand Deleted Successfully!!";
                }
                else
                {
                    message = "Brand can not deleted assigned";

                }



                tran.Commit();
                con.Close();

                return new BaseModel() { Success = true, Message = message };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }


    }

}