﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using WebPOS.Model;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS.Registration
{
    public partial class BrandWiseDiscount : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(SaveBrandWiseDiscountModel SaveBrandWiseDiscountModel)
        {
            var CompID = "01";
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                var PartyCode = SaveBrandWiseDiscountModel.PartyCode;


                SqlCommand cmdDelete = new SqlCommand("Delete from BrandWiseDiscount where  CompID='" + CompID + "' and PartyCode='" + PartyCode + "'", con, tran);
                cmdDelete.ExecuteNonQuery();


                
                foreach (var brand in SaveBrandWiseDiscountModel.Brands )
                {

                    var code = brand.Code;
                    var rate = brand.Rate;
                    SqlCommand cmd3 = new SqlCommand("Insert into  BrandWiseDiscount(PartyCode, BrandCode, Disc_Rate, Disc_Rate_Plus, CompID) values('" + PartyCode + "', " + code + ", " + rate + ", " + 0 + ", '" + CompID + "')", con, tran);
                    cmd3.ExecuteNonQuery();
                }

                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Brand Rate Updated" };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }


    }
}