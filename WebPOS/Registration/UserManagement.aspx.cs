﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using WebPOS.Model;
namespace WebPOS.Registration
{
    public partial class UserManagement : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(User model)
        {
            try
            {
                var stationId = model.StationId;
                if (con.State == ConnectionState.Closed) { con.Open(); }

                SqlCommand userCmd = new SqlCommand("Select sno From Userss where UserName='" + model.UserName + "' and StationId='" + stationId + "'", con);
                var isUserExist = userCmd.ExecuteScalar();
                if (isUserExist == null)
                {
                    SqlCommand snoCmd = new SqlCommand("Select MAX(sno) From Userss", con);
                    var ind = snoCmd.ExecuteScalar();
                    var id = (int)snoCmd.ExecuteScalar();
                    tran = con.BeginTransaction();

                    SqlCommand cmdInsert = new SqlCommand("Insert into Userss(Sno,Name,UserName,Password,CompId,StationId,AddedBy) Values(" + (id + 1) + ",'" + model.Name + "','" + model.UserName + "','" + model.Password + "','" + CompID + "'," + stationId + "," + HttpContext.Current.Session["UserId"].ToString() + ")", con, tran);
                    cmdInsert.ExecuteNonQuery();

                    tran.Commit();
                    con.Close();
                    return new BaseModel() { Success = true, Message = "User Added Successfully" };
                }
                else
                {
                    return new BaseModel() { Success = false, Message = "User name already taken." };
                }
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel SaveEdit(User model)
        {

            try
            {
                if (con.State == ConnectionState.Closed) { con.Open(); }
              
                var isAdmin = HttpContext.Current.Session["UserId"] == null? false:HttpContext.Current.Session["UserId"].ToString() == "0";  //0 mean admin
                if (isAdmin)
                {

                    SqlCommand userCmd = new SqlCommand("Select sno From Userss where sno='" + model.Code + "' and StationId=" + model.StationId + "", con);
                    var isUserExist = userCmd.ExecuteScalar();
                    if (isUserExist != null)
                    {
                        SqlCommand usernameCmd = new SqlCommand("Select sno From Userss where sno !=" + model.Code + " and UserName = '" + model.UserName + "' and StationId=" + model.StationId + "", con);
                        var isUserNameExist = usernameCmd.ExecuteScalar();
                        if (isUserNameExist != null)
                        {
                            return new BaseModel() { Success = false, Message = "Login Name Already Exist." };

                        }

                        var stationId = model.StationId;
                        tran = con.BeginTransaction();
                        SqlCommand cmdInsert = new SqlCommand("Update Userss Set Name='" + model.Name + "', UserName='" + model.UserName + "',Password='" + model.Password + "', UpdatedBy=" + HttpContext.Current.Session["UserId"].ToString() + " Where sno=" + model.Code + " and StationId='" + stationId + "'", con, tran);
                        cmdInsert.ExecuteNonQuery();

                        tran.Commit();
                        con.Close();
                        return new BaseModel() { Success = true, Message = "User Updated Successfully" };
                    }
                    else
                    {
                       
                        return new BaseModel() { Success = false, Message = "No User Exist." };
                    }
                }
                else
                {
                  
                    return new BaseModel() { Success = false, Message = "You do not have access to do this." };

                }
            }
            catch (Exception ex)
            {
                if (tran!=null)
                {
                    tran.Rollback();
                }
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

    }

}