﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using WebPOS.Model;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS.Registration
{
    public partial class ManufacturerManagment : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(Brand Brand)
        {
            var CompID = "01";
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                var ManufacturerID = Brand.Code;
                var ManufacturerName = Brand.Name;

                Module1 objModule1 = new Module1();
                var message = "";
                if (Convert.ToDecimal(ManufacturerID) > 0)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Update Manufacturers set Name='" + ManufacturerName + "' where id=" + ManufacturerID, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                    message = "Manufacturer UpDated";

                }
                else
                {
                    var MaxManufacturerID = objModule1.MaxManufCode();
                    SqlCommand cmdDelete = new SqlCommand("Insert into Manufacturers(ID,Name,CompID) Values(" + MaxManufacturerID + ",'" + ManufacturerName + "','" + CompID + "')", con, tran);
                    cmdDelete.ExecuteNonQuery();
                    message = "New Manufacturer Registered susscesfully.";
                }



                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel DeleteManufacturer(Brand Brand)
        {
            var CompID = "01";
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                ModItem objModItem = new ModItem();
                var ManufacturerCode = Brand.Code;

                Module1 objModule1 = new Module1();
                var AlreadyAsignedToItem = objModItem.ManufacturerAssignedToItem(ManufacturerCode);
                var message = "";
                if (!AlreadyAsignedToItem)
                {
                    SqlCommand cmdDelete1 = new SqlCommand("delete from Manufacturers where id=" + ManufacturerCode + " and CompID='" + CompID + "'", con, tran);
                    cmdDelete1.ExecuteNonQuery();
                    message = "Manufactuere Deleted";
                }
                else
                {
                    message = "Manufactuere can not be deleted assigned";
                    return new BaseModel() { Success = false, Message = message };
                }



                tran.Commit();
                con.Close();

                return new BaseModel() { Success = true, Message = message };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
            finally
            {
                con.Close();
            }
        }


    }

}