﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="_UsersList.ascx.cs" Inherits="WebPOS.Registration._UsersList" %>

<ul class="item-list striped user-list">
    <li class="item item-list-header">
        <div class="item-row">
            <div class="item-col item-col-header">
                <div>
                    <span>Sr. No.</span>
                </div>
            </div>
            <div class="item-col item-col-header">
                <div>
                    <span>Code</span>
                </div>
            </div>
            <div class="item-col item-col-header">
                <div>
                    <span>Name</span>
                </div>
            </div>
            <%if (Request.Url.AbsolutePath.Contains("UserManagement.aspx"))
                { %>
            <div class="item-col item-col-header">
                <div>
                    <span>Login Name</span>
                </div>
            </div>
            <div class="item-col item-col-header">
                <div>
                    <span>Password</span>
                </div>
            </div>
            <%if ((bool)HttpContext.Current.Session["IsAdmin"])
                     { %>

            <div class="item-col item-col-header pr-5">
                <div>
                    <span></span>
                </div>
            </div>
            <%} %>
            <%} %>
        </div>
    </li>
    <li class="userLoader text-center">
        <i class="fas fa-spinner fa-spin"></i> Loading users...
    </li>
</ul>
