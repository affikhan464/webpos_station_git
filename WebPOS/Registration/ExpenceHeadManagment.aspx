﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="ExpenceHeadManagment.aspx.cs" Inherits="WebPOS.Registration.ExpenceHeadManagment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  
    <div class="container-fluid mb-3">
        <div class="mt-4">

            <section class="form">
                <h2 class="form-header fa-money-bill-alt">Expence Head Managment</h2>
                <div class="row">
                     <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <span class="input input--hoshi  input--filled">
                            <select id="lstLel4List" class="round input__field--hoshi" onchange="GetOnClickLevel5List()">
                            </select>
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Control Account </span>
                            </label>
                        </span>
                    </div>
                    
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <span class="input input--hoshi">
                            <input class="BrandName empty1 input__field input__field--hoshi" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Account Name</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                        <span class="input input--hoshi">

                            <input id="txtSellingPrice1" class="BrandCode input__field input__field--hoshi empty1"  disabled="disabled"  type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Account Code</span>
                            </label>
                        </span>
                    </div>
                   
                </div>

                <hr />

                <div class="row">
                      <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100 fa-sync"id="btnReset" onclick="reset()"> Reset </a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a id="btnSave"  onclick="save()"  class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                        </span>
                    </div>
                </div>

            </section>
        </div>

    </div>
    <div class="container-fluid">
        <div class="card sameheight-item items" data-exclude="xs,sm,lg">
            <div class="card-header bordered">
                <div class="header-block">
                    <h3 class="title">List of Expence Heads </h3>
                </div>
               
            </div>
            <ul class="item-list striped">
                <li class="item item-list-header">
                    <div class="item-row">
                        <div class="item-col item-col-header item-col-sales">
                            <div>
                                <span>Sr. No.</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header item-col-sales">
                            <div>
                                <span>Code</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header item-col-title">
                            <div>
                                <span>Name</span>
                            </div>
                        </div>

                        <div class="item-col item-col-header item-col-date">
                        </div>
                    </div>
                </li>

            </ul>
        </div>
    </div>
   
    </asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">


    <script type="text/javascript">
        $(document).ready(function () {

            GetLevel4List();
            GetLevel5List("01040201");

        });
        function reset() {
            $(".empty1").val("");
        }
        function GetOnClickLevel5List() {
            var GLCode = $("#lstLel4List").val();
            GetLevel5List(GLCode);
        }

        function GetLevel5List(GLCode) {

            $.ajax({
                url: '/WebPOSService.asmx/Level5_List',
                type: "Post",

                data: { "GLCode": GLCode },


                success: function (BrandList) {
                    $(".item-list .item-data").remove();
                    for (var i = 0; i < BrandList.length; i++) {
                        var code = BrandList[i].Code;
                        var name = BrandList[i].Name;

                        $(".item-list").append('<li class="item item-data"><div class="item-row"><div class="item-col item-col-sales"><div class="item-heading">Sr. No.</div><div>' + (i + 1) + '</div></div> <div class="item-col item-col-sales"><div class="item-heading">Code</div><div>' + code + '</div></div><div class="item-col item-col-title no-overflow"> <div><a href="" class=""><h4 class="item-title no-wrap">' + name + ' </h4></a></div></div><div class="item-col item-col-date"><div><i data-code="' + code + '" data-name="' + name + '"  onclick="edit(this)"  class="fas fa-edit mr-3"></i> <i data-code="' + code + '"  onclick="deleteItem(this)"  class="fas fa-trash mr-3"></i> </div></div></div></li>');


                    }

                },
                fail: function (jqXhr, exception) {

                }
            });



        }
        function empty1() {
            $(".BrandName").val("");
            $(".BrandCode").val("");

        }
        function edit(element) {
            empty1();
            var code = element.dataset.code;
            var name = element.dataset.name;

            if (code != 1) {
                $(".BrandName").val(name);
                $(".BrandCode").val(code);
            }
            scrollTo(".BrandName");
            updateInputStyle();
        }
        function deleteItem(element) {
            empty1();
            var code = element.dataset.code;

            if (code != 1) {
                swal({
                    title: 'Are you sure to delete?',
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Yes Delete It',
                    showLoaderOnConfirm: true,
                    preConfirm: (text) => {
                        return new Promise((resolve) => {
                            deleteAttribute(code);
                            resolve();
                        })
                    },
                    allowOutsideClick: () => !swal.isLoading()
                })
            }
        }
        function deleteAttribute(code) {
            
            var Brand = {
                Code: code
            }
            $.ajax({
                url: "/Registration/ExpenceHeadManagment.aspx/DeleteHead",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ Brand: Brand }),
                success: function (BaseModel) {
                    debugger
                    if (BaseModel.d.Success) {
                        GetOnClickLevel5List();
                        $(".BrandName").val("");
                        $(".BrandCode").val("");
                        smallSwal("", BaseModel.d.Message, "success");
                        updateInputStyle();
                    }
                    else {
                        smallSwal("Failed", BaseModel.d.Message, "error");
                    }
                    $(".BrandName").val("");
                    $(".BrandCode").val("");
                }
            });

        }

        function save() {
            var brandName = $(".BrandName").val();
            var GLCodeLvl4 = $("#lstLel4List").val();
            var GLCodeOld = $(".BrandCode").val();
            var Brand = {
                Code: GLCodeLvl4,
                Name: brandName,
                Code2: GLCodeOld
            }
            debugger
            $.ajax({
                url: "/Registration/ExpenceHeadManagment.aspx/Save",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ Brand: Brand }),
                success: function (BaseModel) {
                    debugger
                    if (BaseModel.d.Success) {
                        GetOnClickLevel5List();
                        $(".BrandName").val("");
                        $(".BrandCode").val("");
                        smallSwal("", BaseModel.d.Message, "success");
                    }
                    else {
                        smallSwal("Error", BaseModel.d.Message, "error");
                    }
                }
            });
        }

      
        function GetLevel4List() {
            $.ajax({
                url: '/WebPOSService.asmx/Level4_List',
                type: "Post",
                success: function (BrandList) {
                    for (var i = 0; i < BrandList.length; i++) {
                        var code = BrandList[i].Code;
                        var name = BrandList[i].Name;
                        $(' <option value="' + code + '">' + name + '</option>').appendTo("#lstLel4List");
                    }
                },
                fail: function (jqXhr, exception) {
                }
            });
        }

    </script>
</asp:Content>
