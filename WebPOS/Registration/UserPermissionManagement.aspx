﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="UserPermissionManagement.aspx.cs" Inherits="WebPOS.Registration.UserPermissionManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container-fluid mb-3">
        <div class="mt-4">
            <section class="form">
                <h2 class="form-header ">User Permission Managment</h2>
                <div class="row">
                    <div class="col-12 col-sm-4">
                        <div class="input-group  mb-4">
                            <label class="w-100">Select Station</label>
                            <select class="form-control" id="DDLStation" name="txtuser">
                            </select>
                        </div>
                    </div>
                </div>
                <div class="card sameheight-item items" data-exclude="xs,sm,lg">

                    <div class="card-header bordered">
                        <div class="header-block">
                            <h3 class="title">Select User </h3>
                        </div>
                    </div>
                    <%@ Register Src="~/Registration/_UsersList.ascx" TagName="_UsersList" TagPrefix="uc" %>
                    <uc:_userslist id="_UsersList1" runat="server" />

                </div>

                <hr />

                <div class="row">

                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="save()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                        </span>
                    </div>
                </div>

            </section>
        </div>

    </div>
    <div class="container-fluid">

        <div class="card sameheight-item items" data-exclude="xs,sm,lg">
            <div class="card-header bordered">
                <div class="header-block">
                    <h3 class="title">List of Pages</h3>
                </div>
            </div>
            <ul class="item-list pages-list striped ">
                <li class="item item-list-header">
                    <div class="item-row">
                    </div>
                </li>

            </ul>
        </div>

        <hr />
        <div class="card sameheight-item items" data-exclude="xs,sm,lg">
            <div class="card-header bordered">
                <div class="header-block">
                    <h3 class="title">Other Permissions</h3>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <label class="d-flex">
                            <input name="AllowedOtherStationsSelect" type="checkbox" class="checkbox otherPermissions" /><span data-toggle="tooltip" title="This will allow selected salesman to select station against item in transaction pages."></span>
                            <i>Allow to Select Other Station Against Item</i>
                        </label>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <label class="d-flex">
                            <input name="UserAllowedToSetSellingPrice" type="checkbox" class="checkbox otherPermissions" /><span data-toggle="tooltip" title="This will allow selected salesman to set Sale Rate/Selling price."></span>
                            <i>Allow to Set Selling Price</i>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <hr />
        <%@ Register Src="~/Registration/_OtherManagment.ascx" TagName="_OtherManagment" TagPrefix="uc" %>
        <uc:_othermanagment id="_OtherManagment1" runat="server" />
    </div>

</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    <script src="/Script/Dropdown.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="/Script/UserPermissionManagement.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
</asp:Content>
