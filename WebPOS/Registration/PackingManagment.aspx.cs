﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.ModelBinding;
using System.Web.Services;
using WebPOS.Model;
using System.Web.Script.Services;

namespace WebPOS.Registration
{
    public partial class PackingManagment : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(Brand Brand)
        {
            var CompID = "01";
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                var id = Brand.Code;
                var name = Brand.Name;

                Module1 objModule1 = new Module1();
                var message = "";
                if (Convert.ToDecimal(id) > 0)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Update Packing set Name='" + name + "' where id=" + id, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                    message = "Packing UpDated";

                }
                else
                {
                    SqlCommand cmdDelete = new SqlCommand("Insert into Packing(ID,Name,CompID) Values(" + objModule1.MaxPackingCode() + ",'" + name + "','" + CompID + "')", con, tran);
                    cmdDelete.ExecuteNonQuery();
                    message = "New Packing Registered susscesfully.";
                }



                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel DeletePacking(Brand Brand)
        {
            var CompID = "01";
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                ModItem objModItem = new ModItem();
                var code = Brand.Code;

                Module1 objModule1 = new Module1();
                var AlreadyAsignedToItem = objModItem.PackingAssignedToItem(code);
                var message = "";
                if (!AlreadyAsignedToItem)
                {
                    SqlCommand cmdDelete1 = new SqlCommand("delete from Packing where id=" + code + " and CompID='" + CompID + "'", con, tran);
                    cmdDelete1.ExecuteNonQuery();
                    message = "Packing Deleted";
                }
                else
                {

                    return new BaseModel() { Success = false, Message = "Packing can not be deleted, because it is assigned to item." };

                }



                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };


            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
            finally
            {
                con.Close();
            }
        }




    }
}
