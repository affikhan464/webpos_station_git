﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="UpdateSellingPrice.aspx.cs" Inherits="WebPOS.UpdateSellingPrice" %>

<%-- <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">


    <link href="../css/allitems.css?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>" rel="stylesheet"  />



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container-fluid">
        <div class="mt-4">

            <section class="form">
                <h2 class="form-header fa-money-bill-alt">Set/Update Selling Price</h2>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <input id="txtItemName"  data-id="txtItemName" data-type="Item" data-function="GetRecords" class="autocomplete SearchBox ItemDescription empty1 input__field input__field--hoshi" autocomplete="off" type="text" />

                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Item Name</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                             <input id="txtItemCode" class="ItemCode input__field input__field--hoshi empty1" disabled="disabled" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Item Code</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                             <input id="txtSellingPrice1" class="input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Selling Price 1</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                             <input id="txtMinLimit" class="input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Min Limit</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                             <input id="txtMaxLimit" class="input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Max Limit</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                             <input id="txtSellingPrice2" class="input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Selling Price 2</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                             <input id="txtDealRs1" class="input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Deal Rs 1</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                             <input id="txtSellingPrice3" class="input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Selling Price 3</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                             <input id="txtDealRs2" class="input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Deal Rs 2</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                             <input id="txtSellingPrice4" class="input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Selling Price 4</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                             <input id="txtDealRs3" class="input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Deal Rs 3</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                             <input id="txtSellingPrice5" class="input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Selling Price 5</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                             <input id="txtAvgCost" class="input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Average Cost</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                             <input id="txtFixedCost" class="input__field input__field--hoshi empty1" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Fixed Cost</span>
                            </label>
                        </span>
                    </div>
                </div>

                <hr />

                <div class="row  justify-content-end">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="save()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                        </span>
                    </div>
                </div>

            </section>
        </div>
    </div>

   

    </asp:Content>

    <asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">

    <script src="../Script/Autocomplete.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>

    <script type="text/javascript">


        function GetData() {
            $(".empty1").val = "";
            var ItemCode = document.getElementById("txtItemCode").value;

            $.ajax({
                url: '/WebPOSService.asmx/ItemData_AgainstItemCode',
                type: "Post",

                data: { "ItemCode": ItemCode },


                success: function (ItemDate007) {


                    var Description = ItemDate007.Description;
                    var SellingCost1 = ItemDate007.SellingCost1;
                    var SellingCost2 = ItemDate007.SellingCost2;
                    var SellingCost3 = ItemDate007.SellingCost3;
                    var SellingCost4 = ItemDate007.SellingCost4;
                    var SellingCost5 = ItemDate007.SellingCost5;
                    var DealRs1 = ItemDate007.DealRs1;
                    var DealRs2 = ItemDate007.DealRs2;
                    var DealRs3 = ItemDate007.DealRs3;
                    var Ave_Cost = ItemDate007.Ave_Cost;
                    var MinLimit = ItemDate007.MinLimit;
                    var MaxLimit = ItemDate007.MaxLimit;
                    var DealRs1 = ItemDate007.DealRs1;
                    var DealRs2 = ItemDate007.DealRs2;
                    var DealRs3 = ItemDate007.DealRs3;
                    var FixedCost = ItemDate007.FixedCost;


                    document.getElementById("txtSellingPrice1").value = SellingCost1;
                    document.getElementById("txtSellingPrice2").value = SellingCost2;
                    document.getElementById("txtSellingPrice3").value = SellingCost3;
                    document.getElementById("txtSellingPrice4").value = SellingCost4;
                    document.getElementById("txtSellingPrice5").value = SellingCost5;
                    document.getElementById("txtMinLimit").value = MinLimit;
                    document.getElementById("txtMaxLimit").value = MaxLimit;
                    document.getElementById("txtDealRs1").value = DealRs1;
                    document.getElementById("txtDealRs2").value = DealRs2;
                    document.getElementById("txtDealRs3").value = DealRs3;
                    document.getElementById("txtAvgCost").value = Ave_Cost;
                    document.getElementById("txtFixedCost").value = FixedCost;
                    updateInputStyle();

                },
                fail: function (jqXhr, exception) {

                }
            });


        }
        function Empty1() {
            $(".empty1").val("");
        }
        function save() {
            var ItemCode = document.getElementById("txtItemCode").value;
            var SellingCost1 = document.getElementById("txtSellingPrice1").value;
            var SellingCost2 = document.getElementById("txtSellingPrice2").value;
            var SellingCost3 = document.getElementById("txtSellingPrice3").value;
            var SellingCost4 = document.getElementById("txtSellingPrice4").value;
            var SellingCost5 = document.getElementById("txtSellingPrice5").value;
            var Ave_Cost = document.getElementById("txtAvgCost").value;
            var fixedCost = document.getElementById("txtFixedCost").value;
            var DealRs1 = document.getElementById("txtDealRs1").value;
            var DealRs2 = document.getElementById("txtDealRs2").value;
            var DealRs3 = document.getElementById("txtDealRs3").value;
            var MinLimit = document.getElementById("txtMinLimit").value;
            var MaxLimit = document.getElementById("txtMaxLimit").value;

            SellingCost1 = isNaN(SellingCost1) ? 0 : SellingCost1;
            SellingCost2 = isNaN(SellingCost2) ? 0 : SellingCost2;
            SellingCost3 = isNaN(SellingCost3) ? 0 : SellingCost3;
            SellingCost4 = isNaN(SellingCost4) ? 0 : SellingCost4;
            SellingCost5 = isNaN(SellingCost5) ? 0 : SellingCost5;
            Ave_Cost = isNaN(Ave_Cost) ? 0 : Ave_Cost;
            fixedCost = isNaN(fixedCost) ? 0 : fixedCost;
            DealRs1 = isNaN(DealRs1) ? 0 : DealRs1;
            DealRs2 = isNaN(DealRs2) ? 0 : DealRs2;
            DealRs3 = isNaN(DealRs3) ? 0 : DealRs3;
            MinLimit = isNaN(MinLimit) ? 0 : MinLimit;
            MaxLimit = isNaN(MaxLimit) ? 0 : MaxLimit;

            var ItemModel = {
                Code: ItemCode,
                SellingCost1: SellingCost1,
                SellingCost2: SellingCost2,
                SellingCost3: SellingCost3,
                SellingCost4: SellingCost4,
                SellingCost5: SellingCost5,
                Ave_Cost: Ave_Cost,
                FixedCost: fixedCost,
                DealRs1: DealRs1,
                DealRs2: DealRs2,
                DealRs3: DealRs3,
                MinLimit: MinLimit,
                MaxLimit: MaxLimit

            }


            debugger
            $.ajax({
                url: "/Registration/UpdateSellingPrice.aspx/Save",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ ItemModel: ItemModel }),
                success: function (BaseModel) {
                    debugger
                    if (BaseModel.d.Success) {
                        Empty1();
                        smallSwal("Updated", BaseModel.d.Message, "success");
                        updateInputStyle();
                    }
                    else {
                        smallSwal("Failed", BaseModel.d.Message, "error");
                    }


                }

            });

        }



    </script>


</asp:Content>
