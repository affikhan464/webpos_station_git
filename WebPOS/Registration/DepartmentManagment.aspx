﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="DepartmentManagment.aspx.cs" Inherits="WebPOS.Registration.DepartmentManagment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="container-fluid mb-3">
        <div class="mt-4">

            <section class="form">
                <h2 class="form-header fa-money-bill-alt">Department Management</h2>
                <div class="row">

                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <input class="BrandName empty1 input__field input__field--hoshi" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Department Name</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input id="txtSellingPrice1" class="BrandCode input__field input__field--hoshi empty1" disabled="disabled" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Department Code</span>
                            </label>
                        </span>
                    </div>

                </div>

                <hr />

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100 fa-sync" id="btnReset" onclick="reset()"><span>Reset </span></a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="save()" class=" btn btn-3 btn-bm btn-3e fa-save w-100"><span>Save</span></a>
                        </span>
                    </div>
                </div>

            </section>
        </div>

    </div>
    <div class="container-fluid">
        <div class="card sameheight-item items" data-exclude="xs,sm,lg">
            <div class="card-header bordered">
                <div class="header-block">
                    <h3 class="title">List of Departments </h3>
                </div>

            </div>
            <ul class="item-list striped">
                <li class="item item-list-header">
                    <div class="item-row">
                        <div class="item-col item-col-header item-col-sales">
                            <div>
                                <span>Sr. No.</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header item-col-sales">
                            <div>
                                <span>Code</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header item-col-title">
                            <div>
                                <span>Name</span>
                            </div>
                        </div>

                        <div class="item-col item-col-header item-col-date">
                        </div>
                    </div>
                </li>

            </ul>
        </div>
        <hr />
        <%@ Register Src="~/Registration/_OtherManagment.ascx" TagName="_OtherManagment" TagPrefix="uc" %>
        <uc:_OtherManagment ID="_OtherManagment1" runat="server" />
    </div>

</asp:Content>

<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">


    <script type="text/javascript">
        $(document).ready(function () {
            GetAllList();

        });
        function reset() {
            $(".empty1").val("");
        }
        function GetAllList() {

            $.ajax({
                url: '/WebPOSService.asmx/Department_List',
                type: "Post",

                //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


                success: function (BrandList) {
                    $(".item-list .item-data").remove();
                    for (var i = 0; i < BrandList.length; i++) {
                        var code = BrandList[i].Code;
                        var name = BrandList[i].Name;
                        var item = `<li class='item item-data'>
                                        <div class='item-row'>
                                            <div class ="item-col item-col-sales">
                                                <div class ="item-heading">Sr.No.</div>
                                                <div>${(i + 1)}</div>
                                            </div>
                                            <div class ="item-col item-col-sales">
                                                 <div class ="item-heading">Code</div>
                                                 <div>${code}</div>
                                            </div>
                                            <div class ="item-col item-col-title no-overflow">
                                                <div>
                                                    <a href="" class =""><h4 class ="item-title no-wrap">${name} </h4></a>
                                                </div>
                                            </div>
                                            <div class ="item-col item-col-date">
                                                <div>
                                                    <i data-code="${code}" data-name="${name}"  onclick="edit(this)"  class ="fas fa-edit mr-3"></i>
                                                    <i data-code="${code}"  onclick="deleteItem(this)"  class ="fas fa-trash mr-3"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </li>`;

                        $(".item-list").append(item);


                    }

                },
                fail: function (jqXhr, exception) {


                }
            });



        }


        function empty1() {
            $(".BrandName").val("");
            $(".BrandCode").val("");

        }
        function edit(element) {
            empty1();
            var code = element.dataset.code;
            var name = element.dataset.name;

            if (code != 1) {
                $(".BrandName").val(name);
                $(".BrandCode").val(code);
            }
            scrollTo(".BrandName");
            updateInputStyle();
        }
        function deleteItem(element) {
            empty1();
            var code = element.dataset.code;

            if (code != 1) {
                swal({
                    title: 'Are you sure to delete?',
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Yes Delete It',
                    showLoaderOnConfirm: true,
                    preConfirm: (text) => {
                        return new Promise((resolve) => {
                            deleteAttribute(code);
                            resolve();
                        })
                    },
                    allowOutsideClick: () => !swal.isLoading()
                })
            }
        }
        function deleteAttribute(code) {

            var Brand = {
                Code: code
            }
            $.ajax({
                url: "/Registration/DepartmentManagment.aspx/Delete",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ Brand: Brand }),
                success: function (BaseModel) {
                    debugger
                    if (BaseModel.d.Success) {
                        GetAllList();
                        smallSwal("Deleted", BaseModel.d.Message, "success");
                        updateInputStyle();
                    }
                    else {
                        smallSwal("Failed", BaseModel.d.Message, "error");
                    }
                    $(".BrandName").val("");
                    $(".BrandCode").val("");
                }
            });

        }

        function save() {
            var brandCode = $(".BrandCode").val();
            var brandName = $(".BrandName").val();
            if (brandCode == "") { brandCode = 0; }
            var Brand = {
                Code: brandCode,
                Name: brandName,
            }
            debugger
            $.ajax({
                url: "/Registration/DepartmentManagment.aspx/Save",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ Brand: Brand }),
                success: function (BaseModel) {
                    debugger
                    if (BaseModel.d.Success) {
                        GetAllList();
                        $(".BrandName").val("");
                        $(".BrandCode").val("");
                        updateInputStyle();
                        smallSwal("", BaseModel.d.Message, "success");
                    }
                    else {
                        smallSwal("Error", BaseModel.d.Message, "error");
                    }
                }
            });
        }

    </script>
</asp:Content>
