﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="_AddUser.ascx.cs" Inherits="WebPOS.Registration._AddUser" %>

<div class="row UserForm">
    <input name="id" type="hidden" />

    <div class="col-12 col-sm-6">
        <span class="input input--hoshi">
            <input name="name" class="empty1 input__field input__field--hoshi" type="text" autocomplete="off" />
            <label class="input__label input__label--hoshi">
                <span class="input__label-content input__label-content--hoshi">Name </span>
            </label>
        </span>
    </div>
    <div class="col-12 col-sm-6">
        <span class="input input--hoshi">
            <input name="loginName" class="empty1 input__field input__field--hoshi" type="text" />
            <label class="input__label input__label--hoshi">
                <span class="input__label-content input__label-content--hoshi">Login Name </span>
            </label>
        </span>
    </div>

    <div class="col-12 col-sm-6">
        <span class="input input--hoshi">
            <input name="password" class="empty1 input__field input__field--hoshi" type="text" />
            <label class="input__label input__label--hoshi">
                <span class="input__label-content input__label-content--hoshi">Password </span>
            </label>
        </span>
    </div>
</div>
