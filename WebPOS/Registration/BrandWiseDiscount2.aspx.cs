﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using WebPOS.Model;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS.Registration
{
    public partial class BrandWiseDiscount2 : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(SaveBrandWiseDiscountModel2 SaveBrandWiseDiscountModel)
        {
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                var brandCode = SaveBrandWiseDiscountModel.BrandCode;
                var discount = SaveBrandWiseDiscountModel.Discount;

                var cmd = new SqlCommand("Select * From PartyCode where NorBalance=1 ", con, tran);

                DataSet objDs = new DataSet();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;

                dAdapter.Fill(objDs);

                SqlCommand cmdDelete = new SqlCommand("Delete from BrandWiseDiscount where  CompID='" + CompID + "' and BrandCode=" + brandCode + "", con, tran);
                cmdDelete.ExecuteNonQuery();
                var data = objDs.Tables[0];
                var items = new List<Product>();

                for (int i = 0; i < data.Rows.Count; i++)
                {
                    var partyCode = data.Rows[i]["Code"].ToString();

                    SqlCommand cmd3 = new SqlCommand("Insert into  BrandWiseDiscount(PartyCode, BrandCode, Disc_Rate, Disc_Rate_Plus, CompID) values('" + partyCode + "', " + brandCode + ", " + discount + ", " + 0 + ", '" + CompID + "')", con, tran);
                    cmd3.ExecuteNonQuery();
                }


                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = "Brand Rate Updated" };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }


    }
}