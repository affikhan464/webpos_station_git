﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="BankAccountManagment.aspx.cs" Inherits="WebPOS.Registration.BankAccountManagment" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     
        
    <div class="container-fluid mb-3">
        <div class="mt-4">

            <section class="form" id="form-section">
                <h2 class="form-header fa-money-bill-alt">Bank Account Managment</h2>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <input class="BrandName empty1 input__field input__field--hoshi" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Bank Name</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input class="BrandCode input__field input__field--hoshi empty1" type="text" autocomplete="off" disabled="disabled" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Bank Code</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input class="AccountNo empty1 input__field input__field--hoshi" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Account #</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input class="AccountTitle empty1 input__field input__field--hoshi" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Account Title</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input class="Address empty1 input__field input__field--hoshi" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Bank Address</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input class="PhoneNo empty1 input__field input__field--hoshi" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Phone No.</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">

                            <input class="FaxNo empty1 input__field input__field--hoshi" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Fax No.</span>
                            </label>
                        </span>
                    </div>
                   
                </div>

                <hr />

                <div class="row">
                      <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100 fa-sync"id="btnReset" onclick="reset()"> Reset </a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a id="btnSave"  onclick="save()"  class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                        </span>
                    </div>
                </div>

            </section>
        </div>

    </div>
    <div class="container-fluid">
        <div class="card sameheight-item items" data-exclude="xs,sm,lg">
            <div class="card-header bordered">
                <div class="header-block">
                    <h3 class="title">List of Bank Account </h3>
                </div>
               
            </div>
            <ul class="item-list striped">
                <li class="item item-list-header">
                    <div class="item-row">
                        <div class="item-col item-col-header flex-1">
                            <div>
                                <span>Sr. No.</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header flex-4">
                            <div>
                                <span>Code</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header flex-5">
                            <div>
                                <span>Bank Name</span>
                            </div>
                        </div>
                         <div class="item-col item-col-header flex-4">
                            <div>
                                <span>Account No.</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header flex-3">
                            <div>
                                <span>Account Title</span>
                            </div>
                        </div>

                        <div class="item-col item-col-header flex-3">
                            <div>
                                <span>Address</span>
                            </div>
                        </div>

                        <div class="item-col item-col-header flex-3">
                            <div>
                                <span>Phone No.</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header flex-3">
                            <div>
                                <span>Fax No.</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header item-col-date flex-3">
                        </div>
                    </div>
                </li>

            </ul>
        </div>
    </div>
   
    </asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">

    <script type="text/javascript">
        $(document).ready(function () {
           
          
            GetLevel5List("01010102");

        });
        function reset() {
            $(".empty1").val("");
            updateInputStyle();
        }
        function GetOnClickLevel5List() {
            var GLCode = "01010102";
            GetLevel5List(GLCode);
        }

        function GetLevel5List(GLCode) {

            $.ajax({
                url: '/WebPOSService.asmx/Level5_ListBank',
                type: "Post",

                data: { "GLCode": GLCode },


                success: function (listBank) {
                    $(".item-list .item-data").remove();
                    for (var i = 0; i < listBank.length; i++) {
                        var code = listBank[i].BankCode;
                        var name = listBank[i].BankName;
                        var AccountNo = listBank[i].AccountNo;
                        var Title = listBank[i].Title;
                        var Address = listBank[i].Address;
                        var Phone = listBank[i].Phone;
                        var Fax = listBank[i].Fax;

                        $(".item-list").append('<li class="item item-data"><div class="item-row"><div class="item-col flex-1"><div class="item-heading">Sr. No.</div><div>' + (i + 1) + '</div></div>   <div class="item-col flex-4"><div class="item-heading">Code</div><div>' + code + '</div></div>    <div class="item-col flex-5 no-overflow"> <div><a class=""><h4 class="item-title no-wrap">' + name + ' </h4></a></div></div>    <div class="item-col flex-4"><div class="item-heading">Account No.</div><div>' + AccountNo + '</div></div>         <div class="item-col flex-3"><div class="item-heading">Title</div><div>' + Title + '</div></div>          <div class="item-col flex-3"><div class="item-heading">Address</div><div>' + Address + '</div></div>      <div class="item-col flex-3"><div class="item-heading">Phone</div><div>' + Phone + '</div></div>      <div class="item-col flex-3"><div class="item-heading">Fax</div><div>' + Fax + '</div></div>     <div class="item-col item-col-date flex-3"><div><i  data-brandcode="' + code + '" data-brandname="' + name + '" data-accountno="' + AccountNo + '" data-title="' + Title + '" data-address="' + Address + '" data-phone="' + Phone + '" data-fax="' + Fax + '"   onclick="edit(this)"  class="fas fa-edit mr-3"></i> <i data-code="' + code + '"  onclick="deleteItem(this)"  class="fas fa-trash mr-3"></i> </div></div></div></li>');

                    }

                },
                fail: function (jqXhr, exception) {

                }
            });



        }
        function empty1() {
            $(".BrandName").val("");
            $(".BrandCode").val("");

        }
        function edit(element) {
            empty1();
            var code = element.dataset.brandcode;
            var name = element.dataset.brandname;
            var AccountNo = element.dataset.accountno;
            var Title = element.dataset.title;
            var Address = element.dataset.address;
            var Phone = element.dataset.phone;
            var Fax = element.dataset.fax;
            
                $(".BrandName").val(name);
                $(".BrandCode").val(code);
                $(".AccountNo").val(AccountNo);
                $(".AccountTitle").val(Title);
                $(".Address").val(Address);
                $(".PhoneNo").val(Phone);
                $(".FaxNo").val(Fax);
                scrollTo("#form-section");
                updateInputStyle();
            
        }

        

        
        function save() {
            
            var BankCode = $(".BrandCode").val();
            var BankName = $(".BrandName").val();
            var AccountNo = $(".AccountNo").val();
            var Title = $(".AccountTitle                            ").val();
            var Address = $(".Address").val();
            var Phone = $(".PhoneNo").val();
            var Fax = $(".FaxNo").val();
            var BankModel = {
                    BankCode: BankCode,
                    BankName: BankName,
                    AccountNo: AccountNo,
                    Title: Title,
                    Address: Address,
                    Phone: Phone,
                    Fax: Fax
                }
            debugger
            $.ajax({
                url: "/Registration/BankAccountManagment.aspx/Save",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ BankModel: BankModel }),
                success: function (BaseModel) {
                    debugger
                    if (BaseModel.d.Success) {
                        GetOnClickLevel5List();
                        $(".empty1").val("");
                        smallSwal("", BaseModel.d.Message, "success");
                        updateInputStyle();
                    }
                    else {
                        smallSwal("Failed", BaseModel.d.Message, "error");
                    }


                }

            });

            }

        function deleteItem(element) {
            empty1();
            var code = element.dataset.code;

            if (code != 1) {
                swal({
                    title: 'Are you sure to delete?',
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Yes Delete It',
                    showLoaderOnConfirm: true,
                    preConfirm: (text) => {
                        return new Promise((resolve) => {
                            deleteAttribute(code);
                            resolve();
                        })
                    },
                    allowOutsideClick: () => !swal.isLoading()
                })
            }
        }
        function deleteAttribute(code) {

            var Brand = {
                Code: code
            }
            debugger
            $.ajax({
                url: "/Registration/BankAccountManagment.aspx/DeleteHead",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ Brand: Brand }),
                success: function (BaseModel) {
                    debugger
                    if (BaseModel.d.Success) {
                        GetOnClickLevel5List();
                        $(".empty1").val("");
                        updateInputStyle();
                        smallSwal("", BaseModel.d.Message, "success");
                    }
                    else {
                        $(".empty1").val("");
                        smallSwal("Failed", BaseModel.d.Message, "error");
                        updateInputStyle();
                    }
                    $(".BrandName").val("");
                    $(".BrandCode").val("");
                }
            });
        }
       

    </script>

 
</asp:Content>
