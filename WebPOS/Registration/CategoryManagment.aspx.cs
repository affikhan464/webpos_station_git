﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using WebPOS.Model;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS.Registration
{
    public partial class CategoryManagment : System.Web.UI.Page
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        static SqlTransaction tran;
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel Save(Brand Brand)
        {
            var CompID = "01";
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                var CategoryID = Brand.Code;
                var CategoryName = Brand.Name;
                var Blocked = Brand.Blocked;
                var Verify = Brand.VerifyIME;
                Module1 objModule1 = new Module1();
                var message = "";
                if (Convert.ToDecimal(CategoryID) > 0)
                {
                    SqlCommand cmdUpdate = new SqlCommand("Update Category set Name='" + CategoryName + "' where id=" + CategoryID, con, tran);
                    cmdUpdate.ExecuteNonQuery();
                    message = "Category UpDated";

                }
                else
                {
                    var MaxCategoryID = objModule1.MaxCategoryCode();
                    SqlCommand cmdDelete = new SqlCommand("Insert into Category(ID,Name,CompID) Values(" + MaxCategoryID + ",'" + CategoryName + "','" + CompID + "')", con, tran);
                    cmdDelete.ExecuteNonQuery();
                    message = "New Category Registered susscesfully.";
                }



                tran.Commit();
                con.Close();
                return new BaseModel() { Success = true, Message = message };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel DeleteCategory(Brand Brand)
        {
            var CompID = "01";
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                tran = con.BeginTransaction();

                ModItem objModItem = new ModItem();
                var CategoryCode = Brand.Code;
                var BrandName = Brand.Name;
                var Blocked = Brand.Blocked;
                var Verify = Brand.VerifyIME;
                Module1 objModule1 = new Module1();
                var AlreadyAsignedToItem = objModItem.CategoryAssignedToItem(CategoryCode);
                var message = "";
                if (!AlreadyAsignedToItem)
                {
                    SqlCommand cmdDelete1 = new SqlCommand("delete from Category where id=" + CategoryCode, con, tran);
                    cmdDelete1.ExecuteNonQuery();
                    //SqlCommand cmdDelete2 = new SqlCommand("delete  from VoucherNoVerification where V_No='" + BrandCode + "' and V_Type='MaxBrandCode'", con, tran);
                    //cmdDelete2.ExecuteNonQuery();
                    message = "Category Deleted";
                }
                else
                {
                    message = "Category can not be deleted assigned";
                    return new BaseModel() { Success = false, Message = message };

                }



                tran.Commit();
                con.Close();

                return new BaseModel() { Success = true, Message = message };
            }
            catch (Exception ex)
            {
                tran.Rollback();
                con.Close();
                return new BaseModel() { Success = false, Message = ex.Message };
            }
            finally
            {
                con.Close();
            }
        }


    }

}