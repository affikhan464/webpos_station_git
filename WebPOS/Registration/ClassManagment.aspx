﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="ClassManagment.aspx.cs" Inherits="WebPOS.Registration.ClassManagment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid mb-3">
        <div class="mt-4">

            <section class="form">
                <h2 class="form-header">Class Managment</h2>
                <div class="row">


                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-6">
                        <span class="input input--hoshi">
                            <input id="Name" class="Name empty1 input__field input__field--hoshi" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Name</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-6">
                        <span class="input input--hoshi">

                            <input class="Code input__field input__field--hoshi empty1" disabled="disabled" type="text" autocomplete="off" />
                            <label class="input__label input__label--hoshi">
                                <span class="input__label-content input__label-content--hoshi">Code</span>
                            </label>
                        </span>
                    </div>

                </div>

                <hr />

                <div class="row">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a class="btn btn-3 btn-bm btn-3e w-100 fa-sync" id="btnReset" onclick="reset()">Reset </a>
                        </span>
                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <span class="input input--hoshi">
                            <a id="btnSave" onclick="save()" class=" btn btn-3 btn-bm btn-3e fa-save w-100">Save</a>
                        </span>
                    </div>
                </div>

            </section>
        </div>

    </div>
    <div class="container-fluid">
        <div class="card sameheight-item items" data-exclude="xs,sm,lg">
            <div class="card-header bordered">
                <div class="header-block">
                    <h3 class="title">List of Classes </h3>
                </div>

            </div>
            <ul class="item-list striped">
                <li class="item item-list-header">
                    <div class="item-row">
                        <div class="item-col item-col-header flex-1">
                            <div>
                                <span>Sr. No.</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header  flex-2">
                            <div>
                                <span>Code</span>
                            </div>
                        </div>
                        <div class="item-col item-col-header  flex-4">
                            <div>
                                <span>Name</span>
                            </div>
                        </div>

                        <div class="item-col item-col-header  flex-3">
                        </div>
                    </div>
                </li>

            </ul>
        </div>
        <hr />
        <%@ Register Src="~/Registration/_OtherManagment.ascx" TagName="_OtherManagment" TagPrefix="uc" %>
        <uc:_OtherManagment ID="_OtherManagment1" runat="server" />
    </div>

</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    <script type="text/javascript">
        $(document).ready(function () {

            GetAllClassList();

        });

        function GetAllClassList() {

            $.ajax({
                url: '/WebPOSService.asmx/Class_List',
                type: "Post",

                //data: { "ItemCode": ItemCode, "PartyCode": PartyCode },


                success: function (BrandList) {
                    $(".item-list .item-data").remove();
                    for (var i = 0; i < BrandList.length; i++) {
                        var code = BrandList[i].Code;
                        var name = BrandList[i].Name;
                        var deleteAttr = '';
                        if (code != 1) {
                            deleteAttr = ' <i data-code="' + code + '"  onclick="deleteItem(this)"  class="fas fa-trash mr-3"></i> ';
                        }

                        $(".item-list").append('<li class="item item-data"><div class="item-row"><div class="item-col flex-1"><div class="item-heading">Sr. No.</div><div>' + (i + 1) + '</div></div>   <div class="item-col flex-2"><div class="item-heading">Code</div><div>' + code + '</div></div>    <div class="item-col flex-4 no-overflow"> <div><a class=""><h4 class="item-title no-wrap">' + name + ' </h4></a></div></div>        <div class="item-col item-col-date flex-3"><div><i  data-brandcode="' + code + '" data-brandname="' + name + '" onclick="edit(this)" class="fas fa-edit mr-3"></i> ' + deleteAttr + ' </div></div></div></li>');

                    }

                },
                fail: function (jqXhr, exception) {

                }
            });



        }
        function empty1() {
            $(".Name").val("");
            $(".Code").val("");

        }
        function edit(element) {
            empty1();

            var code = element.dataset.brandcode;
            var name = element.dataset.brandname;

            $(".Name").val(name);
            $(".Code").val(code);

            updateInputStyle();
        }


        function save() {
            var brandName = $("#Name").val();
            var brandCode = $(".Code").val();

            if (brandCode == "") { brandCode = 0; }
            var Brand = {
                Code: brandCode,
                Name: brandName,
            }


            debugger
            $.ajax({
                url: "/Registration/ClassManagment.aspx/Save",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ Brand: Brand }),
                success: function (BaseModel) {
                    debugger
                    if (BaseModel.d.Success) {
                        GetAllClassList();
                        $(".Name").val("");
                        $(".Code").val("");

                        smallSwal(BaseModel.d.Message, '', "success");
                    }
                    else {
                        smallSwal("Failed", BaseModel.d.Message, "error");
                    }


                }

            });

        }

        function deleteItem(element) {
            var code = element.dataset.code;
            var Brand = {
                Code: code

            }
            swal({
                title: 'Are you sure to delete?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Yes Delete It',
                showLoaderOnConfirm: true,
                preConfirm: (text) => {
                    return new Promise((resolve) => {



                        $.ajax({
                            url: "/Registration/ClassManagment.aspx/DeleteClass",
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            data: JSON.stringify({ Brand: Brand }),
                            success: function (BaseModel) {
                                debugger
                                if (BaseModel.d.Success) {
                                    GetAllClassList();
                                    smallSwal(BaseModel.d.Message, '', "success");
                                }
                                else {
                                    smallSwal("Failed", BaseModel.d.Message, "error");
                                }

                            }

                        });



                        resolve();
                    })
                },
                allowOutsideClick: () => !swal.isLoading()
            })


        }



    </script>

</asp:Content>
