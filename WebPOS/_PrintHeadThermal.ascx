﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="_PrintHeadThermal.ascx.cs" Inherits="WebPOS._PrintHeadThermal" %>
<div class="row justify-content-center">
    <div class="col-12 text-center">

        <h1 class="CompanyName"><%= ConfigurationManager.AppSettings["CompanyName"] %></h1>
    </div>
    <div class="col-6 text-center">

        <address class="m-0">

            <p class="StationAddress"><span><%= Session["StationAddress"] %></span></p>

            <p class="StationNumber">Phone #: <%= Session["StationPhoneNumber"] %></p>
        </address>
    </div>
</div>
