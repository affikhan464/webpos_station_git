var Countdown = {
    init: function(date, id) {
        let countdownDate = new Date(date).getTime();

        let oldHours;
        let oldMinutes;
        let oldSeconds;

        countdown = setInterval(function(){
            var diff = Math.abs(countdownDate - new Date()) / 1000;

            months = Math.floor(diff / 2.628e+6);
            diff -= months * 2.628e+6;

            days = Math.floor(diff / 86400);
            diff -= days * 86400;
             hours = Math.floor(diff / 3600) % 24;
            diff -= hours * 3600;

             minutes = Math.floor(diff / 60) % 60;
            diff -= minutes * 60;

            seconds = Math.floor(diff % 60);

            var monthsStr = months;
            if (months < 10) {
                monthsStr = "0" + months;
            }
            var daysStr = days;
            if (days < 10) {
                daysStr = "0" + days;
            }

            var hoursStr = hours;
            if (hours < 10) {
                hoursStr = "0" + hours;
            }

            var minutesStr = minutes;
            if (minutes < 10) {
                minutesStr = "0" + minutes;
            }

            var secondsStr = seconds;
            if (seconds < 10) {
                secondsStr = "0" + seconds;
            }

            if (days < 0 && hours < 0 && minutes < 0 && seconds < 0 && months<0) {
                daysStr = "00";
                hoursStr = "00";
                minutesStr = "00";
                secondsStr = "00";
                monthsStr = "00";

                console.log("close");
                if (typeof countdown !== "undefined") {
                    clearInterval(countdown);
                }
            }
            Countdown.setCurrentCountdown(secondsStr, minutesStr, hoursStr, daysStr, monthsStr, id);


        }, 1000);
    },
    setCurrentCountdown: function (seconds, minutes, hours, days, months, id) {
        var mtext = months > 1 ? "months" : "month";
        var dtext = months > 1 ? "days" : "day";
        if (months != 0) {         
            $(id).text(`${months}-${mtext} ${days}-${dtext}  ${hours}:${minutes}:${seconds}`);
        } else if (days!=0) {
            $(id).text(` ${days}-${dtext} ${hours}:${minutes}:${seconds}`);
        } else {
            $(id).text(`${hours}:${minutes}:${seconds}`);
        }
    }
  };