﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
namespace WebPOS
{
    public class Module2
    {
        static string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        public int NormalBalanceParties(string PartyCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select NorBalance from PartyCode where CompID='" + CompID + "' and Code='" + PartyCode + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int NorBal = 1;



            if (dt.Rows.Count > 0)
            {
                NorBal = Convert.ToInt32(dt.Rows[0]["NorBalance"]);

            }


            return NorBal;
        }
        public int NormalBalanceGL(string PartyCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select NorBalance from GLCode where CompID='" + CompID + "' and Code='" + PartyCode + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int NorBal = 1;



            if (dt.Rows.Count > 0)
            {
                NorBal = Convert.ToInt32(dt.Rows[0]["NorBalance"]);

            }


            return NorBal;
        }
        public int NormalBalanceGLWithTransaction(string PartyCode, SqlConnection connection, SqlTransaction trans)
        {
            var cmd = new SqlCommand("Select NorBalance from GLCode where CompID='" + CompID + "' and Code='" + PartyCode + "'", connection, trans);
            var adptr = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adptr.Fill(dt);
            int NorBal = 1;

            if (dt.Rows.Count > 0)
            {
                NorBal = Convert.ToInt32(dt.Rows[0]["NorBalance"]);

            }


            return NorBal;
        }




    }
}