﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS
{
    public class ModQtyOfInventory
    {
        

        Module7 objModule7 = new Module7(); 
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        public void QtyAvailableOne(string ItemCode, SqlTransaction trans, SqlConnection con)
        {
            var cmd = new SqlCommand("select isnull(sum(Opening),0) as Opening from InventoryOpeningBalance where  CompID='" + CompID + "' and ICode=" + ItemCode + " and Dat='" + objModule7.StartDate() + "'", con,trans);
            cmd.CommandTimeout = 100000000;
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            decimal OpeningBalance = 0;
            if (dt.Rows.Count > 0)
            {
                OpeningBalance = Convert.ToDecimal(dt.Rows[0]["Opening"]);
            }
            var cmd2 = new SqlCommand("select isnull(sum(received),0) as received , isnull(sum(issued),0) as issued from inventory where  CompID='" + CompID + "' and ICode=" + ItemCode + " and dat>='" + objModule7.StartDate() + "' and dat<='" + objModule7.EndDate(DateTime.Today) + "'", con, trans);
            cmd2.CommandTimeout = 100000000;
            SqlDataAdapter adapter2 = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            adapter2.Fill(dt2);
            decimal UnitsReceived = 0;
            decimal UnitsIssued = 0;

            if (dt2.Rows.Count > 0)
            {
                UnitsReceived = Convert.ToDecimal(dt2.Rows[0]["received"]);
                UnitsIssued = Convert.ToDecimal(dt2.Rows[0]["issued"]);
            }
            decimal QtyAvailable = 0;
            QtyAvailable = (OpeningBalance + UnitsReceived) - UnitsIssued;
            
            SqlCommand cmd3 = new SqlCommand("update InvCode set qty=" + QtyAvailable + " where  CompID='" + CompID + "' and Code=" + ItemCode, con,trans);
            cmd3.CommandTimeout = 100000000;
            cmd3.ExecuteNonQuery();
        }


        public void QtyAvailableOneStation(string ItemCode,string StationID, SqlTransaction trans, SqlConnection con)
        {
            var cmd = new SqlCommand("select isnull((Opening),0) as Opening from InventoryOpeningBalance where  CompID='" + CompID + "' and StationId="+StationID+" and ICode=" + ItemCode + " and Dat='" + objModule7.StartDate() + "'", con, trans);
            cmd.CommandTimeout = 100000000;
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            decimal OpeningBalance = 0;
            if (dt.Rows.Count > 0)
            {
                OpeningBalance = Convert.ToDecimal(dt.Rows[0]["Opening"]);
            }
            var cmd2 = new SqlCommand("select isnull(sum(received),0) as received , isnull(sum(issued),0) as issued from inventory where  CompID='" + CompID + "' and StationId=" + StationID + " and ICode=" + ItemCode + " and dat>='" + objModule7.StartDate() + "' and dat<='" + objModule7.EndDate(DateTime.Today) + "'", con, trans);
            cmd2.CommandTimeout = 100000000;
            SqlDataAdapter adapter2 = new SqlDataAdapter(cmd2);
            DataTable dt2 = new DataTable();
            adapter2.Fill(dt2);
            decimal UnitsReceived = 0;
            decimal UnitsIssued = 0;

            if (dt2.Rows.Count > 0)
            {
                UnitsReceived = Convert.ToDecimal(dt2.Rows[0]["received"]);
                UnitsIssued = Convert.ToDecimal(dt2.Rows[0]["issued"]);
            }
            decimal QtyAvailable = 0;
            QtyAvailable = (OpeningBalance + UnitsReceived) - UnitsIssued;

            SqlCommand cmd3 = new SqlCommand("update InventoryOpeningBalance set current_qty=" + QtyAvailable + " where  CompID='" + CompID + "' and StationId=" + StationID + " and Dat='" + objModule7.StartDate() + "' and ICode=" + ItemCode, con, trans);
            cmd3.CommandTimeout = 100000000;
            cmd3.ExecuteNonQuery();
        }

        public string TransactionExistOFItem(decimal ItemCode)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select Top 1 ICode from Inventory Where ICode=" + ItemCode, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            string  TransactionExistOFItem = "No";
            if (dt.Rows.Count > 0)
            {
                TransactionExistOFItem = "Yes";
            }
            return TransactionExistOFItem ;
        }


    }
}