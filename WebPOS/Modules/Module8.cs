﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS
{
    public class Module8
    {

        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        public string PartyNameAgainstCode(string Code)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Name from PartyCode where  CompID='" + CompID + "' and Code='" + Code + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            string Name = "";


            if (dt.Rows.Count > 0)
            {
                Name = Convert.ToString(dt.Rows[0]["Name"]);

            }


            return Name;
        }
        public void UpdateCurrentYearPartiesClosingBalance()
        {
            SqlCommand cmd = new SqlCommand(@"select Code,NorBalance from PartyCode  where compid='" + CompID + "'", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            string PartyCode = "";
            Module7 objModule7 = new Module7();
            decimal OpBal = 0;
            decimal AmoDr = 0;
            decimal AmoCr = 0;
            Module2 objModule2 = new Module2();
            decimal Closing = 0;
            int NorBalance = 1;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                PartyCode = (dt.Rows[i]["Code"]).ToString();
                NorBalance = Convert.ToInt32(dt.Rows[i]["NorBalance"]);

                ////----------------------------------------------

                SqlDataAdapter cmd1 = new SqlDataAdapter("select isnull(OpBalance,0) as OpBalance from PartyOpBalance  where compid='" + CompID + "' and Code='" + PartyCode + "' and  dat='" + objModule7.StartDate() + "'", con);
                DataTable dt1 = new DataTable();
                cmd1.Fill(dt1);

                if (dt1.Rows.Count > 0)
                {
                    OpBal = Convert.ToDecimal(dt1.Rows[0]["OpBalance"]);
                }


                SqlDataAdapter cmd2 = new SqlDataAdapter("select isnull(sum(AmountDr),0) as  AmountDr, isnull(sum(AmountCr),0) as AmountCr from PartiesLedger where CompId='" + CompID + "' and Code='" + PartyCode + "' and dateDr>='" + objModule7.StartDate() + "' and dateDr<='" + objModule7.EndDate(DateTime.Today) + "'", con);
                DataTable dt2 = new DataTable();
                cmd2.Fill(dt2);

                if (dt2.Rows.Count > 0)
                {
                    AmoDr = Convert.ToDecimal(dt2.Rows[0]["AmountDr"]);
                    AmoCr = Convert.ToDecimal(dt2.Rows[0]["AmountCr"]);
                }




                if (NorBalance == 1) { Closing = (OpBal + AmoDr) - AmoCr; }
                else
                if (NorBalance == 2) { Closing = (OpBal + AmoCr) - AmoDr; }



                if (con.State == ConnectionState.Closed) { con.Open(); }
                SqlCommand cmd44 = new SqlCommand("UpDate PartyCode set Balance=" + Closing + " where CompId='" + CompID + "' and Code='" + PartyCode + "'", con);
                cmd44.ExecuteNonQuery();
                SqlCommand cmd45 = new SqlCommand("UpDate GLCode set Balance=" + Closing + " where CompId='" + CompID + "' and Code='" + PartyCode + "'", con);
                cmd45.ExecuteNonQuery();
                con.Close();

                }           
           
        }

        public void UpdateCurrentYearInventoryClosingBalance()
        {
            SqlCommand cmd = new SqlCommand(@"select Code from InvCode  where compid='" + CompID + "'", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            Int32 InvCode = 1;
            Module7 objModule7 = new Module7();
            decimal OpBal = 0;
            decimal QtyDr = 0;
            decimal QtyCr = 0;
            Module2 objModule2 = new Module2();
            decimal Closing = 0;
            
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                InvCode = Convert.ToInt32(dt.Rows[i]["Code"]);
                

                ////----------------------------------------------

                SqlDataAdapter cmd1 = new SqlDataAdapter("select isnull(Opening,0) as OpBalance from InventoryOpeningBalance  where compid='" + CompID + "' and ICode=" + InvCode + " and  Dat='" + objModule7.StartDate() + "'", con);
                DataTable dt1 = new DataTable();
                cmd1.Fill(dt1);

                if (dt1.Rows.Count > 0)
                {
                    OpBal = Convert.ToDecimal(dt1.Rows[0]["OpBalance"]);
                }


                SqlDataAdapter cmd2 = new SqlDataAdapter("select isnull(sum(Received),0) as  Received, isnull(sum(Issued),0) as Issued from Inventory where CompId='" + CompID + "' and ICode=" + InvCode + " and Dat>='" + objModule7.StartDate() + "' and Dat<='" + objModule7.EndDate(DateTime.Today) + "'", con);
                DataTable dt2 = new DataTable();
                cmd2.Fill(dt2);

                if (dt2.Rows.Count > 0)
                {
                    QtyDr = Convert.ToDecimal(dt2.Rows[0]["Received"]);
                    QtyCr = Convert.ToDecimal(dt2.Rows[0]["Issued"]);
                }




               Closing = (OpBal + QtyDr) - QtyCr; 
               



                if (con.State == ConnectionState.Closed) { con.Open(); }
                SqlCommand cmd44 = new SqlCommand("UpDate InvCode set qty=" + Closing + " where CompId='" + CompID + "' and Code=" + InvCode, con);
                cmd44.ExecuteNonQuery();
                
                con.Close();





                ////-------------------------------------------

            }





        }


    }
}