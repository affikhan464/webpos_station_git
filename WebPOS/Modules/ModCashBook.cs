﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS
{
    public class ModCashBook
    {
        
        string CompID = "01";
        static ModGL objModGL = new ModGL();
        static string CashAccountGLCode = "01" + objModGL.GLCodeAgainstGLLockName("CashAccountGLCode");
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        public decimal UpToDateClosingBalanceGL_2(string Code,int NB, DateTime StartDate,DateTime EndDate)
        {

            Module7 objModule7 = new Module7();
            decimal OpBal = 0;
            //SqlDataAdapter cmd = new SqlDataAdapter("select isnull(sum(OpeningBal),0) as OpeningBal from GLOpeningBalance where   CompID='" + CompID + "' and code like '" + Code + "%' and Date1='" + objModule7.StartDateTwo(StartDate) + "'", con);
            SqlDataAdapter cmd = new SqlDataAdapter("select OpeningBal as OpeningBal from GLOpeningBalance where   CompID='" + CompID + "' and code ='" + Code + "' and Date1='" + objModule7.StartDateTwo(StartDate) + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            
            if (dt.Rows.Count > 0)
            {
                OpBal = Convert.ToDecimal(dt.Rows[0]["OpeningBal"]);

            }

            decimal sumDebit = 0;
            decimal sumCredit = 0;

            //SqlDataAdapter cmd1 = new SqlDataAdapter("select isnull(sum(AmountDr),0) as Dr, isnull( sum(AmountCr),0) as Cr from GeneralLedger where  CompID='" + CompID + "' and code like '" + Code + "%' and datedr>='" + objModule7.StartDateTwo(StartDate) + "' and datedr<='" + StartDate + "'", con);
            SqlDataAdapter cmd1 = new SqlDataAdapter("select isnull(sum(AmountDr),0) as Dr, isnull( sum(AmountCr),0) as Cr from GeneralLedger where  CompID='" + CompID + "'", con);
            DataTable dt1 = new DataTable();
            cmd1.Fill(dt1);

            if (dt1.Rows.Count > 0)
            {
                sumDebit = Convert.ToDecimal (dt1.Rows[0]["Dr"]);
                sumCredit = Convert.ToDecimal(dt1.Rows[0]["Dr"]);


            }

            decimal FinalBalance=0;
            if (NB == 1) {FinalBalance = (OpBal + sumDebit) - sumCredit;}
            else    
            if (NB == 2) {FinalBalance = (OpBal + sumCredit) - sumDebit;}

            return FinalBalance;


        }
        public decimal OpeningCash(DateTime StartDate, DateTime EndDate)
        {
            decimal op = 0;
            decimal OpeningCash = 0;
            decimal Dr = 0;
            decimal Cr = 0;
            Module7 objModule7 = new Module7();
            
           
            SqlDataAdapter cmd = new SqlDataAdapter("select OpeningBal from GLOpeningBalance where   CompID='" + CompID + "' and Code='" + CashAccountGLCode + "' and Date1='" + objModule7.StartDateTwo(StartDate) + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                op = Convert.ToDecimal(dt.Rows[0]["OpeningBal"]);

            }

            SqlDataAdapter cmd1 = new SqlDataAdapter("select isnull(sum(AmountDr),0) as AmountDr , isnull(sum(AmountCr),0) as AmountCr from GeneralLedger where  CompID='" + CompID + "' and code='" + CashAccountGLCode + "' and Datedr>='" + objModule7.StartDateTwo(StartDate) + "' and datedr<'" + StartDate + "'", con);
            DataTable dt1 = new DataTable();
            cmd1.Fill(dt1);

            if (dt1.Rows.Count > 0)
            {
                Dr = Convert.ToDecimal(dt1.Rows[0]["AmountDr"]);
                Cr = Convert.ToDecimal(dt1.Rows[0]["AmountCr"]);
            }

            OpeningCash = (op + Dr) - Cr;
            return OpeningCash;

        }
        public decimal CashSale(DateTime StartDa, DateTime EndDa)
        {
            decimal CashPaid = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(CashPaid),0) as CashPaid from Invoice3 where  compID='01' and date1>='" + StartDa + "' and date1<='" + EndDa + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                CashPaid = Convert.ToDecimal(dt.Rows[0]["CashPaid"]);

            }

            return CashPaid;

        }
        public decimal CashPurchaseReturn(DateTime StartDate, DateTime EndDate)
        {
            decimal CashPurchaseR = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(Paid),0) as Paid  from PurchaseReturn1 where compID='01' and Date1>='" + StartDate + "' and Date1<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                CashPurchaseR = Convert.ToDecimal(dt.Rows[0]["Paid"]);

            }

            return CashPurchaseR;

        }
        public decimal CashInvestmentFromDirector(DateTime StartDate, DateTime EndDate)
        {
            decimal CashInvestmentFromD = 0;
            ModGL objModGL = new ModGL();
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(AmountDr),0) as AmountDr from GeneralLedger where compID='01' and  Code='" + CashAccountGLCode + "' and SecondDescription='CashInvestment'  and  dateDr>='" + StartDate + "' and dateDr<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                CashInvestmentFromD = Convert.ToDecimal(dt.Rows[0]["AmountDr"]);

            }

            return CashInvestmentFromD;

        }
        public decimal CashReceivedFromParties(DateTime StartDate, DateTime EndDate)
        {
            decimal CashReceivedFromPart = 0;
            
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(AmountDr),0) as AmountDr from GeneralLedger where compID='01' and Code='" + CashAccountGLCode + "' and SecondDescription='FromParties' and  dateDr>='" + StartDate + "' and dateDr<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                CashReceivedFromPart = Convert.ToDecimal(dt.Rows[0]["AmountDr"]);

            }

            return CashReceivedFromPart;

        }
        public decimal DepositInToBank(DateTime StartDate, DateTime EndDate)
        {
            decimal DepositInToB = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(AmountCr),0) as AmountCr from GeneralLedger where compID='01' and Code='" + CashAccountGLCode + "' and SecondDescription='CashDepositIntoBank' and  dateDr>='" + StartDate + "' and dateDr<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                DepositInToB = Convert.ToDecimal(dt.Rows[0]["AmountCr"]);

            }

            return DepositInToB;

        }
        public decimal CashSaleReturn(DateTime StartDate, DateTime EndDate)
        {
            decimal CashSaleR = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(Paid),0) as Paid from SaleReturn1 where compID='01' and date1>='" + StartDate + "' and date1<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                CashSaleR = Convert.ToDecimal(dt.Rows[0]["Paid"]);

            }

            return CashSaleR;

        }
        public decimal CashIncentiveReceived(DateTime StartDate, DateTime EndDate)
        {
            decimal DepositInToB = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(AmountDr),0) as AmountDr from GeneralLedger where compID='01' and  Code='" + CashAccountGLCode + "' and SecondDescription='CashIncentiveReceived' and  dateDr>='" + StartDate + "' and dateDr<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                DepositInToB = Convert.ToDecimal(dt.Rows[0]["AmountDr"]);

            }

            return DepositInToB;

        }
        public decimal CashIncentiveGiven(DateTime StartDate, DateTime EndDate)
        {
            decimal DepositInToB = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(AmountCr),0) as AmountCr from GeneralLedger where compID='01' and  Code='" + CashAccountGLCode + "' and SecondDescription='CashIncentiveGiven' and  dateDr>='" + StartDate + "' and dateDr<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                DepositInToB = Convert.ToDecimal(dt.Rows[0]["AmountCr"]);

            }

            return DepositInToB;

        }
        public decimal OperationalExpences(DateTime StartDate, DateTime EndDate)
        {
            decimal DepositInToB = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(AmountCr),0) as AmountCr from GeneralLedger where compID='01' and  Code='" + CashAccountGLCode + "' and SecondDescription='OperationalExpencesThroughCash' and  dateDr>='" + StartDate + "' and dateDr<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                DepositInToB = Convert.ToDecimal(dt.Rows[0]["AmountCr"]);

            }

            return DepositInToB;

        }
        public decimal PersonalDrawing(DateTime StartDate, DateTime EndDate)
        {
            decimal DepositInToB = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(AmountCr),0) as AmountCr from GeneralLedger where compID='01' and Code='" + CashAccountGLCode + "' and SecondDescription='PersonalDrawingThroughCash' and  dateDr>='" + StartDate + "' and dateDr<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                DepositInToB = Convert.ToDecimal(dt.Rows[0]["AmountCr"]);

            }

            return DepositInToB;

        }
        public decimal PaidToParty(DateTime StartDate, DateTime EndDate)
        {
            decimal PaidToP = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(AmountCr),0) as AmountCr from GeneralLedger where compID='01' and Code='" + CashAccountGLCode + "' and SecondDescription='CashPaidToParty' and  dateDr>='" + StartDate + "' and dateDr<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                PaidToP = Convert.ToDecimal(dt.Rows[0]["AmountCr"]);

            }

            return PaidToP;

        }
        public decimal CashPurchase(DateTime StartDate, DateTime EndDate)
        {
            decimal CashPurchase = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(Paid),0) as Paid  from Purchase3 where  compID='01' and Date1>='" + StartDate + "' and Date1<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                CashPurchase = Convert.ToDecimal(dt.Rows[0]["Paid"]);

            }

            return CashPurchase;

        }
        public decimal DrawingFromBank(DateTime StartDate, DateTime EndDate)
        {
            decimal DrawingFromB = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("Select  isnull(Sum(AmountDr),0) as AmountDr from GeneralLedger where compID='01' and Code='" + CashAccountGLCode + "' and SecondDescription='CashWithDrawForPettyCash'  and  dateDr>='" + StartDate + "' and dateDr<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                DrawingFromB = Convert.ToDecimal(dt.Rows[0]["AmountDr"]);

            }

            return DrawingFromB;

        }

    }
}