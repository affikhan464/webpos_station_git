﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebPOS
{
    public class ModSaleReport
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);

        public decimal TotalSaleTo_Party(string PartyCode, DateTime StartDate, DateTime EndDate)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(sum(Invoice3.Total-Discount1),0)  from (Invoice1 INNER JOIN Invoice3 ON Invoice1.InvNo = Invoice3.InvNo)  where Invoice1.CompID='" + CompID + "' and Invoice1.PartyCode='" + PartyCode + "' and Invoice1.date1>='" + StartDate + "'  and Invoice1.date1<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal TotalSale = 0;



            if (dt.Rows.Count > 0)
            {
                TotalSale = Convert.ToInt32(dt.Rows[0]["TotalSale"]);

            }


            return TotalSale;
        }
        public string PartyCodeAgainstSaleBillNo(decimal SaleBillNo)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select PartyCode from Invoice1 where InvNo=" + SaleBillNo + " and CompID='" + CompID + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            string PartyCodeAgainstSaleBillNo = "";



            if (dt.Rows.Count > 0)
            {
                PartyCodeAgainstSaleBillNo = dt.Rows[0]["PartyCode"].ToString();

            }


            return PartyCodeAgainstSaleBillNo;
        }





    }
}