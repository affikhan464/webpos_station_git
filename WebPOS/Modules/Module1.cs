﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS
{
    public class Module1
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        public decimal MaxInvNoVerify()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select isnull(max(InvNo),1) as InvNo from Invoice1 where CompID='" + CompID + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxInvNoVerify = 1;



            if (dt.Rows.Count > 0)
            {
                MaxInvNoVerify = Convert.ToDecimal(dt.Rows[0]["InvNo"]);

            }


            return MaxInvNoVerify;
        }
        public decimal MaxInvNo()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select isnull(max(InvNo),0) as InvNo from Invoice1 where CompID='" + CompID + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxInvNo = 1;



            if (dt.Rows.Count > 0)
            {
                MaxInvNo = Convert.ToDecimal(dt.Rows[0]["InvNo"]);
                MaxInvNo = MaxInvNo + 1;
            }


            return MaxInvNo;
            if (con.State==ConnectionState.Closed) { con.Open(); }
            SqlCommand cmd1 = new SqlCommand("insert into VoucherNoVerification(V_No,V_Type,PID,CompID) values('" + MaxInvNo + "','" + "MaxInvNo" + "','" + "1" + "','" + CompID + "')", con);
            con.Close();
        }
        public string LastInvNoPurchase()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select InvNo from Purchase1 where CompID='" + CompID + "' order by dat desc", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            var LastPurchaseInvNo = string.Empty;



            if (dt.Rows.Count > 0)
            {
                LastPurchaseInvNo = dt.Rows[0]["InvNo"].ToString();

            }


            return LastPurchaseInvNo;
        }
        public string MaxGLCode(string parentCode, int level)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select TOP 1 [Code]  FROM [GLCode]  where Code like '" + parentCode + "%'  and Lvl = " + level + "  group by Code  order by code desc  ", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            var maxGLCode = string.Empty;



            if (dt.Rows.Count > 0)
            {
                var newCode = (Convert.ToInt64(dt.Rows[0]["Code"].ToString().Substring(parentCode.Length))) + 1;

                if (level < 5)
                    maxGLCode = parentCode + newCode.ToString("D2");
                else
                    maxGLCode = parentCode + newCode.ToString("D5");

            }
            else
            {
                if (level < 5)
                    maxGLCode = parentCode + "01";
                else
                    maxGLCode = parentCode + "00001";
            }


            return maxGLCode;
        }
        public decimal MaxInvNoPurchaseReturn()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select isnull(max(InvNo),0) as InvNo from PurchaseReturn1 where CompID='" + CompID + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxInvNoPurchaseReturn = 1;

            if (dt.Rows.Count > 0)
            {
                MaxInvNoPurchaseReturn = Convert.ToDecimal(dt.Rows[0]["InvNo"]);
                MaxInvNoPurchaseReturn = MaxInvNoPurchaseReturn + 1;
            }


            return MaxInvNoPurchaseReturn;
            if (con.State==ConnectionState.Closed) { con.Open(); }
            SqlCommand cmd1 = new SqlCommand("insert into VoucherNoVerification(V_No,V_Type,PID,CompID) values('" + MaxInvNoPurchaseReturn + "','" + "MaxInvNoPurchaseReturn" + "','" + "1" + "','" + CompID + "')", con);
            con.Close();
        }
        public decimal MaxSaleReturnInvNo()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select isnull(max(InvNo),0) as InvNo from SaleReturn1 where CompID='" + CompID + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxSaleReturnInvNo = 1;



            if (dt.Rows.Count > 0)
            {
                MaxSaleReturnInvNo = Convert.ToDecimal(dt.Rows[0]["InvNo"]);
                MaxSaleReturnInvNo = MaxSaleReturnInvNo + 1;
            }


            return MaxSaleReturnInvNo;
            if (con.State==ConnectionState.Closed) { con.Open(); }
            SqlCommand cmd1 = new SqlCommand("insert into VoucherNoVerification(V_No,V_Type,PID,CompID) values('" + MaxSaleReturnInvNo + "','" + "MaxSaleReturnInvNo" + "','" + "1" + "','" + CompID + "')", con);
            con.Close();
        }
        public decimal MaxCPV()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select isnull(max(v_no),0) as v_no from GeneralLedger  where  CompID='" + CompID + "' and V_Type='CPV'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxCPV = 1;



            if (dt.Rows.Count > 0)
            {
                MaxCPV = Convert.ToDecimal(dt.Rows[0]["v_no"]);
                MaxCPV = MaxCPV + 1;

            }


            return MaxCPV;
        }
        public decimal MaxCPV(SqlConnection con, SqlTransaction tran)
        {
            var cmd = new SqlCommand("select isnull(max(v_no),0) as v_no from GeneralLedger  where  CompID='" + CompID + "' and V_Type='CPV'", con, tran);
            var adapter = new SqlDataAdapter(cmd);
            var dt = new DataTable();
            adapter.Fill(dt);
            decimal MaxCPV = 1;



            if (dt.Rows.Count > 0)
            {
                MaxCPV = Convert.ToDecimal(dt.Rows[0]["v_no"]);
                MaxCPV = MaxCPV + 1;

            }


            return MaxCPV;
        }
        public decimal MaxCRV(SqlConnection con,SqlTransaction tran)
        {
            SqlCommand cmd = new SqlCommand("select IsNull(max(V_No),0) as Max_V_No from GeneralLedger  where  CompID='" + CompID + "' and V_Type='CRV'", con,tran);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            decimal Max_V_No = 1;

            if (dt.Rows.Count > 0)
            {
                Max_V_No = Convert.ToDecimal(dt.Rows[0]["Max_V_No"]);
                Max_V_No = Max_V_No + 1;
            }

            return Max_V_No;
        }
        public decimal MaxCRV()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select IsNull(max(V_No),0) as Max_V_No from GeneralLedger  where  CompID='" + CompID + "' and V_Type='CRV'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal Max_V_No = 1;

            if (dt.Rows.Count > 0)
            {
                Max_V_No = Convert.ToDecimal(dt.Rows[0]["Max_V_No"]);
                Max_V_No = Max_V_No + 1;
            }
            if (con.State == ConnectionState.Closed) { con.Open(); }

            con.Close();
            return Max_V_No;
        }
        public decimal MaxJVNo(SqlConnection con, SqlTransaction tran)
        {

            var cmd = new SqlCommand("select IsNull(max(V_No),0) as Max_V_No from GeneralLedger  where  CompID='" + CompID + "' and V_Type='JV'", con,tran);
            SqlDataAdapter adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            decimal Max_V_No = 1;

            if (dt.Rows.Count > 0)
            {
                Max_V_No = Convert.ToDecimal(dt.Rows[0]["Max_V_No"]);
                Max_V_No = Max_V_No + 1;
            }
            return Max_V_No;

        }
        public decimal MaxJVNo()
        {

            SqlDataAdapter cmd = new SqlDataAdapter("select IsNull(max(V_No),0) as Max_V_No from GeneralLedger  where  CompID='" + CompID + "' and V_Type='JV'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal Max_V_No = 1;

            if (dt.Rows.Count > 0)
            {
                Max_V_No = Convert.ToDecimal(dt.Rows[0]["Max_V_No"]);
                Max_V_No = Max_V_No + 1;
            }
            return Max_V_No;

        }

        public decimal MaxCashWithDrawForPettyCash()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select IsNull(max(V_No),0) as MaxV from GeneralLedger  where  CompID='" + CompID + "' and V_Type ='CRV' AND SecondDescription = 'CashWithDrawForPettyCash'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxV = 1;

            if (dt.Rows.Count > 0)
            {
                MaxV = Convert.ToDecimal(dt.Rows[0]["MaxV"]);
                MaxV = MaxV + 1;
            }
            if (con.State==ConnectionState.Closed) { con.Open(); }

            con.Close();
            return MaxV;
        }
        public decimal MaxCashExpenceVoucher()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select IsNull(max(V_No),0) as MaxV from GeneralLedger  where  CompID='" + CompID + "' and V_Type='CPV' and Code='" + "0101010100001" + "' and SecondDescription='OperationalExpencesThroughCash'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxV = 1;

            if (dt.Rows.Count > 0)
            {
                MaxV = Convert.ToDecimal(dt.Rows[0]["MaxV"]);
                MaxV = MaxV + 1;
            }
            if (con.State==ConnectionState.Closed) { con.Open(); }

            con.Close();
            return MaxV;
        }
        public decimal MaxCashReceiptVoucher()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select IsNull(max(V_No),0) as MaxV from GeneralLedger  where  CompID='" + CompID + "'  and V_Type='CRV' and Code='" + "0101010100001" + "' and SecondDescription='FromParties'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxV = 1;

            if (dt.Rows.Count > 0)
            {
                MaxV = Convert.ToDecimal(dt.Rows[0]["MaxV"]);
                MaxV = MaxV + 1;
            }
            if (con.State==ConnectionState.Closed) { con.Open(); }

            con.Close();
            return MaxV;
        }
        public decimal MaxCashDepositInToBankVoucher()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select IsNull(max(V_No),0) as MaxV from GeneralLedger  where  CompID='" + CompID + "' and  V_Type ='brv' AND SecondDescription = 'CashDepositInToBank' ", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxV = 1;

            if (dt.Rows.Count > 0)
            {
                MaxV = Convert.ToDecimal(dt.Rows[0]["MaxV"]);
                MaxV = MaxV + 1;
            }
            if (con.State==ConnectionState.Closed) { con.Open(); }

            con.Close();
            return MaxV;
        }
        public decimal MaxBRV()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select IsNull(max(V_No),0) as MaxBRV from GeneralLedger  where  CompID='" + CompID + "' and V_Type='BRV'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxBRV = 1;

            if (dt.Rows.Count > 0)
            {
                MaxBRV = Convert.ToDecimal(dt.Rows[0]["MaxBRV"]);
                MaxBRV = MaxBRV + 1;
            }
            if (con.State==ConnectionState.Closed) { con.Open(); }
            //SqlCommand cmd1 = new SqlCommand("insert into VoucherNoVerification(V_No,V_Type,PID,CompID) values('" + MaxBRV + "','" + "MaxBRV" + "','" + PID + "','" + CompID + "')", con);
            //cmd1.ExecuteNonQuery();

            con.Close();
            return MaxBRV;
        }

        public decimal MaxCIRV()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select IsNull(max(InvNo),0) as MaxIRV from CashIncentive  where  CompID='" + CompID + "'  and Nature='CashIncentiveReceived'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxIRV = 1;

            if (dt.Rows.Count > 0)
            {
                MaxIRV = Convert.ToDecimal(dt.Rows[0]["MaxIRV"]);
                MaxIRV = MaxIRV + 1;
            }
            if (con.State==ConnectionState.Closed) { con.Open(); }
            //SqlCommand cmd1 = new SqlCommand("insert into VoucherNoVerification(V_No,V_Type,PID,CompID) values('" + MaxBRV + "','" + "MaxBRV" + "','" + PID + "','" + CompID + "')", con);
            //cmd1.ExecuteNonQuery();

            con.Close();
            return MaxIRV;
        }
        public decimal MaxCIGV()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select IsNull(max(InvNo),0) as MaxV from CashIncentive  where  CompID='" + CompID + "'  and Nature='CashIncentiveGiven'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxV = 1;

            if (dt.Rows.Count > 0)
            {
                MaxV = Convert.ToDecimal(dt.Rows[0]["MaxV"]);
                MaxV = MaxV + 1;
            }
            if (con.State==ConnectionState.Closed) { con.Open(); }

            con.Close();
            return MaxV;
        }

        public decimal MaxIncentiveGivenVoucher()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select IsNull(max(InvNo),0) as MaxV from CashIncentive  where  CompID='" + CompID + "' and V_Type = 'JV' and DescriptionOfBillNo = 'IncentiveLoss'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxV = 1;

            if (dt.Rows.Count > 0)
            {
                MaxV = Convert.ToDecimal(dt.Rows[0]["MaxV"]);
                MaxV = MaxV + 1;
            }
            if (con.State==ConnectionState.Closed) { con.Open(); }

            con.Close();
            return MaxV;
        }
        public decimal MaxBPV()
        {
            int PID = 1;
            SqlDataAdapter cmd = new SqlDataAdapter("select IsNull(max(V_No),0) as MaxBPV from GeneralLedger  where  CompID='" + CompID + "' and V_Type='BPV'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxBPV = 1;

            if (dt.Rows.Count > 0)
            {
                MaxBPV = Convert.ToDecimal(dt.Rows[0]["MaxBPV"]);
                MaxBPV = MaxBPV + 1;
            }
            //if (con.State==ConnectionState.Closed) { con.Open(); }
            ////SqlCommand cmd1 = new SqlCommand("insert into VoucherNoVerification(V_No,V_Type,PID,CompID) values('" + MaxBPV + "','" + "MaxBPV" + "','" + PID + "','" + CompID + "')", con);
            ////cmd1.ExecuteNonQuery();

            //con.Close();
            return MaxBPV;
        }
        public string MaxPartyCode(Int32 NorBalance)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select max(code) as code from PartyCode where CompID='" + CompID + "' and NorBalance=" + NorBalance, con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            string MaxPartyC = "";
            if (NorBalance == 1) { MaxPartyC = CompID + "01010300001"; }
            if (NorBalance == 2) { MaxPartyC = CompID + "02010100001"; }

            if (dt.Rows.Count > 0 && dt.Rows[0]["code"] != DBNull.Value)
            {
                MaxPartyC = "0" + Convert.ToString((Convert.ToDecimal(dt.Rows[0]["code"]) + 1));
            }
            return MaxPartyC;
        }
        public decimal MaxPackingCode()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select max(ID) as ID from Packing where CompID='" + CompID + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxPackingCod = 1;
            if (dt.Rows.Count > 0)
            {
                MaxPackingCod = Convert.ToDecimal(dt.Rows[0]["ID"]);
                MaxPackingCod = MaxPackingCod + 1;
            }
            return MaxPackingCod;
        }
        public decimal MaxManufCode()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select max(ID) as ID from Manufacturers where CompID='" + CompID + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxMafCod = 1;
            if (dt.Rows.Count > 0)
            {
                MaxMafCod = Convert.ToDecimal(dt.Rows[0]["ID"]);
                MaxMafCod = MaxMafCod + 1;
            }
            return MaxMafCod;
        }
        public decimal MaxInvCode()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select isnull(max(Code),0) as Code from invcode where CompID='" + CompID + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxInvC = 0;
            if (dt.Rows.Count > 0)
            {
                MaxInvC = Convert.ToDecimal(dt.Rows[0]["Code"]);
            }
            return MaxInvC + 1;
        }
        public decimal MaxGodown()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select max(ID) as ID from Godown where CompID='" + CompID + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxGoDownCod = 1;
            if (dt.Rows.Count > 0)
            {
                MaxGoDownCod = Convert.ToDecimal(dt.Rows[0]["ID"]);
                MaxGoDownCod = MaxGoDownCod + 1;
            }
            return MaxGoDownCod;
        }
        public decimal MaxColorCode()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select max(ID) as ID from Color where CompID='" + CompID + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxColorCod = 1;
            if (dt.Rows.Count > 0)
            {
                MaxColorCod = Convert.ToDecimal(dt.Rows[0]["ID"]);
                MaxColorCod = MaxColorCod + 1;
            }
            return MaxColorCod;
        }
        public decimal MaxAccountUnitCode()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select max(Code) as ID from AccountUnit where CompID='" + CompID + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxCode = 1;
            if (dt.Rows.Count > 0)
            {
                MaxCode = Convert.ToDecimal(dt.Rows[0]["ID"]);
                MaxCode = MaxCode + 1;
            }
            return MaxCode;
        }
        public decimal MaxClassCode()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select max(ID) as ID from Class where CompID='" + CompID + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxClassCod = 1;
            if (dt.Rows.Count > 0)
            {
                MaxClassCod = Convert.ToDecimal(dt.Rows[0]["ID"]);
                MaxClassCod = MaxClassCod + 1;
            }
            return MaxClassCod;
        }
        public decimal MaxHeightCode()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select max(ID) as ID from Height where CompID='" + CompID + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxClassCod = 1;
            if (dt.Rows.Count > 0)
            {
                MaxClassCod = Convert.ToDecimal(dt.Rows[0]["ID"]);
                MaxClassCod = MaxClassCod + 1;
            }
            return MaxClassCod;
        }
        public decimal MaxLengthCode()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select max(ID) as ID from Length where CompID='" + CompID + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxClassCod = 1;
            if (dt.Rows.Count > 0)
            {
                MaxClassCod = Convert.ToDecimal(dt.Rows[0]["ID"]);
                MaxClassCod = MaxClassCod + 1;
            }
            return MaxClassCod;
        }
        public decimal MaxCategoryCode()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select max(ID) as ID from Category where CompID='" + CompID + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxCategoryCod = 1;
            if (dt.Rows.Count > 0)
            {
                MaxCategoryCod = Convert.ToDecimal(dt.Rows[0]["ID"]);
                MaxCategoryCod = MaxCategoryCod + 1;
            }
            return MaxCategoryCod;
        }
        public decimal MaxSaleManCode()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select max(ID) as ID from SaleManList where CompID='" + CompID + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxSaleManCode = 1;
            if (dt.Rows.Count > 0)
            {
                MaxSaleManCode = Convert.ToDecimal(dt.Rows[0]["ID"]);
                MaxSaleManCode = MaxSaleManCode + 1;
            }
            return MaxSaleManCode;
        }

        public decimal MaxBrandCode()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select isnull(max(Code),0) as Code from BrandName where CompID='" + CompID + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxBrandCod = 1;
            if (dt.Rows.Count > 0)
            {
                MaxBrandCod = Convert.ToDecimal(dt.Rows[0]["Code"]);
                MaxBrandCod = MaxBrandCod + 1;
            }
            return MaxBrandCod;
        }
        public decimal MaxSaleReturnInvNoVerify()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select max(InvNo) as InvNo from SaleReturn1 where CompID='" + CompID + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxInvNoVerify = 1;



            if (dt.Rows.Count > 0)
            {
                MaxInvNoVerify = Convert.ToDecimal(dt.Rows[0]["InvNo"]);

            }


            return MaxInvNoVerify;
        }
      
        public decimal MaxStockMovementVoucher()
        {
            var strSql = "select max(SMONo) as v_no from InventoryMovement where compid='" + CompID + "'";

            //Iif (con.State == ConnectionState.Closed) { con.Open(); }


            var cmd = new SqlCommand(strSql, con);
            var adapter = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            decimal MaxVNo = 1;



            if (dt.Rows.Count > 0)
            {
                MaxVNo = Convert.ToDecimal(dt.Rows[0]["v_no"]) + 1;
            }
            con.Close();

            return MaxVNo;
        }
        public decimal MaxStationCode()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select max(ID) as Id from Station where CompID='" + CompID + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal MaxNo = 1;

            if (dt.Rows.Count > 0)
            {
                MaxNo = Convert.ToDecimal(dt.Rows[0]["Id"]) + 1;
            }


            return MaxNo;
        }
    }
}