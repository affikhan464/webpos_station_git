﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebPOS
{



    public class Module4
    {
        static string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        public void ClosingBalancePartiesNew(string PartyCode, SqlConnection connection, SqlTransaction trans)
        {

            Module7 objModule7 = new Module7();
            decimal OpBal = 0;
            decimal AmoDr = 0;
            decimal AmoCr = 0;
            var cmd1 = new SqlCommand("select isnull(OpBalance,0) as OpBalance from PartyOpBalance where CompId='" + CompID + "' and Code='" + PartyCode + "' and dat='" + objModule7.StartDate() + "'", connection, trans);

            SqlDataAdapter adptr1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            adptr1.Fill(dt1);

            if (dt1.Rows.Count > 0)
            {
                OpBal = Convert.ToDecimal(dt1.Rows[0]["OpBalance"]);
            }

            var cmd = new SqlCommand("select isnull(sum(AmountDr),0) as  AmountDr, isnull(sum(AmountCr),0) as AmountCr from PartiesLedger where CompId='" + CompID + "' and Code='" + PartyCode + "' and dateDr>='" + objModule7.StartDate() + "' and dateDr<='" + objModule7.EndDate(DateTime.Today) + "'", connection, trans);
            SqlDataAdapter adptr = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adptr.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                AmoDr = Convert.ToDecimal(dt.Rows[0]["AmountDr"]);
                AmoCr = Convert.ToDecimal(dt.Rows[0]["AmountCr"]);
            }




            Module2 objModule2 = new Module2();
            decimal Closing = 0;
            int NorBalance = objModule2.NormalBalanceGL(PartyCode);
            //NorBalance = 1;
            if (NorBalance == 1) { Closing = (OpBal + AmoDr) - AmoCr; }
            else
            if (NorBalance == 2) { Closing = (OpBal + AmoCr) - AmoDr; }




            SqlCommand cmd44 = new SqlCommand("UpDate PartyCode set Balance=" + Closing + " where CompId='" + CompID + "' and Code='" + PartyCode + "'", connection, trans);
            cmd44.ExecuteNonQuery();
            SqlCommand cmd45 = new SqlCommand("UpDate GLCode set Balance=" + Closing + " where CompId='" + CompID + "' and Code='" + PartyCode + "'", connection, trans);
            cmd45.ExecuteNonQuery();

        }
        public void ClosingBalanceParties(string PartyCode)
        {

            Module7 objModule7 = new Module7();
            decimal OpBal = 0;
            decimal AmoDr = 0;
            decimal AmoCr = 0;
            SqlDataAdapter cmd = new SqlDataAdapter("select isnull(OpBalance,0) as OpBalance from PartyOpBalance where CompId='" + CompID + "' and Code='" + PartyCode + "' and dat='" + objModule7.StartDate() + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                OpBal = Convert.ToDecimal(dt.Rows[0]["OpBalance"]);
            }


            SqlDataAdapter cmd1 = new SqlDataAdapter("select isnull(sum(AmountDr),0) as  AmountDr, isnull(sum(AmountCr),0) as AmountCr from PartiesLedger where CompId='" + CompID + "' and Code='" + PartyCode + "' and dateDr>='" + objModule7.StartDate() + "' and dateDr<='" + objModule7.EndDate(DateTime.Today) + "'", con);
            DataTable dt1 = new DataTable();
            cmd1.Fill(dt1);

            if (dt1.Rows.Count > 0)
            {
                AmoDr = Convert.ToDecimal(dt1.Rows[0]["AmountDr"]);
                AmoCr = Convert.ToDecimal(dt1.Rows[0]["AmountCr"]);
            }




            Module2 objModule2 = new Module2();
            decimal Closing = 0;
            int NorBalance = objModule2.NormalBalanceGL(PartyCode);
            //NorBalance = 1;
            if (NorBalance == 1) { Closing = (OpBal + AmoDr) - AmoCr; }
            else
            if (NorBalance == 2) { Closing = (OpBal + AmoCr) - AmoDr; }



            if (con.State==ConnectionState.Closed) { con.Open(); }
            SqlCommand cmd44 = new SqlCommand("UpDate PartyCode set Balance=" + Closing + " where CompId='" + CompID + "' and Code='" + PartyCode + "'", con);
            cmd44.ExecuteNonQuery();
            SqlCommand cmd45 = new SqlCommand("UpDate GLCode set Balance=" + Closing + " where CompId='" + CompID + "' and Code='" + PartyCode + "'", con);
            cmd45.ExecuteNonQuery();
            con.Close();
        }

        public void ClosingBalanceNewTechniqueIssue(decimal ItemCode, decimal Qty, SqlTransaction trans, SqlConnection conn)
        {
            Module7 objModule7 = new Module7();
            decimal PreviousBalance = 0;
            decimal CurrentlyIssued = Qty;

            var cmd = new SqlCommand("select isnull(Qty,0) as Qty from InvCode where CompId='" + CompID + "' and Code=" + ItemCode, conn, trans);
            var adptr = new SqlDataAdapter(cmd);
            var dt = new DataTable();
            adptr.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                PreviousBalance = Convert.ToDecimal(dt.Rows[0]["Qty"]);
            }


            Module2 objModule2 = new Module2();

            decimal ClosingBal = (PreviousBalance - CurrentlyIssued);


            SqlCommand cmd44 = new SqlCommand("UpDate InvCode set Qty=" + ClosingBal + " where CompId='" + CompID + "' and Code=" + ItemCode, conn, trans);
            cmd44.ExecuteNonQuery();


        }
        public void ClosingBalanceNewTechniqueReceived(decimal ItemCode, decimal Qty, SqlTransaction trans, SqlConnection conn)
        {
            Module7 objModule7 = new Module7();
            decimal PreviousBalance = 0;
            decimal CurrentlyIssued = Qty;
            var cmd = new SqlCommand("select isnull(Qty,0) as Qty from InvCode where CompId='" + CompID + "' and Code=" + ItemCode, conn, trans);
            var adptr = new SqlDataAdapter(cmd);
            var dt = new DataTable();
            adptr.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                PreviousBalance = Convert.ToDecimal(dt.Rows[0]["Qty"]);
            }


            Module2 objModule2 = new Module2();

            decimal ClosingBal = (PreviousBalance + CurrentlyIssued);

            
            SqlCommand cmd44 = new SqlCommand("UpDate InvCode set Qty=" + ClosingBal + " where CompId='" + CompID + "' and Code=" + ItemCode, conn,trans);
            cmd44.ExecuteNonQuery();
           

        }

        public void ClosingBalance(decimal ItemCode)
        {
            Module7 objModule7 = new Module7();
            decimal OpBalance = 0;
            decimal Received = 0;
            decimal Issued = 0;
            decimal ClosingBal = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("select isnull(Opening,0) as Opening from InventoryOpeningBalance where CompId='" + CompID + "' and ICode=" + ItemCode + " and dat='" + objModule7.StartDate() + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                OpBalance = Convert.ToDecimal(dt.Rows[0]["Opening"]);
            }


            SqlDataAdapter cmd2 = new SqlDataAdapter("select isnull(sum(received),0) as received, isnull(sum(issued),0) as issued from Inventory where CompId='" + CompID + "' and ICode=" + ItemCode + " and dat>='" + objModule7.StartDate() + "' and dat<='" + objModule7.EndDate(DateTime.Today) + "'", con);
            DataTable dt2 = new DataTable();
            cmd2.Fill(dt2);

            if (dt2.Rows.Count > 0)
            {
                Received = Convert.ToDecimal(dt2.Rows[0]["received"]);
                Issued = Convert.ToDecimal(dt2.Rows[0]["issued"]);
            }



            ClosingBal = (OpBalance + Received - Issued);

            if (con.State==ConnectionState.Closed) { con.Open(); }
            SqlCommand cmd3 = new SqlCommand("UpDate InvCode set Qty=" + ClosingBal + " where CompId='" + CompID + "' and Code=" + ItemCode, con);
            cmd3.ExecuteNonQuery();
            con.Close();

        }
        public void ClosingBalanceNewTechniqueStationIssue(decimal ItemCode, decimal Qty, int StationID, SqlTransaction trans, SqlConnection con)
        {
            Module7 objModule7 = new Module7();

            var cmd = new SqlCommand("select isnull(Current_QTY,0) as Current_QTY from InventoryOpeningBalance where CompId='" + CompID + "' and  ICode=" + ItemCode + " and StationID=" + StationID + " and Dat='" + objModule7.StartDate() + "'", con, trans);
            SqlDataAdapter adptr = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adptr.Fill(dt);
            decimal PreviousBalance = 0;
            decimal Closing = 0;
            //Int32 StopSale=0;

            if (dt.Rows.Count > 0)
            {
                PreviousBalance = Convert.ToDecimal(dt.Rows[0]["Current_QTY"]);

            }

            Closing = (PreviousBalance - Qty);


            SqlCommand cmdUp = new SqlCommand("update InventoryOpeningBalance set Current_QTY=" + Closing + " where CompID='" + CompID + "' and ICode=" + ItemCode + " and StationID=" + StationID + " and Dat='" + objModule7.StartDate() + "'", con, trans);
            cmdUp.ExecuteNonQuery();

        }

        public void ClosingBalanceNewTechniqueStationReceived(decimal ItemCode, decimal Qty, int StationID, SqlTransaction trans, SqlConnection con)
        {
            decimal PreviousBalance = 0;
            decimal Closing = 0;
            decimal CurrentlyIssued = Qty;
            Module7 objModule7 = new Module7();

            var cmd = new SqlCommand("select Current_QTY from InventoryOpeningBalance where CompID='" + CompID + "' and ICode=" + ItemCode + " and StationID=" + StationID + " and Dat='" + objModule7.StartDate() + "'", con, trans);
            SqlDataAdapter adptr = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adptr.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                PreviousBalance = Convert.ToDecimal(dt.Rows[0]["Current_QTY"]);

            }

            Closing = (PreviousBalance + CurrentlyIssued);

            SqlCommand cmdUp = new SqlCommand("update InventoryOpeningBalance set Current_QTY=" + Closing + " where CompID='" + CompID + "' and ICode=" + ItemCode + " and StationID=" + StationID + " and Dat='" + objModule7.StartDate() + "'", con, trans);
            cmdUp.ExecuteNonQuery();

        }



    }
}