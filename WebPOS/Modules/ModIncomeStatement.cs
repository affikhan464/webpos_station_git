﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebPOS
{
    public class ModIncomeStatement
    {
        
        string CompID = "01";
        string CashAccountGLCode = "01" + "01010100001";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        public decimal CashIncentiveReceived(DateTime StartDate, DateTime EndDate)
        {

            decimal CashIncentiveR = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(AmountDr),0) as AmountDr from GeneralLedger where  CompID='" + CompID + "' and  Code='" + CashAccountGLCode + "' and SecondDescription='CashIncentiveReceived'  and  dateDr>='" + StartDate + "' and dateDr<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                CashIncentiveR = Convert.ToDecimal(dt.Rows[0]["AmountDr"]);

            }

            return CashIncentiveR;
        }
        public decimal CashIncentiveGiven(DateTime StartDate, DateTime EndDate)
        {

            decimal CashIncentiveG = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(AmountCr),0) as AmountCr from GeneralLedger where  CompID='" + CompID + "' and  Code='" + CashAccountGLCode + "' and SecondDescription='CashIncentiveGiven'  and  dateDr>='" + StartDate + "' and dateDr<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                CashIncentiveG = Convert.ToDecimal(dt.Rows[0]["AmountCr"]);

            }

            return CashIncentiveG;


        }

    }
}