﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS
{
    public class ModGLCode
    {
        static string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        Module7 objModule7 = new Module7();
        public decimal UpToDateBalanceOfParty(string PartyCode, int NormalBalance, DateTime EndDate)
        {
            Module7 objModule7 = new Module7();
            decimal OpBal = 0;
            string CompID = "01";
            //Select OpBalance from                              PartyOpBalance where CompID = '" & logon.Compand Code = '" & Trim(PartyCodeand Dat= '" & Module7.StartDateTwo(EndDate2) & "'"
            SqlDataAdapter cmd = new SqlDataAdapter("select isnull(sum(OpBalance),0) as OpeningBal from PartyOpBalance where CompID='" + CompID + "' and code ='" + PartyCode + "' and Dat='" + objModule7.StartDateTwo(EndDate) + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                OpBal = Convert.ToDecimal(dt.Rows[0]["OpeningBal"]);

            }
            decimal sumDebit = 0;
            decimal sumCredit = 0;
            //select sum(AmountDr) as Dr,sum(AmountCr) as Cr from PartiesLedger where  CompID='" & logon.CompID & "' and Code='" & Trim(PartyCode) & "' and dateDr>='" & Module7.StartDateTwo(EndDate2) & "' and dateDr<='" & EndDate2 & "'"
            SqlDataAdapter cmd2 = new SqlDataAdapter("select isnull(sum(AmountDr),0) as AmountDr , isnull(sum(AmountCr),0) as AmountCr from PartiesLedger where CompID='" + CompID + "' and code = '" + PartyCode + "' and datedr>='" + objModule7.StartDateTwo(EndDate) + "' and datedr<='" + EndDate + "'", con);
            DataTable dt2 = new DataTable();
            cmd2.Fill(dt2);

            if (dt2.Rows.Count > 0)
            {
                sumDebit = Convert.ToDecimal(dt2.Rows[0]["AmountDr"]);
                sumCredit = Convert.ToDecimal(dt2.Rows[0]["AmountCr"]);

            }


            decimal FinalBalance = 0;
            if (NormalBalance == 1)
            {
                FinalBalance = (OpBal + sumDebit) - sumCredit;
            }
            else
            if (NormalBalance == 2)
            {
                FinalBalance = (OpBal + sumCredit) - sumDebit;
            }
            return FinalBalance;
        }
        public string MaxGLCodeLevel5(String Level4Code)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select  max(Code) as Code from GLCode where  CompID='" + CompID + "' and Code like '" + Level4Code + "%' and lvl=5", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            string MaxGLCodeLevel5 = Level4Code + "00001";

            if (dt.Rows.Count > 0 && dt.Rows[0]["Code"] != DBNull.Value)
            {
                MaxGLCodeLevel5 = "0" + Convert.ToString((Convert.ToDecimal(dt.Rows[0]["Code"]) + 1));
            }
            return MaxGLCodeLevel5;
        }
        public void UpDateClosingBal_GL(string GLCode, decimal CurrentTranAmount, Int32 Plus_Minus)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Balance,NorBalance from  GLCode where CompId='" + CompID + "' and Code='" + GLCode + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal PBalance = 0;
            Int32 NorBalance = 1;
            if (dt.Rows.Count > 0)
            {
                PBalance = Convert.ToDecimal(dt.Rows[0]["Balance"]);
                NorBalance = Convert.ToInt32(dt.Rows[0]["NorBalance"]);

            }
            if (con.State==ConnectionState.Closed) { con.Open(); }
            if (NorBalance == 1 && Plus_Minus == 1)
            {
                SqlCommand cmd1 = new SqlCommand("Update GLCode set Balance=" + (PBalance + CurrentTranAmount) + " where  CompId='" + CompID + "' and Code='" + GLCode + "'", con);
                cmd1.ExecuteNonQuery();
            }
            else
                if (NorBalance == 1 && Plus_Minus == 2)
            {
                SqlCommand cmd1 = new SqlCommand("Update GLCode set Balance=" + (PBalance - CurrentTranAmount) + " where  CompId='" + CompID + "' and Code='" + GLCode + "'", con);
                cmd1.ExecuteNonQuery();
            }
            else
                    if (NorBalance == 2 && Plus_Minus == 1)
            {
                SqlCommand cmd1 = new SqlCommand("Update GLCode set Balance=" + (PBalance + CurrentTranAmount) + " where  CompId='" + CompID + "' and Code='" + GLCode + "'", con);
                cmd1.ExecuteNonQuery();
            }
            else
                        if (NorBalance == 2 && Plus_Minus == 2)
            {
                SqlCommand cmd1 = new SqlCommand("Update GLCode set Balance=" + (PBalance - CurrentTranAmount) + " where  CompId='" + CompID + "' and Code='" + GLCode + "'", con);
                cmd1.ExecuteNonQuery();
            }
            con.Close();

        }

        public decimal UpToDateClosingBalanceGL(string GLCode, int NormalBalance, DateTime StartDate, DateTime EndDate)
        {

            decimal OpBal = 0;
            SqlDataAdapter cmd = new SqlDataAdapter("select isnull(sum(OpeningBal),0) as OpeningBal from GLOpeningBalance where   CompID='" + CompID + "' and code like '" + GLCode + "%' and Date1='" + objModule7.StartDateTwo(StartDate) + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                OpBal = Convert.ToDecimal(dt.Rows[0]["OpeningBal"]);

            }
            decimal sumDebit = 0;
            decimal sumCredit = 0;

            SqlDataAdapter cmd2 = new SqlDataAdapter("select isnull(sum(AmountDr),0) as AmountDr , isnull(sum(AmountCr),0) as AmountCr from GeneralLedger where  CompID='" + CompID + "' and code like '" + GLCode + "%' and datedr>='" + StartDate + "' and datedr<='" + EndDate + "'", con);
            DataTable dt2 = new DataTable();
            cmd2.Fill(dt2);

            if (dt2.Rows.Count > 0)
            {
                sumDebit = Convert.ToDecimal(dt2.Rows[0]["AmountDr"]);
                sumCredit = Convert.ToDecimal(dt2.Rows[0]["AmountCr"]);

            }


            decimal FinalBalance = 0;
            if (NormalBalance == 1)
            {
                FinalBalance = (OpBal + sumDebit) - sumCredit;
            }
            else
            if (NormalBalance == 2)
            {
                FinalBalance = (OpBal + sumCredit) - sumDebit;
            }
            return FinalBalance;
        }
        public string TransactionExistAgainstGLCode(string GLCode)
        {


            SqlDataAdapter cmd = new SqlDataAdapter("Select * from GeneralLedger where code='" + GLCode + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            string TransactionExistAgainstGLCode = "No";
            if (dt.Rows.Count > 0)
            {
                TransactionExistAgainstGLCode = "Yes";

            }
            return TransactionExistAgainstGLCode;


        }
        public bool DeleteGLCode(string GLCode)
        {

            if (TransactionExistAgainstGLCode(GLCode) == "No")
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                SqlCommand cmdDelete1 = new SqlCommand("Delete  from GLCode where Code='" + GLCode + "'", con);
                cmdDelete1.ExecuteNonQuery();

                SqlCommand cmdDelete2 = new SqlCommand("Delete  from GLOpeningBalance where Code='" + GLCode + "'", con);
                cmdDelete2.ExecuteNonQuery();

                SqlCommand cmdDelete3 = new SqlCommand("Delete  from PartyCode where Code='" + GLCode + "'", con);
                cmdDelete3.ExecuteNonQuery();

                SqlCommand cmdDelete4 = new SqlCommand("Delete  from PartyOpBalance where Code='" + GLCode + "'", con);
                cmdDelete4.ExecuteNonQuery();
                con.Close();
                return true;
            }
            else
            {
                return false;
            }


        }
        public string DeleteGLCodeBank(string GLCode)
        {

            if (TransactionExistAgainstGLCode(GLCode) == "No")
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                SqlCommand cmdDelete1 = new SqlCommand("Delete  from GLCode where Code='" + GLCode + "'", con);
                cmdDelete1.ExecuteNonQuery();

                SqlCommand cmdDelete2 = new SqlCommand("Delete  from GLOpeningBalance where Code='" + GLCode + "'", con);
                cmdDelete2.ExecuteNonQuery();

                SqlCommand cmdDelete3 = new SqlCommand("Delete  from PartyCode where Code='" + GLCode + "'", con);
                cmdDelete3.ExecuteNonQuery();

                SqlCommand cmdDelete4 = new SqlCommand("Delete  from PartyOpBalance where Code='" + GLCode + "'", con);
                cmdDelete4.ExecuteNonQuery();

                SqlCommand cmdDelete5 = new SqlCommand("Delete  from BankCode where BankCode ='" + GLCode + "'", con);
                cmdDelete5.ExecuteNonQuery();
                con.Close();
                return "GL Head Deleted";
            }
            else
            {
                return "GL Head can not be deleted, transaction exist.";
            }


        }
        public string GLTitleAgainstCode(string GLCode)
        {


            SqlDataAdapter cmd = new SqlDataAdapter("Select Title  from GLCode where CompID='" + CompID + "' and code='" + GLCode + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            string GLTitle = "";
            if (dt.Rows.Count > 0)
            {
                GLTitle = Convert.ToString(dt.Rows[0]["Title"]);

            }


            return GLTitle;


        }
        public string GLCodeAgainstTitle2(string GLTitle)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("Select Code  from GLCode where  CompID='" + CompID + "' and Title='" + GLTitle + "' and lvl=5", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            string GLCode = "";
            if (dt.Rows.Count > 0)
            {
                GLCode = Convert.ToString(dt.Rows[0]["Code"]);
            }
            return GLCode;
        }
        public string GLTitleAgainstGLCode(string GLCode)
        {


            SqlDataAdapter cmd = new SqlDataAdapter("Select Title  from GLCode where CompId='" + CompID + "' and code='" + GLCode + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            string GLTitle = "";
            if (dt.Rows.Count > 0)
            {
                GLTitle = Convert.ToString(dt.Rows[0]["Title"]);

            }


            return GLTitle;


        }
        public string GLCodeAgainstGLTitle(string GLTitle)
        {

            string GLTitle1 = GLTitle;
            SqlDataAdapter cmd = new SqlDataAdapter("Select Code  from GLCode where lvl=5 and CompId='" + CompID + "' and Title='" + GLTitle1 + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            string GLCode = "";
            if (dt.Rows.Count > 0)
            {
                GLCode = Convert.ToString(dt.Rows[0]["Code"]);

            }


            return GLCode;


        }
        public void ClosingBalanceGLWithTransaction(string GLCode, SqlConnection connection, SqlTransaction trans)
        {

            Module7 objModule7 = new Module7();
            decimal OpBal = 0;
            var cmd = new SqlCommand("select IsNull(OpeningBal,0) as OpeningBal from GLOpeningBalance where   CompID='" + CompID + "' and code ='" + GLCode + "' and Date1='" + objModule7.StartDate() + "'", connection, trans);
            SqlDataAdapter adptr1 = new SqlDataAdapter(cmd);

            DataTable dt = new DataTable();
            adptr1.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                OpBal = Convert.ToDecimal(dt.Rows[0]["OpeningBal"]);

            }

            decimal sumDebit = 0;
            decimal sumCredit = 0;


            var cmd1 = new SqlCommand("select IsNull(sum(AmountDr),0) as Dr, IsNull( sum(AmountCr),0) as Cr from GeneralLedger where  CompID='" + CompID + "' and Code='" + GLCode + "' and dateDr>='" + objModule7.StartDate() + "' and dateDr<='" + objModule7.EndDate(DateTime.Today) + "'", connection, trans);
            var adptr2 = new SqlDataAdapter(cmd1);

            DataTable dt1 = new DataTable();
            adptr2.Fill(dt1);

            if (dt1.Rows.Count > 0)
            {
                sumDebit = Convert.ToDecimal(dt1.Rows[0]["Dr"]);
                sumCredit = Convert.ToDecimal(dt1.Rows[0]["Cr"]);
            }
            int NorBalance = 1;
            Module2 objModule2 = new Module2();
            NorBalance = objModule2.NormalBalanceGLWithTransaction(GLCode, connection, trans);
            decimal FinalBalance = 0;
            if (NorBalance == 1) { FinalBalance = (OpBal + sumDebit) - sumCredit; }
            else
            if (NorBalance == 2) { FinalBalance = (OpBal + sumCredit) - sumDebit; }


            SqlCommand cmd3 = new SqlCommand("update GLCode set Balance=" + FinalBalance + " where  CompID='" + CompID + "' and Code='" + GLCode + "'", connection, trans);
            cmd3.ExecuteNonQuery();


        }
        public void ClosingBalanceGL(string GLCode)
        {

            Module7 objModule7 = new Module7();
            decimal OpBal = 0;
            SqlDataAdapter cmd = new SqlDataAdapter("select IsNull(OpeningBal,0) as OpeningBal from GLOpeningBalance where   CompID='" + CompID + "' and code ='" + GLCode + "' and Date1='" + objModule7.StartDate() + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                OpBal = Convert.ToDecimal(dt.Rows[0]["OpeningBal"]);

            }

            decimal sumDebit = 0;
            decimal sumCredit = 0;


            SqlDataAdapter cmd1 = new SqlDataAdapter("select IsNull(sum(AmountDr),0) as Dr, IsNull( sum(AmountCr),0) as Cr from GeneralLedger where  CompID='" + CompID + "' and Code='" + GLCode + "' and dateDr>='" + objModule7.StartDate() + "' and dateDr<='" + objModule7.EndDate(DateTime.Today) + "'", con);
            DataTable dt1 = new DataTable();
            cmd1.Fill(dt1);

            if (dt1.Rows.Count > 0)
            {
                sumDebit = Convert.ToDecimal(dt1.Rows[0]["Dr"]);
                sumCredit = Convert.ToDecimal(dt1.Rows[0]["Cr"]);


            }
            int NorBalance = 1;
            Module2 objModule2 = new Module2();
            NorBalance = objModule2.NormalBalanceGL(GLCode);
            decimal FinalBalance = 0;
            if (NorBalance == 1) { FinalBalance = (OpBal + sumDebit) - sumCredit; }
            else
            if (NorBalance == 2) { FinalBalance = (OpBal + sumCredit) - sumDebit; }

            if (con.State==ConnectionState.Closed) { con.Open(); }
            SqlCommand cmd3 = new SqlCommand("update GLCode set Balance=" + FinalBalance + " where  CompID='" + CompID + "' and Code='" + GLCode + "'", con);
            cmd3.ExecuteNonQuery();
            con.Close();


        }
        public decimal GLClosingBalance(string GLCod, int NB, DateTime StartDate, DateTime EndDate)
        {
            string GLCode = GLCod;
            decimal OpBal = 0;
            decimal sumDebit = 0;
            decimal sumCredit = 0;
            decimal FinalBalance = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("select isnull(sum(OpeningBal),0) as OpeningBal from GLOpeningBalance where CompID='" + CompID + "' and code like '" + GLCode + "%' and Date1='" + objModule7.StartDate() + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                OpBal = Convert.ToDecimal(dt.Rows[0]["OpeningBal"]);

            }

            SqlDataAdapter cmd1 = new SqlDataAdapter("select isnull(sum(AmountDr),0) as AmountDr , isnull(sum(AmountCr),0) as AmountCr from GeneralLedger where CompID='" + CompID + "' and code like '" + GLCode + "%' and datedr>='" + objModule7.StartDate() + "' and datedr<='" + objModule7.EndDate(DateTime.Today) + "'", con);
            DataTable dt1 = new DataTable();
            cmd1.Fill(dt1);

            if (dt1.Rows.Count > 0)
            {
                sumDebit = Convert.ToDecimal(dt1.Rows[0]["AmountDr"]);
                sumCredit = Convert.ToDecimal(dt1.Rows[0]["AmountCr"]);
            }
            if (NB == 1) { FinalBalance = (OpBal + sumDebit) - sumCredit; }
            if (NB == 2) { FinalBalance = (OpBal + sumCredit) - sumDebit; }

            return FinalBalance;

        }
        public decimal GLOpeningBalance(string GLCode)
        {

            Module7 objModule7 = new Module7();
            decimal OpBal = 0;
            SqlDataAdapter cmd = new SqlDataAdapter("select IsNull(OpeningBal,0) as OpeningBal from GLOpeningBalance where   CompID='" + CompID + "' and code ='" + GLCode + "' and Date1='" + objModule7.StartDate() + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                OpBal = Convert.ToDecimal(dt.Rows[0]["OpeningBal"]);

            }

            return OpBal;

        }
    }
}