﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace WebPOS
{
    public class ModViewInventory
    {

        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);

       

        


        
        public decimal ItemQtyReceivedFromToNormal(decimal ItemCode,DateTime StartDate,DateTime EndDate)
        {

            decimal received = 0;
            Module7 objModule7 = new Module7();
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(sum(received),0) as received from Inventory where ICode=" + ItemCode + " and CompID='" + CompID + "' and Dat>='" + StartDate + "' and Dat<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                received = Convert.ToInt16(dt.Rows[0]["received"]);

            }

            return received;
        }
        public  Decimal UpToDateOpeningBalanceNormal(decimal ItemCode, DateTime EndDate)
        {
            string CompID = "01";
            Module7 obj = new Module7();
            Module7 objModule7 = new Module7();
            DateTime dat = objModule7.StartDateTwo(EndDate);
            decimal OpB = 0;
            SqlDataAdapter cmd = new SqlDataAdapter("select isnull(sum(Opening),0) as Opening from InventoryOpeningBalance where CompID='" + CompID + "' and ICode='" + ItemCode + "' and Dat='" + objModule7.StartDateTwo(EndDate) + "'", con);
            
            DataTable dt = new DataTable();
            cmd.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                OpB = Convert.ToInt32(dt.Rows[0]["Opening"]);

            }

            decimal Dr = 0;
            decimal Cr = 0;

            SqlDataAdapter cmd1 = new SqlDataAdapter("select isnull(sum(Received),0) as D,isnull(sum(Issued),0) as C from Inventory where  CompID='" + CompID + "' and ICode='" + ItemCode + "' and Dat>='" + dat + "' and Dat<'" + EndDate + "'", con);
            DataTable dt1 = new DataTable();
            cmd1.Fill(dt1);


            if (dt.Rows.Count > 0)
            {
                //if (dt1.Rows[0].IsNull = false) { }  
                Dr = Convert.ToDecimal(dt1.Rows[0]["D"]);
                Cr = Convert.ToDecimal(dt1.Rows[0]["C"]);
            }


            OpB = OpB + Dr - Cr;

            return OpB;
        }
        public decimal ItemQtyIssuedFromToNormal(decimal ItemCode, DateTime StartDate, DateTime EndDate)
        {

            decimal Issued = 0;
            Module7 objModule7 = new Module7();
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(sum(Issued),0) as Issued from Inventory where ICode=" + ItemCode + " and CompID='" + CompID + "' and Dat>='" + StartDate + "' and Dat<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                Issued = Convert.ToInt16(dt.Rows[0]["Issued"]);

            }

            return Issued;
        }
        public decimal StationWiseStockIssuedDuringTheYear(int StationID, int ItemCode)
        {

            decimal Issued = 0;
            Module7 objModule7 = new Module7();
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(sum(Issued),0) as Issued from Inventory where ICode=" + ItemCode + " and CompID='" + CompID + "' and StationID=" + StationID + " and Dat>='" + objModule7.StartDate() + "' and Dat<='" + objModule7.EndDate(DateTime.Today ) + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                Issued = Convert.ToInt16(dt.Rows[0]["Issued"]);

            }

            return Issued;
        }
        public decimal ClosingBalanceAgainstItemCode(decimal ItemCode)
        {

            decimal Qty = 0;
            Module7 objModule7 = new Module7();
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(sum(Qty),0) as Qty from InvCode where Code=" + ItemCode + " and CompID='" + CompID + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                Qty = Convert.ToInt16(dt.Rows[0]["Qty"]);

            }

            return Qty;
        }


    }
}