﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS
{
    public class ModGL
    {
        
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        public decimal UpToDateClosingBalanceGL_2(string Code,int NB, DateTime StartDate,DateTime EndDate)
        {

            Module7 objModule7 = new Module7();
            decimal OpBal = 0;
            //SqlDataAdapter cmd = new SqlDataAdapter("select isnull(sum(OpeningBal),0) as OpeningBal from GLOpeningBalance where   CompID='" + CompID + "' and code like '" + Code + "%' and Date1='" + objModule7.StartDateTwo(StartDate) + "'", con);
            SqlDataAdapter cmd = new SqlDataAdapter("select OpeningBal as OpeningBal from GLOpeningBalance where   CompID='" + CompID + "' and code ='" + Code + "' and Date1='" + objModule7.StartDateTwo(StartDate) + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            
            if (dt.Rows.Count > 0)
            {
                OpBal = Convert.ToDecimal(dt.Rows[0]["OpeningBal"]);

            }

            decimal sumDebit = 0;
            decimal sumCredit = 0;

            //SqlDataAdapter cmd1 = new SqlDataAdapter("select isnull(sum(AmountDr),0) as Dr, isnull( sum(AmountCr),0) as Cr from GeneralLedger where  CompID='" + CompID + "' and code like '" + Code + "%' and datedr>='" + objModule7.StartDateTwo(StartDate) + "' and datedr<='" + StartDate + "'", con);
            SqlDataAdapter cmd1 = new SqlDataAdapter("select isnull(sum(AmountDr),0) as Dr, isnull( sum(AmountCr),0) as Cr from GeneralLedger where  CompID='" + CompID + "'", con);
            DataTable dt1 = new DataTable();
            cmd1.Fill(dt1);

            if (dt1.Rows.Count > 0)
            {
                sumDebit = Convert.ToDecimal (dt1.Rows[0]["Dr"]);
                sumCredit = Convert.ToDecimal(dt1.Rows[0]["Dr"]);


            }

            decimal FinalBalance=0;
            if (NB == 1) {FinalBalance = (OpBal + sumDebit) - sumCredit;}
            else    
            if (NB == 2) {FinalBalance = (OpBal + sumCredit) - sumDebit;}

            return FinalBalance;


        }
        public decimal UpToDateDebitGL(string GLCode, DateTime StartDate, DateTime EndDate)
        {

            
            
            SqlDataAdapter cmd = new SqlDataAdapter("select isnull(sum(AmountDr),0) as Dr, isnull(sum(AmountCr),0) as Cr from GeneralLedger where  CompID='" + CompID + "' and code='" + GLCode + "' and datedr>='" + StartDate + "' and datedr<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal UpToDateDebitG = 0;
            if (dt.Rows.Count > 0)
            {
                UpToDateDebitG = Convert.ToDecimal(dt.Rows[0]["Dr"]);

            }


            return UpToDateDebitG;


        }
        public decimal UpToDateCreditGL(string GLCode, DateTime StartDate, DateTime EndDate)
        {

            

            SqlDataAdapter cmd = new SqlDataAdapter("select isnull(sum(AmountDr),0) as Dr, isnull(sum(AmountCr),0) as Cr from GeneralLedger where  CompID='" + CompID + "' and code='" + GLCode + "' and datedr>='" + StartDate + "' and datedr<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal UpToDateCreditGL = 0;
            if (dt.Rows.Count > 0)
            {
                UpToDateCreditGL = Convert.ToDecimal(dt.Rows[0]["Cr"]);

            }


            return UpToDateCreditGL;


        }
        public string BankAccountNoAgainstBankCode(string BankCode)
        {
            string AccountNo = "";
            SqlDataAdapter cmd = new SqlDataAdapter("select AccountNo from BankCode where  CompID='" + CompID + "' and BankCode='" + BankCode + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                AccountNo =Convert.ToString(dt.Rows[0]["AccountNo"]);
            }
            return AccountNo;
        }

        public string GLCodeAgainstGLLockName(string GLLockName)
        {
            string GLCode = "";
            SqlDataAdapter cmd = new SqlDataAdapter("select GLCode from GLCodeLock where  GLTitle='" + GLLockName + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                GLCode = Convert.ToString(dt.Rows[0]["GLCode"]);
            }
            return GLCode;
        }
        public string GLTitleAgainstCode(string GLCode)
        {
            string GLTitle = "";
            SqlDataAdapter cmd = new SqlDataAdapter("select Title from GLCode where  CompID='01' and Code='" + GLCode + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                GLTitle = Convert.ToString(dt.Rows[0]["Title"]);
            }
            return GLTitle;
        }
        public Int32 GLNorBalanceAgainstCode(string GLCode)
        {
            Int32 GLNorBalance = 1;
            SqlDataAdapter cmd = new SqlDataAdapter("select NorBalance from GLCode where  CompID='01' and Code='" + GLCode + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                GLNorBalance = Convert.ToInt32(dt.Rows[0]["NorBalance"]);
            }
            return GLNorBalance;
        }

    }
}