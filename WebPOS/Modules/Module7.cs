﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace WebPOS
{


    public class Module7
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        public DateTime StartDateTwo(DateTime CurrentDate)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select Start_Date,Start_Month from YearStartEnd where CompID='" + CompID + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int Start_Date = 1;
            int Start_Month = 7;


            if (dt.Rows.Count > 0)
            {
                Start_Date = Convert.ToInt32(dt.Rows[0]["Start_Date"]);
                Start_Month = Convert.ToInt32(dt.Rows[0]["Start_Month"]);
            }

            int yea = CurrentDate.Year;
            if (CurrentDate.Month < 7)
            {
                yea = yea - 1;
            }

            DateTime RetValu = Convert.ToDateTime(Start_Month + "/" + Start_Date + "/" + yea);
            // string StartDateTwo = "07" + "/" + "01" + "/" + yea;
            return RetValu;
        }

        public DateTime StartDate()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select Start_Date,Start_Month from YearStartEnd where CompID='" + CompID + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int Start_Date = 1;
            int Start_Month = 7;


            if (dt.Rows.Count > 0)
            {
                Start_Date = Convert.ToInt32(dt.Rows[0]["Start_Date"]);
                Start_Month = Convert.ToInt32(dt.Rows[0]["Start_Month"]);
            }

            int yea = DateTime.Today.Year;
            if (DateTime.Today.Month < 7)
            {
                yea = yea - 1;
            }

            DateTime RetValu = Convert.ToDateTime(Start_Month + "/" + Start_Date + "/" + yea);
            // string StartDateTwo = "07" + "/" + "01" + "/" + yea;
            return RetValu;
        }


        public string StartDateWithOutYear()
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select Start_Date,Start_Month from YearStartEnd where CompID='" + CompID + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int Start_Date = 1;
            int Start_Month = 7;

            if (dt.Rows.Count > 0)
            {
                Start_Date = Convert.ToInt32(dt.Rows[0]["Start_Date"]);
                Start_Month = Convert.ToInt32(dt.Rows[0]["Start_Month"]);
            }

            string RetValu = Start_Month + "/" + Start_Date;
            return RetValu;
        }

        internal DateTime StartDateTwo(string v)
        {
            throw new NotImplementedException();
        }

        public DateTime EndDate(DateTime CurrentDate)
        {
            SqlDataAdapter cmd = new SqlDataAdapter("select End_Date,End_Month from YearStartEnd where CompID='" + CompID + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            int End_Date = 30;
            int End_Month = 6;

            CurrentDate = DateTime.Today;

            if (dt.Rows.Count > 0)
            {
                End_Date = Convert.ToInt32(dt.Rows[0]["End_Date"]);
                End_Month = Convert.ToInt32(dt.Rows[0]["End_Month"]);
            }

            int yea = CurrentDate.Year;
            if (CurrentDate.Month > 6)
            {
                yea = yea + 1;
            }

            DateTime RetValu = Convert.ToDateTime(End_Month + "/" + End_Date + "/" + yea);

            return RetValu;

        }


    }
}