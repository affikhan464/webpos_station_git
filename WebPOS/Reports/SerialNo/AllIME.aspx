﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="AllIME.aspx.cs" Inherits="WebPOS.Reports.SaleReports.AllIME" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  
      
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

				<div class="searchAppSection reports">
				<div class="contentContainer">
					<h2>All <span class="IMEType">Sold</span> Serial Number</h2>
					<div class="BMSearchWrapper row">
                        <div class="col col-12 col-sm-6 col-md-3">
                            	<select id="SoldTypeDD" class="dropdown">
									<option value="1">Sold IME</option>
									<option value="0">UnSold IME</option>
						        </select>
						</div>
                        <div class="col col-12 col-sm-6 col-md-3">
                              
                            	<select id="PageSizeDD" class="dropdown">
									<option value="100">Page Size: 100</option>
									<option value="250">Page Size: 250</option>
									<option value="500">Page Size: 500</option>
									<option value="1000">Page Size: 1000</option>
									<option value="2000">Page Size: 2000</option>
						        </select>
						</div>
						<div class="col col-12 col-sm-6 col-md-3">
						    <a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-spinner"></i> Get Record</a>
					    </div>
						
                        <div class="col col-12 col-sm-6 col-md-3">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
						</div>
					</div><!-- searchAppSection -->
				</div>
			</div><!-- featuredSection -->
	
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="DaySaleGroup2 reports recommendedSection allItemsView container-fluid">


								<div class="itemsHeader">
									<div class="seven phCol">
										<a href="javascript:void(0)"> 
										Sr #
										</a>
									</div>
									<div class="sixteen phCol">
										<a href="javascript:void(0)"> 
										<span class="dateType">Date Sold</span>
                                          
										</a>
									</div>
                                    <div class="thirteen phCol">
										<a href="javascript:void(0)"> 
											Item Name
										</a>
									</div>
									<div class="sixteen phCol">                                                                    
										<a href="javascript:void(0)"> 
											<span class="clientType">Client Name</span> 
										</a>
									</div>
                                    <div class="thirteen phCol">
										<a href="javascript:void(0)"> 
											  Bill #
										</a>
									</div>
									<div class="sixteen phCol">
										<a href="javascript:void(0)"> 
											Item Serial No/IME
										</a>
									</div>
								
						
								
								</div><!-- itemsHeader -->	
                         <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
								<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow'>
								<div class='seven'><span></span></div>
								<div class='seven'><span></span></div>
								<div class='thirteen'><span></span></div>
								<div class='thirteen'><span></span></div>
								<div class='thirteen'><span></span></div>
                                <div class='sixteen'><input  disabled="disabled"  name='totalQuantity'/></div>
							</div>
						</div>
							
						</div>
                    <div class="container-fluid"><a class="loadMoreBtn disableClick redBtn w-100 mt-2 mb-2 float-left"><i class="fas fa-spinner"></i> Load More <span class="pageSizeText">100</span> Records</a></div>
					</div><!-- recommendedSection -->


	
				</div>
   </asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
     <script src="<%=ResolveClientUrl("/Script/Autocomplete.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>

   <script type="text/javascript">

       var counter = 0;
      
       $(document).ready(function () {
           $(".loader").show();
               
                resizeTable();
                initializeDropdown();
                var soldType = getQueryString('sold', window.location.href) == null ? 1 : getQueryString('sold', window.location.href);
                if (soldType!=1) 
                    $('#SoldTypeDD .dd-option-value[value=0]').parent().click()
                
                getReport();

       });
       function getQueryString(name, url) {
           if (!url) url = window.location.href;
           name = name.replace(/[\[\]]/g, '\\$&');
           var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
               results = regex.exec(url);
           if (!results) return null;
           if (!results[2]) return '';
           return decodeURIComponent(results[2].replace(/\+/g, ' '));
       }
            function initializeDropdown() {
                $("#PageSizeDD").ddslick({
                    width: "100%",
                    onSelected: function (data) {
                        $(".pageSizeText").text(data.selectedData.value);
                        clearInvoice();
                    }
                });
                $("#SoldTypeDD").ddslick({
                    width:"100%",
                    onSelected: function (data) {
                        clearInvoice();
                        if (data.selectedData.value == "1")
                        {
                            $(".IMEType").text("Sold");
                            $(".dateType").text("Date Sold");
                            $(".clientType").text("Client Name");
                        }
                        else
                        {
                            $(".dateType").text("Purchase Date");
                            $(".clientType").text("Vendor Name");
                            $(".IMEType").text("Unsold");
                        }

                    }
                });
            }
            $(window).resize(function () {
                resizeTable();
              
            });

          
            $("#toTxbx").on("keypress", function (e) {

                if (e.which == 13) {
                    $("#currentPage").remove();
                    $("<input hidden id='currentPage' value='0' />").appendTo("body");
                    clearInvoice();
                    counter = 0;
                    getReport();
                    
                }
            });
            $("#searchBtn").on("click", function (e) {
                counter = 0;
                $("#currentPage").remove();
                $("<input hidden id='currentPage' value='0' />").appendTo("body");
                clearInvoice();
                $(".loader").show();
                getReport();
             
            });
            $(".loadMoreBtn").on("click", function (e) {
                $(".loadMoreBtn i").addClass("fa-spin");
                var pageSize = $("#PageSizeDD .dd-selected-value").val();
                var currentPage = Number($("#currentPage").val())+1;
                counter = pageSize * currentPage;
                
                $("#currentPage").val(currentPage);
               
               
                getReport();
             
            });
            function rerenderSerialNumber() {
                var rows = $("#myTable tbody tr .SNo");
                var sno = 0;
                rows.each(function (index, element) {
                    sno += 1;
                    $(element).text(sno);
                })
            }

            window.smoothScroll = function (target) {
                var scrollContainer = target;
                do { //find scroll container
                    scrollContainer = scrollContainer.parentNode;
                    if (!scrollContainer) return;
                    scrollContainer.scrollTop += 1;
                } while (scrollContainer.scrollTop == 0);

                var targetY = 0;
                do { //find the top of target relatively to the container
                    if (target == scrollContainer) break;
                    targetY += target.offsetTop;
                } while (target = target.offsetParent);

                scroll = function (c, a, b, i) {
                    i++; if (i > 30) return;
                    c.scrollTop =( a + (b - a) / 30 * i)-201;
                    setTimeout(function () { scroll(c, a, b, i); }, 20);
                }
                // start scrolling
                scroll(scrollContainer, scrollContainer.scrollTop, targetY, 0);
            }
            function getReport() { 

                var pageSize = $("#PageSizeDD .dd-selected-value").val() == null || $("#PageSizeDD .dd-selected-value").val() == undefined ? 100 : $("#PageSizeDD .dd-selected-value").val();
                var soldType = $("#SoldTypeDD .dd-selected-value").val() == null || $("#SoldTypeDD .dd-selected-value").val() == undefined ? 1 : $("#SoldTypeDD .dd-selected-value").val();

                var currentPage = 0;
                if ($("#currentPage").val() == "" || $("#currentPage").val() == undefined) {
                    currentPage = 0;
                    $("#currentPage").remove();
                    $("<input hidden id='currentPage' value='0' />").appendTo("body");
                } else{
                    currentPage = $("#currentPage").val();
                }
                $.ajax({
                    url: '/Reports/SerialNo/AllIME.aspx/getReport',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ "page": Number(currentPage), "pageSize": Number(pageSize), "soldType": soldType }),
                    success: function (response) {
                      

                        if (response.d.Success) {
                            if (response.d.ListofInvoices.length>0) {
                                var firstItem = counter+1;
                            
                            for (var i = 0; i < response.d.ListofInvoices.length; i++) {
                                counter++;
                                $("<div id='"+counter+"' class='itemRow newRow'>"+
                                   
                                    "<div class='seven name'><span>" + counter + "</span></div>" +
                                    "<div class='sixteen name'><span>" + response.d.ListofInvoices[i].Date + "</span></div>" +
                                    "<div class='thirteen name'> <input value='" + response.d.ListofInvoices[i].Description + "' disabled /></div>" +
                                    "<div class='sixteen name'> <input value='" + response.d.ListofInvoices[i].PartyName + "' disabled /></div>" +
                                    "<div class='thirteen name'> <input value='" + response.d.ListofInvoices[i].InvoiceNumber + "' disabled /></div>" +
                                    "<div class='sixteen'> <input value='" + response.d.ListofInvoices[i].IME + "' disabled /></div>" +
                                     "</div>").appendTo(".dayCashAndCreditSale .itemsSection");


                            }
                            calculateData();
                            $(".loader").hide();
                            $(".loadMoreBtn i").removeClass("fa-spin");
                            if (response.d.ListofInvoices.length == pageSize)
                                $(".loadMoreBtn").removeClass("disableClick");
                            else
                                $(".loadMoreBtn").addClass("disableClick");

                            smoothScroll(document.getElementById(firstItem));
                          
                            } else if (response.d.ListofInvoices.length == 0 && currentPage == "0") {
                                $(".loadMoreBtn i").removeClass("fa-spin");
                                $(".loader").hide(); swal({
                                    title: "No Result Found ", type:'error',
                                    text: "No Result Found!"
                                });
                            }
                         
                        }
                        else {
                            $(".loader").hide();
                            swal({
                                title: "there is some error",
                                text: response.d.Message
                            });
                        }

                    },
                    error: function (error) { swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                    }
                });
            }
            function clearInvoice() {
                $(".loadMoreBtn").addClass("disableClick");
                $("#currentPage").remove();
                $("<input hidden id='currentPage' value='0' />").appendTo("body");
                $(".itemsSection .itemRow").remove();
            }
            function calculateData() {
                var itemRows = $(".itemsSection .itemRow");
                var sum = 0;

                itemRows.each(function (index, element) {
                    sum = index + 1;
                });
                
                $("[name=totalQuantity]").val('');
                $("[name=totalQuantity").val(sum);

            }
   </script>
</asp:Content>
