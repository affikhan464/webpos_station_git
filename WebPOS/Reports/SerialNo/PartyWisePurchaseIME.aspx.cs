﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class PartyWisePurchaseIME : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string partyCode)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, partyCode);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, string partyCode)
        {

            SqlCommand cmd = new SqlCommand(@"
                SELECT t.Date1,VoucherNo,PartyCode.Name as 'PartyName',t.Description as 'Transaction',InvCode.Description, IME                                  
                FROM  InventorySerialNoFinal as t
                JOIN PartyCode on t.PartyCode=PartyCode.Code 
                JOIN InvCode on t.ItemCode=InvCode.Code 
                WHERE PartyCode = '" + partyCode + "' and t.CompID = '" + CompID + "' and t.Date1 >= '" + StartDate + "' and t.Date1 <= '" + EndDate + "' order by t.Date1 asc", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.Date = Convert.ToDateTime(dt.Rows[i]["Date1"]).ToShortDateString();
                invoice.InvoiceNumber = Convert.ToString(dt.Rows[i]["VoucherNo"]);
                invoice.PartyName = Convert.ToString(dt.Rows[i]["PartyName"]);
                invoice.Transaction = Convert.ToString(dt.Rows[i]["Transaction"]);
                invoice.IME = Convert.ToString(dt.Rows[i]["IME"]);
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]).Trim();

                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {
            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
            //var itemCode = ItemCode.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/ItemWiseSaleDetailCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&itemCode=" + itemCode);

        }
    }
}