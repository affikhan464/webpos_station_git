﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class FindIME : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string IME, string MatchIME, int page = 0, int pageSize = 100)
        {
            try
            {

                var model = GetTheReport(IME, MatchIME, page, pageSize);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel DeleteIME(string IME, string invoiceNumber)
        {
            try
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
                SqlCommand cmd = new SqlCommand(@"
                        Delete From InventorySerialNoFinal 
                        WHERE IME='" + IME + "' and VoucherNo='" + invoiceNumber + "'", con);
                cmd.ExecuteNonQuery();
                con.Close();

                return new BaseModel { Success = true };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }
        public static List<InvoiceViewModel> GetTheReport(string IME, string MatchIME, int page, int pageSize)
        {
            page += 1;
            var AndIME = "";
            if (MatchIME == MatchIMEType.Left)
            {
                AndIME = " and t.IME like '" + IME + "%' ";
            }
            else if (MatchIME == MatchIMEType.Right)
            {
                AndIME = " and t.IME like '%" + IME + "'";
            }
            else if (MatchIME == MatchIMEType.AnyWhere)
            {
                AndIME = " and t.IME like '%" + IME + "%'";
            }
            else if (MatchIME == MatchIMEType.ExactMatch)
            {
                AndIME = " and t.IME like '" + IME + "'";
            }
            SqlCommand cmd = new SqlCommand(@"
                  SELECT * FROM
                  (
                      SELECT TOP " + pageSize + @" * FROM
                      (
                          SELECT TOP " + (page * pageSize) + @" t.ItemCode,InvCode.Description,BrandName.Name as 'BrandName',InvCode.qty,t.Date1,t.Description as 'Transaction',PartyCode.Name as 'PartyName', VoucherNo,t.Price_Cost, IME
                          FROM InventorySerialNoFinal as t
                  
                  		JOIN PartyCode on t.PartyCode=PartyCode.Code 
                  		JOIN InvCode on t.ItemCode=InvCode.Code 
                  		JOIN BrandName on InvCode.Brand=BrandName.Code 
                          WHERE  t.CompID = '" + CompID + "' " + AndIME + @"
                          ORDER BY t.Date1 ASC
                      ) AS t1 
                      ORDER BY t1.Date1 DESC
                  ) AS t2 
                  ORDER BY t2.Date1 ASC", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.InvoiceNumber = Convert.ToString(dt.Rows[i]["VoucherNo"]);
                invoice.Date = Convert.ToDateTime(dt.Rows[i]["Date1"]).ToShortDateString();
                invoice.PartyName = Convert.ToString(dt.Rows[i]["PartyName"]);
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                invoice.BrandName = Convert.ToString(dt.Rows[i]["BrandName"]);
                invoice.Quantity = Convert.ToString(dt.Rows[i]["qty"]);
                invoice.ItemCode = Convert.ToString(dt.Rows[i]["ItemCode"]);
                invoice.Transaction = Convert.ToString(dt.Rows[i]["Transaction"]);
                invoice.Price = Convert.ToString(dt.Rows[i]["Price_Cost"]);
                invoice.IME = Convert.ToString(dt.Rows[i]["IME"]);
                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {
            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
            //var itemCode = ItemCode.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/ItemWiseSaleDetailCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&itemCode=" + itemCode);

        }
    }
}