﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class ItemWiseSoldSerialNo : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string itemCode)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, itemCode);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, string itemCode)
        {

            //SqlCommand cmd = new SqlCommand(@"
            //            SELECT t.Date1,IME,PartyCode.Name,VoucherNo
            //            FROM  InventorySerialNoFinal as t
            //            JOIN PartyCode on t.PartyCode=PartyCode.Code  
            //            WHERE t.ItemCode = '" + itemCode + "' and  t.Date1 = (SELECT MAX(Date1) FROM InventorySerialNoFinal as n where t.IME=n.IME  and sold=1) and  sold =1 and t.CompID='" + CompID + "' and t.Date1>='" + StartDate + "' and t.Date1<='" + EndDate + "'", con);
            SqlCommand cmd = new SqlCommand(@" Select
                                                InventorySerialNoFinal.ItemCode,
                                                Date1,
                                                IME,
                                                PartyCode.Name,
                                                VoucherNo,
                                                InventorySerialNoFinal.Description as Transa,
                                                invcode.Description as ItemName,
                                                InventorySerialNoFinal.Price_Cost as Cost
                                            from InventorySerialNoFinal
                                                    inner join PartyCode on PartyCode.Code=InventorySerialNoFinal.PartyCode
                                                    inner join invcode on invcode.code=InventorySerialNoFinal.ItemCode
                                                    
                                            where 
                                                    InventorySerialNoFinal.ItemCode= '" + itemCode + @"' and
                                                    InventorySerialNoFinal.sold =1 and 
                                                    InventorySerialNoFinal.CompID='" + CompID + @"' and
                                                    Date1>='" + StartDate + @"' and 
                                                    Date1<='" + EndDate + "'", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);

            ModItem objModItem = new ModItem();

            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();
                invoice.ItemCode= Convert.ToString(dt.Rows[i]["ItemCode"]);
                invoice.Description= Convert.ToString(dt.Rows[i]["ItemName"]);
                invoice.Date = Convert.ToDateTime(dt.Rows[i]["Date1"]).ToShortDateString();
                invoice.DepartmentName = Convert.ToString(dt.Rows[i]["Transa"]);
                invoice.PartyName = Convert.ToString(dt.Rows[i]["Name"]);
                invoice.InvoiceNumber = Convert.ToString(dt.Rows[i]["VoucherNo"]);
                invoice.Rate = Convert.ToString(dt.Rows[i]["Cost"]);
                invoice.IME = Convert.ToString(dt.Rows[i]["IME"]);
                invoice.BrandName =objModItem.BrandNameAgainstBrandCode( objModItem.BrandCodeAgainstItemCode(Convert.ToInt32( dt.Rows[i]["ItemCode"])));
                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            var itemCode = ItemCode.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/ItemWiseSaleDetailCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&itemCode=" + itemCode);

        }
    }
}