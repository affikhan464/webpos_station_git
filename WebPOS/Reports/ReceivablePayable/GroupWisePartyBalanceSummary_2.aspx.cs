﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class GroupWisePartyBalanceSummary_2 : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport( string PartyGroupID)
        {
            try
            {
                //var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      //System.Globalization.CultureInfo.InvariantCulture);
                //var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                   // System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(PartyGroupID);
                return new BaseModel { Success = true, PrintReport = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }


        public static List<PrintingTable> GetTheReport(String PartyGroupID)
        {

            var model = new List<PrintingTable>();

            ModItem objModItem = new ModItem();
            ModGLCode objModGLCode = new ModGLCode();
            int PartyGID = Convert.ToInt32(PartyGroupID);
            SqlCommand cmd = new SqlCommand("Select PartyCode.Code, PartyCode.Name, PartyCode.Address, PartyCode.Phone, PartyCode.Mobile, PartyCode.Balance,PartyCode.NorBalance,PartyCode.OwnerName,PartyCode.Mobile from PartyCode where PartyCode.CompID = '01' and PartyCode.bALANCE <> 0 AND GroupID = " + PartyGID + " order by PartyCode.Name", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            var dt = new DataTable();
            adpt.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var PrintingT = new PrintingTable();
                var NorBalance= Convert.ToInt32 (dt.Rows[i]["NorBalance"]);
                var PartyCode= Convert.ToString(dt.Rows[i]["Code"]);
                var PartyName = Convert.ToString(dt.Rows[i]["Name"]);
                var Address = Convert.ToString(dt.Rows[i]["Address"]);
                var Owner = Convert.ToString(dt.Rows[i]["OwnerName"]);
                var Phone = Convert.ToString(dt.Rows[i]["Phone"]) + "/" + Convert.ToString(dt.Rows[i]["Mobile"]);
                var Balance = Convert.ToString(dt.Rows[i]["Balance"]);

                PrintingT.Text_1 = PartyCode.ToString();
                PrintingT.Text_2 = PartyName.ToString();
                PrintingT.Text_3 = Owner.ToString();
                PrintingT.Text_4 = Phone.ToString();
                PrintingT.Text_5 = Balance.ToString();
                
                    model.Add(PrintingT);
            }





            return model;
        }

        protected void BtnClick(object sender, EventArgs e)
        {
            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
           // var brandCode = "";// brandCodeTxbx.Text;
           // Response.Redirect("~/Reports/SaleReports/CReports/BrandWiseSaleSummaryCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&brandCode=" + brandCode);
        }
    }
}