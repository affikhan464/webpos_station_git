﻿<%@ Page Language="C#" Title="" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="frmPartyLedgerSlim.aspx.cs" Inherits="WebPOS.frmPartyLedgerSlim" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <%--<script type="text/javascript">
        $(function () {
            $('#<%= txtPartyName.ClientID %>').autocomplete({
                 source: function (request, response) {
                     $.ajax({

                         url: "/WebService1.asmx/GetPartyNames",
                         data: "{ 'PName': '" + request.term + "'}",
                         type: "POST",
                         dataType: "json",
                         contentType: "application/json;charset=utf-8",
                         success: function (result) {
                             response(result.d);
                         },
                         error: function (result) {
                             alert('There is Problem processing your request');
                         }
                     });
                 }
             });
         });

    </script>--%>
    <link href="../../css/allitems.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    
    <br /><h2 class="heading" >Party Ledger</h2><br />
    
           
    <div style ="width:100%;height:100%;text-align:center; vertical-align:middle">

        
            
                    <asp:Label ID="Label1" runat="server" Text="Start Date"></asp:Label>
                    <asp:TextBox ID="StartDate" CssClass="datetimepicker"   runat="server" style="width:100px; text-align:center;" ></asp:TextBox>
                    <asp:Label ID="Label2" runat="server" Text="End Date"></asp:Label>
                    <asp:TextBox ID="txtEndDate" CssClass="datetimepicker"   runat="server" style="width:100px; text-align:center;"></asp:TextBox>
                    <asp:Label ID="Label3" runat="server" Text="Select Party"></asp:Label>
                    <asp:TextBox ID="txtPartyName" data-id="PartyTextbox" data-type="Party" data-function="GetParty" CssClass="PartyBox clearable PartySearchBox autocomplete" style="width:285px;" runat="server"></asp:TextBox>
                     <%--<input id="PartySearchBox" data-id="PartyTextbox" data-type="Party" data-function="GetParty" class="clearable PartySearchBox autocomplete" name="PartyName" type="text" placeholder="Enter a party name" />--%>
                    <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
                

</div> 
<div style="width:100%">
        
               
        
  

            <br /><br />



    <center>
        <asp:GridView CssClass="grid" ID="GridView1"  AutoGenerateColumns="False" HeaderStyle-BackColor ="WhiteSmoke"   AlternatingRowStyle-BackColor ="WhiteSmoke"
      runat="server" OnRowDataBound="GridView1_RowDataBound" ShowFooter="True" FooterStyle-BackColor="Wheat" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical">
<AlternatingRowStyle BackColor="#DCDCDC"></AlternatingRowStyle>
      
            <Columns>

        <asp:BoundField HeaderText="Date" DataField="Date" DataFormatString="{0:d MMM yyyy}" ReadOnly="true"  >  
                <ItemStyle Width="160px" HorizontalAlign="Left"/>
                </asp:BoundField>
        <asp:BoundField HeaderText="Description" DataField="Description" ReadOnly="true"  >  
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
        <asp:BoundField HeaderText="Debit" DataField="Debit" ReadOnly="true"  >  
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
        <asp:BoundField HeaderText="Credit" DataField="Credit" ReadOnly="true"  >  
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
        <asp:BoundField HeaderText="Balance" DataField="Balance" ReadOnly="true"  >
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
        <asp:BoundField HeaderText="DescriptionOfBill" DataField="DescriptionOfBill" ReadOnly="true" Visible="False"  />  
        <asp:BoundField HeaderText="BillNo" DataField="BillNo" ReadOnly="true" Visible="False"  />  
      </Columns>

      <FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>

      <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White"></HeaderStyle>
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#0000A9" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#000065" />
      </asp:GridView>


 </div>
    <script src="../../Script/jquery.min.js"></script>
    <script src="../../Script/jquery-ui.min.js"></script>
    <script src="../../Script/Autocomplete.js"></script>
 </asp:Content>
