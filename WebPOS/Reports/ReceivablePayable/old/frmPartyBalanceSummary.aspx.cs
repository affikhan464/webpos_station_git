﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebPOS.Reports.ReceivablePayable
{
    public partial class frmPartyBalanceSummary : System.Web.UI.Page
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            LoadReport();
        }
        void LoadReport()
        {


            if (rdbClient.Checked == true)
            {
                SqlCommand cmd = new SqlCommand("Select PartyCode.Code , PartyCode.Name  ,  PartyCode.NorBalance  ,Convert(numeric(18,2), PartyCode.Balance)as Balance   from PartyCode  where  CompID='" + CompID + "' and  PartyCode.NorBalance=1 and  PartyCode.Balance <> 0  order by PartyCode.Name", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataSet dset = new DataSet();
                adpt.Fill(dset);
                GridView1.DataSource = dset;
                GridView1.DataBind();
                //GridView1.DataSource = dset;
                //GridView1.DataBind();
            }
            else
            if (rdbSupplyer.Checked == true)
            {
                SqlCommand cmd = new SqlCommand("Select PartyCode.Code , PartyCode.Name  ,  PartyCode.NorBalance  , Convert(numeric(18,2), PartyCode.Balance)as Balance from PartyCode  where  CompID='" + CompID + "' and  PartyCode.NorBalance=2 and  PartyCode.Balance <> 0  order by PartyCode.Name", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataSet dset = new DataSet();
                adpt.Fill(dset);
                GridView1.DataSource = dset;
                GridView1.DataBind();
                //GridView1.DataSource = dset;
                //GridView1.DataBind();

            }
            else
                if (rdbAll.Checked == true)
            {
                SqlCommand cmd = new SqlCommand("Select PartyCode.Code , PartyCode.Name  ,  PartyCode.NorBalance  , Convert(numeric(18,2), PartyCode.Balance)as Balance from PartyCode  where  CompID='" + CompID + "' and  PartyCode.Balance <> 0  order by PartyCode.Name", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataSet dset = new DataSet();
                adpt.Fill(dset);
                GridView1.DataSource = dset;
                GridView1.DataBind();
                //GridView1.DataSource = dset;
                //GridView1.DataBind();

            }
        }

        public decimal Receivable(decimal Bal, int NorBalance)

        {
            decimal Receivable = 0;

            if (NorBalance == 1 && Bal > 0)
            {
                Receivable = Bal;
            }
            else
            if (NorBalance == 2 && Bal < 0)
            {
                Receivable = (Bal * -1);
            }

            return Receivable;
        }

        public decimal Payable(decimal Bal, int NorBalance)
        {
            decimal Payable = 0;

            if (NorBalance == 2 && Bal > 0)
            {
                Payable = Bal;
            }
            else
                if (NorBalance == 1 && Bal < 0)
            {
                Payable = (Bal * -1);
            }
            return Payable;
        }

        public string Nature(int NorBalance)
        {
            string Nature = "";

            if (NorBalance == 1)
            {
                Nature = "Client";
            }
            else
                if (NorBalance == 2)
            {
                Nature = "Supplyer";
            }
            return Nature;
        }
        decimal TotDr = 0;
        decimal TotCr = 0;
        int counter = 1;
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (counter < 3) { 
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblDr = (Label)e.Row.FindControl("lblDebit");
                TotDr += Convert.ToDecimal(lblDr.Text);

                //TotDr += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "lblDebit"));

                Label lblCr = (Label)e.Row.FindControl("lblCredit");
                TotCr += Convert.ToDecimal(lblCr.Text);
                //lblDr.Text="";
                //lblCr.Text = "";
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {




                e.Row.Cells[0].Text = "Totals:";
                // for the Footer, display the running totals
                e.Row.Cells[3].Text = TotDr.ToString();
                e.Row.Cells[4].Text = TotCr.ToString();


                e.Row.Cells[3].HorizontalAlign = e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[4].HorizontalAlign = e.Row.Cells[4].HorizontalAlign = HorizontalAlign.Right;


                e.Row.Font.Bold = true;
            }
            //  }
        }





    }


}
