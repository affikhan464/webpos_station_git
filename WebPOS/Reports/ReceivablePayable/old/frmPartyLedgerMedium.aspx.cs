﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebPOS.Reports.ReceivablePayable
{
    public partial class frmPartyLedgerMedium : System.Web.UI.Page
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                try
                {
                    LoadPartyList();
                    StartDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                    txtEndDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                }
                catch (Exception ex)
                {
                    Response.Write("error  = " + ex.Message);

                }

            }


        }








        Decimal PartyOpBal(string PartyCode, DateTime StartDate, int NorBalance)
        {
            Module7 obj = new Module7();
            DateTime dat = obj.StartDateTwo(StartDate);
            decimal OpB = 0;
            SqlDataAdapter cmd = new SqlDataAdapter("select OpBalance from PartyOpBalance where  CompID='" + CompID + "' and Code='" + PartyCode + "' and Dat='" + dat + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                OpB = Convert.ToInt32(dt.Rows[0]["OpBalance"]);

            }

            //'''''''''''''''''''''''''''''''''''''
            decimal Dr = 0;
            decimal Cr = 0;

            SqlDataAdapter cmd1 = new SqlDataAdapter("select isnull(sum(AmountDr),0) as D,isnull(sum(AmountCr),0) as C from PartiesLedger where  CompID='" + CompID + "' and code='" + PartyCode + "' and Datedr>='" + dat + "' and datedr<'" + StartDate + "'", con);
            DataTable dt1 = new DataTable();
            cmd1.Fill(dt1);


            if (dt.Rows.Count > 0)
            {
                //if (dt1.Rows[0].IsNull = false) { }  
                Dr = Convert.ToDecimal(dt1.Rows[0]["D"]);
                Cr = Convert.ToDecimal(dt1.Rows[0]["C"]);
            }


            if (NorBalance == 1)
            {
                OpB = OpB + Dr - Cr;

            }
            else
                if (NorBalance == 2)
            {
                OpB = OpB + Cr - Dr;
            }
            return OpB;
        }



        void LoadPartyList()
        {

            SqlCommand cmd = new SqlCommand("select Name from PartyCode order by Name", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);
            lstParty.DataSource = dset;
            lstParty.DataBind();
            lstParty.DataTextField = "Name";
            lstParty.DataBind();

        }

        void LoadPartyLedgerMedium(DateTime StartDate, DateTime EndDate)
        {


            ModPartyCodeAgainstName objPC = new ModPartyCodeAgainstName();
            Module2 objModule2 = new Module2();

            string PartyCode = objPC.PartyCode(lstParty.SelectedItem.Value);
            int NorBalance = 1;
            NorBalance = objModule2.NormalBalanceParties(PartyCode);
            decimal OpBalance = 0;

            DateTime abc = Convert.ToDateTime(StartDate);
            OpBalance = PartyOpBal(PartyCode, Convert.ToDateTime(abc), NorBalance);

            SqlCommand cmd = new SqlCommand("select dateDr,DescriptionDr,Convert(numeric(18,2), AmountDr)as AmountDr,Convert(numeric(18,2),AmountCr) as AmountCr,DescriptionOfBillNo,BillNo,S_No,IsNull(DisRs,0) as DisRs,    DealRs     ,   IsNull(Rate,0) as Rate   , IsNull(Amount,0) as Amount      ,   IsNull(Qty,0) as Qty,IsNull(DisPer,0) as DisPer from PartiesLedger where CompID='" + CompID + "' and code='" + PartyCode + "' and datedr>='" + StartDate + "' and datedr<='" + EndDate + "'", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);



            DataTable dt = new DataTable();
            dt.Columns.Add("Date");
            dt.Columns.Add("Description");
            dt.Columns.Add("Qty");
            dt.Columns.Add("Rate");
            dt.Columns.Add("DisRs");
            dt.Columns.Add("DisPer");
            dt.Columns.Add("Amount");

            dt.Columns.Add("Debit");
            dt.Columns.Add("Credit");
            dt.Columns.Add("Balance");
            dt.Columns.Add("DescriptionOfBill");
            dt.Columns.Add("BillNo");

            dt.Rows.Add();
            dt.Rows[0]["Date"] = "";
            dt.Rows[0]["Description"] = "Opening Balance";
            dt.Rows[0]["Qty"] = "0";
            dt.Rows[0]["Rate"] = "0";
            dt.Rows[0]["DisRs"] = "0";
            dt.Rows[0]["DisPer"] = "0";
            dt.Rows[0]["Amount"] = "0";

            dt.Rows[0]["Debit"] = "0";
            dt.Rows[0]["Credit"] = "0";
            dt.Rows[0]["Balance"] = OpBalance;
            dt.Rows[0]["DescriptionOfBill"] = "";
            dt.Rows[0]["BillNo"] = "";


            int j = 1;
            decimal ClosingBalance = 0;
            decimal Debit = 0;
            decimal Credit = 0;
            for (int i = 0; i < dset.Tables[0].Rows.Count; i++)
            {
                Debit = Convert.ToDecimal(dset.Tables[0].Rows[i]["AmountDr"]);
                Credit = Convert.ToDecimal(dset.Tables[0].Rows[i]["AmountCr"]);
                if (j == 1 && NorBalance == 1)
                {
                    ClosingBalance = (OpBalance + Debit) - Credit;
                }
                else
                    if (j > 1 && NorBalance == 1)
                {
                    ClosingBalance = (ClosingBalance + Debit) - Credit;
                }

                if (j == 1 && NorBalance == 2)
                {
                    ClosingBalance = (OpBalance + Credit) - Debit;
                }
                else
                    if (j > 1 && NorBalance == 2)
                {
                    ClosingBalance = (ClosingBalance + Credit) - Debit;
                }


                dt.Rows.Add();
                dt.Rows[j]["Date"] = Convert.ToDateTime(dset.Tables[0].Rows[i]["datedr"]).ToString("dd-MMM-yyyy");
                dt.Rows[j]["Description"] = dset.Tables[0].Rows[i]["DescriptionDr"];
                dt.Rows[j]["Qty"] = dset.Tables[0].Rows[i]["Qty"].ToString();
                dt.Rows[j]["Rate"] = dset.Tables[0].Rows[i]["Rate"].ToString();
                dt.Rows[j]["DisRs"] = dset.Tables[0].Rows[i]["DisRs"].ToString();
                dt.Rows[j]["DisPer"] = dset.Tables[0].Rows[i]["DisPer"].ToString();
                dt.Rows[j]["Amount"] = dset.Tables[0].Rows[i]["Amount"].ToString();

                dt.Rows[j]["Debit"] = dset.Tables[0].Rows[i]["AmountDr"];
                dt.Rows[j]["Credit"] = dset.Tables[0].Rows[i]["AmountCr"];
                dt.Rows[j]["Balance"] = ClosingBalance;
                dt.Rows[j]["DescriptionOfBill"] = dset.Tables[0].Rows[i]["DescriptionOfBillNo"]; ;
                dt.Rows[j]["BillNo"] = dset.Tables[0].Rows[i]["BillNo"];

                j++;
            }


            GridView1.DataSource = dt;
            GridView1.DataBind();


        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                LoadPartyLedgerMedium(Convert.ToDateTime(StartDate.Text), Convert.ToDateTime(txtEndDate.Text));
            }
            catch (Exception ex)
            {
                Response.Write("error  = " + ex.Message);

            }

        }
        decimal TotQty = 0;
        decimal TotDr = 0;
        decimal TotCr = 0;
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // add the UnitPrice and QuantityTotal to the running total variables
                TotQty += Convert.ToDecimal(e.Row.Cells[2].Text);
                TotDr += Convert.ToDecimal(e.Row.Cells[7].Text);
                TotCr += Convert.ToDecimal(e.Row.Cells[8].Text);
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";
                // for the Footer, display the running totals
                e.Row.Cells[2].Text = TotQty.ToString();
                e.Row.Cells[7].Text = TotDr.ToString();
                e.Row.Cells[8].Text = TotCr.ToString();

                e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Font.Bold = true;
            }
        }



    }
}