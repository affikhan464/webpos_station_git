﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebPOS.Reports.ReceivablePayable
{
    public partial class frmPartyBalanceGroupWise1 : System.Web.UI.Page
    {
        ModGL objModGL = new ModGL();
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtStartDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                txtEndDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                LoadPartyGroupList();
            }

           

        }


        void LoadPartyGroupList()
        {

            SqlCommand cmd = new SqlCommand("select GroupName from PartyGroup where CompID='" + CompID + "' order by GroupName", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);
            lstPartyGroup.DataSource = dset;
            lstPartyGroup.DataBind();
            lstPartyGroup.DataTextField = "GroupName";
            lstPartyGroup.DataBind();
        }

        void LoadReportMain(DateTime StartDate, DateTime EndDate, string PartyGroupName)
        {
            ModPartyCodeAgainstName objParty = new ModPartyCodeAgainstName();
            int GroupID = objParty.GroupCodeAgainstGroupName(PartyGroupName);



            SqlCommand cmd = new SqlCommand("Select Code,Name,Address,Phone,Mobile,Balance,NorBalance from PartyCode where  CompID='" + CompID + "' and bALANCE <> 0 AND GroupID=" + GroupID, con);


            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);

            GridView1.DataSource = dset;
            GridView1.DataBind();


        }






        protected void Button1_Click(object sender, EventArgs e)
        {

            LoadReportMain(Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text), Convert.ToString(lstPartyGroup.Text));
        }




       



        public Decimal PartyOpBal(string PartyCode, DateTime StartDate, int NorBalance)
        {
            Module7 obj = new Module7();
            DateTime dat = obj.StartDateTwo(StartDate);
            decimal OpB = 0;
            SqlDataAdapter cmd = new SqlDataAdapter("select OpBalance from PartyOpBalance where  CompID='" + CompID + "' and Code='" + PartyCode + "' and Dat='" + dat + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                OpB = Convert.ToInt32(dt.Rows[0]["OpBalance"]);

            }

            //'''''''''''''''''''''''''''''''''''''
            decimal Dr = 0;
            decimal Cr = 0;

            SqlDataAdapter cmd1 = new SqlDataAdapter("select isnull(sum(AmountDr),0) as D,isnull(sum(AmountCr),0) as C from PartiesLedger where  CompID='" + CompID + "' and code='" + PartyCode + "' and Datedr>='" + dat + "' and datedr<'" + StartDate + "'", con);
            DataTable dt1 = new DataTable();
            cmd1.Fill(dt1);


            if (dt.Rows.Count > 0)
            {
                //if (dt1.Rows[0].IsNull = false) { }  
                Dr = Convert.ToDecimal(dt1.Rows[0]["D"]);
                Cr = Convert.ToDecimal(dt1.Rows[0]["C"]);
            }


            if (NorBalance == 1)
            {
                OpB = OpB + Dr - Cr;

            }
            else
                if (NorBalance == 2)
                {
                    OpB = OpB + Cr - Dr;
                }
            return OpB;
        }



        public decimal Closing(string Code, int NB)
        {

            decimal Opbalance = PartyOpBal(Code, Convert.ToDateTime(txtStartDate.Text), NB);
            decimal Dr = UpToDateDr(Code);
            decimal Cr = UpToDateCr(Code);

            decimal Clos = Opbalance + Dr - Cr;
            return Clos;
        }

        public decimal UpToDateDr(string Code)
        {

            decimal UpToDateDr = 0;
            UpToDateDr = objModGL.UpToDateDebitGL(Code, Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
            return UpToDateDr;
        }
        public decimal UpToDateCr(string Code)
        {

            decimal UpToDateCr = 0;
            UpToDateCr = objModGL.UpToDateCreditGL(Code, Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
            return UpToDateCr;
        }
        decimal TotDr = 0;
        decimal TotCr = 0;
        decimal TotOp = 0;
        decimal TotClo = 0;
        decimal TotCurr = 0;
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblOp = (Label)e.Row.FindControl("lblOp");
                TotOp += Convert.ToDecimal(lblOp.Text);
                Label lblDr = (Label)e.Row.FindControl("lblDr");
                TotDr += Convert.ToDecimal(lblDr.Text);
                Label lblCr = (Label)e.Row.FindControl("lblCr");
                TotCr += Convert.ToDecimal(lblCr.Text);
                Label lblCro = (Label)e.Row.FindControl("lblCloBal");
                TotClo += Convert.ToDecimal(lblCro.Text);

                Label lblCurr = (Label)e.Row.FindControl("lblCurrBal");
                TotCurr += Convert.ToDecimal(lblCurr.Text);


            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";
                // for the Footer, display the running totals
                e.Row.Cells[5].Text = TotOp.ToString();
                e.Row.Cells[6].Text = TotDr.ToString();
                e.Row.Cells[7].Text = TotCr.ToString();
                e.Row.Cells[8].Text = TotClo.ToString();
                e.Row.Cells[9].Text = TotCurr.ToString();

                e.Row.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[6].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[7].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[8].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[9].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Font.Bold = true;
            }
        }






    }
}