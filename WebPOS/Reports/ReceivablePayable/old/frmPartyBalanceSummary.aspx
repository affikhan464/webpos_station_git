﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="frmPartyBalanceSummary.aspx.cs" Inherits="WebPOS.Reports.ReceivablePayable.frmPartyBalanceSummary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="<%= ResolveUrl("~/css/style.css") %>" rel="stylesheet" />
    <link href="<%= ResolveUrl("~/css/reset.css") %>" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 


         <br /><h2 class="heading"  >Parties Balance Summary</h2><br />
        
    <div style ="width:100%;height:100%;text-align:center; vertical-align:middle">
    <table border="0" style="width:100%">
         <tr style="background-color:aliceblue;align-items:center">

            <td> <asp:RadioButton ID="rdbClient" runat="server" Text="Clients" GroupName="one"/><asp:RadioButton ID="rdbSupplyer" runat="server" text="Supplyer" GroupName="one"/><asp:RadioButton ID="rdbAll" runat="server" text="All" GroupName="one"/>
                <asp:Button ID="Button1" runat="server" Text="Report"  OnClick="Button1_Click" Width="55px"/>


            </td>
        </tr>

    </table>
</div>
        <div>
    <br/>
            <center>
           <asp:GridView CssClass="grid" ID="GridView1"  AutoGenerateColumns="False" HeaderStyle-BackColor ="WhiteSmoke"   AlternatingRowStyle-BackColor ="WhiteSmoke"
      runat="server" ShowFooter="True"  FooterStyle-BackColor="Wheat" OnRowDataBound="GridView1_RowDataBound" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical">

<AlternatingRowStyle BackColor="#DCDCDC"></AlternatingRowStyle>

      <Columns>
        
         <asp:TemplateField HeaderText="S. #"> 
               <ItemTemplate> <span style="text-align:left"> <asp:Label ID="lblSNo" runat="server"  Text='<%# Convert.ToInt32( Container.DataItemIndex+1) %>'></asp:Label>
               </span> 
          </ItemTemplate>
               <ItemStyle HorizontalAlign="Center" />
          </asp:TemplateField>
         
        
          <asp:BoundField HeaderText="Party Name" DataField="Name" ReadOnly="true"  />


           <asp:TemplateField HeaderText="Nature"> 
               <ItemTemplate> <span style="text-align:left"> <asp:Label ID="lblNature" runat="server"  Text='<%#  Nature(Convert.ToInt16(Eval("NorBalance")))  %>'></asp:Label>
               </span> 
          </ItemTemplate>
               <ItemStyle HorizontalAlign="Center" />
          </asp:TemplateField>
         
           <asp:TemplateField HeaderText="Debit"> 
               <ItemTemplate> <span style="text-align:left"> <asp:Label ID="lblDebit" runat="server"  Text='<%#  Receivable(Convert.ToDecimal( Eval("Balance")),Convert.ToInt16(Eval("NorBalance")))  %>'></asp:Label>
               </span> 
          </ItemTemplate>
               <ItemStyle HorizontalAlign="Right" />
          </asp:TemplateField>
         
          
          <asp:TemplateField HeaderText="Credit"> 
               <ItemTemplate> <span style="text-align:left"> <asp:Label ID="lblCredit" runat="server"  Text='<%#  Payable(Convert.ToDecimal( Eval("Balance")),Convert.ToInt16(Eval("NorBalance")))  %>'></asp:Label>
               </span> 
          </ItemTemplate>
               <ItemStyle HorizontalAlign="Right" />
          </asp:TemplateField>
         

          
          
          

      </Columns>

<FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>

<HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White"></HeaderStyle>
               <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
               <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
               <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
               <SortedAscendingCellStyle BackColor="#F1F1F1" />
               <SortedAscendingHeaderStyle BackColor="#0000A9" />
               <SortedDescendingCellStyle BackColor="#CAC9C9" />
               <SortedDescendingHeaderStyle BackColor="#000065" />
    </asp:GridView>
    
    </div>

<%--</form>
</body>
</html>--%>


</asp:Content>
