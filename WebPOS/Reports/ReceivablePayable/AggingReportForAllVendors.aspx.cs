﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.ReceivablePayable
{
    public partial class AggingReportForAllVendors : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport()
        {
            try
            {

                var model = GetTheReport();
                return new BaseModel { Success = true, AggingReports = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<AggingReportViewModel> GetTheReport()
        {
            decimal NetSale = 0;
            ModSaleReport objModSaleReport = new ModSaleReport();

            ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
            //int PartyGroupID = objModPartyCodeAgainstName.PartyGroupIDAgainstGroupName(lstPartyGroup.Text);
            var ActualStartDate = DateTime.ParseExact(DateTime.Now.AddDays(-14).ToShortDateString() + " 00:00:00", "M/d/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            var ActualEndDate = DateTime.ParseExact(DateTime.Now.ToShortDateString() + " 00:00:00", "M/d/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);


            //DateTime ActualStartDate = DateTime.Now.AddDays(-15);
            //DateTime ActualEndDate = DateTime.Now;
            decimal NetBalance = 0;
            decimal RemainingBalance = 0;
            string PartyCode;
            string PartyName;

            DateTime StartDate;

            //1-15 days
            var FirstStartDate = ActualStartDate.AddDays(-15);
            var FirstEndDate = FirstStartDate.AddDays(14);

            //16-30 days
            var SecondStartDate = FirstStartDate.AddDays(-15);
            var SecondEndDate = SecondStartDate.AddDays(14);

            //31-45 days
            var ThirdStartDate = SecondStartDate.AddDays(-15);
            var ThirdEndDate = ThirdStartDate.AddDays(14);

            //46-60 days
            var FourthStartDate = ThirdStartDate.AddDays(-15);
            var FourthEndDate = FourthStartDate.AddDays(14);

            //61-75 days
            var FifthStartDate = FourthStartDate.AddDays(-15);
            var FifthEndDate = FifthStartDate.AddDays(14);

            //76-90 days
            var SixthStartDate = FifthStartDate.AddDays(-15);
            var SixthEndDate = SixthStartDate.AddDays(14);

            //90+ days

            var SeventhStartDate = SixthStartDate.AddYears(-100);
            var SeventhEndDate = SixthStartDate.AddDays(-1);

            SqlCommand cmd = new SqlCommand(@"Select 
                Code,
                Name,
                Balance,
                Norbalance,
                   (Select isnull(sum(Purchase3.Total-Purchase3.Discount),0) 
                    from Purchase1 INNER JOIN Purchase3 ON Purchase1.InvNo = Purchase3.InvNo  
                    where Purchase1.CompID='" + CompID + @"' 
                    and Purchase1.VenderCode=Code 
                    and Purchase1.dat>='" + FirstStartDate + @"'  
                    and Purchase1.dat<='" + FirstEndDate + @"') 
                as FirstTotalSale,
                   (Select isnull(sum(Purchase3.Total-Purchase3.Discount),0) 
                    from Purchase1 INNER JOIN Purchase3 ON Purchase1.InvNo = Purchase3.InvNo  
                    where Purchase1.CompID='" + CompID + @"' 
                    and Purchase1.VenderCode=Code 
                    and Purchase1.dat>='" + SecondStartDate + @"'  
                    and Purchase1.dat<='" + SecondEndDate + @"') 
                as SecondTotalSale,
                   (Select isnull(sum(Purchase3.Total-Purchase3.Discount),0) 
                    from Purchase1 INNER JOIN Purchase3 ON Purchase1.InvNo = Purchase3.InvNo  
                    where Purchase1.CompID='" + CompID + @"' 
                    and Purchase1.VenderCode=Code 
                    and Purchase1.dat>='" + ThirdStartDate + @"'  
                    and Purchase1.dat<='" + ThirdEndDate + @"') 
                as ThirdTotalSale,
                   (Select isnull(sum(Purchase3.Total-Purchase3.Discount),0) 
                    from Purchase1 INNER JOIN Purchase3 ON Purchase1.InvNo = Purchase3.InvNo  
                    where Purchase1.CompID='" + CompID + @"' 
                    and Purchase1.VenderCode=Code 
                    and Purchase1.dat>='" + FourthStartDate + @"'  
                    and Purchase1.dat<='" + FourthEndDate + @"') 
                as FourthTotalSale,
                   (Select isnull(sum(Purchase3.Total-Purchase3.Discount),0) 
                    from Purchase1 INNER JOIN Purchase3 ON Purchase1.InvNo = Purchase3.InvNo  
                    where Purchase1.CompID='" + CompID + @"' 
                    and Purchase1.VenderCode=Code 
                    and Purchase1.dat>='" + FifthStartDate + @"'  
                    and Purchase1.dat<='" + FifthEndDate + @"') 
                as FifthTotalSale,
                   (Select isnull(sum(Purchase3.Total-Purchase3.Discount),0) 
                    from Purchase1 INNER JOIN Purchase3 ON Purchase1.InvNo = Purchase3.InvNo  
                    where Purchase1.CompID='" + CompID + @"' 
                    and Purchase1.VenderCode=Code 
                    and Purchase1.dat>='" + SixthStartDate + @"'  
                    and Purchase1.dat<='" + SixthEndDate + @"') 
                as SixthTotalSale,
                   (Select isnull(sum(Purchase3.Total-Purchase3.Discount),0) 
                    from Purchase1 INNER JOIN Purchase3 ON Purchase1.InvNo = Purchase3.InvNo  
                    where Purchase1.CompID='" + CompID + @"' 
                    and Purchase1.VenderCode=Code 
                    and Purchase1.dat>='" + SeventhStartDate + @"'  
                    and Purchase1.dat<='" + SeventhEndDate + @"') 
                as SeventhTotalSale
                
                from PartyCode
                where CompID = '" + CompID + @"'
                and Balance > 0
                and NorBalance = 2
                order by name", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<AggingReportViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {

                DateTime EndDate = ActualEndDate;
                PartyCode = Convert.ToString(dt.Rows[i]["Code"]);
                PartyName = Convert.ToString(dt.Rows[i]["Name"]);
                NetBalance = Convert.ToDecimal(dt.Rows[i]["Balance"]);
                RemainingBalance = NetBalance;

                var invoice = new AggingReportViewModel();
                decimal NetTotalSale = 0;
                for (int j = 1; j < 8; j++)
                {
                    //NetSale = objModSaleReport.TotalSaleTo_Party(PartyCode, StartDate, EndDate);

                    if (j == 1)
                    {
                        NetSale = Convert.ToDecimal(dt.Rows[i]["FirstTotalSale"]);
                    }

                    else if (j == 2)
                    {
                        NetSale = Convert.ToDecimal(dt.Rows[i]["SecondTotalSale"]);
                    }
                    else if (j == 3)
                    {
                        NetSale = Convert.ToDecimal(dt.Rows[i]["ThirdTotalSale"]);
                    }
                    else if (j == 4)
                    {
                        NetSale = Convert.ToDecimal(dt.Rows[i]["FourthTotalSale"]);
                    }
                    else if (j == 5)
                    {
                        NetSale = Convert.ToDecimal(dt.Rows[i]["FifthTotalSale"]);
                    }
                    else if (j == 6)
                    {
                        NetSale = Convert.ToDecimal(dt.Rows[i]["SixthTotalSale"]);
                    }
                    else
                    {
                        NetSale = Convert.ToDecimal(dt.Rows[i]["SeventhTotalSale"]);
                    }

                    decimal totalSale;
                    if (RemainingBalance > 0 && j < 7)
                    {

                        if (RemainingBalance > NetSale)
                        {
                            totalSale = NetSale;

                        }
                        else
                        {
                            totalSale = RemainingBalance;

                        }
                        RemainingBalance = RemainingBalance - NetSale;
                    }
                    else if (RemainingBalance > 0 && j == 7)
                    {
                        totalSale = RemainingBalance;
                    }
                    else
                    {
                        totalSale = 0;
                    }

                    if (j == 1)
                    {
                        invoice.FirstRange = totalSale.ToString();
                    }

                    else if (j == 2)
                    {
                        invoice.SecondRange = totalSale.ToString();
                    }
                    else if (j == 3)
                    {
                        invoice.ThirdRange = totalSale.ToString();
                    }
                    else if (j == 4)
                    {
                        invoice.FourthRange = totalSale.ToString();
                    }
                    else if (j == 5)
                    {
                        invoice.FifthRange = totalSale.ToString();
                    }
                    else if (j == 6)
                    {
                        invoice.SixthRange = totalSale.ToString();
                    }
                    else
                    {
                        invoice.SeventhRange = totalSale.ToString();
                    }

                    NetTotalSale += totalSale;

                }
                NetSale = 0;
                invoice.Code = PartyCode;
                invoice.Name = PartyName;
                invoice.NetBalance = NetTotalSale.ToString();
                model.Add(invoice);




            }
            return model;
        }
        protected void BtnClick(object sender, EventArgs e)
        {

            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}