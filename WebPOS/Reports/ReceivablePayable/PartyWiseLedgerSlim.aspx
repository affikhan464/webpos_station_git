﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="PartyWiseLedgerSlim.aspx.cs" Inherits="WebPOS.Reports.StockReport.PartyWiseLedgerSlim" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

   
    <link href="../../css/allitems.css?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>" rel="stylesheet"  />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="searchAppSection reports">
        <div class="contentContainer">
            <h2>Party Wise Ledger Slim</h2>
            <div class="BMSearchWrapper row">
                <!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->
                <div class="col col-12 col-sm-6 col-md-3">
                    <span class="userlLabel">From</span>
                    <asp:TextBox ID="fromTxbx" CssClass="datetimepicker fromTxbx Date" runat="server"></asp:TextBox>
                    <%--<input id="fromTxbx" class=" datetimepicker fromTxbx"  name="Date" type="text" />--%>
                </div>
                <div class="col col-12 col-sm-6 col-md-3">
                    <span class="userlLabel">To</span>
                    <asp:TextBox ID="toTxbx" CssClass="datetimepicker toTxbx Date" runat="server"></asp:TextBox>

                    <%--<input id="toTxbx" class="datetimepicker toTxbx" type="text"  name="Date"/>--%>
                </div>

                <div class="col col-12 col-sm-6 col-md-3">

                    <input id="PartySearchBox" data-id="PartyTextbox" data-type="Party"
                        data-function="GetParty" class=" px-1 clientInput clearable PartySearchBox autocomplete form-control" name="PartyName" type="text" placeholder="Enter a party name" />
                </div>
                <div class="col col-12 col-sm-6 col-md-3">
                    <span class="userlLabel">Party Code</span>
                    <asp:TextBox ID="txtPartyCode" disabled="disabled" ClientIDMode="Static" CssClass="PartyCode" name="PartyCode" runat="server"></asp:TextBox>
                </div>


                <div class="col col-12 col-sm-6 col-md-6">
                    <a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-spinner"></i>Get Record</a>
                </div>

                <div class="col col-12 col-sm-6 col-md-6">
                    <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>Print</a>
                </div>

                <%--<asp:Button ID="Button1" CssClass="printBtn" OnClick="BtnClick" runat="server"  Text="Print" />--%>
            </div>
            <!-- searchAppSection -->
        </div>
    </div>
    <!-- featuredSection -->

    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">

        <div class="contentContainer">

            <div class="DaySaleGroup2 reports recommendedSection allItemsView">


                <div class="itemsHeader d-flex">

                    <div class="sixteen phCol flex-1">
                        <a href="javascript:void(0)">Date
                        </a>
                    </div>
                    <div class="sixteen phCol flex-2">
                        <a href="javascript:void(0)">Description
                        </a>
                    </div>
                    <div class="thirteen phCol flex-1">
                        <a href="javascript:void(0)">Debit
                        </a>
                    </div>
                    <div class="thirteen phCol flex-1">
                        <a href="javascript:void(0)">Credit
                        </a>
                    </div>

                    <div class="thirteen phCol flex-1">
                        <a href="javascript:void(0)">Balance
											
                        </a>
                    </div>



                </div>
                <!-- itemsHeader -->
                <div class="loader">
                    <div class="display-table">
                        <div class="display-table-row">
                            <div class="display-table-cell">
                                <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="itemsSection">
                </div>
                <div class="itemsFooter">
                    <div class='itemRow newRow d-flex'>
                        <div class='sixteen flex-1'><span></span></div>
                        <div class='sixteen flex-2'><span></span></div>
                        <div class='thirteen flex-1'>
                            <input disabled="disabled" name='totalDebit' /></div>
                        <div class='thirteen flex-1'>
                            <input disabled="disabled" name='totalCredit' /></div>
                        <div class='thirteen flex-1'>
                            <input disabled="disabled" name='closingBalance' /></div>

                    </div>
                </div>

            </div>

        </div>
        <!-- recommendedSection -->



    </div>

    <script src="<%=ResolveClientUrl("/Script/Vendor/jquery.min.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="<%=ResolveClientUrl("/Script/Vendor/jquery-ui.min.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="<%=ResolveClientUrl("/Script/Vendor/jquery.datetimepicker.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="<%=ResolveClientUrl("/Script/InitializeDateTimePicker.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="<%=ResolveClientUrl("/Script/Autocomplete.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>

    <script type="text/javascript">

        var counter = 0;

        $(document).ready(function () {
            initializeDatePicker();
            resizeTable();
        });

        $(window).resize(function () {
            resizeTable();

        });


        $("#toTxbx").on("keypress", function (e) {

            if (e.key == 13) {

                clearInvoice();
                counter = 0;
                getReport();

            }
        });
        $("#searchBtn").on("click", function (e) {
            counter = 0;

            clearInvoice();
            getReport();

        });

        $(document).on('click', '#print', function () {
            window.open(
                '/Reports/ReceivablePayable/Prints/PartyWiseLedgerSlim_Print.aspx?from=' + $(".fromTxbx").val() + '&to=' + $(".toTxbx").val() + '&partyCode=' + $(".PartyCode").val(),
                '_blank'
            );
        })
        function getReport() {
            $(".loader").show();

            $.ajax({
                url: '/Reports/ReceivablePayable/PartyWiseLedgerSlim.aspx/getReport',
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ "from": $(".fromTxbx").val(), "to": $(".toTxbx").val(), "partyCode": $(".PartyCode").val() }),
                success: function (response) {


                    if (response.d.Success) {
                        if (response.d.PrintReport.length > 0) {


                            for (var i = 0; i < response.d.PrintReport.length; i++) {
                                counter++;
                                $("<div class='itemRow newRow d-flex'>" +
                                    "<div class='sixteen flex-1'><span>" + response.d.PrintReport[i].Text_1 + "</span></div>" +
                                    "<div class='sixteen flex-2'><span>" + response.d.PrintReport[i].Text_2 + "</span></div>" +
                                    "<div class='thirteen flex-1' ><input  value=" + Number(response.d.PrintReport[i].Text_9).toFixed(2) + " disabled name='Debit'/></div>" +
                                    "<div class='thirteen flex-1' ><input  value=" + Number(response.d.PrintReport[i].Text_10).toFixed(2) + " disabled name='Credit'/></div>" +
                                    "<div class='thirteen flex-1' ><input  value=" + Number(response.d.PrintReport[i].Text_11).toFixed(2) + " disabled name='ClosingB'/></div>" +
                                    "</div>").appendTo(".itemsSection");


                            }
                            calculateData();
                            var closingBalance = $(".itemsSection .itemRow:last").find("[name=Balance]").val();
                            $(".itemsFooter [name=closingBalance]").val(closingBalance);
                            $(".loader").hide();

                        } else if (response.d.PrintReport.length == 0) {
                            $(".loader").hide(); swal({
                                title: "No Result Found ", type: 'error', type: 'error',
                                text: "No Result Found in the selected Date Range"
                            });
                        }

                    }
                    else {
                        $(".loader").hide();
                        swal({
                            title: "there is some error",
                            text: response.d.Message
                        });
                    }

                },
                error: function (error) {
                    swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                }
            });
        }
        function clearInvoice() {


            $(".itemsSection .itemRow").remove();
        }
        function calculateData() {

            var sum;
            var inputs = ["Debit", 'Credit'];
            for (var i = 0; i < inputs.length; i++) {

                var currentInput = inputs[i];
                sum = 0;
                $("input[name = '" + currentInput + "']").each(function () {

                    if (this.value.trim() === "") {
                        sum = (Number(sum) + Number(0));
                    } else {
                        sum = (Number(sum) + Number(this.value));

                    }

                });

                $("[name=total" + currentInput + "]").val('');
                $("[name=total" + currentInput + "]").val(Number(sum).toFixed(2));


            }


        }
    </script>
</asp:Content>
