﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="GroupWisePartyBalanceSummary_2.aspx.cs" Inherits="WebPOS.Reports.SaleReports.GroupWisePartyBalanceSummary_2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
	<link rel="stylesheet" href="<%=ResolveClientUrl("/css/jquery.datetimepicker.css")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>" />
    <link href="../../css/allitems.css?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>" rel="stylesheet"  />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

			<div class="reports searchAppSection">
				<div class="contentContainer">
					<h2>Party Group Wise Balance Summary-2</h2>
					<div class="BMSearchWrapper row">
						<!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->
                         
                       
                            <div class="col col-xs-12 col-sm-6 col-md-3">
								<select id="PartyGroupDD" class="dropdown">
									<option value="" class="selectCl" onclick="manInvSelection(this)">Select Brand Name</option>
						        </select>
							</div>
                         
                        
                       
                        
						    <div class="col col-xs-12 col-sm-3 col-md-2">
							        <a href="#" id="searchBtn" class="greenBtn"><i class="fas fa-spinner"></i> Get Record</a>
							</div>
						
                            <div class="col col-xs-12 col-sm-3 col-md-1">
                                    <a href="#" id="print" class="redBtn"><i class="fas fa-print"></i>  Print</a>
							</div>
					</div><!-- searchAppSection -->
				</div>
			</div><!-- featuredSection -->
	
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="recommendedSection reports allItemsView">


								<div class="itemsHeader">
									<div class="seven phCol">
										<a href="javascript:void(0)"> 
											Serial No.
										</a>
									</div>
									
									<div class="thirteen phCol">                                                                    
										<a href="javascript:void(0)"> 
											PartyCode
										</a>
									</div>
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
											PartyName
										</a>
									</div>
                                    <div class="thirteen phCol">
										<a href="javascript:void(0)"> 
											Contact Person
										</a>
									</div>
									
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Phone #
											
										</a>
									</div>
                                    
                                    <div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Balance
											
										</a>
									</div>
         
									
								</div><!-- itemsHeader -->	
                         <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
								<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow'>
								<div class='seven'><span></span></div>
								<div class='thirteen'><span></span></div>
						    	<div class='thirteen'><input  disabled="disabled"  name='A'/></div>
								<div class='thirteen'><input  disabled="disabled"  name='A'/></div>
                                <div class='thirteen'><input  disabled="disabled"  name='A'/></div>
								
								<div class='thirteen'><input  disabled="disabled"  name='totalClosingB'/></div>
								
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
    
	<script src="<%=ResolveClientUrl("/Script/Vendor/jquery.min.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
	<script src="<%=ResolveClientUrl("/Script/Vendor/jquery-ui.min.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="<%=ResolveClientUrl("/Script/Vendor/jquery.datetimepicker.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="<%=ResolveClientUrl("/Script/InitializeDateTimePicker.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
	<script src="<%=ResolveClientUrl("/Script/Vendor/ddslick.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    
    <script src="<%=ResolveClientUrl("/Script/DropDown.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>


   <script type="text/javascript">

            $(document).ready(function () {
                initializeDatePicker();
                appendAttribute.init("PartyGroupDD", "PartyGroup");
                resizeTable();
            });

            $(window).resize(function () {
                resizeTable();
              
            });

            function resizeTable() {

                setTimeout(function () {

                    var header = $(".searchAppSection").height();
                  
                    var itemSectionHeight = $(window).height() - header - 120;
                    $(".dayCashAndCreditSale .itemsSection ").css("height", itemSectionHeight);

                }, 500);

            }
            $("#toTxbx").on("keypress", function (e) {
                if (e.key==13) {
                    getReport();
                   
                }
            });
            $("#searchBtn").on("click", function (e) {
                    clearInvoice();
                    getReport();

             
            });
            function clearInvoice() {


                $(".itemsSection .itemRow").remove();
            }
            function getReport() { $(".loader").show();

                $.ajax({
                    url: '/Reports/ReceivablePayable/GroupWisePartyBalanceSummary_2.aspx/getReport',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({  "PartyGroupID": $("#PartyGroupDD .dd-selected-value").val() }),
                    success: function (response) {
                     

                        if (response.d.Success) {
                            if (response.d.PrintReport.length>0) {
                                clearInvoice();
                            
                                for (var i = 0; i < response.d.PrintReport.length; i++) {
                                   debugger
                                $("<div class='itemRow newRow'>" +
                                    "<div class='seven'><span>" + (parseInt(i) + 1) + "</span></div>" +
                                    "<div class='thirteen'><span>" + response.d.PrintReport[i].Text_1 + "</span></div>" +
                                    "<div class='thirteen'><input  value='" + response.d.PrintReport[i].Text_2 + "' disabled name='PartyName' /></div>" +
                                    "<div class='thirteen'><input  value='" + response.d.PrintReport[i].Text_3 + "' disabled name='Owner' /></div>" +
                                    "<div class='thirteen'><input  value='" + response.d.PrintReport[i].Text_4 + "' disabled name='Phone' /></div>" +
                                    "<div class='thirteen'><input  value=" + Number(response.d.PrintReport[i].Text_5).toFixed(2) + " disabled name='ClosingB' /></div>" +
                                   
                                    
                                    "</div>").appendTo(".itemsSection");


                            }
                            $(".loader").hide();
                            calculateData();
                        
                            } else {
                                $(".loader").hide(); swal({
                                    title: "No Result Found ",
                                    text: "No Result Found!"
                                });
                            }
                         
                        }
                        else {
                            $(".loader").hide();
                            swal({
                                title: "there is some error",
                                text: response.d.Message
                            });
                        }

                    },
                    fail: function () {

                    }
                });
            }

            function clearInvoice() {

            
                $(".itemsSection .itemRow").remove();
            }
            function calculateData() {

                var sum;
                var inputs = ['ClosingB'];
                for (var i = 0; i < inputs.length; i++) {

                    var currentInput = inputs[i];
                    sum = 0;
                    $("input[name = '" + currentInput + "']").each(function () {

                        if (this.value.trim() === "") {
                            sum = (parseFloat(sum) + parseFloat(0));
                        } else {
                            sum = (parseFloat(sum) + parseFloat(this.value));

                        }

                    });

                    $("[name=total" + currentInput + "]").val('');
                    $("[name=total" + currentInput + "]").val(parseFloat(sum).toFixed(2));


                }

            }




   </script>
  
</asp:Content>
