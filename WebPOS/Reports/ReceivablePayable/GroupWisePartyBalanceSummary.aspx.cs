﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.StockReport
{
    public partial class GroupWisePartyBalanceSummary : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string PartyType, string PGCode)
        {
            try
            {
                var model = GetTheReport(PartyType, PGCode);
                return new BaseModel { Success = true, PrintReport = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<PrintingTable> GetTheReport(string PartyType, string PGCode)
        {
            var andNorBalance = string.Empty;
            if (PartyType == Party.Client)
            {
                andNorBalance = "and NorBalance = 1";
            }
            else if (PartyType == Party.Supplier)
            {
                andNorBalance = "and NorBalance = 2";
            }
            var andPGCode = string.Empty;
            if (PGCode != "0")
            {
                andPGCode = "and GroupID = " + PGCode;
            }

            SqlCommand cmd = new SqlCommand(@"Select 
                Code,
                Name,
                Balance,
                Address,
                Norbalance,Phone,Mobile
                from PartyCode
                where CompID = '" + CompID + @"'
                " + andNorBalance + @"
                " + andPGCode + @"
                and  Balance <> 0
                
                order by name", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);


            var PrintingTab = new List<PrintingTable>();

            for (int i = 0; i < dset.Tables[0].Rows.Count; i++)
            {

                decimal Debit = 0;
                decimal Credit = 0;
                string partyCode = Convert.ToString(dset.Tables[0].Rows[i]["Code"]);
                var partyName = dset.Tables[0].Rows[i]["Name"].ToString();
                var Address = dset.Tables[0].Rows[i]["Address"].ToString() + "/" + dset.Tables[0].Rows[i]["Phone"].ToString() + "/" + dset.Tables[0].Rows[i]["Mobile"].ToString();
                var norBalance = Convert.ToDecimal(dset.Tables[0].Rows[i]["Norbalance"]);
                var balance = Convert.ToDecimal(dset.Tables[0].Rows[i]["Balance"]);

                if (norBalance == 1 && balance > 0)
                {
                    Debit = balance;
                }
                else if (norBalance == 2 && balance < 0)
                {
                    Debit = (balance * -1);
                }
                else if (norBalance == 2 && balance > 0)
                {
                    Credit = balance;
                }
                else if (norBalance == 1 && balance < 0)
                {
                    Credit = (balance * -1);
                }

                var PrintTab = new PrintingTable()
                {
                    Text_1 = partyCode.ToString(),
                    Text_2 = partyName,
                    Text_3 = Address,
                    Text_4 = norBalance == 1 ? "Client" : "Supplier",
                    Text_5 = Debit.ToString(),
                    Text_6 = Credit.ToString(),
                };
                PrintingTab.Add(PrintTab);
            }


            //-------------------------------------------


            return PrintingTab;



        }

        protected void BtnClick(object sender, EventArgs e)
        {

        }
    }
}