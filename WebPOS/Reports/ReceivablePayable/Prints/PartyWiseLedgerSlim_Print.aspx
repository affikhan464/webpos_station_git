﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PartyWiseLedgerSlim_Print.aspx.cs" Inherits="WebPOS.PartyWiseLedgerSlim_Print" %>

<%--<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.4000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>--%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Print</title>
    <link rel="stylesheet" href="/css/vendor.min.css" />
    <link rel="stylesheet" href="/css/print.css" />

</head>
<body class="p-3">
    <form id="form1" runat="server">
        <%--<div>
            <CR:CrystalReportViewer ID="CRViewer" runat="server" AutoDataBind="true" HasPrintButton="True" PrintMode="ActiveX" GroupTreeStyle-ShowLines="True" />
        </div>--%>
        <%--  <div class="mx-auto w-25 text-center">
            <a class="printPageBtn"><i class="fas fa-print fa-4x p-3"></i></a>
        </div>--%>
        <div id="Page">
            <%@ Register Src="~/_PrintHead.ascx" TagName="_PrintHead" TagPrefix="uc" %>
            <uc:_PrintHead ID="_PrintHead1" runat="server" />
            <div class="row mb-3">
                <div class="col-6 ">
                    <strong>Party Name:</strong> <span class="PartyName"></span>
                </div>
                <div class="col-12">
                    <strong>From Date:</strong> <span class="DateFrom"></span>
                </div>
                <div class="col-6 ">
                    <strong>To Date:</strong> <span class="DateTo"></span>
                </div>
            </div>
            <table>
                <thead>
                </thead>
                <tbody>
                </tbody>
            </table>
            
        </div>
    </form>

    <script src="/js/vendor.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="/Script/Vendor/sweetalert.min.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="/Script/GetQueryString.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script>
        $(document).on("click", ".printPageBtn", function () {
            var printContent = document.getElementById("Page");
            var WinPrint = window.open('', '', 'width=827,height=1169');
            WinPrint.document.write('<link rel="stylesheet" href="/css/vendor.min.css" />');
            WinPrint.document.write('<link rel="stylesheet" href="/css/print.css" />');
            WinPrint.document.write(printContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
        });
        $(document).ready(function () {

            $.ajax({
                url: "/Reports/ReceivablePayable/Prints/PartyWiseLedgerSlim_Print.aspx/GetReport",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ "from": getQueryString("from"), "to": getQueryString("to"), "partyCode": getQueryString("partyCode") }),

                success: function (BaseModel) {
                    debugger
                    if (BaseModel.d.Success) {
                        if (BaseModel.d.Data.Rows.length) {

                            var rows = BaseModel.d.Data.Rows;

                            var headrow = "";
                            for (var j = 0; j < Object.values(rows[0]).length; j++) {
                                headrow += `<th>${Object.keys(rows[0])[j]}</th>`;
                            }

                            $("table thead").append(headrow);
                            var invrows = '';
                            for (var i = 0; i < rows.length; i++) {
                                var row = '<tr>';
                                for (var j = 0; j < Object.values(rows[i]).length; j++) {
                                    var tdclass = '';
                                    var value=Object.values(rows[i])[j]
                                    var key=Object.keys(rows[i])[j]
                                    if (key == 'Balance' ||key == 'Debit' ||key == 'Credit' ||key == 'Qty' ||key == 'Rate' ||key == 'PurchasingCost' ||key == 'SellingCost' || key == 'Amount') {
                                        tdclass = 'class="text-right"';
                                        value = (Number(value)).toFixed(2);
                                    }
                                    row += `<td ${tdclass}>${value}</td>`;

                                }
                                row += '</tr>';
                                invrows += row;
                            }

                            var footerrow = '<tr>';
                            for (var j = 0; j < Object.values(rows[0]).length; j++) {
                                footerrow +=
                                    `
                                    <td class="Total${Object.keys(rows[0])[j]} font-weight-bold text-right"></td>
                                `;
                            }
                            footerrow += '</tr>';
                            invrows += footerrow;
                            $("table tbody").append(invrows);

                            var data = BaseModel.d.Data;
                            for (var j = 0; j < Object.keys(data).length; j++) {
                                var key = Object.keys(data)[j];
                                var value = Object.values(data)[j];
                                 if (key != "Rows") {
                                    if (isNaN(value) || value == ""|| key == "InvoiceNumber"|| value == null) {
                                        $("." + key).text(value);
                                    } else {
                                        $("." + key).text((Number(value)).toFixed(2));
                                    }
                                }
                            }
                            
                            $("td").each(function () {
                                if (!$(this).text().trim().length) {
                                    $(this).addClass("border-0");
                                }
                            });
                            window.print()
                        }
                    }
                    else if (BaseModel.d.Success == false && BaseModel.d.LoginAgain == true) {
                        swal({
                            title: "Login Again",
                            html:
                                `<a target="_blank" href="/?c=1" style="display:block;color:black;font-weight:700">Login Here and print it again.</a> `,

                            type: "warning"

                        });
                         $("table tbody").append("<tr><td><h2 class='text-center'><a onclick='return location.href=location.href;'>Refresh this Page after login</a></h1></td></tr>");

                    }
                    else {
                        swal("Error", BaseModel.d.Message, "error");
                    }


                }



            })
        })
    </script>
</body>
</html>
