﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS
{
    public partial class PartyWiseLedgerSlim_Print : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        { }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel GetReport(string from, string to, string partyCode)
        {
            try
            {
                if (HttpContext.Current.Session["UserId"] == null)
                {
                    return new BaseModel() { Success = false, LoginAgain = true };
                }

                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, partyCode);
                return new BaseModel() { Success = true, Data = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }
        public static PrintViewModel<object> GetTheReport(DateTime StartDate, DateTime EndDate, string PartyCode)
        {
            //var model = new List<PrintingTable>();
            var model = new PrintViewModel<object>();
            var rows = new List<object>();

            Module7 objModule7 = new Module7();
            ModItem objModItem = new ModItem();
            ModGLCode objModGLCode = new ModGLCode();
            Module2 objModule2 = new Module2();
            var modParty = new ModPartyCodeAgainstName();
            decimal OpB = 0;
            //string PartyCode = "0101010300038";
            var PartyName = modParty.PartyNameAgainstCode(PartyCode);
            Int32 NorBalance = objModule2.NormalBalanceParties(PartyCode);
            SqlCommand cmd = new SqlCommand("select isnull(sum(OpBalance),0) as OpBalance from PartyOpBalance where CompID = '" + CompID + "' and Code = '" + PartyCode + "' and Dat = '" + objModule7.StartDateTwo(StartDate) + "'", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            var dt = new DataTable();
            adpt.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                OpB = Convert.ToInt32(dt.Rows[0]["OpBalance"]);

            }

            decimal Dr = 0;
            decimal Cr = 0;
            //   "select sum(AmountDr),sum(AmountCr) from PartiesLedger where  CompID='" & logon.CompID & "' and code='" &                  Trim(PartyCode) & "' and Datedr>='" & Module7.StartDateTwo(StartDate) & "' and datedr<" & "'" & StartDate & "'"
            SqlDataAdapter cmd1 = new SqlDataAdapter("select isnull(sum(AmountDr),0) as D,isnull(sum(AmountCr),0) as C from PartiesLedger where  CompID='" + CompID + "' and code='" + PartyCode + "' and Datedr>='" + objModule7.StartDateTwo(StartDate) + "' and datedr<'" + StartDate + "'", con);
            DataTable dt1 = new DataTable();
            cmd1.Fill(dt1);


            if (dt1.Rows.Count > 0)
            {
                //if (dt1.Rows[0].IsNull = false) { }  
                Dr = Convert.ToDecimal(dt1.Rows[0]["D"]);
                Cr = Convert.ToDecimal(dt1.Rows[0]["C"]);
            }


            if (NorBalance == 1)
            {
                OpB = OpB + Dr - Cr;

            }
            else
               if (NorBalance == 2)
            {
                OpB = OpB + Cr - Dr;
            }


            var openingBalanceRow = new PrintingTable();
            var Date = Convert.ToString(StartDate);
            var Opening = Convert.ToString(OpB);
            
            var rowOP = new
            {
                Date = Convert.ToDateTime(Date).ToString("dd-MMM-yyyy"),
                Description = "Opening Balance" + Dr.ToString(),
                Debit = Convert.ToString("0"),
                Credit = Convert.ToString("0"),
                Balance = OpB.ToString()
            };
            

            rows.Add(rowOP);
            //---------------------------------END OF Opening Balance---------------------------------
            Decimal ClosingBalance = 0;
            Int32 j = 1;

            SqlCommand cmd2 = new SqlCommand("select dateDr, DescriptionDr, Convert(numeric(18, 2), AmountDr) as AmountDr, Convert(numeric(18, 2), AmountCr) as AmountCr, IsNull(Qty, 0) as Qty, IsNull(Rate, 0) as Rate, IsNull(Amount, 0) as Amount, DescriptionOfBillNo, S_No, BillNo, IsNull(DisRs, 0) as DisRs, IsNull(DisPer, 0) as DisPer, IsNull(DealRs, 0) as DealRs from PartiesLedger where CompID = '" + CompID + "' and code = '" + PartyCode + "'  and datedr >= '" + StartDate + "' and datedr <= '" + EndDate + "' order by System_Date_Time", con);
            SqlDataAdapter adpt2 = new SqlDataAdapter(cmd2);
            var dt2 = new DataTable();
            adpt2.Fill(dt2);
            decimal TotalDebit = 0;
            decimal TotalCredit = 0;
            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                var PrintingT = new PrintingTable();

                var Date1 = Convert.ToDateTime(dt2.Rows[i]["dateDr"]).ToString("dd-MMM-yyyy"); // Convert.ToDateTime(dset.Tables[0].Rows[i]["dateDr"]).ToString("dd-MMM-yyyy"),
                var Description = Convert.ToString(dt2.Rows[i]["DescriptionDr"]);
                var Qty = Convert.ToString(dt2.Rows[i]["Qty"]);
                var Rate = Convert.ToString(dt2.Rows[i]["Rate"]);
                var DisRs = Convert.ToString(dt2.Rows[i]["DisRs"]);
                var DisPer = Convert.ToString(dt2.Rows[i]["DisPer"]);
                var DealRs = Convert.ToString(dt2.Rows[i]["DealRs"]);
                var Amount = Convert.ToString(dt2.Rows[i]["Amount"]);
                var AmountDr = Convert.ToString(dt2.Rows[i]["AmountDr"]);
                var AmountCr = Convert.ToString(dt2.Rows[i]["AmountCr"]);

                //-----------------------------------Closing Balance Calculation Start
                if (j == 1 && NorBalance == 1)
                {
                    ClosingBalance = (Convert.ToDecimal(Opening) + Convert.ToDecimal(AmountDr)) - Convert.ToDecimal(AmountCr);
                }
                if (j > 1 && NorBalance == 1)
                {
                    ClosingBalance = (Convert.ToDecimal(ClosingBalance) + Convert.ToDecimal(AmountDr)) - Convert.ToDecimal(AmountCr);
                }

                if (j == 1 && NorBalance == 2)
                {
                    ClosingBalance = (Convert.ToDecimal(Opening) + Convert.ToDecimal(AmountCr)) - Convert.ToDecimal(AmountDr);
                }
                if (j > 1 && NorBalance == 2)
                {
                    ClosingBalance = (Convert.ToDecimal(ClosingBalance) + Convert.ToDecimal(AmountCr)) - Convert.ToDecimal(AmountDr);
                }
                j++;
                //-----------------------------------Closing Balance Calculation End

                

                var row = new
                {
                    Date = Date1.ToString(),
                    Description = Description.ToString(),
                    Debit = AmountDr.ToString(),
                    Credit = AmountCr.ToString(),
                    Balance = ClosingBalance.ToString()
                };
                TotalDebit += Convert.ToDecimal(AmountDr);
                TotalCredit += Convert.ToDecimal(AmountCr);
                rows.Add(row);
            }

            model.Rows = rows;
            
            model.PartyName = PartyName;
            model.DateFrom = StartDate.ToString("dd/MM/yyyy");
            model.DateTo = EndDate.ToString("dd/MM/yyyy");
            model.TotalDebit = TotalDebit;
            model.TotalCredit = TotalCredit;
            return model;
        }
        public DateTime getDate(string date)
        {
            var newdate = DateTime.ParseExact(date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);
            return newdate;
        }
    }
}