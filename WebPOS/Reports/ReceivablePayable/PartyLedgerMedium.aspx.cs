﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.ReceivablePayable
{
    public partial class PartyLedgerMedium : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string partyCode)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, partyCode);
                return new BaseModel { Success = true, PrintReport = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<PrintingTable> GetTheReport(DateTime StartDate, DateTime EndDate, string PartyCode)
        {

            var model = new List<PrintingTable>();
            Module7 objModule7 = new Module7();
            ModItem objModItem = new ModItem();
            ModGLCode objModGLCode = new ModGLCode();
            Module2 objModule2 = new Module2();
            decimal OpB = 0;
            //string PartyCode = "0101010300038";
            Int32 NorBalance = objModule2.NormalBalanceParties(PartyCode);
            SqlCommand cmd = new SqlCommand("select OpBalance from PartyOpBalance where CompID = '" + CompID + "' and Code = '" + PartyCode + "' and Dat = '" + objModule7.StartDateTwo(StartDate) + "'", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            var dt = new DataTable();
            adpt.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                OpB = Convert.ToInt32(dt.Rows[0]["OpBalance"]);

            }

            decimal Dr = 0;
            decimal Cr = 0;
            //   "select sum(AmountDr),sum(AmountCr) from PartiesLedger where  CompID='" & logon.CompID & "' and code='" &                  Trim(PartyCode) & "' and Datedr>='" & Module7.StartDateTwo(StartDate) & "' and datedr<" & "'" & StartDate & "'"
            SqlDataAdapter cmd1 = new SqlDataAdapter("select isnull(sum(AmountDr),0) as D,isnull(sum(AmountCr),0) as C from PartiesLedger where  CompID='" + CompID + "' and code='" + PartyCode + "' and Datedr>='" + objModule7.StartDateTwo(StartDate) + "' and datedr<'" + StartDate + "'", con);
            DataTable dt1 = new DataTable();
            cmd1.Fill(dt1);


            if (dt1.Rows.Count > 0)
            {
                //if (dt1.Rows[0].IsNull = false) { }  
                Dr = Convert.ToDecimal(dt1.Rows[0]["D"]);
                Cr = Convert.ToDecimal(dt1.Rows[0]["C"]);
            }


            if (NorBalance == 1)
            {
                OpB = OpB + Dr - Cr;

            }
            else
               if (NorBalance == 2)
            {
                OpB = OpB + Cr - Dr;
            }


            var openingBalanceRow = new PrintingTable();
            var Date = Convert.ToString(StartDate);
            var Opening = Convert.ToString(OpB);


            openingBalanceRow.Text_1 = Convert.ToDateTime(Date).ToString("dd-MMM-yyyy");// Date.ToString();
            openingBalanceRow.Text_2 = "Opening Balance" + Dr.ToString();
            openingBalanceRow.Text_3 = Convert.ToString("0");
            openingBalanceRow.Text_4 = Convert.ToString("0");
            openingBalanceRow.Text_11 = OpB.ToString();

            model.Add(openingBalanceRow);
            //---------------------------------END OF Opening Balance---------------------------------
            Decimal ClosingBalance = 0;
            Int32 j = 1;

            SqlCommand cmd2 = new SqlCommand("select dateDr, DescriptionDr, Convert(numeric(18, 2), AmountDr) as AmountDr, Convert(numeric(18, 2), AmountCr) as AmountCr, IsNull(Qty, 0) as Qty, IsNull(Rate, 0) as Rate, IsNull(Amount, 0) as Amount, DescriptionOfBillNo, S_No, BillNo, IsNull(DisRs, 0) as DisRs, IsNull(DisPer, 0) as DisPer, IsNull(DealRs, 0) as DealRs from PartiesLedger where CompID = '" + CompID + "' and code = '" + PartyCode + "'  and datedr >= '" + StartDate + "' and datedr <= '" + EndDate + "' order by System_Date_Time", con);
            SqlDataAdapter adpt2 = new SqlDataAdapter(cmd2);
            var dt2 = new DataTable();
            adpt2.Fill(dt2);

            for (int i = 0; i < dt2.Rows.Count; i++)
            {
                var PrintingT = new PrintingTable();

                var Date1 = Convert.ToDateTime(dt2.Rows[i]["dateDr"]).ToString("dd-MMM-yyyy"); // Convert.ToDateTime(dset.Tables[0].Rows[i]["dateDr"]).ToString("dd-MMM-yyyy"),
                var Description = Convert.ToString(dt2.Rows[i]["DescriptionDr"]);
                var Qty = Convert.ToString(dt2.Rows[i]["Qty"]);
                var Rate = Convert.ToString(dt2.Rows[i]["Rate"]);
                var DisRs = Convert.ToString(dt2.Rows[i]["DisRs"]);
                var DisPer = Convert.ToString(dt2.Rows[i]["DisPer"]);
                var DealRs = Convert.ToString(dt2.Rows[i]["DealRs"]);
                var Amount = Convert.ToString(dt2.Rows[i]["Amount"]);
                var AmountDr = Convert.ToString(dt2.Rows[i]["AmountDr"]);
                var AmountCr = Convert.ToString(dt2.Rows[i]["AmountCr"]);

                //-----------------------------------Closing Balance Calculation Start
                if (j == 1 && NorBalance == 1)
                {
                    ClosingBalance = (Convert.ToDecimal(Opening) + Convert.ToDecimal(AmountDr)) - Convert.ToDecimal(AmountCr);
                }
                if (j > 1 && NorBalance == 1)
                {
                    ClosingBalance = (Convert.ToDecimal(ClosingBalance) + Convert.ToDecimal(AmountDr)) - Convert.ToDecimal(AmountCr);
                }

                if (j == 1 && NorBalance == 2)
                {
                    ClosingBalance = (Convert.ToDecimal(Opening) + Convert.ToDecimal(AmountCr)) - Convert.ToDecimal(AmountDr);
                }
                if (j > 1 && NorBalance == 2)
                {
                    ClosingBalance = (Convert.ToDecimal(ClosingBalance) + Convert.ToDecimal(AmountCr)) - Convert.ToDecimal(AmountDr);
                }
                j++;
                //-----------------------------------Closing Balance Calculation End

                PrintingT.Text_1 = Date1.ToString();
                PrintingT.Text_2 = Description.ToString();
                PrintingT.Text_3 = Qty.ToString();
                PrintingT.Text_4 = Rate.ToString();
                PrintingT.Text_5 = DisRs.ToString();
                PrintingT.Text_6 = DisPer.ToString();
                PrintingT.Text_7 = DealRs.ToString();
                PrintingT.Text_8 = Amount.ToString();
                PrintingT.Text_9 = AmountDr.ToString();
                PrintingT.Text_10 = AmountCr.ToString();
                PrintingT.Text_11 = ClosingBalance.ToString();

                model.Add(PrintingT);
            }

            return model;
        }


        public static decimal PartyOpBal(string PartyCode, DateTime StartDate, int NorBalance)
        {
            Module7 obj = new Module7();
            DateTime dat = obj.StartDateTwo(StartDate);
            decimal OpB = 0;
            SqlDataAdapter cmd = new SqlDataAdapter("select OpBalance from PartyOpBalance where  CompID='" + CompID + "' and Code='" + PartyCode + "' and Dat='" + dat + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                OpB = Convert.ToInt32(dt.Rows[0]["OpBalance"]);

            }

            //'''''''''''''''''''''''''''''''''''''
            decimal Dr = 0;
            decimal Cr = 0;

            SqlDataAdapter cmd1 = new SqlDataAdapter("select isnull(sum(AmountDr),0) as D,isnull(sum(AmountCr),0) as C from PartiesLedger where  CompID='" + CompID + "' and code='" + PartyCode + "' and Datedr>='" + dat + "' and datedr<'" + StartDate + "'", con);
            DataTable dt1 = new DataTable();
            cmd1.Fill(dt1);


            if (dt.Rows.Count > 0)
            {
                //if (dt1.Rows[0].IsNull = false) { }  
                Dr = Convert.ToDecimal(dt1.Rows[0]["D"]);
                Cr = Convert.ToDecimal(dt1.Rows[0]["C"]);
            }


            if (NorBalance == 1)
            {
                OpB = OpB + Dr - Cr;

            }
            else
                if (NorBalance == 2)
            {
                OpB = OpB + Cr - Dr;
            }
            return OpB;
        }
        protected void BtnClick(object sender, EventArgs e)
        {
            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
            //var itemCode = ItemCode.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/ItemWiseSaleDetailCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&itemCode=" + itemCode);

        }
    }
}