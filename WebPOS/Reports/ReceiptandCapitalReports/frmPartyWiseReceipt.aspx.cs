﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS.Reports.Receipt_and_Capital_Reports
{
    public partial class frmPartyWiseReceipt : System.Web.UI.Page
    {
        string CompID = "01";
        string CashAccountGLCode = "01010100001"; 
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtStartDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                txtEndDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                LoadPartyList();
            }
        }


        void LoadPartyList()
        {

            SqlCommand cmd = new SqlCommand("select Name from PartyCode where CompID='" + CompID + "' order by Name", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);
            lstParty.DataSource = dset;
            lstParty.DataBind();
            lstParty.DataTextField = "Name";
            lstParty.DataBind();

        }

        void LoadReport( DateTime StartDate, DateTime EndDate)
        {


            ModPartyCodeAgainstName objPartyCodeAgainstName = new ModPartyCodeAgainstName();
            string PartyCode = objPartyCodeAgainstName.PartyCode(lstParty.Text);
            string CashAccountGLCode1 = "0101010100001";

            if (rdbThroughCash.Checked ==true && chkParty.Checked==true )
            {
                SqlCommand cmd = new SqlCommand("Select  datedr,VenderCode,V_No,AmountDr,Narration,BankCode from GeneralLedger where  CompID='" + CompID + "' and  V_Type='CRV' and Code='" + CashAccountGLCode1 + "' and SecondDescription ='FromParties' and VenderCode='" + PartyCode + "' and datedr>='" + StartDate + "' and datedr<='" + EndDate + "' order by System_Date_Time", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataSet dset = new DataSet();
                adpt.Fill(dset);
                GridView1.DataSource = dset;
                GridView1.DataBind();
            }
            else
                if (rdbThroughBank.Checked == true && chkParty.Checked == true)
                {
                    SqlCommand cmd = new SqlCommand("Select  datedr,VenderCode,V_No,AmountDr,Narration,BankCode from GeneralLedger where  CompID='" + CompID + "' and  AmountDr>0 and V_Type='BRV'  and SecondDescription ='FromPartiesThroughBank' and VenderCode='" + PartyCode + "' and datedr>='" + StartDate + "' and datedr<='" + EndDate + "' order by System_Date_Time ", con);
                    SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                    DataSet dset = new DataSet();
                    adpt.Fill(dset);

                    GridView1.DataSource = dset;
                    GridView1.DataBind();
                }
                else
                    if (rdbOnInvoice.Checked == true && chkParty.Checked == true)
                    {
                        SqlCommand cmd = new SqlCommand("Select Invoice1.PartyCode as VenderCode , Invoice1.Date1 as datedr , Invoice1.InvNo as V_No , Invoice1.InvNo as BankCode ,Invoice3.CashPaid as AmountDr,Invoice1.Name as Narration from (Invoice1 inner join invoice3 on invoice1.InvNo=invoice3.InvNo) where  Invoice1.CompID='" + CompID + "' and  Invoice1.PartyCode='" + PartyCode + "' and Invoice1.Date1>='" + StartDate + "' and Invoice1.Date1<='" + EndDate + "' order by Invoice1.InvNo", con);
                        SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                        DataSet dset = new DataSet();
                        adpt.Fill(dset);
                        GridView1.DataSource = dset;
                        GridView1.DataBind();
                    }
                    else
                if (rdbThroughCash.Checked ==true && chkParty.Checked==false )
            {
                SqlCommand cmd = new SqlCommand("Select  datedr,VenderCode,V_No,AmountDr,Narration,BankCode from GeneralLedger where  CompID='" + CompID + "' and  V_Type='CRV' and Code='" + CashAccountGLCode1 + "' and SecondDescription ='FromParties' and datedr>='" + StartDate + "' and datedr<='" + EndDate + "' order by System_Date_Time", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataSet dset = new DataSet();
                adpt.Fill(dset);
                GridView1.DataSource = dset;
                GridView1.DataBind();
            }
            else
                if (rdbThroughBank.Checked == true && chkParty.Checked == false)
                {
                    SqlCommand cmd = new SqlCommand("Select  datedr,VenderCode,V_No,AmountDr,Narration,BankCode from GeneralLedger where  CompID='" + CompID + "' and  AmountDr>0 and V_Type='BRV'  and SecondDescription ='FromPartiesThroughBank' and datedr>='" + StartDate + "' and datedr<='" + EndDate + "' order by System_Date_Time ", con);
                    SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                    DataSet dset = new DataSet();
                    adpt.Fill(dset);

                    GridView1.DataSource = dset;
                    GridView1.DataBind();
                }
                else
                    if (rdbOnInvoice.Checked == true && chkParty.Checked == false)
                    {
                        SqlCommand cmd = new SqlCommand("Select Invoice1.PartyCode as VenderCode , Invoice1.Date1 as datedr , Invoice1.InvNo as V_No , Invoice1.InvNo as BankCode ,Invoice3.CashPaid as AmountDr,Invoice1.Name as Narration from (Invoice1 inner join invoice3 on invoice1.InvNo=invoice3.InvNo) where  Invoice1.CompID='" + CompID + "' and  Invoice1.Date1>='" + StartDate + "' and Invoice1.Date1<='" + EndDate + "' order by Invoice1.InvNo", con);
                        SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                        DataSet dset = new DataSet();
                        adpt.Fill(dset);
                        GridView1.DataSource = dset;
                        GridView1.DataBind();
                    }





        }






        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                LoadReport(Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
            }
            catch (Exception ex)
            {
                Response.Write("error  = " + ex.Message);

            }
        }


        decimal TotAmo = 0;
        
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // add the UnitPrice and QuantityTotal to the running total variables
                TotAmo += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "AmountDr"));
                
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";
                // for the Footer, display the running totals
                e.Row.Cells[6].Text = TotAmo.ToString();
                
                e.Row.Cells[6].HorizontalAlign = e.Row.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Font.Bold = true;
            }
        }

        public string BankNameAgainstCodeLocal(string BankCode)
        {
            ModGLCode objModGLCode = new ModGLCode();
            string BankName="";

            BankName = objModGLCode.GLTitleAgainstGLCode(BankCode);
            return BankName;

        }



    }
}