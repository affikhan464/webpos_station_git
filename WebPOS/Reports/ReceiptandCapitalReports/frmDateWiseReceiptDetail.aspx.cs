﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebPOS.Reports.Receipt_and_Capital_Reports
{
    public partial class frmDateWiseReceiptDetail : System.Web.UI.Page
    {
        string CompID = "01";
        string CashAccountGLCode ="01" + "01010100001";
        //CashAccountGLCode = CompID + "01010100001";
        Module8 objModParty = new Module8 ();
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtStartDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                txtEndDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                
            }
        }


        

        void DateWiseReceiptDetail(DateTime StartDate, DateTime EndDate)
        {


            SqlCommand cmd = new SqlCommand("Select  datedr,VenderCode,V_No,AmountDr from GeneralLedger where  CompID='" + CompID + "' and  V_Type='CRV' and Code='" + CashAccountGLCode + "' and SecondDescription='FromParties' and datedr>='" + StartDate + "' and datedr<='" + EndDate + "' order by System_Date_Time", con);


            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);

            GridView1.DataSource = dset;
            GridView1.DataBind();


        }

        public string NameAgainstCode(string Code)
        {

            
            string Name = objModParty.PartyNameAgainstCode(Code);
            return Name;
        }




        protected void Button1_Click(object sender, EventArgs e)
        {
            DateWiseReceiptDetail (Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
        }


        decimal TotTotal = 0;
       

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // add the UnitPrice and QuantityTotal to the running total variables
                TotTotal += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "AmountDr"));
                

            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";
                // for the Footer, display the running totals
                e.Row.Cells[3].Text = TotTotal.ToString();
                

                e.Row.Cells[3].HorizontalAlign = e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                
                e.Row.Font.Bold = true;
            }
        }
    

    }
}