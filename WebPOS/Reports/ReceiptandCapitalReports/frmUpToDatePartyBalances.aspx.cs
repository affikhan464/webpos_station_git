﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS.Reports.Receipt_and_Capital_Reports
{
    public partial class frmUpToDatePartyBalances : System.Web.UI.Page
    {
        string CompID = "01";
        string CashAccountGLCode = "01010100001";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtEndDate.Text = DateTime.Today.ToString("MM/dd/yyyy"); 
            }
        }


       

        void LoadReport( DateTime EndDate)
        {





            SqlCommand cmd = new SqlCommand("Select Code, Name ,  NorBalance , Balance from PartyCode  where  CompID='" + CompID + "' order by PartyCode.Name", con);


                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataSet dset = new DataSet();
                adpt.Fill(dset);

                GridView1.DataSource = dset;
                GridView1.DataBind();
            
        }






        protected void Button1_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    LoadReport( Convert.ToDateTime(txtEndDate.Text));
            //}
            //catch (Exception ex)
            //{
            //    Response.Write("error  = " + ex.Message);

            //}
            LoadReport(Convert.ToDateTime(txtEndDate.Text));
        }


        decimal TotAmo = 0;

       

        public decimal Receivable(decimal Bal, int NorBalance)
        {
                decimal Receivable=0;
                decimal Balance=0;
            Balance=Bal;
         if (NorBalance == 1 && Bal > 0) 
              {
                  Receivable = Balance;
              }
              else
             if (NorBalance == 2 && Bal < 0)
             {
                 Receivable = Balance;
             }

            return Balance;

        }
        public string ClientSupplyer(int NorBalance)
        {
            String ClientS="";
            if (NorBalance == 1) { ClientS = "Client"; }
            if (NorBalance == 2) { ClientS = "Supplyer"; }
            return ClientS;
        }
        public decimal UpToDateBalanceofPartyDr( string PartyCode ,int NorBalance)
        {
            DateTime EndDate = Convert.ToDateTime(txtEndDate.Text);
            Module7 objModule7 = new Module7();
            DateTime StartDate = objModule7.StartDateTwo(Convert.ToDateTime( txtEndDate.Text));
            decimal Balance = 0;
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(OpBalance,0) as OpBalance from PartyOpBalance where  CompID='" + CompID + "' and Code='" + PartyCode + "' and Dat='" + StartDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal Opbalance = 0;
            if (dt.Rows.Count > 0)
            {
                Opbalance = Convert.ToDecimal(dt.Rows[0]["OpBalance"]);

            }

            SqlDataAdapter cmd1 = new SqlDataAdapter("select isnull(sum(AmountDr),0)  as Dr, isnull(sum(AmountCr),0) as Cr from PartiesLedger where  CompID='" + CompID +  "' and Code='" + PartyCode + "' and dateDr>='" + StartDate + "' and dateDr<='" + EndDate + "'", con);
            DataTable dt1 = new DataTable();
            cmd1.Fill(dt1);
            decimal TotalDr = 0;
            decimal TotalCr = 0;
            if (dt.Rows.Count > 0)
            {
                TotalDr = Convert.ToDecimal(dt1.Rows[0]["Dr"]);
                TotalCr = Convert.ToDecimal(dt1.Rows[0]["Cr"]);
            }
            if (NorBalance == 1) { Balance = Opbalance + TotalDr - TotalCr; }
            if (NorBalance == 2) { Balance = Opbalance + TotalCr - TotalDr; }

            if (NorBalance == 1 && Balance < 0) { Balance = 0; }
            if (NorBalance == 2 && Balance > 0) { Balance = 0; }
            return Balance;

        }
        public decimal UpToDateBalanceofPartyCr(string PartyCode, int NorBalance)
        {
            DateTime EndDate = Convert.ToDateTime(txtEndDate.Text);
            Module7 objModule7 = new Module7();
            DateTime StartDate = objModule7.StartDateTwo(Convert.ToDateTime(txtEndDate.Text));
            decimal Balance = 0;
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(OpBalance,0) as OpBalance from PartyOpBalance where  CompID='" + CompID + "' and Code='" + PartyCode + "' and Dat='" + StartDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal Opbalance = 0;
            if (dt.Rows.Count > 0)
            {
                Opbalance = Convert.ToDecimal(dt.Rows[0]["OpBalance"]);

            }

            SqlDataAdapter cmd1 = new SqlDataAdapter("select isnull(sum(AmountDr),0)  as Dr, isnull(sum(AmountCr),0) as Cr from PartiesLedger where  CompID='" + CompID + "' and Code='" + PartyCode + "' and dateDr>='" + StartDate + "' and dateDr<='" + EndDate + "'", con);
            DataTable dt1 = new DataTable();
            cmd1.Fill(dt1);
            decimal TotalDr = 0;
            decimal TotalCr = 0;
            if (dt.Rows.Count > 0)
            {
                TotalDr = Convert.ToDecimal(dt1.Rows[0]["Dr"]);
                TotalCr = Convert.ToDecimal(dt1.Rows[0]["Cr"]);
            }
            if (NorBalance == 1) { Balance = Opbalance + TotalDr - TotalCr; }
            if (NorBalance == 2) { Balance = Opbalance + TotalCr - TotalDr; }

            if (NorBalance == 2 && Balance < 0) { Balance = 0; }
            if (NorBalance == 1 && Balance > 0) { Balance = 0; }
            return Balance;

        }
        decimal TotDebit = 0;
        decimal TotCredit = 0;
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // add the UnitPrice and QuantityTotal to the running total variables                
                Label lblDebit = (Label)e.Row.FindControl("lblDebit");
                TotDebit += Convert.ToDecimal(lblDebit.Text);
                Label lblCredit = (Label)e.Row.FindControl("lblCredit");
                TotCredit += Convert.ToDecimal(lblCredit.Text);
            
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";
                // for the Footer, display the running totals
                e.Row.Cells[3].Text = TotDebit.ToString();
                e.Row.Cells[4].Text = TotCredit.ToString();
                

                e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                
                e.Row.Font.Bold = true;
            }
        }


    }
}