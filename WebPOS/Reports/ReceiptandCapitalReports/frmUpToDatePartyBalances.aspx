﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="~/Reports/Receipt and Capital Reports/frmUpToDatePartyBalances.aspx.cs"  Inherits="WebPOS.Reports.Receipt_and_Capital_Reports.frmUpToDatePartyBalances" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="<%= ResolveUrl("~/css/style.css") %>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>" rel="stylesheet"  />
    <link href="<%= ResolveUrl("~/css/reset.css") %>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>" rel="stylesheet"  />

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <div>
        
            <br /><h2 class="heading" >UpTo Date Parties Balances</h2><br />
  
 <div style ="width:100%;height:100%;text-align:center; vertical-align:middle">
    <table  border="0" style="width:100%">
        <tr  style="background-color:aliceblue"  >
            <td>
                <asp:Label ID="Label3" runat="server" Text="Select Date"></asp:Label>
                <asp:TextBox ID="txtEndDate" CssClass="datetimepicker"  runat="server" ReadOnly="true"></asp:TextBox>
                <asp:Button ID="Button1" runat="server" Text="Report" OnClick="Button1_Click"  />
            </td> 
                 
     </tr>
    </table>

</div>
<br />

        <asp:GridView CssClass="grid" ID="GridView1" AutoGenerateColumns="False" HeaderStyle-BackColor ="WhiteSmoke"   AlternatingRowStyle-BackColor ="WhiteSmoke"
      runat="server" OnRowDataBound="GridView1_RowDataBound" ShowFooter="True"  FooterStyle-BackColor="Wheat" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical">
<AlternatingRowStyle BackColor="#DCDCDC"></AlternatingRowStyle>
      <Columns>
        <asp:TemplateField HeaderText="S. #"> 
               <ItemTemplate> <span style="text-align:left"> <asp:Label ID="Label1" runat="server"  Text='<%# Convert.ToInt32( Container.DataItemIndex+1) %>'></asp:Label>
               </span> 
          </ItemTemplate>
               <ItemStyle HorizontalAlign="Center" />
          </asp:TemplateField>
          
          <asp:BoundField HeaderText="Name" DataField="Name" ReadOnly="true" >
          <ItemStyle HorizontalAlign="Left" />
          </asp:BoundField>

       


          <asp:TemplateField HeaderText="Nature"> 
               <ItemTemplate> <span style="text-align:left"> <asp:Label ID="Nature" runat="server"  Text='<%# ClientSupplyer( Convert.ToInt16( Eval("NorBalance"))) %>'></asp:Label>
               </span> 
          </ItemTemplate>
               <ItemStyle HorizontalAlign="Center" />
          </asp:TemplateField>
        

          <asp:TemplateField HeaderText="Debit"> 
               <ItemTemplate> <span style="text-align:left"> <asp:Label ID="lblDebit" runat="server"  Text='<%# UpToDateBalanceofPartyDr(Convert.ToString( Eval("Code")),Convert.ToInt16( Eval("NorBalance"))) %>'></asp:Label>
               </span> 
          </ItemTemplate>
               <ItemStyle HorizontalAlign="Right" />
          </asp:TemplateField>
        

       
        <asp:TemplateField HeaderText="Credit"> 
               <ItemTemplate> <span style="text-align:left"> <asp:Label ID="lblCredit" runat="server"  Text='<%# UpToDateBalanceofPartyCr(Convert.ToString( Eval("Code")),Convert.ToInt16( Eval("NorBalance"))) %>'></asp:Label>
               </span> 
          </ItemTemplate>
               <ItemStyle HorizontalAlign="Right" />
          </asp:TemplateField>
        
       
          
          
                 
          

      </Columns>

<FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>

<HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White"></HeaderStyle>
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#0000A9" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#000065" />
    </asp:GridView>

    </div>
 
</asp:Content>