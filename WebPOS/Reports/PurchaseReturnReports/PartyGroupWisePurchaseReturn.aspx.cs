﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.PurchaseReturnReports
{
    public partial class PartyGroupWisePurchaseReturn : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string PGCode)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, PGCode);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, string PGCode)
        {

            SqlCommand cmd = new SqlCommand(@"
                         Select 
                            PurchaseReturn1.Date1,
						    PurchaseReturn1.InvNo,
                            PurchaseReturn1.Name,
                            Sum(PurchaseReturn1.Total) as Total,
						    Sum(PurchaseReturn1.Discount) as Discount1,
                            Sum(PurchaseReturn1.Paid ) as Received,
                            (SELECT Balance FROM PartyCode where Code=PurchaseReturn1.PartyCode) as CurrentBalance                          
                         from PurchaseReturn1        
                         where PurchaseReturn1.PartyGPID = '" + PGCode + "' and PurchaseReturn1.CompID='" + CompID + "' and PurchaseReturn1.Date1>='" + StartDate + "' and PurchaseReturn1.Date1<='" + EndDate + "'  GROUP BY PurchaseReturn1.InvNo,PurchaseReturn1.Date1,PurchaseReturn1.Name,PurchaseReturn1.PartyCode   order by PurchaseReturn1.InvNo     ", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.Date = Convert.ToDateTime(dt.Rows[i]["Date1"]).ToString("dd/MMM/yyyy");
                invoice.InvoiceNumber = Convert.ToString(dt.Rows[i]["InvNo"]);
                invoice.PartyName = Convert.ToString(dt.Rows[i]["Name"]);
                invoice.Total = Convert.ToString(dt.Rows[i]["Total"]);
                invoice.NetDiscount = Convert.ToString(dt.Rows[i]["Discount1"]);
                invoice.Paid = Convert.ToString(dt.Rows[i]["Received"]);
                invoice.CurrentBalance = Convert.ToString(dt.Rows[i]["CurrentBalance"]);
                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            var partyGroupCode = "";// PGCodeTxbx.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/PartyGroupWiseSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&partyGroupCode=" + partyGroupCode);
        }
    }
}