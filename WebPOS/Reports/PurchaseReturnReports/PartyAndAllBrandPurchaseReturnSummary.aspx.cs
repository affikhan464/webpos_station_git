﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.PurchaseReturnReports
{
    public partial class PartyAndAllBrandPurchaseReturnSummary : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string code)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, code);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, string code)
        {

            SqlCommand cmd = new SqlCommand(@"
                select
				BrandName.Name,
                Sum(PurchaseReturn2.Qty) as Qty ,
                Sum(PurchaseReturn2.Amount) as Amount,
				Isnull(
				    (Select Sum(Purchase2.Qty) from Purchase2
                    inner join Purchase1 on Purchase2.InvNo=Purchase1.InvNo 
				   inner join InvCode on Purchase2.Code=InvCode.Code 				
                    where Purchase1.Dat>= '" + StartDate + @"'
                    and Purchase1.Dat <= '" + EndDate + @"'
                    and InvCode.Brand = BrandName.Code	
                    group by InvCode.Brand				
                ),0) as ReturnQty,				
                Isnull(
                    (Select Sum(Purchase2.Amount) from Purchase2				
                    inner join Purchase1 on Purchase2.InvNo=Purchase1.InvNo 
                   inner join InvCode on Purchase2.Code=InvCode.Code 				
                    where Purchase1.Dat>= '" + StartDate + @"'
                    and Purchase1.Dat <= '" + EndDate + @"'
                    and InvCode.Brand = BrandName.Code			
                    group by InvCode.Brand					
                ),0) as ReturnAmount 
			 
                FROM PurchaseReturn1  
                iNNER JOIN PurchaseReturn2 ON PurchaseReturn1.InvNo = PurchaseReturn2.InvNo   
                inner join InvCode on PurchaseReturn2.Code=InvCode.Code 
                iNNER JOIN BrandName ON InvCode.Brand = BrandName.Code  
                where  PurchaseReturn1.PartyCode='" + code + @"' 
                and PurchaseReturn1.CompID ='" + CompID + @"'
                and PurchaseReturn1.Date1 >= '" + StartDate + @"'
                and PurchaseReturn1.Date1 <= '" + EndDate + @"'
                GROUP BY  BrandName.Name,BrandName.Code 
                order by  BrandName.Name   ", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.Description = Convert.ToString(dt.Rows[i]["Name"]);

                var Quantity = Convert.ToInt32(dt.Rows[i]["ReturnQty"]);
                var ReturnQty = Convert.ToInt32(dt.Rows[i]["Qty"]);
                var ReturnAmount = Convert.ToInt32(dt.Rows[i]["Amount"]);
                var Amount = Convert.ToInt32(dt.Rows[i]["ReturnAmount"]);

                invoice.Quantity = Quantity.ToString();
                invoice.Amount = Amount.ToString();
                invoice.ReturnQty = ReturnQty.ToString();
                invoice.ReturnAmount = ReturnAmount.ToString();
                invoice.NetQtySold = (Quantity - ReturnQty).ToString();
                invoice.NetAmountSold = (Amount - ReturnAmount).ToString();

                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            //  Response.Redirect("~/Reports/SaleReports/CReports/ItemAndPartyWiseSaleDetailCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&partyCode=" + partyCode+ "&itemCode=" + itemCode);
        }
    }
}