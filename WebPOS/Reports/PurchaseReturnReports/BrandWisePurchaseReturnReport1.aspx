﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="BrandWisePurchaseReturnReport1.aspx.cs" Inherits="WebPOS.Reports.PurchaseReturnReports.BrandWisePurchaseReturnReport1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

			<div class="reports searchAppSection">
				<div class="contentContainer">
					<h2>Brand Wise Purchase Return Report 1</h2>
					<div class="BMSearchWrapper row">
						<!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->
                         <div class="col col-12 col-sm-6 col-md-3">
                            <span class="userlLabel">From</span>
                            <asp:TextBox ID="fromTxbx" CssClass="datetimepicker fromTxbx Date" runat="server"></asp:TextBox>
                            <%--<input id="fromTxbx" class=" datetimepicker fromTxbx"  name="Date" type="text" />--%>
                        </div>
                       <div class="col col-12 col-sm-6 col-md-3">
                            <span class="userlLabel">To</span>
                            <asp:TextBox ID="toTxbx" CssClass="datetimepicker toTxbx Date" runat="server"></asp:TextBox>

                            <%--<input id="toTxbx" class="datetimepicker toTxbx" type="text"  name="Date"/>--%>
                        </div>
                        <div class="col col-12 col-sm-6 col-md-3">
								<select id="BrandNameDD" class="dropdown">
									<option value="" class="selectCl" onclick="manInvSelection(this)">Select Brand Name</option>
						        </select>
							</div>
                         
                        
                       
                        
						 <div class="col col-12 col-sm-3 col-md-2">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-spinner"></i> Get Record</a>
							</div>
						
                        <div class="col col-12 col-sm-3 col-md-1">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
							</div>
					</div><!-- searchAppSection -->
				</div>
			</div><!-- featuredSection -->
	
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="recommendedSection reports allItemsView">


								<div class="itemsHeader">
									<div class="seven phCol">
										<a href="javascript:void(0)"> 
											Serial No.
										</a>
									</div>
									
								
                                    <div class="eleven phCol">                                                                    
										<a href="javascript:void(0)"> 
											Item Name
										</a>
									</div>
									<div class="seven phCol">
										<a href="javascript:void(0)"> 
											Qty
										</a>
									</div>
									
									<div class="eleven phCol">
										<a href="javascript:void(0)"> 
										Amount
											
										</a>
									</div>
                                           <div class="seven phCol">
										<a href="javascript:void(0)"> 
										Return Qty
											
										</a>
									</div>
                                    <div class="eleven phCol">
										<a href="javascript:void(0)"> 
										Return Amount
											
										</a>
									</div>
                                    <div class="eleven phCol">
										<a href="javascript:void(0)"> 
										Net Qty
											
										</a>
									</div>
									<div class="eleven phCol">
										<a href="javascript:void(0)"> 
										Net Amount
											
										</a>
									</div>
									<div class="eleven  phCol">
										<a href="javascript:void(0)"> 
										In Hand Qty
											
										</a>
									</div>
									
								</div><!-- itemsHeader -->	
                         <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
								<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow'>
								<div class='seven'><span></span></div>
								<div class='eleven'><span></span></div>
						    	<div class='seven'><input  disabled="disabled"  name='totalQuantity'/></div>
								<div class='eleven'><input  disabled="disabled"  name='totalAmount'/></div>
                                <div class='seven'><input  disabled="disabled"  name='totalReturnQty'/></div>
								<div class='eleven'><input  disabled="disabled"  name='totalReturnAmount'/></div>
								<div class='eleven'><input  disabled="disabled"  name='totalNetQty'/></div>
								<div class='eleven'><input  disabled="disabled"  name='totalNetAmount'/></div>
								
								<div class='eleven'><input  disabled="disabled"  name='totalInHandQty'/></div>
								
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
    
	
</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    
    <script src="<%=ResolveClientUrl("/Script/DropDown.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>


   <script type="text/javascript">

            $(document).ready(function () {
                initializeDatePicker();
                appendAttribute.init("BrandNameDD", "BrandName");
                resizeTable();
            });

            $(window).resize(function () {
                resizeTable();
              
            });

       
            $("#toTxbx").on("keypress", function (e) {
                if (e.key==13) {
                    getReport();
                   
                }
            });
            $("#searchBtn").on("click", function (e) {
              
                    getReport();

             
            });
            function getReport() { $(".loader").show();

                $.ajax({
                    url: '/Reports/PurchaseReturnReports/BrandWisePurchaseReturnReport1.aspx/getReport',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ "from": $(".fromTxbx").val(), "to": $(".toTxbx").val(), "brandCode": $("#BrandNameDD .dd-selected-value").val() }),
                    success: function (response) {
                      

                        if (response.d.Success) {
                            if (response.d.ListofInvoices.length>0) {
                                clearInvoice();
                            
                            for (var i = 0; i < response.d.ListofInvoices.length; i++) {
                                
                              
                                $("<div class='itemRow newRow'>" +
                                    "<div class='seven'><span>" + (parseInt(i) + 1) + "</span></div>" +
                                    "<div class='eleven'><span>" + response.d.ListofInvoices[i].Description + "</span></div>" +
                                    "<div class='seven'><input  id='newDescriptionTextbox_" + i + "' value=" + parseFloat(response.d.ListofInvoices[i].Quantity).toFixed(2) + " disabled name='Quantity' /></div>" +
                                    "<div class='eleven'><input  id='newDescriptionTextbox_" + i + "' value=" + parseFloat(response.d.ListofInvoices[i].Amount).toFixed(2) + " disabled name='Amount' /></div>" +
                                    "<div class='seven'><input  value=" + parseFloat(response.d.ListofInvoices[i].ReturnQty).toFixed(2) + " disabled name='ReturnQty'/></div>" +
                                    "<div class='eleven'><input  value=" + parseFloat(response.d.ListofInvoices[i].ReturnAmount).toFixed(2) + " disabled name='ReturnAmount'/></div>" +
                                    "<div class='eleven'><input  value=" + parseFloat(response.d.ListofInvoices[i].NetQtySold).toFixed(2) + " disabled name='NetQty'/></div>" +
                                    "<div class='eleven'><input  value=" + parseFloat(response.d.ListofInvoices[i].NetAmountSold).toFixed(2) + " disabled name='NetAmount'/></div>" +
                                      "<div class='eleven '><input name='InHandQty'  value=" + parseFloat(response.d.ListofInvoices[i].InHandQty).toFixed(2) + " disabled /></div>" +
                                 "</div>").appendTo(".dayCashAndCreditSale .itemsSection");


                            }
                            $(".loader").hide();
                            calculateData();
                        
                            } else {
                                $(".loader").hide(); swal({
                                    title: "No Result Found ",
                                    text: "No Result Found!"
                                });
                            }
                         
                        }
                        else {
                            $(".loader").hide();
                            swal({
                                title: "there is some error",
                                text: response.d.Message
                            });
                        }

                    },
                    error: function (error) { swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                    }
                });
            }

            function clearInvoice() {

            
                $(".itemsSection .itemRow").remove();
            }
            function calculateData() {

                var sum;
                var inputs = ["ReturnQty", 'Quantity', 'Amount', 'ReturnAmount', 'NetAmount', 'NetQty', 'InHandQty'];
                for (var i = 0; i < inputs.length; i++) {

                    var currentInput = inputs[i];
                    sum = 0;
                    $("input[name = '" + currentInput + "']").each(function () {

                        if (this.value.trim() === "") {
                            sum = (parseFloat(sum) + parseFloat(0));
                        } else {
                            sum = (parseFloat(sum) + parseFloat(this.value));

                        }

                    });

                    $("[name=total" + currentInput + "]").val('');
                    $("[name=total" + currentInput + "]").val(parseFloat(sum).toFixed(2));


                }

            }




   </script>
  
</asp:Content>
