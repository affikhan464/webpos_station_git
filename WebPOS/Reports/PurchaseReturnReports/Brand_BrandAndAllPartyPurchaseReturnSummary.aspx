﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="Brand_BrandAndAllPartyPurchaseReturnSummary.aspx.cs" Inherits="WebPOS.Reports.PurchaseReturnReports.Brand_BrandAndAllPartyPurchaseReturnSummary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
	<link rel="stylesheet" href="<%=ResolveClientUrl("/css/jquery.datetimepicker.css")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>" />
    <link href="../../css/allitems.css?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>" rel="stylesheet"  />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

				<div class="reports searchAppSection">
				<div class="contentContainer">
					<h2>Brand &amp; All Party Purchase Return Summary</h2>
					<div class="BMSearchWrapper row">
						<!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->
                       
                         <div  class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">From</span>
                                <asp:TextBox ID="fromTxbx" CssClass="datetimepicker fromTxbx Date" runat="server"></asp:TextBox>
								<%--<input id="fromTxbx" class=" datetimepicker fromTxbx"  name="Date" type="text" />--%>
							</div>
							<div  class="col col-12 col-sm-6 col-md-3">
                             <span class="userlLabel">To</span>
                                <asp:TextBox ID="toTxbx" CssClass="datetimepicker toTxbx Date" runat="server"></asp:TextBox>

								<%--<input id="toTxbx" class="datetimepicker toTxbx" type="text"  name="Date"/>--%>
							</div>
                           <div class="DDWrapper col col-12 col-sm-6 col-md-3">
                            <select id="BrandNameDD" class="dropdown">
                                <option value="0">Select Brand</option>
                            </select>
                        </div>
                        
                          
							
						<div class="col col-12 col-sm-3 col-md-2">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-list-ul"></i>  Get</a>
							</div>
						
                        <div class="col col-12 col-sm-3 col-md-1">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
						 <%--<asp:Button ID="Button2" Cssclass="btn btn-bm"  runat="server" Text="Print" />--%>
							</div>
					</div><!-- searchAppSection -->
				</div>
			</div><!-- featuredSection -->
	
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="DaySaleGroup2 BrandAndAllPrtywiseSale reports recommendedSection allItemsView">


								<div class="itemsHeader">
                                    

									<div class="seven phCol">
										<a href="javascript:void(0)"> 
											S #
										</a>
									</div>
									<div class="thirteen  phCol">                                                                    
										<a href="javascript:void(0)"> 
											Party Name
										</a>
									</div>
									
                                      
                                    <div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Return Quantity
											
										</a>
									</div>
                                    <div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Item Discount
											
										</a>
									</div>
                                    <div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Deal Rs.
											
										</a>
									</div>
                                    <div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Return Amount
											
										</a>
									</div>
                                    
								</div><!-- itemsHeader -->	
                         <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
								<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow'>
								<div class='seven'><span></span></div>
								<div class='thirteen '><span></span></div>
								
                                <div class='thirteen'><input  disabled="disabled"  name='totalReturnQty'/></div>
                                <div class='thirteen'><input  disabled="disabled"  name='totalItem_Disc'/></div>
                                <div class='thirteen'><input  disabled="disabled"  name='totalDealRs'/></div>
								<div class='thirteen'><input  disabled="disabled"  name='totalReturnAmount'/></div>
								
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
     </asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
	
    <script src="<%=ResolveClientUrl("/Script/Vendor/ddslick.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="<%=ResolveClientUrl("/Script/Dropdown.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
   <script type="text/javascript">

       var counter = 0;
       
       $(document).ready(function () {
           appendAttribute.init("BrandNameDD", "BrandName");

                initializeDatePicker();
                resizeTable();
            });

            $(window).resize(function () {
                resizeTable();
              
            });

          
            $("#toTxbx").on("keypress", function (e) {
                if (e.key==13) {
                    $("#currentPage").remove();
                    $("<input hidden id='currentPage' value='0' />").appendTo("body");
                    clearInvoice();
                    counter = 0;
                    getReport();
                    
                }
            });
            $("#searchBtn").on("click", function (e) {
                counter = 0;
                $("#currentPage").remove();
                $("<input hidden id='currentPage' value='0' />").appendTo("body");
                clearInvoice();
                getReport();
             
            });
            function getReport() { $(".loader").show();

                var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? "0" : $("#currentPage").val();
                $.ajax({
                    url: '/Reports/PurchaseReturnReports/Brand_BrandAndAllPartyPurchaseReturnSummary.aspx/getReport',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ "from": $(".fromTxbx").val(), "to": $(".toTxbx").val(), "brandCode": $("#BrandNameDD .dd-selected-value").val() }),
                    success: function (response) {
                      

                        if (response.d.Success) {
                            if (response.d.ListofInvoices.length>0) {
                                
                            
                            for (var i = 0; i < response.d.ListofInvoices.length; i++) {
                                counter++;
                                $("<div class='itemRow newRow'>"+
                                     "<div class='seven'><span>" + counter + "</span></div>" +
                                    "<div class='thirteen  name'><input  id='newDescriptionTextbox_" + i + "' value='" + response.d.ListofInvoices[i].PartyName + "' disabled /></div>" +
                                    
                                     "<div class='thirteen'><input  value=" + Number(response.d.ListofInvoices[i].ReturnQty).toFixed(2) + " disabled name='ReturnQty'/></div>" +
                                     "<div class='thirteen'><input  value=" + Number(response.d.ListofInvoices[i].Item_Disc).toFixed(2) + " disabled name='Item_Disc'/></div>" +
                                     "<div class='thirteen'><input  value=" + Number(response.d.ListofInvoices[i].DealRs).toFixed(2) + " disabled name='DealRs'/></div>" +
                                    "<div class='thirteen'><input  value=" + Number(response.d.ListofInvoices[i].ReturnAmount).toFixed(2) + " disabled name='ReturnAmount'/></div>" +
                                   

                                    "</div>").appendTo(".dayCashAndCreditSale .itemsSection");

                            }
                            $(".itemsSection .descriptionData.name").mouseenter(function (e) {

                                var text = $($(this).children()[0]).val();
                                $(".descriptionText").text(text);
                                $(".descriptionText").show();
                            });
                            $(" .itemsSection .descriptionData.name").mouseleave(function (e) {

                                $(".descriptionText").text("");
                                $(".descriptionText").hide();
                            });
                            calculateData();
                            $(".loader").hide();
                        
                            } else if (response.d.ListofInvoices.length == 0 && currentPage == "0") {
                                $(".loader").hide(); swal({
                                    title: "No Result Found ",
                                    text: "No Result Found!"
                                });
                            }
                         
                        }
                        else {
                            $(".loader").hide();
                            swal({
                                title: "there is some error",
                                text: response.d.Message
                            });
                        }

                    },
                    error: function (error) { swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                    }
                });
            }
            function clearInvoice() {


                $(".itemsSection .itemRow").remove();
            }
            function calculateData() {

                var sum;
                var inputs = ["ReturnQty", 'DealRs', 'Item_Disc', 'ReturnAmount'];

                for (var i = 0; i < inputs.length; i++) {

                    var currentInput = inputs[i];
                    sum = 0;
                    $("input[name = '" + currentInput + "']").each(function () {

                        if (this.value.trim() === "") {
                            sum = (Number(sum) + Number(0));
                        } else {
                            sum = (Number(sum) + Number(this.value));

                        }

                    });

                    $("[name=total" + currentInput + "]").val('');
                    $("[name=total" + currentInput + "]").val(Number(sum).toFixed(2));


                }

            }
   </script>
</asp:Content>
