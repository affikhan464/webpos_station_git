﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.PurchaseReturnReports
{
    public partial class ClassOrWarrentyWisePurchaseReturnSummary : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string classCode)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, classCode);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, string classCode)
        {

            SqlCommand cmd = new SqlCommand(@"Select  PurchaseReturn1.Date1,PurchaseReturn2.Description,Sum(PurchaseReturn2.Qty) as Qty,Sum(PurchaseReturn2.Rate )as Rate, Sum(PurchaseReturn2.Amount) as Amount 
            from PurchaseReturn2
            
            inner join PurchaseReturn1 on PurchaseReturn2.InvNo = PurchaseReturn1.InvNo
            
             inner join InvCode on PurchaseReturn2.Code=InvCode.Code  
            inner  join Class on InvCode.Class =Class.id
            
            where  Class.id='" + classCode + "' and PurchaseReturn1.CompID='" + CompID + "' and InvCode.Nature=1  and PurchaseReturn1.Date1>='" + StartDate + "' and PurchaseReturn1.Date1<='" + EndDate + "' group by PurchaseReturn1.Date1,PurchaseReturn2.Description  ", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);


            var model = new List<InvoiceViewModel>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                invoice.Quantity = Convert.ToString(dt.Rows[i]["Qty"]);
                invoice.Rate = Convert.ToString(dt.Rows[i]["Rate"]);
                invoice.Amount = Convert.ToString(dt.Rows[i]["Amount"]);
                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/DaySaleGroupByItemCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}