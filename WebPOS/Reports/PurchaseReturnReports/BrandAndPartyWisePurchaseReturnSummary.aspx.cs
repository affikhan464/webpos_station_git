﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.PurchaseReturnReports
{
    public partial class BrandAndPartyWisePurchaseReturnSummary : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string partyCode, string brandCode)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, partyCode, brandCode);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, string partyCode, string brandCode)
        {

            SqlCommand cmd = new SqlCommand(@"
                           Select 
                              InvCode.Code  as Code , 
                PurchaseReturn2.Description as Description , 
                Sum(PurchaseReturn2.Qty) as Qty ,
                Sum(PurchaseReturn2.Amount) as Amount,

				Isnull((
                        Select Sum(Purchase2.Qty) from Purchase2
                            inner join Purchase1 on Purchase2.InvNo=Purchase1.InvNo 
				        where Purchase1.Dat>='" + StartDate + @"'  
                        and Purchase1.Dat <= '" + EndDate + @"' 
                        and Purchase2.Code=InvCode.Code 
                        group by Purchase2.Code,Purchase2.Description				
                ),0) as ReturnQty,				
                Isnull((
                        Select Sum(Purchase2.Amount) from Purchase2				
                            inner join Purchase1 on Purchase2.InvNo=Purchase1.InvNo 
                        where Purchase1.Dat >= '" + StartDate + @"'  
                        and Purchase1.Dat <= '" + EndDate + @"' 
                        and Purchase2.Code=InvCode.Code  			
                        group by Purchase2.Code,Purchase2.Description				
                ),0) as ReturnAmount,		
                Isnull((
                        Select Sum(InvCode.qty) from InvCode				
                        where  InvCode.Code=InvCode.Code  			
                ),0) as InHandQty 
                
                FROM PurchaseReturn1                 
                INNER JOIN PurchaseReturn2 ON PurchaseReturn1.InvNo = PurchaseReturn2.InvNo          
                inner join InvCode on PurchaseReturn2.Code=InvCode.Code 
                where  PurchaseReturn1.PartyCode = '" + partyCode + @"' 
                and InvCode.Brand= '" + brandCode + @"' 
                and PurchaseReturn1.CompID='" + CompID + @"' 
                and InvCode.Nature=1 
                and PurchaseReturn1.Date1>='" + StartDate + @"' 
                and PurchaseReturn1.Date1<='" + EndDate + @"' 
                GROUP BY InvCode.Code ,PurchaseReturn2.Description       
                Order by PurchaseReturn2.Description", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();



                invoice.ItemCode = (dt.Rows[i]["Code"]).ToString();
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]).Trim();

                var Quantity = Convert.ToInt32(dt.Rows[i]["ReturnQty"]);
                var ReturnQty = Convert.ToInt32(dt.Rows[i]["Qty"]);
                var ReturnAmount = Convert.ToInt32(dt.Rows[i]["Amount"]);
                var Amount = Convert.ToInt32(dt.Rows[i]["ReturnAmount"]);

                invoice.Quantity = Quantity.ToString();
                invoice.Amount = Amount.ToString();
                invoice.ReturnQty = ReturnQty.ToString();
                invoice.ReturnAmount = ReturnAmount.ToString();
                invoice.NetQtySold = (Quantity - ReturnQty).ToString();
                invoice.NetAmountSold = (Amount - ReturnAmount).ToString();

                invoice.InHandQty = Convert.ToString(dt.Rows[i]["InHandQty"]);

                model.Add(invoice);
            }

            return model;



        }
    }
}