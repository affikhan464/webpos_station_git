﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.PurchaseReturnReports
{
    public partial class PartyWisePurchaseReturnSummary : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string partyCode)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, partyCode);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, string partyCode)
        {

            SqlCommand cmd = new SqlCommand(@"
                           Select 
                             PurchaseReturn1.InvNo as Bill, 
                             PurchaseReturn1.Date1 as date,
                             Sum(PurchaseReturn1.Discount ) as Discount,
                             Sum(PurchaseReturn1.Paid ) as CashPaid

                           from PurchaseReturn1 
                         
                           where PurchaseReturn1.PartyCode = '" + partyCode + "' and PurchaseReturn1.CompID='" + CompID + "' and PurchaseReturn1.Date1>='" + StartDate + "' and PurchaseReturn1.Date1<='" + EndDate + "' GROUP BY PurchaseReturn1.InvNo,PurchaseReturn1.Date1  order by PurchaseReturn1.InvNo     ", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.InvoiceNumber = Convert.ToString(dt.Rows[i]["Bill"]);
                invoice.Date = Convert.ToString(dt.Rows[i]["date"]);
                invoice.NetDiscount = Convert.ToString(dt.Rows[i]["Discount"]);
                invoice.Paid = Convert.ToString(dt.Rows[i]["CashPaid"]);

                model.Add(invoice);
            }

            return model;



        }
    }
}