﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="Color_ColorWiseSaleReturnSummary.aspx.cs" Inherits="WebPOS.Reports.SaleReturnReports.Color_ColorWiseSaleReturnSummary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

			<div class="reports searchAppSection">
				<div class="contentContainer">
					<h2>Color Wise Sale Return Summary</h2>
					<div class="BMSearchWrapper row">
						<!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->
                         <div class="col col-12 col-sm-6 col-md-3">
                            <span class="userlLabel">From</span>
                            <asp:TextBox ID="fromTxbx" CssClass="datetimepicker fromTxbx Date" runat="server"></asp:TextBox>
                            <%--<input id="fromTxbx" class=" datetimepicker fromTxbx"  name="Date" type="text" />--%>
                        </div>
                       <div class="col col-12 col-sm-6 col-md-3">
                            <span class="userlLabel">To</span>
                            <asp:TextBox ID="toTxbx" CssClass="datetimepicker toTxbx Date" runat="server"></asp:TextBox>

                            <%--<input id="toTxbx" class="datetimepicker toTxbx" type="text"  name="Date"/>--%>
                        </div>
                        <div class="col col-12 col-sm-6 col-md-3">
								<select id="BrandNameDD" class="dropdown">
									<option value="" class="selectCl" onclick="manInvSelection(this)">Select Brand Name</option>
						        </select>
							</div>
                         
                        
                       
                        
						 <div class="col col-12 col-sm-3 col-md-2">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-spinner"></i> Get Record</a>
							</div>
						
                        <div class="col col-12 col-sm-3 col-md-1">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
							</div>
					</div><!-- searchAppSection -->
				</div>
			</div><!-- featuredSection -->
	
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="recommendedSection reports allItemsView">


								<div class="itemsHeader">
									<div class="seven phCol">
										<a href="javascript:void(0)"> 
											S. No.
										</a>
									</div>
									<div class="seven phCol">
										<a href="javascript:void(0)"> 
											Code
										</a>
									</div>
									<div class="thirteen phCol">                                                                    
										<a href="javascript:void(0)"> 
											Item Name
										</a>
									</div>
									
                                    
                                    <div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Return Quantity
											
										</a>
									</div>
                                    <div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Item Discount
											
										</a>
									</div>
                                    <div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Deal Rs.
											
										</a>
									</div>
                                    <div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Return Amount
											
										</a>
									</div>
                                    
								</div><!-- itemsHeader -->	
                         <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
								<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow'>
								<div class='seven'><span></span></div>
                                <div class='seven'><span></span></div>
								<div class='thirteen'><span></span></div>
						    	
                                <div class='thirteen'><input  disabled="disabled"  name='totalReturnQty'/></div>
                                <div class='thirteen'><input  disabled="disabled"  name='totalItem_Disc'/></div>
                                <div class='thirteen'><input  disabled="disabled"  name='totalDealRs'/></div>
								<div class='thirteen'><input  disabled="disabled"  name='totalReturnAmount'/></div>
								
								
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
    
   </asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    
    	<script src="<%=ResolveClientUrl("/Script/Vendor/ddslick.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    
    <script src="<%=ResolveClientUrl("/Script/DropDown.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>


   <script type="text/javascript">

            $(document).ready(function () {
                initializeDatePicker();
                appendAttribute.init("BrandNameDD", "Color");
                resizeTable();
            });

            $(window).resize(function () {
                resizeTable();
              
            });

            
            $("#toTxbx").on("keypress", function (e) {
                if (e.key==13) {
                    getReport();
                   
                }
            });
            $("#searchBtn").on("click", function (e) {
              
                    getReport();

             
            });
            function getReport() { $(".loader").show();

                $.ajax({
                    url: '/Reports/SaleReturnReports/Color_ColorWiseSaleReturnSummary.aspx/getReport',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ "from": $(".fromTxbx").val(), "to": $(".toTxbx").val(), "ColorID": $("#BrandNameDD .dd-selected-value").val() }),
                    success: function (response) {
                      

                        if (response.d.Success) {
                            if (response.d.ListofInvoices.length>0) {
                                clearInvoice();
                            
                            for (var i = 0; i < response.d.ListofInvoices.length; i++) {
                               
                              
                                $("<div class='itemRow newRow'>" +
                                    "<div class='seven'><span>" + (Number(i) + 1) + "</span></div>" +
                                    "<div class='thirteen'><span>" + response.d.ListofInvoices[i].ItemCode + "</span></div>" +
                                    "<div class='thirteen'><span>" + response.d.ListofInvoices[i].BrandName + "</span></div>" +
                                    
                                    "<div class='thirteen'><input  value=" + Number(response.d.ListofInvoices[i].ReturnQty).toFixed(2) + " disabled name='ReturnQty'/></div>" +
                                    "<div class='thirteen'><input  value=" + Number(response.d.ListofInvoices[i].Item_Disc).toFixed(2) + " disabled name='Item_Disc'/></div>" +
                                    "<div class='thirteen'><input  value=" + Number(response.d.ListofInvoices[i].DealRs).toFixed(2) + " disabled name='DealRs'/></div>" +
                                    "<div class='thirteen'><input  value=" + Number(response.d.ListofInvoices[i].ReturnAmount).toFixed(2) + " disabled name='ReturnAmount'/></div>" +
                                    
                                    "</div>").appendTo(".dayCashAndCreditSale .itemsSection");


                            }
                            $(".loader").hide();
                            calculateData();
                        
                            } else {
                                $(".loader").hide(); swal({
                                    title: "No Result Found ",
                                    text: "No Result Found!"
                                });
                            }
                         
                        }
                        else {
                            $(".loader").hide();
                            swal({
                                title: "there is some error",
                                text: response.d.Message
                            });
                        }

                    },
                    error: function (error) { swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                    }
                });
            }

            function clearInvoice() {

            
                $(".itemsSection .itemRow").remove();
            }
            function calculateData() {

                var sum;
                var inputs = [ 'ReturnQty', 'Item_Disc', 'DealRs', 'ReturnAmount'];
                for (var i = 0; i < inputs.length; i++) {

                    var currentInput = inputs[i];
                    sum = 0;
                    $("input[name = '" + currentInput + "']").each(function () {

                        if (this.value.trim() === "") {
                            sum = (Number(sum) + Number(0));
                        } else {
                            sum = (Number(sum) + Number(this.value));

                        }

                    });

                    $("[name=total" + currentInput + "]").val('');
                    $("[name=total" + currentInput + "]").val(Number(sum).toFixed(2));


                }

            }




   </script>
  
</asp:Content>
