﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReturnReports
{
    public partial class ItemWiseSaleReturnDetail : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string itemCode)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, itemCode);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, string itemCode)
        {

            SqlCommand cmd = new SqlCommand(@"
                        Select 
                          SaleReturn1.InvNo as Bill, 
                          SaleReturn1.Date1 as date,
                          SaleReturn1.Name,
                          SaleReturn2.Qty as qty,
                            SaleReturn2.ItemDis as ItemDis,
                            isnull(SaleReturn2.DealRs,0) as DealRs,
                          SaleReturn2.Rate  as Rate,
                          SaleReturn2.Amount  as Amount
                          
                          from SaleReturn1 
                          inner join SaleReturn2 on SaleReturn1.InvNo=SaleReturn2.InvNo

                           where 
                            SaleReturn2.Code = '" + itemCode + @"' and 
                            SaleReturn1.CompID='" + CompID + @"' and 
                            SaleReturn1.Date1>='" + StartDate + @"' and 
                            SaleReturn1.Date1<='" + EndDate + @"' 
                           
                            order by SaleReturn1.Date1,SaleReturn1.InvNo     ", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.InvoiceNumber = Convert.ToString(dt.Rows[i]["Bill"]);
                invoice.Date = Convert.ToDateTime(dt.Rows[i]["date"]).ToString("dd-MMM-yyyy");// Convert.ToString(dt.Rows[i]["date"]);
                invoice.PartyName = Convert.ToString(dt.Rows[i]["Name"]);
                invoice.Quantity = Convert.ToString(dt.Rows[i]["qty"]);
                invoice.Item_Disc = Convert.ToString(dt.Rows[i]["ItemDis"]);
                invoice.DealRs = Convert.ToString(dt.Rows[i]["DealRs"]);
                invoice.Rate = Convert.ToString(dt.Rows[i]["Rate"]);
                invoice.Amount = Convert.ToString(dt.Rows[i]["Amount"]);

                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            var itemCode = ItemCode.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/ItemWiseSaleDetailCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&itemCode=" + itemCode);

        }
    }
}