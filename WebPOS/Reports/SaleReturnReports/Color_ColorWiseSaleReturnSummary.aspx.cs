﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReturnReports
{
    public partial class Color_ColorWiseSaleReturnSummary : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string ColorID)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(ColorID, fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }


        public static List<InvoiceViewModel> GetTheReport(String ColorIDD, DateTime StartDate, DateTime EndDate)
        {

            var model = new List<InvoiceViewModel>();


            ModItem objModItem = new ModItem();
            int ColorID = Convert.ToInt32(ColorIDD);
           
            SqlCommand cmd = new SqlCommand(@"SELECT SR2.Code, SR2.Description, sum(SR2.QTY) as SaleReturnQty, sum(SR2.amount) as SaleReturnAmount,
                                            sum(SR2.ItemDis) as ItemDis, sum(SR2.DealRs) as  DealRs
                                                FROM salereturn2 SR2 inner join SaleReturn1  on SaleReturn1.invno = SR2.invno
                                                inner join invcode on SR2.code = invcode.code
                                                 inner join Color on invcode.Color = Color.ID

                                                        WHERE
                                                            SR2.date1 >= '" + StartDate + @"'  and
                                                            SR2.date1 <= '" + EndDate + @"'  and
                                                            invcode.Color = '" + ColorID + @"' 
                                                           

                                                             GROUP BY SR2.Code, SR2.Description 
                                                                ORDER BY SR2.Description ", con);


            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            var dt = new DataTable();
            adpt.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                
                var ReturnQty = Convert.ToInt32(dt.Rows[i]["SaleReturnQty"]);
                var ReturnAmount = Convert.ToInt32(dt.Rows[i]["SaleReturnAmount"]);
                
                invoice.ItemCode= Convert.ToString (dt.Rows[i]["Code"]);
                invoice.BrandName = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                
                invoice.ReturnQty = ReturnQty.ToString();
                invoice.Item_Disc = Convert.ToString(dt.Rows[i]["ItemDis"]);
                invoice.DealRs = Convert.ToString(dt.Rows[i]["DealRs"]);
                invoice.ReturnAmount = ReturnAmount.ToString();
                

                model.Add(invoice);
            }





            return model;
        }

        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            var brandCode = "";// brandCodeTxbx.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/BrandWiseSaleSummaryCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&brandCode=" + brandCode);
        }
    }
}