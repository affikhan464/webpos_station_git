﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReturnReports
{
    public partial class BrandWiseSaleReturnReport1 : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string brandCode)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(brandCode, fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }


        public static List<InvoiceViewModel> GetTheReport(String brandCode, DateTime StartDate, DateTime EndDate)
        {

            var model = new List<InvoiceViewModel>();


            ModItem objModItem = new ModItem();
            int BrandID = Convert.ToInt32(brandCode);
            
            SqlCommand cmd = new SqlCommand(@"SELECT salereturn2.Code, salereturn2.Description, sum(salereturn2.QTY) as SaleReturnQty, sum(salereturn2.amount) as SaleReturnAmount
                                                        FROM 
                                                            salereturn2  inner join SaleReturn1  on SaleReturn1.invno = salereturn2.invno
                                                            inner join invcode on salereturn2.code = invcode.code
                                                            inner join brandname on invcode.brand = brandname.code

                                                        WHERE
                                                            salereturn2.date1 >= '" + StartDate + @"'  and
                                                            salereturn2.date1 <= '" + EndDate + @"'  and
                                                            invcode.Brand = '" + BrandID + @"' 
                                                            
                                                             GROUP BY salereturn2.Code, salereturn2.Description 
                                                             ORDER BY salereturn2.Description ", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            var dt = new DataTable();
            adpt.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();
                //var Quantity = Convert.ToInt32(dt.Rows[i]["SaleQty"]);
                var ReturnQty = Convert.ToInt32(dt.Rows[i]["SaleReturnQty"]);
                var ReturnAmount = Convert.ToInt32(dt.Rows[i]["SaleReturnAmount"]);
                //var Amount = Convert.ToInt32(dt.Rows[i]["SaleAmount"]);


                invoice.PartyName = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                //invoice.Quantity = Quantity.ToString();
                invoice.ReturnQty = ReturnQty.ToString();
                invoice.ReturnAmount = ReturnAmount.ToString();
                //invoice.Amount = Amount.ToString();
                //invoice.NetQtySold = (Quantity - ReturnQty).ToString();
                //invoice.NetAmountSold = (Amount - ReturnAmount).ToString();

                model.Add(invoice);
            }




            return model;
        }

        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            var brandCode = "";// brandCodeTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/BrandWiseSaleSummaryCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&brandCode=" + brandCode);
        }
    }
}