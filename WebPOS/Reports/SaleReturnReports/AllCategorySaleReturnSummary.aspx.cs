﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReturnReports
{
    public partial class AllCategorySaleReturnSummary : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate)
        {

            ////////SqlCommand cmd = new SqlCommand(@"
            ////////            Select   BrandName.Name,sum(Invoice2.Qty) as Qty, Sum(Invoice2.Amount) as Amount from Invoice2 
            ////////            inner join invoice1 on invoice2.InvNo=invoice1.InvNo 
            ////////            inner join InvCode on Invoice2.Code=InvCode.Code 
            ////////            inner join BrandName on InvCode.Brand=BrandName.Code 
            ////////            where  Invoice1.CompID='" + CompID + @"' 
            ////////            and Invoice2.ItemNature=1 
            ////////            and Invoice1.Date1>='" + StartDate + @"' 
            ////////            and invoice1.Date1<='" + EndDate + @"' 
            ////////            group by BrandName.Name 
            ////////            order by BrandName.Name", con);


            SqlCommand cmd = new SqlCommand(@"SELECT Category.ID, Category.Name, sum(SR2.QTY) as SaleReturnQty, sum(SR2.amount) as SaleReturnAmount
                                                            FROM salereturn2 SR2 inner join SaleReturn1 on SR2.invno=salereturn1.invno
                                                            inner join invcode on SR2.code=invcode.code
                                                            inner join Category on invcode.Category=Category.ID

                                                        WHERE

                                                            SR2.date1 >= '" + StartDate + @"'  
                                                            and SR2.date1 <='" + EndDate + @"'
                                                                                                  
                                                            Group by Category.ID, Category.Name  
                                                            ORDER BY Category.Name", con);



            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                //var Quantity = Convert.ToInt32(dt.Rows[i]["SaleQty"]);
                var ReturnQty = Convert.ToInt32(dt.Rows[i]["SaleReturnQty"]);
                var ReturnAmount = Convert.ToInt32(dt.Rows[i]["SaleReturnAmount"]);
                //var Amount = Convert.ToInt32(dt.Rows[i]["SaleAmount"]);


                invoice.ItemCode = (dt.Rows[i]["ID"]).ToString();
                invoice.Description = Convert.ToString(dt.Rows[i]["Name"]);
                //invoice.Quantity = Quantity.ToString();
                invoice.ReturnQty = ReturnQty.ToString();
                invoice.ReturnAmount = ReturnAmount.ToString();
                //invoice.Amount = Amount.ToString();
                //invoice.NetQtySold = (Quantity - ReturnQty).ToString();
                //invoice.NetAmountSold = (Amount - ReturnAmount).ToString();
                // invoice.InHandQty = Convert.ToString(dt.Rows[i]["InHandQty"]);


                model.Add(invoice);
            }

            return model;

        }
        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            //var itemCode = itemCodeTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/ItemWiseSaleGroupByPartyCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&itemCode=" + itemCode);

        }
    }
}