﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebPOS.Reports.SaleReports.CReports
{
    public partial class DateWiseSaleSummaryCR : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {
            var cr = new DateWiseSaleSummary();
            var StartDate =getDate( Request.QueryString["startDate"]); 
            var EndDate =getDate( Request.QueryString["endDate"]);

            SqlCommand cmd = new SqlCommand(@"Select Invoice1.Date1 as [Date],Count(*) as Invoices ,Sum(Invoice3.Total) as Total,Sum(Invoice3.Discount1) as Discount,Sum(Invoice3.CashPaid) as Paid,Sum(Invoice3.Total)-(Sum(Invoice3.Discount1)+Sum(Invoice3.CashPaid)) as Balance from (Invoice1 inner join invoice3 on invoice1.InvNo=invoice3.InvNo) where Invoice1.CompID='" + CompID + "' and  Invoice1.Date1>='" + StartDate + "' and Invoice1.Date1<='" + EndDate + "' GROUP BY Invoice1.Date1 order by Invoice1.Date1 ", con);

            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            con.Open();
            dAdapter.Fill(objDs);
            con.Close();
            var dt = objDs.Tables[0];
            var dset = new DataSet();
            
            var model = new List<Object>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new
                {

                  Date = Convert.ToDateTime(dt.Rows[i]["Date"]).ToString("dd/MM/yyyy"),
                  NumberOfInvoices = Math.Round(Convert.ToDecimal(dt.Rows[i]["Invoices"]),2),
                  NetDiscount = Math.Round(Convert.ToDecimal(dt.Rows[i]["Discount"]),2),
                  Total = Math.Round(Convert.ToDecimal(dt.Rows[i]["Total"]),2),
                  Paid = Math.Round(Convert.ToDecimal(dt.Rows[i]["Paid"]),2),
                  Balance = Math.Round(Convert.ToDecimal(dt.Rows[i]["Balance"]),2)

                };
                model.Add(invoice);
            }

            cr.SetDataSource(model);
            var InstalledPrinters = PrinterSettings.InstalledPrinters.Count;
            //if (InstalledPrinters > 4)
            //var a= cr.PrintOptions.PrinterName.ToString();
            //  cr.PrintToPrinter(1, false, 0, 0);
            //else
            CRViewer.ReportSource = cr;

        }

        public DateTime getDate(string date)
        {
            var newdate = DateTime.ParseExact(date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);
            return newdate;
        }
    }
}