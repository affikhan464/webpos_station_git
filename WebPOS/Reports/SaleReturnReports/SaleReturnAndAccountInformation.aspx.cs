﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReturnReports
{
    public partial class SaleReturnAndAccountInformation : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate)
        {

            SqlCommand cmd = new SqlCommand(@"
                        Select 
                                
                          Invoice2.Description,
                          Sum(Invoice2.Qty) as qty,
                          Sum(Invoice2.Amount ) as Amount
                          
                          from Invoice1 
                         inner join invoice2 on Invoice1.InvNo=Invoice2.InvNo

                           where  Invoice1.CompID='" + CompID + @"'
                           and Invoice1.Date1>='" + StartDate + @"' 
                           and Invoice1.Date1<='" + EndDate + @"' 
                           GROUP BY Invoice2.Description  
                           order by Invoice2.Description     ", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.PartyName = Convert.ToString(dt.Rows[i]["PartyName"]);
                invoice.OpeningBalance = Convert.ToString(dt.Rows[i]["OpeningBalance"]);
                invoice.TotalSale = Convert.ToString(dt.Rows[i]["TotalSale"]);
                invoice.TotalReturn = Convert.ToString(dt.Rows[i]["TotalReturn"]);
                invoice.NetSale = Convert.ToString(dt.Rows[i]["NetSale"]);
                invoice.TotalReceived = Convert.ToString(dt.Rows[i]["TotalReceived"]);
                invoice.TotalPayment = Convert.ToString(dt.Rows[i]["TotalPayment"]);
                invoice.Debit = Convert.ToString(dt.Rows[i]["Debit"]);
                invoice.Credit = Convert.ToString(dt.Rows[i]["Credit"]);
                invoice.ClosingBalance = Convert.ToString(dt.Rows[i]["ClosingBalance"]);



                model.Add(invoice);
            }

            return model;

        }
        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            //var itemCode = itemCodeTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/ItemWiseSaleGroupByPartyCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&itemCode=" + itemCode);

        }
    }
}