﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReturnReports
{
    public partial class PartyWiseItemReturnSummary : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string partyCode)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, partyCode);
                return new BaseModel { Success = true, ListofInvoices=model};

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false,Message=ex.Message};
            }
        }

        public static  List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, string partyCode)
        {
            
            SqlCommand cmd = new SqlCommand(@"
                           Select 
                                SaleReturn2.Description as Description,
                                Sum(SaleReturn2.qty ) as qty,
                                Sum(SaleReturn2.Amount ) as Amount
                           from 
                                SaleReturn2
                                inner join SaleReturn1 on SaleReturn2.InvNo=SaleReturn1.InvNo
                           where 
                                SaleReturn1.PartyCode = '" + partyCode + @"' and 
                                SaleReturn1.CompID='" + CompID + @"' and
                                SaleReturn1.Date1>='" + StartDate + @"' and
                                SaleReturn1.Date1<='" + EndDate + @"' 
                           GROUP BY SaleReturn2.Description  
                           order by SaleReturn2.Description     ", con);
               SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                invoice.Quantity = Convert.ToString(dt.Rows[i]["qty"]);
                invoice.Amount = Convert.ToString(dt.Rows[i]["Amount"]);
            

                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            //var partyCode = PartyCode.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/PartyWiseItemSummaryCR.aspx?startDate=" + startDate + "&endDate="+ endDate+ "&partyCode="+partyCode);
        }
    }
}