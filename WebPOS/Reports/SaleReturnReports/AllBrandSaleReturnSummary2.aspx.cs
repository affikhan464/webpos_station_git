﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReturnReports
{
    public partial class AllBrandSaleReturnSummary2 : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate)
        {

            SqlCommand cmd = new SqlCommand(@"
                select
				BrandName.Name,
                Sum(SaleReturn2.Qty) as Qty ,
                Sum(SaleReturn2.Amount) as Amount,
				Isnull(
				    (Select Sum(invoice2.Qty) from invoice2
                  inner JOIN invoice1 ON invoice2.InvNo = invoice1.InvNo 
				    inner join InvCode on invoice2.Code=InvCode.Code 				
                     where invoice1.Date1>= '" + StartDate + @"'
                    and invoice1.Date1 <= '" + EndDate + @"'
                    and InvCode.Brand = BrandName.Code				
                    group by InvCode.Brand				
                ),0) as ReturnQty,				
                Isnull(				
                    (Select Sum(invoice2.Amount) from invoice2				
                  inner JOIN invoice1 ON invoice2.InvNo = invoice1.InvNo 
			
                   inner join InvCode on invoice2.Code=InvCode.Code 				
                    where invoice1.Date1>= '" + StartDate + @"'
                    and invoice1.Date1 <= '" + EndDate + @"'
                    and InvCode.Brand = BrandName.Code				
                    group by InvCode.Brand					
                ),0) as ReturnAmount
	 
                FROM SaleReturn1  
                iNNER JOIN SaleReturn2 ON SaleReturn1.InvNo = SaleReturn2.InvNo 
                inner join InvCode on SaleReturn2.Code=InvCode.Code
                iNNER JOIN BrandName ON InvCode.Brand = BrandName.Code  
                where SaleReturn1.CompID ='" + CompID + @"' 
                and SaleReturn1.Date1 >= '" + StartDate + @"' 
                and SaleReturn1.Date1 <= '" + EndDate + @"'  
                GROUP BY BrandName.Code, BrandName.Name 
                order by  BrandName.Name   ", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();


                invoice.Description = Convert.ToString(dt.Rows[i]["Name"]);

                var Quantity = Convert.ToInt32(dt.Rows[i]["ReturnQty"]);
                var ReturnQty = Convert.ToInt32(dt.Rows[i]["Qty"]);
                var ReturnAmount = Convert.ToInt32(dt.Rows[i]["Amount"]);
                var Amount = Convert.ToInt32(dt.Rows[i]["ReturnAmount"]);

                invoice.Quantity = Quantity.ToString();
                invoice.Amount = Amount.ToString();
                invoice.ReturnQty = ReturnQty.ToString();
                invoice.ReturnAmount = ReturnAmount.ToString();
                invoice.NetQtySold = (Quantity - ReturnQty).ToString();
                invoice.NetAmountSold = (Amount - ReturnAmount).ToString();

                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            //  Response.Redirect("~/Reports/SaleReports/CReports/ItemAndPartyWiseSaleDetailCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&partyCode=" + partyCode+ "&itemCode=" + itemCode);
        }
    }
}