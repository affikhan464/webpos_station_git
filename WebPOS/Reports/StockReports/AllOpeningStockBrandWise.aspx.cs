﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class AllOpeningStockBrandWise : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string brandId)
        {
            try
            {

                var model = GetTheReport(brandId);
                return new BaseModel { Success = true, PrintReport = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<PrintingTable> GetTheReport(string brandId)
        {

            Module7 objModule7 = new Module7();

            //////SqlCommand cmd = new SqlCommand(@"select 
            //////    InventoryOpeningBalance.ICode as ItemCode,
            //////    Cost_Per_Unit,
            //////    InvCode.Description,
            //////    BrandName.Name as BrandName,
            //////    Convert(numeric(18, 2),
            //////    sum(InventoryOpeningBalance.Opening)) as Opening
            //////    from InventoryOpeningBalance
            //////    inner join InvCode on InventoryOpeningBalance.ICode = InvCode.Code
            //////    join BrandName on InvCode.Brand = BrandName.Code
            //////    where InvCode.CompID = '" + CompID + @"' and
            //////    Opening > 0  and
            //////    Dat = '" + objModule7.StartDate() + @"'
            //////    group by InventoryOpeningBalance.ICode,
            //////    InvCode.Description,
            //////    BrandName.Name,
            //////    InventoryOpeningBalance.Cost_Per_Unit,
            //////    InventoryOpeningBalance.Total_Value
            //////    order by InvCode.Description", con);

            SqlCommand cmd = new SqlCommand("select InventoryOpeningBalance.ICode,InventoryOpeningBalance.Opening,InventoryOpeningBalance.Cost_Per_Unit,InventoryOpeningBalance.Total_Value,InvCode.Description,BrandName.Name as BrandName from (InventoryOpeningBalance inner join InvCode on InventoryOpeningBalance.ICode=InvCode.Code) inner join BrandName on invcode.Brand=BrandName.Code where  InvCode.CompID='" + CompID + "' and  InvCode.Brand=" + brandId + " and Opening >0  and Dat='" + objModule7.StartDate() + " ' order by InvCode.Description", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<PrintingTable>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var report = new PrintingTable();
                var Quantity = Convert.ToInt32(dt.Rows[i]["Opening"]);
                var cost = Convert.ToInt32(dt.Rows[i]["Cost_Per_Unit"]);
                report.Text_1 = Convert.ToString(dt.Rows[i]["ICode"]);
                report.Text_2 = Convert.ToString(dt.Rows[i]["BrandName"]);
                report.Text_3 = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                report.Text_4 = Convert.ToString(dt.Rows[i]["Opening"]);
                report.Text_5 = cost.ToString();
                report.Text_6 = (Quantity * cost).ToString();
                model.Add(report);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}