﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class BrandAndItemWiseStockSummery : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(int brandId, string searchText)
        {
            try
            {

                var model = GetTheReport(brandId, searchText);
                return new BaseModel { Success = true, Reports = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<ReportViewModel> GetTheReport(int brandId, string searchText)
        {
            var module7 = new Module7();
            var adpt2 = new SqlDataAdapter("Select * From Station", con);

            var brandWhereClause = string.Empty;
            if (brandId != 0)
            {
                brandWhereClause = " and BrandName.Code = " + brandId + " ";
            }

            var searchTextWhereClause = string.Empty;
            if (searchText.Trim() != string.Empty)
            {
                searchTextWhereClause = " and Description like '%" + searchText + "%' ";
            }
            DataTable dt1 = new DataTable();
            adpt2.Fill(dt1);
            var stationIds = new List<int>();
            var stationNames= new List<string>();

            for (int i = 0; i < dt1.Rows.Count; i++)
            {
                stationIds.Add(Convert.ToInt32(dt1.Rows[i]["Id"]));
                stationNames.Add(dt1.Rows[i]["Name"].ToString());
            }
            var quantities = string.Empty;
            for (int i = 0; i < stationIds.Count; i++)
            {
                quantities += "IsNull((SELECT Current_QTY FROM InventoryOpeningBalance Where StationID = " + stationIds[i] + " and ICode = InvCode.code and Dat = '" + module7.StartDate().ToString("MM/dd/yyyy") + @"' ), 0) as Quantity"+ (i+1) +", ";
            }
            SqlCommand cmd = new SqlCommand(@"select InvCode.Code as ItemCode,BrandName.Name as BrandName,LTRIM(Description) as Description," +
                quantities +
                "qty," +
                "isnull(InvCode.Ave_Cost,0) as  Rate ,(isnull(InvCode.Ave_Cost,0) * InvCode.qty) as Amount from InvCode inner join BrandName on InvCode.Brand=BrandName.Code  " +
                "where  InvCode.CompID='" + CompID + "' "+brandWhereClause + searchTextWhereClause +"  and  InvCode.Nature=1 and InvCode.Active=1 and InvCode.qty>0 order by Description", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<ReportViewModel>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new ReportViewModel();

                invoice.ItemCode = Convert.ToString(dt.Rows[i]["ItemCode"]);
                invoice.BrandName = Convert.ToString(dt.Rows[i]["BrandName"]);
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]).Trim().Trim();
                invoice.Quantity = Convert.ToString(dt.Rows[i]["qty"]);
                invoice.QuantityCount = dt1.Rows.Count;
                invoice.Quantities = new List<int>();
                for (int j = 0; j < dt1.Rows.Count; j++)
                {
                    var qty = Convert.ToInt32(dt.Rows[i]["Quantity" + (j + 1)]);
                    invoice.Quantities.Add(qty);
                }
                invoice.StationNames = new List<string>();
                for (int j = 0; j < stationNames.Count; j++)
                {
                    invoice.StationNames.Add(stationNames[j]);
                }
                invoice.Rate = Convert.ToString(dt.Rows[i]["Rate"]);
                var totalQty = invoice.Quantities.Sum();
                invoice.Amount = (totalQty * Convert.ToDecimal(invoice.Rate)).ToString();
                model.Add(invoice);
            }

            return model.OrderBy(a=>a.Description).ToList();



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}