﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="StationStockReportBrandWise.aspx.cs" Inherits="WebPOS.Reports.SaleReports.StationStockReportBrandWise" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



    <div class="reports searchAppSection">
        <div class="contentContainer">
            <h2>All Stock</h2>

            <div class="BMSearchWrapper clearfix row align-items-end">
                <div class="col-12 col-md-3">
                    <label>Brand</label>
                    <div class="d-inline-flex w-100">
                        <span class="input input--hoshi checkbox--hoshi float-left w-auto p-0 pt-1 h-auto">
                            <label>
                                <input id="brandCheck" class="checkbox" type="checkbox" />
                                <span></span>
                            </label>
                        </span>
                        <select id="BrandNameDD" class="dropdown" data-type="BrandName">
                            <option value="0">Select Brand</option>
                        </select>
                    </div>
                </div>
                <div class="col-12 col-md-3">
                    <a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-list-ul"></i>Get Report</a>
                </div>
                <div class="col-12 col-md-3">
                    <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>Print</a>
                </div>
            </div>
            <!-- searchAppSection -->
        </div>
    </div>
    <!-- featuredSection -->

    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">

        <div class="contentContainer">

            <div class="reports recommendedSection allItemsView">


                <div class="itemsHeader">
                    <div class="two phCol">
                        <a href="javascript:void(0)">S#
                        </a>
                    </div>
                    <div class="three phCol">
                        <a href="javascript:void(0)">Item Code
                        </a>
                    </div>

                    <div class="fifteen  phCol">
                        <a href="javascript:void(0)">Item Name
                        </a>
                    </div>
                    <div class="seven phCol">
                        <a href="javascript:void(0)">Brand Name
                        </a>
                    </div>
                    <div class="three phCol">
                        <a href="javascript:void(0)">Qty
                        </a>
                    </div>
                    <div class="seven">
                        <a href="javascript:void(0)">Selling Price
                        </a>
                    </div>
                    <div class="seven">
                        <a href="javascript:void(0)">Amount
                        </a>
                    </div>

                </div>
                <!-- itemsHeader -->
                <div class="loader">
                    <div class="display-table">
                        <div class="display-table-row">
                            <div class="display-table-cell">
                                <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="itemsSection">
                </div>
                <div class="itemsFooter">
                    <div class='itemRow newRow'>
                        <div class='two'><span></span></div>
                        <div class='three'><span></span></div>
                        <div class='fifteen'><span></span></div>
                        <div class='seven'><span></span></div>
                        <div class='three'>
                            <input disabled="disabled" name='totalQuantity' /></div>
                        <div class='seven'><span></span></div>
                        <div class='seven'>
                            <input disabled="disabled" name='totalAmount' /></div>

                    </div>
                </div>

            </div>

        </div>
        <!-- recommendedSection -->



    </div>

</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    <script src="<%=ResolveClientUrl("/Script/Dropdown.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script type="text/javascript">


        $(document).ready(function () {
            appendAttribute.init("BrandNameDD", "BrandName");
            resizeTable();
        });

        $(window).resize(function () {
            resizeTable();

        });

        $(document).on('click', '#print', function () {
            var brandId = 0;
            var stationId = $("#AppStationId").val();
            var multiplyTo = 2;
            
            if ($("#brandCheck").prop("checked")) {
                brandId = $("#BrandNameDD .dd-selected-value").val();
            }

            window.open(
                '/Reports/StockReports/Prints/StationStockReportBrandWisePrint.aspx?type=individual&brandId=' + brandId + '&stationId=' + stationId + '&multiplyTo=' + multiplyTo,
                '_blank'
            );
        })
        $("#searchBtn").on("click", function (e) {

            $("#currentPage").remove();
            $("<input hidden id='currentPage' value='" + 0 + "' />").appendTo("body");
            clearInvoice();
            getReport();

        });
        function getReport() {
            $(".loader").show(); $(".loader").show();
            var data = {
                brandId: 0,
                stationId: $("#AppStationId").val(),
                multiplyTo: 2
            };
            if ($("#brandCheck").prop("checked")) {
                data.brandId = $("#BrandNameDD .dd-selected-value").val();
            }

            var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? "0" : $("#currentPage").val();
            $(".loader").show();
            $.ajax({
                url: '/Reports/StockReports/StationStockReportBrandWise.aspx/getReport',
                type: "POST",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",

                success: function (response) {


                    if (response.d.Success) {
                        if (response.d.Reports.length > 0) {

                            for (var i = 0; i < response.d.Reports.length; i++) {
                                var count = i + 1;
                                $("<div class='itemRow newRow'>" +
                                    "<div class='two'><span>" + count + "</span></div>" +
                                    "<div class='three'><span>" + response.d.Reports[i].ItemCode + "</span></div>" +
                                    "<div class='fifteen name'><span class='font-size-12' >" + response.d.Reports[i].Description + "</span></div>" +
                                    "<div class='seven name'><input value='" + response.d.Reports[i].BrandName + "' disabled /></div>" +
                                    "<div class='three'><input name='Quantity' type='text' value=" + parseFloat(response.d.Reports[i].Quantity).toFixed(2) + " disabled /></div>" +
                                    "<div class='seven'><input name='SellingPrice' value=" + parseInt(response.d.Reports[i].SellingCost).toFixed(2) + " disabled /></div>" +
                                    "<div class='seven'><input name='Amount'  id='newAmountTextbox_" + i + "' value=" + parseInt(response.d.Reports[i].Amount).toFixed(2) + " disabled /></div>" +
                                    "</div>").appendTo(".dayCashAndCreditSale .itemsSection");
                            }
                            $(".loader").hide();
                            calculateData();

                        } else if (response.d.Reports.length == 0 && currentPage == "0") {
                            $(".loader").hide();
                            swal({
                                title: "No Result Found ", type: 'error',
                                text: "No Result Found!"
                            });
                        }

                    }
                    else {
                        $(".loader").hide();
                        swal({
                            title: "there is some error",
                            text: response.d.Message
                        });
                    }

                },
                error: function (error) {
                    swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                }
            });
        }

        function clearInvoice() {


            $(".itemsSection .itemRow").remove();
        }
        function calculateData() {

            var sum;
            var inputs = ["Quantity", 'Amount'];
            for (var i = 0; i < inputs.length; i++) {

                var currentInput = inputs[i];
                sum = 0;
                $("input[name = '" + currentInput + "']").each(function () {

                    if (this.value.trim() === "") {
                        sum = (parseFloat(sum) + parseFloat(0));
                    } else {
                        sum = (parseFloat(sum) + parseFloat(this.value));

                    }

                });

                $("[name=total" + currentInput + "]").val('');
                $("[name=total" + currentInput + "]").val(parseFloat(sum).toFixed(2));


            }

        }
    </script>
</asp:Content>
