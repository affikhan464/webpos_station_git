﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class BrandWiseStockReport_1 : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string BrandID)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, BrandID);
                return new BaseModel { Success = true, PrintReport = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<PrintingTable> GetTheReport(DateTime StartDate, DateTime EndDate , string BrandID)
        {
            var module7 = new Module7();
            ModViewInventory objModViewInventory = new ModViewInventory();
            ModItem objModItem = new ModItem();
            var brandWhereClause = string.Empty;
            if (BrandID != "0")
            {
                brandWhereClause = " and BrandName.Code = " + BrandID + " ";
            }

            ////var searchTextWhereClause = string.Empty;
            ////if (searchText.Trim() != string.Empty)
            ////{
            ////    searchTextWhereClause = " and Description like '%" + searchText + "%' ";
            ////}



            //////SqlCommand cmd = new SqlCommand(@"select InvCode.Code as ItemCode,BrandName.Name as BrandName,Description," +
            //////    "qty," +
            //////    "isnull(InvCode.Ave_Cost,0) as  Rate ,(isnull(InvCode.Ave_Cost,0) * InvCode.qty) as Amount from InvCode inner join BrandName on InvCode.Brand=BrandName.Code  " +
            //////    "where  InvCode.CompID='" + CompID + "' "+brandWhereClause + searchTextWhereClause +"  and  InvCode.Nature=1 and InvCode.Active=1 and InvCode.qty>0 order by Description", con);

            SqlCommand cmd = new SqlCommand("select InvCode.Code as ItemCode,BrandName.Name as BrandName,Description,qty,isnull(InvCode.Ave_Cost,0) as  Rate ,(isnull(InvCode.Ave_Cost,0) * InvCode.qty) as Amount from InvCode inner join BrandName on InvCode.Brand=BrandName.Code where  InvCode.CompID='" + CompID + "'" + brandWhereClause + " and InvCode.Nature=1 order by Description", con);




            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);






            var model = new List<PrintingTable >();
            decimal Openint = 0;
            decimal PurQty = 0;
            decimal SaleRetQty = 0;
            decimal SoldQty = 0;
            decimal PurRetQty = 0;
            decimal Amount = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var PTRow = new PrintingTable();
                Openint = objModViewInventory.UpToDateOpeningBalanceNormal(Convert.ToDecimal(Convert.ToString(dt.Rows[i]["ItemCode"])), StartDate);
                PurQty = objModItem.ItemPurchasedQtyAgainstItemCode(Convert.ToDecimal(Convert.ToString(dt.Rows[i]["ItemCode"])), StartDate, EndDate);
                SaleRetQty = objModItem.ItemSaleReturnedQtyAgainstItemCode(Convert.ToDecimal(Convert.ToString(dt.Rows[i]["ItemCode"])), StartDate, EndDate);

                SoldQty =  objModItem.ItemSoldQtyAgainstItemCode(Convert.ToDecimal(Convert.ToString(dt.Rows[i]["ItemCode"])), StartDate, EndDate);
                PurRetQty = objModItem.ItemPurchasedReturnAgainstItemCode(Convert.ToDecimal(Convert.ToString(dt.Rows[i]["ItemCode"])), StartDate, EndDate);

                if (Openint != 0 || PurQty!=0 || SaleRetQty!=0 || SoldQty!=0 || PurRetQty!=0)
                {
                    PTRow.ItemCode = Convert.ToString(dt.Rows[i]["ItemCode"]);
                    PTRow.BrandNAme = Convert.ToString(dt.Rows[i]["BrandName"]);
                    PTRow.ItemName = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                    PTRow.Opening = Convert.ToString(Openint);
                    PTRow.PurchasedQty = Convert.ToString(PurQty);
                    PTRow.SaleReturnQty = Convert.ToString(SaleRetQty);
                    PTRow.SoldQty = Convert.ToString(SoldQty);
                    PTRow.PurchaseReturnQty = Convert.ToString(PurRetQty);
                    PTRow.InHandQty= Convert.ToString(dt.Rows[i]["Qty"]);
                    PTRow.Rate = Convert.ToString(dt.Rows[i]["Rate"]);

                    if (Convert.ToDecimal(PTRow.InHandQty) > 0)
                    {
                        Amount = (Convert.ToDecimal(PTRow.InHandQty) * Convert.ToDecimal(PTRow.Rate));
                    }
                    PTRow.Amount = Convert.ToString(Amount);
                    Amount = 0;
                    model.Add(PTRow);
                }
            }

            return model.OrderBy(a=>a.Text_3).ToList();



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}