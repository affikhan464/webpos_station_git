﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BrandAndItemWiseStockSummeryPrint.aspx.cs" Inherits="WebPOS.BrandAndItemWiseStockSummeryPrint" %>

<%--<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.4000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>--%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Print</title>
    <link rel="stylesheet" href="/css/vendor.min.css" />
    <link rel="stylesheet" href="/css/print.css" />

</head>
<body class="p-3">
    <form id="form1" runat="server">
        <%--<div>
            <CR:CrystalReportViewer ID="CRViewer" runat="server" AutoDataBind="true" HasPrintButton="True" PrintMode="ActiveX" GroupTreeStyle-ShowLines="True" />
        </div>--%>
        <%--  <div class="mx-auto w-25 text-center">
            <a class="printPageBtn"><i class="fas fa-print fa-4x p-3"></i></a>
        </div>--%>
        <div id="Page" class=" p-3 py-5">
            <%@ Register Src="~/_PrintHead.ascx" TagName="_PrintHead" TagPrefix="uc" %>
            <uc:_PrintHead ID="_PrintHead1" runat="server" />
            <div class="row mb-3">
                <div class="col-6 ">
                    <strong>BRAND:</strong> <span class="BrandName"></span>
                </div>
                <div class="col-6 text-right">
                    <strong>DATE:</strong> <span class="Date1"><%=DateTime.Now.ToString("dd/MM/yyyy") %></span>
                </div>
            </div>
            <table>
                <thead>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </form>

    <script src="/js/vendor.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="/Script/Vendor/sweetalert.min.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="/Script/GetQueryString.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script>

        $(document).ready(function () {

            $.ajax({
                url: "/Reports/StockReports/Prints/BrandAndItemWiseStockSummeryPrint.aspx/GetReport",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ "brandId": getQueryString("brandId"),  "searchText": getQueryString("searchText")}),
                success: function (BaseModel) {
                    debugger
                    if (BaseModel.d.Success) {


                        if (BaseModel.d.Data.DRows.length) {

                            var rows = BaseModel.d.Data.DRows;

                            // Columns
                            var headrow = "";
                            for (var j = 0; j < rows[0].length; j++) {
                                var key = rows[0][j].Key.split("___");
                                headrow += `<th>${key[0]}</th>`;
                            }
                            //Rows
                            $("table thead").append(headrow);
                            var invrows = '';
                            for (var i = 0; i < rows.length; i++) {
                                var row = '<tr>';
                                for (var j = 0; j < Object.values(rows[i]).length; j++) {
                                    var tdclass = '';
                                    var key = rows[i][j].Key.split("___");

                                    var value = rows[i][j].Value;
                                    if (key[0] == 'Rate' || key[0] == 'Amount') {
                                        tdclass = 'class="text-right"';
                                        value = (Number(value)).toFixed(2);
                                    }
                                    if (key[1]) {
                                        key = key[1];
                                    } else {
                                        key = key[0];
                                    }
                                    row += `<td name="${key}" ${tdclass}>${value}</td>`;

                                }
                                row += '</tr>';
                                invrows += row;
                            }

                            var footerrow = '<tr>';
                            for (var j = 0; j < Object.values(rows[0]).length; j++) {
                                var key = rows[0][j].Key.split("___");
                                if (key[1]) {
                                    key = key[1];
                                } else {
                                    key = key[0];

                                }
                                footerrow += `<td class="Total${key} font-weight-bold text-right"></td>`;
                            }
                            footerrow += '</tr>';
                            invrows += footerrow;
                            $("table tbody").append(invrows);

                            var data = BaseModel.d.Data;
                            for (var j = 0; j < Object.keys(data).length; j++) {
                                var key = Object.keys(data)[j];
                                 var value = Object.values(data)[j];
                                 if (key != "Rows") {
                                     if (isNaN(value) || value == ""|| key == "InvoiceNumber"|| value == null) {
                                        $("." + key).text(value);
                                    } else {
                                        $("." + key).text((Number(value)).toFixed(2));
                                    }
                                }
                            }

                            //Calculate Quantities
                            var inputs = $("tr:first-child [name*=Qty]");
                            for (var i = 0; i < inputs.length; i++) {

                                var currentInput = inputs[i].attributes.name.value;
                                var sum = 0;

                                $("[name = '" + currentInput + "']").each(function () {

                                    if (this.innerText.trim() === "") {
                                        sum = (Number(sum) + Number(0));
                                    } else {
                                        sum = (Number(sum) + Number(this.innerText));

                                    }

                                });



                                $(".Total" + currentInput).text('');
                                $(".Total" + currentInput).text((sum).toFixed(2));


                            }

                            $("td").each(function () {
                                if (!$(this).text().trim().length) {
                                    $(this).addClass("border-0");
                                }
                            });


                            window.print()
                        }

                    }
                    else if (BaseModel.d.Success == false && BaseModel.d.LoginAgain == true) {
                        swal({
                            title: "Login Again",
                            html:
                                `<a target="_blank" href="/?c=1" style="display:block;color:black;font-weight:700">Login Here and print it again.</a> `,

                            type: "warning"

                        });
                         $("table tbody").append("<tr><td><h2 class='text-center'><a onclick='return location.href=location.href;'>Refresh this Page after login</a></h1></td></tr>");
                    }
                    else {
                        swal("Error", BaseModel.d.Message, "error");
                    }


                }



            })
        })
    </script>
</body>
</html>
