﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS
{
    public partial class AllStockBrandAndStationWisePrint : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        { }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel GetReport(int brandId, int stationId, int multiplyTo)
        {
            if (HttpContext.Current.Session["UserId"] == null)
            {
                return new BaseModel() { Success = false, LoginAgain = true };
            }
            try
            {


                var module7 = new Module7();
                var brandCheck = string.Empty;
                var stationCheck = string.Empty;
                var stationQtyWhereClause = string.Empty;
                SqlCommand cmd;
                if (brandId != 0)
                    brandCheck = " and BrandName.Code=" + brandId;
                if (stationId != 0)
                {

                    stationCheck = @",
                    IsNull((SELECT Current_QTY
                        FROM InventoryOpeningBalance
                        Where StationID = " + stationId + @" and ICode = InvCode.code and Dat = '" + module7.StartDate().ToString("MM/dd/yyyy") + @"'
                    ),0) 
                    as stationqty";
                    stationQtyWhereClause = "  WHERE stationqty>0  ";
                }

                cmd = new SqlCommand("SELECT * FROM ( select BrandName.Name as BrandName " + stationCheck + ", InvCode.Code as ItemCode,LTRIM(Description) as Description,qty, isnull(InvCode.Ave_Cost,0) as 'UnitPrice',(isnull(InvCode.Ave_Cost,0) * InvCode.qty) as Value,isnull(InvCode.SellingCost,0) as SellingCost,isnull(InvCode.PurchasingCost,0) as PurchasingCost from InvCode " +
             "inner join BrandName on InvCode.Brand=BrandName.Code  " +
             "where  InvCode.CompID='" + CompID + "' and " +
             " InvCode.Nature=1 and InvCode.Active=1 " +
              brandCheck +
              " and InvCode.qty>0 ) as InnerTable  " + stationQtyWhereClause + " order by Description", con);



                DataTable dt = new DataTable();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed)
                {
                    if (con.State == ConnectionState.Closed) { con.Open(); }
                }

                dAdapter.Fill(dt);
                con.Close();



                var rows = new List<object>();

                var model = new PrintViewModel<object>();

                model.BrandName = dt.Rows[0]["BrandName"].ToString();
                var stationName = "Report from Different Stations";
                if (stationId != 0)
                {
                    var adpt2 = new SqlDataAdapter("Select * From Station Where Id=" + stationId, con);
                    DataTable dt1 = new DataTable();
                    adpt2.Fill(dt1);
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        stationName = "Report from Station: "+ dt1.Rows[i]["Name"].ToString();
                    }
                }
                model.ReportStationName = stationName;
                decimal TotalQty = 0;
                decimal TotalAmount = 0;
               
                    for (int i = 0; i < dt.Rows.Count; i++)
                {
                    try
                    {
                        if (Convert.ToString(dt.Rows[i]["ItemCode"])== "4495")
                        {

                        }
                        var brandName = dt.Rows[0]["BrandName"].ToString();
                        if (brandName != dt.Rows[i]["BrandName"].ToString())
                        {
                            model.BrandName = "Different Brands";
                        }

                        var qty = stationId != 0 ? Convert.ToString(dt.Rows[i]["stationqty"]) : Convert.ToString(dt.Rows[i]["qty"]);
                        var rate = Convert.ToString(dt.Rows[i]["UnitPrice"]);
                        var sellingCost = Convert.ToString(dt.Rows[i]["SellingCost"]);
                        var purchasingCost = Convert.ToString(dt.Rows[i]["PurchasingCost"]);
                        var amount = "0";
                        if (multiplyTo == 1)
                        {
                            amount = (Convert.ToDecimal(qty) * Convert.ToDecimal(rate)).ToString();
                            TotalAmount += Convert.ToDecimal(qty) * Convert.ToDecimal(rate);
                        }
                        else if (multiplyTo == 2)
                        {
                            amount = (Convert.ToDecimal(qty) * Convert.ToDecimal(sellingCost)).ToString();
                            TotalAmount += Convert.ToDecimal(qty) * Convert.ToDecimal(sellingCost);
                        }
                        else if (multiplyTo == 3)
                        {
                            amount = (Convert.ToDecimal(qty) * Convert.ToDecimal(purchasingCost)).ToString();
                            TotalAmount += Convert.ToDecimal(qty) * Convert.ToDecimal(purchasingCost);
                        }

                        var row = new
                        {
                            ItemCode = Convert.ToString(dt.Rows[i]["ItemCode"]),
                            BrandName = Convert.ToString(dt.Rows[i]["BrandName"]),
                            Description = Convert.ToString(dt.Rows[i]["Description"]).Trim(),
                            Qty = qty,
                            Rate = rate,
                            SellingCost = sellingCost,
                            PurchasingCost = purchasingCost,
                            Amount = amount
                        };
                        TotalQty += Convert.ToDecimal(qty);

                        rows.Add(row);
                    }

                    catch (Exception ex)
                    {

                        return new BaseModel() { Success = false, Message = ex.Message };
                    }
                }
                
                model.Rows = rows;
                //if (model.Rows.Any())
                //{
                model.TotalQty = TotalQty;
                model.TotalAmount = TotalAmount.ToString();
                //}
                return new BaseModel() { Success = true, Data = model };

            }
            catch (Exception ex)
            {

                return new BaseModel() { Success = true, Message = ex.Message };

            }
        }

        public DateTime getDate(string date)
        {
            var newdate = DateTime.ParseExact(date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);
            return newdate;
        }
    }
}