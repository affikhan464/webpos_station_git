﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS
{
    public partial class BrandWiseStockSummeryPrint : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        { }
        public static List<ReportViewModel> GetTheReport(string brandId)
        {
            var module7 = new Module7();
            var adpt2 = new SqlDataAdapter("Select * From Station", con);

            DataTable dt1 = new DataTable();
            adpt2.Fill(dt1);
            var stationIds = new List<int>();
            var stationNames = new List<string>();

            for (int i = 0; i < dt1.Rows.Count; i++)
            {
                stationIds.Add(Convert.ToInt32(dt1.Rows[i]["Id"]));
                stationNames.Add(dt1.Rows[i]["Name"].ToString());
            }
            var quantities = string.Empty;
            for (int i = 0; i < stationIds.Count; i++)
            {
                quantities += "IsNull((SELECT Current_QTY FROM InventoryOpeningBalance Where StationID = " + stationIds[i] + " and ICode = InvCode.code and Dat = '" + module7.StartDate().ToString("MM/dd/yyyy") + @"' ), 0) as Quantity" + (i + 1) + ", ";
            }
            SqlCommand cmd = new SqlCommand(@"select InvCode.Code as ItemCode,BrandName.Name as BrandName,Description," +
                quantities +
                "qty," +
                "isnull(InvCode.Ave_Cost,0) as  Rate ,(isnull(InvCode.Ave_Cost,0) * InvCode.qty) as Amount from InvCode inner join BrandName on InvCode.Brand=BrandName.Code  where BrandName.Code=" + brandId + " and InvCode.CompID='" + CompID + "' and  InvCode.Nature=1 and InvCode.Active=1 and InvCode.qty>0 order by Brand ,Description", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<ReportViewModel>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new ReportViewModel();

                invoice.ItemCode = Convert.ToString(dt.Rows[i]["ItemCode"]);
                invoice.BrandName = Convert.ToString(dt.Rows[i]["BrandName"]);
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                invoice.Quantity = Convert.ToString(dt.Rows[i]["qty"]);
                invoice.QuantityCount = dt1.Rows.Count;
                invoice.Quantities = new List<int>();
                for (int j = 0; j < dt1.Rows.Count; j++)
                {
                    var qty = Convert.ToInt32(dt.Rows[i]["Quantity" + (j + 1)]);
                    invoice.Quantities.Add(qty);
                }
                invoice.StationNames = new List<string>();
                for (int j = 0; j < stationNames.Count; j++)
                {
                    invoice.StationNames.Add(stationNames[j]);
                }
                invoice.Rate = Convert.ToString(dt.Rows[i]["Rate"]);
                var totalQty = invoice.Quantities.Sum();
                invoice.Amount = (totalQty * Convert.ToDecimal(invoice.Rate)).ToString();
                model.Add(invoice);
            }

            return model;



        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel GetReport(string brandId)
        {
            if (HttpContext.Current.Session["UserId"] == null)
            {
                return new BaseModel() { Success = false, LoginAgain = true };
            }
            try
            {
                var module7 = new Module7();
                var adpt2 = new SqlDataAdapter("Select * From Station", con);

                DataTable dt1 = new DataTable();
                adpt2.Fill(dt1);
                var stationIds = new List<int>();
                var stationNames = new List<string>();

                for (int i = 0; i < dt1.Rows.Count; i++)
                {
                    stationIds.Add(Convert.ToInt32(dt1.Rows[i]["Id"]));
                    stationNames.Add(dt1.Rows[i]["Name"].ToString());
                }
                var quantities = string.Empty;
                for (int i = 0; i < stationIds.Count; i++)
                {
                    quantities += "IsNull((SELECT Current_QTY FROM InventoryOpeningBalance Where StationID = " + stationIds[i] + " and ICode = InvCode.code and Dat = '" + module7.StartDate().ToString("MM/dd/yyyy") + @"' ), 0) as Quantity" + (i + 1) + ", ";
                }
                SqlCommand cmd = new SqlCommand(@"select InvCode.Code as ItemCode,BrandName.Name as BrandName,LTRIM(Description) as Description," +
                    quantities +
                    "qty," +
                    "isnull(InvCode.Ave_Cost,0) as  Rate ,(isnull(InvCode.Ave_Cost,0) * InvCode.qty) as Amount from InvCode inner join BrandName on InvCode.Brand=BrandName.Code  where BrandName.Code=" + brandId + " and InvCode.CompID='" + CompID + "' and  InvCode.Nature=1 and InvCode.Active=1 and InvCode.qty>0 order by Description", con);

                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adpt.Fill(dt);

                var model = new PrintViewModel<object>();
                model.BrandName = dt.Rows[0]["BrandName"].ToString();
                var myUnderlyingObject = new List<IDictionary<string, object>>();
                decimal totalAmount = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dynamic invoice = new ExpandoObject();
                    IDictionary<string, object> obj = invoice;

                    obj.Add("ItemCode", dt.Rows[i]["ItemCode"].ToString());
                    obj.Add("Description", dt.Rows[i]["Description"].ToString().Trim());
                  
                    var totalQty = 0;
                    for (int j = 0; j < dt1.Rows.Count; j++)
                    {
                        var qty = Convert.ToInt32(dt.Rows[i]["Quantity" + (j + 1)]);
                        totalQty += qty;
                        obj.Add(stationNames[j]+"___Qty"+(j+1), qty.ToString());
                    }
                    obj.Add("Rate", dt.Rows[i]["Rate"].ToString());
                    totalAmount += (totalQty * Convert.ToDecimal(invoice.Rate));
                    obj.Add("Amount", (totalQty * Convert.ToDecimal(invoice.Rate)).ToString());
                    myUnderlyingObject.Add(obj);
                }
                model.DRows = myUnderlyingObject.OrderBy(a => a["Description"]).ToList();
                model.TotalAmount = totalAmount.ToString();


                return new BaseModel() { Success = true, Data = model };

            }
            catch (Exception ex)
            {

                return new BaseModel() { Success = true, Message = ex.Message };

            }
        }

        public DateTime getDate(string date)
        {
            var newdate = DateTime.ParseExact(date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);
            return newdate;
        }
    }
}