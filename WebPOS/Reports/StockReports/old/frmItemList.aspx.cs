﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS.Reports.StockReports
{
    public partial class frmItemList : System.Web.UI.Page
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {


            LoadItemList();


        }

        void LoadItemList()
        {





            SqlCommand cmd = new SqlCommand("Select  Code,BarCode,Description,Convert(numeric(18,2), SellingCost) as SellingCost , Convert(numeric(18,2), Ave_Cost) as Ave_Cost , Convert(numeric(18,2), Qty) as Qty from InvCode where  CompID='" + CompID + "' order by Description", con);


            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);

            GridView1 .DataSource = dset;
            GridView1.DataBind();


        }


        decimal TotQty = 0;
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TotQty += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Qty"));
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";
                e.Row.Cells[4].Text = TotQty.ToString();
             
                e.Row.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Font.Bold = true;
            }
        }




    }
}