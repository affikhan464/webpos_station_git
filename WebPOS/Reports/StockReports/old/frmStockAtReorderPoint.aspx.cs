﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebPOS.Reports.StockReports
{
    public partial class frmStockAtReorderPoint : System.Web.UI.Page
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadBrandList();
            }
        }


        void LoadBrandList()
        {

            SqlCommand cmd = new SqlCommand("select Name from BrandName where CompID='" + CompID + "' order by name", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);
            lstBrand.DataSource = dset;
            lstBrand.DataBind();
            lstBrand.DataTextField = "Name";
            lstBrand.DataBind();

        }

        void LoadReport(String BrandName)
        {


            

            if (chkBrand.Checked == false) {
                SqlCommand cmd = new SqlCommand("Select Code,Description,qty,ReorderLevel,ReorderQty,isnull(Ave_Cost,0) as Ave_Cost from invCode where  CompID='" + CompID + "' and  qty<ReorderLevel and Visiable=1 order by description", con);


            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);

            GridView1.DataSource = dset;
            GridView1.DataBind();
           }
            else
                if (chkBrand.Checked == true)
                {
                   // ModItem objModItem = new ModItem();
                    ModItem objModItem = new ModItem();
                    int BrandID = objModItem.BrandCodeAgainstBrandName(BrandName);
                    SqlCommand cmd = new SqlCommand("Select Code,Description,qty,ReorderLevel,ReorderQty,isnull(Ave_Cost,0) as Ave_Cost from invCode where  CompID='" + CompID + "' and  qty<ReorderLevel and Visiable=1 and Brand='" + BrandID + "' order by description", con);


                    SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                    DataSet dset = new DataSet();
                    adpt.Fill(dset);
                    GridView1.DataSource = dset;
                    GridView1.DataBind();


                }


        }

        
       


        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                LoadReport(lstBrand.SelectedItem.Value);
            }
            catch (Exception ex)
            {
                Response.Write("error  = " + ex.Message);

            }
           

        }

        decimal TotAmo = 0;
        decimal TotQty = 0;

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                //TotAmo += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Amount"));
                TotQty += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Qty"));
                Label lblAmount = (Label)e.Row.FindControl("lblAmountRequired");
                TotAmo += Convert.ToDecimal(lblAmount.Text);

            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";


                e.Row.Cells[3].Text = TotQty.ToString();
                e.Row.Cells[7].Text = TotAmo.ToString();

                e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[7].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Font.Bold = true;
            }
        }


    }
}