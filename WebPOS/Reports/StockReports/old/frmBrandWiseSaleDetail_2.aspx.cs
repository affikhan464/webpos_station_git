﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebPOS.Reports.SaleReports
{
    public partial class frmBrandWiseSaleDetail_2 : System.Web.UI.Page
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                try
                {
                    LoadBrandList();
                    LoadStationList();
                    txtStartDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                    txtEndDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                }
                catch (Exception ex)
                {
                    Response.Write("error  = " + ex.Message);

                }

            }


        }


        void LoadBrandList()
        {

            SqlCommand cmd = new SqlCommand("select Name from BrandName where CompID='01' order by Name", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);
            lstBrand.DataSource = dset;
            lstBrand.DataBind();
            lstBrand.DataTextField = "Name";
            lstBrand.DataBind();

        }
        void LoadStationList()
        {

            SqlCommand cmd = new SqlCommand("select Name from Station where CompID='01' order by Name", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);
            lstStation.DataSource = dset;
            lstStation.DataBind();
            lstStation.DataTextField = "Name";
            lstStation.DataBind();

        }


        void LoadReport(DateTime StartDate, DateTime EndDate)
        {


            ModItem objModItem = new ModItem();
            decimal BrandCode = objModItem.BrandCodeAgainstBrandName(lstBrand.Text);



            if (chkStation.Checked == false && chkBrand.Checked==true)
            {
                SqlCommand cmd = new SqlCommand("Select Invoice1.InvNo as InvNo ,Invoice1.date1 as date1,Invoice1.Name as Party,Invoice2.Description as ItemName,  Invoice2.Rate as Cost , Invoice2.QTY as QTY,Invoice2.Item_Discount as Item_Discount, isnull(Invoice2.DealRs,0) as DealRs,((Invoice2.Rate*Invoice2.QTY)-(Invoice2.Item_Discount+isnull(Invoice2.DealRs,0))) as FinalAmount from (Invoice1 INNER JOIN Invoice2 ON Invoice1.InvNo = Invoice2.InvNo) inner join invcode on invoice2.code=invcode.code where Invoice1.CompID='" + CompID + "' and Invoice1.Date1>='" + StartDate + "' and  Invoice1.Date1<='" + EndDate + "' and InvCode.Brand ='" + BrandCode + "' order by  Invoice1.System_Date_Time", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataSet dset = new DataSet();
                adpt.Fill(dset);

                GridView1.DataSource = dset;
                GridView1.DataBind();
            }
            else
                if (chkStation.Checked == true && chkBrand.Checked == true)
                {
                    int StationID = 1;
                    ModViewInventory objModViewInventory = new ModViewInventory();
                   

                    SqlCommand cmd = new SqlCommand("Select Invoice1.InvNo as InvNo ,Invoice1.date1 as date1,Invoice1.Name as Party,Invoice2.Description as ItemName,  Invoice2.Rate as Cost , Invoice2.QTY as QTY,Invoice2.Item_Discount as Item_Discount, isnull(Invoice2.DealRs,0) as DealRs,((Invoice2.Rate*Invoice2.QTY)-(Invoice2.Item_Discount+isnull(Invoice2.DealRs,0))) as FinalAmount from (Invoice1 INNER JOIN Invoice2 ON Invoice1.InvNo = Invoice2.InvNo) where Invoice1.CompID='" + CompID + "'  and Invoice1.Date1>='" + StartDate + "' and  Invoice1.Date1<='" + EndDate + "' and Invoice1.StationID=" + StationID + " and Invoice2.BrandName ='" + BrandCode + "' order by  Invoice1.System_Date_Time", con);
                    SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                    DataSet dset = new DataSet();
                    adpt.Fill(dset);

                    GridView1.DataSource = dset;
                    GridView1.DataBind();

                }
                else
                    if (chkStation.Checked == false && chkBrand.Checked == false)
                    {
                        SqlCommand cmd = new SqlCommand("Select Invoice1.InvNo as InvNo ,Invoice1.date1 as date1,Invoice1.Name as Party,Invoice2.Description as ItemName,  Invoice2.Rate as Cost , Invoice2.QTY as QTY,Invoice2.Item_Discount as Item_Discount, isnull(Invoice2.DealRs,0) as DealRs,((Invoice2.Rate*Invoice2.QTY)-(Invoice2.Item_Discount+isnull(Invoice2.DealRs,0))) as FinalAmount from (Invoice1 INNER JOIN Invoice2 ON Invoice1.InvNo = Invoice2.InvNo) where Invoice1.CompID='" + CompID + "' and Invoice1.Date1>='" + StartDate + "' and  Invoice1.Date1<='" + EndDate + "'  order by  Invoice1.System_Date_Time", con);
                        SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                        DataSet dset = new DataSet();
                        adpt.Fill(dset);

                        GridView1.DataSource = dset;
                        GridView1.DataBind();
                    }
                    else
                        if (chkStation.Checked == true && chkBrand.Checked == false)
                        {
                            int StationID = 1;
                            ModViewInventory objModViewInventory = new ModViewInventory();
                            

                            SqlCommand cmd = new SqlCommand("Select Invoice1.InvNo as InvNo ,Invoice1.date1 as date1,Invoice1.Name as Party,Invoice2.Description as ItemName,  Invoice2.Rate as Cost , Invoice2.QTY as QTY,Invoice2.Item_Discount as Item_Discount, isnull(Invoice2.DealRs,0) as DealRs,((Invoice2.Rate*Invoice2.QTY)-(Invoice2.Item_Discount+isnull(Invoice2.DealRs,0))) as FinalAmount from (Invoice1 INNER JOIN Invoice2 ON Invoice1.InvNo = Invoice2.InvNo) where Invoice1.CompID='" + CompID + "'  and Invoice1.Date1>='" + StartDate + "' and  Invoice1.Date1<='" + EndDate + "' and Invoice1.StationID=" + StationID + " order by  Invoice1.System_Date_Time", con);
                            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                            DataSet dset = new DataSet();
                            adpt.Fill(dset);

                            GridView1.DataSource = dset;
                            GridView1.DataBind();

                        }




        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                LoadReport(Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
            }
            catch
            { }
        }


        decimal TotQty = 0;
        decimal TotItem_Discount = 0;
        decimal TotDealRs = 0;
        decimal TotNetAcount = 0;

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // add the UnitPrice and QuantityTotal to the running total variables                
                TotQty += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Qty"));
                TotItem_Discount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Item_Discount"));
                TotDealRs += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "DealRs"));
                TotNetAcount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "FinalAmount"));
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";
                // for the Footer, display the running totals
                e.Row.Cells[5].Text = TotQty.ToString();
                e.Row.Cells[7].Text = TotItem_Discount.ToString();
                e.Row.Cells[8].Text = TotDealRs.ToString();
                e.Row.Cells[9].Text = TotNetAcount.ToString();

                e.Row.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[7].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[8].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[9].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Font.Bold = true;
            }
        }





    }
}