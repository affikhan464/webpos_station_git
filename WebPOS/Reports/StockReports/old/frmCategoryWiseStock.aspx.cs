﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WebPOS.Reports.StockReports
{
    public partial class frmCategoryWiseStock : System.Web.UI.Page
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadCategoryList();
            }
        }




        void LoadBrandList()
        {

            SqlCommand cmd = new SqlCommand("select Name from BrandName where CompID='" + CompID + "' order by name", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);
            lst.DataSource = dset;
            lst.DataBind();
            lst.DataTextField = "Name";
            lst.DataBind();

        }
        void LoadCategoryList()
        {

            SqlCommand cmd = new SqlCommand("select Name from Category where CompID='" + CompID + "' order by name", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);
            lst.DataSource = dset;
            lst.DataBind();
            lst.DataTextField = "Name";
            lst.DataBind();

        }
        void LoadClassList()
        {

            SqlCommand cmd = new SqlCommand("select Name from Class where CompID='" + CompID + "' order by name", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);
            lst.DataSource = dset;
            lst.DataBind();
            lst.DataTextField = "Name";
            lst.DataBind();

        }
        void LoadGoDownList()
        {
            SqlCommand cmd = new SqlCommand("select Name from GoDown where CompID='" + CompID + "' order by name", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);
            lst.DataSource = dset;
            lst.DataBind();
            lst.DataTextField = "Name";
            lst.DataBind();

        }
        void LoadPackingList()
        {
            SqlCommand cmd = new SqlCommand("select Name from Packing where CompID='" + CompID + "' order by name", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);
            lst.DataSource = dset;
            lst.DataBind();
            lst.DataTextField = "Name";
            lst.DataBind();

        }
        void LoadStock(String lstName)
        {

            if (rdoBrand.Checked == true)
            {
                ModItem objModItem = new ModItem();
                int BrandID = objModItem.BrandCodeAgainstBrandName(lstName);
                SqlCommand cmd = new SqlCommand("select Code,Description,qty,round(SellingCost,2) as SellingCost ,round(Ave_Cost,2) as Ave_Cost ,round((qty*Ave_Cost),2) as Amount from InvCode where  CompID='" + CompID + "' and  qty>0 and Brand='" + BrandID + "' order by Description", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataSet dset = new DataSet();
                adpt.Fill(dset);
                GridView1.DataSource = dset;
                GridView1.DataBind();
            }
            else
                if (rdoCategory.Checked == true)
                {
                    ModItem objModItem = new ModItem();
                    int CategoryID = objModItem.CategoryIDAgainstCategoryName(lstName);
                    SqlCommand cmd = new SqlCommand("select Code,Description,qty,round(SellingCost,2) as SellingCost , round(Ave_Cost,2) as Ave_Cost , round((qty*Ave_Cost),2) as Amount from InvCode where  CompID='" + CompID + "' and  qty>0 and Category=" + CategoryID + " order by Description", con);
                    SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                    DataSet dset = new DataSet();
                    adpt.Fill(dset);
                    GridView1.DataSource = dset;
                    GridView1.DataBind();
                }
                else
                    if (rdoClass.Checked == true)
                    {
                        ModItem objModItem = new ModItem();
                        int ClassID = objModItem.ClassIDAgainstClassName(lstName);
                        SqlCommand cmd = new SqlCommand("select Code,Description,qty,round(SellingCost,2) as SellingCost , round(Ave_Cost,2) as Ave_Cost , round((qty*Ave_Cost),2) as Amount from InvCode where  CompID='" + CompID + "' and  qty>0 and Class=" + ClassID + " order by Description", con);
                        SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                        DataSet dset = new DataSet();
                        adpt.Fill(dset);
                        GridView1.DataSource = dset;
                        GridView1.DataBind();
                    }
                    else
                        if (rdoGoDown.Checked == true)
                        {
                            ModItem objModItem = new ModItem();
                            int GoDownID = objModItem.GoDownIDAgainstGoDownName(lstName);
                            SqlCommand cmd = new SqlCommand("select Code,Description,qty,round(SellingCost,2) as SellingCost , round(Ave_Cost,2) as Ave_Cost , round((qty*Ave_Cost),2) as Amount from InvCode where  CompID='" + CompID + "' and  qty>0 and Godown=" + GoDownID + " order by Description", con);
                            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                            DataSet dset = new DataSet();
                            adpt.Fill(dset);
                            GridView1.DataSource = dset;
                            GridView1.DataBind();
                        }
                        else
                            if (rdoPacking.Checked == true)
                            {

                                ModItem objModItem = new ModItem();
                                int PackingID = objModItem.PackingIDAgainstPackingName(lstName);
                                SqlCommand cmd = new SqlCommand("select Code,Description,qty,round(SellingCost,2) as SellingCost , round(Ave_Cost,2) as Ave_Cost , round((qty*Ave_Cost)) as Amount from InvCode where  CompID='" + CompID + "' and  qty>0 and Packing=" + PackingID + " order by Description", con);
                                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                                DataSet dset = new DataSet();
                                adpt.Fill(dset);
                                GridView1.DataSource = dset;
                                GridView1.DataBind();
                            }

                
                


        }


        public Decimal AmountCalculation(decimal Qty, decimal SaleRate, decimal Code)
        {
            ModItem objItem = new ModItem();

            decimal Amount = 0;
            decimal Cost = 0;

            if (rdoAvgCost.Checked == true)
            {
                Cost = objItem.ItemAverageCost(Code);
            }
            if (rdoLastPurchase.Checked == true)
            {
                Cost = objItem.LastPurchasePrice(Convert.ToString(Code));
                //Cost = Code;
            }
            if (rdoSellingPrice.Checked == true)
            {
                Cost = SaleRate;
            }
            Amount = (Cost * Qty);

            return Math.Round(Amount,2);
        }
        public Decimal LastPurchaseRate(decimal Code)
        {
            ModItem objItem = new ModItem();
            decimal LastPuRate = objItem.LastPurchasePrice(Convert.ToString(Code));



            return LastPuRate;
        }



        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                LoadStock(lst.SelectedItem.Value);
            }
            catch (Exception ex)
            {
                Response.Write("error  = " + ex.Message);

            }

        }

        decimal TotAmo = 0;
        decimal TotQty = 0;

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                //TotAmo += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Amount"));
                TotQty += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Qty"));
                //TotAmo += Convert.ToDecimal(e.Row.Cells[5].Text); 
                Label lblAmount = (Label)e.Row.FindControl("lblAmount");
                TotAmo += Convert.ToDecimal(lblAmount.Text);

            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";


                e.Row.Cells[2].Text = TotQty.ToString();
                e.Row.Cells[6].Text = TotAmo.ToString();

                e.Row.Cells[2].HorizontalAlign = e.Row.Cells[6].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Font.Bold = true;
            }
        }

        protected void rdoCategory_CheckedChanged(object sender, EventArgs e)
        {
            lblCategory.Text="Select category";
            LoadCategoryList();
        }

        protected void rdoBrand_CheckedChanged(object sender, EventArgs e)
        {
            lblCategory.Text = "Select Brand";
            LoadBrandList();
        }

        protected void rdoClass_CheckedChanged(object sender, EventArgs e)
        {
            lblCategory.Text = "Select Class";
            LoadClassList();
        }

        protected void rdoPacking_CheckedChanged(object sender, EventArgs e)
        {
            lblCategory.Text = "Select Class";
            LoadPackingList();
        }

        protected void rdoGoDown_CheckedChanged(object sender, EventArgs e)
        {
            lblCategory.Text = "Select Class";
            LoadGoDownList();
        }


    }
}