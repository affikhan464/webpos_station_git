﻿<%@ Page Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="frmCategoryWiseStock.aspx.cs" Inherits="WebPOS.Reports.StockReports.frmCategoryWiseStock" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="<%= ResolveUrl("~/css/style.css") %>" rel="stylesheet" />
    <link href="<%= ResolveUrl("~/css/reset.css") %>" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

           <br /><h2 class="heading"  >Stock With Amount</h2><br />
        
    <div style ="width:100%;height:100%;text-align:center; vertical-align:middle">
    <table  border="0" style="width:100%">
        <tr  >
            <td >
                <asp:Label ID="lblCategory" runat="server" Text="Select Category"></asp:Label><asp:DropDownList ID="lst" runat="server"></asp:DropDownList>
            <asp:Button ID="Button1" runat="server" Text="Report" OnClick="Button1_Click"  />
            </td>   
     </tr>
     <tr>
         <td >
             <asp:RadioButton style="color:blue;" ID="rdoAvgCost" GroupName ="one" runat="server"  Text="Average Cost" Checked="true" />
             <asp:RadioButton style="color:blue;" ID="rdoLastPurchase" GroupName="one" runat="server" text="Last Purchase"/>
             <asp:RadioButton style="color:blue;" ID="rdoSellingPrice" GroupName="one" runat="server" text="Selling Price"/>
         </td>  
     </tr>
     <tr>
         <td >
             <asp:RadioButton style="color:blue;" ID="rdoBrand" GroupName ="two" runat="server"   Text="Brand" Checked="true" AutoPostBack="True" OnCheckedChanged="rdoBrand_CheckedChanged"  />
             <asp:RadioButton style="color:blue;" ID="rdoCategory" GroupName ="two" runat="server"   Text="Category" Checked="true" AutoPostBack="True" OnCheckedChanged="rdoCategory_CheckedChanged"  />
             <asp:RadioButton style="color:blue;" AutoPostBack="true" GroupName="two" ID="rdoClass" Text="Class" runat="server" OnCheckedChanged="rdoClass_CheckedChanged" />
             
             <asp:RadioButton style="color:blue;" ID="rdoPacking" GroupName="two" runat="server" text="Packing" AutoPostBack="True" OnCheckedChanged="rdoPacking_CheckedChanged"/>
             <asp:RadioButton style="color:blue;" ID="rdoGoDown" GroupName="two" runat="server" text="GoDown" AutoPostBack="True" OnCheckedChanged="rdoGoDown_CheckedChanged"/>
     </td>  
     </tr>
    </table>


       <asp:GridView CssClass="grid" ID="GridView1" AutoGenerateColumns="False" HeaderStyle-BackColor ="WhiteSmoke"   AlternatingRowStyle-BackColor ="WhiteSmoke"
      runat="server"   OnRowDataBound="GridView1_RowDataBound" ShowFooter="True"  FooterStyle-BackColor="Wheat" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical">
<AlternatingRowStyle BackColor="#DCDCDC"></AlternatingRowStyle>
      <Columns>
        <asp:BoundField HeaderText="ItemCode" DataField="Code" ReadOnly="true" >
          <ItemStyle HorizontalAlign="Center" />
          </asp:BoundField>
        <asp:BoundField HeaderText="ItemName" DataField="Description" ReadOnly="true" >
          <ItemStyle HorizontalAlign="Left" />
          </asp:BoundField>
        <asp:BoundField HeaderText="Quantity" DataField="qty" ReadOnly="true" >

          <ItemStyle HorizontalAlign="Right" />
          </asp:BoundField>

          <asp:BoundField HeaderText="Selling Price" DataField="SellingCost" ReadOnly="true" >
          <ItemStyle HorizontalAlign="Right" />
          </asp:BoundField>
          <asp:BoundField HeaderText="Average Cost" DataField="Ave_Cost" ReadOnly="true" >
         
          
         
          
           <ItemStyle HorizontalAlign="Right" />
          </asp:BoundField>
         
          
         
          
           <asp:TemplateField HeaderText="Last Purchase Rate">
             
               <ItemTemplate>  
                   <asp:Label ID="Label1" runat="server" Text='<%# LastPurchaseRate(Convert.ToDecimal( Eval("Code"))) %>'></asp:Label>
               </ItemTemplate>
               <ItemStyle HorizontalAlign="Right" />
          </asp:TemplateField>
          
          
           <asp:TemplateField HeaderText="Amount">
             
               <ItemTemplate>  
                   <asp:Label ID="lblAmount" runat="server" Text='<%# AmountCalculation(Convert.ToDecimal( Eval("qty")),Convert.ToDecimal(Eval("SellingCost")),Convert.ToDecimal(Eval("Code"))) %>'></asp:Label>
               </ItemTemplate>
               <ItemStyle HorizontalAlign="Right" />
          </asp:TemplateField>
          

      </Columns>

<FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>

<HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White"></HeaderStyle>
           <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
           <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
           <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
           <SortedAscendingCellStyle BackColor="#F1F1F1" />
           <SortedAscendingHeaderStyle BackColor="#0000A9" />
           <SortedDescendingCellStyle BackColor="#CAC9C9" />
           <SortedDescendingHeaderStyle BackColor="#000065" />
    </asp:GridView>
    </div>




</asp:Content>