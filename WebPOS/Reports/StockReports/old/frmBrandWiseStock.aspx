﻿<%@ Page Language="C#" MasterPageFile="~/masterpage.Master"  AutoEventWireup="true" CodeBehind="frmBrandWiseStock.aspx.cs" Inherits="WebPOS.frmBrandWiseStock" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="<%= ResolveUrl("~/css/style.css") %>" rel="stylesheet" />
    <link href="<%= ResolveUrl("~/css/reset.css") %>" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

        <br /><h2 class="heading" >Brand Wise Stock With Amount</h2><br />
        
    <div style ="width:100%;height:100%;text-align:center; vertical-align:middle">
  
        
            
                <asp:CheckBox ID="chkBrand" runat="server" Text="Brand" />
                <asp:DropDownList ID="lstBrand" runat="server"></asp:DropDownList>
                
                
                <asp:Button ID="Button1" runat="server" Text="Get Report" OnClick="Button1_Click"  />
            
    
     
         
             
             
             <asp:RadioButton style="color:red" ID="rdoAvgCost" GroupName ="one" runat="server"  Text="Average Cost" Checked="true" />
             <asp:RadioButton style="color:black" ID="rdoLastPurchase"  GroupName="one" runat="server" text="Last Purchase"/>
             <asp:RadioButton style="color:orange" ID="rdoSellingPrice" GroupName="one" runat="server" text="Selling Price"/>



         <br />
        <br />
     <center>
    

        <div  style=" padding-bottom:50px;"> <centre>
        <asp:GridView CssClass="grid" ID="GridView1" AutoGenerateColumns="False" HeaderStyle-BackColor="WhiteSmoke" AlternatingRowStyle-BackColor="WhiteSmoke"
            runat="server" OnRowDataBound="GridView1_RowDataBound" ShowFooter="True"  FooterStyle-BackColor="Wheat" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical">
            <AlternatingRowStyle BackColor="#DCDCDC"></AlternatingRowStyle>
            <Columns>
                <asp:BoundField HeaderText="ItemCode" DataField="Code" ReadOnly="true">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField HeaderText="ItemName" DataField="Description" ReadOnly="true">
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField HeaderText="Quantity" DataField="qty" ReadOnly="true">

                    <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>

                <asp:BoundField HeaderText="Selling Price" DataField="SellingCost" ReadOnly="true">
                    <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
                <asp:BoundField HeaderText="Average Cost" DataField="Ave_Cost" ReadOnly="true">




                    <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>




                <asp:TemplateField HeaderText="Last Purchase Rate">

                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# LastPurchaseRate(Convert.ToDecimal( Eval("Code"))) %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                </asp:TemplateField>


                <asp:TemplateField HeaderText="Amount">

                    <ItemTemplate>
                        <asp:Label ID="lblAmount" runat="server" Text='<%# AmountCalculation(Convert.ToDecimal( Eval("qty")),Convert.ToDecimal(Eval("SellingCost")),Convert.ToDecimal(Eval("Code"))) %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                </asp:TemplateField>


            </Columns>




            <FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>
            <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White"></HeaderStyle>
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#0000A9" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#000065" />
        </asp:GridView>
            </div>



       


    </div>


    
  </asp:Content>