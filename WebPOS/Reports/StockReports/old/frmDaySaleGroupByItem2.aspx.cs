﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebPOS.Reports.SaleReports
{
    public partial class frmDaySaleGroupByItem2 : System.Web.UI.Page
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtStartDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                txtEndDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
               
            }
            

        }


       

        void MainReport(DateTime StartDate, DateTime EndDate)
        {

            SqlCommand cmd = new SqlCommand("SELECT  Invoice2.Code as Code , Invoice2.Description as Description , Sum(Invoice2.Qty) as QtySold ,Sum(Invoice2.Amount) as AmoSold FROM (Invoice1 INNER JOIN Invoice2 ON Invoice1.InvNo = Invoice2.InvNo) where Invoice2.CompID='" + CompID + "' and   Invoice2.ItemNature = 1 and Invoice1.Date1>='" + StartDate +  "' and Invoice1.Date1<='" + EndDate + "' GROUP BY Invoice2.Code,Invoice2.Description order by Invoice2.Description", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);

            GridView1.DataSource = dset;
            GridView1.DataBind();


        }






        protected void Button1_Click(object sender, EventArgs e)
        {

            MainReport(Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
        }


        decimal TotQtySold = 0;
        decimal TotAmoSold = 0;
        decimal TotQtySaleReturn = 0;
        decimal TotAmoSaleReturn = 0;
        decimal TotNetSoldQty = 0;
        decimal TotNetSoldAmo = 0;
        decimal TotCurrentQty = 0;
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // add the UnitPrice and QuantityTotal to the running total variables
                TotQtySold += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "QtySold"));
                TotAmoSold += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "AmoSold"));
        
                Label lblReturnQty = (Label)e.Row.FindControl("lblSaleReturnQty");
                TotQtySaleReturn += Convert.ToDecimal(lblReturnQty.Text);

                Label lblReturnAmo = (Label)e.Row.FindControl("lblSaleRetAmo");
                TotAmoSaleReturn += Convert.ToDecimal(lblReturnAmo.Text);

                Label lblNetSoldQty = (Label)e.Row.FindControl("lblNetSoldQty");
                TotNetSoldQty += Convert.ToDecimal(lblNetSoldQty.Text);

                Label lblNetSoldAmo = (Label)e.Row.FindControl("lblNetSoldAmount");
                TotNetSoldAmo += Convert.ToDecimal(lblNetSoldAmo.Text);

                Label lblCurrentQty = (Label)e.Row.FindControl("lblAvailableQty");
                TotCurrentQty += Convert.ToDecimal(lblCurrentQty.Text);
              
                
                
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";
                // for the Footer, display the running totals
                e.Row.Cells[3].Text = TotQtySold.ToString();
                e.Row.Cells[4].Text = TotAmoSold.ToString();
                e.Row.Cells[5].Text = TotQtySaleReturn.ToString();
                e.Row.Cells[6].Text = TotAmoSaleReturn.ToString();
                e.Row.Cells[7].Text = TotNetSoldQty.ToString();
                e.Row.Cells[8].Text = TotNetSoldAmo.ToString();
                e.Row.Cells[9].Text = TotCurrentQty.ToString();
                
                e.Row.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[6].HorizontalAlign= HorizontalAlign.Right;
                e.Row.Cells[7].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[8].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[9].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Font.Bold = true;
            }
        }

        public decimal  SaleReturnQty(string ItemCode)
        {

            DateTime StartDate = Convert.ToDateTime(txtStartDate.Text);
            DateTime EndDate = Convert.ToDateTime(txtEndDate.Text);
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(Qty),0) as Qty from SaleReturn2 where CompID='" + CompID + "' and Code='" + ItemCode + "' and Date1>='" + StartDate + "' and Date1<='" + EndDate + "'", con);
            //SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(Qty),0) as Qty from Invoice2", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal Qty = 0;



            if (dt.Rows.Count > 0)
            {
                Qty = Convert.ToDecimal(dt.Rows[0]["Qty"]);

            }


            return Qty;



        }
        public decimal availableQtyLocal(string ItemCode)
        {
            ModItem objItem=new ModItem();
            decimal qty=objItem.ItemAvailableQty(ItemCode);
            return qty;
        }
        public decimal SaleReturnAmount(string ItemCode)
        {

            DateTime StartDate = Convert.ToDateTime(txtStartDate.Text);
            DateTime EndDate = Convert.ToDateTime(txtEndDate.Text);
            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(Amount),0) as Amount from SaleReturn2 where CompID='" + CompID + "' and Code='" + ItemCode + "' and Date1>='" + StartDate + "' and Date1<='" + EndDate + "'", con);
            //SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(Qty),0) as Qty from Invoice2", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);
            decimal Amount = 0;

            if (dt.Rows.Count > 0)
            {
                Amount = Convert.ToDecimal(dt.Rows[0]["Amount"]);

            }


            return Amount;

        }
        

    }
}