﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS
{
    public partial class frmBrandWiseStock : System.Web.UI.Page
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr); 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
                LoadBrandList();
            }
        }


        void LoadBrandList()
        {

            SqlCommand cmd = new SqlCommand("select Name from BrandName where CompID='" + CompID + "' order by name", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);
            lstBrand.DataSource = dset;
            lstBrand.DataBind();
            lstBrand.DataTextField = "Name";
            lstBrand.DataBind();

        }
        
        void BrandWiseStock(String BrandName)
        {
            
            ModViewInventory objModViewInventory = new ModViewInventory();
           

            ModItem  objModItem = new ModItem();
            int BrandID = objModItem.BrandCodeAgainstBrandName(BrandName);


            if (chkBrand.Checked == true )
            {
                SqlCommand cmd = new SqlCommand("select Code,Description,isnull(qty,0) as qty , isnull(SellingCost,0) as  SellingCost , isnull(Ave_Cost,0) as Ave_Cost , isnull(qty,0)*isnull(Ave_Cost,0) as Amount from InvCode where  CompID='" + CompID + "' and  qty>0 and Brand='" + BrandID + "' order by Description", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataSet dset = new DataSet();
                adpt.Fill(dset);

                GridView1.DataSource = dset;
                GridView1.DataBind();

               
            }
           
            if (chkBrand.Checked == false )
            {
                SqlCommand cmd = new SqlCommand("select InvCode.Code as Code, InvCode.Description as Description , isnull(InvCode.qty,0) as qty , isnull(InvCode.SellingCost,0) as  SellingCost , isnull(InvCode.Ave_Cost,0) as Ave_Cost , isnull(InvCode.qty,0)*isnull(InvCode.Ave_Cost,0) as Amount from InvCode where  InvCode.CompID='" + CompID + "' and  invcode.qty>0 order by Description", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataSet dset = new DataSet();
                adpt.Fill(dset);

                GridView1.DataSource = dset;
                GridView1.DataBind();
            }
            


        }

        public Decimal AmountCalculation(decimal Qty, decimal SaleRate, decimal Code)
        {
            ModItem objItem=new ModItem();
            
            decimal Amount=0;
            decimal Cost = 0;
            
            if (rdoAvgCost.Checked ==true ) 
                {
                    Cost = objItem.ItemAverageCost(Code);
                }
            if (rdoLastPurchase.Checked==true ) 
                {
                    Cost = objItem.LastPurchasePrice(Convert.ToString(Code));
                    //Cost = Code;
                }
            if (rdoSellingPrice.Checked==true  ) 
                {
                Cost = SaleRate;
                }
            Amount=(Cost*Qty);

            return Amount;
        }
        public Decimal LastPurchaseRate( decimal Code)
        {
            ModItem objItem = new ModItem();
            decimal LastPuRate= objItem.LastPurchasePrice(Convert.ToString(Code));

            
            
            return LastPuRate;
        }



        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                BrandWiseStock(lstBrand.SelectedItem.Value);
            }
            catch (Exception ex)
            {
                Response.Write("error  = " + ex.Message);

            }

           
        }

        decimal TotAmo = 0;
        decimal TotQty = 0;
        
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                
                //TotAmo += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Amount"));
                TotQty += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Qty"));
                //TotAmo += Convert.ToDecimal(e.Row.Cells[5].Text); 
                Label lblAmount = (Label)e.Row.FindControl("lblAmount");
                TotAmo += Convert.ToDecimal(lblAmount.Text);
                   
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";
                
                
                e.Row.Cells[2].Text = TotQty.ToString();
                e.Row.Cells[6].Text = TotAmo.ToString();

                e.Row.Cells[2].HorizontalAlign = e.Row.Cells[6].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Font.Bold = true;
            }
        }


    }
}