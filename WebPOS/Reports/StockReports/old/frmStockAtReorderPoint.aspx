﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="frmStockAtReorderPoint.aspx.cs" Inherits="WebPOS.Reports.StockReports.frmStockAtReorderPoint" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="<%= ResolveUrl("~/css/style.css") %>" rel="stylesheet" />
    <link href="<%= ResolveUrl("~/css/reset.css") %>" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <br /> <h2 class="heading" >Brand Wise Stock At Reorder Point</h2><br />
        
    <div style ="width:100%;height:100%;text-align:center; vertical-align:middle">
    <table  border="0" style="width:100%">
        <tr  >
            <td >
                <asp:CheckBox style="color:blue;" ID="chkBrand" runat="server" text="Select Brand"/>
            Select Brand<asp:DropDownList ID="lstBrand" runat="server"></asp:DropDownList>
            <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click"  />
            </td>   
     </tr>
     
    </table>


        <asp:GridView CssClass="grid"  ID="GridView1" AutoGenerateColumns="False" HeaderStyle-BackColor="WhiteSmoke" AlternatingRowStyle-BackColor="WhiteSmoke"
            runat="server" OnRowDataBound="GridView1_RowDataBound" ShowFooter="True"  FooterStyle-BackColor="Wheat" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical">
            <AlternatingRowStyle BackColor="#DCDCDC"></AlternatingRowStyle>
            <Columns>
               <asp:TemplateField HeaderText="S. #"> 
               <ItemTemplate> 
                    <span style="text-align:left"> 
                        <asp:Label ID="lblSNO" runat="server"  Text='<%# Convert.ToInt32( Container.DataItemIndex+1) %>'></asp:Label>
                    </span> 
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>

                
                <asp:BoundField HeaderText="ItemCode" DataField="Code" ReadOnly="true">
                    <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
                <asp:BoundField HeaderText="ItemName" DataField="Description" ReadOnly="true">
                    <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField HeaderText="Quantity In Hand" DataField="qty" ReadOnly="true">

                    <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>

                <asp:BoundField HeaderText="Reorder Level" DataField="ReorderLevel" ReadOnly="true">
                    <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>

                <asp:BoundField HeaderText="Reorder Qty" DataField="ReorderQty" ReadOnly="true">
                    <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>

                 <asp:BoundField HeaderText="Average Cost" DataField="Ave_Cost" ReadOnly="true">
                    <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>


                <asp:TemplateField HeaderText="Amount">

                    <ItemTemplate>
                        <asp:Label ID="lblAmountRequired" runat="server" Text='<%#   (Convert.ToDecimal(Eval("ReorderQty")) * Convert.ToDecimal( Eval("Ave_Cost"))) %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                </asp:TemplateField>


            </Columns>

            <FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>

            <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White"></HeaderStyle>
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#0000A9" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#000065" />
        </asp:GridView>
    </div>




</asp:Content>
