﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="frmTotalStockWithAmount.aspx.cs" Inherits="WebPOS.Reports.StockReports.frmTotalStockWithAmount" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="<%= ResolveUrl("~/css/style.css") %>" rel="stylesheet" />
    <link href="<%= ResolveUrl("~/css/reset.css") %>" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="divHeading" >
           <br /> <h2 >Total Stock With Amount</h2><br />
    
  
   
<br />
         <center>
        <asp:GridView CssClass="grid" ID="GridView1" AutoGenerateColumns="False" HeaderStyle-BackColor ="WhiteSmoke"   AlternatingRowStyle-BackColor ="WhiteSmoke"
      runat="server"  OnRowDataBound="GridView1_RowDataBound" ShowFooter="True"  FooterStyle-BackColor="Wheat" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical">
<AlternatingRowStyle BackColor="#DCDCDC"></AlternatingRowStyle>
      <Columns>
        <asp:TemplateField HeaderText="S. #"> 
               <ItemTemplate> <span style="text-align:left"> <asp:Label ID="Label1" runat="server"  Text='<%# Convert.ToInt32( Container.DataItemIndex+1) %>'></asp:Label>
               </span> 
          </ItemTemplate>
               <ItemStyle HorizontalAlign="Center" />
          </asp:TemplateField>
         
        <asp:BoundField HeaderText="Code" DataField="Code" ReadOnly="true"  />
         
          <asp:BoundField HeaderText="Brand" DataField="Name" ReadOnly="true" >
          <ItemStyle HorizontalAlign="left" />
          </asp:BoundField>
        <asp:BoundField HeaderText="Description" DataField="Description" ReadOnly="true" >
            <ItemStyle HorizontalAlign="left" />
          </asp:BoundField>
          <asp:BoundField HeaderText="Quantity" DataField="qty" ReadOnly="true" >
        <ItemStyle HorizontalAlign="right" Width="90px"/>
          </asp:BoundField>
          <asp:BoundField HeaderText="Rate" DataField="Ave_Cost" ReadOnly="true" >
          <ItemStyle HorizontalAlign="Right" Width="90px" />
          </asp:BoundField>
          
          <asp:BoundField HeaderText="Amount" DataField="Amount" ReadOnly="true" >
          <ItemStyle HorizontalAlign="Right" Width="90px" />
          </asp:BoundField>    
          

      </Columns>

<FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>

<HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White"></HeaderStyle>
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#0000A9" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#000065" />
    </asp:GridView>

    </div>
    
   </asp:Content>