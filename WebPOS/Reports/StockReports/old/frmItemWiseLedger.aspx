﻿<%@ Page Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="frmItemWiseLedger.aspx.cs" Inherits="WebPOS.Reports.StockReports.frmItemWiseLedger" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="<%= ResolveUrl("~/css/style.css") %>" rel="stylesheet" />
    <link href="<%= ResolveUrl("~/css/reset.css") %>" rel="stylesheet" />
    <link href="../../css/allitems.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
       <br /> <h2 class="heading" >Stock Ledger Card</h2><br />
       

<div style ="width:100%;height:100%;text-align:center; vertical-align:middle">
            
            
                
                    <asp:Label ID="Label1" runat="server" Text="Start Date"></asp:Label>
                    <asp:TextBox CssClass="datetimepicker"  ID="StartDate" runat="server" Width="80px"></asp:TextBox>
                    <asp:Label ID="Label2" runat="server" Text="End Date"></asp:Label>
                    <asp:TextBox CssClass="datetimepicker"  ID="txtEndDate" runat="server" Width="80px"></asp:TextBox>
                    
                
            
            
                
                    <asp:Label ID="Label6" runat="server" Text="Select Item"></asp:Label>
                    
                    
                <asp:TextBox ID="txtItemName" CssClass="autocomplete SearchBox"  data-id="txtItemName" data-type="Item" data-function="GetRecords"  runat="server" Width="360px"></asp:TextBox>
                   <asp:Button ID="Button1" runat="server" Text="Report" OnClick="Button1_Click" /> 
                <br />
            
        
</div>
        <div style="width:100%">
        
               
        
  

            <br /><br />


            <center>

        <asp:GridView CssClass="grid" ID="GridView1" AutoGenerateColumns="False" HeaderStyle-BackColor ="WhiteSmoke"   AlternatingRowStyle-BackColor ="WhiteSmoke"
      runat="server" OnRowDataBound="GridView1_RowDataBound" ShowFooter="True"  FooterStyle-BackColor="Wheat" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical">
<AlternatingRowStyle BackColor="#DCDCDC"></AlternatingRowStyle>
      
            <Columns>

        <asp:BoundField HeaderText="Date" DataField="Date" DataFormatString="{0:d MMM yyyy}" ReadOnly="true"  >  
                <ItemStyle Width="100px" />
                </asp:BoundField>
        <asp:BoundField HeaderText="Description" DataField="Description" ReadOnly="true" >  
               <ItemStyle HorizontalAlign="Center" />

            </asp:BoundField>

                
           





        <asp:BoundField HeaderText="Debit" DataField="Debit" ReadOnly="true"  >  
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
        <asp:BoundField HeaderText="Credit" DataField="Credit" ReadOnly="true"  >  
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
        <asp:BoundField HeaderText="Balance" DataField="Balance" ReadOnly="true"  >
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
        <asp:BoundField HeaderText="DescriptionOfBill" DataField="DescriptionOfBill" ReadOnly="true" Visible="False"  />  
        <asp:BoundField HeaderText="BillNo" DataField="BillNo" ReadOnly="true" Visible="False"  />  
      </Columns>

<FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>

<HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White"></HeaderStyle>
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#0000A9" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#000065" />
    </asp:GridView>


    </div>
    <script src="../../Script/jquery.min.js"></script>
    <script src="../../Script/jquery-ui.min.js"></script>
    <script src="../../Script/Autocomplete.js"></script>
</asp:Content>