﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebPOS.Reports.StockReports
{
    public partial class frmOpeningStock : System.Web.UI.Page
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {


            LoadItemList();


        }

        void LoadItemList()
        {





            Module7 objModule7 = new Module7();

            SqlCommand cmd = new SqlCommand(@"select 
                InventoryOpeningBalance.ICode,
                InvCode.Description,
                BrandName.Name as BrandName,
                Convert(numeric(18, 2),
                sum(InventoryOpeningBalance.Opening)) as Opening
                from InventoryOpeningBalance
                inner join InvCode on InventoryOpeningBalance.ICode = InvCode.Code
                join BrandName on InvCode.Brand = BrandName.Code
                where InvCode.CompID = '" + CompID + @"' and
                Opening > 0  and
                Dat = '" + objModule7.StartDate() + @"'
                group by InventoryOpeningBalance.ICode,
                InvCode.Description,
                BrandName.Name,
                InventoryOpeningBalance.Cost_Per_Unit,
                InventoryOpeningBalance.Total_Value
                order by InvCode.Description", con);


            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);

            GridView1.DataSource = dset;
            GridView1.DataBind();


        }


        decimal TotQty = 0;

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TotQty += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Opening"));

            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";
                e.Row.Cells[2].Text = TotQty.ToString();

                e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;

                e.Row.Font.Bold = true;
            }
        }




    }
}