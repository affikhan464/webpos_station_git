﻿<%@ Page Language="C#" Title="" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="frmDaySaleGroupByItem2.aspx.cs" Inherits="WebPOS.Reports.SaleReports.frmDaySaleGroupByItem2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="<%= ResolveUrl("~/css/style.css") %>" rel="stylesheet" />
    <link href="<%= ResolveUrl("~/css/reset.css") %>" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



         <br /> <h2 class="heading" >Day Sale Group By Item - 2</h2><br />
        
    <div style ="width:100%;height:100%;text-align:center; vertical-align:middle">
   
        
        <table  border="0" style="width:100%">
        <tr  style="background-color:#f2e9e9"  >
        <td>
                <asp:Label ID="Label2" runat="server" Text="Start Date"></asp:Label>
                <asp:TextBox ID="txtStartDate"  CssClass="datetimepicker"  runat="server" ></asp:TextBox> 
                <asp:Label ID="Label3" runat="server" Text="End Date"></asp:Label>
                <asp:TextBox ID="txtEndDate"  CssClass="datetimepicker"  runat="server" ></asp:TextBox>
                <asp:Button ID="Button1" runat="server" Text="Report" OnClick="Button1_Click"  />
        </td>
        </tr>
        </table>
               
 <br/>

        <asp:GridView CssClass="grid" ID="GridView1" AutoGenerateColumns="False" HeaderStyle-BackColor ="WhiteSmoke"   AlternatingRowStyle-BackColor ="WhiteSmoke"
      runat="server" OnRowDataBound="GridView1_RowDataBound" ShowFooter="True"  FooterStyle-BackColor="Wheat" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical">
<AlternatingRowStyle BackColor="#DCDCDC"></AlternatingRowStyle>
      <Columns>
          
          <asp:TemplateField HeaderText="S. #"> 
               <ItemTemplate> <span style="text-align:left"> <asp:Label ID="lblSN" runat="server"  Text='<%# Convert.ToInt32( Container.DataItemIndex+1) %>'></asp:Label>
               </span> 
          </ItemTemplate>
               <ItemStyle HorizontalAlign="Center" />
          </asp:TemplateField>
          
        <asp:BoundField HeaderText="ItemCode" DataField="Code" ReadOnly="true"  >
          <ItemStyle HorizontalAlign="Center" />
          </asp:BoundField>
          <asp:BoundField HeaderText="Item Name" DataField="Description" ReadOnly="true"  >
          <ItemStyle HorizontalAlign="Left" />
          </asp:BoundField>


        <asp:BoundField HeaderText="Sold Qty." DataField="QtySold" ReadOnly="true"  >

          <ItemStyle HorizontalAlign="Right" />
          </asp:BoundField>

        <asp:BoundField HeaderText="Sold Qty Amount" DataField="AmoSold" ReadOnly="true" >
          <ItemStyle HorizontalAlign="Right" />
          </asp:BoundField>
         
          <asp:TemplateField HeaderText="Sale Return Qty">      
               <ItemTemplate> <asp:Label ID="lblSaleReturnQty" runat="server"  Text='<%#  SaleReturnQty(Convert.ToString(Eval("Code"))) %>'></asp:Label>
               
               </ItemTemplate>
               <ItemStyle HorizontalAlign="Right" />
          </asp:TemplateField>
          <asp:TemplateField HeaderText="Sale Return Amount">      
               <ItemTemplate> <asp:Label ID="lblSaleRetAmo" runat="server"  Text='<%#  SaleReturnAmount(Convert.ToString(Eval("Code"))) %>'></asp:Label>
               
               </ItemTemplate>
               <ItemStyle HorizontalAlign="Right" />
          </asp:TemplateField>
          <asp:TemplateField HeaderText="Net Sold Qty">      
               <ItemTemplate> <asp:Label ID="lblNetSoldQty" runat="server"  Text='<%#  Convert.ToDecimal( Eval("QtySold"))-SaleReturnQty(Convert.ToString(Eval("Code"))) %>'></asp:Label>
               
               </ItemTemplate>
               <ItemStyle HorizontalAlign="Right" />
          </asp:TemplateField>
          <asp:TemplateField HeaderText="Net Sold Amount">      
               <ItemTemplate> <asp:Label ID="lblNetSoldAmount" runat="server"  Text='<%#  Convert.ToDecimal( Eval("AmoSold"))-SaleReturnAmount(Convert.ToString(Eval("Code"))) %>'></asp:Label>
               
               </ItemTemplate>
               <ItemStyle HorizontalAlign="Right" />
          </asp:TemplateField>
          <asp:TemplateField HeaderText="Available Qty.">      
               <ItemTemplate> <asp:Label ID="lblAvailableQty" runat="server"  Text='<%#  availableQtyLocal(Convert.ToString( Eval("Code"))) %>'></asp:Label>
               
               </ItemTemplate>
               <ItemStyle HorizontalAlign="Right" />
          </asp:TemplateField>
          

      </Columns>

    <FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>

     <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White"></HeaderStyle>
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#0000A9" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#000065" />
    </asp:GridView>

    
</div>
</asp:Content>

