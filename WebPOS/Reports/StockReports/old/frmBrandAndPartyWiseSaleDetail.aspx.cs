﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebPOS.Reports.SaleReports
{
    public partial class frmBrandAndPartyWiseSaleDetail : System.Web.UI.Page
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                try
                {
                   
                    LoadBrandList();
                    LoadPartyList();
                    StartDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                    txtEndDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                }
                catch (Exception ex)
                {
                    Response.Write("error  = " + ex.Message);

                }

            }


        }

        


        void LoadBrandList()
        {

            SqlCommand cmd = new SqlCommand("select Name from BrandName where CompID='01' order by Name", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);
            lstBrand.DataSource = dset;
            lstBrand.DataBind();
            lstBrand.DataTextField = "Name";
            lstBrand.DataBind();

        }
        void LoadPartyList()
        {

            if (rdoClient.Checked == true)
            {
                SqlCommand cmd = new SqlCommand("select Name from PartyCode where NorBalance=1 and CompID='01' order by Name", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataSet dset = new DataSet();
                adpt.Fill(dset);
                lstParty.DataSource = dset;
            }
            else
                if (rdoAll.Checked == true)
                {
                    SqlCommand cmd = new SqlCommand("select Name from PartyCode where CompID='01' order by Name", con);
                    SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                    DataSet dset = new DataSet();
                    adpt.Fill(dset);
                    lstParty.DataSource = dset;
                }
                else
                {
                    SqlCommand cmd = new SqlCommand("select Name from PartyCode where CompID='01' order by Name", con);
                    SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                    DataSet dset = new DataSet();
                    adpt.Fill(dset);
                    lstParty.DataSource = dset;
                }



            lstParty.DataBind();
            lstParty.DataTextField = "Name";
            lstParty.DataBind();

        }
        void LoadReport(DateTime StartDate, DateTime EndDate)
        {
           
            ModViewInventory objModViewInventory = new ModViewInventory();
           

            ModItem objModItem = new ModItem();
            decimal BrandID = objModItem.BrandCodeAgainstBrandName(lstBrand.Text);

            ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
            string PartyCode = objModPartyCodeAgainstName.PartyCode(lstParty.Text);
            string sql = "";
            if(chkParty.Checked==false && chkBrand.Checked==false)
            {
                sql = "Select Invoice1.Name , Invoice2.Description as ItemName,  sum(Invoice2.QTY) as QTY, sum(Invoice2.Amount) as Amount , sum(Invoice2.Item_Discount) as Item_Discount, isnull(sum(Invoice2.DealRs),0) as DealRs from (Invoice2 inner join invoice1 on Invoice2.InvNo=Invoice1.InvNo) where Invoice1.date1>='" + StartDate + "' and Invoice1.date1<='" + EndDate + "' group by Invoice1.Name ,Invoice2.Description order by Invoice2.Description"; 
            }
            else
            if (chkParty.Checked == true && chkBrand.Checked == false)
            {
                sql = "Select Invoice1.Name , Invoice2.Description as ItemName,  sum(Invoice2.QTY) as QTY, sum(Invoice2.Amount) as Amount , sum(Invoice2.Item_Discount) as Item_Discount, isnull(sum(Invoice2.DealRs),0) as DealRs from (Invoice2 inner join invoice1 on Invoice2.InvNo=Invoice1.InvNo) where Invoice1.PartyCode='" + PartyCode + "' and Invoice1.date1>='" + StartDate + "' and Invoice1.date1<='" + EndDate + "' group by Invoice1.Name,Invoice2.Description order by Invoice2.Description";
            }
            else
            if (chkParty.Checked == false && chkBrand.Checked == true)
            {
                sql = "Select Invoice1.Name , Invoice2.Description as ItemName,  sum(Invoice2.QTY) as QTY, sum(Invoice2.Amount) as Amount , sum(Invoice2.Item_Discount) as Item_Discount, isnull(sum(Invoice2.DealRs),0) as DealRs from (Invoice2 inner join invoice1 on Invoice2.InvNo=Invoice1.InvNo) inner join BrandName on Invoice2.BrandName=BrandName.Code where BrandName.Code='" + BrandID  + "' and Invoice1.date1>='" + StartDate + "' and Invoice1.date1<='" + EndDate + "' group by Invoice1.Name ,Invoice2.Description order by Invoice2.Description";
            }
            else
            if (chkParty.Checked == true && chkBrand.Checked == true)
            {
               sql = "Select Invoice1.Name , Invoice2.Description as ItemName,  sum(Invoice2.QTY) as QTY, sum(Invoice2.Amount) as Amount , sum(Invoice2.Item_Discount) as Item_Discount, isnull(sum(Invoice2.DealRs),0) as DealRs from (Invoice2 inner join invoice1 on Invoice2.InvNo=Invoice1.InvNo) inner join BrandName on Invoice2.BrandName=BrandName.Code where Invoice1.PartyCode='" + PartyCode + "' and BrandName.Code='" + BrandID + "' and Invoice1.date1>='" + StartDate + "' and Invoice1.date1<='" + EndDate + "' group by Invoice1.Name , Invoice2.Description";
            }
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);
            GridView1.DataSource = dset;
            GridView1.DataBind();

        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                LoadReport(Convert.ToDateTime(StartDate.Text), Convert.ToDateTime(txtEndDate.Text));
            }
            catch (Exception ex)
            {
                Response.Write("error  = " + ex.Message);

            }


        }

        decimal TotQty = 0;
        decimal TotAmount = 0;
        decimal TotItem_Discount = 0;
        decimal TotDealRs = 0;
        decimal TotNetAcount = 0;

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // add the UnitPrice and QuantityTotal to the running total variables                
                TotQty += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Qty"));
                TotAmount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Amount"));
                TotItem_Discount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Item_Discount"));
                TotDealRs += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "DealRs"));
                Label lblAmount = (Label)e.Row.FindControl("lblNetAmount");
                TotNetAcount += Convert.ToDecimal(lblAmount.Text);
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";
                // for the Footer, display the running totals
                e.Row.Cells[3].Text = TotQty.ToString();
                e.Row.Cells[4].Text = TotAmount.ToString();
                e.Row.Cells[5].Text = TotItem_Discount.ToString();
                e.Row.Cells[6].Text = TotDealRs.ToString();
                e.Row.Cells[7].Text = TotNetAcount.ToString();

                e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[6].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[7].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Font.Bold = true;
            }
        }

        protected void rdoAll_CheckedChanged(object sender, EventArgs e)
        {
            LoadPartyList();
        }

       

        protected void rdoClient_CheckedChanged(object sender, EventArgs e)
        {
            LoadPartyList();
        }



    }
}