﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebPOS.Reports.StockReports
{
    public partial class frmItemWiseLedger : System.Web.UI.Page
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {


                if (!IsPostBack)
                {
                    StartDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                    txtEndDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                    
                }


                

            }


        }
        
        Decimal ItemOpBal(decimal ItemCode, DateTime StartDate)
        {
            Module7 obj = new Module7();
            DateTime dat = obj.StartDateTwo(StartDate);
            decimal OpB = 0;
            SqlDataAdapter cmd = new SqlDataAdapter("select isnull(sum(Opening),0) as Opening from InventoryOpeningBalance where  CompID='" + CompID + "' and ICode='" + ItemCode + "' and Dat='" + dat + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                OpB = Convert.ToInt32(dt.Rows[0]["Opening"]);

            }

            decimal Dr = 0;
            decimal Cr = 0;

            SqlDataAdapter cmd1 = new SqlDataAdapter("select isnull(sum(Received),0) as D,isnull(sum(Issued),0) as C from Inventory where  CompID='" + CompID + "' and ICode='" + ItemCode + "' and Dat>='" + dat + "' and Dat<'" + StartDate + "'", con);
            DataTable dt1 = new DataTable();
            cmd1.Fill(dt1);


            if (dt.Rows.Count > 0)
            {
                //if (dt1.Rows[0].IsNull = false) { }  
                Dr = Convert.ToDecimal(dt1.Rows[0]["D"]);
                Cr = Convert.ToDecimal(dt1.Rows[0]["C"]);
            }


                OpB = OpB + Dr - Cr;

            return OpB;
        }



        

        void LoadItemLedgerSlim(DateTime StartDate, DateTime EndDate)
        {


            ModItem objModItem = new ModItem();
            

            decimal ItemCode = objModItem.ItemCodeAgainstItemName(txtItemName.Text);
            
            
            decimal OpBalance = 0;

            DateTime abc = Convert.ToDateTime(StartDate);
            OpBalance = ItemOpBal(ItemCode, Convert.ToDateTime(abc));




            SqlCommand cmd = new SqlCommand("select Dat,Description,Convert(numeric(18,2), Received) as AmountDr,Convert(numeric(18,2),Issued) as AmountCr,DescriptionOfBillNo,BillNo from Inventory where CompID='" + CompID + "' and ICode='" + ItemCode + "' and Dat>='" + StartDate + "' and Dat<='" + EndDate + "' order by System_Date_Time", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);



            DataTable dt = new DataTable();
            dt.Columns.Add("Date");
            dt.Columns.Add("Description");
            dt.Columns.Add("Debit");
            dt.Columns.Add("Credit");
            dt.Columns.Add("Balance");
            dt.Columns.Add("DescriptionOfBill");
            dt.Columns.Add("BillNo");

            dt.Rows.Add();
            dt.Rows[0]["Date"] = "";
            dt.Rows[0]["Description"] = "Opening Balance";
            dt.Rows[0]["Debit"] = "0";
            dt.Rows[0]["Credit"] = "0";
            dt.Rows[0]["Balance"] = OpBalance;
            dt.Rows[0]["DescriptionOfBill"] = "";
            dt.Rows[0]["BillNo"] = "";


            int j = 1;
            decimal ClosingBalance = 0;
            decimal Debit = 0;
            decimal Credit = 0;
            for (int i = 0; i < dset.Tables[0].Rows.Count; i++)
            {
                Debit = Convert.ToDecimal(dset.Tables[0].Rows[i]["AmountDr"]);
                Credit = Convert.ToDecimal(dset.Tables[0].Rows[i]["AmountCr"]);
                if (j == 1 )
                {
                    ClosingBalance = (OpBalance + Debit) - Credit;
                }
                else
                    if (j > 1 )
                    {
                        ClosingBalance = (ClosingBalance + Debit) - Credit;
                    }

                

                dt.Rows.Add();
                dt.Rows[j]["Date"] = Convert.ToDateTime( dset.Tables[0].Rows[i]["Dat"]).ToString("dd-MMM-yyyy");
                dt.Rows[j]["Description"] = dset.Tables[0].Rows[i]["Description"];
                dt.Rows[j]["Debit"] = dset.Tables[0].Rows[i]["AmountDr"];
                dt.Rows[j]["Credit"] = dset.Tables[0].Rows[i]["AmountCr"];
                dt.Rows[j]["Balance"] = ClosingBalance;
                dt.Rows[j]["DescriptionOfBill"] = dset.Tables[0].Rows[i]["DescriptionOfBillNo"]; ;
                dt.Rows[j]["BillNo"] = dset.Tables[0].Rows[i]["BillNo"];

                j++;
            }


            GridView1.DataSource = dt;
            GridView1.DataBind();


        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                LoadItemLedgerSlim(Convert.ToDateTime(StartDate.Text), Convert.ToDateTime(txtEndDate.Text));
            }
            catch (Exception ex)
            {
                Response.Write("error  = " + ex.Message);

            }

        }

        decimal TotDr = 0;
        decimal TotCr = 0;
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // add the UnitPrice and QuantityTotal to the running total variables
                TotDr += Convert.ToDecimal(e.Row.Cells[2].Text);
                TotCr += Convert.ToDecimal(e.Row.Cells[3].Text);
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";
                // for the Footer, display the running totals
                e.Row.Cells[2].Text = TotDr.ToString();
                e.Row.Cells[3].Text = TotCr.ToString();

                e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Font.Bold = true;
            }
        }



    }
}