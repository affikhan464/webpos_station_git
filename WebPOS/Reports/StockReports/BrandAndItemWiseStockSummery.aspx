﻿<%@ page title="" language="C#" masterpagefile="~/masterpage.Master" autoeventwireup="true" codebehind="BrandAndItemWiseStockSummery.aspx.cs" inherits="WebPOS.Reports.SaleReports.BrandAndItemWiseStockSummery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:content id="Content2" contentplaceholderid="ContentPlaceHolder1" runat="server">

    

			<div class="reports searchAppSection">
				<div class="contentContainer">
					<h2>Brand And Item Wise StockSummery</h2>
					<div class="BMSearchWrapper row align-items-end">
                       <div class="col col-12 col-sm-6 col-md-3">
                           <label>Brand</label>
                             <div class="d-inline-flex w-100">
                                <span class="input input--hoshi checkbox--hoshi float-left w-auto p-0 pt-1 h-auto">
                                    <label>
                                        <input id="brandCheck" class="checkbox" type="checkbox" />
                                        <span></span>
                                    </label>
                                </span>
                                <select id="BrandNameDD" class="dropdown" data-type="BrandName">
                                    <option value="0">Select Brand</option>
                                </select>
                            </div>
                        </div>
                        <div class="col col-12 col-sm-6 col-md-3">
                          <label>Search Item</label>
                             <div class="d-inline-flex w-100">
                                <span class="input input--hoshi checkbox--hoshi float-left w-auto p-0 pt-1 h-auto">
                                    <label>
                                        <input id="searchBoxCheck" class="checkbox" type="checkbox" checked />
                                        <span></span>
                                    </label>
                                </span>
                               <input type="text" id="searchBox" class="form-control  p-2" />
                            </div>
                        </div>
						 <div class="col col-12 col-sm-3 col-md-3">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-search"></i>  Get Report</a>
							</div>
						
                        <div class="col col-12 col-sm-3 col-md-3">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
							</div>

					</div><!-- searchAppSection -->
				</div>
			</div><!-- featuredSection -->
	
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="reports recommendedSection allItemsView">

						<div class="itemsHeader d-flex">
									<div class="one flex-1 phCol ">
										<a href="javascript:void(0)"> 
											S#
										</a>
									</div>
									<div class="one flex-1 phCol">
										<a href="javascript:void(0)"> 
											Code
										</a>
									</div>
									<div class="seven flex-2  phCol">                                                                    
										<a href="javascript:void(0)"> 
											Item Name
										</a>
									</div>
									
									
								</div><!-- itemsHeader -->	
                        <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
						<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow d-flex footerQuantities'>
								<div class='one flex-1'><span></span></div>
								<div class='one flex-1'><span></span></div>
								<div class='seven flex-2'><span></span></div>
                               
								
								
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
    
   </asp:content>
<asp:content runat="server" id="content4" contentplaceholderid="Scripts">
    <script src="<%=ResolveClientUrl("/Script/Dropdown.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
   <script type="text/javascript">


       $(document).ready(function () {
           appendAttribute.init("BrandNameDD", "BrandName");
           setTimeout(function () {
               //getReport();
           }, 1000)
           resizeTable();

           //appendAttribute takes one value one is the DropdownId and Table name from which data is collected.

       });

       $(window).resize(function () {
           resizeTable();

       });
       
       
       $(document).on('click', '#print', function () {

           var brandId = 0;
           var searchText = "";

           if ($("#searchBoxCheck").prop("checked")) {
               searchText = $("#searchBox").val();
           }
           if ($("#brandCheck").prop("checked")) {
               brandId = $("#BrandNameDD .dd-selected-value").val();
           }
           window.open(
               '/Reports/StockReports/Prints/BrandAndItemWiseStockSummeryPrint.aspx?type=individual&brandId=' + brandId + '&searchText=' + searchText,
               '_blank'
           );
       });
       $("#searchBtn").on("click", function (e) {

           $("#currentPage").remove();
           $("<input hidden id='currentPage' value='" + 0 + "' />").appendTo("body");
           clearInvoice();
           getReport();

       });
       function getReport() {
           $(".loader").show();

           var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? "0" : $("#currentPage").val();
           $(".loader").show();
            var data = {
               brandId: 0,
               searchText: ""
           };
           if ($("#searchBoxCheck").prop("checked")) {
               data.searchText = $("#searchBox").val();
           }
           if ($("#brandCheck").prop("checked")) {
               data.brandId = $("#BrandNameDD .dd-selected-value").val();
           }

           $.ajax({
               url: '/Reports/StockReports/BrandAndItemWiseStockSummery.aspx/getReport',
               type: "POST",
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               data: JSON.stringify(data),

               success: function (response) {


                   if (response.d.Success) {
                       if (response.d.Reports.length > 0) {
                           var header = "";
                           QuantityCount = response.d.Reports[0].QuantityCount;
                           for (var j = 0; j < response.d.Reports[0].QuantityCount; j++) {
                               
                                 header += `<div class="stationCol four flex-1 phCol  main-light-background">
								           	        <a class="font-size-12" href="javascript:void(0)"> 
								           		        ${response.d.Reports[0].StationNames[j]}
								           	        </a>
								                </div>`;
                                
                           }
                           $(".stationCol").remove();
                           $(".itemsHeader").append(header);

                           var width = response.d.Reports[0].QuantityCount * 5;
                           $(".quantitiesHeader").addClass("w-" + width);
                           for (var i = 0; i < response.d.Reports.length; i++) {
                               var count = i + 1;
                               var qtyinputs = "";
                               for (var j = 0; j < response.d.Reports[i].QuantityCount; j++) {
                                    qtyinputs += `<div class='four flex-1'><input name='Quantity${j + 1}' type='text' value="${parseFloat(response.d.Reports[i].Quantities[j]).toFixed(2)}" disabled /></div>`;
                                }
                               $("<div class='itemRow newRow d-flex'>" +
                                   "<div class='one flex-1'><span>" + count + "</span></div>" +
                                   "<div class='one flex-1'><span>" + response.d.Reports[i].ItemCode + "</span></div>" +
                                   "<div class='seven flex-2'><span class='font-size-12' >" + response.d.Reports[i].Description + "</span></div>" +
                                   qtyinputs +
                                    "</div>").appendTo(".dayCashAndCreditSale .itemsSection");


                           }
                           $(".loader").hide();
                           calculateData();
                           resizeTable();

                       } else if (response.d.Reports.length == 0 && currentPage == "0") {
                           $(".loader").hide();
                           swal({
                               title: "No Result Found ", type:'error',
                               text: "No Result Found!"
                           });
                       }

                   }
                   else {
                       $(".loader").hide();
                       swal({
                           title: "there is some error",
                           text: response.d.Message
                       });
                   }

               },
               error: function (error) {
                   swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

               }
           });
       }

       function clearInvoice() {


           $(".itemsSection .itemRow").remove();
       }
       function calculateData() {

           var sum;
           var inputs = [];
           var totalQuantity = "";
           for (var j = 0; j < QuantityCount; j++) {
               inputs.push('Quantity' + (j+1));

               totalQuantity += `<div class='stationCol four border-0 flex-1'><input  disabled="disabled"  name='totalQuantity${(j+1)}'/></div>`;
           }
           $(".footerQuantities").append(totalQuantity);

           for (var i = 0; i < inputs.length; i++) {

               var currentInput = inputs[i];
               sum = 0;
               $("input[name = '" + currentInput + "']").each(function () {

                   if (this.value.trim() === "") {
                       sum = (parseFloat(sum) + parseFloat(0));
                   } else {
                       sum = (parseFloat(sum) + parseFloat(this.value));

                   }

               });

               $("[name=total" + currentInput + "]").val('');
               $("[name=total" + currentInput + "]").val(parseFloat(sum).toFixed(2));


           }

       }
   </script>
</asp:content>
