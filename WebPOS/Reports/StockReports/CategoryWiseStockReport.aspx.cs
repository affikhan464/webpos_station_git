﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class CategoryWiseStockReport : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string attributeId, string attribute)
        {
            try
            {

                var model = GetTheReport(attributeId, attribute);
                return new BaseModel { Success = true, Reports = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<ReportViewModel> GetTheReport(string attributeId, string attribute)
        {
            var selectedAttributeType = attribute;
            var selectedAttributeTypeId = attributeId;

            SqlCommand cmd = new SqlCommand("select InvCode.Code,InvCode.Description,BrandName.Name as BrandName,InvCode.qty,isnull(InvCode.Ave_Cost,0) as Ave_Cost, (InvCode.qty*isnull(InvCode.Ave_Cost,0)) as Amount from InvCode inner join BrandName on InvCode.Brand=BrandName.Code where  InvCode.CompID='" + CompID + "' and  qty>0 and " + selectedAttributeType + "=" + selectedAttributeTypeId + " order by BrandName.Name", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<ReportViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new ReportViewModel();

                invoice.ItemCode = Convert.ToString(dt.Rows[i]["Code"]);
                invoice.BrandName = Convert.ToString(dt.Rows[i]["BrandName"]);
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                invoice.Quantity = Convert.ToString(dt.Rows[i]["qty"]);
                invoice.Rate = Convert.ToString(dt.Rows[i]["Ave_Cost"]);
                invoice.Amount = Convert.ToString(dt.Rows[i]["Amount"]);
                invoice.Attribute = attribute;
                invoice.AttributeType = attribute;
                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}