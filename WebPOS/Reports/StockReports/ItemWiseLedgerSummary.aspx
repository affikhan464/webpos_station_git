﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="ItemWiseLedgerSummary.aspx.cs" Inherits="WebPOS.Reports.StockReport.ItemWiseLedgerSummary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
      
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

				<div class="searchAppSection reports">
				<div class="contentContainer">
					<h2>Item Wise Ledger Summary</h2>
					<div class="BMSearchWrapper row">
						<!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->
						   <div class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">From</span>
                                <asp:TextBox ID="fromTxbx" CssClass="datetimepicker fromTxbx Date" runat="server"></asp:TextBox>
								<%--<input id="fromTxbx" class=" datetimepicker fromTxbx"  name="Date" type="text" />--%>
							</div>
							<div class="col col-12 col-sm-6 col-md-3">
                                <span class="userlLabel">To</span>
                                <asp:TextBox ID="toTxbx" CssClass="datetimepicker toTxbx Date" runat="server"></asp:TextBox>

								<%--<input id="toTxbx" class="datetimepicker toTxbx" type="text"  name="Date"/>--%>
							</div>
                   
                        <div class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">Item Name</span>
								<input id="SearchBox"  data-id="itemTextbox" data-type="Item" data-function="GetRecords"  class="SearchBox autocomplete"  name="ItemDescription" type="text" />
							</div>
                        <div class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">Item Code</span>
                                <asp:TextBox ID="ItemCode" ClientIDMode="Static" CssClass="ItemCode"  name="ItemCode"  runat="server"></asp:TextBox>

							</div>
                          
                       
						 <div class="col col-12 col-sm-6 col-md-6">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-spinner"></i> Get Record</a>
							</div>
						
                        <div class="col col-12 col-sm-6 col-md-6">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
							</div>
						
                          <%--<asp:Button ID="Button1" CssClass="printBtn" OnClick="BtnClick" runat="server"  Text="Print" />--%>
					</div><!-- searchAppSection -->
				</div>
			</div><!-- featuredSection -->
	
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="DaySaleGroup2 reports recommendedSection allItemsView">


								<div class="itemsHeader">
									
									<div class="sixteen phCol">
										<a href="javascript:void(0)"> 
										Date
										</a>
									</div>
                                    <div class="sixteen phCol">
										<a href="javascript:void(0)"> 
											Description
										</a>
									</div>
									<div class="thirteen phCol">                                                                    
										<a href="javascript:void(0)"> 
											Debit
										</a>
									</div>
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
											Credit
										</a>
									</div>
								
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Balance
											
										</a>
									</div>
								
                              
								
								</div><!-- itemsHeader -->	
                         <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
								<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow'>
								<div class='sixteen''><span></span></div>
								<div class='sixteen'><span></span></div>
								<div class='thirteen'><input  disabled="disabled"  name='totalDebit'/></div>
								<div class='thirteen'><input  disabled="disabled"  name='totalCredit'/></div>
								<div class='thirteen'><span></span></div>
                                
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
    
	   </asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
     <script src="<%=ResolveClientUrl("/Script/Autocomplete.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>

   <script type="text/javascript">

       var counter = 0;
      
            $(document).ready(function () {
                initializeDatePicker();
                resizeTable();
            });

            $(window).resize(function () {
                resizeTable();
              
            });

         
            $("#toTxbx").on("keypress", function (e) {

                if (e.key == 13) {
                  
                    clearInvoice();
                    counter = 0;
                    getReport();
                    
                }
            });
            $("#searchBtn").on("click", function (e) {
                counter = 0;
               
                clearInvoice();
                getReport();
             
            });

            function getReport() { $(".loader").show();

                $.ajax({
                    url: '/Reports/StockReports/ItemWiseLedgerSummary.aspx/getReport',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ "from": $(".fromTxbx").val(), "to": $(".toTxbx").val(), "itemCode": $(".ItemCode").val() }),
                    success: function (response) {
                      

                        if (response.d.Success) {
                            if (response.d.PrintReport.length > 0) {
                                
                            
                                for (var i = 0; i < response.d.PrintReport.length; i++) {
                                    counter++;
                                    
                                $("<div class='itemRow newRow'>"+
                                   
                                    "<div class='sixteen'><span>" + response.d.PrintReport[i].Text_1 + "</span></div>" +
                                    "<div class='sixteen name' title='" + response.d.PrintReport[i].Text_2 + "'> <span class='font-size-12' >" + response.d.PrintReport[i].Text_2 + "</span></div>" +
                                    "<div class='thirteen'>             <input name='Debit' type='text' value=" + Number(response.d.PrintReport[i].Text_3).toFixed(2) + " disabled /></div>" +
                                    "<div class='thirteen'>      <input name='Credit' value=" + Number(response.d.PrintReport[i].Text_4).toFixed(2) + " disabled /></div>" +
                                    "<div class='thirteen'>      <input name='Balance'  value=" + Number(response.d.PrintReport[i].Text_5).toFixed(2) + " disabled /></div>" +
                                   "</div>").appendTo(".dayCashAndCreditSale .itemsSection");


                            }
                            appendTooltip();
                            calculateData();
                            $(".loader").hide();
                        
                            } else if (response.d.PrintReport.length == 0) {
                                $(".loader").hide(); swal({
                                    title: "No Result Found ",
                                    text: "No Result Found in the selected Date Range"
                                });
                            }
                         
                        }
                        else {
                            $(".loader").hide();
                            swal({
                                title: "there is some error",
                                text: response.d.Message
                            });
                        }

                    },
                    error: function (error) { swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                    }
                });
            }
            function clearInvoice() {


                $(".itemsSection .itemRow").remove();
            }
            function calculateData() {

                var sum;
                var inputs = ["Debit", 'Credit'];
                for (var i = 0; i < inputs.length; i++) {

                    var currentInput = inputs[i];
                    sum = 0;
                    $("input[name = '" + currentInput + "']").each(function () {

                        if (this.value.trim() === "") {
                            sum = (Number(sum) + Number(0));
                        } else {
                            sum = (Number(sum) + Number(this.value));

                        }

                    });

                    $("[name=total" + currentInput + "]").val('');
                    $("[name=total" + currentInput + "]").val(Number(sum).toFixed(2));


                }

            }
   </script>
</asp:Content>
