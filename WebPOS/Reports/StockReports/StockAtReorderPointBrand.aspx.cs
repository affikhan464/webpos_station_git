﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class StockAtReorderPointBrand : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string brandId)
        {
            try
            {

                var model = GetTheReport(brandId);
                return new BaseModel { Success = true, Reports = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<ReportViewModel> GetTheReport(string brandId)
        {

            SqlCommand cmd = new SqlCommand("Select InvCode.Code as ItemCode,Description,qty,ReorderLevel,ReorderQty,isnull(Ave_Cost,0) as Ave_Cost from invCode  inner join BrandName on InvCode.Brand=BrandName.Code  where BrandName.Code=" + brandId + " and InvCode.CompID='" + CompID + "' and  qty<ReorderLevel and Visiable=1 order by description", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<ReportViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new ReportViewModel();
                var InHandQty = Convert.ToInt32(dt.Rows[i]["qty"]);
                var ReorderQty = Convert.ToInt32(dt.Rows[i]["ReorderQty"]);
                var ReorderLevel = Convert.ToInt32(dt.Rows[i]["ReorderLevel"]);
                var requiredQty = (ReorderLevel - InHandQty) + ReorderQty;

                var unitPrice = dt.Rows[i]["Ave_Cost"] != null ? Convert.ToInt32(dt.Rows[i]["Ave_Cost"]) : 0;
                var amountRequired = unitPrice * requiredQty;

                invoice.ItemCode = Convert.ToString(dt.Rows[i]["ItemCode"]);
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                invoice.Quantity = InHandQty.ToString();
                invoice.ReorderQty = requiredQty;
                invoice.ReorderLevel = ReorderLevel;
                invoice.Rate = unitPrice.ToString();
                invoice.Amount = amountRequired.ToString();
                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}