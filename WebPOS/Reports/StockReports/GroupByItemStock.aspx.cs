﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class GroupByItemStock : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        private static SqlCommand cmd;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport( string GroupID,string StartDate,string EndDate)
        {
            try
            {
                var StartDateFrom = DateTime.ParseExact(StartDate + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var EndDateTo = DateTime.ParseExact(EndDate + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);

                var model = GetTheReport( GroupID, StartDateFrom, EndDateTo);
                return new BaseModel { Success = true, PrintReport = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<PrintingTable> GetTheReport(string GroupID, DateTime StartDate, DateTime EndDate)
        {

            //DateTime StartDate =Convert.ToDateTime( "10/01/2019");
            //DateTime EndDate =Convert.ToDateTime( "10/15/2019");
            ModItem objModItem = new ModItem();
            ModViewInventory objModViewInventory = new ModViewInventory();
            SqlCommand cmd = new SqlCommand("select InvCode.Code as ItemCode,BrandName.Name as BrandName,Description,qty, isnull(InvCode.Ave_Cost,0) as  AvgCost ,isnull(InvCode.SellingCost,0) as  SellingCost ,(isnull(InvCode.Ave_Cost,0) * InvCode.qty) as Amount from InvCode inner join BrandName on InvCode.Brand=BrandName.Code  where  InvCode.CompID='" + CompID + "' and  InvCode.GroupID=" + GroupID + " and InvCode.Active=1  order by Description", con);
                

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<PrintingTable>();


            decimal Rate = 0;
            decimal OpeningUnits = 0;
            decimal Received = 0;
            decimal Issued = 0;
            decimal Balance = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var PrintingT = new PrintingTable();

               
                PrintingT.ItemCode= Convert.ToString(dt.Rows[i]["ItemCode"]);

                OpeningUnits = objModViewInventory.UpToDateOpeningBalanceNormal(Convert.ToDecimal(PrintingT.ItemCode), StartDate);
                Received = objModViewInventory.ItemQtyReceivedFromToNormal(Convert.ToDecimal(PrintingT.ItemCode), StartDate, EndDate);
                Issued =objModViewInventory.ItemQtyIssuedFromToNormal(Convert.ToDecimal(PrintingT.ItemCode), StartDate, EndDate);
                Balance = (OpeningUnits + Received) - Issued;


                if (OpeningUnits !=0 || Received !=0 || Issued !=0)
                {
                    PrintingT.BrandNAme = Convert.ToString(dt.Rows[i]["BrandName"]);
                    PrintingT.ItemName = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                    PrintingT.Opening = Convert.ToString(OpeningUnits);
                    PrintingT.Received = Convert.ToString(Received);
                    PrintingT.Issued = Convert.ToString(Issued);
                    PrintingT.Balance = Convert.ToString(Balance);
                    model.Add(PrintingT);
                }
            }
            model = model.OrderBy(a => a.ItemName).ThenBy(x => x.BrandNAme).ToList();
            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}