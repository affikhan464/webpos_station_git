﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="ItemWiseLedger.aspx.cs" Inherits="WebPOS.Reports.StockReport.ItemWiseLedger" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
      
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

				<div class="searchAppSection reports">
				<div class="contentContainer">
					<h2>Item Wise Ledger</h2>
					<div class="BMSearchWrapper row">
						<!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->
						   <div class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">From</span>
                                <asp:TextBox ID="fromTxbx" CssClass="datetimepicker fromTxbx Date" runat="server"></asp:TextBox>
								<%--<input id="fromTxbx" class=" datetimepicker fromTxbx"  name="Date" type="text" />--%>
							</div>
							<div class="col col-12 col-sm-6 col-md-3">
                                <span class="userlLabel">To</span>
                                <asp:TextBox ID="toTxbx" CssClass="datetimepicker toTxbx Date" runat="server"></asp:TextBox>

								<%--<input id="toTxbx" class="datetimepicker toTxbx" type="text"  name="Date"/>--%>
							</div>
                   
                        <div class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">Item Name</span>
								<input id="SearchBox"  data-id="itemTextbox" data-type="Item" data-function="GetRecords"  class="SearchBox autocomplete"  name="ItemDescription" type="text" />
							</div>
                        <div class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">Item Code</span>
                                <asp:TextBox ID="ItemCode" ClientIDMode="Static" CssClass="ItemCode"  name="ItemCode"  runat="server"></asp:TextBox>

							</div>
                          
                       
						 <div class="col col-12 col-sm-6 col-md-6">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-spinner"></i> Get Record</a>
							</div>
						
                        <div class="col col-12 col-sm-6 col-md-6">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
							</div>
						
                          <%--<asp:Button ID="Button1" CssClass="printBtn" OnClick="BtnClick" runat="server"  Text="Print" />--%>
					</div><!-- searchAppSection -->
				</div>
			</div><!-- featuredSection -->
	
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="DaySaleGroup2 reports recommendedSection allItemsView">


								<div class="itemsHeader">
									
									<div class="sixteen phCol">
										<a href="javascript:void(0)"> 
										Date
										</a>
									</div>
                                    <div class="sixteen phCol">
										<a href="javascript:void(0)"> 
											Description
										</a>
									</div>
									<div class="thirteen phCol">                                                                    
										<a href="javascript:void(0)"> 
											Debit
										</a>
									</div>
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
											Credit
										</a>
									</div>
								
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
										Balance
											
										</a>
									</div>
								
                              
								
								</div><!-- itemsHeader -->	
                         <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
								<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow'>
								<div class='sixteen'><span></span></div>
								<div class='sixteen'><span></span></div>
								<div class='thirteen'><input  disabled="disabled"  name='totalDebit'/></div>
								<div class='thirteen'><input  disabled="disabled"  name='totalCredit'/></div>
								<div class='thirteen'><span></span></div>
                                
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
    
	   </asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
     <script src="<%=ResolveClientUrl("/Script/Autocomplete.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>

   <script type="text/javascript">

       var counter = 0;
      
            $(document).ready(function () {
                initializeDatePicker();
                resizeTable();
            });

            $(window).resize(function () {
                resizeTable();
              
            });

         
            $("#toTxbx").on("keypress", function (e) {

                if (e.key == 13) {
                  
                    clearInvoice();
                    counter = 0;
                    getReport();
                    
                }
            });
            $("#searchBtn").on("click", function (e) {
                counter = 0;
               
                clearInvoice();
                getReport();
             
            });

            function getReport() { $(".loader").show();

                $.ajax({
                    url: '/Reports/StockReports/ItemWiseLedger.aspx/getReport',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ "from": $(".fromTxbx").val(), "to": $(".toTxbx").val(), "itemCode": $(".ItemCode").val() }),
                    success: function (response) {
                      

                        if (response.d.Success) {
                            if (response.d.Reports.length>0) {
                                
                            
                            for (var i = 0; i < response.d.Reports.length; i++) {
                                counter++;
                                $("<div class='itemRow newRow'>"+
                                   
                                    "<div class='sixteen'><span>" + response.d.Reports[i].Date + "</span></div>" +
                                    "<div class='sixteen name' title='" + response.d.Reports[i].Description + "'> <span class='font-size-12' >" + response.d.Reports[i].Description + "</span></div>" +
                                    "<div class='thirteen'>             <input name='Debit' type='text' value=" + parseFloat(response.d.Reports[i].Debit).toFixed(2) + " disabled /></div>" +
                                    "<div class='thirteen'>      <input name='Credit' value=" + parseFloat(response.d.Reports[i].Credit).toFixed(2) + " disabled /></div>" +
                                    "<div class='thirteen'>      <input name='Balance'  value=" + parseFloat(response.d.Reports[i].Balance).toFixed(2) + " disabled /></div>" +
                                   "</div>").appendTo(".dayCashAndCreditSale .itemsSection");


                            }
                            appendTooltip();
                            calculateData();
                            $(".loader").hide();
                        
                            } else if (response.d.Reports.length == 0 ) {
                                $(".loader").hide(); swal({
                                    title: "No Result Found ", type:'error',
                                    text: "No Result Found in the selected Date Range"
                                });
                            }
                         
                        }
                        else {
                            $(".loader").hide();
                            swal({
                                title: "there is some error",
                                text: response.d.Message
                            });
                        }

                    },
                    error: function (error) { swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                    }
                });
            }
            function clearInvoice() {


                $(".itemsSection .itemRow").remove();
            }
            function calculateData() {

                var sum;
                var inputs = ["Debit", 'Credit'];
                for (var i = 0; i < inputs.length; i++) {

                    var currentInput = inputs[i];
                    sum = 0;
                    $("input[name = '" + currentInput + "']").each(function () {

                        if (this.value.trim() === "") {
                            sum = (parseFloat(sum) + parseFloat(0));
                        } else {
                            sum = (parseFloat(sum) + parseFloat(this.value));

                        }

                    });

                    $("[name=total" + currentInput + "]").val('');
                    $("[name=total" + currentInput + "]").val(parseFloat(sum).toFixed(2));


                }

            }
   </script>
</asp:Content>
