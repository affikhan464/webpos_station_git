﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class ProcessedItemReceivingSummary : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport( string StartDate,string EndDate, int BrandID)
        {
            try
            {
                var fromDate = DateTime.ParseExact(StartDate + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(EndDate + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);

                var model = GetTheReport(fromDate, toDate, BrandID);
                return new BaseModel { Success = true, PrintReport = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<PrintingTable> GetTheReport(DateTime StartDate, DateTime EndDate,int BrandID)
        {
            var module7 = new Module7();
         
            


            
            SqlCommand cmd = new SqlCommand("Select ProcessDept_Detail.Code_Received, ProcessDept_Detail.Description_Received,sum(ProcessDept_Detail.Qty_Received)as Qty_Received  from (ProcessDept_Master INNER JOIN ProcessDept_Detail ON ProcessDept_Master.InvNo = ProcessDept_Detail.InvNo) where ProcessDept_Master.CompID='" + CompID + "' and ProcessDept_Detail.Qty_Received>0 and ProcessDept_Master.Date1>='" + StartDate + "' and  ProcessDept_Master.Date1<='" + EndDate + "'  group by  ProcessDept_Detail.Code_Received,ProcessDept_Detail.Description_Received order by ProcessDept_Detail.Description_Received ", con);




            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);






            var model = new List<PrintingTable >();
            DateTime dat = StartDate;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var PTRow = new PrintingTable();
                // StartDate.ToString("dd-MMM-yyyy"),
                //dat =Convert.ToDateTime(dt.Rows[i]["Date1"]);
                //PTRow.Date1 = dat.ToString("dd-MMM-yyyy") ;
                //PTRow.VoucherNo = Convert.ToString(dt.Rows[i]["InvNo"]);
                PTRow.ItemCode = Convert.ToString(dt.Rows[i]["Code_Received"]);
                PTRow.ItemName = Convert.ToString(dt.Rows[i]["Description_Received"]);
                PTRow.Received = Convert.ToString(dt.Rows[i]["Qty_Received"]);
                //PTRow.Rate = Convert.ToString(dt.Rows[i]["Rate_Received"]);
               // PTRow.Amount = Convert.ToString(dt.Rows[i]["Amount_Received"]);

                model.Add(PTRow);
            }

            return model.OrderBy(a=>a.Text_3).ToList();



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}