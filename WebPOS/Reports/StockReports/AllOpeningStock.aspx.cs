﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class AllOpeningStock : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport()
        {
            try
            {

                var model = GetTheReport();
                return new BaseModel { Success = true, Reports = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<ReportViewModel> GetTheReport()
        {

            Module7 objModule7 = new Module7();

            SqlCommand cmd = new SqlCommand(@"select 
                InventoryOpeningBalance.ICode as ItemCode,
                Cost_Per_Unit,
                InvCode.Description,
                BrandName.Name as BrandName,
                Convert(numeric(18, 2),
                sum(InventoryOpeningBalance.Opening)) as Opening
                from InventoryOpeningBalance
                inner join InvCode on InventoryOpeningBalance.ICode = InvCode.Code
                join BrandName on InvCode.Brand = BrandName.Code
                where InvCode.CompID = '" + CompID + @"' and
                Opening > 0  and
                Dat = '" + objModule7.StartDate() + @"'
                group by InventoryOpeningBalance.ICode,
                InvCode.Description,
                BrandName.Name,
                InventoryOpeningBalance.Cost_Per_Unit,
                InventoryOpeningBalance.Total_Value
                order by InvCode.Description", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<ReportViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new ReportViewModel();
                var Quantity = Convert.ToInt32(dt.Rows[i]["Opening"]);
                var cost = Convert.ToInt32(dt.Rows[i]["Cost_Per_Unit"]);
                invoice.ItemCode = Convert.ToString(dt.Rows[i]["ItemCode"]);
                invoice.BrandName = Convert.ToString(dt.Rows[i]["BrandName"]);
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                invoice.Quantity = Convert.ToString(dt.Rows[i]["Opening"]);
                invoice.Amount = (Quantity * cost).ToString();
                invoice.UnitCost = cost;
                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}