﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class ModelWiseStock : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string ModelID)
        {
            try
            {

                var model = GetTheReport(ModelID);
                return new BaseModel { Success = true, PrintReport = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<PrintingTable> GetTheReport(string ModelID)
        {
            ModItem objModItem = new ModItem();
            SqlCommand cmd = new SqlCommand("Select InvCode.Code,InvCode.Ave_Cost,InvCode.Reference,InvCode. Reference2,Description,InvCode.qty,BrandName.Name as Brand,Model.Name as ModelName,Class.Name as ClassName from (invCode inner join BrandName on BrandName.Code=InvCode.Brand) inner join Model on Model.Code=InvCode.Model inner join Class on Class.ID=InvCode.Class where  InvCode.CompID='" + CompID + "' and  InvCode.Model=" + ModelID + "  and Visiable=1 order by description", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<PrintingTable>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var PrintRow = new PrintingTable();
               

               
                

                PrintRow.ItemCode = Convert.ToString(dt.Rows[i]["Code"]);
                PrintRow.BrandNAme = Convert.ToString(dt.Rows[i]["Brand"]);
                PrintRow.ItemName = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                PrintRow.Reference = Convert.ToString(dt.Rows[i]["Reference"]);
                PrintRow.Reference2 = Convert.ToString(dt.Rows[i]["Reference2"]);
                PrintRow.ModelName = Convert.ToString(dt.Rows[i]["ModelName"]);
                PrintRow.ClassName = Convert.ToString(dt.Rows[i]["ClassName"]);
                PrintRow.Rate = Convert.ToString(dt.Rows[i]["Ave_Cost"]);
                PrintRow.InHandQty = Convert.ToString(dt.Rows[i]["Qty"]);

                //PrintRow.ReorderLevel = ReorderLevel.ToString();
                //PrintRow.InHandQty = InHandQty.ToString();
                //PrintRow.ShortQty = Convert.ToString(ReorderLevel - InHandQty);
                //PrintRow.ReOrderQty = ReorderQty.ToString();
                //PrintRow.RequiredQty = requiredQty.ToString(); // 
                //PrintRow.LastPurchasePrice = Convert.ToString(objModItem.LastPurchasePrice(PrintRow.ItemCode));
                //PrintRow.AmountRequired = Convert.ToString(Convert.ToDecimal(PrintRow.LastPurchasePrice) * Convert.ToDecimal(PrintRow.RequiredQty));

                model.Add(PrintRow);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}