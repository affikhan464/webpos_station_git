﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class StationStockReportBrandWise : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(int brandId, int stationId, int multiplyTo)
        {
            try
            {

                var model = GetTheReport(brandId, stationId, multiplyTo);
                return new BaseModel { Success = true, Reports = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<ReportViewModel> GetTheReport(int brandId, int stationId, int multiplyTo)
        {
            var module7 = new Module7();
            var brandCheck = string.Empty;
            var stationCheck = string.Empty;
            var stationQtyWhereClause = string.Empty;
            SqlCommand cmd;
            if (brandId != 0)
                brandCheck = " and BrandName.Code=" + brandId;
            if (stationId != 0)
            {

                stationCheck = @",
                    IsNull((SELECT Current_QTY
                        FROM InventoryOpeningBalance
                        Where StationID = " + stationId + @" and ICode = InvCode.code and Dat = '" + module7.StartDate().ToString("MM/dd/yyyy") + @"'
                    ),0) 
                    as stationqty";
                stationQtyWhereClause = "  WHERE stationqty>0  ";
            }
            cmd = new SqlCommand("SELECT * FROM ( select BrandName.Name as BrandName " + stationCheck + ", InvCode.Code as ItemCode,Description,qty, isnull(InvCode.Ave_Cost,0) as 'UnitPrice',(isnull(InvCode.Ave_Cost,0) * InvCode.qty) as Value,isnull(InvCode.SellingCost,0) as SellingCost,isnull(InvCode.PurchasingCost,0) as PurchasingCost from InvCode " +
                "inner join BrandName on InvCode.Brand=BrandName.Code  " +
                "where  InvCode.CompID='" + CompID + "' and " +
                " InvCode.Nature=1 and InvCode.Active=1 " +
                 brandCheck +
                 " and InvCode.qty>0 ) as InnerTable  "+ stationQtyWhereClause+ " order by BrandName, Description", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<ReportViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new ReportViewModel();

                invoice.ItemCode = Convert.ToString(dt.Rows[i]["ItemCode"]);
                invoice.BrandName = Convert.ToString(dt.Rows[i]["BrandName"]);
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                invoice.Quantity = Convert.ToString(dt.Rows[i]["qty"]);
                invoice.Rate = Convert.ToString(dt.Rows[i]["UnitPrice"]);
                invoice.SellingCost = Convert.ToString(dt.Rows[i]["SellingCost"]);
                invoice.PurchasingCost = Convert.ToString(dt.Rows[i]["PurchasingCost"]);
                
                if (stationId!=0)
                {
                    invoice.Quantity = Convert.ToString(dt.Rows[i]["stationqty"]);
                }
                if (multiplyTo==1)
                {
                    invoice.Amount = (Convert.ToDecimal(invoice.Quantity) * Convert.ToDecimal(invoice.Rate)).ToString();
                }
                else if (multiplyTo == 2)
                {
                    invoice.Amount = (Convert.ToDecimal(invoice.Quantity) * Convert.ToDecimal(invoice.SellingCost)).ToString();
                }
                else if (multiplyTo == 3)
                {
                    invoice.Amount = (Convert.ToDecimal(invoice.Quantity) * Convert.ToDecimal(invoice.PurchasingCost)).ToString();
                }
                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}