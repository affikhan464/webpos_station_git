﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="TotalStockWithOutAmount.aspx.cs" Inherits="WebPOS.Reports.SaleReports.TotalStockWithOutAmount" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

			<div class="dayCashAndCreditSale searchAppSection">
				<div class="contentContainer">
					<h2>Total Stock With Out Amount</h2>
					<div class="BMSearchWrapper clearfix row">
						 <div class="col col-12 col-sm-6 col-md-6">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-list-ul"></i>  Get Report</a>
							</div>
						
                        <div class="col col-12 col-sm-6 col-md-6">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
							</div>

					</div><!-- searchAppSection -->
				</div>
			</div><!-- featuredSection -->
	
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="reports recommendedSection allItemsView">


								<div class="itemsHeader">
									<div class="seven phCol">
										<a href="javascript:void(0)"> 
											S#
										</a>
									</div>
									<div class="seven phCol">
										<a href="javascript:void(0)"> 
											Item Code
										</a>
									</div>
                                    <div class="thirteen phCol">
										<a href="javascript:void(0)"> 
											Brand Name
										</a>
									</div>
									<div class="thirteen  phCol">                                                                    
										<a href="javascript:void(0)"> 
											Item Name
										</a>
									</div>
									<div class="thirteen phCol">
										<a href="javascript:void(0)"> 
											Qty
										</a>
									</div>
									
									
								</div><!-- itemsHeader -->	
                        <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
						<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow'>
								<div class='seven'><span></span></div>
								<div class='seven'><span></span></div>
								<div class='thirteen'><span></span></div>
								<div class='thirteen'><span></span></div>
								<div class='thirteen'><input  disabled="disabled"  name='totalQuantity'/></div>
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
    
	  
   </asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">

   <script type="text/javascript">

       
            $(document).ready(function () {
                getReport();
                resizeTable();
            });

            $(window).resize(function () {
                resizeTable();
              
            });

         

            $("#searchBtn").on("click", function (e) {
                
                $("#currentPage").remove();
                $("<input hidden id='currentPage' value='" + 0 + "' />").appendTo("body");
                    clearInvoice();
                    getReport();
             
            });
            function getReport() { $(".loader").show();

                var currentPage=$("#currentPage").val()=="" || $("#currentPage").val()==undefined ?"0":$("#currentPage").val();
                $(".loader").show();
                $.ajax({
                    url: '/Reports/StockReports/TotalStockWithAmount.aspx/getReport',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                   
                    success: function (response) {
                      

                        if (response.d.Success) {
                            if (response.d.Reports.length > 0) {
                            
                                for (var i = 0; i < response.d.Reports.length; i++) {
                                var count = i + 1;
                                $("<div class='itemRow newRow'>" +
                                    "<div class='seven'><span>" + count + "</span></div>" +
                                    "<div class='seven'><span>" + response.d.Reports[i].ItemCode + "</span></div>" +
                                    "<div class='thirteen name'><input value='" + response.d.Reports[i].BrandName + "' disabled /></div>" +
                                    "<div class='thirteen name' title='" + response.d.Reports[i].Description + "'><span class='font-size-12' >" + response.d.Reports[i].Description + "</span></div>" +
                                    "<div class='thirteen'><input name='Quantity' type='text' id='newQuantityTextbox_" + i + "' value=" + parseFloat(response.d.Reports[i].Quantity).toFixed(2) + " disabled /></div>" +
                                    "</div>").appendTo(".dayCashAndCreditSale .itemsSection");


                            }
                            $(".loader").hide();
                            appendTooltip();
                            calculateData();
                        
                            } else if (response.d.Reports.length == 0 && currentPage == "0") {
                                $(".loader").hide();
                                swal({
                                    title: "No Result Found ", type:'error',
                                    text: "No Result Found!"
                                });
                            }
                         
                        }
                        else {
                            $(".loader").hide();
                            swal({
                                title: "there is some error",
                                text: response.d.Message
                            });
                        }

                    },
                    error: function (error) { swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                    }
                });
            }

            function clearInvoice() {

            
                $(".itemsSection .itemRow").remove();
            }
            function calculateData() {

                var sum;
                var inputs = ["Quantity"];
                for (var i = 0; i < inputs.length; i++) {

                    var currentInput = inputs[i];
                    sum = 0;
                    $("input[name = '" + currentInput + "']").each(function () {

                        if (this.value.trim() === "") {
                            sum = (parseFloat(sum) + parseFloat(0));
                        } else {
                            sum = (parseFloat(sum) + parseFloat(this.value));

                        }

                    });

                    $("[name=total" + currentInput + "]").val('');
                    $("[name=total" + currentInput + "]").val(parseFloat(sum).toFixed(2));


                }

            }
   </script>
</asp:Content>
