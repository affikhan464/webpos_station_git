﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using WebPOS.Model;
using System.Web.Script.Services;
using System.Web.Services;

namespace WebPOS.Reports.StockReports
{
    public partial class StockEqualToZero : System.Web.UI.Page
    {
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport()
        {
            try
            {

                var model = GetTheReport();
                return new BaseModel { Success = true, Reports = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<ReportViewModel> GetTheReport()
        {

            SqlCommand cmd = new SqlCommand("select InvCode.Code,InvCode.Description,InvCode.qty,BrandName.Name as BrandName from (InvCode inner join BrandName on BrandName.Code=InvCode.Brand)  where  InvCode.CompID='" + CompID + "' and  InvCode.Nature=1 and InvCode.Active=1 and InvCode.qty=0 order by BrandName.Name , InvCode.Description", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<ReportViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new ReportViewModel();
                invoice.BrandName = Convert.ToString(dt.Rows[i]["BrandName"]);

                invoice.ItemCode = Convert.ToString(dt.Rows[i]["Code"]);
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                invoice.Quantity = Convert.ToString(dt.Rows[i]["qty"]);
                model.Add(invoice);
            }

            return model;



        }




    }
}