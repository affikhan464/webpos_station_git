﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="StockLessThenZeroBrandWise.aspx.cs" Inherits="WebPOS.Reports.StockReports.StockLessThenZeroBrandWise" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    

			<div class="dayCashAndCreditSale  searchAppSection">
				<div class="contentContainer">
					<h2>Stock Less Then Zero Brand Wise</h2>
				<div class="BMSearchWrapper clearfix row">
                       <div class="DDWrapper col col-12 col-sm-6 col-md-6">
                            <select id="BrandNameDD" class="dropdown">
                                <option value="0">Select Brand</option>
                            </select>
                        </div>
						<div class="col col-12 col-sm-3 col-md-3">
							<a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-list-ul"></i>  Get</a>
							</div>
						
                        <div class="col col-12 col-sm-3 col-md-3">
                            <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>  Print</a>
						 <%--<asp:Button ID="Button2" Cssclass="btn btn-bm"  runat="server" Text="Print" />--%>
							</div>
					</div><!-- searchAppSection -->
				</div>
			</div><!-- featuredSection -->
	
    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
                
				<div class="contentContainer">					

					<div class="reports recommendedSection allItemsView">


								<div class="itemsHeader">
									<div class="seven phCol">
										<a href="javascript:void(0)"> 
											S#
										</a>
									</div>
									<div class="seven phCol">
										<a href="javascript:void(0)"> 
											Item Code
										</a>
									</div>
                                   	<div class="fourteen  phCol">                                                                    
										<a href="javascript:void(0)"> 
											Brand Name
										</a>
									</div>
									<div class="fourteen  phCol">                                                                    
										<a href="javascript:void(0)"> 
											Description
										</a>
									</div>
									<div class="fourteen phCol">
										<a href="javascript:void(0)"> 
											Qty
										</a>
									</div>
									
									
								</div><!-- itemsHeader -->	
                        <div class="loader">
                          <div class="display-table">
                              <div class="display-table-row">
                                  <div class="display-table-cell">
                                         <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                  </div>
                              </div>
                          </div>
                         
                        </div>
						<div class="itemsSection">	
											
							</div>
						<div class="itemsFooter">
							<div class='itemRow newRow'>
								<div class='seven'><span></span></div>
								<div class='seven'><span></span></div>
								<div class='fourteen'><span></span></div>
								<div class='fourteen'><span></span></div>
								<div class='fourteen'><span></span></div>
								
								
							</div>
						</div>
							
						</div>

					</div><!-- recommendedSection -->


	
				</div>
   </asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
   <script src="<%=ResolveClientUrl("/Script/Dropdown.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
   <script type="text/javascript">

       
       $(document).ready(function () {
          
           setTimeout(function () {
               appendAttribute.init("BrandNameDD", "BrandName");
               //getReport();
           }, 1000)
          
                resizeTable();

                //appendAttribute takes two value one is the DropdownId and Table name from which data is collected.
             
            });

            $(window).resize(function () {
                resizeTable();
              
            });


            $("#searchBtn").on("click", function (e) {
                
                    clearInvoice();
                    getReport();
             
            });
            function getReport() { $(".loader").show();

                 $(".loader").show();
                $.ajax({
                    url: '/Reports/StockReports/StockLessThenZeroBrandWise.aspx/getReport',
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ "brandId": $("#BrandNameDD .dd-selected-value").val() }),
                    success: function (response) {
                        if (response.d.Success) {
                            
                            if (response.d.PrintReport.length > 0) {
                  
                                for (var i = 0; i < response.d.PrintReport.length; i++) {
                                var count = i + 1;
                                $("<div class='itemRow newRow'>" +
                                    "<div class='seven'><span>" + count + "</span></div>" +
                                    "<div class='seven'><span>" + response.d.PrintReport[i].ItemCode + "</span></div>" +
                                    "<div class='fourteen name'><input value='" + response.d.PrintReport[i].BrandNAme + "' disabled /></div>" +
                                    "<div class='fourteen name' title='" + response.d.PrintReport[i].ItemName + "'><span class='font-size-12' >" + response.d.PrintReport[i].ItemName + "</span></div>" +
                                    "<div class='fourteen'><input value=" + Number(response.d.PrintReport[i].InHandQty).toFixed(2) + " disabled /></div>" +
                                   "</div>").appendTo(".BMtable .itemsSection");

                            }
                            $(".loader").hide();
                            appendTooltip();
                            calculateData();
                        
                            } else if (response.d.PrintReport.length == 0 && currentPage == "0") {
                                $(".loader").hide();
                                swal({
                                    title: "No Data Found ",
                                    //text: "No Result Found!"
                                });
                            }
                         
                        }
                        else {
                            $(".loader").hide();
                            swal({
                                title: "there is some error",
                                text: response.d.Message
                            });
                        }
                       

                    },
                    error: function (error) {
                        swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                    }
                });
            }

            function clearInvoice() {

            
                $(".itemsSection .itemRow").remove();
            }
           
   </script>
</asp:Content>