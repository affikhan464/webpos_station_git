﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.PurchaseReports
{
    public partial class PartyGroupWisePurchase : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string PGCode)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, PGCode);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, string PGCode)
        {

            SqlCommand cmd = new SqlCommand(@"
                         Select 
                            Purchase1.Dat,
						    Purchase1.InvNo,
                            Purchase1.Vender,
                            Sum(Purchase3.Total) as Total,
						    Sum(Purchase3.Discount) as Discount1,
                            Sum(Purchase3.Paid ) as Received,
                            (SELECT Balance FROM PartyCode where Code=Purchase1.PartyCode) as CurrentBalance                          
                         from Purchase1        
                         inner join Purchase3 on Purchase1.InvNo=Purchase3.InvNo
                         where Purchase1.PartyGPID = '" + PGCode + "' and Purchase1.CompID='" + CompID + "' and Purchase1.Dat>='" + StartDate + "' and Purchase1.Dat<='" + EndDate + "'  GROUP BY Purchase1.InvNo,Purchase1.Dat,Purchase1.Vender,Purchase1.PartyCode   order by Purchase1.InvNo     ", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.Date = Convert.ToDateTime(dt.Rows[i]["Date1"]).ToString("dd/MMM/yyyy");
                invoice.InvoiceNumber = Convert.ToString(dt.Rows[i]["InvNo"]);
                invoice.PartyName = Convert.ToString(dt.Rows[i]["Vender"]);
                invoice.Total = Convert.ToString(dt.Rows[i]["Total"]);
                invoice.NetDiscount = Convert.ToString(dt.Rows[i]["Discount1"]);
                invoice.Paid = Convert.ToString(dt.Rows[i]["Received"]);
                invoice.CurrentBalance = Convert.ToString(dt.Rows[i]["CurrentBalance"]);
                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            var partyGroupCode = "";// PGCodeTxbx.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/PartyGroupWiseSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&partyGroupCode=" + partyGroupCode);
        }
    }
}