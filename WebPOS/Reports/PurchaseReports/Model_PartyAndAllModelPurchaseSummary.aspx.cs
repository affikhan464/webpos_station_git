﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class Model_PartyAndAllModelPurchaseSummary : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string code)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, code);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, string code)
        {


            SqlCommand cmd = new SqlCommand(@"select A.Model,A.Name,
                                                                sum(A.pq) as [PurQty],
		                                                        sum(A.pa) as [PurAmount],
		                                                        sum(A.prq) as [PurReturnQty],
		                                                        sum(A.pra) as [PurReturnAmount]
                  FROM(SELECT invcode.Model, Model.Name, Pur2.QTY[pq], Pur2.amount[pa], 0 as [prq], 0 as [pra]
                  FROM Purchase1 as Pur1 inner JOIN Purchase2 Pur2 ON Pur1.invno = Pur2.invno
                              inner join invcode on Pur2.code = invcode.code
                              inner join Model on invcode.Model = Model.Code
                      where
                              Pur1.dat >= '" + StartDate + @"'
                              and Pur1.dat <= '" + EndDate  + @"'
                              and Pur1.VenderCode = '" + code + @"' 
                      UNION ALL
                     SELECT invcode.Model, Model.Name, 0 as [pq], 0 as [pa], PR2.QTY[prq], PR2.amount as [pra]
                     FROM PurchaseReturn2 PR2 inner join PurchaseReturn1  on PurchaseReturn1.invno = PR2.invno
                             inner join invcode on PR2.code = invcode.code
                              inner join Model on invcode.Model = Model.Code
                      WHERE
                                   PR2.date1 >= '" + StartDate + @"'
                                   and PR2.date1 <= '" + EndDate + @"'
                                   and PurchaseReturn1.PartyCode ='" + code + @"'
                                   ) AS A JOIN invcode InvC ON InvC.code = A.Model
                                   GROUP BY A.Model, A.Name ORDER BY A.Name", con); 




            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();
                var Quantity = Convert.ToInt32(dt.Rows[i]["PurQty"]);
                var ReturnQty = Convert.ToInt32(dt.Rows[i]["PurReturnQty"]);
                var ReturnAmount =  Convert.ToInt32(dt.Rows[i]["PurReturnAmount"]);
                var Amount = Convert.ToInt32(dt.Rows[i]["PurAmount"]);

                invoice.Description = Convert.ToString(dt.Rows[i]["Name"]);
                invoice.Quantity = Quantity.ToString();
                invoice.Amount = Amount.ToString();
                invoice.ReturnQty = Convert.ToString(dt.Rows[i]["PurReturnQty"]);
                invoice.ReturnAmount = Convert.ToString(dt.Rows[i]["PurReturnAmount"]);
                invoice.NetQtySold = (Quantity - ReturnQty).ToString();
                invoice.NetAmountSold = (Amount - ReturnAmount).ToString();

                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            //  Response.Redirect("~/Reports/SaleReports/CReports/ItemAndPartyWiseSaleDetailCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&partyCode=" + partyCode+ "&itemCode=" + itemCode);
        }
    }
}