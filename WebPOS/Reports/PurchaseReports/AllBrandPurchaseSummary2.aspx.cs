﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.PurchaseReports
{
    public partial class AllBrandPurchaseSummary2 : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate)
        {

            SqlCommand cmd = new SqlCommand(@"
                select
				BrandName.Name,
                Sum(Purchase2.Qty) as Qty1 ,
                Sum(Purchase2.Amount) as Amount,
				Isnull(
				    (Select Sum(PurchaseReturn2.Qty) from PurchaseReturn2
				    inner join InvCode on PurchaseReturn2.Code=InvCode.Code 				
                    where Date1>= '" + StartDate + @"'
                    and Date1 <= '" + EndDate + @"'
                    and InvCode.Brand = BrandName.Code				
                    group by InvCode.Brand				
                ),0) as ReturnQty,				
                Isnull(				
                    (Select Sum(PurchaseReturn2.Amount) from PurchaseReturn2				
                   inner join InvCode on PurchaseReturn2.Code=InvCode.Code 				
                    where Date1>= '" + StartDate + @"'
                    and Date1 <= '" + EndDate + @"'
                    and InvCode.Brand = BrandName.Code				
                    group by InvCode.Brand					
                ),0) as ReturnAmount,				
                (Sum(Purchase2.Qty)-Isnull(				
                    (Select Sum(PurchaseReturn2.Qty) from PurchaseReturn2				
                    inner join InvCode on PurchaseReturn2.Code=InvCode.Code 				
                    where Date1>= '" + StartDate + @"'
                    and Date1 <= '" + EndDate + @"'
                    and InvCode.Brand = BrandName.Code				
                    group by InvCode.Brand					
                ),0)) as NetQtySold,				
                (Sum(Purchase2.Amount)-Isnull(				
                    (Select Sum(PurchaseReturn2.Amount) from PurchaseReturn2				
                    inner join InvCode on PurchaseReturn2.Code=InvCode.Code 				
                    where Date1>= '" + StartDate + @"'
                    and Date1 <= '" + EndDate + @"'
                    and InvCode.Brand = BrandName.Code				
                    group by InvCode.Brand				
                ),0)) as NetAmountSold				 
                FROM Purchase1  
                iNNER JOIN Purchase2 ON Purchase1.InvNo = Purchase2.InvNo 
                inner join InvCode on Purchase2.Code=InvCode.Code
                iNNER JOIN BrandName ON InvCode.Brand = BrandName.Code  
                where Purchase1.CompID ='" + CompID + @"' 
                and Purchase1.Dat >= '" + StartDate + @"' 
                and Purchase1.Dat <= '" + EndDate + @"'  
                GROUP BY BrandName.Code, BrandName.Name 
                order by  BrandName.Name   ", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.Description = Convert.ToString(dt.Rows[i]["Name"]);
                invoice.Quantity = Convert.ToString(dt.Rows[i]["Qty1"]);
                invoice.Amount = Convert.ToString(dt.Rows[i]["Amount"]);
                invoice.ReturnQty = Convert.ToString(dt.Rows[i]["ReturnQty"]);
                invoice.ReturnAmount = Convert.ToString(dt.Rows[i]["ReturnAmount"]);
                invoice.NetQtySold = Convert.ToString(dt.Rows[i]["NetQtySold"]);
                invoice.NetAmountSold = Convert.ToString(dt.Rows[i]["NetAmountSold"]);

                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            //  Response.Redirect("~/Reports/SaleReports/CReports/ItemAndPartyWiseSaleDetailCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&partyCode=" + partyCode+ "&itemCode=" + itemCode);
        }
    }
}