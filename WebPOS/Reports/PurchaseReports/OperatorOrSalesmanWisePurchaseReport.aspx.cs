﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.PurchaseReports
{
    public partial class OperatorOrSalesmanWisePurchaseReport : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string salesmanCode)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, salesmanCode);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, string salesmanCode)
        {

            SqlCommand cmd = new SqlCommand(@"
                           Select 
                             Purchase1.InvNo as Bill, 
                             Purchase1.Dat as date,
                             Purchase1.Name as PartyName,
                             Sum(Purchase3.Total ) as Payable,
                             Sum(Purchase3.Discount ) as Discount,
                             Sum(Purchase3.Paid ) as CashPaid,
                             (Sum(Purchase3.Total )-Sum(Purchase3.Discount1 )-Sum(Purchase3.Paid )) as Balance
                           from Purchase1 
                           inner join Purchase3 on Purchase1.InvNo=Purchase3.InvNo
                           where Purchase1.SaleManCode = '" + salesmanCode + @"' 
                           and Purchase1.CompID = '" + CompID + @"'
                           and Purchase1.Dat >= '" + StartDate + @"'
                           and Purchase1.Dat <= '" + EndDate + @"'
                           GROUP BY Purchase1.InvNo, Purchase1.Dat, Purchase1.Name
                           order by Purchase1.InvNo     ", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.InvoiceNumber = Convert.ToString(dt.Rows[i]["Bill"]);
                invoice.Date = Convert.ToDateTime(dt.Rows[i]["date"]).ToString("dd/MMM/yyyy");
                invoice.PartyName = Convert.ToString(dt.Rows[i]["PartyName"]);

                invoice.Total = Convert.ToString(dt.Rows[i]["Payable"]);
                invoice.NetDiscount = Convert.ToString(dt.Rows[i]["Discount"]);
                invoice.Paid = Convert.ToString(dt.Rows[i]["CashPaid"]);
                invoice.Balance = Convert.ToString(dt.Rows[i]["Balance"]);

                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            //var partyCode = PartyCode.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/PartyWiseItemSummaryCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&partyCode=" + partyCode);
        }
    }
}