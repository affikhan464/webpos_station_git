﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.PurchaseReports
{
    public partial class DayPurchaseGroupByItem : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate)
        {

            SqlCommand cmd = new SqlCommand(@"SELECT  
                Purchase2.Code as Code , 
                Purchase2.Description as Description , 
                Sum(Purchase2.Qty) as Qty1 ,
                Sum(Purchase2.Amount) as Amount , 
                Sum(DealRs) as DealRs , 
                sum(Item_Discount) as Item_Discount1
                 
                FROM (
                        Purchase1 INNER JOIN Purchase2 ON Purchase1.InvNo = Purchase2.InvNo) 
                where 
                        Purchase1.CompID='" + CompID + @"' and 
                        Purchase1.Dat>='" + StartDate + @"' and 
                        Purchase1.Dat<='" + EndDate + @"'
                GROUP BY 
                        Purchase2.Code,Purchase2.Description  
                order by 
                        Purchase2.Description     ", con);
            //SqlCommand cmd = new SqlCommand("Select Purchase1.Dat as [Date],Purchase1.InvNo as Invoice , Purchase1.Name as Name,Purchase3.Total as Total,Purchase3.Discount1 as Discount,Purchase3.CashPaid as Paid,Purchase3.Total-(Purchase3.Discount1+Purchase3.CashPaid) as Balance from (Purchase1 inner join Purchase3 on Purchase1.InvNo=Purchase3.InvNo) where Purchase1.CompID='" + CompID + "' and  Purchase1.Dat>='" + StartDate + "' and Purchase1.Dat<='" + EndDate + "' order by Purchase1.InvNo", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.ItemCode = (dt.Rows[i]["Code"]).ToString();
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                invoice.Quantity = Convert.ToString(dt.Rows[i]["Qty1"]);
                invoice.NetDiscount = Convert.ToString(dt.Rows[i]["Item_Discount1"]);
                invoice.DealRs = Convert.ToString(dt.Rows[i]["DealRs"]);
                invoice.Amount = Convert.ToString(dt.Rows[i]["Amount"]);
                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/DaySaleGroupByItemCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}