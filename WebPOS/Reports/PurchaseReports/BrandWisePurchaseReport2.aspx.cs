﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.PurchaseReports
{
    public partial class BrandWisePurchaseReport2 : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string brandCode)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(brandCode, fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }


        public static List<InvoiceViewModel> GetTheReport(String brandCode, DateTime StartDate, DateTime EndDate)
        {

            var model = new List<InvoiceViewModel>();


            ModItem objModItem = new ModItem();
            int BrandID = Convert.ToInt32(brandCode);
            SqlCommand cmd = new SqlCommand(@"Select	  Purchase1.Vender ,
                InvCode.Code,
	         Purchase2.Description as ItemName,
             Sum(Purchase2.Amount) as Amount,
             Sum(Purchase2.Qty) as Qty,
				      
                        (ISNULL((Select Sum(PurchaseReturn2.Amount)
                        from PurchaseReturn2
                        inner join InvCode as item on PurchaseReturn2.Code=item.Code 
                        inner join PurchaseReturn1 on PurchaseReturn2.InvNo=PurchaseReturn1.InvNo 
                        where item.Brand='" + BrandID + @"'
                        
	                    and PurchaseReturn1.Date1>='" + StartDate + @"' 
						and PurchaseReturn1.Date1<='" + EndDate + @"'
                        group by item.Brand),0))
                as ReturnAmount,
                       
                        (ISNULL((Select Sum(PurchaseReturn2.Qty) from PurchaseReturn2 
                        inner join InvCode as item on PurchaseReturn2.Code=item.Code 
                        inner join PurchaseReturn1 on PurchaseReturn2.InvNo=PurchaseReturn1.InvNo                         
                        where item.Brand='" + BrandID + @"'   
                       
	                    and PurchaseReturn1.Date1>='" + StartDate + @"' 
						and PurchaseReturn1.Date1<='" + EndDate + @"'
                        group by item.Brand),0))
                as ReturnQty,		
                Isnull((
                        Select Sum(item.qty) from InvCode	item			
                        where  item.Code=InvCode.Code  			
                ),0) as InHandQty 

             FROM Purchase2       as Purchase2   
             INNER JOIN Purchase1 as Purchase1 ON Purchase2.InvNo = Purchase1.InvNo   
            inner join InvCode on Purchase2.Code=InvCode.Code   
             where Purchase1.CompID = '" + CompID + "'   and InvCode.Brand='" + brandCode + "' and Purchase1.Dat>= '" + StartDate + "'   and Purchase1.Dat <= '" + EndDate + "' GROUP BY  InvCode.Code,Purchase1.Vender, Purchase2.Description order by Purchase1.Vender", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            var dt = new DataTable();
            adpt.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();
                var Quantity = Convert.ToInt32(dt.Rows[i]["Qty"]);
                var ReturnQty = Convert.ToInt32(dt.Rows[i]["ReturnQty"]);
                var ReturnAmount = Convert.ToInt32(dt.Rows[i]["ReturnAmount"]);
                var Amount = Convert.ToInt32(dt.Rows[i]["Amount"]);


                invoice.PartyName = Convert.ToString(dt.Rows[i]["Vender"]);
                invoice.Description = Convert.ToString(dt.Rows[i]["ItemName"]);
                invoice.Quantity = Quantity.ToString();
                invoice.ReturnQty = ReturnQty.ToString();
                invoice.ReturnAmount = ReturnAmount.ToString();
                invoice.Amount = Amount.ToString();
                invoice.NetQtySold = (Quantity - ReturnQty).ToString();
                invoice.NetAmountSold = (Amount - ReturnAmount).ToString();
                invoice.InHandQty = Convert.ToString(dt.Rows[i]["InHandQty"]);

                model.Add(invoice);
            }





            return model;
        }

        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            var brandCode = "";// brandCodeTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/BrandWiseSaleSummaryCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&brandCode=" + brandCode);
        }
    }
}