﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.PurchaseReports
{
    public partial class DayPurchaseGroupByItem2 : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate)
        {

            ////        SqlCommand cmd = new SqlCommand(@"SELECT
            ////            Purchase2.Code as Code , 
            ////            Purchase2.Description as Description , 
            ////            Sum(Purchase2.Qty) as Qty1 ,
            ////            Sum(Purchase2.Amount) as Amount,

            ////Isnull((Select Sum(PurchaseReturn2.Qty) from PurchaseReturn2
            ////where  Date1>='" + StartDate + "'  and Date1 <= '" + EndDate + "' and PurchaseReturn2.Code=Purchase2.Code and PurchaseReturn2.Description=Purchase2.Description                group by PurchaseReturn2.Code,PurchaseReturn2.Description				),0) as ReturnQty,				Isnull((Select Sum(PurchaseReturn2.Amount) from PurchaseReturn2                where Date1 >= '" + StartDate + "'  and Date1 <= '" + EndDate + "' and PurchaseReturn2.Code=Purchase2.Code and PurchaseReturn2.Description=Purchase2.Description				group by PurchaseReturn2.Code,PurchaseReturn2.Description				),0) as ReturnAmount,				(Sum(Purchase2.Qty)-Isnull((Select Sum(PurchaseReturn2.Qty) from PurchaseReturn2                where Date1 >= '" + StartDate + "'  and Date1 <= '" + EndDate + "' and PurchaseReturn2.Code=Purchase2.Code and PurchaseReturn2.Description=Purchase2.Description				group by PurchaseReturn2.Code,PurchaseReturn2.Description				),0)) as NetQtySold				,				(Sum(Purchase2.Amount)-Isnull((Select Sum(PurchaseReturn2.Amount) from PurchaseReturn2                where Date1 >= '" + StartDate + "'  and Date1 <= '" + EndDate + "' and PurchaseReturn2.Code=Purchase2.Code and PurchaseReturn2.Description=Purchase2.Description				group by PurchaseReturn2.Code,PurchaseReturn2.Description				),0)) as NetAmountSold,				Isnull((Select Sum(InvCode.qty) from InvCode				where InvCode.Code=Purchase2.Code and InvCode.Description=Purchase2.Description				),0) as InHandQty                              FROM Purchase1                 INNER JOIN Purchase2 ON Purchase1.InvNo = Purchase2.InvNo                where Purchase1.CompID='" + CompID + "'  and Purchase1.Dat>='" + StartDate + "' and Purchase1.Dat<='" + EndDate + "' GROUP BY Purchase2.Code,Purchase2.Description                  order by Purchase2.Description     ", con);


            SqlCommand cmd = new SqlCommand(@"select A.Code,A.Description,
                         sum(A.pq) as [PurQty],
		         sum(A.pa) as [PurAmount],
		         sum(A.prq) as [PurReturnQty],
		         sum(A.pra) as [PurReturnAmount]
		         
                         FROM(SELECT Pur2.Code, Pur2.Description, Pur2.QTY[pq], Pur2.amount[pa], 0 as [prq], 0 as [pra]
                         FROM Purchase1 as Pur1 inner JOIN Purchase2 Pur2 ON Pur1.invno = Pur2.invno
                              inner join invcode on Pur2.code = invcode.code
                              inner join brandname on invcode.brand = brandname.code

                         where
                              Pur1.dat >= '" + StartDate + @"'  and
                              Pur1.dat <= '" + EndDate + @"'
                        UNION ALL

                        SELECT PR2.Code, PR2.Description, 0 as [pq], 0 as [pa], PR2.QTY[prq], PR2.amount as [pra]
                        FROM PurchaseReturn2 PR2 inner join PurchaseReturn1  on PurchaseReturn1.invno = PR2.invno

                            inner join invcode on PR2.code = invcode.code
                                inner join brandname on invcode.brand = brandname.code

                          WHERE
                                PR2.date1 >= '" + StartDate + @"' and
                                PR2.date1 <= '" + EndDate + @"'

                                ) AS A JOIN invcode InvC ON InvC.code = A.Code

                                GROUP BY A.Code, A.Description ORDER BY A.Description", con);



            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.ItemCode = (dt.Rows[i]["Code"]).ToString();
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                invoice.Quantity = Convert.ToString(dt.Rows[i]["PurQty"]);
                invoice.Amount = Convert.ToString(dt.Rows[i]["PurAmount"]);
                invoice.ReturnQty = Convert.ToString(dt.Rows[i]["PurReturnQty"]);
                invoice.ReturnAmount = Convert.ToString(dt.Rows[i]["PurReturnAmount"]);
                invoice.NetQtySold = Convert.ToString(Convert.ToDecimal(invoice.Quantity) - Convert.ToDecimal(invoice.ReturnQty));
                invoice.NetAmountSold = Convert.ToString(Convert.ToDecimal(invoice.Amount) - Convert.ToDecimal(invoice.ReturnAmount));
                //invoice.InHandQty = Convert.ToString(dt.Rows[i]["InHandQty"]);

                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/DaySaleGroupByItem2CR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}