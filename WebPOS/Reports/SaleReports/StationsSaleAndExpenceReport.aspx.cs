﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class StationsSaleAndExpenceReport : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                                   System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate);
                return new BaseModel { Success = true, Report = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static ReportViewModel GetTheReport(DateTime StartDate, DateTime EndDate)
        {
            var module7 = new Module7();
            var adpt2 = new SqlDataAdapter("Select * From Station", con);
            
            DataTable dt1 = new DataTable();
            adpt2.Fill(dt1);
            var stationIds = new List<int>();
            var stationNames= new List<string>();

            for (int i = 0; i < dt1.Rows.Count; i++)
            {
                stationIds.Add(Convert.ToInt32(dt1.Rows[i]["Id"]));
                stationNames.Add(dt1.Rows[i]["Name"].ToString());
            }
            var salesAmounts = string.Empty;
            for (int i = 0; i < stationIds.Count; i++)
            {
                salesAmounts += "IsNull((SELECT Sum(Total) FROM Invoice3 join invoice1 on invoice3.invno=invoice1.invno Where invoice1.StationID = " + stationIds[i] + " and  invoice1.Date1>='" + StartDate + "' and invoice1.Date1 <= '" + EndDate + "'  ), 0) as SaleAmount" + (i+1) +", ";
            }

            var expnceAmounts = string.Empty;
            for (int i = 0; i < stationIds.Count; i++)
            {
                expnceAmounts += "IsNull((SELECT Sum(AmountDr) FROM GeneralLedger  Where StationID = " + stationIds[i] + " and dateDr>='" + StartDate + "' and dateDr <= '" + EndDate + "' and SecondDescription='OperationalExpencesThroughCash'), 0) as ExpenceAmount" + (i + 1) + ", ";
            }
            SqlCommand cmd = new SqlCommand(@"select " +

                salesAmounts +
                expnceAmounts +
              
                "count(*) as  c  from Invoice3", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
           
          
                var invoice = new ReportViewModel();
                
                invoice.AmountCount = dt1.Rows.Count;
                invoice.Sales = new List<decimal>();
                invoice.Expences = new List<decimal>();
                for (int j = 0; j < dt1.Rows.Count; j++)
                {
                    var amnt = Convert.ToDecimal(dt.Rows[0]["SaleAmount" + (j + 1)]);
                    invoice.Sales.Add(amnt);
                }
                for (int j = 0; j < dt1.Rows.Count; j++)
                {
                    var amnt = Convert.ToDecimal(dt.Rows[0]["ExpenceAmount" + (j + 1)]);
                    invoice.Expences.Add(amnt);
                }
                invoice.StationNames = new List<string>();
                for (int j = 0; j < stationNames.Count; j++)
                {
                    invoice.StationNames.Add(stationNames[j]);
                };
                invoice.StationIds = new List<int>();
                for (int j = 0; j < stationIds.Count; j++)
                {
                    invoice.StationIds.Add(stationIds[j]);
                };
          

            return invoice;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            //var startDate = fromTxbx.Text;
            //var endDate = toTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}