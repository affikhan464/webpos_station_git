﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class BrandWiseItemGP : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, int brandCode)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(brandCode, fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }


        public static List<InvoiceViewModel> GetTheReport(int brandCode, DateTime StartDate, DateTime EndDate)
        {

            var model = new List<InvoiceViewModel>();

            var brandWhereClause = string.Empty;
            if (brandCode != 0)
            {
                brandWhereClause = " and BrandName.Code = " + brandCode + " ";
            }
            ModItem objModItem = new ModItem();
            SqlCommand cmd = new SqlCommand(@"SELECT
              
                Sum(invoice2.Amount) as Amount,
                Sum(invoice2.Qty) as Qty, 
				Invoice2.Purchase_Amount as Cost ,
                invoice2.Description as Description, 
                BrandName.Name as BrandNam
                FROM Invoice1         
                INNER JOIN invoice2 as invoice2 ON Invoice1.InvNo = invoice2.InvNo  
                inner join InvCode as Inventory on invoice2.Code=Inventory.Code
                INNER JOIN BrandName  ON Inventory.Brand = BrandName.Code        
                where Invoice1.CompID = '" + CompID + @"' 
                " + brandWhereClause + @" 
                and Invoice2.ItemNature = 1  
                and Invoice1.Date1 >= '" + StartDate + @"'  
                and Invoice1.Date1 <= '" + EndDate + @"'   
                GROUP BY  BrandName.Name,invoice2.Description,Invoice2.Purchase_Amount
                order by BrandName.Name,invoice2.Description", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            var dt = new DataTable();
            adpt.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                var Quantity = Convert.ToInt32(dt.Rows[i]["Qty"]);
                var Amount = Convert.ToInt32(dt.Rows[i]["Amount"]);

                var cost = Convert.ToInt32(dt.Rows[i]["Cost"]);

                invoice.BrandName = Convert.ToString(dt.Rows[i]["BrandNam"]).Trim();
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                invoice.Quantity = Quantity.ToString();
                invoice.Amount = Amount.ToString();
                invoice.Cost = cost.ToString();
                invoice.Profit = (Amount- cost).ToString();
                //invoice.GP = (Amount- cost).ToString();

                model.Add(invoice);
            }





            return model;
        }

        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            var brandCode = "";// brandCodeTxbx.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/BrandWiseSaleSummaryCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&brandCode=" + brandCode);
        }
    }
}