﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="DayCashAndCreditSaleWithExpense.aspx.cs" Inherits="WebPOS.Reports.SaleReports.DayCashAndCreditSaleWithExpense" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

   
    <link href="../../css/allitems.css?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>" rel="stylesheet"  />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



    <div class="reports searchAppSection">
        <div class="contentContainer">
            <h2>Day Cash And Credit Sale With Expense</h2>
             
            <div class="BMSearchWrapper row align-items-end">
                <!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->
                <div class="col col-12 col-sm-3 col-md-3">
                    <span class="userlLabel">From</span>
                    <asp:TextBox ID="fromTxbx" CssClass="datetimepicker fromTxbx Date" runat="server"></asp:TextBox>
                    <%--<input id="fromTxbx" class=" datetimepicker fromTxbx"  name="Date" type="text" />--%>
                </div>
                <div class="col col-12 col-sm-3 col-md-3">
                    <span class="userlLabel">To</span>
                    <asp:TextBox ID="toTxbx" CssClass="datetimepicker toTxbx Date" runat="server"></asp:TextBox>

                    <%--<input id="toTxbx" class="datetimepicker toTxbx" type="text"  name="Date"/>--%>
                </div>
                <div class="col-12 col-md-3">
                    <label>Station</label>
                    <div class="d-inline-flex w-100">
                        <span class="input input--hoshi checkbox--hoshi float-left w-auto p-0 pt-1 h-auto">
                            <label>
                                <input id="stationCheck" class="checkbox" type="checkbox" checked />
                                <span></span>
                            </label>
                        </span>
                        <select id="StationDD" class="dropdown" data-type="Station">
                            <option value="0">Select Station</option>
                        </select>
                    </div>
                </div>

                <div class="col col-12 col-sm-3 col-md-2">
                    <a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-spinner"></i>Get Record</a>
                </div>

                <div class="col col-12 col-sm-3 col-md-1">
                    <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>Print</a>
                </div>
            </div>
            <!-- searchAppSection -->
            <h2 class="BMSearchWrapper mt-1 text-center profitText hidden bg-light text-dark">
                  (Sale) <span class="totalPaid text-success">0.00</span> - (Expense) <span class="totalExpenseAmount  text-danger">0.00</span> = <span class="totalProfit">0.00</span>
             </h2>
        </div>
    </div>
    <!-- featuredSection -->
    <div class="row">
        <div class="col-12 col-md-6">
            <h3 class="sectionHeading text-center">Sale</h3>

            <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">

                <div class="contentContainer">

                    <div class="reports recommendedSection allItemsView">


                        <div class="itemsHeader">
                            <div class="thirteen phCol">
                                <a href="javascript:void(0)">Date
                                </a>
                            </div>
                            <div class="seven phCol">
                                <a href="javascript:void(0)">Invoice
                                </a>
                            </div>
                            <div class="thirteen  phCol">
                                <a href="javascript:void(0)">Name
                                </a>
                            </div>
                            <div class="thirteen phCol">
                                <a href="javascript:void(0)">Total
                                </a>
                            </div>
                            <div class="eight phCol">
                                <a href="javascript:void(0)">Discount
											
                                </a>
                            </div>
                            <div class="thirteen phCol">
                                <a href="javascript:void(0)">Cash Received
											
                                </a>
                            </div>
                            <div class="thirteen phCol">
                                <a href="javascript:void(0)">Balance
											
                                </a>
                            </div>
                        </div>
                        <!-- itemsHeader -->
                        <div class="loader">
                            <div class="display-table">
                                <div class="display-table-row">
                                    <div class="display-table-cell">
                                        <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="itemsSection sales">
                        </div>
                        <div class="itemsFooter">
                            <div class='itemRow newRow'>
                                <div class='thirteen'><span></span></div>
                                <div class='seven'><span></span></div>
                                <div class='thirteen'><span></span></div>
                                <div class='thirteen'>
                                    <input disabled="disabled" name='totalTotal' />
                                </div>
                                <div class='eight'>
                                    <input disabled="disabled" name='totalDiscount' />
                                </div>
                                <div class='thirteen'>
                                    <input disabled="disabled" name='totalPaid' />
                                </div>
                                <div class='thirteen'>
                                    <input disabled="disabled" name='totalBalance' />
                                </div>

                            </div>
                        </div>

                    </div>

                </div>
                <!-- recommendedSection -->



            </div>
        </div>
        <div class="col-12 col-md-6">
            <h3 class="sectionHeading text-center">Expenses</h3>
            <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">

                <div class="contentContainer">

                    <div class="reports recommendedSection allItemsView">


                        <div class="itemsHeader">
                            <div class="seven phCol">
                                <a href="javascript:void(0)">Date
                                </a>
                            </div>
                            <div class="seven phCol">
                                <a href="javascript:void(0)">Voucher No.
                                </a>
                            </div>
                            <div class="thirteen  phCol">
                                <a href="javascript:void(0)">Expense Head
                                </a>
                            </div>
                            <div class="thirteen phCol">
                                <a href="javascript:void(0)">Expense Amount
                                </a>
                            </div>
                        </div>
                        <!-- itemsHeader -->
                        <div class="loader">
                            <div class="display-table">
                                <div class="display-table-row">
                                    <div class="display-table-cell">
                                        <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="itemsSection expenses">
                        </div>
                        <div class="itemsFooter">
                            <div class='itemRow newRow'>
                                <div class='seven'><span></span></div>
                                <div class='seven'><span></span></div>
                                <div class='thirteen'><span></span></div>
                                <div class='thirteen'>
                                    <input disabled="disabled" name='totalExpenseAmount' />
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <!-- recommendedSection -->



            </div>
        </div>
    </div>
</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    <script src="<%=ResolveClientUrl("/Script/Dropdown.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="/Script/Voucher/ExpenseDetail.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>

    <script type="text/javascript">

        $(document).ready(function () {
            initializeDatePicker();
            appendAttribute.init("StationDD", "Station");
            resizeTable($(".sectionHeading").height());
        });

        $(window).resize(function () {
            resizeTable($(".sectionHeading").height());
        });


        $("#toTxbx").on("keypress", function (e) {
            if (e.key == 13) {
                $("#currentPage").remove();
                $("<input hidden id='currentPage' value='" + 0 + "' />").appendTo("body");
                clearInvoice();
                getReport();

            }
        });

        $("#searchBtn").on("click", function (e) {

            $("#currentPage").remove();
            $("<input hidden id='currentPage' value='" + 0 + "' />").appendTo("body");
            clearInvoice();
            getReport();

        });
        function getReport() {
            $(".loader").show();
            var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? "0" : $("#currentPage").val();

            var data = {
                from: $(".fromTxbx").val(),
                to: $(".toTxbx").val(),
                stationId: 0
            }
            if ($("#stationCheck").prop("checked")) {
                data.stationId = $("#StationDD .dd-selected-value").val();
            }
            $.ajax({
                url: '/Reports/SaleReports/DayCashAndCreditSaleWithExpense.aspx/getReport',
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify(data),
                success: function (response) {
                    if (response.d.Success) {
                        if (response.d.SaleAndExpense.Sales.length > 0 || response.d.SaleAndExpense.Expenses.length > 0) {
                            for (var i = 0; i < response.d.SaleAndExpense.Sales.length; i++) {
                        
                                var sale = response.d.SaleAndExpense.Sales[i];

                                $("<div class='itemRow newRow'>" +
                                    "<div class='thirteen'><span>" + sale.Date + "</span></div>" +
                                    "<div class='seven'><span><a class='text-info text-underline float-left' target='_blank' href='/Correction/SaleEditNew.aspx?InvNo=" + sale.InvoiceNumber + "'>" + sale.InvoiceNumber + "</a></span></div>" +
                                    "<div class='thirteen name'><input  id='newDescriptionTextbox_" + i + "' value='" + sale.PartyName + "' disabled /></div>" +
                                    "<div class='thirteen'><input name='Total' type='text' id='newQtyTextbox_" + i + "' value=" + Number(sale.Total).toFixed(2) + " disabled /></div>" +
                                    "<div class='eight'><input name='Discount'  id='newRateTextbox_" + i + "' value=" + Number(sale.NetDiscount).toFixed(2) + " disabled /></div>" +
                                    "<div class='thirteen'><input name='Paid'  id='newGAmountTextbox_" + i + "' value=" + Number(sale.Paid).toFixed(2) + " disabled /></div>" +
                                    "<div class='thirteen'><input name='Balance'  id='newItemDTextbox_" + i + "' value=" + Number(sale.Balance).toFixed(2) + " disabled /></div>" +
                                    "</div>").appendTo(".dayCashAndCreditSale .itemsSection.sales");
                               
                            }

                            for (var i = 0; i < response.d.SaleAndExpense.Expenses.length; i++) {
                               

                                var expense = response.d.SaleAndExpense.Expenses[i];
                                $("<div class='itemRow newRow'>" +
                                    "<div class='seven'><span>" + expense.Date + "</span></div>" +
                                    "<div class='seven'><span><a class='text-info text-underline float-left' target='_blank' href='/Correction/ExpenseEntryEdit.aspx?v=" + expense.VoucherNo + "'>" + expense.VoucherNo + "</a></span></div>" +
                                    "<div class='thirteen name'><input value='" + expense.ExpenseHead + "' disabled /></div>" +
                                    "<div class='thirteen'><input name='ExpenseAmount' type='text' value=" + Number(expense.Paid).toFixed(2) + " disabled /></div>" +
                                    "</div>").appendTo(".dayCashAndCreditSale .itemsSection.expenses");
                                
                            }
                            $(".loader").hide();
                            calculateData();

                        } else if (response.d.SaleAndExpense.Sales.length == 0 && response.d.SaleAndExpense.Expenses.length == 0 && currentPage == "0") {
                            $(".loader").hide(); swal({
                                title: "No Result Found ", type: 'error',
                                text: "No Result Found!"
                            });
                        }

                    }
                    else {
                        $(".loader").hide();
                        swal({
                            title: "there is some error",
                            text: response.d.Message
                        });
                    }

                },
                error: function (error) {
                    swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                }
            });
        }

        function clearInvoice() {


            $(".itemsSection .itemRow").remove();
        }
        function calculateData() {

            var sum;
            var inputs = ["Total", 'Discount', 'Paid', 'Balance','ExpenseAmount'];
            for (var i = 0; i < inputs.length; i++) {

                var currentInput = inputs[i];
                sum = 0;
                $("input[name = '" + currentInput + "']").each(function () {

                    if (this.value.trim() === "") {
                        sum = (Number(sum) + Number(0));
                    } else {
                        sum = (Number(sum) + Number(this.value));

                    }

                });

                $("[name=total" + currentInput + "]").val('');
                $("[name=total" + currentInput + "]").val(Number(sum).toFixed(2));
                $(".total" + currentInput).text('');
                $(".total" + currentInput).text(Number(sum).toFixed(2));


            }
            var totalProfit = Number($("[name=totalPaid]").val()) - Number($("[name=totalExpenseAmount]").val());
            if (totalProfit >= 0) {
                $(".totalProfit").addClass('text-success');
                $(".totalProfit").removeClass('text-danger');
            } else {
                $(".totalProfit").removeClass('text-success');
                $(".totalProfit").addClass('text-danger');
            }
            $(".totalProfit").text('');
            $(".totalProfit").text(Number(totalProfit).toFixed(2));
        }
    </script>
</asp:Content>
