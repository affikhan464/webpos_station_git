﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class Model_ModelAndAllPartySaleSummary : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string ModelID)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, ModelID);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, string ModelID)
        {

            SqlCommand cmd = new SqlCommand(@"select A.PartyCode, A.Name, A.Model,
                                                    sum(A.sq) as [SaleQty],
                                                    sum(A.sa) as [SaleAmount],
                                                    sum(A.srq) as [SaleReturnQty],
                                                    sum(A.sra) as [SaleReturnAmount]


                                  FROM(SELECT Inv1.PartyCode, Inv1.Name,  invcode.Model, Inv2.QTY[sq], Inv2.amount[sa], 0 as [srq], 0 as [sra]
                                  FROM invoice1 as Inv1 inner JOIN invoice2 Inv2 ON Inv1.invno = Inv2.invno
                                           inner join invcode on inv2.code = invcode.code
                                           inner join Model on invcode.Model = Model.Code

                                                        where
                                                            Inv1.date1 >= '" + StartDate + @"' 
                                                            and Inv1.date1 <= '" + EndDate + @"'
                                                            and invcode.Color = '" + ModelID + @"' 
                                                        UNION ALL

                                         SELECT SaleReturn1.PartyCode, SaleReturn1.Name,invcode.Model, 0 as [sq], 0 as [sa], SR2.QTY[srq], SR2.amount as [sra]
                                         FROM salereturn2 SR2 inner join SaleReturn1 on SR2.invno = salereturn1.invno
                                         inner join invcode on SR2.code = invcode.code
                                         inner join Model on invcode.Model = Model.Code

                                                        WHERE

                                                            SR2.date1 >= '" + StartDate + @"' 
                                                            and SR2.date1 <= '" + EndDate + @"'
                                                            and invcode.Color = '" + ModelID + @"'

                                                            ) AS A JOIN invcode InvC ON InvC.code = A.Model

                                                              GROUP BY A.PartyCode, A.Name, A.Model ORDER BY A.Name ", con);





            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();
                var Quantity = Convert.ToInt32(dt.Rows[i]["SaleQty"]);
                var ReturnQty = Convert.ToInt32(dt.Rows[i]["SaleReturnQty"]);
                var ReturnAmount =  Convert.ToInt32(dt.Rows[i]["SaleReturnAmount"]);
                var Amount = Convert.ToInt32(dt.Rows[i]["SaleAmount"]);


                invoice.PartyName = Convert.ToString(dt.Rows[i]["Name"]);
                invoice.Quantity = Quantity.ToString();
                invoice.ReturnQty =  ReturnQty.ToString();
                invoice.ReturnAmount =  ReturnAmount.ToString();
                invoice.Amount = Amount.ToString();
                invoice.NetQtySold = (Quantity - ReturnQty).ToString();
                invoice.NetAmountSold = (Amount - ReturnAmount).ToString();
                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            var itemCode = "";//brandCodeTxbx.Text;
                              // Response.Redirect("~/Reports/SaleReports/CReports/ItemAndPartyWiseSaleDetailCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&partyCode=" + partyCode+ "&itemCode=" + itemCode);
        }
    }
}