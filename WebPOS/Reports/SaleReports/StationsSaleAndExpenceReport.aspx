﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="StationsSaleAndExpenceReport.aspx.cs" Inherits="WebPOS.Reports.SaleReports.StationsSaleAndExpenceReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



    <div class="reports searchAppSection">
        <div class="contentContainer">
            <h2>Stations Sale And Expence Report</h2>
            <div class="BMSearchWrapper row align-items-end">
                <div class="col col-12 col-sm-6 col-md-3 ">
                    <span class="userlLabel">From</span>
                    <asp:TextBox ID="fromTxbx" CssClass="datetimepicker fromTxbx Date" runat="server"></asp:TextBox>
                </div>
                <div class="col col-12 col-sm-6 col-md-3">
                    <span class="userlLabel">To</span>
                    <asp:TextBox ID="toTxbx" CssClass="datetimepicker toTxbx Date" runat="server"></asp:TextBox>
                </div>
                <div class="col col-12 col-sm-3 col-md-3">
                    <a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-search"></i>Get Report</a>
                </div>

                <div class="col col-12 col-sm-3 col-md-3">
                    <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>Print</a>
                </div>
            </div>
            <!-- searchAppSection -->
        </div>
    </div>
    <!-- featuredSection -->

    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">

        <div class="contentContainer">

            <div class="reports recommendedSection allItemsView">

                <div class="itemsHeader d-flex">
                    <div class="five flex-1 phCol ">
                        <a href="javascript:void(0)">Sale And Expence
                        </a>
                    </div>
                </div>
                <!-- itemsHeader -->
                <div class="loader">
                    <div class="display-table">
                        <div class="display-table-row">
                            <div class="display-table-cell">
                                <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="itemsSection">
                </div>
                <div class="itemsFooter">
                    <div class='itemRow newRow d-flex footerQuantities'>
                        <div class='five flex-1'><span></span></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- recommendedSection -->
    </div>

    <%@ Register Src="~/Transactions/_ExpenseDetail.ascx" TagName="_ExpenseDetail" TagPrefix="uc" %>
    <uc:_expensedetail id="_ExpenseDetail1" runat="server" />

</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    <script src="<%=ResolveClientUrl("/Script/Dropdown.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="/Script/Voucher/ExpenseDetail.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script type="text/javascript">


        $(document).ready(function () {

            setTimeout(function () {
                //getReport();
            }, 1000)
            resizeTable();

            //appendAttribute takes two value one is the DropdownId and Table name from which data is collected.

        });

        $(window).resize(function () {
            resizeTable();
        });


        $(document).on('click', '#print', function () {

            var brandId = 0;
            var searchText = "";

            if ($("#searchBoxCheck").prop("checked")) {
                searchText = $("#searchBox").val();
            }
            if ($("#brandCheck").prop("checked")) {
                brandId = $("#BrandNameDD .dd-selected-value").val();
            }
            //window.open(
            //    '/Reports/StockReports/Prints/BrandAndItemWiseStockSummeryPrint.aspx?type=individual&brandId=' + brandId + '&searchText=' + searchText,
            //    '_blank'
            //);
        });
        $("#searchBtn").on("click", function (e) {

            $("#currentPage").remove();
            $("<input hidden id='currentPage' value='" + 0 + "' />").appendTo("body");
            clearInvoice();
            getReport();

        });
        function getReport() {
            $(".loader").show();

            var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? "0" : $("#currentPage").val();
            $(".loader").show();

            $.ajax({
                url: '/Reports/SaleReports/StationsSaleAndExpenceReport.aspx/getReport',
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ "from": $(".fromTxbx").val(), "to": $(".toTxbx").val() }),

                success: function (response) {
                    var report = response.d.Report;

                    if (response.d.Success) {
                        if (report == null) {
                            $(".loader").hide();
                            swal({
                                title: "No Result Found ", type: 'error',
                                text: "No Result Found!"
                            });
                        }
                        else {
                            var header = "";
                            AmountCount = report.AmountCount;
                            for (var j = 0; j < AmountCount; j++) {

                                header += `<div class="stationCol five flex-1 phCol  main-light-background">
								               <a href="javascript:void(0)"> 
								                    ${report.StationNames[j]}
								               </a>
								           </div>`;

                            }
                            $(".stationCol").remove();
                            $(".itemsHeader").append(header);

                            var columns = "";
                            for (var j = 0; j < report.Sales.length; j++) {
                                columns += `<div class='five flex-1'><span class="text-right">${Number(report.Sales[j]).toFixed(2)}</span></div>`;
                            }
                            $("<div class='itemRow newRow d-flex'>" +
                                "<div class='five flex-1'><span>Sales</span></div>" +
                                columns +
                                "</div>").appendTo(".dayCashAndCreditSale .itemsSection");

                            var columns = "";
                            for (var j = 0; j < report.Expences.length; j++) {
                                if (Number(report.Expences[j]) > 0) {
                                    columns += `<div class='five flex-1'><span><a href="javascript:void(0)" class="text-info" data-stationid="${report.StationIds[j]}" data-expenseamount="${Number(report.Expences[j]).toFixed(2)}" onclick="getExpenseDetails(this)">${Number(report.Expences[j]).toFixed(2)}</a></span></div>`;
                                } else {
                                    columns += `<div class='five flex-1'><span class="text-right">${Number(report.Expences[j]).toFixed(2)}</span></div>`;
                                }
                            }
                            $("<div class='itemRow newRow d-flex'>" +
                                "<div class='five flex-1'><span>Expences</span></div>" +
                                columns +
                                "</div>").appendTo(".dayCashAndCreditSale .itemsSection");

                            $(".loader").hide();
                            resizeTable();

                        }
                    }
                    else {
                        $(".loader").hide();
                        swal({
                            title: "there is some error",
                            text: response.d.Message, type: "error"
                        });
                    }
                },
                error: function (error) {
                    swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                }
            });
        }

        function clearInvoice() {
            $(".itemsSection .itemRow").remove();
        }
      
    </script>
</asp:Content>
