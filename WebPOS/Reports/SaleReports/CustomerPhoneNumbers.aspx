﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="CustomerPhoneNumbers.aspx.cs" Inherits="WebPOS.Reports.SaleReports.CustomerPhoneNumbers" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



    <div class="brandWiseGroupByPartySaleSummary brandWiseSaleSummary reports searchAppSection">
        <div class="contentContainer">
            <h2>All Customers Data</h2>
            <div class="BMSearchWrapper row align-items-end">
                <!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->
                <div class="col col-12 col-sm-6 col-md-3">
                    <span class="userlLabel">From</span>
                    <asp:TextBox ID="fromTxbx" CssClass="datetimepicker fromTxbx Date" runat="server"></asp:TextBox>
                    <%--<input id="fromTxbx" class=" datetimepicker fromTxbx"  name="Date" type="text" />--%>
                </div>
                <div class="col col-12 col-sm-6 col-md-3">
                    <span class="userlLabel">To</span>
                    <asp:TextBox ID="toTxbx" CssClass="datetimepicker toTxbx Date" runat="server"></asp:TextBox>

                    <%--<input id="toTxbx" class="datetimepicker toTxbx" type="text"  name="Date"/>--%>
                </div>
                <div class="col-12 col-md-2">
                    <label>Station</label>
                    <div class="d-inline-flex w-100">
                        <span class="input input--hoshi checkbox--hoshi float-left w-auto p-0 pt-1 h-auto">
                            <label>
                                <input id="stationCheck" class="checkbox" type="checkbox" />
                                <span></span>
                            </label>
                        </span>
                        <select id="StationDD" class="dropdown" data-type="Station">
                            <option value="0">Select Station</option>
                        </select>
                    </div>
                </div>
                <div class="col col-12 col-sm-6 col-md-2">
                    <a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-list-ul"></i>Get Report</a>
                </div>

                <div class="col col-12 col-sm-6 col-md-2">
                    <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>Print</a>

                </div>

                <%--<asp:Button ID="Button1" CssClass="printBtn" OnClick="BtnClick" runat="server"  Text="Print" />--%>
            </div>
            <!-- searchAppSection -->
        </div>
    </div>
    <!-- featuredSection -->

    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">

        <div class="contentContainer">

            <div class="DaySaleGroup2 reports recommendedSection allItemsView">


                <div class="itemsHeader">

                    <div class="seven phCol">
                        <a href="javascript:void(0)">Serial #
                        </a>
                    </div>

                    <div class="thirteen phCol">
                        <a href="javascript:void(0)">Customer Name
                        </a>
                    </div>
                    <div class="thirteen phCol">
                        <a href="javascript:void(0)">Address
                        </a>
                    </div>
                    <div class="thirteen phCol">
                        <a href="javascript:void(0)">Mobile Number
                        </a>
                    </div>
                    <div class="thirteen phCol">
                        <a href="javascript:void(0)">Phone Numeber
                        </a>
                    </div>
                </div>
                <!-- itemsHeader -->
                <div class="loader">
                    <div class="display-table">
                        <div class="display-table-row">
                            <div class="display-table-cell">
                                <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="itemsSection">
                </div>
                <div class="itemsFooter">
                    <div class='itemRow newRow'>
                        <div class='seven'><span></span></div>
                        <div class='thirteen'><span></span></div>
                        <div class='thirteen'><span></span></div>
                    </div>
                </div>

            </div>

        </div>
        <!-- recommendedSection -->



    </div>

</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">
    <script src="<%=ResolveClientUrl("/Script/Dropdown.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>

    <script type="text/javascript">

        var counter = 0;

        $(document).ready(function () {
            initializeDatePicker();
            resizeTable();
           appendAttribute.init("StationDD", "Station");

        });

        $(window).resize(function () {
            resizeTable();

        });


        $("#toTxbx").on("keypress", function (e) {

            if (e.key == 13) {
                $("#currentPage").remove();
                $("<input hidden id='currentPage' value='0' />").appendTo("body");
                clearInvoice();
                counter = 0;
                getReport();

            }
        });
        $("#searchBtn").on("click", function (e) {
            counter = 0;
            $("#currentPage").remove();
            $("<input hidden id='currentPage' value='0' />").appendTo("body");
            clearInvoice();
            getReport();

        });

        function getReport() {
            $(".loader").show();
            var stationId = 0;
          
            if ($("#stationCheck").prop("checked")) {
                stationId = $("#StationDD .dd-selected-value").val();
            }
            var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? "0" : $("#currentPage").val();
            $.ajax({
                url: '/Reports/SaleReports/CustomerPhoneNumbers.aspx/getReport',
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ "from": $(".fromTxbx").val(), "to": $(".toTxbx").val(), "stationId" : stationId }),
                success: function (response) {
                    if (response.d.Success) {
                        if (response.d.ListofInvoices.length > 0) {

                            for (var i = 0; i < response.d.ListofInvoices.length; i++) {
                                counter++;
                                $("<div class='itemRow newRow'>" +
                                    "<div class='seven'><span>" + counter + "</span></div>" +
                                    "<div class='thirteen name'> <input value='" + response.d.ListofInvoices[i].PartyName + "' disabled /></div>" +
                                    "<div class='thirteen name'> <input value='" + response.d.ListofInvoices[i].Address + "' disabled /></div>" +
                                    "<div class='thirteen name'> <input value='" + response.d.ListofInvoices[i].MobileNumber + "' disabled /></div>" +
                                    "<div class='thirteen name'> <input value='" + response.d.ListofInvoices[i].PhoneNumber + "' disabled /></div>" +
                                    "</div>").appendTo(".dayCashAndCreditSale .itemsSection");
                            }
                            calculateData();
                            $(".loader").hide();

                        } else if (response.d.ListofInvoices.length == 0 && currentPage == "0") {
                            $(".loader").hide(); swal({
                                title: "No Result Found ", type: 'error',
                                text: "No Result Found!"
                            });
                        }

                    }
                    else {
                        $(".loader").hide();
                        swal({
                            title: "there is some error",
                            text: response.d.Message
                        });
                    }

                },
                error: function (error) {
                    swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                }
            });
        }
        $(document).on('keydown', function (e) {
            if ((e.ctrlKey || e.metaKey) && (e.key == "p" || e.charCode == 16 || e.charCode == 112 || e.keyCode == 80)) { //Ctrl + P
                e.preventDefault();
                printThisTransaction();
            }
        });
        function printThisTransaction() {
            var stationId = 0;
            var stationName = '';
            if ($("#stationCheck").prop("checked")) {
                stationId = $("#StationDD .dd-selected-value").val();
                stationName = $("#StationDD .dd-selected-text").text();
            }
            window.open(
                '/Reports/SaleReports/Prints/CustomerPhoneNumbersCR_A4.aspx?from=' + $(".fromTxbx").val() + '&to=' + $(".toTxbx").val() + '&stationId=' + stationId + '&stationName=' + stationName,
                '_blank'
            );
        }
        $(document).on('click', '#print', function () {
            printThisTransaction();           
        });

        function clearInvoice() {


            $(".itemsSection .itemRow").remove();
        }
        function calculateData() {

            var sum;
            var inputs = ["Quantity", 'Amount', 'ReturnQty', 'ReturnAmount', 'NetQtySold', 'NetAmountSold'];
            for (var i = 0; i < inputs.length; i++) {

                var currentInput = inputs[i];
                sum = 0;
                $("input[name = '" + currentInput + "']").each(function () {

                    if (this.value.trim() === "") {
                        sum = (parseFloat(sum) + parseFloat(0));
                    } else {
                        sum = (parseFloat(sum) + parseFloat(this.value));

                    }

                });

                $("[name=total" + currentInput + "]").val('');
                $("[name=total" + currentInput + "]").val(parseFloat(sum).toFixed(2));


            }

        }
    </script>
</asp:Content>
