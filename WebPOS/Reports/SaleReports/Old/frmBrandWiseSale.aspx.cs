﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
namespace WebPOS
{
    public partial class frmBrandWiseSale : System.Web.UI.Page
    {

        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               // LoadStationList();
                LoadBrandList();
                txtStartDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                txtEndDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
            }
        }

        void LoadStationList1111()
        {

            SqlCommand cmd = new SqlCommand("select Name from Station where CompID='01' order by Name", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);
            lstStation.DataSource = dset;
            lstStation.DataBind();
            lstStation.DataTextField = "Name";
            lstStation.DataBind();

        }
        void LoadBrandList()
        {
             
            SqlCommand cmd = new SqlCommand("select Name from BrandName where CompID='" + CompID + "' order by name", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);
            lstBrand.DataSource = dset;
            lstBrand.DataBind();
            lstBrand.DataTextField = "Name";
            lstBrand.DataBind();
            

        }

        void BrandWiseSale(String BrandName,DateTime StartDate,DateTime EndDate)
        {


            if (chkBrand.Checked==true && chkStation.Checked == false)
            {
                ModItem objModItem = new ModItem();
                int BrandID = objModItem.BrandCodeAgainstBrandName(BrandName);
                //SqlCommand cmd = new SqlCommand("Select   Invoice2.Code , Invoice2.Description as Description,sum(Invoice2.Qty) as Qty, Sum(Invoice2.Amount) as Amount ,Sum(Invoice2.AmouBeforDisc) as AmouBeforDisc,Sum(Invoice2.Item_Discount) as Item_Discount,Sum(Invoice2.DealRs) as DealRs from (Invoice2 inner join invoice1 on invoice2.InvNo=invoice1.InvNo) inner join invcode on invoice2.code=invcode.code  where  Invoice1.CompID='" + CompID + "' and Invoice2.ItemNature=1 and Invoice2.BrandName='" + BrandID + "' and Invoice1.Date1>='" + StartDate + "' and invoice1.Date1<='" + EndDate + "' group by Invoice2.Description , Invoice2.Code   order by Invoice2.Description ", con);
                SqlCommand cmd = new SqlCommand("Select   Invoice2.Code , Invoice2.Description as Description,sum(Invoice2.Qty) as Qty, Sum(Invoice2.Amount) as Amount ,Sum(Invoice2.AmouBeforDisc) as AmouBeforDisc,Sum(Invoice2.Item_Discount) as Item_Discount,Sum(Invoice2.DealRs) as DealRs from (Invoice2 inner join invoice1 on invoice2.InvNo=invoice1.InvNo) inner join invcode on invoice2.code=invcode.code  where  Invoice1.CompID='" + CompID + "' and Invoice2.ItemNature=1 and InvCode.Brand='" + BrandID + "' and Invoice1.Date1>='" + StartDate + "' and invoice1.Date1<='" + EndDate + "' group by Invoice2.Description , Invoice2.Code   order by Invoice2.Description ", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataSet dset = new DataSet();
                adpt.Fill(dset);

                GridView1.DataSource = dset;
                GridView1.DataBind();

            }
            else
                if(chkBrand.Checked==true && chkStation.Checked==true )
                {
                    int StationID = 1;
                    ModViewInventory objModViewInventory = new ModViewInventory();
                    
                    ModItem objModItem = new ModItem();
                    int BrandID = objModItem.BrandCodeAgainstBrandName(BrandName);
                    SqlCommand cmd = new SqlCommand("Select  Invoice2.Code , Invoice2.Description as Description,sum(Invoice2.Qty) as Qty, Sum(Invoice2.Amount) as Amount ,Sum(Invoice2.AmouBeforDisc) as AmouBeforDisc,Sum(Invoice2.Item_Discount) as Item_Discount,Sum(Invoice2.DealRs) as DealRs from (Invoice2 inner join invoice1 on invoice2.InvNo=invoice1.InvNo) inner join invcode on invoice2.code=invcode.code where  Invoice1.CompID='" + CompID + "' and Invoice2.ItemNature=1 and Invoice2.BrandName='" + BrandID + "' and Invoice2.StationID=" + StationID + " and Invoice1.Date1>='" + StartDate + "' and invoice1.Date1<='" + EndDate + "' group by Invoice2.Description , Invoice2.Code  order by Invoice2.Description ", con);
                    SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                    DataSet dset = new DataSet();
                    adpt.Fill(dset);
                    GridView1.DataSource = dset;
                    GridView1.DataBind();

                }
            if (chkBrand.Checked == false && chkStation.Checked == false)
            {
                ModItem objModItem = new ModItem();
                int BrandID = objModItem.BrandCodeAgainstBrandName(BrandName);
                SqlCommand cmd = new SqlCommand("Select  Invoice2.Code , Invoice2.Description as Description,sum(Invoice2.Qty) as Qty, Sum(Invoice2.Amount) as Amount ,Sum(Invoice2.AmouBeforDisc) as AmouBeforDisc,Sum(Invoice2.Item_Discount) as Item_Discount,Sum(Invoice2.DealRs) as DealRs from (Invoice2 inner join invoice1 on invoice2.InvNo=invoice1.InvNo) inner join invcode on invoice2.code=invcode.code  where  Invoice1.CompID='" + CompID + "' and Invoice2.ItemNature=1 and Invoice1.Date1>='" + StartDate + "' and invoice1.Date1<='" + EndDate + "' group by Invoice2.Description , Invoice2.Code  order by Invoice2.Description ", con);
                //SqlCommand cmd = new SqlCommand("Select   Invoice2.Description as Description,sum(Invoice2.Qty) as Qty, Sum(Invoice2.Amount) as Amount ,Sum(Invoice2.AmouBeforDisc) as AmouBeforDisc,Sum(Invoice2.Item_Discount) as Item_Discount,Sum(Invoice2.DealRs) as DealRs from Invoice1 a inner join Invoice2 b on a.InvNo=b.InvNo inner join InvCode c on c.Code=b.Code inner join BrandName d on d.Code=c.Brand  where  Invoice1.CompID='" + CompID + "' and Invoice2.ItemNature=1 and Invoice1.Date1>='" + StartDate + "' and invoice1.Date1<='" + EndDate + "' group by Invoice2.Description", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataSet dset = new DataSet();
                adpt.Fill(dset);

                GridView1.DataSource = dset;
                GridView1.DataBind();

            }
            else
                if (chkBrand.Checked == false && chkStation.Checked == true)
                {
                    int StationID = 1;
                    ModViewInventory objModViewInventory = new ModViewInventory();
                    
                    ModItem objModItem = new ModItem();
                    int BrandID = objModItem.BrandCodeAgainstBrandName(BrandName);
                    SqlCommand cmd = new SqlCommand("Select Invoice2.Code ,  Invoice2.Description as Description,sum(Invoice2.Qty) as Qty, Sum(Invoice2.Amount) as Amount ,Sum(Invoice2.AmouBeforDisc) as AmouBeforDisc,Sum(Invoice2.Item_Discount) as Item_Discount,Sum(Invoice2.DealRs) as DealRs from (Invoice2 inner join invoice1 on invoice2.InvNo=invoice1.InvNo)  inner join invcode on invoice2.code=invcode.code where  Invoice1.CompID='" + CompID + "' and Invoice2.ItemNature=1 and Invoice2.StationID=" + StationID + " and Invoice1.Date1>='" + StartDate + "' and invoice1.Date1<='" + EndDate + "' group by Invoice2.Description ,Invoice2.Code  order by Invoice2.Description ", con);
                    SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                    DataSet dset = new DataSet();
                    adpt.Fill(dset);
                    GridView1.DataSource = dset;
                    GridView1.DataBind();

                }

            

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            BrandWiseSale(lstBrand.SelectedItem.Value ,Convert.ToDateTime( txtStartDate.Text),Convert.ToDateTime( txtEndDate.Text) );
        }

     
        decimal TotAmo = 0;
        decimal TotQty = 0;
        decimal TotItem_Discount = 0;
        decimal TotDealRs = 0;
        decimal AmoAfterDis = 0;
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // add the UnitPrice and QuantityTotal to the running total variables
                TotAmo += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Amount"));
                TotQty += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Qty"));
                TotItem_Discount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Item_Discount"));
                TotDealRs += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "DealRs"));
                AmoAfterDis += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Amount")) - (Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Item_Discount")) + Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "DealRs")));
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";
                // for the Footer, display the running totals
               
                e.Row.Cells[2].Text = TotQty.ToString();
                e.Row.Cells[3].Text = TotAmo.ToString();
                e.Row.Cells[4].Text = TotItem_Discount.ToString();
                e.Row.Cells[5].Text = TotDealRs.ToString();
                e.Row.Cells[6].Text = AmoAfterDis.ToString();

                e.Row.Cells[2].HorizontalAlign =  HorizontalAlign.Right;
                e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[6].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Font.Bold = true;
            }
        }

    }
}