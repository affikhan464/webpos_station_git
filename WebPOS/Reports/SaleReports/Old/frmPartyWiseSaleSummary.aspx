﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="frmPartyWiseSaleSummary.aspx.cs" Inherits="WebPOS.Reports.SaleReports.frmPartyWiseSaleSummary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="<%= ResolveUrl("~/css/style.css") %>" rel="stylesheet" />
    <link href="<%= ResolveUrl("~/css/reset.css") %>" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    
        
        
        <br /><h2 class="heading" >Party Wise Sale Summary</h2><br />
       
        <div style ="width:100%;height:100%;text-align:center; vertical-align:middle">    
        <table border="0" style="width:100%" id="top">
            <tr style="background-color:#f2e9e9;align-items:center">
                <td  >
                    <asp:Label ID="Label1" runat="server" Text="Start Date"></asp:Label>
                    <asp:TextBox ID="StartDate" CssClass="datepic" runat="server"></asp:TextBox>
                    <asp:Label ID="Label2" runat="server" Text="End Date"></asp:Label>
                    <asp:TextBox ID="txtEndDate" CssClass="datepic" runat="server"></asp:TextBox>
                   
                   
                    <asp:Label ID="Label4" runat="server" Text="Select Party"></asp:Label>
                    <asp:DropDownList AutoPostBack="false" ID="lstParty" runat="server">  </asp:DropDownList>
                    <asp:RadioButton ID="rdoAll" runat="server" text="All" GroupName="one" AutoPostBack="True" OnCheckedChanged="rdoAll_CheckedChanged"/><asp:RadioButton ID="rdoClient" runat="server" text="Clients only" GroupName="one" AutoPostBack="True" Checked="True" OnCheckedChanged="rdoVender_CheckedChanged"/>
                    <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
                </td>
            </tr>
        </table>
</div>
        <div style="width:100%">
        
               
        
  

            <br /><br />




        <asp:GridView CssClass="grid" ID="GridView1" AutoGenerateColumns="False" HeaderStyle-BackColor ="WhiteSmoke"   AlternatingRowStyle-BackColor ="WhiteSmoke"
      runat="server" OnRowDataBound="GridView1_RowDataBound" ShowFooter="True"  FooterStyle-BackColor="Wheat" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical">
<AlternatingRowStyle BackColor="#DCDCDC"></AlternatingRowStyle>
      
      <Columns>
          <asp:TemplateField HeaderText="S. #"> 
               <ItemTemplate> 
                    <span style="text-align:left"> 
                        <asp:Label ID="Label1" runat="server"  Text='<%# Convert.ToInt32( Container.DataItemIndex+1) %>'></asp:Label>
                    </span> 
             </ItemTemplate>
         <ItemStyle HorizontalAlign="Center" />
          </asp:TemplateField>

        



        
           <asp:BoundField HeaderText="Item Name" DataField="ItemName" ReadOnly="true"  >  
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
          
        <asp:BoundField HeaderText="Quantity" DataField="QTY" ReadOnly="true"  >  
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
        <asp:BoundField HeaderText="Amount" DataField="Amount" ReadOnly="true"  >  
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
        
           <asp:BoundField HeaderText="Item Discount" DataField="Item_Discount" ReadOnly="true"  >
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
        <asp:BoundField HeaderText="Deal" DataField="DealRs" ReadOnly="true"  >
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>

        
           <asp:TemplateField HeaderText="Net Amount">      
               <ItemTemplate> <span style="text-align:left"> <asp:Label ID="lblNetAmount" runat="server"  Text='<%# Convert.ToDecimal( Eval("Amount"))-(Convert.ToDecimal( Eval("Item_Discount"))+Convert.ToDecimal( Eval("DealRs"))) %>'></asp:Label>
               </span> 
               </ItemTemplate>
               <ItemStyle HorizontalAlign="Right" />
       </asp:TemplateField>
          
          
      </Columns>

<FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>

<HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White"></HeaderStyle>
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#0000A9" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#000065" />
    </asp:GridView>


    </div>



</asp:Content>
