﻿<%@ Page Language="C#" Title="" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="frmDayCashCreditSale.aspx.cs" Inherits="WebPOS.Reports.SaleReports.frmDayCashCreditSale" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="<%= ResolveUrl("~/css/style.css") %>" rel="stylesheet" />
    <link href="<%= ResolveUrl("~/css/reset.css") %>" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">




             <br /> <h2 class="heading" >Day Cash & Credit Sale</h2><br />
       
       <div style ="width:100%;height:100%;text-align:center; vertical-align:middle">
            <table border="0" style="width: 100%">
                <tr style="background-color: #f2e9e9">
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="Start Date"></asp:Label>
                        <asp:TextBox ID="txtStartDate" CssClass="datepic" runat="server"></asp:TextBox>
                        <asp:Label ID="Label3" runat="server" Text="End Date"></asp:Label>
                        <asp:TextBox ID="txtEndDate" CssClass="datepic" runat="server"></asp:TextBox>
                        
                        <asp:Button ID="Button1" runat="server" Text="Report" OnClick="Button1_Click" />
                    </td>
                </tr>
            </table>
           </div>
        <div>
            <br />
            <center>
            <asp:GridView CssClass="grid" ID="GridView1" AutoGenerateColumns="False" HeaderStyle-BackColor="WhiteSmoke" AlternatingRowStyle-BackColor="WhiteSmoke"
                runat="server" OnRowDataBound="GridView1_RowDataBound" ShowFooter="True"  FooterStyle-BackColor="Wheat" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical">
                <AlternatingRowStyle BackColor="#DCDCDC"></AlternatingRowStyle>
                <Columns>
                    <asp:BoundField HeaderText="Date" DataField="Date" DataFormatString="{0:d MMM yyyy}" ReadOnly="true">
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Invoice #" DataField="Invoice" ReadOnly="true">

                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>

                    <asp:BoundField HeaderText="Party Name" DataField="Name" ReadOnly="true" />
                    <asp:BoundField HeaderText="Total" DataField="Total" ReadOnly="true">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Discount" DataField="Discount" ReadOnly="true">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Paid" DataField="Paid" ReadOnly="true">
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Balance">
                        <ItemTemplate>
                            <span style="text-align: left">
                                <asp:Label ID="Label1" runat="server" Text='<%# Convert.ToDecimal( Eval("Total"))-(Convert.ToDecimal( Eval("Discount"))+Convert.ToDecimal( Eval("Paid"))) %>'></asp:Label>
                            </span>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>



                </Columns>

                <FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>

                <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White"></HeaderStyle>
                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#0000A9" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#000065" />
            </asp:GridView>

        </div>
  
  </asp:Content>
