﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebPOS.Reports.ReceivablePayable
{
    public partial class frmAggingReportAllClients : System.Web.UI.Page
    {

        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {
            
                if (!IsPostBack)
                {
                    try{
                    LoadPartyGroup();
                    txtStartDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                      }
                    catch(Exception ex)
                    {

                        Response.Write("error  = " + ex.Message);
                    }
                    
                }
            
          

        }

        void LoadPartyGroup()
        {

            SqlCommand cmd = new SqlCommand("select GroupName from PartyGroup where CompID='01' order by GroupName", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);
            lstPartyGroup.DataSource = dset;
            lstPartyGroup.DataBind();
            lstPartyGroup.DataTextField = "GroupName";
            lstPartyGroup.DataBind();

        }


        void AggingReport(String StationName, DateTime StartDate)
        {
           
            decimal NetSale=0;
            ModSaleReport objModSaleReport=new ModSaleReport();

            ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
            int PartyGroupID = objModPartyCodeAgainstName.PartyGroupIDAgainstGroupName(lstPartyGroup.Text);

            DateTime ActualStartDate = Convert.ToDateTime(txtStartDate.Text).AddDays(-14);
            DateTime ActualEndDate = Convert.ToDateTime(txtStartDate.Text);
            string sql="";

            if(chkStation.Checked==false)
                { sql = "Select Code,Name,Balance,Norbalance from PartyCode where CompID='" + CompID + "' and Balance>0 and NorBalance=1 order by name";}    
            else
                if(chkStation.Checked==true)
                {sql = "Select Code,Name,Balance,Norbalance from PartyCode where CompID='" + CompID + "' and GroupID=" + PartyGroupID + " and Balance>0 and NorBalance=1 order by name";}
                    

            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);

            decimal NetBalance=0;
            decimal RemainingBalance=0;
            string  PartyCode;
            string PartyName;
            int row = 1; 

            int Col=0;
             DataTable dt = new DataTable();
            dt.Columns.Add("SNO");
            dt.Columns.Add("Code");
            dt.Columns.Add("PartyName");
            dt.Columns.Add("1");
            dt.Columns.Add("2");
            dt.Columns.Add("3");
            dt.Columns.Add("4");
            dt.Columns.Add("5");
            dt.Columns.Add("6");
            dt.Columns.Add("7");
            dt.Columns.Add("Balance");

            for (int i = 0; i < dset.Tables[0].Rows.Count; i++)
            {
                 StartDate = ActualStartDate;
                 DateTime EndDate = ActualEndDate;
                 PartyCode = Convert.ToString(dset.Tables[0].Rows[i]["Code"]);
                 PartyName = Convert.ToString(dset.Tables[0].Rows[i]["Name"]);
                 NetBalance = Math.Round( Convert.ToDecimal(dset.Tables[0].Rows[i]["Balance"]),2);
                 RemainingBalance = NetBalance;
                 Col = 3;

                 dt.Rows.Add();
                 dt.Rows[i]["SNO"] = i;
                 dt.Rows[i]["Code"] = PartyCode;
                 dt.Rows[i]["PartyName"] = PartyName;
                 //dt.Rows[i]["1"] = Math.Round( NetBalance,2);
                 dt.Rows[i]["Balance"] = NetBalance;

                
                for(int j=1;j<8;j++)
                 {
                
                      if (RemainingBalance > 0 && j < 7)
                      {
                          NetSale =  objModSaleReport.TotalSaleTo_Party(PartyCode, StartDate, EndDate);
                          if (RemainingBalance > NetSale)
                          {
                              dt.Rows[i][Col] = NetSale;
                              
                          }
                          else
                          {
                              dt.Rows[i][Col] = RemainingBalance;
                              
                          }
                          RemainingBalance = RemainingBalance - NetSale;
                      }
                      else
                          if (RemainingBalance > 0 && j == 7)
                          {
                              dt.Rows[i][Col] = RemainingBalance;
                          }
                          else
                          {
                              dt.Rows[i][Col] = 0;
                          }


                      Col = Col + 1;
                      StartDate = StartDate.AddDays(-15);
                      EndDate = EndDate.AddDays(-15);
                      NetSale = 0;
                 
              
                 }
            }

            GridView1.DataSource = dt;
            GridView1.DataBind();

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
           
            try
            {
                AggingReport(lstPartyGroup.Text, Convert.ToDateTime(txtStartDate.Text));
            }
            catch (Exception ex)
            {
                Response.Write("error  = " + ex.Message);

            }
        }


        decimal Tot1 = 0;
        decimal Tot2 = 0;
        decimal Tot3 = 0;
        decimal Tot4 = 0;
        decimal Tot5 = 0;
        decimal Tot6 = 0;
        decimal Tot7 = 0;
        decimal TotBalance = 0;
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // add the UnitPrice and QuantityTotal to the running total variables
                Tot1 += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "1"));
                Tot2 += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "2"));
                Tot3 += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "3"));
                Tot4 += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "4"));
                Tot5 += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "5"));
                Tot6 += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "6"));
                Tot7 += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "7"));
                TotBalance += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Balance"));
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";
                //// for the Footer, display the running totals
                e.Row.Cells[3].Text = Tot1.ToString();
                e.Row.Cells[4].Text = Tot2.ToString();
                e.Row.Cells[5].Text = Tot3.ToString();
                e.Row.Cells[6].Text = Tot4.ToString();
                e.Row.Cells[7].Text = Tot5.ToString();
                e.Row.Cells[8].Text = Tot6.ToString();
                e.Row.Cells[9].Text = Tot7.ToString();
                e.Row.Cells[10].Text = TotBalance.ToString();
                
                e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[6].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[7].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[8].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[9].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[10].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Font.Bold = true;
            }
        }

    }
}