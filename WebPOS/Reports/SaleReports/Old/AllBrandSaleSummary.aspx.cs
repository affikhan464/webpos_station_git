﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;


namespace WebPOS.Reports.SaleReports
{
    public partial class AllBrandSaleSummary : System.Web.UI.Page
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {


            if (!IsPostBack)
            {
               
                txtStartDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                txtEndDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
            }

        }

       

        
        
        void AllBrandWiseSaleSummary(DateTime StartDate,DateTime EndDate)
        {

           
            ModItem objModItem = new ModItem();
           
            SqlCommand cmd = new SqlCommand("Select   BrandName.Name,sum(Invoice2.Qty) as Qty, Sum(Invoice2.Amount) as Amount ,Sum(Invoice2.AmouBeforDisc) as AmouBeforDisc,Sum(Invoice2.Item_Discount) as Item_Discount,Sum(Invoice2.DealRs) as DealRs from (Invoice2 inner join invoice1 on invoice2.InvNo=invoice1.InvNo) inner join BrandName on Invoice2.BrandName=BrandName.Code where  Invoice1.CompID='" + CompID + "' and Invoice2.ItemNature=1 and Invoice1.Date1>='" + StartDate + "' and invoice1.Date1<='" + EndDate + "' group by BrandName.Name order by BrandName.Name", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataSet dset = new DataSet();
                adpt.Fill(dset);

                GridView1.DataSource = dset;
                GridView1.DataBind();
            
            

        }






        protected void Button1_Click(object sender, EventArgs e)
        {

            try
            {
                AllBrandWiseSaleSummary(Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
            }
            catch (Exception ex)
            {
                Response.Write("error  = " + ex.Message);

            }

               
            
        }

     
        decimal TotAmo = 0;
        decimal TotQty = 0;
        decimal TotItem_Discount = 0;
        decimal TotDealRs = 0;
        decimal AmoAfterDis = 0;
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // add the UnitPrice and QuantityTotal to the running total variables
                TotAmo += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Amount"));
                TotQty += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Qty"));
                TotItem_Discount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Item_Discount"));
                TotDealRs += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "DealRs"));
                AmoAfterDis += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Amount")) - (Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Item_Discount")) + Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "DealRs")));
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";
                // for the Footer, display the running totals
                e.Row.Cells[2].Text = TotAmo.ToString();
                e.Row.Cells[1].Text = TotQty.ToString();
                e.Row.Cells[5].Text = AmoAfterDis.ToString();
                e.Row.Cells[3].Text = TotItem_Discount.ToString();
                e.Row.Cells[4].Text = TotDealRs.ToString();
                
                e.Row.Cells[1].HorizontalAlign =  HorizontalAlign.Right;
                e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Font.Bold = true;
            }
        }

    
    }
}