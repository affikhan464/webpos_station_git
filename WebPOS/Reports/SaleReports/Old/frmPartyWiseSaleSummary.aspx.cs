﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebPOS.Reports.SaleReports
{
    public partial class frmPartyWiseSaleSummary : System.Web.UI.Page
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                try
                {
                   
                    LoadPartyList();
                    StartDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                    txtEndDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                }
                catch (Exception ex)
                {
                    Response.Write("error  = " + ex.Message);

                }

            }


        }




       
        void LoadPartyList()
        {

            if (rdoClient.Checked == true)
            {
                SqlCommand cmd = new SqlCommand("select Name from PartyCode where NorBalance=1 and CompID='01' order by Name", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataSet dset = new DataSet();
                adpt.Fill(dset);
                lstParty.DataSource = dset;
            }
            else
                if (rdoAll.Checked == true)
                {
                    SqlCommand cmd = new SqlCommand("select Name from PartyCode where CompID='01' order by Name", con);
                    SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                    DataSet dset = new DataSet();
                    adpt.Fill(dset);
                    lstParty.DataSource = dset;
                }
                else
                {
                    SqlCommand cmd = new SqlCommand("select Name from PartyCode where CompID='01' order by Name", con);
                    SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                    DataSet dset = new DataSet();
                    adpt.Fill(dset);
                    lstParty.DataSource = dset;
                }



            lstParty.DataBind();
            lstParty.DataTextField = "Name";
            lstParty.DataBind();

        }
        void LoadReport(DateTime StartDate, DateTime EndDate)
        {


            
            ModPartyCodeAgainstName objModPartyCodeAgainstName = new ModPartyCodeAgainstName();
            string PartyCode = objModPartyCodeAgainstName.PartyCode(lstParty.Text);


            //SqlCommand cmd = new SqlCommand("Select InvNo,date1,VenderCode,Cost,QTY from Purchase2 where  CompID='" + CompID + "' and Date1>='" + StartDate + "' and Date1<='" + EndDate + "' and Code='" + ItemCode + "' order by  System_Date_Time", con);

            //SqlCommand cmd = new SqlCommand("Select Invoice2.Description as ItemName,  sum(Invoice2.QTY) as QTY, sum(Invoice2.Amount) as Amount , sum(Invoice2.Item_Discount) as Item_Discount, isnull(sum(Invoice2.DealRs),0) as DealRs from (Invoice2 inner join invcode on Invoice2.code=invcode.Code) where Invcode.Brand='" + BrandID + "' and Invoice1.PartyCode='" + PartyCode + "' and  Purchase2.date1>='" + StartDate + "' and Purchase2.date1<='" + EndDate + "' group by Purchase2.Description", con);
            //SqlCommand cmd = new SqlCommand("Select Invoice2.Description as ItemName,  sum(Invoice2.QTY) as QTY, sum(Invoice2.Amount) as Amount , sum(Invoice2.Item_Discount) as Item_Discount, isnull(sum(Invoice2.DealRs),0) as DealRs from (Invoice2 inner join invcode on Invoice2.code=invcode.Code) where Invcode.Brand='" + BrandID + "' and Invoice1.PartyCode='" + PartyCode + "' and  Purchase2.date1>='" + StartDate + "' and Purchase2.date1<='" + EndDate + "' group by Purchase2.Description", con);
            SqlCommand cmd = new SqlCommand("Select Invoice2.Description as ItemName,  sum(Invoice2.QTY) as QTY, sum(Invoice2.Amount) as Amount , sum(Invoice2.Item_Discount) as Item_Discount, isnull(sum(Invoice2.DealRs),0) as DealRs from (Invoice2 inner join invoice1 on Invoice2.InvNo=Invoice1.InvNo) where Invoice1.PartyCode='" + PartyCode + "' and  Invoice1.date1>='" + StartDate + "' and Invoice1.date1<='" + EndDate + "' group by Invoice2.Description", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);

            GridView1.DataSource = dset;
            GridView1.DataBind();


        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                LoadReport(Convert.ToDateTime(StartDate.Text), Convert.ToDateTime(txtEndDate.Text));
            }
            catch (Exception ex)
            {
                Response.Write("error  = " + ex.Message);

            }


        }

        decimal TotQty = 0;
        decimal TotAmount = 0;
        decimal TotItem_Discount = 0;
        decimal TotDealRs = 0;
        decimal TotNetAcount = 0;

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // add the UnitPrice and QuantityTotal to the running total variables                
                TotQty += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Qty"));
                TotAmount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Amount"));
                TotItem_Discount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Item_Discount"));
                TotDealRs += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "DealRs"));
                Label lblAmount = (Label)e.Row.FindControl("lblNetAmount");
                TotNetAcount += Convert.ToDecimal(lblAmount.Text);
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";
                // for the Footer, display the running totals
                e.Row.Cells[2].Text = TotQty.ToString();
                e.Row.Cells[3].Text = TotAmount.ToString();

                e.Row.Cells[4].Text = TotItem_Discount.ToString();
                e.Row.Cells[5].Text = TotDealRs.ToString();
                e.Row.Cells[6].Text = TotNetAcount.ToString();

                e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[6].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Font.Bold = true;
            }
        }

        protected void rdoAll_CheckedChanged(object sender, EventArgs e)
        {
            LoadPartyList();
        }

        protected void rdoVender_CheckedChanged(object sender, EventArgs e)
        {
            LoadPartyList();
        }



    }
}