﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebPOS.Reports.SaleReports
{
    public partial class frmDayCashCreditSale : System.Web.UI.Page
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                   
                    txtStartDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                    txtEndDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                }
                catch (Exception ex)
                {
                    Response.Write("error  = " + ex.Message);

                }

            }
        }

       
       

        void DayCashCreditSale( DateTime StartDate, DateTime EndDate)
        {



           
                SqlCommand cmd = new SqlCommand("Select Invoice1.Date1 as [Date],Invoice1.InvNo as Invoice , Invoice1.Name as Name,Invoice3.Total as Total,Invoice3.Discount1 as Discount,Invoice3.CashPaid as Paid,Invoice3.Total-(Invoice3.Discount1+Invoice3.CashPaid) as Balance from (Invoice1 inner join invoice3 on invoice1.InvNo=invoice3.InvNo) where Invoice1.CompID='" + CompID + "' and  Invoice1.Date1>='" + StartDate + "' and Invoice1.Date1<='" + EndDate + "' order by Invoice1.InvNo", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataSet dset = new DataSet();
                adpt.Fill(dset);

                GridView1.DataSource = dset;
                GridView1.DataBind();
            
        }

       



        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                DayCashCreditSale(Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
            }
            catch (Exception ex)
            {
                Response.Write("error  = " + ex.Message);

            }
            
        }


        decimal TotTotal = 0;
        decimal TotDiscount = 0;
        decimal TotPaid = 0;
        decimal TotBalance = 0;
        
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // add the UnitPrice and QuantityTotal to the running total variables
                TotTotal += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Total"));
                TotDiscount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Discount"));
                TotPaid += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Paid"));
                TotBalance += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Balance"));
                
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";
                // for the Footer, display the running totals
                e.Row.Cells[3].Text = TotTotal.ToString();
                e.Row.Cells[4].Text = TotDiscount.ToString();
                e.Row.Cells[5].Text = TotPaid.ToString();
                e.Row.Cells[6].Text = TotBalance.ToString();
                

                e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[6].HorizontalAlign = HorizontalAlign.Right;

                e.Row.Font.Bold = true;
            }
        }
    
    
    }
}