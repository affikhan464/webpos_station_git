﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="frmAggingReportAllClients.aspx.cs" Inherits="WebPOS.Reports.Sale_Return.frmAggingReportAllClients" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="<%= ResolveUrl("~/css/style.css") %>" rel="stylesheet" />
    <link href="<%= ResolveUrl("~/css/reset.css") %>" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

   
           <br /> <h2 class="heading" >Agging Report</h2><br />
   
<div style ="width:100%;height:100%;text-align:center; vertical-align:middle">

    <table  border="0" style="width:100%">
        <tr  style="background-color:#f2e9e9"  >
            <td >
                <asp:Label ID="Label2" runat="server" Text="Start Date"></asp:Label>
                <asp:TextBox ID="txtStartDate" CssClass="datepic" runat="server"></asp:TextBox>
               
                <asp:CheckBox ID="chkPartyGroup" runat="server" text="Party Group"/>
                <asp:DropDownList ID="lstPartyGroup" runat="server"></asp:DropDownList>
                <asp:Button ID="Button1" runat="server" Text="Report" OnClick="Button1_Click"  />
         </td>  
     </tr>
    </table>
<br />

        <asp:GridView width="100%"  ID="GridView1" AutoGenerateColumns="False" HeaderStyle-BackColor ="WhiteSmoke"   AlternatingRowStyle-BackColor ="WhiteSmoke"
      runat="server" OnRowDataBound="GridView1_RowDataBound" ShowFooter="True"  FooterStyle-BackColor="Wheat" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical">
<AlternatingRowStyle BackColor="#DCDCDC"></AlternatingRowStyle>
      <Columns>
        
          <asp:TemplateField HeaderText="S. #"> 
               <ItemTemplate> <span style="text-align:left"> <asp:Label ID="Label1" runat="server"  Text='<%# Convert.ToInt32( Container.DataItemIndex+1) %>'></asp:Label>
               </span> 
          </ItemTemplate>
               <ItemStyle HorizontalAlign="Center" />
          </asp:TemplateField>

          <asp:BoundField HeaderText="Code" DataField="Code" ReadOnly="true"  >
          <ItemStyle HorizontalAlign="Center" />
          </asp:BoundField>

          <asp:BoundField HeaderText="Party Name" DataField="PartyName" ReadOnly="true" >
          <ItemStyle HorizontalAlign="left" />
          </asp:BoundField>

          <asp:BoundField HeaderText="1 to 15" DataField="1" ReadOnly="true" >
          <ItemStyle HorizontalAlign="Right" />
          </asp:BoundField>
       
          <asp:BoundField HeaderText="16 to 30" DataField="2" ReadOnly="true" >
          <ItemStyle HorizontalAlign="Right" />
          </asp:BoundField>
       
          <asp:BoundField HeaderText="31 to 45" DataField="3" ReadOnly="true" >
          <ItemStyle HorizontalAlign="Right" />
          </asp:BoundField>

          <asp:BoundField HeaderText="46 to 60" DataField="4" ReadOnly="true" >
          <ItemStyle HorizontalAlign="Right" />
          </asp:BoundField>

          <asp:BoundField HeaderText="61 to 75" DataField="5" ReadOnly="true" >
          <ItemStyle HorizontalAlign="Right" />
          </asp:BoundField>

          <asp:BoundField HeaderText="76 to 90" DataField="6" ReadOnly="true" >
          <ItemStyle HorizontalAlign="Right" />
          </asp:BoundField>

          <asp:BoundField HeaderText="90++" DataField="7" ReadOnly="true" >
          <ItemStyle HorizontalAlign="Right" />
          </asp:BoundField>

          <asp:BoundField HeaderText="Net Balance" DataField="Balance" ReadOnly="true" >
          <ItemStyle HorizontalAlign="Right" />
          </asp:BoundField>

      </Columns>

<FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>

<HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White"></HeaderStyle>
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#0000A9" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#000065" />
    </asp:GridView>

    </div>
</asp:Content>
