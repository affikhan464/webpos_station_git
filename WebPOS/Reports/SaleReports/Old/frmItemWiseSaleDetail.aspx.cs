﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebPOS.Reports.SaleReports
{
    public partial class frmItemWiseSaleDetail : System.Web.UI.Page
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                

                txtStartDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                txtEndDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                LoadItemList();
            }

           
           
        }


        void LoadItemList()
        {
            if (chkOnlySoldItem.Checked == false)
            {
                SqlCommand cmd = new SqlCommand("select Description from InvCode where CompID='" + CompID + "' order by Description", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataSet dset = new DataSet();
                adpt.Fill(dset);
                lstItemName.DataSource = dset;
                lstItemName.DataBind();
                lstItemName.DataTextField = "Description";
                lstItemName.DataBind();
            }
            else
                if(chkOnlySoldItem.Checked==true)
                {
                    SqlCommand cmd = new SqlCommand("select Invoice2.Description as Description from (invoice1 inner join invoice2 on invoice1.invno=invoice2.invno) where Invoice1.CompID='" + CompID + "' and Invoice1.Date1>='" + Convert.ToDateTime(txtStartDate.Text) + "' and Invoice1.Date1<='" + Convert.ToDateTime(txtEndDate.Text) + "' order by Invoice1.Description", con);
                    SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                    DataSet dset = new DataSet();
                    adpt.Fill(dset);
                    lstItemName.DataSource = dset;
                    lstItemName.DataBind();
                    lstItemName.DataTextField = "Description";
                    lstItemName.DataBind();
 
                }
        }

        void ItemWiseSaleDetail(DateTime StartDate, DateTime EndDate,string ItemName)
        {


            ModItem objModItem =new ModItem();
            decimal ItemCode=objModItem.ItemCodeAgainstItemName(ItemName);



            SqlCommand cmd = new SqlCommand("SELECT  Invoice1.Date1 as Date1,Invoice1.InvNo as InvNo,Invoice1.Name as Name, Invoice2.Code as Code , Invoice2.Description as Description , Invoice2.Qty as Qty1 ,Invoice2.Amount as Amount , Invoice2.DealRs as DealRs , Invoice2.Item_Discount as Item_Discount1 FROM (Invoice1 INNER JOIN Invoice2 ON Invoice1.InvNo = Invoice2.InvNo) where Invoice1.CompID='" + CompID + "' and   Invoice2.Code=" + ItemCode + " and Invoice2.ItemNature = 1 and Invoice1.Date1>='" + StartDate + "' and Invoice1.Date1<='" + EndDate + "'  order by Invoice2.Description", con);


            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);

            GridView1.DataSource = dset;
            GridView1.DataBind();


        }






        protected void Button1_Click(object sender, EventArgs e)
        {

            ItemWiseSaleDetail(Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text),Convert.ToString(lstItemName.Text) );
        }


        decimal TotQty = 0;
        decimal TotItem_Discount = 0;
        decimal TotDealRs = 0;
        decimal TotAmount = 0;
        decimal TotNetAmount = 0;
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // add the UnitPrice and QuantityTotal to the running total variables
                TotQty += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Qty1"));
                TotAmount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Amount"));
                TotItem_Discount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Item_Discount1"));
                TotDealRs += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "DealRs"));

                TotNetAmount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Amount")) - (Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Item_Discount1")) + Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "DealRs")));
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";
                // for the Footer, display the running totals
                e.Row.Cells[4].Text = TotQty.ToString();
                e.Row.Cells[5].Text = TotAmount.ToString();
                e.Row.Cells[6].Text = TotItem_Discount.ToString();
                e.Row.Cells[7].Text = TotDealRs.ToString();
                e.Row.Cells[8].Text = TotNetAmount.ToString();


                e.Row.Cells[4].HorizontalAlign = e.Row.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[5].HorizontalAlign = e.Row.Cells[5].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[6].HorizontalAlign = e.Row.Cells[6].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[7].HorizontalAlign = e.Row.Cells[7].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[8].HorizontalAlign = e.Row.Cells[8].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Font.Bold = true;
            }
        }

        protected void chkOnlySoldItem_CheckedChanged(object sender, EventArgs e)
        {
            LoadItemList();
        }
       

       
    }
}