﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class CategoryWiseSaleSummary : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string catCode)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(catCode, fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }


        public static List<InvoiceViewModel> GetTheReport(String catCode, DateTime StartDate, DateTime EndDate)
        {

            var model = new List<InvoiceViewModel>();


            ModItem objModItem = new ModItem();
            int CatID = Convert.ToInt32(catCode);
            SqlCommand cmd = new SqlCommand(@"SELECT
              
               
               
                Sum(invoice2.Amount) as Amount,
				      
                        (ISNULL((Select Sum(SaleReturn2.Amount)
                        from SaleReturn2
                            inner join SaleReturn1 on SaleReturn2.InvNo=SaleReturn1.InvNo 
                        inner join InvCode as item on SaleReturn2.Code=item.Code 
                        where item.Category='" + CatID + @"'
                        and item.Code=Invoice2.Code
                        and SaleReturn1.Date1>='" + StartDate + @"' 
						and SaleReturn1.Date1<='" + EndDate + @"'
                        group by item.Code),0))
                as ReturnAmount,
                       
                        (ISNULL((Select Sum(SaleReturn2.Qty) 
                        from SaleReturn2 
                            inner join SaleReturn1 on SaleReturn2.InvNo=SaleReturn1.InvNo 
                        inner join InvCode as item on SaleReturn2.Code=item.Code 
                        where item.Category='" + CatID + @"' 
                        and SaleReturn1.Date1>='" + StartDate + @"' 
					    and SaleReturn1.Date1<='" + EndDate + @"'  
                        and item.Code=Invoice2.Code
                        group by item.Code),0))
                as ReturnQty, 

                Sum(invoice2.Qty) as Qty, 
                Inventory.Description 
                FROM Invoice1         
                INNER JOIN invoice2 as invoice2 ON Invoice1.InvNo = invoice2.InvNo  
                inner join InvCode as Inventory on invoice2.Code=Inventory.Code                
                inner join Category on Inventory.Category=Category.id 
   
                where Invoice1.CompID = '" + CompID + @"' 
                and Category.id='" + CatID + @"' 
                and Invoice2.ItemNature = 1  
                and Invoice1.Date1 >= '" + StartDate + @"'  
                and Invoice1.Date1 <= '" + EndDate + @"'   
                GROUP BY  Invoice2.Code,Inventory.Description ", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            var dt = new DataTable();
            adpt.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                var Quantity = Convert.ToInt32(dt.Rows[i]["Qty"]);
                var ReturnQty = Convert.ToInt32(dt.Rows[i]["ReturnQty"]);
                var ReturnAmount = Convert.ToInt32(dt.Rows[i]["ReturnAmount"]);
                var Amount = Convert.ToInt32(dt.Rows[i]["Amount"]);

                invoice.BrandName = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                invoice.Quantity = Quantity.ToString();
                invoice.ReturnQty = ReturnQty.ToString();
                invoice.ReturnAmount = ReturnAmount.ToString();
                invoice.Amount = Amount.ToString();
                invoice.NetQtySold = (Quantity - ReturnQty).ToString();
                invoice.NetAmountSold = (Amount - ReturnAmount).ToString();

                model.Add(invoice);
            }





            return model;
        }

        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            var brandCode = "";// brandCodeTxbx.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/BrandWiseSaleSummaryCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&brandCode=" + brandCode);
        }
    }
}