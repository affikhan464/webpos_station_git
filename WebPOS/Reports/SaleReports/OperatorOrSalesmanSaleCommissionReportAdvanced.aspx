﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="OperatorOrSalesmanSaleCommissionReportAdvanced.aspx.cs" Inherits="WebPOS.Reports.SaleReports.OperatorOrSalesmanSaleCommissionReportAdvanced" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



    <div class="searchAppSection reports">
        <div class="contentContainer">
            <h2>Operator Or Salesman Wise Sale Report</h2>
            <div class="BMSearchWrapper row">
                <div class="col col-12 col-sm-6 col-md-3">
                    <span class="userlLabel">From</span>
                    <asp:TextBox ID="fromTxbx" CssClass="datetimepicker fromTxbx Date" runat="server"></asp:TextBox>
                    <%--<input id="fromTxbx" class=" datetimepicker fromTxbx"  name="Date" type="text" />--%>
                </div>

                <div class="col col-12 col-sm-6 col-md-3">
                    <span class="userlLabel">To</span>
                    <asp:TextBox ID="toTxbx" CssClass="datetimepicker toTxbx Date" runat="server"></asp:TextBox>

                    <%--<input id="toTxbx" class="datetimepicker toTxbx" type="text"  name="Date"/>--%>
                </div>
                <div class="col col-12 col-sm-6 col-md-2">

                    <select id="salesManOrOperatorDD" class="dropdown ">
                        <option value="" class="selectCl" onclick="manInvSelection(this)">Select Sales Man</option>

                    </select>

                </div>
                <div class="col col-12 col-sm-6 col-md-2">
                    <a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-list-ul"></i>Get Report</a>
                </div>

                <div class="col col-12 col-sm-6 col-md-2">
                    <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>Print</a>
                    <%--<asp:Button ID="Button2" Cssclass="btn btn-bm"  runat="server" Text="Print" />--%>
                </div>



            </div>
            <!-- searchAppSection -->
        </div>
    </div>
    <!-- featuredSection -->

    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">

        <div class="contentContainer">

            <div class="DaySaleGroup2 reports recommendedSection allItemsView">
                <div class="itemsHeader">
                    <div class="two phCol ">
                        <a href="javascript:void(0)">Sr. No.
                        </a>
                    </div>
                    <div class="two phCol ">
                        <a href="javascript:void(0)">Inv. No.
                        </a>
                    </div>
                    <div class="seven phCol">
                        <a href="javascript:void(0)">Date
                        </a>
                    </div>
                    <div class="eight flex-5 phCol ">
                        <a href="javascript:void(0)">Item Name
                        </a>
                    </div>
                    <div class="five phCol">
                        <a href="javascript:void(0)">Qty
                        </a>
                    </div>
                    <div class="five phCol">
                        <a href="javascript:void(0)">Amnt
                        </a>
                    </div>
                    <div class="eight flex-3 phCol">
                        <a href="javascript:void(0)">Unit / Total Sell. Price
                        </a>
                    </div>

                    <div class="five phCol">
                        <a href="javascript:void(0)">Profit/Loss
											
                        </a>
                    </div>
                    <div class="five phCol">
                        <a href="javascript:void(0)">10% Comm.
                        </a>
                    </div>
                    <div class="eight flex-3 phCol">
                        <a href="javascript:void(0)">Per Piece Comm.
                        </a>
                    </div>
                    <div class="five phCol">
                        <a href="javascript:void(0)">Net Comm.
                        </a>
                    </div>


                </div>
                <!-- itemsHeader -->
                <div class="loader">
                    <div class="display-table">
                        <div class="display-table-row">
                            <div class="display-table-cell">
                                <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="itemsSection">
                </div>
                <div class="itemsFooter">
                    <div class='itemRow newRow'>
                        <div class='two'><span></span></div>
                        <div class='two'><span></span></div>
                        <div class='seven'><span></span></div>
                        <div class='eight flex-5'><span></span></div>
                        <div class='five'>
                            <input disabled="disabled" name='totalQty' />
                        </div>
                        <div class='five'>
                            <input disabled="disabled" name='totalAmount' />
                        </div>
                        <div class='eight flex-3'>
                            <input disabled="disabled" name='totalCost' />
                        </div>
                        <div class='five'>
                            <input disabled="disabled" name='totalProfitLoss' />
                        </div>
                        <div class='five'>
                            <input disabled="disabled" name='totalTenComm' />
                        </div>
                        <div class='eight flex-3'>
                            <input disabled="disabled" name='totalComm' />
                        </div>
                        <div class='five'>
                            <input disabled="disabled" name='totalNetComm' />
                        </div>
                    </div>
                </div>

            </div>

        </div>
        <!-- recommendedSection -->



    </div>

</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">

    <script src="<%=ResolveClientUrl("/Script/Vendor/ddslick.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="<%=ResolveClientUrl("/Script/Dropdown.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script type="text/javascript">

    var counter = 0;

    $(document).ready(function () {
        initializeDatePicker();
        resizeTable();
        appendAttribute.init("salesManOrOperatorDD", "SaleManList");

    });

    $(window).resize(function () {
        resizeTable();

    });
        $(document).on('keydown', function (e) {
            if ((e.ctrlKey || e.metaKey) && (e.key == "p" || e.charCode == 16 || e.charCode == 112 || e.keyCode == 80)) { //Ctrl + P
                e.preventDefault();
                printThisTransaction();
            }
        });
        function printThisTransaction() {
            window.open(
                '/Reports/SaleReports/Prints/OperatorOrSalesmanSaleCommissionReportAdvanced_Print.aspx?from=' + $(".fromTxbx").val() + '&to=' + $(".toTxbx").val() + '&salesmanCode=' + $("#salesManOrOperatorDD .dd-selected-value").val() + '&salesmanName=' + $("#salesManOrOperatorDD .dd-selected-text").text(),
                '_blank'
            );
        }
        $(document).on('click', '#print', function () {
            printThisTransaction();
        });

    $("#toTxbx").on("keypress", function (e) {
        if (e.key == 13) {
            $("#currentPage").remove();
            $("<input hidden id='currentPage' value='0' />").appendTo("body");
            clearInvoice();
            counter = 0;
            getReport();

        }
    });
    $("#searchBtn").on("click", function (e) {
        counter = 0;
        $("#currentPage").remove();
        $("<input hidden id='currentPage' value='0' />").appendTo("body");
        clearInvoice();
        getReport();

    });
    function getReport() {
        $(".loader").show();

        var currentPage = $("#currentPage").val() == "" || $("#currentPage").val() == undefined ? "0" : $("#currentPage").val();
        $.ajax({
            url: '/Reports/SaleReports/OperatorOrSalesmanSaleCommissionReportAdvanced.aspx/getReport',
            type: "POST",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: JSON.stringify({ "from": $(".fromTxbx").val(), "to": $(".toTxbx").val(), "salesmanCode": $("#salesManOrOperatorDD .dd-selected-value").val() }),
            success: function (response) {


                if (response.d.Success) {
                    if (response.d.ListofInvoices.length > 0) {


                        for (var i = 0; i < response.d.ListofInvoices.length; i++) {
                            counter++;
                            var returnedClass = '';
                            var returnedInvNo = '';
                            var returnDate = '';
                            var saleInvNo = '';
                            var InvNo = InvNo = `<a class="text-dark text-underline" target="_blank" 
                                                    href="/correction/SaleEditNew.aspx?InvNo=${response.d.ListofInvoices[i].SaleInvNo}">${response.d.ListofInvoices[i].SaleInvNo}</a>`;
                            if (response.d.ListofInvoices[i].IsReturnedItem && !response.d.ListofInvoices[i].IsUnassignedReturnedItem) {
                                returnedClass = 'returned';
                                returnedInvNo = `<label class="text-danger font-weight-bold d-block">--(Sale Return InvNo=<a class="text-dark text-underline" target="_blank" 
                                                    href="/correction/SaleReturnEditNew.aspx?InvNo=${response.d.ListofInvoices[i].SaleReturnInvNo}">${response.d.ListofInvoices[i].SaleReturnInvNo}</a>)</label>`;
                            
                                saleInvNo = `<label class="text-danger font-weight-bold d-block">--(RETURNED)</label><labe  class="text-success font-weight-bold d-block">--(Sale InvNo=<a class="text-dark text-underline" target="_blank" 
                                                    href="/Correction/SaleEditNew.aspx?InvNo=${response.d.ListofInvoices[i].SaleInvNo}">${response.d.ListofInvoices[i].SaleInvNo}</a>)</label>`;
                                returnDate = `<label class="text-danger font-weight-bold d-block">(Returned Date)</label>`;

                                InvNo = `<a class="text-dark text-underline" target="_blank" 
                                                    href="/correction/SaleReturnEditNew.aspx?InvNo=${response.d.ListofInvoices[i].SaleReturnInvNo}">${response.d.ListofInvoices[i].SaleReturnInvNo}</a>`;
                            }
                            if (response.d.ListofInvoices[i].IsUnassignedReturnedItem) {
                                returnedClass = 'UnassignedReturned';
                                returnedInvNo = `<label class="text-danger font-weight-bold d-block">--(Sale Return InvNo=<a class="text-dark text-underline" target="_blank" 
                                                    href="/correction/SaleReturnEditNew.aspx?InvNo=${response.d.ListofInvoices[i].SaleReturnInvNo}">${response.d.ListofInvoices[i].SaleReturnInvNo}</a>)</label>`;
                            
                                saleInvNo = `<label class="text-danger font-weight-bold d-block">--(RETURNED)</label><label class="text-danger font-weight-bold">-- (Please Correct Sales Man)</label>`;
                                returnDate = `<label class="text-danger font-weight-bold d-block">(Returned Date)</label>`;
                                InvNo = `<a class="text-dark text-underline" target="_blank" 
                                                    href="/correction/SaleReturnEditNew.aspx?InvNo=${response.d.ListofInvoices[i].SaleReturnInvNo}">${response.d.ListofInvoices[i].SaleReturnInvNo}</a>`;
                            }
                            $("<div class='itemRow newRow "+returnedClass+"'>" +
                                "<div class='two'><span>" + counter + "</span></div>" +
                                "<div class='two'><span>" + InvNo + "</span></div>" +
                                "<div class='seven'><span>" + returnDate + response.d.ListofInvoices[i].Date + "</span></div>" +
                                "<div class='eight flex-5'><span>" + response.d.ListofInvoices[i].Description + saleInvNo + returnedInvNo + "</span></div>" +
                                "<div class='five'> <input name='Qty'  value='" + Number(response.d.ListofInvoices[i].Quantity).toFixed(2) + "' disabled /></div>" +
                                "<div class='five'> <input name='Amount'  value='" + Number(response.d.ListofInvoices[i].Amount).toFixed(2) + "' disabled /></div>" +
                                "<div class='eight flex-3'> <input name='Cost'  value='" + Number(response.d.ListofInvoices[i].TotalCost).toFixed(2) + "' hidden /><span class='text-right'> " + response.d.ListofInvoices[i].Cost + "</span></div>" +
                                "<div class='five'> <input name='ProfitLoss'  value='" + Number(response.d.ListofInvoices[i].Profit).toFixed(2) + "' disabled /></div>" +
                                "<div class='five'> <input name='TenComm'  value='" + Number(response.d.ListofInvoices[i].TenCommission).toFixed(2) + "' disabled /></div>" +
                                "<div class='eight flex-3'> <input name='Comm'  value='" + Number(response.d.ListofInvoices[i].TotalPerPieceCommission).toFixed(2) + "' hidden /><span class='text-right'> " + response.d.ListofInvoices[i].PerPieceCommission + "</span></div>" +
                                "<div class='five'> <input name='NetComm'  value='" + Number(response.d.ListofInvoices[i].NetCommission).toFixed(2) + "' disabled /></div>" +
                                "</div>").appendTo(".dayCashAndCreditSale .itemsSection");


                        } $(".loader").hide();
                        calculateData();


                    } else if (response.d.ListofInvoices.length == 0 && currentPage == "0") {
                        $(".loader").hide();
                        swal({
                            title: "No Result Found ", type: 'error',
                            text: "No Result Found!"
                        });
                    }

                }
                else {
                    $(".loader").hide();
                    swal({
                        title: "there is some error",
                        text: response.d.Message
                    });
                }

            },
            error: function (error) {
                swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

            }
        });
    }
    function clearInvoice() {


        $(".itemsSection .itemRow").remove();
    }
    function calculateData() {

        var sum;
        var inputs = ["Qty",  'Amount', 'Cost', 'ProfitLoss', 'TenComm', 'Comm', 'NetComm'];
        for (var i = 0; i < inputs.length; i++) {

            var currentInput = inputs[i];
            sum = 0;
            $("input[name = '" + currentInput + "']").each(function () {

                if (this.value.trim() === "") {
                    sum = (Number(sum) + Number(0));
                } else {
                    sum = (Number(sum) + Number(this.value));

                }

            });

            $("[name=total" + currentInput + "]").val('');
            $("[name=total" + currentInput + "]").val(Number(sum).toFixed(2));


        }

    }
    </script>
</asp:Content>
