﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class DaySaleGroupByItem2 : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);

        public static object SELECT { get; private set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices=model};

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false,Message=ex.Message};
            }
        }

        public static  List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate)
        {
            
            SqlCommand cmd = new SqlCommand(@"SELECT
                Invoice2.Code as Code , 
                Invoice2.Description as Description , 
                Sum(Invoice2.Qty) as Qty1 ,
                Sum(Invoice2.Amount) as Amount,

				Isnull((Select Sum(SaleReturn2.Qty) from SaleReturn2
				where  Date1>='" + StartDate + "'  and Date1 <= '" + EndDate + "' and SaleReturn2.Code=Invoice2.Code and SaleReturn2.Description=Invoice2.Description                group by SaleReturn2.Code,SaleReturn2.Description				),0) as ReturnQty,				Isnull((Select Sum(SaleReturn2.Amount) from SaleReturn2                where Date1 >= '" + StartDate + "'  and Date1 <= '" + EndDate + "' and SaleReturn2.Code=Invoice2.Code and SaleReturn2.Description=Invoice2.Description				group by SaleReturn2.Code,SaleReturn2.Description				),0) as ReturnAmount,				(Sum(Invoice2.Qty)-Isnull((Select Sum(SaleReturn2.Qty) from SaleReturn2                where Date1 >= '" + StartDate + "'  and Date1 <= '" + EndDate + "' and SaleReturn2.Code=Invoice2.Code and SaleReturn2.Description=Invoice2.Description				group by SaleReturn2.Code,SaleReturn2.Description				),0)) as NetQtySold				,				(Sum(Invoice2.Amount)-Isnull((Select Sum(SaleReturn2.Amount) from SaleReturn2                where Date1 >= '" + StartDate + "'  and Date1 <= '" + EndDate + "' and SaleReturn2.Code=Invoice2.Code and SaleReturn2.Description=Invoice2.Description				group by SaleReturn2.Code,SaleReturn2.Description				),0)) as NetAmountSold,				Isnull((Select Sum(InvCode.qty) from InvCode				where InvCode.Code=Invoice2.Code and InvCode.Description=Invoice2.Description				),0) as InHandQty                              FROM Invoice1                 INNER JOIN invoice2 ON Invoice1.InvNo = Invoice2.InvNo                where Invoice1.CompID='" + CompID + "' and   Invoice2.ItemNature = 1  and Invoice1.Date1>='" + StartDate + "' and Invoice1.Date1<='" + EndDate + "' GROUP BY Invoice2.Code,Invoice2.Description                  order by Invoice2.Description     ", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.ItemCode = (dt.Rows[i]["Code"]).ToString();
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                invoice.Quantity = Convert.ToString(dt.Rows[i]["Qty1"]);
                invoice.Amount = Convert.ToString(dt.Rows[i]["Amount"]);
                invoice.ReturnQty = Convert.ToString(dt.Rows[i]["ReturnQty"]);
                invoice.ReturnAmount = Convert.ToString(dt.Rows[i]["ReturnAmount"]);
                invoice.NetQtySold = Convert.ToString(dt.Rows[i]["NetQtySold"]);
                invoice.NetAmountSold = Convert.ToString(dt.Rows[i]["NetAmountSold"]);
                invoice.InHandQty = Convert.ToString(dt.Rows[i]["InHandQty"]);

                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/DaySaleGroupByItem2CR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}