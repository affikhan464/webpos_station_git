﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OperatorOrSalesmanSaleCommissionReport_Print.aspx.cs" Inherits="WebPOS.Reports.Prints.OperatorOrSalesmanSaleCommissionReport_Print" %>

<%--<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.4000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>--%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Print</title>
    <link rel="stylesheet" href="/css/vendor.min.css" />
    <link rel="stylesheet" href="/css/print.css" />

</head>
<body class="p-3">
    <form id="form1" runat="server">
        <%--<div>
            <CR:CrystalReportViewer ID="CRViewer" runat="server" AutoDataBind="true" HasPrintButton="True" PrintMode="ActiveX" GroupTreeStyle-ShowLines="True" />
        </div>--%>
        <%--  <div class="mx-auto w-25 text-center">
            <a class="printPageBtn"><i class="fas fa-print fa-4x p-3"></i></a>
        </div>--%>
        <div id="Page">
            <%@ Register Src="~/_PrintHead.ascx" TagName="_PrintHead" TagPrefix="uc" %>
            <uc:_printhead id="_PrintHead1" runat="server" />
            <div class="row mb-3 justify-content-left align-self-end text-capitalize">
                <div class="col-12 text-center">
                    <strong class="font-size-25 p-2 salesmanName"> </strong>
                </div>
            </div>
            <div class="row mb-3 justify-content-left align-self-end text-capitalize">
                <div class="col-12 ">
                    <strong>From Date:</strong> <span class="DateFrom"></span>
                </div>
                <div class="col-12 ">
                    <strong>To Date:</strong> <span class="DateTo"></span>
                </div>
            </div>
            <table>
                <thead>
                </thead>
                <tbody>
                </tbody>
            </table>
            <div class="row mt-5 justify-content-around">
                <div class="col-6 font-weight-bold text-left mt-4">
                    <span class=" font-size-13">(Software By: SoftKorner 03335101642)</span>
                </div>
                <div class="col-6 font-weight-bold text-right mt-4">
                    <span class=" font-size-13"><%=DateTime.Now %></span>
                </div>
            </div>
        </div>
    </form>

    <script src="/js/vendor.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="/Script/Vendor/sweetalert.min.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="/Script/GetQueryString.js?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script>

        $(document).ready(function () {

            $.ajax({
                url: "/Reports/SaleReports/Prints/OperatorOrSalesmanSaleCommissionReport_Print.aspx/GetReport",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ salesmanCode: getQueryString("salesmanCode"), from: getQueryString("from"), to: getQueryString("to") }),
                success: function (BaseModel) {
                    
                    if (BaseModel.d.Success) {
                        if (BaseModel.d.Data.Rows.length) {

                            var rows = BaseModel.d.Data.Rows;
                            var salesmanName = getQueryString("salesmanName");
                            $(".salesmanName").text(salesmanName);
                            var headrow = "";
                            for (var j = 0; j < Object.values(rows[0]).length; j++) {
                                var key = Object.keys(rows[0])[j];
                                if (key == "Description") {
                                    key = "Item Name";
                                }
                                if (key == "Rate") {
                                    key = "Cost";
                                } if (key == "ReturnQty") {
                                    key = "Return Qty";
                                }
                                if (key == "ProfitOrLoss") {
                                    key = "Profit/Loss";
                                }
                                if (key == "TenCommission") {
                                    key = "10% Comm.";
                                }
                                if (key == "PerPieceCommission") {
                                    key = "Comm./Piece";
                                }
                                if (key == "NetCommission") {
                                    key = "Net Comm.";
                                }
                                headrow += `<th class="text-uppercase text-center" >${key}</th>`;
                            }

                            $("table thead").append(headrow);
                            var invrows = '';
                            for (var i = 0; i < rows.length; i++) {
                                var row = '<tr>';
                                for (var j = 0; j < Object.values(rows[i]).length; j++) {
                                    var tdclass = '';
                                    var value = Object.values(rows[i])[j];
                                    var key = Object.keys(rows[i])[j];
                                    if (key == 'Qty' || key == 'ReturnQty' || key == 'Amount' || key == 'Cost' || key == 'ProfitOrLoss' || key == 'TenCommission' || key == 'PerPieceCommission' || key == 'NetCommission') {
                                        tdclass = 'class="text-right"';
                                        value = (Number(value)).toFixed(2);
                                    }
                                    else {
                                        tdclass = 'class="text-left"';
                                    }
                                    row += `<td ${tdclass}><input type="hidden" name="${key}" value="${value}" > ${value}</td>`;

                                }
                                row += '</tr>';
                                invrows += row;
                            }
                            // Total Rows
                            var totalCol = Object.values(rows[0]).length;
                            var footerTotals = ["QTY"];

                            for (var trows = 0; trows < footerTotals.length; trows++) {

                                var footerrow = '<tr>';

                                for (var j = 0; j < totalCol; j++) {
                                    var currentCol = j + 1;
                                    var key = Object.keys(rows[0])[j];
                                    if (trows == 0) {

                                        footerrow += `<td class="Total${Object.keys(rows[0])[j]} font-weight-bold text-right"></td>`;

                                    }
                                    else {
                                        if (currentCol == (totalCol - 1)) {
                                            footerrow += `<td class="font-weight-bold text-left">${footerTotals[trows].name}</td>`;
                                        }
                                        else if (currentCol == (totalCol)) {
                                            footerrow += `<td class="font-weight-bold text-right ${footerTotals[trows].class}"></td>`;
                                        }
                                        else {
                                            footerrow += `<td class="font-weight-bold text-right"></td>`;
                                        }
                                    }
                                }
                                footerrow += '</tr>';
                                invrows += footerrow;
                            }


                            $("table tbody").append(invrows);
                            calculateData();

                            
                            $("td").each(function () {
                                if (!$(this).text().trim().length) {
                                    $(this).addClass("border-0");
                                }
                            });

                            window.print()
                        }
                    }
                    else if (BaseModel.d.Success == false && BaseModel.d.LoginAgain == true) {
                        swal({
                            title: "Login Again",
                            html:
                                `<a target="_blank" href="/?c=1" style="display:block;color:black;font-weight:700">Login Here and print it again.</a> `,

                            type: "warning"

                        });
                        $("table tbody").append("<tr><td><h2 class='text-center'><a onclick='return location.href=location.href;'>Refresh this Page after login</a></h1></td></tr>");
                    }
                    else {
                        swal("Error", BaseModel.d.Message, "error");
                    }
                }
            })
        });
        function calculateData() {
            debugger
            var sum;
            var inputs = ["Qty", "Amount", "Cost",'ProfitOrLoss', 'TenCommission', 'PerPieceCommission', 'NetCommission', 'ReturnQty'];
            for (var i = 0; i < inputs.length; i++) {

                var currentInput = inputs[i];
                sum = 0;
                $("input[name = '" + currentInput + "']").each(function () {

                    if (this.value.trim() === "") {
                        sum = (Number(sum) + Number(0));
                    } else {
                        sum = (Number(sum) + Number(this.value));

                    }

                });

                $(".Total" + currentInput).text(Number(sum).toFixed(2));


            }

        }
    </script>
</body>
</html>
