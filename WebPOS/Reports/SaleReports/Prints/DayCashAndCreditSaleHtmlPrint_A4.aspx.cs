﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Transactions.TransactionPrints
{
    public partial class DayCashAndCreditSaleHtmlPrint_A4 : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        { }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel GetReport(string from, string to, int stationId,string stationName)
        {
            if (HttpContext.Current.Session["UserId"] == null)
            {
                return new BaseModel() { Success = false, LoginAgain = true };
            }
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var stationWhereClause = string.Empty;
                var stationExpenseWhereClause = string.Empty;
                if (stationId != 0)
                {
                    stationWhereClause = "  and Invoice1.StationId=" + stationId;
                    stationExpenseWhereClause = " StationID = " + stationId + " and ";
                }
                SqlCommand cmd = new SqlCommand(
                "Select " +
                "Invoice1.Date1 as [Date]," +
                "Invoice1.InvNo as Invoice , " +
                "Invoice1.Name as Name," +
                "Invoice3.Total as Total," +
                "Invoice3.Discount1 as Discount," +
                "Invoice3.CashPaid as Paid," +
                "Invoice3.Total-(Invoice3.Discount1+Invoice3.CashPaid) as Balance " +
                "from (Invoice1 inner join invoice3 on invoice1.InvNo=invoice3.InvNo) " +
                "where Invoice1.CompID='" + CompID + "' and  " +
                "Invoice1.Date1>='" + fromDate + "' and " +
                "Invoice1.Date1<='" + toDate + "' " + stationWhereClause +
                " order by Invoice1.InvNo; " +
                "SELECT Sum(AmountDr) as AmountDr FROM GeneralLedger  Where " + stationExpenseWhereClause + " dateDr>='" + fromDate + "' and dateDr <= '" + toDate + "' and SecondDescription='OperationalExpencesThroughCash'; ", con);

                var ds = new CRDataset();
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);

                if (con.State == ConnectionState.Closed)
                {
                    if (con.State == ConnectionState.Closed) { con.Open(); }
                }

                adpt.Fill(ds);
                var dt = ds.Tables["Table"];

                con.Close();
                var model = new PrintViewModel<object>();
                var rows = new List<object>();

                
                model.DateFrom = fromDate.ToString("dd/MM/yyyy");
                model.DateTo = toDate.ToString("dd/MM/yyyy");
                model.ReportStationName = stationName;

                var expenseAmount = ds.Tables["Table1"].Rows[0]["AmountDr"];

                var expenceRow = new
                {
                    Date = "--",
                    InvoiceNumber = "--",
                    PartyName = "Expences",
                    Total = "--",
                    NetDiscount = "--",
                    Paid = (-Convert.ToDecimal(expenseAmount)).ToString(),
                    Balance = "--",
                };
                rows.Add(expenceRow);
                decimal totalTotal = -Convert.ToDecimal(expenseAmount);
                decimal totalPaid = 0;
                decimal totalBalance = 0;
                decimal totalNetDiscount = 0;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                   
                    var row = new 
                    {
                        Date = Convert.ToDateTime(dt.Rows[i]["Date"]).ToString("dd/MMM/yyyy"),
                        InvoiceNumber = Convert.ToString(dt.Rows[i]["Invoice"]),
                        PartyName = Convert.ToString(dt.Rows[i]["Name"]) == "" ? "No Party Name Available" : Convert.ToString(dt.Rows[i]["Name"]),
                        Total = Convert.ToString(dt.Rows[i]["Total"]),
                        NetDiscount = Convert.ToString(dt.Rows[i]["Discount"]),
                        Paid = Convert.ToString(dt.Rows[i]["Paid"]),
                        Balance = Convert.ToString(dt.Rows[i]["Balance"]),
                    };
                    totalTotal += Convert.ToDecimal(row.Total);
                    totalPaid += Convert.ToDecimal(row.Paid);
                    totalBalance += Convert.ToDecimal(row.Balance);
                    totalNetDiscount += Convert.ToDecimal(row.NetDiscount);
                   rows.Add(row);
                }
                model.Rows = rows;
                model.TotalTotal = totalTotal;
                model.TotalPaid = totalPaid - Convert.ToDecimal(expenseAmount);
                model.TotalBalance = totalBalance;
                model.TotalNetDiscount = totalNetDiscount;
                return new BaseModel() { Success = true, Data = model };

            }
            catch (Exception ex)
            {

                return new BaseModel() { Success = true, Message = ex.Message };

            }
        }

        public DateTime getDate(string date)
        {
            var newdate = DateTime.ParseExact(date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);
            return newdate;
        }
    }
}