﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.Prints
{
    public partial class OperatorOrSalesmanSaleCommissionReport_Print : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        { }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel GetReport(string from, string to, string salesmanCode)
        {
            if (HttpContext.Current.Session["UserId"] == null)
            {
                return new BaseModel() { Success = false, LoginAgain = true };
            }
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
              
                    var cmd = new SqlCommand(@"
                       Select 

                            invoice1.InvNo,
                            invoice2.Description,
                           invoice1.Date1,
						   Sum(Invoice2.Qty) as Qty,
						   Invoice2.ActualSellingPrice as SellingCost ,
						   Sum(Invoice2.Amount) as Amount,
						   Invoice2.PerPieceCommission as PerPieceCommission,
						   IsNull(InvCode.PerPieceCommission,0) as  previousCommission,
						   Invoice1.SaleManCode,
						   Invoice2.Code as ItemCode

                           from Invoice1 
                           inner join invoice2 on Invoice1.InvNo=invoice2.InvNo
                           inner join InvCode on InvCode.Code=invoice2.Code
                          where Invoice1.SaleManCode = '" + salesmanCode + "' " +
                          "and Invoice1.CompID='" + CompID + "' " +
                          "and Invoice1.Date1>='" + fromDate + "' " +
                          "and Invoice1.Date1<='" + toDate + @"'  


                          group by invoice1.InvNo,invoice1.Date1,Invoice2.Code,Invoice1.SaleManCode,invoice2.Description,Invoice2.ActualSellingPrice,Invoice2.Rate,InvCode.PerPieceCommission,Invoice2.PerPieceCommission 
						   order by invoice2.Description
                            ", con);

                DataTable dt = new DataTable();
                SqlDataAdapter dAdapter = new SqlDataAdapter();
                dAdapter.SelectCommand = cmd;
                if (con.State == ConnectionState.Closed)
                {
                    if (con.State == ConnectionState.Closed) { con.Open(); }
                }

                dAdapter.Fill(dt);
                con.Close();
                var model = new PrintViewModel<OperatorSalesManPrintPage>();
                var rows = new List<OperatorSalesManPrintPage>();

                
                model.DateFrom = fromDate.ToString("dd/MM/yyyy");
                model.DateTo = toDate.ToString("dd/MM/yyyy");

                var TotalQty = 0;
                decimal TotalNetCommission = 0;
                decimal TotalPerPieceCommission = 0;
                decimal TotalCost = 0;
                decimal TotalProfitOrLoss = 0;
                decimal TotalTenCommission = 0;
                decimal TotalAmount = 0;

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    
                    var amount = Convert.ToDecimal(dt.Rows[i]["Amount"]);
                    var qtySold = Convert.ToDecimal(dt.Rows[i]["Qty"]);
                    var unitCost = Convert.ToDecimal(dt.Rows[i]["SellingCost"]);
                    var isPerPieceCommissionNull = string.IsNullOrEmpty(dt.Rows[i]["PerPieceCommission"].ToString());
                    var previousCommission = string.IsNullOrEmpty(dt.Rows[i]["previousCommission"].ToString()) ? 0 : Convert.ToDecimal(dt.Rows[i]["previousCommission"].ToString());
                    var perPieceCommission = string.Empty;
                    var totalCost = unitCost * qtySold;
                    var profit = amount - totalCost;
                    decimal tenComm = 0;
                    decimal totalComm = 0;
                    if (profit > 0)
                    {
                        tenComm = (profit / 100) * 10;
                    }
                    if (isPerPieceCommissionNull)
                    {
                        if (profit > 0)
                        {
                            var commission = previousCommission;
                            totalComm = commission * qtySold;
                            if (commission != 0)
                            {
                                if (qtySold == 1)
                                {
                                    perPieceCommission = "(" + commission.ToString("N2") + ")";

                                }
                                else
                                {

                                    perPieceCommission = "(" + commission.ToString("N2") + " / " + totalComm.ToString("N2") + ")";
                                }
                            }
                            else
                            {
                                perPieceCommission = "0.00";
                            }

                        }
                        else
                        {
                            perPieceCommission = "0.00";
                        }
                    }
                    else
                    {
                        if (profit > 0)
                        {
                            var commission = Convert.ToInt32(dt.Rows[i]["PerPieceCommission"].ToString());
                            totalComm = commission * qtySold;
                            if (commission != 0)
                            {
                                if (qtySold == 1)
                                {
                                    perPieceCommission = commission.ToString("N2");

                                }
                                else
                                {
                                    perPieceCommission = commission.ToString("N2") + " / " + totalComm.ToString("N2");
                                }
                            }
                            else
                            {
                                perPieceCommission = "0.00";
                            }
                        }
                        else
                        {
                            perPieceCommission = "0.00";
                        }

                    }
                    var cost = string.Empty;
                    if (qtySold == 1)
                    {
                        cost = unitCost.ToString("N2");
                    }
                    else
                    {
                        cost = unitCost.ToString("N2") + " / " + totalCost.ToString("N2");
                    }

                    var row = new OperatorSalesManPrintPage()
                    {
                        Code = Convert.ToString(dt.Rows[i]["ItemCode"]),
                        Date = Convert.ToDateTime(dt.Rows[i]["Date1"]).ToString("dd/MM/yyyy"),
                        Description = Convert.ToString(dt.Rows[i]["Description"]).Trim(),
                        Qty = Convert.ToString(dt.Rows[i]["Qty"]),
                        ReturnQty = "0",
                        Amount = amount.ToString(),
                        Cost = totalCost.ToString(),
                        ProfitOrLoss = profit.ToString(),
                        TenCommission = tenComm.ToString(),
                        PerPieceCommission = totalComm.ToString(),
                        NetCommission = (totalComm + tenComm).ToString(),

                    };
                    //TotalQty += Convert.ToInt32(qtySold);
                    //TotalNetCommission += (totalComm + tenComm);
                    //TotalPerPieceCommission += totalComm ;
                    //TotalCost           += totalCost;
                    //TotalProfitOrLoss   += profit;
                    //TotalTenCommission += tenComm;
                    //TotalAmount += amount;
                    rows.Add(row);
                }

                

                model.Rows=rows;

                var returnedItems = getReturnedItems(fromDate, toDate, salesmanCode);

                var itemToWhichReturnAdded = new List<string>();
               
                decimal retTotalNetCommission = 0;
                decimal retTotalPerPieceCommission = 0;
                decimal retTotalProfitOrLoss = 0;
                decimal retTotalTenCommission = 0;
                decimal retTotalQty = 0;
                decimal retTotalCost = 0;
                foreach (var returnedItem in returnedItems)
                {
                    var item = model.Rows.FirstOrDefault(x => x.Code == returnedItem.Code && x.Amount == returnedItem.Amount && x.Qty== returnedItem.Quantity && !itemToWhichReturnAdded.Contains(x.Code));
                    if (item != null)
                    {
                        itemToWhichReturnAdded.Add(item.Code);
                        item.ReturnQty = returnedItem.Quantity;
                        item.Amount = "0";
                        item.ProfitOrLoss = "0";
                        item.Cost = "0";
                        item.TenCommission = "0";
                        item.PerPieceCommission = "0";
                        item.NetCommission = "0";

                        retTotalQty+= Convert.ToDecimal(returnedItem.Quantity);

                    }
                    else
                    {

                        var itemWhoseSoldItemNotFound = new OperatorSalesManPrintPage()
                        {
                            Code = returnedItem.Code,
                            Date = returnedItem.Date,
                            Description = returnedItem.Description,
                            Qty = "0",
                            ReturnQty = returnedItem.Quantity,
                            Amount = (-1 * Convert.ToDecimal(returnedItem.Amount)).ToString(),
                            Cost = (-1 * Convert.ToDecimal(returnedItem.Cost)).ToString(),
                            ProfitOrLoss = returnedItem.Profit,
                            TenCommission = returnedItem.TenCommission,
                            PerPieceCommission = returnedItem.PerPieceCommission,
                            NetCommission = returnedItem.NetCommission,

                        };

                        retTotalNetCommission += Convert.ToDecimal(itemWhoseSoldItemNotFound.NetCommission);
                        retTotalPerPieceCommission += Convert.ToDecimal(itemWhoseSoldItemNotFound.PerPieceCommission);
                        retTotalProfitOrLoss += Convert.ToDecimal(itemWhoseSoldItemNotFound.ProfitOrLoss);
                        retTotalTenCommission += Convert.ToDecimal(itemWhoseSoldItemNotFound.TenCommission);
                        retTotalQty+= Convert.ToDecimal(itemWhoseSoldItemNotFound.ReturnQty);

                        model.Rows.Add(itemWhoseSoldItemNotFound);
                    }
                }


                model.Rows = model.Rows.OrderBy(x=>x.ReturnQty).ThenBy(a => a.Description).ToList();
               

                //model.TotalQty = TotalQty;
                //model.TotalReturnQty = retTotalQty;
                //model.TotalNetCommission = TotalNetCommission - retTotalNetCommission;
                //model.TotalPerPieceCommission = TotalPerPieceCommission - retTotalPerPieceCommission;
                //model.TotalCost = "--";
                //model.TotalProfitOrLoss = TotalProfitOrLoss - retTotalProfitOrLoss;
                //model.TotalTenCommission = TotalTenCommission - retTotalTenCommission;
                //model.TotalAmount = "--";
                return new BaseModel() { Success = true, Data = model };

            }
            catch (Exception ex)
            {

                return new BaseModel() { Success = true, Message = ex.Message };

            }
        }
        public static List<InvoiceViewModel> getReturnedItems(DateTime StartDate, DateTime EndDate, string salesmanCode)
        {
            var returnedInvoiceViewModel = new ReturnedInvoiceViewModel<InvoiceViewModel>();
            SqlCommand itemReturnedCmd = new SqlCommand(@"
            SELECT 
	            SaleReturn1.Date1,
	            SaleReturn1.InvNo,
	            SaleReturn1.SalesMan,
                SaleReturn2.Code,
                SaleReturn2.Description,
                SaleReturn2.Rate,
                SaleReturn2.Qty,
                SaleReturn2.Amount,
                SaleReturn1.PartyCode

                FROM SaleReturn2
                join SaleReturn1 on SaleReturn2.InvNo = SaleReturn1.InvNo
                Where 
                SaleReturn1.Date1 >= '" + StartDate + @"' and
                SaleReturn1.Date1 <= '" + EndDate + @"' and
                SaleReturn1.SalesMan='" + salesmanCode + @"'
            ", con);
            SqlDataAdapter adpt = new SqlDataAdapter(itemReturnedCmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);

            var returnedItems = new List<InvoiceViewModel>();


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var Date = Convert.ToString(dt.Rows[i]["Date1"]).Trim();
                var Description = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                var InvNo = Convert.ToString(dt.Rows[i]["InvNo"]).Trim();
                var SalesManCode = Convert.ToString(dt.Rows[i]["SalesMan"]);
                var Code = Convert.ToString(dt.Rows[i]["Code"]);
                var Rate = Convert.ToString(dt.Rows[i]["Rate"]);
                var Qty = Convert.ToString(dt.Rows[i]["Qty"]);
                var Amount = Convert.ToString(dt.Rows[i]["Amount"]);
                var PartyCode = Convert.ToString(dt.Rows[i]["PartyCode"]);

                var returnedItem = new InvoiceViewModel
                {
                    Date = Date,
                    Code = Code,
                    SaleReturnInvNo = InvNo,
                    Quantity = Qty,
                    Rate = Rate,
                    Amount = Amount,
                    SalesManCode = SalesManCode,
                    Description = Description,
                    PartyCode = PartyCode

                };
                returnedItems.Add(returnedItem);
            }
            var calculatedModel = new List<InvoiceViewModel>();

            foreach (var item in returnedItems)
            {
                var itemSoldCMD = new SqlCommand(@"
                Select 
                    Top 1
                    Invoice1.InvNo,
                    Invoice1.SaleManCode,
                    Invoice2.Code,
                    Invoice2.Description,
                    Invoice2.Rate,
                    Invoice2.Qty,
					Invoice2.ActualSellingPrice as SellingCost ,
                    Invoice2.PerPieceCommission as PerPieceCommission,
				    IsNull(InvCode.PerPieceCommission,0) as  previousCommission,
                    Invoice2.Amount
                    
                From Invoice1
                join Invoice2 on Invoice2.InvNo=Invoice1.InvNo
                inner join InvCode on InvCode.Code=invoice2.Code
                Where 
           	        InvCode.Code=" + item.Code + @" and 
           	        SaleManCode=" + item.SalesManCode + @" and 
                    Rate ='" + item.Rate + @"'and 
                    PartyCode ='" + item.PartyCode + @"'
                Order By Date1 Desc
                ", con);
                SqlDataAdapter adpt2 = new SqlDataAdapter(itemSoldCMD);
                DataTable dt2 = new DataTable();
                adpt2.Fill(dt2);


                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    var invoice = new InvoiceViewModel();
                    var InvNo = dt2.Rows[i]["InvNo"].ToString();
                    var amount = Convert.ToDecimal(dt2.Rows[i]["Amount"]);
                    var returnAmount = Convert.ToDecimal(item.Amount);
                    var qty = Convert.ToDecimal(item.Quantity); //Return Qty
                    var returnQty = Convert.ToDecimal(item.Quantity);
                    var unitCost = Convert.ToDecimal(dt2.Rows[i]["SellingCost"]);
                    var isPerPieceCommissionNull = string.IsNullOrEmpty(dt2.Rows[i]["PerPieceCommission"].ToString());
                    var previousCommission = string.IsNullOrEmpty(dt2.Rows[i]["previousCommission"].ToString()) ? 0 : Convert.ToDecimal(dt2.Rows[i]["previousCommission"].ToString());
                    var perPieceCommission = string.Empty;

                    //var netqtySold = qty - returnQty;
                    var qtySold = qty;
                    var totalCost = qtySold * unitCost;
                    //var totalReturnedItemCost = returnQty * unitCost;
                    var profit = totalCost - returnAmount;
                    //var returnProfit = (returnAmount - totalReturnedItemCost);
                    //var netAmount = (amount - returnAmount);

                    //profit = profit - returnProfit;

                    decimal tenComm = 0;
                    decimal totalComm = 0;

                    if (profit < 0)
                    {
                        tenComm = (profit / 100) * 10;
                    }
                    if (isPerPieceCommissionNull)
                    {
                        if (profit < 0)
                        {
                            var commission = -previousCommission;
                            totalComm = commission * qtySold;
                            if (commission != 0)
                            {
                                if (qtySold == 1)
                                {
                                    perPieceCommission = "(" + commission.ToString("N2") + ")";

                                }
                                else
                                {

                                    perPieceCommission = "(" + commission.ToString("N2") + " / " + totalComm.ToString("N2") + ")";
                                }
                            }
                            else
                            {
                                perPieceCommission = "0.00";
                            }

                        }
                        else
                        {
                            perPieceCommission = "0.00";
                        }
                    }
                    else
                    {
                        if (profit < 0)
                        {
                            var commission = -Convert.ToInt32(dt2.Rows[i]["PerPieceCommission"].ToString());
                            totalComm = commission * qtySold;
                            if (commission != 0)
                            {
                                if (qtySold == 1)
                                {
                                    perPieceCommission = commission.ToString("N2");

                                }
                                else
                                {
                                    perPieceCommission = commission.ToString("N2") + " / " + totalComm.ToString("N2");
                                }
                            }
                            else
                            {
                                perPieceCommission = "0.00";
                            }
                        }
                        else
                        {
                            perPieceCommission = "0.00";
                        }

                    }
                    var cost = unitCost;
                    
                    invoice.Date = Convert.ToDateTime(item.Date).ToString("dd/MM/yyyy");
                    invoice.Description = Convert.ToString(dt2.Rows[i]["Description"]).Trim();
                    invoice.Quantity = Convert.ToString(qty);

                    invoice.Code = Convert.ToString(dt2.Rows[i]["Code"]);
                    invoice.Cost = cost.ToString();
                    invoice.TotalCost = (totalCost).ToString();
                    invoice.Amount = (returnAmount).ToString();
                    invoice.Profit = profit.ToString();
                    invoice.TenCommission = tenComm.ToString();
                    invoice.PerPieceCommission = perPieceCommission;
                    invoice.TotalPerPieceCommission = totalComm.ToString();
                    invoice.NetCommission = (totalComm + tenComm).ToString();
                    invoice.IsReturnedItem = true;
                    invoice.SaleReturnInvNo = item.SaleReturnInvNo;
                    invoice.SaleInvNo = InvNo;


                    calculatedModel.Add(invoice);
                }
            }

            return calculatedModel;

        }
        public static ReturnedInvoiceViewModel<OperatorSalesManPrintPage> getReturnedItems1(DateTime StartDate, DateTime EndDate, string salesmanCode)
        {

            var TotalQty = 0;
            decimal TotalNetCommission = 0;
            decimal TotalPerPieceCommission = 0;
            decimal TotalCost = 0;
            decimal TotalProfitOrLoss = 0;
            decimal TotalTenCommission = 0;
            decimal TotalAmount = 0;
            var returnedInvoiceViewModel = new ReturnedInvoiceViewModel<OperatorSalesManPrintPage>();
            SqlCommand itemReturnedCmd = new SqlCommand(@"
            SELECT 
	            SaleReturn1.Date1,
	            SaleReturn1.InvNo,
	            SaleReturn1.SalesMan,
                SaleReturn2.Code,
                SaleReturn2.Description,
                SaleReturn2.Rate,
                SaleReturn2.Qty,
                SaleReturn2.Amount,
                SaleReturn1.PartyCode

                FROM SaleReturn2
                join SaleReturn1 on SaleReturn2.InvNo = SaleReturn1.InvNo
                Where 
                SaleReturn1.Date1 >= '" + StartDate + @"' and
                SaleReturn1.Date1 <= '" + EndDate + @"' and
                SaleReturn1.SalesMan=" + salesmanCode + @"
            ", con);
            SqlDataAdapter adpt = new SqlDataAdapter(itemReturnedCmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);

            var returnedItems = new List<InvoiceViewModel>();


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var Date = Convert.ToString(dt.Rows[i]["Date1"]).Trim();
                var Description = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                var InvNo = Convert.ToString(dt.Rows[i]["InvNo"]).Trim();
                var SalesManCode = Convert.ToString(dt.Rows[i]["SalesMan"]);
                var Code = Convert.ToString(dt.Rows[i]["Code"]);
                var Rate = Convert.ToString(dt.Rows[i]["Rate"]);
                var Qty = Convert.ToString(dt.Rows[i]["Qty"]);
                var Amount = Convert.ToString(dt.Rows[i]["Amount"]);
                var PartyCode = Convert.ToString(dt.Rows[i]["PartyCode"]);

                var returnedItem = new InvoiceViewModel
                {
                    Date = Date,
                    Code = Code,
                    SaleReturnInvNo = InvNo,
                    Quantity = Qty,
                    Rate = Rate,
                    Amount = Amount,
                    SalesManCode = SalesManCode,
                    Description = Description,
                    PartyCode = PartyCode

                };
                returnedItems.Add(returnedItem);
            }
            var calculatedModel = new List<OperatorSalesManPrintPage>();
            var calculatedCodes = new List<string>();

            foreach (var item in returnedItems)
            {
                var itemSoldCMD = new SqlCommand(@"
                Select 
                    Top 1
                    Invoice1.InvNo,
                    Invoice1.SaleManCode,
                    Invoice2.Code,
                    Invoice2.Description,
                    Invoice2.Rate,
                    Invoice2.Qty,
					Invoice2.ActualSellingPrice as SellingCost ,
                    Invoice2.PerPieceCommission as PerPieceCommission,
				    IsNull(InvCode.PerPieceCommission,0) as  previousCommission,
                    Invoice2.Amount
                    
                From Invoice1
                join Invoice2 on Invoice2.InvNo=Invoice1.InvNo
                inner join InvCode on InvCode.Code=invoice2.Code
                Where 
           	        InvCode.Code=" + item.Code + @" and 
           	        SaleManCode=" + item.SalesManCode + @" and 
                    Rate ='" + item.Rate + @"'and 
                    PartyCode ='" + item.PartyCode + @"'
                Order By Date1 Desc
                ", con);
                SqlDataAdapter adpt2 = new SqlDataAdapter(itemSoldCMD);
                DataTable dt2 = new DataTable();
                adpt2.Fill(dt2);


                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    var invoice = new InvoiceViewModel();
                    var InvNo = dt2.Rows[i]["InvNo"].ToString();
                    var amount = Convert.ToDecimal(dt2.Rows[i]["Amount"]);
                    var returnAmount = Convert.ToDecimal(item.Amount);
                    var qty = Convert.ToDecimal(item.Quantity); //Return Qty
                    var returnQty = Convert.ToDecimal(item.Quantity);
                    var unitCost = Convert.ToDecimal(dt2.Rows[i]["SellingCost"]);
                    var isPerPieceCommissionNull = string.IsNullOrEmpty(dt2.Rows[i]["PerPieceCommission"].ToString());
                    var previousCommission = string.IsNullOrEmpty(dt2.Rows[i]["previousCommission"].ToString()) ? 0 : Convert.ToDecimal(dt2.Rows[i]["previousCommission"].ToString());
                    var perPieceCommission = string.Empty;

                    //var netqtySold = qty - returnQty;
                    var qtySold = qty;
                    var totalCost = qtySold * unitCost;
                    //var totalReturnedItemCost = returnQty * unitCost;
                    var profit = totalCost - returnAmount;
                    //var returnProfit = (returnAmount - totalReturnedItemCost);
                    //var netAmount = (amount - returnAmount);

                    //profit = profit - returnProfit;

                    decimal tenComm = 0;
                    decimal totalComm = 0;

                    if (profit < 0)
                    {
                        tenComm = (profit / 100) * 10;
                    }
                    if (isPerPieceCommissionNull)
                    {
                        if (profit < 0)
                        {
                            var commission = -previousCommission;
                            totalComm = commission * qtySold;
                            if (commission != 0)
                            {
                                if (qtySold == 1)
                                {
                                    perPieceCommission = "(" + commission.ToString("N2") + ")";

                                }
                                else
                                {

                                    perPieceCommission = "(" + commission.ToString("N2") + " / " + totalComm.ToString("N2") + ")";
                                }
                            }
                            else
                            {
                                perPieceCommission = "0.00";
                            }

                        }
                        else
                        {
                            perPieceCommission = "0.00";
                        }
                    }
                    else
                    {
                        if (profit < 0)
                        {
                            var commission = -Convert.ToInt32(dt2.Rows[i]["PerPieceCommission"].ToString());
                            totalComm = commission * qtySold;
                            if (commission != 0)
                            {
                                if (qtySold == 1)
                                {
                                    perPieceCommission = commission.ToString("N2");

                                }
                                else
                                {
                                    perPieceCommission = commission.ToString("N2") + " / " + totalComm.ToString("N2");
                                }
                            }
                            else
                            {
                                perPieceCommission = "0.00";
                            }
                        }
                        else
                        {
                            perPieceCommission = "0.00";
                        }

                    }
                    var cost = unitCost.ToString("N2");
                    if (qtySold == 1)
                    {
                        cost = unitCost.ToString("N2");

                    }
                    else
                    {
                        cost = unitCost.ToString("N2") + " / " + totalCost.ToString("N2");
                    }

                    var row = new OperatorSalesManPrintPage()
                    {
                       
                        Date = Convert.ToDateTime(item.Date).ToString("dd/MM/yyyy"),
                        Description = Convert.ToString(dt2.Rows[i]["Description"]).Trim() + "(--Returned)",
                        Qty = Convert.ToString(-qty),
                        Amount = (-returnAmount).ToString(),
                        Cost = totalCost.ToString(),
                        ProfitOrLoss = profit.ToString(),
                        TenCommission = tenComm.ToString(),
                        PerPieceCommission = totalComm.ToString(),
                        NetCommission = (totalComm + tenComm).ToString()


                    };

                    TotalQty += Convert.ToInt32(qtySold);
                    TotalNetCommission += (totalComm + tenComm);
                    TotalPerPieceCommission += totalComm;
                    TotalCost += totalCost;
                    TotalProfitOrLoss += profit;
                    TotalTenCommission += tenComm;
                    TotalAmount += returnAmount;

                    calculatedCodes.Add(Convert.ToString(dt2.Rows[i]["Code"]));
                    calculatedModel.Add(row);
                }
            }

            returnedInvoiceViewModel.CalculatedObj = calculatedModel;
            returnedInvoiceViewModel.TotalQty = -TotalQty;
            returnedInvoiceViewModel.TotalNetCommission = TotalNetCommission;
            returnedInvoiceViewModel.TotalPerPieceCommission = TotalPerPieceCommission;
            returnedInvoiceViewModel.TotalCost = -TotalCost;
            returnedInvoiceViewModel.TotalProfitOrLoss = TotalProfitOrLoss;
            returnedInvoiceViewModel.TotalTenCommission = TotalTenCommission;
            returnedInvoiceViewModel.TotalAmount = -TotalAmount;
          
            return returnedInvoiceViewModel;

        }
        public DateTime getDate(string date)
        {
            var newdate = DateTime.ParseExact(date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);
            return newdate;
        }
    }
    public class OperatorSalesManPrintPage
    {
        public string Code { get; set; }
        public string Date { get; set; }
        public string Description { get; set; }
        public string Qty { get; set; }
        public string ReturnQty { get; set; }
        public string Amount { get; set; }
        public string Cost { get; set; }
        public string ProfitOrLoss { get; set; }
        public string TenCommission { get; set; }
        public string PerPieceCommission { get; set; }
        public string NetCommission { get; set; }
    }

}