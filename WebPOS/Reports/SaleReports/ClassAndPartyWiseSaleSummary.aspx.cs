﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class ClassAndPartyWiseSaleSummary : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string partyCode, string ClassID)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, partyCode, ClassID);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, string partyCode, string ClassID)
        {

            ////        SqlCommand cmd = new SqlCommand(@"
            ////                       Select 
            ////                          InvCode.Code  as Code , 
            ////            Invoice2.Description as Description , 
            ////            Sum(Invoice2.Qty) as Qty ,
            ////            Sum(Invoice2.Amount) as Amount,

            ////Isnull((
            ////                    Select Sum(SaleReturn2.Qty) from SaleReturn2
            ////        where Date1>='" + StartDate + @"'  
            ////                    and Date1 <= '" + EndDate + @"' 
            ////                    and SaleReturn2.Code=InvCode.Code 
            ////                    group by SaleReturn2.Code,SaleReturn2.Description				
            ////            ),0) as ReturnQty,				
            ////            Isnull((
            ////                    Select Sum(SaleReturn2.Amount) from SaleReturn2				
            ////                    where Date1 >= '" + StartDate + @"'  
            ////                    and Date1 <= '" + EndDate + @"' 
            ////                    and SaleReturn2.Code=InvCode.Code  			
            ////                    group by SaleReturn2.Code,SaleReturn2.Description				
            ////            ),0) as ReturnAmount,		
            ////            Isnull((
            ////                    Select Sum(InvCode.qty) from InvCode				
            ////                    where  InvCode.Code=InvCode.Code  			
            ////            ),0) as InHandQty 

            ////            FROM Invoice1                 
            ////            INNER JOIN Invoice2 ON Invoice1.InvNo = Invoice2.InvNo          
            ////            inner join InvCode on Invoice2.Code=InvCode.Code 
            ////            where  Invoice1.PartyCode = '" + partyCode + @"' 
            ////            and InvCode.Brand= '" + brandCode + @"' 
            ////            and Invoice1.CompID='" + CompID + @"' 
            ////            and Invoice2.ItemNature = 1  
            ////            and Invoice1.Date1>='" + StartDate + @"' 
            ////            and Invoice1.Date1<='" + EndDate + @"' 
            ////            GROUP BY InvCode.Code ,Invoice2.Description       
            ////            Order by Invoice2.Description", con);
            SqlCommand cmd = new SqlCommand(@"select A.code,A.Description,
                                                            sum(A.sq) as [SaleQty],
		                                                    sum(A.sa) as [SaleAmount],
		                                                    sum(A.srq) as [SaleReturnQty],
		                                                    sum(A.sra) as [SaleReturnAmount]
                                                        
                                        FROM(SELECT Inv2.Code, Inv2.Description, Inv2.QTY[sq], Inv2.amount[sa], 0 as [srq], 0 as [sra]
                                        FROM invoice1 Inv1 inner JOIN invoice2 Inv2 ON Inv1.invno = Inv2.invno
        
                                                        where

                                                            Inv1.date1 >= '" + StartDate + @"' 
                                                            and Inv1.date1 <='" + EndDate + @"' 
                                                            and Inv1.PartyCode = '" + partyCode + @"' 
                                                            

                                                        UNION ALL

                                                            SELECT SR2.Code, SR2.Description, 0 as [sq], 0 as [sa], SR2.QTY[srq], SR2.amount as [sra]
                                                            FROM salereturn2 SR2 inner join SaleReturn1 on SR2.invno=salereturn1.invno
                                                            inner join invcode on SR2.code=invcode.code

                                                        WHERE

                                                            SR2.date1 >= '" + StartDate + @"'  
                                                            and SR2.date1 <='" + EndDate + @"'
                                                            and invcode.Class ='" + ClassID + @"'
                                                            and SaleReturn1.PartyCode = '" + partyCode + @"' 

                                                            ) AS A JOIN invcode InvC ON InvC.code = A.Code

                                                            and InvC.Class ='" + ClassID + @"' 

                                                             GROUP BY A.Code, A.Description , InvC.Qty ORDER BY A.Description", con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                var Quantity = Convert.ToInt32(dt.Rows[i]["SaleQty"]);
                var ReturnQty = Convert.ToInt32(dt.Rows[i]["SaleReturnQty"]);
                var ReturnAmount = Convert.ToInt32(dt.Rows[i]["SaleReturnAmount"]);
                var Amount = Convert.ToInt32(dt.Rows[i]["SaleAmount"]);


                invoice.ItemCode = (dt.Rows[i]["Code"]).ToString();
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                invoice.Quantity = Quantity.ToString();
                invoice.ReturnQty = ReturnQty.ToString();
                invoice.ReturnAmount = ReturnAmount.ToString();
                invoice.Amount = Amount.ToString();
                invoice.NetQtySold = (Quantity - ReturnQty).ToString();
                invoice.NetAmountSold = (Amount - ReturnAmount).ToString();
               // invoice.InHandQty = Convert.ToString(dt.Rows[i]["InHandQty"]);

                model.Add(invoice);
            }

            return model;




        }
        protected void BtnClick(object sender, EventArgs e)
        {

            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            //var partyCode = PartyCode.Text;
            var itemCode = ""; //brandCodeTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/ItemAndPartyWiseSaleDetailCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&partyCode=" + partyCode + "&itemCode=" + itemCode);
        }
    }
}