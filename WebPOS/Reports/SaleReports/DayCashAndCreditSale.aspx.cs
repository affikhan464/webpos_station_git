﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class DayCashAndCreditSale : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, int stationId)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, stationId);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, int stationId)
        {
            var stationWhereClause = string.Empty;
            var stationExpenseWhereClause = string.Empty;
            if (stationId != 0)
            {
                stationWhereClause = "  and Invoice1.StationId=" + stationId;
                stationExpenseWhereClause = " StationID = " + stationId + " and ";
            }

            SqlCommand cmd = new SqlCommand(
                "Select " +
                "Invoice1.Date1 as [Date]," +
                "Invoice1.InvNo as Invoice , " +
                "Invoice1.Name as Name," +
                "Invoice3.Total as Total," +
                "Invoice3.Discount1 as Discount," +
                "Invoice3.CashPaid as Paid," +
                "Invoice3.Total-(Invoice3.Discount1+Invoice3.CashPaid) as Balance " +
                "from (Invoice1 inner join invoice3 on invoice1.InvNo=invoice3.InvNo) " +
                "where Invoice1.CompID='" + CompID + "' and  " +
                "Invoice1.Date1>='" + StartDate + "' and " +
                "Invoice1.Date1<='" + EndDate + "' " + stationWhereClause +
                " order by Invoice1.InvNo; " +
                "SELECT Sum(AmountDr) as AmountDr FROM GeneralLedger  Where "+ stationExpenseWhereClause + " dateDr>='" + StartDate + "' and dateDr <= '" + EndDate + "' and SecondDescription='OperationalExpencesThroughCash'; ", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            adpt.Fill(ds);
            var dt = ds.Tables[0];
            var expenseAmount = ds.Tables[1].Rows[0]["AmountDr"];

            var model = new List<InvoiceViewModel>();
            var expenceRow = new InvoiceViewModel();
            expenceRow.Date = "--";
            expenceRow.InvoiceNumber = "--";
            expenceRow.PartyName = "Expences";
            expenceRow.NetDiscount = "--"; ;
            expenceRow.Total = "--";
            expenceRow.Paid = expenseAmount.ToString() ;
            expenceRow.Balance = "--";
            model.Add(expenceRow);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.Date = Convert.ToDateTime(dt.Rows[i]["Date"]).ToString("dd/MMM/yyyy");
                invoice.InvoiceNumber = Convert.ToString(dt.Rows[i]["Invoice"]);
                invoice.PartyName = Convert.ToString(dt.Rows[i]["Name"]) == "" ? "No Party Name Available" : Convert.ToString(dt.Rows[i]["Name"]);
                invoice.NetDiscount = Convert.ToString(dt.Rows[i]["Discount"]);
                invoice.Total = Convert.ToString(dt.Rows[i]["Total"]);
                invoice.Paid = Convert.ToString(dt.Rows[i]["Paid"]);
                invoice.Balance = Convert.ToString(dt.Rows[i]["Balance"]);
                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}