﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class OperatorOrSalesmanSaleCommissionReportAdvanced : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string salesmanCode)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, salesmanCode);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }


        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, string salesmanCode)
        //{

        //    SqlCommand cmd = new SqlCommand(@"
        //                   Select 
        //                   invoice2.Description,
						  // Sum(Invoice2.Qty) as Qty,
						  // Invoice2.ActualSellingPrice as SellingCost ,
						  // Sum(Invoice2.Amount) as Amount,
						  // Invoice2.PerPieceCommission as PerPieceCommission,
						  // IsNull(InvCode.PerPieceCommission,0) as  previousCommission,
						  // Invoice1.SaleManCode,
						  // Invoice2.Code as ItemCode,
        //                   (ISNULL((
        //                           Select Sum(SaleReturn2.Qty) from SaleReturn2 
        //                           inner join InvCode as item on SaleReturn2.Code=item.Code 
        //                           inner join SaleReturn1 on SaleReturn2.InvNo=SaleReturn1.InvNo                         
        //                           where 
        //                            SaleReturn1.Date1>='" + StartDate + @"' 
				    //          		and SaleReturn1.Date1<='" + EndDate + @"'
				    //          		and SaleReturn1.SalesMan=Invoice1.SaleManCode
				    //          		and SaleReturn2.Code=Invoice2.Code
        //                           ),0))
        //                   as ReturnQty,
        //                   (ISNULL((
        //                           Select Sum(SaleReturn2.Amount) from SaleReturn2 
        //                           inner join InvCode as item on SaleReturn2.Code=item.Code 
        //                           inner join SaleReturn1 on SaleReturn2.InvNo=SaleReturn1.InvNo                         
        //                           where 
        //                            SaleReturn1.Date1>='" + StartDate + @"' 
				    //          		and SaleReturn1.Date1<='" + EndDate + @"'
				    //          		and SaleReturn1.SalesMan=Invoice1.SaleManCode
				    //          		and SaleReturn2.Code=Invoice2.Code
        //                           ),0))
        //                   as ReturnAmount
        //                   from Invoice1 
        //                   inner join invoice2 on Invoice1.InvNo=invoice2.InvNo
        //                   inner join InvCode on InvCode.Code=invoice2.Code
        //                  where Invoice1.SaleManCode = '" + salesmanCode + "' " +
        //                  "and Invoice1.CompID='" + CompID + "' " +
        //                  "and Invoice1.Date1>='" + StartDate + "' " +
        //                  "and Invoice1.Date1<='" + EndDate + @"'  


        //                    group by Invoice2.Code,Invoice1.SaleManCode,invoice2.Description,Invoice2.ActualSellingPrice,Invoice2.Rate,InvCode.PerPieceCommission,Invoice2.PerPieceCommission 
						  // order by invoice2.Description
        //                    ", con);
        //    SqlDataAdapter adpt = new SqlDataAdapter(cmd);
        //    DataTable dt = new DataTable();
        //    adpt.Fill(dt);
        //    var model = new List<InvoiceViewModel>();

        //    //Storing last month sold item code so that its return should not calculate again with previous months returned items
        //    var lastMonthSoldItemCodes = new List<string>();



        //    for (int i = 0; i < dt.Rows.Count; i++)
        //    {

        //        var invoice = new InvoiceViewModel();
        //        var itemCode = dt.Rows[i]["ItemCode"].ToString();
        //        lastMonthSoldItemCodes.Add(itemCode);
        //        var amount = Convert.ToDecimal(dt.Rows[i]["Amount"]);
        //        var returnAmount = Convert.ToDecimal(dt.Rows[i]["ReturnAmount"]);
        //        var qty = Convert.ToDecimal(dt.Rows[i]["Qty"]);
        //        var returnQty = Convert.ToDecimal(dt.Rows[i]["ReturnQty"]);
        //        var unitCost = Convert.ToDecimal(dt.Rows[i]["SellingCost"]);
        //        var isPerPieceCommissionNull = string.IsNullOrEmpty(dt.Rows[i]["PerPieceCommission"].ToString());
        //        var previousCommission = string.IsNullOrEmpty(dt.Rows[i]["previousCommission"].ToString()) ? 0 : Convert.ToDecimal(dt.Rows[i]["previousCommission"].ToString());
        //        var perPieceCommission = string.Empty;

        //        var netqtySold = qty - returnQty;
        //        var qtySold = qty;
        //        var totalCost = qtySold * unitCost;
        //        var totalReturnedItemCost = returnQty * unitCost;
        //        var profit = amount - totalCost;
        //        var returnProfit = (returnAmount - totalReturnedItemCost);
        //        var netAmount = (amount - returnAmount);

        //        profit = profit - returnProfit;

        //        decimal tenComm = 0;
        //        decimal totalComm = 0;
        //        if (profit > 0)
        //        {
        //            tenComm = (profit / 100) * 10;
        //        }
        //        if (isPerPieceCommissionNull)
        //        {
        //            if (profit > 0)
        //            {
        //                var commission = previousCommission;
        //                totalComm = commission * qtySold;
        //                if (commission != 0)
        //                {
        //                    if (qtySold == 1)
        //                    {
        //                        perPieceCommission = "(" + commission.ToString("N2") + ")";

        //                    }
        //                    else
        //                    {

        //                        perPieceCommission = "(" + commission.ToString("N2") + " / " + totalComm.ToString("N2") + ")";
        //                    }
        //                }
        //                else
        //                {
        //                    perPieceCommission = "0.00";
        //                }

        //            }
        //            else
        //            {
        //                perPieceCommission = "0.00";
        //            }
        //        }
        //        else
        //        {
        //            if (profit > 0)
        //            {
        //                var commission = Convert.ToInt32(dt.Rows[i]["PerPieceCommission"].ToString());
        //                totalComm = commission * qtySold;
        //                if (commission != 0)
        //                {
        //                    if (qtySold == 1)
        //                    {
        //                        perPieceCommission = commission.ToString("N2");

        //                    }
        //                    else
        //                    {
        //                        perPieceCommission = commission.ToString("N2") + " / " + totalComm.ToString("N2");
        //                    }
        //                }
        //                else
        //                {
        //                    perPieceCommission = "0.00";
        //                }
        //            }
        //            else
        //            {
        //                perPieceCommission = "0.00";
        //            }

        //        }
        //        var cost = unitCost.ToString("N2");
        //        if (qtySold == 1)
        //        {
        //            cost = unitCost.ToString("N2");

        //        }
        //        else
        //        {
        //            cost = unitCost.ToString("N2") + " / " + totalCost.ToString("N2");
        //        }
        //        invoice.Description = Convert.ToString(dt.Rows[i]["Description"]).Trim();
        //        invoice.NetQtySold = Convert.ToString(netqtySold);
        //        invoice.Quantity = Convert.ToString(qty);
        //        invoice.ReturnQty = Convert.ToString(returnQty);

        //        invoice.Cost = cost;
        //        invoice.TotalCost = totalCost.ToString();
        //        invoice.Amount = amount.ToString();
        //        invoice.ReturnAmount = returnAmount.ToString();
        //        invoice.NetAmountSold = netAmount.ToString();
        //        invoice.Profit = profit.ToString();
        //        invoice.TenCommission = tenComm.ToString();
        //        invoice.PerPieceCommission = perPieceCommission;
        //        invoice.TotalPerPieceCommission = totalComm.ToString();
        //        invoice.NetCommission = (totalComm + tenComm).ToString();
        //        invoice.IsReturnedItem = false;

        //        model.Add(invoice);
        //    }

        //    var returnedItems = getReturnedItems(StartDate, EndDate, salesmanCode, lastMonthSoldItemCodes);

        //    model.AddRange(returnedItems);

        //    return model;
        //}

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, string salesmanCode)
        {

            SqlCommand cmd = new SqlCommand(@"
                           Select 
                           invoice1.InvNo,
                            invoice2.Description,
                           invoice1.Date1,
						   Sum(Invoice2.Qty) as Qty,
						   Invoice2.ActualSellingPrice as SellingCost ,
						   Sum(Invoice2.Amount) as Amount,
						   Invoice2.PerPieceCommission as PerPieceCommission,
						   IsNull(InvCode.PerPieceCommission,0) as  previousCommission,
						   Invoice1.SaleManCode,
						   Invoice2.Code as ItemCode
                           from Invoice1 
                           inner join invoice2 on Invoice1.InvNo=invoice2.InvNo
                           inner join InvCode on InvCode.Code=invoice2.Code
                          where Invoice1.SaleManCode = '" + salesmanCode + "' " +
                          "and Invoice1.CompID='" + CompID + "' " +
                          "and Invoice1.Date1>='" + StartDate + "' " +
                          "and Invoice1.Date1<='" + EndDate + @"'  


                            group by invoice1.InvNo,invoice1.Date1,Invoice2.Code,Invoice1.SaleManCode,invoice2.Description,Invoice2.ActualSellingPrice,Invoice2.Rate,InvCode.PerPieceCommission,Invoice2.PerPieceCommission 
						   order by invoice2.Description
                            ", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();

            



            for (int i = 0; i < dt.Rows.Count; i++)
            {

                var invoice = new InvoiceViewModel();
                var amount = Convert.ToDecimal(dt.Rows[i]["Amount"]);
                var qty = Convert.ToDecimal(dt.Rows[i]["Qty"]);
                var unitCost = Convert.ToDecimal(dt.Rows[i]["SellingCost"]);
                var isPerPieceCommissionNull = string.IsNullOrEmpty(dt.Rows[i]["PerPieceCommission"].ToString());
                var previousCommission = string.IsNullOrEmpty(dt.Rows[i]["previousCommission"].ToString()) ? 0 : Convert.ToDecimal(dt.Rows[i]["previousCommission"].ToString());
                var perPieceCommission = string.Empty;
                
                var qtySold = qty;
                var totalCost = qtySold * unitCost;
                var profit = amount - totalCost;
                

                decimal tenComm = 0;
                decimal totalComm = 0;
                if (profit > 0)
                {
                    tenComm = (profit / 100) * 10;
                }
                if (isPerPieceCommissionNull)
                {
                    if (profit > 0)
                    {
                        var commission = previousCommission;
                        totalComm = commission * qtySold;
                        if (commission != 0)
                        {
                            if (qtySold == 1)
                            {
                                perPieceCommission = "(" + commission.ToString("N2") + ")";

                            }
                            else
                            {

                                perPieceCommission = "(" + commission.ToString("N2") + " / " + totalComm.ToString("N2") + ")";
                            }
                        }
                        else
                        {
                            perPieceCommission = "0.00";
                        }

                    }
                    else
                    {
                        perPieceCommission = "0.00";
                    }
                }
                else
                {
                    if (profit > 0)
                    {
                        var commission = Convert.ToInt32(dt.Rows[i]["PerPieceCommission"].ToString());
                        totalComm = commission * qtySold;
                        if (commission != 0)
                        {
                            if (qtySold == 1)
                            {
                                perPieceCommission = commission.ToString("N2");

                            }
                            else
                            {
                                perPieceCommission = commission.ToString("N2") + " / " + totalComm.ToString("N2");
                            }
                        }
                        else
                        {
                            perPieceCommission = "0.00";
                        }
                    }
                    else
                    {
                        perPieceCommission = "0.00";
                    }

                }
                var cost = unitCost.ToString("N2");
                if (qtySold == 1)
                {
                    cost = unitCost.ToString("N2");

                }
                else
                {
                    cost = unitCost.ToString("N2") + " / " + totalCost.ToString("N2");
                }
                    invoice.Date = Convert.ToDateTime(dt.Rows[i]["Date1"]).ToString("dd/MM/yyyy") ;
                invoice.SaleInvNo = Convert.ToString(dt.Rows[i]["InvNo"]);
                invoice.Description = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                invoice.Quantity = Convert.ToString(qty);

                invoice.Cost = cost;
                invoice.TotalCost = totalCost.ToString();
                invoice.Amount = amount.ToString();
                invoice.Profit = profit.ToString();
                invoice.TenCommission = tenComm.ToString();
                invoice.PerPieceCommission = perPieceCommission;
                invoice.TotalPerPieceCommission = totalComm.ToString();
                invoice.NetCommission = (totalComm + tenComm).ToString();
                invoice.IsReturnedItem = false;

                model.Add(invoice);
            }

            var returnedItems = getReturnedItems(StartDate, EndDate, salesmanCode);

            model.AddRange(returnedItems.Calculated);
            model = model.OrderBy(a => a.Description).ToList();

            model.AddRange(returnedItems.NotCalculated);


            return model;



        }
        public static ReturnedInvoiceViewModel<InvoiceViewModel> getReturnedItems(DateTime StartDate, DateTime EndDate, string salesmanCode)
        {
            var returnedInvoiceViewModel = new ReturnedInvoiceViewModel<InvoiceViewModel>();
            SqlCommand itemReturnedCmd = new SqlCommand(@"
            SELECT 
	            SaleReturn1.Date1,
	            SaleReturn1.InvNo,
	            SaleReturn1.SalesMan,
                SaleReturn2.Code,
                SaleReturn2.Description,
                SaleReturn2.Rate,
                SaleReturn2.Qty,
                SaleReturn2.Amount,
                SaleReturn1.PartyCode

                FROM SaleReturn2
                join SaleReturn1 on SaleReturn2.InvNo = SaleReturn1.InvNo
                Where 
                SaleReturn1.Date1 >= '" + StartDate + @"' and
                SaleReturn1.Date1 <= '" + EndDate + @"' and
                SaleReturn1.SalesMan='"+ salesmanCode + @"'
            ", con);
            SqlDataAdapter adpt = new SqlDataAdapter(itemReturnedCmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);

            var returnedItems = new List<InvoiceViewModel>();


            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var Date = Convert.ToString(dt.Rows[i]["Date1"]).Trim();
                var Description = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                var InvNo = Convert.ToString(dt.Rows[i]["InvNo"]).Trim();
                var SalesManCode = Convert.ToString(dt.Rows[i]["SalesMan"]);
                var Code = Convert.ToString(dt.Rows[i]["Code"]);
                var Rate = Convert.ToString(dt.Rows[i]["Rate"]);
                var Qty = Convert.ToString(dt.Rows[i]["Qty"]);
                var Amount = Convert.ToString(dt.Rows[i]["Amount"]);
                var PartyCode = Convert.ToString(dt.Rows[i]["PartyCode"]);

                var returnedItem = new InvoiceViewModel
                {
                    Date = Date,
                    Code = Code,
                    SaleReturnInvNo = InvNo,
                    Quantity = Qty,
                    Rate = Rate,
                    Amount = Amount,
                    SalesManCode = SalesManCode,
                    Description = Description,
                    PartyCode = PartyCode

                };
                returnedItems.Add(returnedItem);
            }
            var calculatedModel = new List<InvoiceViewModel>();

            foreach (var item in returnedItems)
            {
                var itemSoldCMD = new SqlCommand(@"
                Select 
                    Top 1
                    Invoice1.InvNo,
                    Invoice1.SaleManCode,
                    Invoice2.Code,
                    Invoice2.Description,
                    Invoice2.Rate,
                    Invoice2.Qty,
					Invoice2.ActualSellingPrice as SellingCost ,
                    Invoice2.PerPieceCommission as PerPieceCommission,
				    IsNull(InvCode.PerPieceCommission,0) as  previousCommission,
                    Invoice2.Amount
                    
                From Invoice1
                join Invoice2 on Invoice2.InvNo=Invoice1.InvNo
                inner join InvCode on InvCode.Code=invoice2.Code
                Where 
           	        InvCode.Code=" + item.Code + @" and 
           	        SaleManCode=" + item.SalesManCode + @" and 
                    Rate ='" + item.Rate + @"'and 
                    PartyCode ='" + item.PartyCode + @"'
                Order By Date1 Desc
                ", con);
                SqlDataAdapter adpt2 = new SqlDataAdapter(itemSoldCMD);
                DataTable dt2 = new DataTable();
                adpt2.Fill(dt2);


                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    var invoice = new InvoiceViewModel();
                    var InvNo = dt2.Rows[i]["InvNo"].ToString();
                    var amount = Convert.ToDecimal(dt2.Rows[i]["Amount"]);
                    var returnAmount = Convert.ToDecimal(item.Amount);
                    var qty = Convert.ToDecimal(item.Quantity); //Return Qty
                    var returnQty = Convert.ToDecimal(item.Quantity);
                    var unitCost = Convert.ToDecimal(dt2.Rows[i]["SellingCost"]);
                    var isPerPieceCommissionNull = string.IsNullOrEmpty(dt2.Rows[i]["PerPieceCommission"].ToString());
                    var previousCommission = string.IsNullOrEmpty(dt2.Rows[i]["previousCommission"].ToString()) ? 0 : Convert.ToDecimal(dt2.Rows[i]["previousCommission"].ToString());
                    var perPieceCommission = string.Empty;

                    //var netqtySold = qty - returnQty;
                    var qtySold = qty;
                    var totalCost = qtySold * unitCost;
                    //var totalReturnedItemCost = returnQty * unitCost;
                    var profit = totalCost - returnAmount;
                    //var returnProfit = (returnAmount - totalReturnedItemCost);
                    //var netAmount = (amount - returnAmount);

                    //profit = profit - returnProfit;

                    decimal tenComm = 0;
                    decimal totalComm = 0;

                    if (profit < 0)
                    {
                        tenComm = (profit / 100) * 10;
                    }
                    if (isPerPieceCommissionNull)
                    {
                        if (profit < 0)
                        {
                            var commission = -previousCommission;
                            totalComm = commission * qtySold;
                            if (commission != 0)
                            {
                                if (qtySold == 1)
                                {
                                    perPieceCommission = "(" + commission.ToString("N2") + ")";

                                }
                                else
                                {

                                    perPieceCommission = "(" + commission.ToString("N2") + " / " + totalComm.ToString("N2") + ")";
                                }
                            }
                            else
                            {
                                perPieceCommission = "0.00";
                            }

                        }
                        else
                        {
                            perPieceCommission = "0.00";
                        }
                    }
                    else
                    {
                        if (profit < 0)
                        {
                            var commission = -Convert.ToInt32(dt2.Rows[i]["PerPieceCommission"].ToString());
                            totalComm = commission * qtySold;
                            if (commission != 0)
                            {
                                if (qtySold == 1)
                                {
                                    perPieceCommission = commission.ToString("N2");

                                }
                                else
                                {
                                    perPieceCommission = commission.ToString("N2") + " / " + totalComm.ToString("N2");
                                }
                            }
                            else
                            {
                                perPieceCommission = "0.00";
                            }
                        }
                        else
                        {
                            perPieceCommission = "0.00";
                        }

                    }
                    var cost = unitCost.ToString("N2");
                    if (qtySold == 1)
                    {
                        cost = unitCost.ToString("N2");

                    }
                    else
                    {
                        cost = unitCost.ToString("N2") + " / " + totalCost.ToString("N2");
                    }
                    invoice.Date = Convert.ToDateTime(item.Date).ToString("dd/MM/yyyy") ;
                    invoice.Description = Convert.ToString(dt2.Rows[i]["Description"]).Trim() ;
                    invoice.Quantity = Convert.ToString(-qty);

                    invoice.Code = Convert.ToString(dt2.Rows[i]["Code"]);
                    invoice.Cost = (-Convert.ToDecimal(cost)).ToString();
                    invoice.TotalCost = (-totalCost).ToString();
                    invoice.Amount = (-returnAmount).ToString();
                    invoice.Profit = profit.ToString();
                    invoice.TenCommission = tenComm.ToString();
                    invoice.PerPieceCommission =perPieceCommission ;
                    invoice.TotalPerPieceCommission = totalComm.ToString();
                    invoice.NetCommission = (totalComm + tenComm).ToString();
                    invoice.IsReturnedItem = true;
                    invoice.SaleReturnInvNo = item.SaleReturnInvNo;
                    invoice.SaleInvNo = InvNo;


                    calculatedModel.Add(invoice);
                }
            }

            returnedInvoiceViewModel.Calculated = calculatedModel;

            var calculatedCodes = calculatedModel.Select(a => a.Code);

            var notCalculatedModel = new List<InvoiceViewModel>();

            var unCalculated = returnedItems.Where(a=>!calculatedCodes.Contains( a.Code));
            foreach (var item in unCalculated)
            {
                    var invoice = new InvoiceViewModel();
                    
                    invoice.Date = Convert.ToDateTime(item.Date).ToString("dd/MM/yyyy") ;
                    invoice.Description = item.Description;
                invoice.NetQtySold = "0.00";
                    invoice.Quantity =  "0.00";
                    invoice.ReturnQty = (-Convert.ToInt32(item.Quantity)).ToString();

                    invoice.Cost = "0.00";
                    invoice.TotalCost = "0.00";
                    invoice.Amount = "0.00";
                    invoice.ReturnAmount = item.Amount;
                    invoice.NetAmountSold = item.Amount;
                    invoice.Profit = "0.00";
                    invoice.TenCommission = "0.00";
                    invoice.PerPieceCommission = "0.00";
                    invoice.TotalPerPieceCommission = "0.00";
                    invoice.NetCommission = "0.00";
                    invoice.IsReturnedItem = true;
                    invoice.IsUnassignedReturnedItem = true;
                    invoice.SaleReturnInvNo = item.SaleReturnInvNo;


                notCalculatedModel.Add(invoice);
                
            }
            returnedInvoiceViewModel.NotCalculated = notCalculatedModel;
            return returnedInvoiceViewModel;

        }
        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            //var partyCode = PartyCode.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/PartyWiseItemSummaryCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&partyCode=" + partyCode);
        }
    }
}