﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebPOS.Reports.SaleReports.CReports
{
    public partial class PartyWiseItemSummaryCR : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {
            var cr = new PartyWiseItemSummary();
            var StartDate = getDate(Request.QueryString["startDate"]);
            var EndDate = getDate(Request.QueryString["endDate"]);
            var partyCode = Request.QueryString["partyCode"];

            SqlCommand cmd = new SqlCommand(@"SELECT
              
                             Invoice2.Description as Description,
                             Sum(Invoice2.qty ) as qty,
                             Sum(Invoice2.Amount ) as Amount
                       

                           from Invoice2
                           inner join invoice1 on Invoice2.InvNo=Invoice1.InvNo
                           where Invoice1.PartyCode = '" + partyCode + "' and Invoice1.CompID='" + CompID + "' and Invoice1.Date1>='" + StartDate + "' and Invoice1.Date1<='" + EndDate + "' GROUP BY Invoice2.Description  order by Invoice2.Description", con);

            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State==ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var dt = objDs.Tables[0];
            var dset = new DataSet();

            var model = new List<Object>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new
                {
                    Description = Convert.ToString(dt.Rows[i]["Description"]),
                    Quantity = Math.Round(Convert.ToDecimal(dt.Rows[i]["qty"]), 2),
                    Amount = Math.Round(Convert.ToDecimal(dt.Rows[i]["Amount"]), 2)
                };
                model.Add(invoice);
            }

            cr.SetDataSource(model);
            var InstalledPrinters = PrinterSettings.InstalledPrinters.Count;
            //if (InstalledPrinters > 4)
            //var a= cr.PrintOptions.PrinterName.ToString();
            //  cr.PrintToPrinter(1, false, 0, 0);
            //else
            CRViewer.ReportSource = cr;

        }

        public DateTime getDate(string date)
        {
            var newdate = DateTime.ParseExact(date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);
            return newdate;
        }
    }
}