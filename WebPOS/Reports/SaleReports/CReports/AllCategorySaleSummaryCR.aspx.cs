﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebPOS.Reports.SaleReports.CReports
{
    public partial class AllCategorySaleSummaryCR : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {
            var cr = new AllCategorySaleSummary();
            var StartDate = getDate(Request.QueryString["startDate"]);
            var EndDate = getDate(Request.QueryString["endDate"]);

            SqlCommand cmd = new SqlCommand(@"  Select   Category.Name,sum(Invoice2.Qty) as Qty, Sum(Invoice2.Amount) as Amount from (Invoice2 inner join invoice1 on invoice2.InvNo=invoice1.InvNo ) inner join Category on invoice2.CategoryCode=Category.id where  Invoice1.CompID='" + CompID + "' and Invoice2.ItemNature=1 and Invoice1.Date1>='" + StartDate + "' and invoice1.Date1<='" + EndDate + "' group by Category.Name order by Category.Name ", con);

            DataTable dt = new DataTable();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State == ConnectionState.Closed)
            {
                if (con.State==ConnectionState.Closed) { con.Open(); }
            }

            dAdapter.Fill(dt);
            con.Close();

            var dset = new DataSet();

            var model = new List<Object>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new
                {
                    SNo = i + 1,
                    CategoryName = Convert.ToString(dt.Rows[i]["Name"]),
                    Quantity = Math.Round(Convert.ToDecimal(dt.Rows[i]["qty"]), 2),
                    Amount = Math.Round(Convert.ToDecimal(dt.Rows[i]["Amount"]), 2)
                };
                model.Add(invoice);
            }

            cr.SetDataSource(model);
            var InstalledPrinters = PrinterSettings.InstalledPrinters.Count;
            //if (InstalledPrinters > 4)
            //var a= cr.PrintOptions.PrinterName.ToString();
            //  cr.PrintToPrinter(1, false, 0, 0);
            //else
            CRViewer.ReportSource = cr;

        }

        public DateTime getDate(string date)
        {
            var newdate = DateTime.ParseExact(date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);
            return newdate;
        }
    }
}