﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebPOS.Reports.SaleReports.CReports
{
    public partial class DaySaleGroupByItem2CR : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {
            var cr = new DaySaleGroupByItem2();
            var StartDate =getDate( Request.QueryString["startDate"]); 
            var EndDate =getDate( Request.QueryString["endDate"]);

            SqlCommand cmd = new SqlCommand(@"SELECT
                Invoice2.Code as Code , 
                Invoice2.Description as Description , 
                Sum(Invoice2.Qty) as Qty1 ,
                Sum(Invoice2.Amount) as Amount,

				Isnull((Select Sum(SaleReturn2.Qty) from SaleReturn2
				where Date1>='" + StartDate + "'  and Date1 <= '" + EndDate + "' and  SaleReturn2.Code=Invoice2.Code and SaleReturn2.Description=Invoice2.Description                group by SaleReturn2.Code,SaleReturn2.Description				),0) as ReturnQty,				Isnull((Select Sum(SaleReturn2.Amount) from SaleReturn2                where Date1 >= '" + StartDate + "'  and Date1 <= '" + EndDate + "' and  SaleReturn2.Code=Invoice2.Code and SaleReturn2.Description=Invoice2.Description				group by SaleReturn2.Code,SaleReturn2.Description				),0) as ReturnAmount,				(Sum(Invoice2.Qty)-Isnull((Select Sum(SaleReturn2.Qty) from SaleReturn2                where Date1 >= '" + StartDate + "'  and Date1 <= '" + EndDate + "' and  SaleReturn2.Code=Invoice2.Code and SaleReturn2.Description=Invoice2.Description				group by SaleReturn2.Code,SaleReturn2.Description				),0)) as NetQtySold				,				(Sum(Invoice2.Amount)-Isnull((Select Sum(SaleReturn2.Amount) from SaleReturn2                where Date1 >= '" + StartDate + "'  and Date1 <= '" + EndDate + "' and  SaleReturn2.Code=Invoice2.Code and SaleReturn2.Description=Invoice2.Description				group by SaleReturn2.Code,SaleReturn2.Description				),0)) as NetAmountSold,				Isnull((Select Sum(InvCode.qty) from InvCode				where InvCode.Code=Invoice2.Code and InvCode.Description=Invoice2.Description				),0) as InHandQty                               FROM Invoice1                 INNER JOIN invoice2 ON Invoice1.InvNo = Invoice2.InvNo                where Invoice1.CompID='" + CompID + "' and   Invoice2.ItemNature = 1  and Invoice1.Date1>='" + StartDate + "' and Invoice1.Date1<='" + EndDate + "' GROUP BY Invoice2.Code,Invoice2.Description                  order by Invoice2.Description ", con);

            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State==ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var dt = objDs.Tables[0];
            var dset = new DataSet();
            
            var model = new List<Object>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new
                {

                ItemCode = (dt.Rows[i]["Code"]).ToString(),
                Description = Convert.ToString(dt.Rows[i]["Description"]),
                Quantity = Math.Round(Convert.ToDecimal(dt.Rows[i]["Qty1"]),2),
                Amount = Math.Round(Convert.ToDecimal(dt.Rows[i]["Amount"]),2),
                ReturnQty = Math.Round(Convert.ToDecimal(dt.Rows[i]["ReturnQty"]),2),
                ReturnAmount = Math.Round(Convert.ToDecimal(dt.Rows[i]["ReturnAmount"]),2),
                NetQtySold = Math.Round(Convert.ToDecimal(dt.Rows[i]["NetQtySold"]),2),
                NetAmountSold = Math.Round(Convert.ToDecimal(dt.Rows[i]["NetAmountSold"]),2),
                InHandQty = Math.Round(Convert.ToDecimal(dt.Rows[i]["InHandQty"]),2)
                    
                   
                };
                model.Add(invoice);
            }

            cr.SetDataSource(model);
            var InstalledPrinters = PrinterSettings.InstalledPrinters.Count;
            //if (InstalledPrinters > 4)
            //var a= cr.PrintOptions.PrinterName.ToString();
            //  cr.PrintToPrinter(1, false, 0, 0);
            //else
            CRViewer.ReportSource = cr;

        }

        public DateTime getDate(string date)
        {
            var newdate = DateTime.ParseExact(date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);
            return newdate;
        }
    }
}