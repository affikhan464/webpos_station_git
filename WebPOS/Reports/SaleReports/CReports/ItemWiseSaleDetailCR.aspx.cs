﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebPOS.Reports.SaleReports.CReports
{
    public partial class ItemWiseSaleDetailCR : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {
            var cr = new ItemWiseSaleDetailCrystalReport();
            var StartDate =getDate( Request.QueryString["startDate"]); 
            var EndDate =getDate( Request.QueryString["endDate"]);
            var partyCode = Request.QueryString["partyCode"];
            var itemCode = Request.QueryString["itemCode"];

            SqlCommand cmd = new SqlCommand(@" Select 
                          Invoice1.InvNo as Bill, 
                          Invoice1.Date1 as date,
                          Invoice1.Name,
                          Sum(Invoice2.Qty) as qty,
                          Sum(Invoice2.Rate ) as Rate,
                          Sum(Invoice2.Amount ) as Amount
                          
                          from Invoice1 
                          inner join invoice3 on Invoice1.InvNo=Invoice3.InvNo
                          inner join invoice2 on Invoice1.InvNo=Invoice2.InvNo

                           where Invoice2.Code = '" + itemCode + "' and Invoice1.CompID='" + CompID + "' and Invoice1.Date1>='" + StartDate + "' and Invoice1.Date1<='" + EndDate + "' GROUP BY Invoice1.InvNo,Invoice1.Date1,Invoice1.Name  order by Invoice1.InvNo     ", con);

            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State==ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var dt = objDs.Tables[0];
            var dset = new DataSet();
            
            var model = new List<Object>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new
                {
                   InvoiceNumber = Convert.ToString(dt.Rows[i]["Bill"]),
                   Date = Convert.ToDateTime(dt.Rows[i]["date"]).ToString("dd/MMM/yyyy"),
                   PartyName = Convert.ToString(dt.Rows[i]["Name"]),
                   Quantity = Math.Round(Convert.ToDecimal(dt.Rows[i]["qty"]), 2),
                   Rate = Math.Round(Convert.ToDecimal(dt.Rows[i]["Rate"]), 2),
                   Amount = Math.Round(Convert.ToDecimal(dt.Rows[i]["Amount"]), 2)
                };
                model.Add(invoice);
            }

            cr.SetDataSource(model);
            var InstalledPrinters = PrinterSettings.InstalledPrinters.Count;
            //if (InstalledPrinters > 4)
            //var a= cr.PrintOptions.PrinterName.ToString();
            //  cr.PrintToPrinter(1, false, 0, 0);
            //else
            CRViewer.ReportSource = cr;

        }

        public DateTime getDate(string date)
        {
            var newdate = DateTime.ParseExact(date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);
            return newdate;
        }
    }
}