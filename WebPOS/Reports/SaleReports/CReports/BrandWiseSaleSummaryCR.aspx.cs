﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebPOS.Reports.SaleReports.CReports
{
    public partial class BrandWiseSaleSummaryCR : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {
            var cr = new BrandWiseSaleSummary();
            var StartDate = getDate(Request.QueryString["startDate"]);
            var EndDate = getDate(Request.QueryString["endDate"]);
            var BrandID = Request.QueryString["brandCode"];

            SqlCommand cmd = new SqlCommand(@"SELECT
              
                Sum(invoice2.Amount) as Amount,

				 (   Select Sum(SaleReturn2.Amount)
                    from SaleReturn2
                    where SaleReturn2.BrandCode='" + BrandID + "'    group by SaleReturn2.BrandCode)as ReturnAmount, (   Select Sum(SaleReturn2.Qty)from SaleReturn2 where SaleReturn2.BrandCode='" + BrandID + "'   group by SaleReturn2.BrandCode) as ReturnQty, Sum(invoice2.Qty) as Qty, BrandNameNew.Name FROM Invoice1         INNER JOIN invoice2 as invoice2 ON Invoice1.InvNo = invoice2.InvNo  INNER JOIN BrandName  as BrandNameNew ON invoice2.BrandName = BrandNameNew.Code     where Invoice1.CompID = '" + CompID + "' and BrandNameNew.Code='" + BrandID + "' and   Invoice2.ItemNature = 1  and Invoice1.Date1 >= '" + StartDate + "'  and Invoice1.Date1 <= '" + EndDate + "'   GROUP BY  BrandNameNew.Name ", con);


            DataSet objDs = new DataSet();
            SqlDataAdapter dAdapter = new SqlDataAdapter();
            dAdapter.SelectCommand = cmd;
            if (con.State==ConnectionState.Closed) { con.Open(); }
            dAdapter.Fill(objDs);
            con.Close();
            var dt = objDs.Tables[0];
            var dset = new DataSet();
            
            var model = new List<Object>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new
                {

                    BrandName = Convert.ToString(dt.Rows[i]["Name"]),
                    Quantity = Math.Round(Convert.ToDecimal(dt.Rows[i]["Qty"]), 2),
                    ReturnQty = Math.Round(Convert.ToDecimal(dt.Rows[i]["ReturnQty"]), 2),
                    ReturnAmount = Math.Round(Convert.ToDecimal(dt.Rows[i]["ReturnAmount"]), 2),
                    Amount = Math.Round(Convert.ToDecimal(dt.Rows[i]["Amount"]), 2),
                };
                model.Add(invoice);
            }

            cr.SetDataSource(model);
            var InstalledPrinters = PrinterSettings.InstalledPrinters.Count;
            //if (InstalledPrinters > 4)
           //var a= cr.PrintOptions.PrinterName.ToString();
              //  cr.PrintToPrinter(1, false, 0, 0);
            //else
               BrandWiseSaleSummaryCRViewer.ReportSource = cr;

        }
        public DateTime getDate(string date)
        {
            var newdate = DateTime.ParseExact(date + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                     System.Globalization.CultureInfo.InvariantCulture);
            return newdate;
        }
    }
}