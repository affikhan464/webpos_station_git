﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class DayCashAndCreditSaleWithExpense : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, int stationId)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, stationId);
                return new BaseModel { Success = true, SaleAndExpense = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static SaleAndExpenseViewModel GetTheReport(DateTime StartDate, DateTime EndDate, int stationId)
        {
            var saleAndExpenseViewModel = new SaleAndExpenseViewModel();
            var stationWhereClause = string.Empty;
            var expenseStationWhereClause = string.Empty;
            if (stationId != 0)
            {
                stationWhereClause = "  and Invoice1.StationId=" + stationId;
                expenseStationWhereClause = " and StationId=" + stationId;
            }

            var expenseCommandText = "Select V_No,dateDr,AmountCr,Code1,Narration,StationId,Station.Name as StationName,GLCode.Title from GeneralLedger join Station on Station.Id=GeneralLedger.StationId join GLCode on GeneralLedger.Code1=GLCode.Code where GeneralLedger.CompID='" + CompID + "' and V_Type='CPV'  and GeneralLedger.Code='" + "0101010100001" + "' and SecondDescription='OperationalExpencesThroughCash' and dateDr>='" + StartDate + "' and dateDr<='" + EndDate + "' " + expenseStationWhereClause + " order by dateDr desc";
            SqlCommand cmd = new SqlCommand(
                "Select " +
                "Invoice1.Date1 as [Date],Invoice1.InvNo as Invoice , Invoice1.Name as Name,Invoice3.Total as Total," +
                    "Invoice3.Discount1 as Discount,Invoice3.CashPaid as Paid,Invoice3.Total-(Invoice3.Discount1+Invoice3.CashPaid) as Balance " +
                "from (Invoice1 inner join invoice3 on invoice1.InvNo=invoice3.InvNo) " +
                "where Invoice1.CompID='" + CompID + "' and  Invoice1.Date1>='" + StartDate + "' and Invoice1.Date1<='" + EndDate + "' " + stationWhereClause +
                " order by Invoice1.InvNo; " +
                expenseCommandText + ";", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            adpt.Fill(ds);
            var saleDt = ds.Tables[0];
            var expensesDt = ds.Tables[1];

            //Sales
            var sales = new List<InvoiceViewModel>();
            var totalSaleAmount = 0.00;
            for (int i = 0; i < saleDt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.Date = Convert.ToDateTime(saleDt.Rows[i]["Date"]).ToString("dd/MM/yyyy");
                invoice.InvoiceNumber = Convert.ToString(saleDt.Rows[i]["Invoice"]);
                invoice.PartyName = Convert.ToString(saleDt.Rows[i]["Name"]) == "" ? "No Party Name Available" : Convert.ToString(saleDt.Rows[i]["Name"]);
                invoice.NetDiscount = Convert.ToString(saleDt.Rows[i]["Discount"]);
                invoice.Total = Convert.ToString(saleDt.Rows[i]["Total"]);
                invoice.Paid = Convert.ToString(saleDt.Rows[i]["Paid"]);
                invoice.Balance = Convert.ToString(saleDt.Rows[i]["Balance"]);
                sales.Add(invoice);

                totalSaleAmount += string.IsNullOrEmpty(invoice.Total) ? 0 : Convert.ToDouble(invoice.Total);
            }

            //Expenses
            var expenses = new List<InvoiceViewModel>();
            var totalExpenseAmount = 0.00;
            for (int i = 0; i < expensesDt.Rows.Count; i++)
            {

                string Date = Convert.ToDateTime(expensesDt.Rows[i]["dateDr"].ToString()).ToString("dd/MM/yyyy");
                string CashPaid = expensesDt.Rows[i]["AmountCr"].ToString();
                string V_No = expensesDt.Rows[i]["V_No"].ToString();
                string Narration = expensesDt.Rows[i]["Narration"].ToString();
                string StationId = expensesDt.Rows[i]["StationId"].ToString();
                string StationName = expensesDt.Rows[i]["StationName"].ToString();
                string Title = expensesDt.Rows[i]["Title"].ToString();

                var invoice = new InvoiceViewModel();

                invoice.VoucherNo = V_No;
                        invoice.Date = Date;
                        invoice.Paid = CashPaid;
                        invoice.Narration = Narration;
                        invoice.StationId = StationId;
                        invoice.StationName = StationName;
                        invoice.ExpenseHead = Title;

                expenses.Add(invoice);

                totalExpenseAmount += string.IsNullOrEmpty(invoice.Paid) ? 0 : Convert.ToDouble(invoice.Paid);
            }

            saleAndExpenseViewModel.TotalSaleAmount = totalSaleAmount.ToString();
            saleAndExpenseViewModel.TotalExpenseAmount = totalExpenseAmount.ToString();
            saleAndExpenseViewModel.Sales = sales;
            saleAndExpenseViewModel.Expenses = expenses;
            return saleAndExpenseViewModel;
        }

        protected void BtnClick(object sender, EventArgs e)
        {

            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}