﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.SaleReports
{
    public partial class Color_ColorAndAllPartySaleSummary : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string ColorID)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, ColorID);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, string ColorID)
        {

            ////     SqlCommand cmd = new SqlCommand(@"
            ////                    Select 
            ////         PartyCode.Name as PartyName , 
            ////         Sum(Invoice2.Qty) as Qty ,
            ////         Sum(Invoice2.Amount) as Amount

            ////                 (ISNULL((
            ////                 Select
            ////                     Sum(SaleReturn2.Amount)
            ////                     from SaleReturn2
            ////                     inner join SaleReturn1 on SaleReturn2.InvNo=SaleReturn1.InvNo 
            ////inner join InvCode as item on SaleReturn2.Code=item.Code
            ////                     inner join PartyCode as party on SaleReturn1.PartyCode=party.Code 
            ////                     where 
            ////                     party.Code=PartyCode.Code
            ////                     and  item.Brand='" + brandCode + @"'

            ////and SaleReturn1.Date1>='" + StartDate + @"' 
            ////and SaleReturn1.Date1<='" + EndDate + @"'
            ////                     group by party.Code),0))
            ////                 as ReturnAmount,

            ////                 (ISNULL((Select Sum(SaleReturn2.Qty) 
            ////                  from SaleReturn2
            ////                     inner join SaleReturn1 on SaleReturn2.InvNo=SaleReturn1.InvNo 
            ////inner join InvCode as item on SaleReturn2.Code=item.Code
            ////                     inner join PartyCode as party on SaleReturn1.PartyCode=party.Code 
            ////                     where 
            ////                     party.Code=PartyCode.Code
            ////                     and  item.Brand='" + brandCode + @"'

            ////and SaleReturn1.Date1>='" + StartDate + @"' 
            ////and SaleReturn1.Date1<='" + EndDate + @"'
            ////                     group by party.Code),0))
            ////         as ReturnQty 


            ////         From Invoice1
            ////         inner join invoice2 on Invoice1.InvNo= Invoice2.InvNo
            ////         inner join InvCode on Invoice2.Code=InvCode.Code 
            ////         inner join PartyCode on Invoice1.PartyCode=PartyCode.Code 

            ////         where InvCode.Brand= '" + brandCode + @"' 
            ////         and Invoice1.CompID='" + CompID + @"' 
            ////         and   Invoice2.ItemNature = 1  
            ////         and Invoice1.Date1>='" + StartDate + @"' 
            ////         and Invoice1.Date1<='" + EndDate + @"' 
            ////         GROUP BY PartyCode.Name,PartyCode.Code                  
            ////         order by PartyCode.Name     ", con);


            SqlCommand cmd = new SqlCommand(@"select A.PartyCode, A.Name, A.Color,
                                                    sum(A.sq) as [SaleQty],
                                                    sum(A.sa) as [SaleAmount],
                                                    sum(A.srq) as [SaleReturnQty],
                                                    sum(A.sra) as [SaleReturnAmount]


                                  FROM(SELECT Inv1.PartyCode, Inv1.Name,  invcode.Color, Inv2.QTY[sq], Inv2.amount[sa], 0 as [srq], 0 as [sra]
                                  FROM invoice1 as Inv1 inner JOIN invoice2 Inv2 ON Inv1.invno = Inv2.invno
                                           inner join invcode on inv2.code = invcode.code
                                           inner join Color on invcode.Color = Color.ID

                                                        where
                                                            Inv1.date1 >= '" + StartDate + @"' 
                                                            and Inv1.date1 <= '" + EndDate + @"'
                                                            and invcode.Color = '" + ColorID + @"' 
                                                        UNION ALL

                                         SELECT SaleReturn1.PartyCode, SaleReturn1.Name,invcode.Color, 0 as [sq], 0 as [sa], SR2.QTY[srq], SR2.amount as [sra]
                                         FROM salereturn2 SR2 inner join SaleReturn1 on SR2.invno = salereturn1.invno
                                         inner join invcode on SR2.code = invcode.code
                                         inner join Color on invcode.Color = Color.ID

                                                        WHERE

                                                            SR2.date1 >= '" + StartDate + @"' 
                                                            and SR2.date1 <= '" + EndDate + @"'
                                                            and invcode.Color = '" + ColorID + @"'

                                                            ) AS A JOIN invcode InvC ON InvC.code = A.Color

                                                              GROUP BY A.PartyCode, A.Name, A.Color ORDER BY A.Name ", con);





            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();
                var Quantity = Convert.ToInt32(dt.Rows[i]["SaleQty"]);
                var ReturnQty = Convert.ToInt32(dt.Rows[i]["SaleReturnQty"]);
                var ReturnAmount =  Convert.ToInt32(dt.Rows[i]["SaleReturnAmount"]);
                var Amount = Convert.ToInt32(dt.Rows[i]["SaleAmount"]);


                invoice.PartyName = Convert.ToString(dt.Rows[i]["Name"]);
                invoice.Quantity = Quantity.ToString();
                invoice.ReturnQty =  ReturnQty.ToString();
                invoice.ReturnAmount =  ReturnAmount.ToString();
                invoice.Amount = Amount.ToString();
                invoice.NetQtySold = (Quantity - ReturnQty).ToString();
                invoice.NetAmountSold = (Amount - ReturnAmount).ToString();
                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {

            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            var itemCode = "";//brandCodeTxbx.Text;
                              // Response.Redirect("~/Reports/SaleReports/CReports/ItemAndPartyWiseSaleDetailCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&partyCode=" + partyCode+ "&itemCode=" + itemCode);
        }
    }
}