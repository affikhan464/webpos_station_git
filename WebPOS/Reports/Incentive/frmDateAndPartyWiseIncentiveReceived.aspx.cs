﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebPOS.Reports.Incentive
{
    public partial class frmDateAndPartyWiseIncentiveReceived : System.Web.UI.Page
    {
        static string CompID = "01";
        string CashAccountGLCode = CompID + "01010100001";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    txtStartDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                    txtEndDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                    LoadPartyList();
                }
                catch (Exception ex)
                {
                    Response.Write("error  = " + ex.Message);

                }

            }
        }



        void LoadReport(DateTime StartDate, DateTime EndDate)
        {


            string IncentiveIncomeGLCode = CompID + "03050100001";
            //string CashAccountGLCode = "0101010100001";
            ModGLCode objModGLCode = new ModGLCode();
            string PartyCode = objModGLCode.GLCodeAgainstGLTitle(lstParty.Text);
            SqlCommand cmd = new SqlCommand("Select DateDr,AmountCr,VenderCode,V_No,Narration from GeneralLedger where compid='" + CompID + "' and  code='" + IncentiveIncomeGLCode + "' and AmountCr>0 and  VenderCode='" + PartyCode + "' and DescriptionOfBillNo='IncentiveIncome' and DateDr>='" + StartDate + "' and dateDr<='" + EndDate + "' order by DateDr", con);


            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);

            GridView1.DataSource = dset;
            GridView1.DataBind();

        }






        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                LoadReport(Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
            }
            catch (Exception ex)
            {
                Response.Write("error  = " + ex.Message);

            }
        }


        decimal TotAmo = 0;

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // add the UnitPrice and QuantityTotal to the running total variables
                TotAmo += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "AmountCr"));

            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";
                // for the Footer, display the running totals
                e.Row.Cells[5].Text = TotAmo.ToString();

                e.Row.Cells[5].HorizontalAlign = HorizontalAlign.Right;

                e.Row.Font.Bold = true;
            }
        }

        public string GLNameAgainstGLCodeLocal(string GLCode)
        {
            ModGLCode objModGLCode = new ModGLCode();
            string GLName = "";

            GLName = objModGLCode.GLTitleAgainstGLCode(GLCode);
            return GLName;

        }
        public decimal GLCurrentBalanceLocal(string GLCode)
        {

            decimal GLClosingBalance = 100;
            ModGLCode objModGLCode = new ModGLCode();

            GLClosingBalance = objModGLCode.GLClosingBalance(GLCode, 1, Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
            return GLClosingBalance;

        }
        void LoadPartyList()
        {

            SqlCommand cmd = new SqlCommand("select Name from PartyCode order by Name", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);
            lstParty.DataSource = dset;
            lstParty.DataBind();
            lstParty.DataTextField = "Name";
            lstParty.DataBind();

        }
        


    }
}