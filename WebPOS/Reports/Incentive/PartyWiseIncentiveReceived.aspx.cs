﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.Incentive
{
    public partial class PartyWiseIncentiveReceived : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string partyCode)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate, partyCode);
                return new BaseModel { Success = true, ListofInvoices=model};

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false,Message=ex.Message};
            }
        }

        public static  List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, string PartyCode)
        {
            

            ModGL objModGL = new ModGL();
            string IncentiveIncomeGLCode = "01"+objModGL.GLCodeAgainstGLLockName("IncentiveIncome");
            SqlCommand cmd = new SqlCommand(@"Select 
                                                        DateDr,
                                                        AmountCr,
                                                        VenderCode,
                                                        V_No,
                                                        Narration,
                                                        PartyCode.Name 
                                                from 
                                                        GeneralLedger 
                                                        inner join partycode on partycode.code= GeneralLedger.VenderCode
                                                where 
                                                        GeneralLedger.code='" + IncentiveIncomeGLCode + @"' and 
                                                        GeneralLedger.CompId='" + CompID + @"' and 
                                                         GeneralLedger.VenderCode='" + PartyCode + @"' and 
                                                        AmountCr>0 and  
                                                        DescriptionOfBillNo='IncentiveIncome' and 
                                                        DateDr>='" + StartDate + @"' and 
                                                        dateDr<='" + EndDate + @"' 
                                                        order by DateDr", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.Date = Convert.ToDateTime((dt.Rows[i]["DateDr"])).ToString("dd-MMM-yyyy");
                invoice.PartyName = Convert.ToString(dt.Rows[i]["Name"]);
                invoice.Description = Convert.ToString(dt.Rows[i]["Narration"]);
                invoice.VoucherNo = Convert.ToString(dt.Rows[i]["V_No"]);
                invoice.Amount = Convert.ToString(dt.Rows[i]["AmountCr"]);

                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            //var partyCode = PartyCode.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/PartyWiseItemSummaryCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&partyCode=" + partyCode);
        }
    }
}