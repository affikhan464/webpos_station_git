﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="frmDateWiseIncentiveReceived.aspx.cs" Inherits="WebPOS.Reports.Incentive.frmDateWiseIncentiveReceived" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="<%= ResolveUrl("~/css/style.css") %>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>" rel="stylesheet"  />
    <link href="<%= ResolveUrl("~/css/reset.css") %>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>" rel="stylesheet"  />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div>
        <br /> <h2 class="heading" >Date Wise Incentive Received</h2><br />
</div>
<div style ="width:100%;height:100%;text-align:center; vertical-align:middle">
   
    <table  border="0" style="width:100%">
        <tr  style="background-color:aliceblue"  >
            <td >
                <asp:Label ID="Label2" runat="server" Text="Start Date"></asp:Label>
                <asp:TextBox ID="txtStartDate"  CssClass="datetimepicker"   runat="server"></asp:TextBox>
                <asp:Label ID="Label3" runat="server" Text="End Date"></asp:Label>
                <asp:TextBox ID="txtEndDate"  CssClass="datetimepicker"   runat="server"></asp:TextBox>
                
                <asp:Button ID="Button1" runat="server" Text="Report" OnClick="Button1_Click"  />
          </td>  
      </tr>
    </table>
<br />
</div>
<div>

        <asp:GridView CssClass="grid" ID="GridView1" AutoGenerateColumns="False" HeaderStyle-BackColor ="WhiteSmoke"   AlternatingRowStyle-BackColor ="WhiteSmoke"
      runat="server" OnRowDataBound="GridView1_RowDataBound" ShowFooter="True"  FooterStyle-BackColor="Wheat" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical">
<AlternatingRowStyle BackColor="#DCDCDC"></AlternatingRowStyle>
     
            <Columns>
        <asp:TemplateField HeaderText="S. #"> 
               <ItemTemplate> <span style="text-align:left"> <asp:Label ID="lblSNO" runat="server"  Text='<%# Convert.ToInt32( Container.DataItemIndex+1) %>'></asp:Label>
               </span> 
          </ItemTemplate>
               <ItemStyle HorizontalAlign="Center" />
          </asp:TemplateField>
          
        
                <asp:BoundField HeaderText="Date" DataField="DateDr" DataFormatString="{0:d MMM yyyy}" ReadOnly="true" >
          <ItemStyle HorizontalAlign="Center" />
          </asp:BoundField>

                



                <asp:TemplateField HeaderText="Party Name"> 
               <ItemTemplate> <span style="text-align:left"> <asp:Label ID="lblGLTitle" runat="server"  Text='<%# GLNameAgainstGLCodeLocal(Convert.ToString( Eval("VenderCode"))) %>'></asp:Label>
               </span> 
          </ItemTemplate>
               <ItemStyle HorizontalAlign="Left" />
          </asp:TemplateField>
          
          <asp:BoundField HeaderText="Narration" DataField="Narration" ReadOnly="true" >
          <ItemStyle HorizontalAlign="Left" />
          </asp:BoundField>

        <asp:BoundField HeaderText="Voucher #" DataField="V_No" ReadOnly="true" >
          <ItemStyle HorizontalAlign="Center" />
          </asp:BoundField>


                <asp:BoundField HeaderText="Amount" DataField="AmountCr" ReadOnly="true" >
          <ItemStyle HorizontalAlign="Right" />
          </asp:BoundField>
          
        
          

      </Columns>

<FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>

<HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White"></HeaderStyle>
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#0000A9" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#000065" />
    </asp:GridView>

    </div>

</asp:Content>
