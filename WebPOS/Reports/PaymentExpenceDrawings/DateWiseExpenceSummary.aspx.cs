﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.PaymentExpenceDrawings
{
    public partial class DateWiseExpenceSummary : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string BankCode)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(BankCode, fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }


        public static List<InvoiceViewModel> GetTheReport(String BankCode, DateTime StartDate, DateTime EndDate)
        {

            var model = new List<InvoiceViewModel>();


            ModItem objModItem = new ModItem();
            //int BrandID = Convert.ToInt32(brandCode);
           
            //////     

            SqlCommand cmd = new SqlCommand(@"Select  GLCode.Title as Title , sum(AmountDr) as AmountDr
                                                from 
                                                        GeneralLedger inner join GLCode on GeneralLedger.Code=GLCode.Code
                                                where  
                                                        GeneralLedger.CompID='" + CompID + @"' and 
                                                        GeneralLedger.Code like '" + CompID + "0402%" + @"' and 
                                                        datedr>='" + StartDate + @"' and 
                                                        datedr<='" + EndDate + @"' 
                                                        group by GLCode.Title order by GLCode.Title ", con);


            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            var dt = new DataTable();
            adpt.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();
                //Convert.ToDateTime(Date).ToString("dd-MMM-yyyy");
                //invoice.Date = Convert.ToDateTime(dt.Rows[i]["dateDr"]).ToString("dd-MMM-yyyy");
                invoice.Particular=Convert.ToString(dt.Rows[i]["Title"]);
                //invoice.Description = Convert.ToString(dt.Rows[i]["Description"]).Trim(); 
                
                //invoice.VoucherNo  = Convert.ToString(dt.Rows[i]["V_No"]);
                invoice.Amount = Convert.ToString(dt.Rows[i]["AmountDr"]);
                model.Add(invoice);
            }





            return model;
        }

        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            var brandCode = "";// brandCodeTxbx.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/BrandWiseSaleSummaryCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&brandCode=" + brandCode);
        }
    }
}