﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.PaymentExpenceDrawings
{
    public partial class StationWiseExpenceDetail : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, string stationId)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(stationId, fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }


        public static List<InvoiceViewModel> GetTheReport(string stationId, DateTime StartDate, DateTime EndDate)
        {

            var model = new List<InvoiceViewModel>();


            ModItem objModItem = new ModItem();
            //int BrandID = Convert.ToInt32(brandCode);
           
            //////     

            SqlCommand cmd = new SqlCommand(
            "Select " +
                "V_No, " +
                "dateDr, " +
                "AmountCr, " +
                "Code1, " +
                "Narration, " +
                "StationId, " +
                "Station.Name as StationName, " +
                "GLCode.Title " +
            "from GeneralLedger " +
            "join Station on Station.Id = GeneralLedger.StationId " +
            "join GLCode on GeneralLedger.Code1 = GLCode.Code " +
            "where GeneralLedger.CompID = '" + CompID + "' and " +
                "V_Type = 'CPV'  and " +
                "GeneralLedger.Code = '0101010100001' and " +
                "SecondDescription = 'OperationalExpencesThroughCash' and " +
                "dateDr >= '"+ StartDate + "' and " +
                "dateDr <= '" + EndDate + "' and " +
                "StationId = "+ stationId+ " " +
            "order by dateDr desc", con);


            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            var dt = new DataTable();
            adpt.Fill(dt);
            
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string Date = Convert.ToDateTime(dt.Rows[i]["dateDr"].ToString()).ToString("dd/MM/yyyy");
                string CashPaid = dt.Rows[i]["AmountCr"].ToString();
                string V_No = dt.Rows[i]["V_No"].ToString();
                string Narration = dt.Rows[i]["Narration"].ToString();
                string StationId = dt.Rows[i]["StationId"].ToString();
                string StationName = dt.Rows[i]["StationName"].ToString();
                string Title = dt.Rows[i]["Title"].ToString();

                var ModelPaymentVoucher = new InvoiceViewModel()
                {
                    VoucherNo = V_No,
                    Date = Date,
                    Amount = CashPaid,
                    Narration = Narration,
                    StationId = StationId,
                    StationName = StationName,
                    ExpenseHead = Title
                };
                model.Add(ModelPaymentVoucher);
            }

                return model;
        }
        
    }
}