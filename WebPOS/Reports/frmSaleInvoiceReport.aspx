﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="frmSaleInvoiceReport.aspx.cs" Inherits="WebPOS.Reports.frmSaleInvoiceReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

      <style>

.Top {
    
   
    margin: auto;
    width: 80%;
    border: 3px solid green;
    padding: 10px;
   
    
}
.Botm {
    
   
    margin: auto;
    width: 80%;
    border: 3px solid green;
    padding: 10px;
   
    
}
/*tbody tr:first-child {
    position: absolute;
    top: 164px;
}*/
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
        
        <h2 style="text-align:center; font-size:20px; " >Sale Invoice</h2>
         
    
      
     <div class="Top">
        
            <asp:Label ID="Label7" runat="server" Text="Invoice No." Style="margin-left:0px;"></asp:Label>
            <asp:TextBox ID="txtInvoiceNo" runat="server" Style=";margin-left:32px;"> </asp:TextBox> <asp:Button ID="btnGetData" runat="server" Text="Get" OnClick ="btnGetData_Click" /><asp:Button ID="btnPrevious" runat="server" OnClick="btnPrevious_Click" Text="Previous"  /><asp:Button ID="btnNext" runat="server" Text="Next" OnClick="btnNext_Click" />
            <asp:Label ID="Label1" runat="server" Text="Customer" Style="margin-left:40px "></asp:Label> 
            <asp:TextBox ID="txtCustomerName" runat="server"  width="338px" Style="margin-left:10px;" ></asp:TextBox> 
             <asp:TextBox ID="txtCustomerCode" runat="server" Text="ClientCode" width="102px" ></asp:TextBox>
            <br />
            <asp:Label ID="Label6" runat="server" Text="Man. Bill" Style="margin-left:0px;"></asp:Label>
            <asp:TextBox ID="txtManualInvNo" runat="server" placeholder="Manual invoice no." Style="margin-left: 46px;"></asp:TextBox>
            <asp:Label ID="Label3" runat="server" Text="Address" Style="margin-left:200px;"></asp:Label><asp:TextBox ID="txtAddress" runat="server" width="448px" style=" margin-left:21px;"></asp:TextBox>  
            <br />
            <asp:Label ID="Label2" runat="server" Text="Date" Style="margin-left:0px" ></asp:Label><asp:TextBox ID="txtDate" runat="server" style="margin-left:81px; "></asp:TextBox>
            <asp:Label ID="Label5" runat="server" Text="Particular" style="margin-left:200px; "></asp:Label><asp:TextBox ID="txtParticular" runat="server" Style="margin-left:12px;" width="448px"></asp:TextBox>
            <br />
            <asp:Label ID="Label4" runat="server" Text="P. Balance" Style="margin-left:0px"></asp:Label><asp:TextBox ID="txtPreviousBalance" runat="server" style="margin-left:45px;"></asp:TextBox> 
        
    </div>
        

   <center>
         <div class="scroll">
        <asp:GridView ID="GridView1" AutoGenerateColumns="False" HeaderStyle-BackColor ="WhiteSmoke"   AlternatingRowStyle-BackColor ="WhiteSmoke"
      runat="server" OnRowDataBound="GridView1_RowDataBound" ShowFooter="True" Width="100%" FooterStyle-BackColor="Wheat" Height="90px" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical" >

<AlternatingRowStyle BackColor="#DCDCDC"></AlternatingRowStyle >
           
            <Columns >
                <asp:BoundField HeaderText="SN0" DataField="SN0" ReadOnly="true"  >
                <ItemStyle HorizontalAlign="Center" Width="50px" />
                </asp:BoundField>
                <asp:BoundField HeaderText="Code" DataField="Code" ReadOnly="true"  >
                <ItemStyle HorizontalAlign="Center" Width="60px" />
                </asp:BoundField>
                <asp:BoundField HeaderText="Description" DataField="Description" ReadOnly="true"  >
                <ItemStyle Width="500px" />
                </asp:BoundField>
                <asp:BoundField HeaderText="Quantity" DataField="Qty" ReadOnly="true"  >
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
                <asp:BoundField HeaderText="Rate" DataField="Rate" ReadOnly="true"  >
                <ItemStyle HorizontalAlign="Right" Width="120px" />
                </asp:BoundField>
                <asp:BoundField HeaderText="Gross Amount" DataField="Amount" ReadOnly="true"  >
                <ItemStyle HorizontalAlign="Right" Width="150px" />
                </asp:BoundField>
                <asp:BoundField HeaderText="Item Dis." DataField="Item_Discount" ReadOnly="true"  >
                <ItemStyle HorizontalAlign="Right" Width="100px" />
                </asp:BoundField>
                <asp:BoundField HeaderText="DealRs" DataField="DealRs" ReadOnly="true"  >
                <ItemStyle HorizontalAlign="Right" Width="100px" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Net Amount">
                       <ItemTemplate> 
                              <span style="text-align:left"> <asp:Label ID="Label1" runat="server"  Text='<%# Convert.ToDecimal( Eval("Amount"))-(Convert.ToDecimal( Eval("Item_Discount"))+Convert.ToDecimal( Eval("DealRs"))) %>'></asp:Label>
                             </span> 
                       </ItemTemplate>
                       <ItemStyle HorizontalAlign="Right" Width="150px" />
               </asp:TemplateField>
                
                
            </Columns>

<FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>

<HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White"></HeaderStyle>
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#0000A9" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#000065" />
        </asp:GridView>
        <br />

            </div>
        
        </center>

    <div class="Botm">

                <asp:Label  runat="server" Text="Percentage Discount" style="margin-left: 434px;" ></asp:Label> 
                <asp:TextBox ID="txtPerDisRate" runat="server" style="text-align:center"></asp:TextBox>          
                <asp:Label  runat="server" Text="Grand Total" ></asp:Label>
                <asp:TextBox ID="txtTotal" runat="server" style="text-align:right"  Font-Bold="True" Font-Size="Medium"></asp:TextBox>
            
      <br />
                
                <asp:Label  runat="server" Text="Deal Discount" Style="margin-left: 434px;" ></asp:Label> 
                <asp:TextBox ID="TextBox1" runat="server" style="text-align:right;margin-left: 40px;"  Font-Bold="True" Font-Size="Medium" Width="156"></asp:TextBox>
                <asp:Label  runat="server" Text="Net Dis." ></asp:Label>
                <asp:TextBox ID="txtNetDiscount" runat="server" style="text-align:right;margin-left:24px;"  Font-Bold="True" Font-Size="Medium" Width="181"></asp:TextBox>
        
       <br />

                <asp:Label  runat="server" Text="Flat Discount" Style="margin-left: 434px;" ></asp:Label> 
                <asp:TextBox ID="txtFlatDisc" runat="server" style="text-align:right;margin-left:45px;"  Font-Bold="True" Font-Size="Medium" Width="156"></asp:TextBox>
                <asp:Label  runat="server" Text="Paid" Style="margin-left: 0px;" ></asp:Label>   
                <asp:TextBox ID="txtPaid" runat="server" style="text-align:right; margin-left:49px;" Font-Bold="True" Font-Size="Medium"></asp:TextBox>
                
        <br />        

               <asp:Label  runat="server" Text="Bill Total" Style="margin-left: 434px;" ></asp:Label>  
               <asp:TextBox ID="txtBillTotal" runat="server" style="text-align:right;margin-left:71px;"  Font-Bold="True" Font-Size="Medium" Width="156"></asp:TextBox>
               <asp:Label  runat="server" Text="Balance" Style="margin-left: 0px;" ></asp:Label>     
               <asp:TextBox ID="txtBalance" runat="server" style="text-align:right;margin-left:26px; "  Font-Bold="True" Font-Size="Medium"></asp:TextBox>   
        <br />


        </div>
    
</asp:Content>
