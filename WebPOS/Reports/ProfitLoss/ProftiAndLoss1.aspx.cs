﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.ProfitLoss
{
    public partial class ProftiAndLoss1 : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to, bool isDetailRequired, int stationId)
        {

            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport( fromDate,  toDate, isDetailRequired,stationId);
                return new BaseModel { Success = true, ListofInvoices=model};

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false,Message=ex.Message};
            }
        }

        public static  List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate, bool isDetailRequired, int stationId)
        {
            var stationWhereClause = string.Empty;
            var stationExpenseWhereClause = string.Empty;
            if (stationId != 0)
            {
                stationWhereClause = "  and Invoice1.StationId=" + stationId;
            }
            SqlCommand cmd;
            if (!isDetailRequired)
            {
                 cmd = new SqlCommand("Select Invoice1.Date1 as Date,Invoice1.InvNo as Invoice, round(isnull(sum(Invoice2.Amount),0),2) as Sale , round(isnull(sum(Invoice2.Amount),0)-isnull(sum(Invoice2.Purchase_Amount),0),2) as Profit , round(isnull(sum(Invoice2.Purchase_Amount),0),2) as Cost   from (Invoice1 INNER JOIN Invoice2 ON Invoice1.InvNo = Invoice2.InvNo)  where  Invoice1.CompID='" + CompID + "' and Invoice1.Date1>='" + StartDate + "' and Invoice1.Date1<='" + EndDate +  "' " + stationWhereClause + " group by Invoice1.Date1,Invoice1.InvNo order by Invoice1.Date1", con);

            }
            else
            {
                 cmd = new SqlCommand("Select Invoice1.Date1 as Date,Invoice1.InvNo as Invoice, round(isnull(Invoice2.Amount,0),2) as Sale , round(isnull(Invoice2.Amount,0)-isnull(Invoice2.Purchase_Amount,0),2) as Profit , round(isnull(Invoice2.Purchase_Amount,0),2) as Cost   from (Invoice1 INNER JOIN Invoice2 ON Invoice1.InvNo = Invoice2.InvNo)  where  Invoice1.CompID='" + CompID + "' and Invoice1.Date1>='" + StartDate + "' and Invoice1.Date1<='" + EndDate + "'" + stationWhereClause + " order by Invoice1.Date1", con);
                
                
            }
            //OFFSET " + skip + " ROWS   FETCH NEXT 150 ROWS ONLY                             
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();

                invoice.Date = Convert.ToDateTime(dt.Rows[i]["Date"]).ToString("dd/MMM/yyyy");
                invoice.InvoiceNumber = Convert.ToString(dt.Rows[i]["Invoice"]);
                 invoice.Sale = Convert.ToString(dt.Rows[i]["Sale"]);
                invoice.Cost = Convert.ToString(dt.Rows[i]["Cost"]);
                invoice.Profit = Convert.ToString(dt.Rows[i]["Profit"]);
                model.Add(invoice);
            }

            return model;



        }
        protected void BtnClick(object sender, EventArgs e)
        {
            
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}