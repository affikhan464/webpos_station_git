﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebPOS.Reports.GeneralLedger
{
    public partial class frmGeneralLedgerSearchText : System.Web.UI.Page
    {
        string  CompID="01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                StartDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                txtEndDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
            }


        }
        

        void LoadReport(DateTime StartDate, DateTime EndDate)
        {


            DateTime abc = Convert.ToDateTime(StartDate);






            SqlCommand cmd = new SqlCommand("select V_No,V_Type,dateDr as Date,AmountDr as Debit,AmountCr as Credit,DescriptionOfBillNo,Narration,Description,ChequeNo  from GeneralLedger  where (AmountCr like '%" + txtSearch.Text + "%' or Narration like '%" + txtSearch.Text + "%' or AmountDr like '%" + txtSearch.Text + "%')  and CompID='" + CompID + "' and datedr>='" + StartDate + "' and datedr<='" + EndDate + "' order by System_Date_Time", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);
            GridView1.DataSource = dset;
            GridView1.DataBind();
        }
        
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                LoadReport(Convert.ToDateTime(StartDate.Text),Convert.ToDateTime(txtEndDate.Text) );

            }
            catch (Exception ex)
            {
                Response.Write("error  = " + ex.Message);

            }


        }

        decimal TotDr = 0;
        decimal TotCr = 0;
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // add the UnitPrice and QuantityTotal to the running total variables
                TotDr += Convert.ToDecimal(e.Row.Cells[2].Text);
                TotCr += Convert.ToDecimal(e.Row.Cells[3].Text);
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";
                // for the Footer, display the running totals
                e.Row.Cells[2].Text = TotDr.ToString();
                e.Row.Cells[3].Text = TotCr.ToString();

                e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Font.Bold = true;
            }
        }



    }
}