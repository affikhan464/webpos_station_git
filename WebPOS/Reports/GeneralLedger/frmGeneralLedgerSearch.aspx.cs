﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebPOS.Reports.GeneralLedger
{
    public partial class frmGeneralLedgerSearch : System.Web.UI.Page
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                StartDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                txtEndDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
            }

        }












       

        void LoadGLSingleSearchWithoutGroupping(DateTime StartDate, DateTime EndDate)
        {


            DateTime abc = Convert.ToDateTime(StartDate);






            SqlCommand cmd = new SqlCommand("select V_No,V_Type,dateDr as Date,Description,Convert(numeric(18,2), AmountDr)as Debit , Convert(numeric(18,2),AmountCr) as Credit ,DescriptionOfBillNo,BillNo,AmountDr-AmountCr as Balance from GeneralLedger where  CompID='" + CompID + "' and datedr>='" + StartDate + "' and datedr<='" + EndDate + "'", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);
            GridView1.DataSource = dset;
            GridView1.DataBind();
        }
        void LoadGLSingleSearchWithGroupping(DateTime StartDate, DateTime EndDate)
        {
            ModGLCode objModGLCode = new ModGLCode();   
            DateTime abc = Convert.ToDateTime(StartDate);
            SqlCommand cmd = new SqlCommand("select dateDr,Code,isnull(sum(AmountDr),0) as AmountDr ,isnull(sum(AmountCr),0) as AmountCr  from GeneralLedger  where  CompID='" + CompID + "' and datedr>='" + StartDate + "' and datedr<='" + EndDate + "' group by dateDr,Code", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);



            DataTable dt = new DataTable();
            dt.Columns.Add("Date");
            dt.Columns.Add("Description");
            dt.Columns.Add("Debit");
            dt.Columns.Add("Credit");
            dt.Columns.Add("Balance");
            dt.Columns.Add("DescriptionOfBill");
            dt.Columns.Add("BillNo");

           

            int j = 0;
            decimal ClosingBalance = 0;
            decimal Debit = 0;
            decimal Credit = 0;
            string GLTitle;
          

            for (int i = 0; i < dset.Tables[0].Rows.Count; i++)
            {
                Debit = Convert.ToDecimal(dset.Tables[0].Rows[i]["AmountDr"]);
                Credit = Convert.ToDecimal(dset.Tables[0].Rows[i]["AmountCr"]);
                GLTitle = objModGLCode.GLTitleAgainstGLCode(Convert.ToString( dset.Tables[0].Rows[i]["Code"]));
                ClosingBalance = (Debit - Credit);
                dt.Rows.Add();
                dt.Rows[j]["Date"] = Convert.ToDateTime(dset.Tables[0].Rows[i]["datedr"]).ToString("dd-MMM-yyyy"); ;
                dt.Rows[j]["Description"] = GLTitle;
                dt.Rows[j]["Debit"] = dset.Tables[0].Rows[i]["AmountDr"];
                dt.Rows[j]["Credit"] = dset.Tables[0].Rows[i]["AmountCr"];
                dt.Rows[j]["Balance"] = ClosingBalance;
                dt.Rows[j]["DescriptionOfBill"] = "";
                dt.Rows[j]["BillNo"] = "";

                j++;
            }


            GridView1.DataSource = dt;
            GridView1.DataBind();


        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (chkGrouping.Checked == false) { LoadGLSingleSearchWithoutGroupping(Convert.ToDateTime(StartDate.Text), Convert.ToDateTime(txtEndDate.Text)); }
                if (chkGrouping.Checked == true) { LoadGLSingleSearchWithGroupping(Convert.ToDateTime(StartDate.Text), Convert.ToDateTime(txtEndDate.Text)); }
            }
            catch (Exception ex)
            {
                Response.Write("error  = " + ex.Message);

            }
                

        }

        decimal TotDr = 0;
        decimal TotCr = 0;
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // add the UnitPrice and QuantityTotal to the running total variables
                TotDr += Convert.ToDecimal(e.Row.Cells[2].Text);
                TotCr += Convert.ToDecimal(e.Row.Cells[3].Text);
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";
                // for the Footer, display the running totals
                e.Row.Cells[2].Text = TotDr.ToString();
                e.Row.Cells[3].Text = TotCr.ToString();

                e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Font.Bold = true;
            }
        }



    }
}