﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace WebPOS.Reports.GeneralLedger
{
    public partial class frmUpToDateBankBalances : System.Web.UI.Page
    {
        string CompID = "01";
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {
            ////try
            ////{
            ////    LoadReport();

            ////}
            ////catch (Exception ex)
            ////{
            ////    Response.Write("error  = " + ex.Message);

            ////}


            LoadReport();

        }


        void LoadReport()
        {


            
            string Level3GLCode ="01010102";





            SqlCommand cmd = new SqlCommand("Select Code,Title,NorBalance from GLCode where Code like '" + Level3GLCode + "%' and lvl=5 order by Code", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);
            GridView1.DataSource = dset;
            GridView1.DataBind();
        }
        public string BankNameLocal(string BankCode)
        {

            ModGLCode objModGLCode=new ModGLCode();
            string BankName=objModGLCode.GLTitleAgainstGLCode(BankCode);
            return BankName;


        }

        public decimal BankBalanceDr(string BankCode)
        {

            ModGLCode objModGLCode = new ModGLCode();
            decimal BankClosingBalance = objModGLCode.GLClosingBalance(BankCode, 1,Convert.ToDateTime( DateTime.Now),Convert.ToDateTime( DateTime.Now));

            decimal abc = 0;
            if (BankClosingBalance > 0) { abc=BankClosingBalance; }
           
            return abc;


        }
        public decimal BankBalanceCr(string BankCode)
        {

            ModGLCode objModGLCode = new ModGLCode();
            decimal BankClosingBalance = objModGLCode.GLClosingBalance(BankCode, 1, Convert.ToDateTime(DateTime.Now), Convert.ToDateTime(DateTime.Now));

            decimal abc = 0;
            if (BankClosingBalance < 0) { abc = BankClosingBalance; }

            return abc;


        }
        decimal TotDr = 0;
        decimal TotCr = 0;
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // add the UnitPrice and QuantityTotal to the running total variables
                Label lblDr = (Label)e.Row.FindControl("lblDebit");
                TotDr += Convert.ToDecimal(lblDr.Text);
                Label lblCr = (Label)e.Row.FindControl("lblCredit");
                TotCr += Convert.ToDecimal(lblCr.Text);
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";
                // for the Footer, display the running totals
                e.Row.Cells[3].Text = TotDr.ToString();
                e.Row.Cells[4].Text = TotCr.ToString();

                e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[4].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Font.Bold = true;
            }
        }



    }
}