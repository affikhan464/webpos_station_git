﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="GeneralLedger.aspx.cs" Inherits="WebPOS.Reports.GeneralLedger.GeneralLedger" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

   
    <link href="../../css/allitems.css?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>" rel="stylesheet"  />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="searchAppSection reports">
        <div class="contentContainer">
            <h2>General Ledger</h2>
            <div class="BMSearchWrapper row">
                <!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->
                <div class="col col-12 col-sm-6 col-md-3">
                    <span class="userlLabel">From</span>
                    <asp:TextBox ID="fromTxbx" CssClass="datetimepicker fromTxbx Date" runat="server"></asp:TextBox>
                    <%--<input id="fromTxbx" class=" datetimepicker fromTxbx"  name="Date" type="text" />--%>
                </div>
                <div class="col col-12 col-sm-6 col-md-3">
                    <span class="userlLabel">To</span>
                    <asp:TextBox ID="toTxbx" CssClass="datetimepicker toTxbx Date" runat="server"></asp:TextBox>

                    <%--<input id="toTxbx" class="datetimepicker toTxbx" type="text"  name="Date"/>--%>
                </div>

                <%--<div  class="col col-12 col-sm-6 col-md-3">
                             <input id="expenceTextbox" data-id="expenceTextbox" data-type="ExpenceHead" data-function="GLList" data-glcode="01" class="autocomplete ExpenceHeadTitle BankTitle empty" name="PartyName" type="text" placeholder="Enter GL Titale" autocomplete="off"/>
							</div>
                            <div  class="col col-12 col-sm-6 col-md-3">
								<span class="userlLabel">Party Code</span>
                              <asp:TextBox id="GLCode" disabled="disabled" ClientIDMode="Static" CssClass="input__field input__field--hoshi ExpenceHeadCode empt GLCode"  name="PartyCode"  runat="server" autocomplete="off"></asp:TextBox>
                                
							</div>--%>
                <div class="col col-12 col-sm-6 col-md-3">

                    <input id="expenceTextbox" data-id="expenceTextbox" data-type="ExpenceHead" data-function="GLList" data-glcode="01" class="autocomplete ExpenceHeadTitle BankTitle empty" name="PartyName" type="text" placeholder="Enter GL Titale" autocomplete="off" />
                </div>
                <div class="col col-12 col-sm-6 col-md-3">
                    <span class="userlLabel">GL Code</span>

                    <asp:TextBox ID="GLCode" disabled="disabled" ClientIDMode="Static" CssClass="ExpenceHeadCode empt GLCode" name="PartyCode" runat="server" autocomplete="off"></asp:TextBox>
                </div>


                <div class="col col-12 col-sm-6 col-md-6">
                    <a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-spinner"></i>Get Record</a>
                </div>

                <div class="col col-12 col-sm-6 col-md-6">
                    <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>Print</a>
                </div>

                <%--<asp:Button ID="Button1" CssClass="printBtn" OnClick="BtnClick" runat="server"  Text="Print" />--%>
            </div>
            <!-- searchAppSection -->
        </div>
    </div>
    <!-- featuredSection -->

    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="height: 299px">
        <div class="contentContainer">

            <div class="DaySaleGroup2 reports recommendedSection allItemsView">


                <div class="itemsHeader d-flex pr-4">

                    <div class="five phCol flex-1">
                        <a href="javascript:void(0)">Date
                        </a>
                    </div>
                    <div class="five phCol flex-5">
                        <a href="javascript:void(0)">Description
                        </a>
                    </div>
                    <div class="five  phCol flex-1">
                        <a href="javascript:void(0)">V. No.
                        </a>
                    </div>
                    <div class="five  phCol flex-1">
                        <a href="javascript:void(0)">V. Type
                        </a>
                    </div>
                    <div class="five  phCol flex-1">
                        <a href="javascript:void(0)">Debit
                        </a>
                    </div>
                    <div class="five  phCol flex-1">
                        <a href="javascript:void(0)">Credit
                        </a>
                    </div>
                    <div class="five  phCol flex-1">
                        <a href="javascript:void(0)">Balance
											
                        </a>
                    </div>
                </div>
                <!-- itemsHeader -->
                <div class="loader">
                    <div class="display-table">
                        <div class="display-table-row">
                            <div class="display-table-cell">
                                <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="itemsSection">
                </div>
                <div class="itemsFooter">
                    <div class='itemRow newRow d-flex pr-4'>
                        <div class='five flex-1'><span></span></div>
                        <div class='five flex-5'><span></span></div>
                        <div class='five flex-1'><span></span></div>
                        <div class='five flex-1'><span></span></div>
                        <div class='five flex-1'>
                            <input disabled="disabled" name='totalDebit' /></div>
                        <div class='five flex-1'>
                            <input disabled="disabled" name='totalCredit' /></div>
                        <div class='five flex-1'>
                            <input disabled="disabled" name='closingBalance' /></div>

                    </div>
                </div>

            </div>

        </div>
        <!-- recommendedSection -->



    </div>

    <script src="<%=ResolveClientUrl("/Script/Vendor/jquery.min.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="<%=ResolveClientUrl("/Script/Vendor/jquery-ui.min.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="<%=ResolveClientUrl("/Script/Vendor/jquery.datetimepicker.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="<%=ResolveClientUrl("/Script/InitializeDateTimePicker.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>
    <script src="<%=ResolveClientUrl("/Script/Autocomplete.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>

    <script type="text/javascript">

        var counter = 0;

        $(document).ready(function () {
            initializeDatePicker();
            resizeTable();
        });

        $(window).resize(function () {
            resizeTable();

        });


        $("#toTxbx").on("keypress", function (e) {

            if (e.key == 13) {

                clearInvoice();
                counter = 0;
                getReport();

            }
        });
        $("#searchBtn").on("click", function (e) {
            counter = 0;

            clearInvoice();
            getReport();

        });

        function getReport() {
            $(".loader").show();

            $.ajax({
                url: '/Reports/GeneralLedger/GeneralLedger.aspx/getReport',
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ "from": $(".fromTxbx").val(), "to": $(".toTxbx").val(), "GLCode": $(".GLCode").val() }),
                success: function (response) {


                    if (response.d.Success) {
                        if (response.d.PrintReport.length > 0) {


                            for (var i = 0; i < response.d.PrintReport.length; i++) {
                                counter++;
                                $("<div class='itemRow newRow d-flex'>" +
                                    "<div class='five flex-1' ><span>" + response.d.PrintReport[i].Text_1 + "</span></div>" +
                                    "<div class='five flex-5'><span>" + response.d.PrintReport[i].Text_2 + "</span></div>" +
                                    "<div class='five flex-1'><span>" + response.d.PrintReport[i].Text_3 + "</span></div>" +
                                    "<div class='five flex-1'><span>" + response.d.PrintReport[i].Text_4 + "</span></div>" +
                                    "<div class='five flex-1' ><input  value=" + Number(response.d.PrintReport[i].Text_9).toFixed(2) + " disabled name='Debit'/></div>" +
                                    "<div class='five flex-1' ><input  value=" + Number(response.d.PrintReport[i].Text_10).toFixed(2) + " disabled name='Credit'/></div>" +
                                    "<div class='five flex-1' ><input  value=" + Number(response.d.PrintReport[i].Text_11).toFixed(2) + " disabled name='ClosingB'/></div>" +
                                    "</div>").appendTo(".itemsSection");


                            }
                            calculateData();
                            var closingBalance = $(".itemsSection .itemRow:last").find("[name=Balance]").val();
                            $(".itemsFooter [name=closingBalance]").val(closingBalance);
                            $(".loader").hide();

                        } else if (response.d.PrintReport.length == 0) {
                            $(".loader").hide(); swal({
                                title: "No Result Found ",
                                text: "No Result Found in the selected Date Range"
                            });
                        }

                    }
                    else {
                        $(".loader").hide();
                        swal({
                            title: "there is some error",
                            text: response.d.Message
                        });
                    }

                },
                error: function (error) {
                    swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                }
            });
        }
        function clearInvoice() {


            $(".itemsSection .itemRow").remove();
        }
        function calculateData() {

            var sum;
            var inputs = ["Debit", 'Credit'];
            for (var i = 0; i < inputs.length; i++) {

                var currentInput = inputs[i];
                sum = 0;
                $("input[name = '" + currentInput + "']").each(function () {

                    if (this.value.trim() === "") {
                        sum = (Number(sum) + Number(0));
                    } else {
                        sum = (Number(sum) + Number(this.value));

                    }

                });

                $("[name=total" + currentInput + "]").val('');
                $("[name=total" + currentInput + "]").val(Number(sum).toFixed(2));


            }


        }
    </script>
</asp:Content>
