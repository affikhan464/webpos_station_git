﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="SingleLedger.aspx.cs" Inherits="WebPOS.Reports.GeneralLedger.SingleLedger" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



    <div class="reports searchAppSection">
        <div class="contentContainer">
            <h2>Single Ledger</h2>
            <div class="BMSearchWrapper row">
                <!-- <span>First time here? <br /><a href="riskValue.html">Click here to read more</a> </span> -->

                <div class="col col-12 col-sm-6 col-md-3">
                    <span class="userlLabel">From</span>
                    <asp:TextBox ID="fromTxbx" CssClass="datetimepicker fromTxbx Date" runat="server"></asp:TextBox>
                    <%--<input id="fromTxbx" class=" datetimepicker fromTxbx"  name="Date" type="text" />--%>
                </div>
                <div class="col col-12 col-sm-6 col-md-3">
                    <span class="userlLabel">To</span>
                    <asp:TextBox ID="toTxbx" CssClass="datetimepicker toTxbx Date" runat="server"></asp:TextBox>

                    <%--<input id="toTxbx" class="datetimepicker toTxbx" type="text"  name="Date"/>--%>
                </div>
                <div class="col col-12 col-sm-6 col-md-3">
                    <span class="userlLabel">Bank Name</span>
                    <input id="expenceTextbox" data-type="ExpenceHead" data-id="expenceTextbox" data-function="GLList" data-glcode="01010102" name="PartyName" class=" autocomplete ExpenceHeadTitle BankTitle empt" type="text" autocomplete="off" />
                </div>
                <div class="col col-12 col-sm-6 col-md-3">
                    <span class="userlLabel">Bank Code</span>
                    <input id="txtCode" class="ExpenceHeadCode empt BankCode" type="text" autocomplete="off" />
                </div>
                <div class="col col-12 col-sm-6 col-md-6">
                    <a href="#" id="searchBtn" class="btn btn-bm"><i class="fas fa-list-ul"></i>Get Report</a>
                </div>

                <div class="col col-12 col-sm-6 col-md-6">
                    <a href="#" id="print" class="btn btn-bm"><i class="fas fa-print"></i>Print</a>
                    <%--<asp:Button ID="Button2" Cssclass="btn btn-bm"  runat="server" Text="Print" />--%>
                </div>
            </div>
            <!-- searchAppSection -->
        </div>
    </div>
    <!-- featuredSection -->

    <div class="businessManagerSellSection dayCashAndCreditSale businessManager BMtable" style="hfive flex-1: 299px">

        <div class="contentContainer">

            <div class="DaySaleGroup2 reports recommendedSection allItemsView">


                <div class="itemsHeader d-flex pr-4">
                    
                    <div class="five phCol flex-1">
                        <a href="javascript:void(0)">Date
                        </a>
                    </div>
                    <div class="five phCol flex-1">
                        <a href="javascript:void(0)">Voucher No.
                        </a>
                    </div>
                    <div class="five phCol flex-1">
                        <a href="javascript:void(0)">Voucher Type
                        </a>
                    </div>
                    <div class="five phCol flex-3">
                        <a href="javascript:void(0)">Description
                        </a>
                    </div>
                    <div class="five phCol flex-1">
                        <a href="javascript:void(0)">Debit
                        </a>
                    </div>
                    <div class="five phCol flex-1">
                        <a href="javascript:void(0)">Credit
                        </a>
                    </div>

                    <div class="five phCol flex-1">
                        <a href="javascript:void(0)">Balance
											
                        </a>
                    </div>



                </div>
                <!-- itemsHeader -->
                <div class="loader">
                    <div class="display-table">
                        <div class="display-table-row">
                            <div class="display-table-cell">
                                <i class="fas fa-spinner fa-spin  main_color_dark_shade"></i>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="itemsSection">
                </div>
                <div class="itemsFooter d-flex">
                    <div class='itemRow newRow pr-4'>
                        <div class='five flex-1'><span></span></div>
                        <div class='five flex-1'><span></span></div>
                        <div class='five flex-1 '><span></span></div>
                        <div class='five flex-3 '><span></span></div>
                        <div class='five flex-1 '>
                            <input disabled="disabled" name='totalDebit' /></div>
                        <div class='five flex-1 '>
                            <input disabled="disabled" name='totalCredit' /></div>
                        <div class='five flex-1 '>
                            <input disabled="disabled" name='closingBalance' /></div>

                    </div>
                </div>

            </div>

        </div>
        <!-- recommendedSection -->



    </div>

</asp:Content>
<asp:Content runat="server" ID="content4" ContentPlaceHolderID="Scripts">

    <script src="<%=ResolveClientUrl("/Script/Autocomplete.js")%>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>"></script>

    <script type="text/javascript">

        var counter = 0;

        $(document).ready(function () {
            initializeDatePicker();
            resizeTable();
        });

        $(window).resize(function () {
            resizeTable();

        });


        $("#toTxbx").on("keypress", function (e) {
            if (e.key == 13) {
                $("#currentPage").remove();
                $("<input hidden id='currentPage' value='0' />").appendTo("body");
                clearInvoice();
                counter = 0;
                getReport();

            }
        });
        $("#searchBtn").on("click", function (e) {
            counter = 0;
            $("#currentPage").remove();
            $("<input hidden id='currentPage' value='0' />").appendTo("body");
            clearInvoice();
            getReport();

        });
        function getReport() {
            $(".loader").show();

            $.ajax({
                url: '/Reports/GeneralLedger/SingleLedger.aspx/getReport',
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({ "from": $(".fromTxbx").val(), "to": $(".toTxbx").val(), "code": $(".ExpenceHeadCode").val() }),
                success: function (response) {


                    if (response.d.Success) {
                        if (response.d.Reports.length > 0) {


                            for (var i = 0; i < response.d.Reports.length; i++) {
                                counter++;
                                $("<div class='itemRow newRow d-flex'>" +

                                    "<div class='five flex-1'><span>" + response.d.Reports[i].Date + "</span></div>" +
                                    "<div class='five flex-1 name'> <span> " + response.d.Reports[i].VoucherNumber + " </span></div>" +
                                    "<div class='five flex-1  name'> <span> " + response.d.Reports[i].VoucherType + " </span></div>" +
                                    "<div class='five flex-3  name'> <span> " + response.d.Reports[i].Description + " </span></div>" +
                                    "<div class='five flex-1 '>             <input name='Debit' type='text' value=" + parseFloat(response.d.Reports[i].Debit).toFixed(2) + " disabled /></div>" +
                                    "<div class='five flex-1 '>      <input name='Credit' value=" + parseFloat(response.d.Reports[i].Credit).toFixed(2) + " disabled /></div>" +
                                    "<div class='five flex-1 '>      <input name='Balance'  value=" + parseFloat(response.d.Reports[i].Balance).toFixed(2) + " disabled /></div>" +
                                    "</div>").appendTo(".dayCashAndCreditSale .itemsSection");


                            }
                            calculateData();
                            var closingBalance = $(".itemsSection .itemRow:last").find("[name=Balance]").val();
                            $(".itemsFooter [name=closingBalance]").val(closingBalance);
                            $(".loader").hide();

                        } else if (response.d.Reports.length == 0) {
                            $(".loader").hide(); swal({
                                title: "No Result Found ", type:'error', type:'error',
                                text: "No Result Found in the selected Date Range"
                            });
                        }

                    }
                    else {
                        $(".loader").hide();
                        swal({
                            title: "there is some error",
                            text: response.d.Message
                        });
                    }

                },
                error: function (error) {
                    swal("Error", error.responseJSON.Message, "error"); $(".loader").hide();

                }
            });
        }
        function clearInvoice() {


            $(".itemsSection .itemRow").remove();
        }
        function calculateData() {

            var sum;
            var inputs = ["Payable", 'NetDiscount', 'Paid', 'Balance'];
            for (var i = 0; i < inputs.length; i++) {

                var currentInput = inputs[i];
                sum = 0;
                $("input[name = '" + currentInput + "']").each(function () {

                    if (this.value.trim() === "") {
                        sum = (parseFloat(sum) + parseFloat(0));
                    } else {
                        sum = (parseFloat(sum) + parseFloat(this.value));

                    }

                });

                $("[name=total" + currentInput + "]").val('');
                $("[name=total" + currentInput + "]").val(parseFloat(sum).toFixed(2));


            }

        }
    </script>
</asp:Content>
