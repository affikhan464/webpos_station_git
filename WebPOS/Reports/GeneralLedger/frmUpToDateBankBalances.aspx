﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="frmUpToDateBankBalances.aspx.cs" Inherits="WebPOS.Reports.GeneralLedger.frmUpToDateBankBalances" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="<%= ResolveUrl("~/css/style.css") %>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>" rel="stylesheet"  />
    <link href="<%= ResolveUrl("~/css/reset.css") %>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>" rel="stylesheet"  />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <br /> <h2 class="heading">Current Bank Balances</h2><br />
        
        

        <div style="width:100%">
     
            <center>

        <asp:GridView CssClass="grid" ID="GridView1" AutoGenerateColumns="False" HeaderStyle-BackColor ="WhiteSmoke"   AlternatingRowStyle-BackColor ="WhiteSmoke"
      runat="server" OnRowDataBound="GridView1_RowDataBound" ShowFooter="True"  FooterStyle-BackColor="Wheat" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical">
<AlternatingRowStyle BackColor="#DCDCDC"></AlternatingRowStyle>
      
            <Columns>

        
                
                
                <asp:TemplateField HeaderText="S. #"> 
               <ItemTemplate> <span style="text-align:left"> <asp:Label ID="Label1" runat="server"  Text='<%# Convert.ToInt32( Container.DataItemIndex+1) %>'></asp:Label>
               </span> 
               </ItemTemplate>
               <ItemStyle HorizontalAlign="Center" />
               </asp:TemplateField>
                
                <asp:BoundField HeaderText="Bank Code" DataField="Code" ReadOnly="true"  >  
                <ItemStyle HorizontalAlign="Center" />
                </asp:BoundField>
               

                <asp:TemplateField HeaderText="Bank Name" >
               <ItemTemplate> <span style="text-align:left"> <asp:Label ID="lblOp" runat="server"  Text='<%# BankNameLocal(Convert.ToString( Eval("Code"))) %>'></asp:Label></span> 
            </ItemTemplate>
               <ItemStyle HorizontalAlign="left" />
          </asp:TemplateField>

        
                <asp:TemplateField HeaderText="Debit">
                 <ItemTemplate> <span style="text-align:left"> <asp:Label ID="lblDebit" runat="server"  Text='<%# BankBalanceDr(Convert.ToString( Eval("Code"))) %>'></asp:Label></span> 
            </ItemTemplate>
               <ItemStyle HorizontalAlign="Right" />
          </asp:TemplateField>

                <asp:TemplateField HeaderText="Credit">
                 <ItemTemplate> <span style="text-align:left"> <asp:Label ID="lblCredit" runat="server"  Text='<%# BankBalanceCr(Convert.ToString( Eval("Code"))) %>'></asp:Label></span> 
            </ItemTemplate>
               <ItemStyle HorizontalAlign="Right" />
          </asp:TemplateField>

                
       
        
      </Columns>

<FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>

<HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White"></HeaderStyle>
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#0000A9" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#000065" />
    </asp:GridView>


    </div>
 







</asp:Content>
