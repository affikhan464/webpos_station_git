﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebPOS.Reports.Rebate
{
    public partial class frmBrandWiseRebateReceivedSummary : System.Web.UI.Page
    {
        string CompID = "01";
        string CashAccountGLCode = "01" + "01010100001";
        //CashAccountGLCode = CompID + "01010100001";
        Module8 objModParty = new Module8();
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtStartDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                txtEndDate.Text = DateTime.Today.ToString("MM/dd/yyyy");

                LoadBrandList();
            }
        }
        void LoadBrandList()
        {

            SqlCommand cmd = new SqlCommand("select Name from BrandName where CompID='01' order by Name", con);
            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);
            lstBrand.DataSource = dset;
            lstBrand.DataBind();
            lstBrand.DataTextField = "Name";
            lstBrand.DataBind();

        }



        void GetReport(DateTime StartDate, DateTime EndDate)
        {


            ModItem objModItem = new ModItem();
            decimal BrandCode = objModItem.BrandCodeAgainstBrandName(lstBrand.Text);
            if (chkBrand.Checked == true)
            {
                SqlCommand cmd = new SqlCommand("Select Rebate2.Date1  ,Rebate2.Code ,  Rebate2.InvNo  ,  Rebate2.PartyCode1 , Rebate2.SN0  ,  Rebate2.Description  ,  Rebate2.Rate  ,  Rebate2.Qty  ,  Rebate2.Amount from (Rebate2 inner join InvCode on Rebate2.Code=InvCode.Code) where Rebate2.CompID='" + CompID + "' and InvCode.Brand='" + BrandCode + "' and Rebate2.Date1>='" + StartDate + "' and Rebate2.Date1<='" + EndDate + "' and  Rebate2.Nature='Income' order by Rebate2.Date1", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataSet dset = new DataSet();
                adpt.Fill(dset);

                GridView1.DataSource = dset;
                GridView1.DataBind();
            }
            else
            if (chkBrand.Checked == false)
            {
                SqlCommand cmd = new SqlCommand("Select Rebate2.Date1  ,Rebate2.Code ,  Rebate2.InvNo  ,  Rebate2.PartyCode1 , Rebate2.SN0  ,  Rebate2.Description  ,  Rebate2.Rate  ,  Rebate2.Qty  ,  Rebate2.Amount from (Rebate2 inner join InvCode on Rebate2.Code=InvCode.Code) where Rebate2.CompID='" + CompID + "' and Rebate2.Date1>='" + StartDate + "' and Rebate2.Date1<='" + EndDate + "' and  Rebate2.Nature='Income' order by Rebate2.Date1", con);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                DataSet dset = new DataSet();
                adpt.Fill(dset);

                GridView1.DataSource = dset;
                GridView1.DataBind();
            }

        }

        public string NameAgainstCode(string Code)
        {


            string Name = objModParty.PartyNameAgainstCode(Code);
            return Name;
        }




        protected void Button1_Click(object sender, EventArgs e)
        {
            GetReport(Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
        }


        decimal TotQty = 0;
        decimal TotAmount = 0;

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // add the UnitPrice and QuantityTotal to the running total variables
                TotQty += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Qty"));
                TotAmount += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Amount"));


            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Totals:";
                // for the Footer, display the running totals
                e.Row.Cells[6].Text = TotQty.ToString();
                e.Row.Cells[8].Text = TotAmount.ToString();


                e.Row.Cells[6].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[8].HorizontalAlign = HorizontalAlign.Right;

                e.Row.Font.Bold = true;
            }
        }


    }
}