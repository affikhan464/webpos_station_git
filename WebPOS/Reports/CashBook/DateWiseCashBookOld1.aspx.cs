﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.CashBook
{
    public partial class DateWiseCashBookOld1 : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport( fromDate,  toDate);
                return new BaseModel { Success = true, ListofInvoices=model};

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false,Message=ex.Message};
            }
        }
        //public decimal OpeningCash(DateTime StartDate, DateTime EndDate)
        //{
            //decimal op = 0;
            //decimal OpeningCash = 0;
            //decimal Dr = 0;
            //decimal Cr = 0;

            //SqlDataAdapter cmd = new SqlDataAdapter("select OpeningBal from GLOpeningBalance where   CompID='" + CompID + "' and Code='" + "CashAccountGLCode" + "' and Date1='" + objModule7.StartDateTwo(StartDate) + "'", con);
            //DataTable dt = new DataTable();
            //cmd.Fill(dt);

            //if (dt.Rows.Count > 0)
            //{
            //    op = Convert.ToDecimal(dt.Rows[0]["OpeningBal"]);

            //}

            //SqlDataAdapter cmd1 = new SqlDataAdapter("select isnull(sum(AmountDr),0) as AmountDr , isnull(sum(AmountCr),0) as AmountCr from GeneralLedger where  CompID='" + CompID + "' and code='" + CashAccountGLCode + "' and Datedr>='" + objModule7.StartDateTwo(StartDate) + "' and datedr<'" + StartDate + "'", con);
            //DataTable dt1 = new DataTable();
            //cmd1.Fill(dt1);

            //if (dt1.Rows.Count > 0)
            //{
            //    Dr = Convert.ToDecimal(dt1.Rows[0]["AmountDr"]);
            //    Cr = Convert.ToDecimal(dt1.Rows[0]["AmountCr"]);
            //}

            //OpeningCash = (op + Dr) - Cr;
            //return OpeningCash;

        //}
        public static  List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate)
        {

            //ModPartyCodeAgainstName objParty = new ModPartyCodeAgainstName();   
            //SqlCommand cmd = new SqlCommand(
            //    "Select Invoice1.Date1 as [Date],Invoice1.InvNo as Invoice , Invoice1.Name as Name,Invoice1.PartyCode,Invoice3.Total as Total," +
            //        "Invoice3.Discount1 as Discount,Invoice3.CashPaid as Paid,Invoice3.Total-(Invoice3.Discount1+Invoice3.CashPaid) as Balance " +
            //    "from (Invoice1 inner join invoice3 on invoice1.InvNo=invoice3.InvNo) " +
            //    "where Invoice1.CompID='" + CompID + "' and  Invoice1.Date1>='" + StartDate + "' and Invoice1.Date1<='" + EndDate + "' " +
            //    " order by Invoice1.InvNo ", con);
            //    SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            //    DataTable dt = new DataTable();
            //    adpt.Fill(dt);
            ModCashBook objModCashBook = new ModCashBook();

            var model = new List<InvoiceViewModel>();


                var invoice = new InvoiceViewModel();
                //Opening Cash
                invoice.Description = "";
                invoice.PartyName = "Opening Cash In Hand";
                invoice.Paid = "0";
                invoice.OpeningBalance = Convert.ToString(objModCashBook.OpeningCash(StartDate,EndDate));
                model.Add(invoice);


            var invoice1 = new InvoiceViewModel();
            //Cash Sale
            invoice1.Description = "Cash Sale";
            invoice1.ClassType = "CashSale";
            invoice1.PartyName = "";
            invoice1.Paid = Convert.ToString(objModCashBook.CashSale(StartDate, EndDate));
            invoice1.OpeningBalance = Convert.ToString("");
            model.Add(invoice1);

            var invoice2 = new InvoiceViewModel();
            invoice2.Description = "Receipt from Parties";
            invoice2.ClassType = "ReceiptfromParties";
            invoice2.PartyName = "";
            invoice2.Paid = Convert.ToString(objModCashBook.CashReceivedFromParties(StartDate, EndDate));
            invoice2.OpeningBalance = Convert.ToString("");
            model.Add(invoice2);

            var invoice3 = new InvoiceViewModel();
            invoice3.Description = "Drawing from Bank";
            invoice3.ClassType = "DrawingfromBank";
            invoice3.PartyName = "";
            invoice3.Paid = Convert.ToString(objModCashBook.DrawingFromBank(StartDate, EndDate));
            invoice3.OpeningBalance = Convert.ToString("");
            model.Add(invoice3);
            

            var invoice4 = new InvoiceViewModel();
            invoice4.Description = "Cash Invenstment";
            invoice4.ClassType = "CashInvenstment";
            invoice4.PartyName = "";
            invoice4.Paid = Convert.ToString(objModCashBook.CashInvestmentFromDirector(StartDate, EndDate));
            invoice4.OpeningBalance = Convert.ToString("");
            model.Add(invoice4);
            

            var invoice5 = new InvoiceViewModel();
            invoice5.Description = "Cash incentive received";
            invoice5.ClassType = "Cashincentivereceived";
            invoice5.PartyName = "";
            invoice5.Paid = Convert.ToString(objModCashBook.CashIncentiveReceived(StartDate, EndDate));
            invoice5.OpeningBalance = Convert.ToString("");
            model.Add(invoice5);

            var invoice6 = new InvoiceViewModel();
            invoice6.Description = "Cash purchase return";
            invoice6.ClassType = "Cashpurchasereturn";
            invoice6.PartyName = "";
            invoice6.Paid = Convert.ToString(objModCashBook.CashPurchaseReturn(StartDate, EndDate));
            invoice6.OpeningBalance = "";
            model.Add(invoice6);

            var invoiceBlank = new InvoiceViewModel();
            invoiceBlank.Description = Convert.ToString("");
            invoiceBlank.PartyName = Convert.ToString("Total Cash Receipts");
            invoiceBlank.OpeningBalance = Convert.ToString((Convert.ToDecimal(invoice1.Paid) + Convert.ToDecimal(invoice2.Paid) + Convert.ToDecimal(invoice3.Paid) + Convert.ToDecimal(invoice4.Paid) + Convert.ToDecimal(invoice5.Paid) + Convert.ToDecimal(invoice6.Paid)));
            model.Add(invoiceBlank);


            var invoice7 = new InvoiceViewModel();
            invoice7.Description = "Cash Purchases";
            invoice7.ClassType = "CashPurchases";
            invoice7.PartyName = "";
            invoice7.Paid = Convert.ToString(objModCashBook.CashPurchase(StartDate, EndDate));
            invoice7.OpeningBalance = Convert.ToString("");
            model.Add(invoice7);

           

            var invoice8 = new InvoiceViewModel();
            invoice8.Description = "Cash Expence";
            invoice8.ClassType = "CashExpence";
            invoice8.PartyName = "";
            invoice8.Paid = Convert.ToString(objModCashBook.OperationalExpences(StartDate, EndDate));
            invoice8.OpeningBalance = Convert.ToString("");
            model.Add(invoice8);

            var invoice9 = new InvoiceViewModel();
            invoice9.Description = "Personal Drawing";
            invoice9.ClassType = "PersonalDrawing";
            invoice9.PartyName = "";
            invoice9.Paid = Convert.ToString(objModCashBook.PersonalDrawing(StartDate, EndDate));
            invoice9.OpeningBalance = Convert.ToString("");
            model.Add(invoice9);

            var invoice10 = new InvoiceViewModel();
            invoice10.Description = "Paid to Parties";
            invoice10.ClassType = "PaidtoParties";
            invoice10.PartyName = "";
            invoice10.Paid = Convert.ToString(objModCashBook.PaidToParty(StartDate, EndDate));
            invoice10.OpeningBalance = Convert.ToString("");
            model.Add(invoice10);

            var invoice11 = new InvoiceViewModel();
            invoice11.Description = "Deposit into Bank";
            invoice11.ClassType = "DepositintoBank";
            invoice11.PartyName = "";
            invoice11.Paid = Convert.ToString(objModCashBook.DepositInToBank(StartDate, EndDate));
            invoice11.OpeningBalance = Convert.ToString("");
            model.Add(invoice11);

            var invoice12 = new InvoiceViewModel();
            invoice12.Description = "Cash incentive given";
            invoice12.ClassType = "Cashincentivegiven";
            invoice12.PartyName = "";
            invoice12.Paid = Convert.ToString(objModCashBook.CashIncentiveGiven(StartDate, EndDate));
            invoice12.OpeningBalance = Convert.ToString("");
            model.Add(invoice12);

            var invoice13 = new InvoiceViewModel();
            invoice13.Description = "Cash Sale Return";
            invoice13.ClassType = "CashSaleReturn";
            invoice13.PartyName = "";
            invoice13.Paid = Convert.ToString(objModCashBook.CashSaleReturn(StartDate, EndDate));
            invoice13.OpeningBalance = ""; 
            model.Add(invoice13);

            var invoice14 = new InvoiceViewModel();
            invoice14.Description = "";
            invoice14.PartyName = Convert.ToString("Total Cash Payments");
            invoice14.OpeningBalance = Convert.ToString((Convert.ToDecimal(invoice7.Paid) + Convert.ToDecimal(invoice8.Paid) + Convert.ToDecimal(invoice9.Paid) + Convert.ToDecimal(invoice10.Paid) + Convert.ToDecimal(invoice11.Paid) + Convert.ToDecimal(invoice12.Paid) + Convert.ToDecimal(invoice13.Paid)));
            model.Add(invoice14);

            var invoice15 = new InvoiceViewModel();
            invoice15.Description = "";
            invoice15.PartyName = Convert.ToString("Closing Cash In Hand");
            invoice15.OpeningBalance = Convert.ToString((Convert.ToDecimal(invoice.OpeningBalance) + Convert.ToDecimal(invoiceBlank.OpeningBalance) - Convert.ToDecimal(invoice14.OpeningBalance) ));
            model.Add(invoice15);


            return model;


        }
        protected void BtnClick(object sender, EventArgs e)
        {
            
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            Response.Redirect("~/Reports/SaleReports/CReports/DayCashAndCreditSaleCR.aspx?startDate=" + startDate + "&endDate=" + endDate);
        }
    }
}