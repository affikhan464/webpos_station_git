﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace WebPOS.Reports.CashBook
{
    public partial class DateWiseCashBookOld2 : System.Web.UI.Page
    {
        string CompID = "01";
        string CashAccountGLCode = "01" + "01010100001";
        //CashAccountGLCode = CompID + "01010100001";
        Module7 objModule7 = new Module7();
        Module8 objModParty = new Module8();
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        SqlConnection con = new SqlConnection(connstr);
        decimal OpBalance = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtStartDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
                txtEndDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
            }
        }



        public decimal OpeningCash(DateTime StartDate,DateTime EndDate)
        {
            decimal op = 0;
            decimal OpeningCash=0;
            decimal Dr = 0;
            decimal Cr = 0;
            
            SqlDataAdapter cmd = new SqlDataAdapter("select OpeningBal from GLOpeningBalance where   CompID='" + CompID + "' and Code='" + CashAccountGLCode + "' and Date1='" + objModule7.StartDateTwo(StartDate) + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                op = Convert.ToDecimal(dt.Rows[0]["OpeningBal"]);

            }

            SqlDataAdapter cmd1 = new SqlDataAdapter("select isnull(sum(AmountDr),0) as AmountDr , isnull(sum(AmountCr),0) as AmountCr from GeneralLedger where  CompID='" + CompID + "' and code='" + CashAccountGLCode + "' and Datedr>='" + objModule7.StartDateTwo(StartDate) + "' and datedr<'" + StartDate + "'", con);
            DataTable dt1 = new DataTable();
            cmd1.Fill(dt1);

            if (dt1.Rows.Count > 0)
            {
                Dr = Convert.ToDecimal(dt1.Rows[0]["AmountDr"]);
                Cr = Convert.ToDecimal(dt1.Rows[0]["AmountCr"]);
            }

            OpeningCash = (op + Dr) - Cr;
            return OpeningCash;
            
        }
        public decimal CashSale(DateTime StartDate, DateTime EndDate)
        {
            decimal CashPaid = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(CashPaid),0) as CashPaid from Invoice3 where  date1>='" + StartDate + "' and date1<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                CashPaid = Convert.ToDecimal(dt.Rows[0]["CashPaid"]);

            }
            
            return CashPaid;

        }
        public decimal DrawingFromBank(DateTime StartDate, DateTime EndDate)
        {
            decimal DrawingFromB = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("Select  isnull(Sum(AmountDr),0) as AmountDr from GeneralLedger where Code='" + CashAccountGLCode + "' and SecondDescription='CashWithDrawForPettyCash'  and  dateDr>='" + StartDate + "' and dateDr<='" + EndDate +  "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                DrawingFromB = Convert.ToDecimal(dt.Rows[0]["AmountDr"]);

            }

            return DrawingFromB;

        }
        public decimal CashReceivedFromParties(DateTime StartDate, DateTime EndDate)
        {
            decimal CashReceivedFromPart = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(AmountDr),0) as AmountDr from GeneralLedger where Code='" + CashAccountGLCode + "' and SecondDescription='FromParties' and  dateDr>='" + StartDate + "' and dateDr<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                CashReceivedFromPart = Convert.ToDecimal(dt.Rows[0]["AmountDr"]);

            }

            return CashReceivedFromPart;

        }
        public decimal CashInvestmentFromDirector(DateTime StartDate, DateTime EndDate)
        {
            decimal CashInvestmentFromD = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(AmountDr),0) as AmountDr from GeneralLedger where  Code='" + CashAccountGLCode + "' and SecondDescription='CashInvestment'  and  dateDr>='" + StartDate + "' and dateDr<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                CashInvestmentFromD = Convert.ToDecimal(dt.Rows[0]["AmountDr"]);

            }

            return CashInvestmentFromD;

        }
        public decimal CashPurchaseReturn(DateTime StartDate, DateTime EndDate)
        {
            decimal CashPurchaseR = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(Paid),0) as Paid  from PurchaseReturn1 where  Date1>='" + StartDate +  "' and Date1<='" +  EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                CashPurchaseR = Convert.ToDecimal(dt.Rows[0]["Paid"]);

            }

            return CashPurchaseR;

        }
        public decimal CashPurchase(DateTime StartDate, DateTime EndDate)
        {
            decimal CashPurchase = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(Paid),0) as Paid  from Purchase3 where  Date1>='" + StartDate + "' and Date1<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                CashPurchase = Convert.ToDecimal(dt.Rows[0]["Paid"]);

            }

            return CashPurchase;

        }

        public decimal PaidToParty(DateTime StartDate, DateTime EndDate)
        {
            decimal PaidToP = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(AmountCr),0) as AmountCr from GeneralLedger where Code='" + CashAccountGLCode + "' and SecondDescription='CashPaidToParty' and  dateDr>='" + StartDate + "' and dateDr<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                PaidToP = Convert.ToDecimal(dt.Rows[0]["AmountCr"]);

            }

            return PaidToP;

        }
        public decimal DepositInToBank(DateTime StartDate, DateTime EndDate)
        {
            decimal DepositInToB = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(AmountCr),0) as AmountCr from GeneralLedger where Code='" + CashAccountGLCode + "' and SecondDescription='CashDepositIntoBank' and  dateDr>='" + StartDate + "' and dateDr<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                DepositInToB = Convert.ToDecimal(dt.Rows[0]["AmountCr"]);

            }

            return DepositInToB;

        }
        public decimal PersonalDrawing(DateTime StartDate, DateTime EndDate)
        {
            decimal DepositInToB = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(AmountCr),0) as AmountCr from GeneralLedger where Code='" + CashAccountGLCode + "' and SecondDescription='PersonalDrawingThroughCash' and  dateDr>='" + StartDate + "' and dateDr<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                DepositInToB = Convert.ToDecimal(dt.Rows[0]["AmountCr"]);

            }

            return DepositInToB;

        }
        public decimal OperationalExpences(DateTime StartDate, DateTime EndDate)
        {
            decimal DepositInToB = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(AmountCr),0) as AmountCr from GeneralLedger where Code='" + CashAccountGLCode + "' and SecondDescription='OperationalExpencesThroughCash' and  dateDr>='" + StartDate + "' and dateDr<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                DepositInToB = Convert.ToDecimal(dt.Rows[0]["AmountCr"]);

            }

            return DepositInToB;

        }
        public decimal CashSaleReturn(DateTime StartDate, DateTime EndDate)
        {
            decimal CashSaleR = 0;

            SqlDataAdapter cmd = new SqlDataAdapter("Select isnull(Sum(Paid),0) as Paid from SaleReturn1 where date1>='" + StartDate + "' and date1<='" + EndDate + "'", con);
            DataTable dt = new DataTable();
            cmd.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                CashSaleR = Convert.ToDecimal(dt.Rows[0]["Paid"]);

            }

            return CashSaleR;

        }

        void GetReport(DateTime StartDate, DateTime EndDate)
        {
            ModIncomeStatement objModIncomeStatement = new ModIncomeStatement();

            SqlCommand cmd = new SqlCommand("Select Rebate1.Date1  ,  Rebate1.InvNo  ,Rebate2.Code ,  Rebate2.SN0  ,  Rebate2.Description  ,Rebate1.Name,  Rebate2.Rate  ,  Rebate2.Qty  ,  Rebate2.Amount from (Rebate1 inner join Rebate2 on Rebate1.InvNo=Rebate2.InvNo) where Rebate1.Date1>='" + StartDate + "' and Rebate1.Date1<='" + EndDate + "' and  Rebate2.Nature='Expence' order by Rebate1.Date1", con);



            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataSet dset = new DataSet();
            adpt.Fill(dset);



            DataTable dt = new DataTable();
           
            dt.Columns.Add("Description");
            dt.Columns.Add("Debit");
            dt.Columns.Add("Credit");
            dt.Columns.Add("Balance");

            OpBalance = OpeningCash(Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
            dt.Rows.Add();
            dt.Rows[0]["Description"] = "Opening Balance";
            dt.Rows[0]["Debit"] = "0";
            dt.Rows[0]["Credit"] = "0";
            dt.Rows[0]["Balance"] = OpBalance;
            
            dt.Rows.Add();
            
            dt.Rows[1]["Description"] = "Cash Sale";
            dt.Rows[1]["Debit"] = CashSale(Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
            dt.Rows[1]["Credit"] = "0";
            dt.Rows[1]["Balance"] = "";
            
            dt.Rows.Add();
            
            dt.Rows[2]["Description"] = "Cash Received From Parties";
            dt.Rows[2]["Debit"] = CashReceivedFromParties(Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
            dt.Rows[2]["Credit"] = "0";
            dt.Rows[2]["Balance"] = "";
            
            dt.Rows.Add();
            
            dt.Rows[3]["Description"] = "Drawing From Bank";
            dt.Rows[3]["Debit"] = DrawingFromBank(Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
            dt.Rows[3]["Credit"] = "0";
            dt.Rows[3]["Balance"] = "";
            
            dt.Rows.Add();
            
            dt.Rows[4]["Description"] = "Cash Investment From Director";
            dt.Rows[4]["Debit"] = CashInvestmentFromDirector(Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
            dt.Rows[4]["Credit"] = "0";
            dt.Rows[4]["Balance"] = "";

            dt.Rows.Add();
            
            dt.Rows[5]["Description"] = "Cash Incentive Received";
            dt.Rows[5]["Debit"] = objModIncomeStatement.CashIncentiveReceived(Convert.ToDateTime(txtStartDate.Text) , Convert.ToDateTime(txtEndDate.Text));
            dt.Rows[5]["Credit"] = "0";
            dt.Rows[5]["Balance"] = "";
            
            dt.Rows.Add();
            
            dt.Rows[6]["Description"] = "Cash Purchase Return";
            dt.Rows[6]["Debit"] = CashPurchaseReturn(Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
            dt.Rows[6]["Credit"] = "0";
            dt.Rows[6]["Balance"] = "";

            dt.Rows.Add();
            
            dt.Rows[7]["Description"] = "Cash Purchase ";
            dt.Rows[7]["Debit"] = "0";
            dt.Rows[7]["Credit"] = CashPurchase(Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text)); 
            dt.Rows[7]["Balance"] = "";

            dt.Rows.Add();
            
            dt.Rows[8]["Description"] = "Paid To Parties";
            dt.Rows[8]["Debit"] = "0";
            dt.Rows[8]["Credit"] = PaidToParty(Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
            dt.Rows[8]["Balance"] = "";


            dt.Rows.Add();
            
            dt.Rows[9]["Description"] = "Operational Expence";
            dt.Rows[9]["Debit"] = "0";
            dt.Rows[9]["Credit"] = OperationalExpences(Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
            dt.Rows[9]["Balance"] = "";



            
            dt.Rows.Add();
            
            dt.Rows[10]["Description"] = "Deposited Into Bank";
            dt.Rows[10]["Debit"] = "0";
            dt.Rows[10]["Credit"] = DepositInToBank(Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
            dt.Rows[10]["Balance"] = "";

            dt.Rows.Add();
            
            dt.Rows[11]["Description"] = "Cash Incentive Given";
            dt.Rows[11]["Debit"] = "0";
            dt.Rows[11]["Credit"] = objModIncomeStatement.CashIncentiveGiven(Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
            dt.Rows[11]["Balance"] = "";

            dt.Rows.Add();
            
            dt.Rows[12]["Description"] = "Personal Drawings";
            dt.Rows[12]["Debit"] = "0";
            dt.Rows[12]["Credit"] = PersonalDrawing(Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
            dt.Rows[12]["Balance"] = "";


            dt.Rows.Add();
            
            dt.Rows[13]["Description"] = "Sale Return";
            dt.Rows[13]["Debit"] = "0";
            dt.Rows[13]["Credit"] = CashSaleReturn(Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
            dt.Rows[13]["Balance"] = "";

            
            
            GridView1.DataSource = dt;
            GridView1.DataBind();


        }

        public string NameAgainstCode(string Code)
        {


            string Name = objModParty.PartyNameAgainstCode(Code);
            return Name;
        }




        protected void Button1_Click(object sender, EventArgs e)
        {
            GetReport(Convert.ToDateTime(txtStartDate.Text), Convert.ToDateTime(txtEndDate.Text));
        }


        decimal TotDr = 0;
        decimal TotCr = 0;
        Decimal Opb = 0;

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // add the UnitPrice and QuantityTotal to the running total variables

                TotDr += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem,"Debit"));
                TotCr += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "Credit"));
                

            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[0].Text = "Balance:";
                // for the Footer, display the running totals
                e.Row.Cells[1].Text = TotDr.ToString();
                e.Row.Cells[2].Text = TotCr.ToString();
                e.Row.Cells[3].Text = Convert.ToString( (OpBalance+TotDr)-TotCr) ;
                

                e.Row.Cells[1].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[2].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Cells[3].HorizontalAlign = HorizontalAlign.Right;
                e.Row.Font.Bold = true;
            }
        }


    }
}