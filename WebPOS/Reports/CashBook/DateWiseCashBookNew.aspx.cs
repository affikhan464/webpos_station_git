﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebPOS.Model;

namespace WebPOS.Reports.CashBook
{
    public partial class DateWiseCashBookNew : System.Web.UI.Page
    {
        static string strConn = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();

        static SqlConnection con = new SqlConnection(strConn);
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static BaseModel getReport(string from, string to)
        {
            try
            {
                var fromDate = DateTime.ParseExact(from + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                      System.Globalization.CultureInfo.InvariantCulture);
                var toDate = DateTime.ParseExact(to + " " + "00:00:00", "dd/MM/yyyy HH:mm:ss",
                                    System.Globalization.CultureInfo.InvariantCulture);
                var model = GetTheReport(fromDate, toDate);
                return new BaseModel { Success = true, ListofInvoices = model };

            }

            catch (Exception ex)
            {
                con.Close();
                return new BaseModel { Success = false, Message = ex.Message };
            }
        }

        public static List<InvoiceViewModel> GetTheReport(DateTime StartDate, DateTime EndDate)
        {


            ModCashBook objModCashBook = new ModCashBook();
            ModGL objModGL = new ModGL();
            SqlCommand cmd = new SqlCommand(@"SELECT A.* FROM
                                                (
                                                    select  system_date_time, Code, Code1, V_No, V_Type, dateDr, Narration, AmountDr, AmountCr,
                                                            DescriptionOfBillNo, Description, ChequeNo
                                                    from 
                                                            GeneralLedger where CompID = '01' and  code1 <> '0103010100001'
                                                            and code = '0101010100001'  and datedr >= '" + StartDate + @"' and
                                                            datedr <= '" + EndDate + @"'
                                                    Union All
                                                            select dateDr system_date_time, '' Code, '0103010100001' Code1, '' V_No, '' V_Type, dateDr, 
                                                            '' Narration, sum(AmountDr) AmountDr, sum(AmountCr) AmountCr,
                                                            '' DescriptionOfBillNo, '' Description, '' ChequeNo
                                                    from 
                                                            GeneralLedger where CompID = '01' and  code1 = '0103010100001'
                                                            and code = '0101010100001'  and datedr >= '" + StartDate + @"' and
                                                            datedr <= '" + EndDate + @"'
                                                    group by dateDr
                                                    ) as A
                                                            ORDER BY A.system_date_time" , con);

            SqlDataAdapter adpt = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adpt.Fill(dt);
            var model = new List<InvoiceViewModel>();




            var invoiceOp = new InvoiceViewModel();
            invoiceOp.Date=Convert.ToDateTime(StartDate).ToString("dd-MMM-yyyy");
            invoiceOp.Description = "";
            invoiceOp.GLTitle = "Opening Cash In Hand";
            invoiceOp.Debit = "0";
            invoiceOp.Credit = "0";
            invoiceOp.ClosingBalance = Convert.ToString(objModCashBook.OpeningCash(StartDate, EndDate));
            model.Add(invoiceOp);








            decimal ClosingBalance = 0;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var invoice = new InvoiceViewModel();
                invoice.VoucherNo = (dt.Rows[i]["V_No"]).ToString();
                invoice.VoucherType = (dt.Rows[i]["V_Type"]).ToString();
                invoice.Date = Convert.ToDateTime(dt.Rows[i]["dateDr"]).ToString("dd-MMM-yyyy");
                invoice.GLTitle = Convert.ToString(objModGL.GLTitleAgainstCode( Convert.ToString( dt.Rows[i]["Code1"])));
                invoice.Description  = Convert.ToString(dt.Rows[i]["Description"]).Trim();
                invoice.Debit = Convert.ToString(dt.Rows[i]["AmountDr"]);
                invoice.Credit = Convert.ToString(dt.Rows[i]["AmountCr"]);




                /////--------closing Start
                if (i == 0)
                {
                     ClosingBalance = Convert.ToDecimal(invoiceOp.ClosingBalance) + Convert.ToDecimal(invoice.Debit) - Convert.ToDecimal(invoice.Credit);
                    invoice.ClosingBalance = Convert.ToString(ClosingBalance);
                }else
                    if (i > 0)
                {
                     ClosingBalance = Convert.ToDecimal(ClosingBalance) + Convert.ToDecimal(invoice.Debit) - Convert.ToDecimal(invoice.Credit);
                    invoice.ClosingBalance = Convert.ToString(ClosingBalance);
                }


                ////---------Closing End





                model.Add(invoice);
            }

            return model;

        }
        protected void BtnClick(object sender, EventArgs e)
        {
            var startDate = fromTxbx.Text;
            var endDate = toTxbx.Text;
            //var itemCode = itemCodeTxbx.Text;
            //Response.Redirect("~/Reports/SaleReports/CReports/ItemWiseSaleGroupByPartyCR.aspx?startDate=" + startDate + "&endDate=" + endDate + "&itemCode=" + itemCode);

        }
    }
}