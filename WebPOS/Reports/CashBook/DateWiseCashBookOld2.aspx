﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage.Master" AutoEventWireup="true" CodeBehind="DateWiseCashBookOld2.aspx.cs" Inherits="WebPOS.Reports.CashBook.DateWiseCashBookOld2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   

    <link href="<%= ResolveUrl("~/css/style.css") %>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>" rel="stylesheet"  />
    <link href="<%= ResolveUrl("~/css/reset.css") %>?v=<%=DateTime.Now.ToString("MMddyyyyhhmm") %>" rel="stylesheet"  />
   
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
        <br /> <h2 class="heading"  >Date Wise Cash Book Old 2</h2><br />
                  
        
   <div style ="width:100%;height:72%; text-align:center; vertical-align:middle">
 
    <table  border="0" style="width:100%;align-content:center">
        <tr   >
            <td >
                <asp:Label ID="Label2" runat="server" Text="Start Date" CssClass="lblcolor"></asp:Label>
                <asp:TextBox ID="txtStartDate" CssClass="datetimepicker"  runat="server" ></asp:TextBox>
                <asp:Label ID="Label3" runat="server" Text="End Date" CssClass="lblcolor"></asp:Label>
                <asp:TextBox ID="txtEndDate" CssClass="datetimepicker"  runat="server" ></asp:TextBox>
                <asp:Button ID="Button1" runat="server" Text="Report" OnClick="Button1_Click"  />
         </td>  
     </tr>
    </table>
        <asp:GridView ID="GridView1" CssClass="grid" AutoGenerateColumns="False" HeaderStyle-BackColor ="WhiteSmoke"   AlternatingRowStyle-BackColor ="WhiteSmoke"
      runat="server" OnRowDataBound="GridView1_RowDataBound" ShowFooter="True"  FooterStyle-BackColor="Wheat" BackColor="White" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical">
<AlternatingRowStyle BackColor="#DCDCDC"></AlternatingRowStyle>
      <Columns>
                
       

                <asp:BoundField HeaderText="Description" DataField="Description"  ReadOnly="true"  >
                <ItemStyle HorizontalAlign="Left" />
                </asp:BoundField>
    
                <asp:BoundField HeaderText="Debit" DataField="Debit"  ReadOnly="true"  >
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
       
                <asp:BoundField HeaderText="Credit" DataField="Credit"  ReadOnly="true"  >
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
       
                <asp:BoundField HeaderText="Balance" DataField="Balance"  ReadOnly="true"  >
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>


      </Columns>

<FooterStyle BackColor="#CCCCCC" ForeColor="Black"></FooterStyle>

<HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White"></HeaderStyle>
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#0000A9" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#000065" />
    </asp:GridView>

       </div>

<br />
<div >

    </div>


</asp:Content>
