'use strict';

const gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    cleanCss = require('gulp-clean-css'),
    debug = require('gulp-debug'),
    sassVariables = require('gulp-sass-variables');
    var exec = require('child_process').exec;

const scssInputDir = 'css/Sass Files/scss/';
const cssOutputDir = "css/";

gulp.task('style1',  function (done) {
    return exec(
        'start sass "'+scssInputDir+'_blue.scss" "'+cssOutputDir+'app-blue.css"'
    )
});

gulp.task('style2',  function (done) {
    return exec(
        'start sass "'+scssInputDir+'_bluecorel.scss" "'+cssOutputDir+'app-bluecorel.css"'
    )
});


gulp.task('style3',  function (done) {
    return exec(
        'start sass "'+scssInputDir+'_green.scss" "'+cssOutputDir+'app-green.css"'
    )
});


gulp.task('style4',  function (done) {
    return exec(
        'start sass "'+scssInputDir+'_purple.scss" "'+cssOutputDir+'app-purple.css"'
    )
});
gulp.task('style5',  function (done) {
    return exec(
        'start sass "'+scssInputDir+'_seagreen.scss" "'+cssOutputDir+'app-seagreen.css"'
    )
});
gulp.task('style6',  function (done) {
    return exec(
        'start sass "'+scssInputDir+'_orange.scss" "'+cssOutputDir+'app-orange.css"'
    )
});
gulp.task('style7',  function (done) {
    return exec(
        'start sass "'+scssInputDir+'_redy.scss" "'+cssOutputDir+'app-redy.css"'
    )
});
gulp.task('style8',  function (done) {
    return exec(
        'start sass "'+scssInputDir+'_darkpink.scss" "'+cssOutputDir+'app-darkpink.css"'
    )
});
gulp.task('style9',  function (done) {
    return exec(
        'start sass "'+scssInputDir+'_purplish.scss" "'+cssOutputDir+'app-purplish.css"'
    )
});

gulp.task('style10',  function (done) {
    return exec(
        'start sass "'+scssInputDir+'_whiteGray.scss" "'+cssOutputDir+'app-gray.css"'
    )
});


gulp.task('style', gulp.series('style1','style2','style3','style4','style5','style6','style7','style8','style9','style10', function (done) {
    done();
}));
//sass

