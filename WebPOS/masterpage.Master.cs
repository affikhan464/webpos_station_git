﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using WebPOS.Model;

namespace WebPOS
{
    public partial class masterpage : System.Web.UI.MasterPage
    {
        static string connstr = ConfigurationManager.ConnectionStrings["database"].ConnectionString;
        static string CompID = ConfigurationManager.AppSettings["CompID"].ToString();
        static SqlConnection con = new SqlConnection(connstr);
        
        public void Page_Load(object sender, EventArgs e)
        {
            Session["WebPOSVersion"] = "3.2";
            if (HttpContext.Current.Session["UserId"] == null)
            {
                Response.Redirect("~/Default.aspx?ReturnUrl="+ Request.Url.PathAndQuery);
            }

            ViewState["PageName"] = string.Empty;
            var userid = HttpContext.Current.Session["UserId"].ToString(); 
            var listPages = HttpContext.Current.Session["UserPermittedPages"] as List<MenuPage>;
            var permission = HttpContext.Current.Session["UserPermissions"] as UserPermisions;
            if (permission!=null)
            {
                ViewState["AllowedOtherStationsSelect"] = permission.AllowedOtherStationsSelect;
                ViewState["UserAllowedToSetSellingPrice"] = permission.UserAllowedToSetSellingPrice;
            }

            var ids = "";
            if (listPages != null)
            {
                if (listPages.Count > 0)
                {
                    ids = string.Join(",", listPages.Select(a => a.Id));
                }
            }


            UserPermittedPage.Attributes["value"] = ids;
            var isAdmin = userid == "0";  //0 mean admin
            if (Request.RawUrl.ToLower() != MenuPages.HomePage.ToLower() && !isAdmin)
            {
                var isPageExist = listPages.Any(a => Request.RawUrl.ToLower().Contains(a.Url.ToLower()));
                if (!isPageExist)
                {
                    Response.Redirect("~/Error/403.html");

                }
            }
        }


    }
}